const path = require("path");
const fs = require("fs");
const webpack = require("webpack");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const Clean = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const RemoveStrictPlugin = require("remove-strict-webpack-plugin");
// const HtmlWebpackExternalsPlugin = require("html-webpack-externals-plugin");
const RobotstxtPlugin = require("robotstxt-webpack-plugin");
const SitemapPlugin = require("sitemap-webpack-plugin").default;

const robotsConfig = require("./robots.config.js");
const sitemapConfig = require("./sitemap.config.js");
let metaConfig = require("./app/meta.config.js");
// const UglifyJSPlugin = require("uglifyjs-webpack-plugin");
// const MiniCssExtractPlugin = require("mini-css-extract-plugin");
// const ExtractTextPlugin = require("extract-text-webpack-plugin");


var __VERSION__ = require("./package.json").version;
// var locales = __GBL_CHAIN__ ? require("./app/assets/locales-gbl") : require("./app/assets/locales");;
var locales = require("./app/assets/locales");

module.exports = function(env) {
    const DEEX_CHAIN = env.deex_chain;
    const GBL_CHAIN = env.gbl_chain;
    const GBLTN_CHAIN = env.gbltn_chain;
    const SCROOGE_CHAIN = env.scrooge_chain;
    const PROD = env.prod;
    var root_dir = path.resolve(__dirname);
    console.log("root_dir", root_dir);
    var outputPath = path.join(root_dir, "assets");
    const baseUrl = "baseUrl" in env ? env.baseUrl : "";
    const testnet = "testnet" in env ? env.testnet : false;
    const release = PROD ? fs.readFileSync("release.txt") : "";


    let regexString = "";
    locales.forEach((l, i) => {
        regexString = regexString + (l + (i < locales.length - 1 ? "|" : ""));
    });
    const localeRegex = new RegExp(regexString);
    var cssLoaders = [
        {
            loader: "style-loader"
        },
        {
            loader: "css-loader"
        },
        {
            loader: "postcss-loader"
        }
    ];

    var scssLoaders = [
        {
            loader: "style-loader"
        },
        {
            loader: "css-loader"
        },
        {
            loader: "postcss-loader"
        },
        {
            loader: "sass-loader",
            options: {
                outputStyle: "expanded"
            }
        }
    ];
    var lessLoaders = [
        {
            loader: "style-loader"
        },
        {
            loader: "css-loader"
        },
        {
            loader: "postcss-loader"
        },
        {
            loader: "less-loader",
            options: {
                outputStyle: "expanded"
            }
        }
    ];

    metaConfig.meta.release = PROD ? release.toString() : "127.0.0.1";

    var plugins = [
        new HtmlWebpackPlugin({
            inject: true,
            template: "!!handlebars-loader!app/assets/index.hbs",
            environment: env.NODE_ENV,
            minify: {
                removeComments: PROD,
                collapseWhitespace: PROD,
                removeRedundantAttributes: PROD,
                useShortDoctype: PROD,
                removeEmptyAttributes: PROD,
                removeStyleLinkTypeAttributes: PROD,
                keepClosingSlash: PROD,
                minifyJS: PROD,
                minifyCSS: PROD,
                minifyURLs: PROD
            },
            templateParameters: {
                title: PROD ? metaConfig.title : "DEEX DEV",
                INCLUDE_BASE: PROD,
                PRODUCTION: PROD
            },
            meta: metaConfig.meta
        }),
        new webpack.DefinePlugin({
            APP_VERSION: JSON.stringify(__VERSION__),
            __ELECTRON__: false,
            __HASH_HISTORY__: false,
            __TESTNET__: testnet,
            __DEPRECATED__: false,
            __BASE_URL__: JSON.stringify(baseUrl),
            __UI_API__: JSON.stringify(
                env.apiUrl || "https://ui.bitshares.eu/api"
            ),
            __DEV__: !PROD,
            __DEEX_CHAIN__: DEEX_CHAIN,
            __SCROOGE_CHAIN__: SCROOGE_CHAIN,
            __GBL_CHAIN__: GBL_CHAIN,
            __GBLTN_CHAIN__: GBLTN_CHAIN,
            "process.env": {
                __DEV__: !PROD,
                __DEEX_CHAIN__: DEEX_CHAIN,
                __SCROOGE_CHAIN__: SCROOGE_CHAIN,
                __GBL_CHAIN__: GBL_CHAIN,
                __GBLTN_CHAIN__: GBLTN_CHAIN,
                __RELEASE__: JSON.stringify(PROD ? release.toString() : "DEV"),
                NODE_ENV: JSON.stringify(PROD ? "production" : "development")
            }
        }),
        new webpack.ContextReplacementPlugin(
            /moment[\/\\]locale$/,
            localeRegex
        ),
        new webpack.ContextReplacementPlugin(
            /react-intl[\/\\]locale-data$/,
            localeRegex
        ),
        new RemoveStrictPlugin(),
        /*new HtmlWebpackExternalsPlugin({
            externals: [
                {
                    module: "tradingview",
                    global: "tradingview",
                    entry: "https://static.deex.exchange/charting_library/charting_library.min.js",
                },
            ],
        }),*/
        new RobotstxtPlugin(robotsConfig),
        new SitemapPlugin("https://deex.exchange", sitemapConfig),
    ];

    if (PROD) {
        outputPath = path.join(root_dir, "build",
            GBL_CHAIN || GBLTN_CHAIN ? "dist-gbl" : 
            (SCROOGE_CHAIN ? "dist-scrooge" : 
            (DEEX_CHAIN ? "dist-deex" : "dist-bitshares")));
        plugins.push(new Clean([outputPath], {root: root_dir}));
        /*plugins.push(new MiniCssExtractPlugin({
            filename: "[name].[contenthash].css"
        }));

        cssLoaders = [
            {loader: MiniCssExtractPlugin.loader},
            {loader: "css-loader"},
            {
                loader: "postcss-loader",
                options: {
                    minimize: true,
                    debug: false
                }
            }
        ];
        scssLoaders = [
            {loader: MiniCssExtractPlugin.loader},
            {loader: "css-loader"},
            {
                loader: "postcss-loader",
                options: {
                    minimize: true,
                    debug: false
                }
            },
            {loader: "sass-loader", options: {outputStyle: "expanded"}}
        ];*/

        /*plugins.push(new ExtractTextPlugin("app.[chunkhash].css"));

        cssLoaders = ExtractTextPlugin.extract({
            fallback: "style-loader",
            use: [
                {loader: "cache-loader"},
                {loader: "css-loader"},
                {
                    loader: "postcss-loader",
                    options: {
                        plugins: [require("autoprefixer")]
                    }
                }
            ]
        });
        scssLoaders = ExtractTextPlugin.extract({
            fallback: "style-loader",
            use: [
                {loader: "css-loader"},
                {
                    loader: "postcss-loader",
                    options: {
                        plugins: [require("autoprefixer")]
                    }
                },
                {loader: "sass-loader", options: {outputStyle: "expanded"}}
            ]
        });*/





    } else {
        plugins.push(
            new webpack.DefinePlugin({
                "process.env": {NODE_ENV: JSON.stringify("development")},
                __DEV__: true
            })
        );
        plugins.push(new webpack.HotModuleReplacementPlugin());
    }

    plugins.push(new CopyWebpackPlugin([
        {
            from: path.join(root_dir, "app", "assets", "locales", "*.json"),
            to: path.join(outputPath, "locales", "[name].[ext]"),
            toType: "template"
        },
        {
            from: path.join(root_dir, "app", "assets", "charting_library"),
            to: path.join(outputPath, "charting_library")
        },{
            from: path.join(root_dir, "app", "assets", "static"),
            to: path.join(outputPath, "static")
        },
        {
            from: path.join(root_dir, "app", "lib", "common", "dictionary_en.json"),
            to: path.join(outputPath, "dictionary.json"),
            toType: "file"
        },
        {
            from: path.join(root_dir, "app", "assets","stylesheets","fonts"),
            to: path.join(outputPath, "fonts")
        }
    ], {}));

    const  entryApp = PROD ?
        path.resolve(root_dir, "app/Main.js") :
        [
            "react-hot-loader/patch",
            "webpack-hot-middleware/client",
            path.resolve(root_dir, "app/Main.js")
        ];

    return {
        mode: PROD ? "production" : "development",
        entry: {
            app: entryApp
        },
        output: {
            publicPath: PROD ? "" : "/",
            path: outputPath,
            pathinfo: !PROD,
            filename: PROD ? "[name].[chunkhash].js" : "[name].js",
            sourceMapFilename: PROD ? "[name].[chunkhash].js.map" : "[name].js.map",
            chunkFilename: PROD ? "[name].[chunkhash].js" : "[name].js"
        },
        module: {
            rules: [
                {
                    test: /\.jsx$/,
                    include: [
                        path.join(root_dir, "app"),
                        path.join(root_dir, "node_modules/react-foundation-apps")
                    ],
                    use: [
                        {
                            loader: "babel-loader",
                            options: {
                                cacheDirectory: PROD,
                                presets: [
                                    "@babel/preset-react",
                                    "@babel/preset-env",
                                ],
                                plugins: [
                                    "react-hot-loader/babel",
                                    "@babel/plugin-transform-async-to-generator"
                                ]
                            }
                        }
                    ]
                },{
                    test: /\.js$/,
                    include: [
                        path.join(root_dir, "app"),
                        path.join(root_dir, "node_modules/react-datepicker2")
                    ],
                    use: [
                        {
                            loader: "babel-loader",
                            options: {
                                cacheDirectory: PROD,
                                presets: [
                                    "@babel/preset-react",
                                    "@babel/preset-env",
                                ],
                                plugins: [
                                    "react-hot-loader/babel",
                                    "@babel/plugin-transform-async-to-generator"
                                ]
                            }
                        }
                    ]
                },
                {
                    test: /\.coffee$/, loader: "coffee-loader"
                },
                {
                    test: /\.(coffee\.md|litcoffee)$/,
                    loader: "coffee-loader?literate"
                },

                {
                    test: /\.md/,
                    use: [
                        {
                            loader: "html-loader",
                            options: {
                                removeAttributeQuotes: false
                            }
                        },
                        {
                            loader: "markdown-loader",
                            options: {}
                        }
                    ]
                },

                {
                    test: /\.json$/,
                    loader: "json-loader",
                    type: "javascript/auto",
                    exclude: [
                        /node_modules/,
                        path.resolve(root_dir, "app/lib/common"),
                        path.resolve(root_dir, "app/assets/locales")
                    ]
                },
                {
                    test: /\.png/,
                    use: [
                        {
                            loader: "file-loader"
                        }
                    ]
                },
                {
                    test: /.*\.svg$/,
                    loaders: [
                        "svg-inline-loader",
                        "svgo-loader"
                    ]
                },
                {
                    test: /\.css$/,
                    use: cssLoaders
                },
                {
                    test: /\.scss$/,
                    use: scssLoaders
                },
                {
                    test: /\.less$/,
                    use: lessLoaders
                },
                {
                    test: /\.(ttf|eot|woff|woff2)$/,
                    use: {
                        loader: "url-loader"
                    },
                },
            ]
        },
        resolve: {
            modules: [
                path.resolve(root_dir, "app"),
                path.resolve(root_dir, "app/lib"),
                "node_modules"
            ],
            alias: {
                "App": path.resolve(root_dir, "app"),
                "Components": path.resolve(root_dir, "app/components"),
                "Assets": path.resolve(root_dir, "app/assets"),
                "Styles": path.resolve(root_dir, "app/assets/stylesheets"),
                "Utility": path.resolve(root_dir, "app/components/Utility"),
            },
            extensions: [".js", ".jsx", ".coffee", ".json"]
        },
        optimization: {
            splitChunks: {
                cacheGroups: {
                    styles: {
                        name: "styles",
                        test: /\.css$/,
                        chunks: "all",
                        enforce: true
                    },
                    vendor: {
                        name: "vendor",
                        test: /node_modules/,
                        chunks: "initial",
                        enforce: true
                    }
                }
            },
            minimizer: [
                new TerserPlugin({
                    cache: true,
                    parallel: true,
                    terserOptions: {
                        ecma: 5,
                        warnings: false,
                        compress: {
                            drop_console: true
                        },
                    }
                })
            ]
        },
        devtool: !PROD ? "inline-cheap-module-source-map" : "cheap-source-map",
        plugins: plugins,
        node: {
            fs: "empty"
        }
    };
};

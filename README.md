HAQQ Exchange UI
============


```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.30.2/install.sh | bash
nvm install v6
nvm use v6
```

Once you have Node installed, you can clone the repo:

```
git clone https://gitlab.com/haqq-exchange/haqq-exchange-ui.git
cd haqq-exchange-ui
```

Before launching the GUI you will need to install the npm packages:

```
npm install
```

## Running the dev server

Dev server with automatic refresh on code changes. Only for debug and development.


```
npm run start
```

```
yarn start
```

## Production

```
npm run build
```
or

```
yarn build
```

### Deploy

```
firebase deploy 
firebase deploy --project=staging
```

### Add projects:

```
firebase use --add
```

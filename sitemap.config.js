module.exports = [
    {
        path: "/",
        lastMod: "2019-02-05T14:09:51+03:00",
        priority: "0.5",
        changeFreq: "monthly"
    },{
        path: "/authorization",
        lastMod: "2019-02-05T14:09:51+03:00",
        priority: "0.5",
        changeFreq: "monthly"
    },{
        path: "/create-account",
        lastMod: "2019-02-05T14:09:51+03:00",
        priority: "0.5",
        changeFreq: "monthly"
    },{
        path: "/market/DEEX_BTC",
        lastMod: "2019-02-05T14:09:51+03:00",
        priority: "0.5",
        changeFreq: "monthly"
    }
];

const path = require("path");
const fs = require("fs");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
// const HardSourceWebpackPlugin = require("hard-source-webpack-plugin");
const Clean = require("clean-webpack-plugin");
require("es6-promise").polyfill();
var locales = require("./app/assets/locales");
var __VERSION__ = require("./package.json").version;
// BASE APP DIR
var root_dir = path.resolve(__dirname);

module.exports = function(env) {
    // STYLE LOADERS
    var cssLoaders = [
        {
            loader: "cache-loader"
        },
        {
            loader: "style-loader"
        },
        {
            loader: "css-loader"
        },
        {
            loader: "postcss-loader"
        }
    ];

    var scssLoaders = [
        {
            loader: "style-loader"
        },
        {
            loader: "css-loader"
        },
        {
            loader: "postcss-loader",
            options: {
                plugins: [require("autoprefixer")]
            }
        },
        {
            loader: "sass-loader",
            options: {
                outputStyle: "expanded"
            }
        }
    ];

    // OUTPUT PATH
    var outputPath = path.join(root_dir, "assets");

    // COMMON PLUGINS
    const baseUrl = env.electron ? "" : "baseUrl" in env ? env.baseUrl : "";
    const PROD = env.prod;

    /*
    * moment and react-intl include tons of locale files, use a regex and
    * ContextReplacementPlugin to only include certain locale files
    */
    let regexString = "";
    locales.forEach((l, i) => {
        regexString = regexString + (l + (i < locales.length - 1 ? "|" : ""));
    });
    const localeRegex = new RegExp(regexString);
    const release = PROD ? fs.readFileSync("release.txt") : "";

    var plugins = [
        new HtmlWebpackPlugin({
            inject: true,
            title: PROD ? "DEEX Beta- " + release.toString() : "DEEX DEV",
            template: "./app/assets/index.html",
            minify: {
                removeComments: PROD,
                collapseWhitespace: PROD,
                removeRedundantAttributes: PROD,
                useShortDoctype: PROD,
                removeEmptyAttributes: PROD,
                removeStyleLinkTypeAttributes: PROD,
                keepClosingSlash: PROD,
                minifyJS: PROD,
                minifyCSS: PROD,
                minifyURLs: PROD
            }
        }),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.DefinePlugin({
            APP_VERSION: JSON.stringify(__VERSION__),
            __ELECTRON__: !!env.electron,
            __HASH_HISTORY__: !!env.hash,
            __BASE_URL__: JSON.stringify(baseUrl),
            __UI_API__: JSON.stringify(
                env.apiUrl || "https://ui.bitshares.eu/api"
            ),
            __TESTNET__: !!env.testnet,
            __DEPRECATED__: !!env.deprecated,
            __DEV__: !PROD,
            "process.env": {
                __DEV__: !PROD,
                __RELEASE__: JSON.stringify(PROD ? release.toString() : "DEV"),
                NODE_ENV: JSON.stringify(PROD ? "production" : "development")
            }
        }),
        new webpack.ContextReplacementPlugin(
            /moment[\/\\]locale$/,
            localeRegex
        ),
        new webpack.ContextReplacementPlugin(
            /react-intl[\/\\]locale-data$/,
            localeRegex
        ),
        new CopyWebpackPlugin([
            {
                from: path.join(root_dir, "app", "assets", "charting_library"),
                to: "charting_library"
            },
            {
                from: path.join(root_dir, "app", "lib", "common", "dictionary_en.json"),
                to: path.join(outputPath, "dictionary.json"),
                toType: "file"
            }
        ]),
        new MiniCssExtractPlugin({})
    ];

    if (PROD) {
        // PROD OUTPUT PATH
        let outputDir = env.electron
            ? "electron"
            : env.hash
                ? !baseUrl
                    ? "hash-history"
                    : `hash-history_${baseUrl.replace("/", "")}`
                : "dist";
        outputPath = path.join(root_dir, "build", outputDir);
        const cacheLoader = path.join(root_dir, ".cache-loader");

        // DIRECTORY CLEANER
        var cleanDirectories = [outputPath, cacheLoader];

        // WRAP INTO CSS FILE
        const extractCSS = new ExtractTextPlugin("app.[chunkhash].css");
        cssLoaders = ExtractTextPlugin.extract({
            fallback: "style-loader",
            use: [
                {loader: "cache-loader"},
                {loader: "css-loader"},
                {
                    loader: "postcss-loader",
                    options: {
                        plugins: [require("autoprefixer")]
                    }
                }
            ]
        });
        scssLoaders = ExtractTextPlugin.extract({
            fallback: "style-loader",
            use: [
                {loader: "css-loader"},
                {
                    loader: "postcss-loader",
                    options: {
                        plugins: [require("autoprefixer")]
                    }
                },
                {loader: "sass-loader", options: {outputStyle: "expanded"}}
            ]
        });

        // PROD PLUGINS
        plugins.push(new Clean(cleanDirectories, {root: root_dir}));
        plugins.push(extractCSS);
        plugins.push(
            new webpack.LoaderOptionsPlugin({
                minimize: true,
                debug: false
            })
        );
        plugins.push(new webpack.optimize.ModuleConcatenationPlugin());
        if (!env.noUgly) {
            plugins.push(
                new webpack.optimize.UglifyJsPlugin({
                    sourceMap: !PROD,
                    comments: !PROD,
                    cache: true,
                    parallel: true,
                    output: {
                        screw_ie8: true
                    },
                    compress: {
                        warnings: false,
                        drop_console: PROD,
                        booleans: false,
                        cascade: false,
                        collapse_vars: false,
                        comparisons: false,
                        hoist_funs: false,
                        hoist_vars: false,
                        if_return: false,
                        join_vars: false,
                        keep_infinity: true,
                        loops: false,
                        negate_iife: false,
                        properties: false,
                        reduce_vars: false,
                        sequences: false,
                        side_effects: false,
                        switches: false,
                        top_retain: false,
                        toplevel: false,
                        unused: false,

                        // Switch off all types of compression except those needed to convince
                        // react-devtools that we're using a production build
                        conditionals: true,
                        dead_code: true,
                        evaluate: true,
                    },
                    mangle: true,
                    ignores: [/charting_library/]
                })
            );
        }
    } else {
        // plugins.push(new webpack.optimize.OccurenceOrderPlugin());
        plugins.push(new webpack.HotModuleReplacementPlugin());
        plugins.push(new webpack.NoEmitOnErrorsPlugin());
    }

    var config = {
        entry: {
            app: PROD ? path.resolve(root_dir, "app/Main.js")
                : [
                    "react-hot-loader/patch",
                    "webpack-hot-middleware/client",
                    path.resolve(root_dir, "app/Main.js")
                ]
        },
        output: {
            publicPath: PROD ? "" : "/",
            path: outputPath,
            filename: PROD ? "[name].[chunkhash].js" : "[name].js",
            pathinfo: !PROD,
            sourceMapFilename: PROD
                ? "[name].[chunkhash].js.map"
                : "[name].js.map"
        },
        devtool: PROD ? "cheap-module-source-map" : "eval",
        module: {
            rules: [
                {
                    test: /\.jsx|\.js$/,
                    include: [
                        path.join(root_dir, "app"),
                        path.join(
                            root_dir,
                            "node_modules/react-foundation-apps"
                        )
                    ],
                    use: [
                        {
                            loader: "cache-loader"
                        },
                        {
                            loader: "babel-loader",
                            options: {
                                babelrc: false,
                                presets: [
                                    "react",
                                    [
                                        "env",
                                        {
                                            targets: {
                                                "uglify": true,
                                                browsers: ["last 2 chrome versions"],
                                            },
                                            modules: false,
                                        },
                                    ],
                                ],
                                plugins: [
                                    "react-hot-loader/babel",
                                    "loadable-components/babel",
                                    ["transform-class-properties", { loose: true }],
                                    "transform-object-rest-spread",
                                ]
                            }
                        }
                    ]
                },
                {
                    test: /\.js$/,
                    include: [
                        path.join(root_dir, "app"),
                        path.join(root_dir, "node_modules/react-datepicker2"),
                        path.join(root_dir, "node_modules/lodash-es")
                    ],
                    loader: "babel-loader",
                    exclude: /node_modules/,
                    options: {
                        babelrc: false,
                        presets: [
                            "react",
                            [
                                "env",
                                {
                                    targets: {
                                        "uglify": true,
                                        browsers: ["last 2 chrome versions"],
                                    },
                                    modules: false,
                                },
                            ],
                        ],
                        plugins: [
                            "react-hot-loader/babel",
                            "loadable-components/babel",
                            ["transform-class-properties", { loose: true }],
                            "transform-object-rest-spread",
                        ],
                    }
                },
                {
                    test: /\.json/,
                    use: [
                        "cache-loader",
                        "json-loader"
                    ],
                    exclude: [
                        path.resolve(root_dir, "app/lib/common"),
                        path.resolve(root_dir, "app/assets/locales")
                    ]
                },
                {test: /\.coffee$/, loader: "coffee-loader"},
                {
                    test: /\.(coffee\.md|litcoffee)$/,
                    loader: "coffee-loader?literate"
                },
                {
                    test: /\.css$/,
                    use: cssLoaders
                },
                {
                    test: /\.scss$/,
                    use: scssLoaders
                },

                {
                    test: /\.png/,
                    use: [
                        {
                            loader: "file-loader"
                        }
                    ]
                },
                {
                    test: /\.woff$/,
                    use: [
                        {
                            loader: "url-loader",
                            options: {
                                limit: 100000,
                                mimetype: "application/font-woff"
                            }
                        }
                    ]
                },
                {
                    test: /.*\.svg$/,
                    loaders: [
                        "cache-loader",
                        "svg-inline-loader",
                        "svgo-loader"
                    ]
                },
                {
                    test: /\.md/,
                    use: [
                        {
                            loader: "html-loader",
                            options: {
                                removeAttributeQuotes: false
                            }
                        },
                        {
                            loader: "remarkable-loader",
                            options: {
                                preset: "full",
                                typographer: true
                            }
                        }
                    ]
                }
            ]
        },
        resolve: {
            modules: [
                path.resolve(root_dir, "app"),
                path.resolve(root_dir, "app/lib"),
                "node_modules"
            ],
            alias: {
                "Components": path.resolve(root_dir, "app/components"),
                "Assets": path.resolve(root_dir, "app/assets"),
                "Utility": path.resolve(root_dir, "app/components/Utility"),
            },
            extensions: [".js", ".jsx", ".coffee", ".json"]
        },
        node: {
            fs: "empty"
        },
        plugins: plugins
    };

    return config;
};

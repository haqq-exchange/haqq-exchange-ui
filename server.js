var path = require("path");
var webpack = require("webpack");
var express = require("express");
var devMiddleware = require("webpack-dev-middleware");
var hotMiddleware = require("webpack-hot-middleware");
var bodyParser = require("body-parser");
var saveLocales = require("./app/assets/locales/save");
//var cloudStorage = require("./app/assets/locales/cloud_storage");
var excelDownload = require("./app/assets/locales/excel.download");
var fs = require("fs");

const perf_dev = process.argv[2] === "perf-dev";


var ProgressPlugin = require("webpack/lib/ProgressPlugin");
var config = require("./webpack.config.js")({
    prod: false,
    testnet: !!process.env.testnet,
    perf_dev,
    deex_chain: process.env.DEEX_CHAIN,
    gbl_chain: process.env.GBL_CHAIN,
    gbltn_chain: process.env.GBLTN_CHAIN,
    scrooge_chain: process.env.SCROOGE_CHAIN
});


var app = express();
var compiler = webpack(config);

var https = require("https");
var http = require("http");

/*
* https://github.com/IonicaBizau/edit-json-file
* */

app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));

compiler.apply(
    new ProgressPlugin(function(percentage, msg) {
        process.stdout.write(
            (percentage * 100).toFixed(2) +
            "% " +
            msg +
            "                 \033[0G"
        );
    })
);

app.use(
    devMiddleware(compiler, {
        publicPath: config.output.publicPath,
        historyApiFallback: true
    })
);

app.use(hotMiddleware(compiler));

// app.get("*", function(req, res) {
//     res.sendFile(path.join(__dirname, "app/assets/index.html"));
// });
/*
* Save json locals
* ./app/assets/locales/
* */
app.post("/save-locales", saveLocales);
app.post("/save-locales/download", excelDownload);

app.get("/*", function(req, res, next) {
    var filename = path.join(compiler.outputPath, "index.html");
    compiler.outputFileSystem.readFile(filename, function(err, result) {
        if (err) {
            return next(err);
        }
        res.set("content-type", "text/html");
        res.send(result);
        res.end();
    });
});

var options = {
    key: fs.readFileSync("./ssl/server.key"),
    cert: fs.readFileSync("./ssl/server.crt")
};

http.createServer(app).listen(8081);
https.createServer(options, app).listen(8086);

console.log("Listening at http://localhost:8081/ or https://localhost:8086/");
// new WebpackDevServer(compiler, {
//     publicPath: config.output.publicPath,
//     hot: true,
//     historyApiFallback: true,
//     quiet: false,
//     stats: {colors: true},
//     port: 8080
// }).listen(8080, '0.0.0.0', function (err, result) {
//     if (err) {
//         console.log(err);
//     }
//     console.log('Listening at 0.0.0.0:8080');
// });

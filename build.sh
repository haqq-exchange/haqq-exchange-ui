#!/usr/bin/env bash

BRANCH=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')
echo -e "Enter name branch current - "$BRANCH"  (Example develop|dev|master)";
read USER_BRANCH;

if [ $USER_BRANCH ]; then
    BRANCH=$USER_BRANCH;
fi

unamestr=`uname`
echo $unamestr
if [ "$unamestr" == "Linux" ] || [ "$unamestr" == "Darwin" ]; then
    echo "Update repo";
    git remote update
    echo "Checkout branch $BRANCH"
    git checkout $BRANCH
    git pull origin $BRANCH --force
    echo "Package install && Components build"
    yarn && yarn build
fi


The present loan agreement regulates the work of cryptoloan Deex platform (hereinafter - creditor, DeexLoan platform) and any user (hereinafter borrower) which refers to Deex with the request for the credit line on the account Deex (hereinafter account) in accordance with the following Terms.
##### 1. Terms and definitions
1.1. Credit line is a credit (loan) issued under the collateral of cryptocurrency funds in accordance with the rules and obligations of this agreement. 

1.2. Cryptocurrency is a digital analogue of money that has a value in monetary terms. 

1.3. Fiat currency is a legal means of payment whose value is set by the government and issued by it.

1.4. Lender – one of the participants in the credit relationship (obligations), which has the right to issue a loan and has the right to demand from another participant in the relationship — the debtor (borrower) certain behavior and performance of obligations under this agreement.

1.5. Borrower is a participant (party) of credit relations that receives a loan and assumes an obligation to return the received funds within the specified period, as well as to properly fulfill its obligations under the credit line which are indicated in the current agreement. 

1.6. Interest rate is the amount specified as a percentage of the loan amount paid by the recipient of the loan for using it for a certain period specified in this agreement.

1.7. Collateral is the amount of funds that act as collateral for the loan and guarantee the fulfillment of the borrower's obligations to the lender under this agreement.

1.8. Borrowed funds is the lender's funds received by the borrower for the period determined under this agreement.

1.9. Collateral ratio is the indicator of the exchange rate which determines difference between collateral and borrowed funds.

##### 2. Opening a credit line
2.1 The loan (hereinafter referred to as a credit line) is provided to the borrower at the lender's discretion, which means that there is no guarantee of approval of any application submitted by the borrower. In case of a negative decision on the application, the reasons for refusal are not disclosed. The credit line is provided for a period determined by the borrower and cannot be extended.  

2.2 Cryptocurrencies are sent to the account on behalf of the borrower. The borrower confirms that it understands and fully accepts the risks associated with cryptocurrency operations and transactions. The borrower is fully aware of the risk of losses (partial or full) in result of cryptocurrency transactions.

2.3 The lender is not obliged to evaluate, control or verify the use and distribution of borrowed funds.
#####  3. Maximum and minimum loan amount
3.1 Loans issued through the DeexLoan platform can be issued in one of the following fiat currencies: US dollar (USD), Russian ruble (RUB). The minimum amount of credit that can be provided through the DeexLoan platform is 1000.00 rubles and/or 10 USD. The maximum amount of credit that can be provided through the DeexLoan platform is 50000.00 rubles and/or 500 USD. The lending platform has the right to change these restrictions at any time for future credit lines.

##### 4. Collateral and interest rate 
4.1. The collateral value of the loan must always be sufficient to cover one hundred percent (100%) of the total amount owed on the credit line.

4.2. The interest rate is fixed and is 0.1% per day of the loan amount. DeexLoan has the right to change this rate at any time for any currency of the loan issued. At the same time, credit lines issued by the originally fixed rate will not be subject of such changes.

##### 5. Credit (loan) period
5.1 DeexLoan platform provides a credit line for a period of not less than one calendar month and not more than 365 days (one calendar year) from the moment of depositing the collateral.

##### 6. Margin call
6.1. If, by the lender's opinion, the cost of collateral is less than the required level the borrower is obliged to provide additional collateral at the lender's request and/or make a partial refund of the borrowed funds in order to maintain the collateral ratio in an acceptable range within the received credit line.

6.2. Any breach by the borrower of the obligation to provide any additional collateral and/or make a partial repayment of the borrowed funds within the specified period is an event of default (hereinafter referred to as «event of default») and allows the lender to exercise all or part of its rights related to the collateral, in particular the rights described below.

##### 7. Сollateral implementation
7.1 If the borrower fails to restore the required collateral level as it is requested by the lender or defaults as defined in the relevant section below, the lender is expressly authorized, but not obligated, to sell all or part of the assets pledged to the lender by the borrower or third parties in accordance with the collateral provided in article 3.1 above.

7.2 Borrower shall release the lender from any liability in accordance with such sale of assets pledged by the borrower to cover the collateral.

##### 8. Interest rate and interest payment
8.1 The annual interest rate applied to the credit line, as well as any fees associated with it, is determined at the time the credit line is provided. 

8.2 Interest is calculated based on the actual number of days that have passed in the 365-day year.

8.3 Interest is accrued daily and debited by the lender on a periodic basis after each reporting calendar day of the following month from the amount of collateral in case the borrower fails to pay the next payment before the specified date (inclusive) for the next payment. 

8.4 Borrower expressly authorizes the lender to automatically deduct any interest from the account without prior notice.

##### 9. Сredit line сlosing
9.1 Repayment of the loan (closing of the credit line) means repayment of borrowed funds and repayment of debt obligations to the lender. The loan (closing of the credit line) is considered to be repaid if all the existing debt obligations of the borrower to the lender are paid at the moment.    

9.2. Early repayment of the credit line means payment of the loan body and interest to the lender before the expiration date in accordance with paragraph 5.1 of this agreement.

9.3 Repayment of the loan (closing of the credit line) at the expense of collateral means the transfer to the lender of the borrower's funds previously established as collateral. If there is no positive balance on the borrower's wallet sufficient to repay the loan, the borrower can request repayment of the loan using collateral.

##### 10. Default
10.1 Occurrence of one of the following events is a default event:
- a) the borrower violates one of the obligations of this agreement, including, but not limited to any obligation to pay the lender any amount (principal, interest, or otherwise) under the credit line;
- b) the borrower violates another obligation to the lender (including any obligation arising from the collateral, including the obligation to maintain assets that the lender considers acceptable and adequate in accordance with article (3.1);
- c) the borrower is unable to pay its debt obligations for the final reporting period under the credit line;
- d) complete termination of the collateral (if the collateral value of the collateral is not sufficient to achieve the required level);
- e) the borrower's performance of its obligations under a credit line, pledge or other collateral is or becomes illegal for the borrower;
- f) from the date of conclusion of this Agreement there has been a significant deterioration (determined by the lender's discretion) of the borrower's business or financial situation, confirmed by the relevant documents.

10.2 If the creditor is aware of the occurrence of a default, it has the right to:
- a) immediately terminate the credit line;
- b) declare immediately payable any principal amount due in respect of a credit line equivalent to the amount borrowed, as well as accrued interest and any other amount due in respect of a credit line under debt obligations;
- c) implement, at the lender's discretion, all or part of the collateral in accordance with the applicable terms of the agreement; 
- d) take any action that the lender determines as necessary to protect its interests.

##### 11. Notifications
11.1 Communication with the lender and sending requests is carried out by e-mail to the address loan@deex.exchange.

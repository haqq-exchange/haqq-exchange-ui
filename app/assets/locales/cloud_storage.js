// Imports the Google Cloud client library
const {Storage} = require("@google-cloud/storage");
//console.log("Storage", Storage);
const storage = new Storage({
    projectId: "digital-axon-205214",
});
//console.log("storage", storage);
//var storage = require("@google-cloud/storage");
var myBucket = __SCROOGE_CHAIN__ ? storage.bucket("static.scrooge.club") : storage.bucket("static.haqq.exchange");
// const [files] = myBucket.getFiles();
async function createBucket() {
    // Creates the new bucket
    const [files] = await myBucket.getFiles();
    files.forEach(file => {
        console.log("file.name", file.name);
    });
}

//createBucket();

//console.log("myBucket", files);

const saveLocales = (req, res) => {
    //console.log("myBucket", myBucket)

    var file = __GBLTN_CHAIN__ ? myBucket.file("gbl/config/settings_api_gbltn.json") : (__GBL_CHAIN__ ? myBucket.file("gbl/config/settings_api.json") : myBucket.file("config/settings_api.json"));
    //console.log("file", file);
    var contents = "This is the contents of the file.";

    file.download().then(function(data) {
        if( data ) {
            let dataUtf = data.toString("utf-8");
            console.log("file", dataUtf);
            dataUtf.DEFAULT_WS_NODE = "SSSS";
        }
        return data;
        // console.log("apiResponse", apiResponse);
    }).then(data => {
        console.log("data", data.toString("utf-8"));
        file.save(data.toString("utf-8"), function(err) {
            console.log("data", data);
            if (!err) {
                // File written successfully.
            } else {
                console.log("err", err);
            }
        });
    });


    //console.log("file", file);

    /*//-
    // If the callback is omitted, we"ll return a Promise.
    //-*/
    file.save(contents).then(function() {
        res.set("content-type", "application/json");
        res.send({
            succes: true
        });
        res.end();
    });


};
module.exports = saveLocales;
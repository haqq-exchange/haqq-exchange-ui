var path = require("path");
var editJsonFile = require("edit-json-file");

const saveLocales = (req, res) => {
    const {
        set: bodySet ,
        unset: bodyUnset,
        files
    } = req.body;

    new Promise(resolve=>{
        files.map((file, index)=>{
            const jsonFile = path.join(__dirname, [file ,"json"].join("."));
            let fileJson = editJsonFile(jsonFile);
            if( bodyUnset ) {
                bodyUnset.map( path => fileJson.unset(path));
            }
            if( bodySet ) {
                for (let key in bodySet) {
                    if (bodySet.hasOwnProperty(key)) {
                        fileJson.set(key, bodySet[key]);
                    }
                }
            }
            fileJson.save(()=>{
                if( files.length === index+1 ) {
                    resolve();
                }
            });
        });

    }).then(()=>{
        res.set("content-type", "application/json");
        res.send({
            succes: true
        });
        res.end();
    });






};
module.exports = saveLocales;
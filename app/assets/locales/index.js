const locales = {
    "locale-tr": require("./locale-tr.json"),
    "locale-en": require("./locale-en.json"),
    "locale-ru": require("./locale-ru.json"),
    "locale-zh": require("./locale-zh.json")
};

module.exports = locales;

// const localesDeex = {
//     "locale-tr": require("./locale-tr.json"),
//     "locale-en": require("./locale-en.json"),
//     "locale-ru": require("./locale-ru.json"),
//     "locale-zh": require("./locale-zh.json")
// }

// const localesGbl = {
//     "locale-ru": require("./locale-ru-gbl.json"),
// }

// let locales = null;

// __GBL_CHAIN__ ? locales = localesGbl : locales = localesDeex;

// console.log("__GBL_CHAIN__", locales)

// module.exports = locales;
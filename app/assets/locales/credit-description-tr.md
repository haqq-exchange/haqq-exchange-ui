Bu kredi sözleşmesi, Deex kripto kredi platformunun (bundan böyle “borç veren”, “DeexLoan platformu” olarak anılacaktır) ve Deex kredi kullanan aşağıda listelenen koşullara göre  (bundan böyle "borçlu" olarak anılacaktır) Deex'ten Deex hesaplarına bir kredi limiti vermesini isteyen herhangi bir kullanıcı hesabı (bundan böyle "hesap" olarak anılacaktır).
##### 1. Terimler ve tanımlamalar:
1.1. Kredi limiti – işbu sözleşmenin kural ve yükümlülükleri çerçevesinde kripto para fonları ile teminat karşılığında verilen bir kredi. 

1.2. Kripto para - parasal açıdan değeri olan dijital bir nakit analogu. 

1.3. Fiat para birimi - değeri hükümet tarafından kurulan ve onun tarafından düzenlenen yasallaştırılmış ödeme araçları.

1.4. Borç veren -  kredisi verme hakkına sahip olan ve işbu sözleşme kapsamındaki yükümlülüklerin yerine getirilmesi için ilişkide başka bir katılımcıdan (borçludan) talepte bulunma hakkına sahip olan.

1.5. Borçlu - kredi alan ve öngörülen süre içinde alınan fonları iade etme yükümlülüğünü üstlenen ve kripto para birimi borç verme hizmetleri Deex'in mevcut sözleşmesinde belirtilen şartları uygun bir şekilde yerine getirme yükümlülüğünü yerine getiren bir kredi ilişkisinin katılımcısı (taraf). 

1.6. Faiz oranı - kredi alıcısı tarafından bu sözleşme çerçevesinde belirtilen belirli bir süre için kullanmak üzere ödenen kredi tutarının yüzdesi olarak belirtilen tutar.

1.7. Teminat - bir kredi için teminat görevi gören ve borçlu tarafından bu sözleşme uyarınca alacaklıya karşı yükümlülüklerini yerine getirmesini garanti eden fonlar.

1.8. Borçlu Fonlar - Borçlu tarafından bu sözleşme çerçevesinde belirlenen süre için alınan fonlar.

1.9. Teminat oranı - teminat ve borç alınan fonlar arasındaki döviz kuru farkının bir göstergesi. 
##### 2. Kredi limiti açılması
2.1 Kredi verenin takdirine bağlı olarak borçluya bir kredi (bundan böyle “kredi limiti” olarak anılacaktır) verilecektir, bu da borçlu tarafından sunulan başvuruların onaylanmasının reddedileceği anlamına gelir. Başvuruyla ilgili olumsuz karar verilmesi durumunda, ret nedenleri açıklanmaz. Kredi limiti borçlu tarafından belirtilen bir süre için sağlanır ve uzatılamaz.  

2.2 Kripto para birimleri, borçlu adına hesaba gönderilir. Borçlu, kripto para birimi işlemleri ve işlemleriyle ilişkili riskleri anladığını ve tamamen kabul ettiğini teyit eder. Borçlu, kripto para birimi işlemlerinden kaynaklanan kayıp (kısmi veya toplam) riskinin tamamen farkındadır.

2.3 Borç verenin ödünç alınan fonların kullanımını ve dağıtımını değerlendirmek, kontrol etmek veya yönlendirmek yükümlü olmadığı açıkça belirtilmektedir.
##### 3. Maksimum ve minimum kredi tutarı
3.1 DeexLoan platformu üzerinden verilen krediler aşağıdaki fiat para birimlerinden birinde verilebilir: ABD doları (USD), Rus rublesi (RUB). DeexLoan platformu üzerinden sağlanabilecek minimum kredi tutarı 1000,00 ruble ve / veya 10 USD. DeexLoan platformu üzerinden sağlanabilecek maksimum kredi tutarı 50.000.00 ruble. ve / veya 500 USD. Borç verme platformu, gelecekteki kredi limitleri için bu kısıtlamaları istediği zaman değiştirme hakkına sahiptir.
##### 4. Kredi teminatı ve faiz oranı
4.1. Kredi güvenliğinin teminat değeri her zaman bir kredi limitindeki toplam borç miktarının en az yüzde yüzünü (% 100) karşılamaya yeterli olmalıdır.

4.2. Faiz oranı sabittir ve kredi tutarının günlük % 0,1'i kadardır. DeexLoan, verilen kredinin herhangi bir para birimi için bu oranı herhangi bir zamanda değiştirme hakkına sahiptir. Ayrıca, başlangıçta sabit faiz oranıyla oluşturulan kredi limitleri bu tür değişikliklere tabi olmayacaktır.
##### 5. Kredi vadesi
5.1 DeexLoan platformu, teminat verildiği andan itibaren bir takvim ayından az olmayan ve 365 günden (bir takvim yılı) fazla olmayan bir süre için bir kredi limiti sağlar.
##### 6. Margin call
6.1. Borç vereninin görüşüne göre, teminatın maliyeti gerekli seviyeden düşükse, borçlu, alacaklının ek teminat sağlama ve / veya alınan kredinin çerçevesi içinde teminat oranını kabul edilebilir bir aralıkta tutmak için bir kısım krediyi talepte bulunma yükümlülüğünü yerine getirir. 

6.2. Borçlu tarafından herhangi bir ek teminat sağlama ve / veya ödünç alınan fonların öngörülen süre içinde kısmi bir geri ödeme yapma yükümlülüğünün ihlali, temerrüde düşülen bir olaydır (bundan böyle “yükümlülükleri ihlali” olarak anılacaktır) ve borç verenin teminatla ilgili haklarının tamamını veya bir kısmını kullanmasına izin verir, özellikle aşağıda açıklanan haklar.
##### 7. Teminatın satılması
7.1 Borçlu, borç verenin talebine göre gerekli güvenlik teminat seviyesini sağlayamazsa veya aşağıdaki ilgili bölümde tanımlandığı gibi bir temerrüt gerçekleşirse, borç veren, borçlunun veya üçüncü tarafların taahhüt ettiği varlıkların tamamını veya bir kısmını borçlu veya üçüncü şahıslar tarafından yukarıdaki 3.1 maddesinde öngörülen taahhüt ile teminatı kullanabilir.

7.2 Borçlu, borç verenin, borçlu tarafından kredi limiti sağlanmasını taahhüt ettiği temitan satışı ile ilgili herhangi bir yükümlülüğünden muaf olmasını sağlar.
##### 8. Faiz oranı ve faiz ödemesi
8.1 Kredi limitine uygulanacak yıllık faiz oranı ve bununla ilişkili ücretler, kredi limitinin verildiği tarihte belirlenir. 

8.2 Faiz, 365 günlük bir yılda geçen gerçek gün sayısına göre hesaplanır.

8.3 Faiz günlük olarak tahakkuk ettirilir ve bir sonraki ayın her raporlama takvim gününden sonra borçlu tarafından bir sonraki ödeme için belirtilen tarihe (dahil) kadar ödeme yapılmaması durumunda teminat tutarından periyodik olarak borçlanır. 

8.4 Borçlu, borç verene önceden bildirimde bulunmaksızın hesaptan doğan faizleri otomatik olarak düşmesine izin verir. 
##### 9. Kredi limitinin kapatılması
9.1 Bir kredinin geri ödenmesi (bir kredi limitinin kapanması), ödünç alınan fonların geri ödenmesini ve bir alacaklıya borcun geri ödenmesini ifade eder. Borçlunun alacaklıya olan mevcut tüm borç yükümlülüklerinin o andaki ödeme durumunda bir kredi (bir kredi limitinin kapanması) geri ödenmiş sayılır.    

9.2. Bir kredi limitinin erken geri ödemesi, kredi sözleşmesinin ve kredi sözleşmesinin sona ermesinden önce alacaklıya olan faizin, bu sözleşmenin 5.1 maddesine uygun olarak yapılması anlamına gelir..

9.3 Teminat karşılığı bir kredinin geri ödenmesi (bir kredi limitinin kapanması), daha önce teminat olarak tesis edilen borçlunun fonlarının alacaklıya havale edilmesi anlamına gelir. Borçlunun cüzdanında krediyi geri ödemek için yeterli bakiye yoksa, borçlu, teminat kullanarak kredinin geri ödenmesini talep edebilir.
##### 10. İflas
10.1 Aşağıdaki olaylardan birinin başlangıcı iflas olayıdır:
- a) borçlu, alacaklıya kredi limiti çerçevesinde herhangi bir tutarı (anapara, faiz veya başka türlü) ödeme yükümlülüğü dahil, ancak bunlarla sınırlı olmamak kaydıyla, kredi limiti ile ilgili yükümlülüklerden birini ihlal eder;
- b) borçlu, alacaklıya karşı başka bir yükümlülüğü ihlal eder (alacaklının madde (3.1) uyarınca kabul edilebilir ve yeterli olduğunu düşündüğü varlıkları koruma yükümlülüğü de dahil olmak üzere, rehin temin etmekten doğan herhangi bir yükümlülük dahil);
- c) borçlu, kredi raporunda nihai raporlama dönemi için borcunu ödeyemez durumdayken;
- d) teminatın tamamen sona erdirilmesi (teminatın teminat değeri gerekli güvenlik seviyesine ulaşmak için yeterli değilse);
- e) Borçlu tarafından bir kredi limiti, rehin veya diğer teminat kapsamındaki yükümlülüklerinin yerine getirilmesi, borçlunun yasadışı olması veya yasa dışı hale gelmesi;
- f) işbu sözleşme imzalanmasından bu yana, borçlunun ticari veya finansal durumu üzerinde ilgili belgelerle teyit edilen (alacaklının takdirine bağlı olarak) önemli bir bozulma olmuşsa.

10.2 Kredi veren bir temerrütten ve iflastan haberdar ise, haklara sahip:
- а) kredi limitini derhal feshedebilir.
- б) Borçlu fonların tutarına eşdeğer bir kredi limiti, borç yükümlülükleri çerçevesinde tahakkuk eden faiz ve kredi limiti ile ilgili diğer tüm tutarlar için anapara dahhil derhal ödenecek;
- в) geçerli sözleşme şartlarına uygun olarak teminatın tamamının veya bir kısmının alacaklının takdirine bağlı olarak satmak; 
- г) alacaklının çıkarlarını korumak için gerekli gördüğü her türlü işlemi yapmak.
##### 11. Bilgilendirme
11.1 Alacaklı ile iletişim ve talep gönderme, loan@deex.exchange adresine e-posta göndererek yapılır. 

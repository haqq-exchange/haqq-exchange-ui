module.exports = {
    darkTheme: {
        bidColor: "#429321",
        bidFillColor: "rgba(180, 236, 81, 0)",
        askColor: "rgba(208, 2, 27, 0.8)",
        askFillColor: "rgba(234, 5, 59, 0)",
        callColor: "#BBBF2B",
        settleColor: "rgba(125, 134, 214, 1)",
        settleFillColor: "rgba(125, 134, 214, 0.5)",
        positiveColor: "#258A14",
        negativeColor: "#DB0000",
        strokeColor: "#FFFF00",
        primaryText: "#e0e0e0",
        volumeColor: "#848484",

        //tooltip
        tooltipBackgroundColor: "rgba(0, 0, 0, 0.4)",
        tooltipColor: "#fff",
        tooltipFillColor: "#fff",
        //axis
        axisLabelsColor: "#fff",
        axisLineColor: "#AAAAAA",
        indicatorLineColor: "#FFFFFF",
        bgColor: "#2a2a2a",
        toolBarbg: "#383838",
        horzGridProperties: {color:"#363c4e"},
        vertGridProperties: {color:"#363c4e"},
        textColor: "#e0e0e0"
    },

    lightTheme: {
        bidColor: "#429321",
        bidFillColor: "rgba(180, 236, 81, 0)",
        askColor: "#d0021b",
        askFillColor: "rgba(234, 5, 59, 0)",
        callColor: "#BBBF2B",
        settleColor: "rgba(125, 134, 214, 1)",
        settleFillColor: "rgba(125, 134, 214, 0.5)",
        positiveColor: "#74BC2A",
        negativeColor: "#DE2233",
        strokeColor: "rgba(0,0,0,0)",
        primaryText: "#242424",
        volumeColor: "#848484",

        //tooltip
        tooltipBackgroundColor: "rgba(255,255,255, 0.9)",
        tooltipColor: "#000",
        tooltipFillColor: "#000",
        //axis
        axisLabelsColor: "#000",
        axisLineColor: "#000",
        indicatorLineColor: "#848484",
        bgColor: "#ffffff",
        toolBarbg: "#fafafa",
        horzGridProperties: {color:"#e1ecf2"},
        vertGridProperties: {color:"#e1ecf2"},
        textColor: "#3d3d3d"
    }
};

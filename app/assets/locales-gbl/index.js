const locales = {
    "locale-tr": require("./locale-tr.json"),
    "locale-en": require("./locale-en.json"),
    "locale-ru": require("./locale-ru.json"),
    "locale-zh": require("./locale-zh.json")
};

module.exports = locales;
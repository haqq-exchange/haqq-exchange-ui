.balance-row.active {
    background-color: #3c3c3c;
}

.balance-row:hover {
    background-color: #3c3c3c;
}

.balance-row {
    margin-left: -10px;
    margin-right: -10px;
    padding: 10px 20px;
}

.balance-row:hover {
    cursor: pointer;
}

.SimpleTrade__help-text {
    font-style: italic;
    padding-top: 5px;
    font-size: 95%;
}

div.button.create-account {
    font-size: 1.2rem;
}

div.input-wrapper {
    > div.input-right-symbol {
        font-size: 0.9rem;
        @include RobotoMedium;
        position: absolute;
        top: 12px;
        right: 8px;
    }
}

.SimpleDepositBridge__info-row {
    > div {
        padding-bottom: 5px;
        > div.float-right {
            clear: both;
            @include RobotoMedium;
        }
    }
}

// QR canvas
.QR {
    padding: 1rem;
    > canvas {
        background: white;
        padding: 0.5rem;
    }
}

.copyIcon {
    padding: 0.4rem;
    margin-right: 1rem;
    vertical-align: middle;
    > button.copyIcon {
        height: inherit;
    }
}

@media only screen and (max-width: 55em) {
    .small-up-1 > div {
        flex: 0 0 100%;
        max-width: 100%;
        margin-bottom: 10px;
        &:last-child {
            margin-bottom: 0;
        }
    }
}
@media only screen and (min-width: 55em) and (max-width: 75em) {
    .medium-up-3 > div {
        margin: 1%;
        max-width: 32%;
        flex: 0 0 32%;
        &:nth-child(3n + 1) {
            margin-left: 0;
        }
        &:nth-child(3n + 3) {
            margin-right: 0;
        }
        &:nth-child(-n + 3) {
            margin-top: 0;
        }
    }
}
@media only screen and (min-width: 75em) {
    .large-up-4 > div {
        margin: 0.82% 0.82%;
        max-width: 23.77%;
        flex: 0 0 23.8%;
        &:nth-child(4n + 1) {
            margin-left: 0;
        }
        &:nth-child(4n + 4) {
            margin-right: 0;
        }
        &:nth-child(-n + 4) {
            margin-top: 0;
        }
        &:nth-last-child(-n + 4):not(.market-box) {
            margin-bottom: 0;
        }
    }
}

.section {
    margin-bottom: 53px;
    &:last-child {
        margin-bottom: 0;
    }
}

.fm-container {
    &__item {
        display: flex;
        justify-content: space-between;
        align-items: flex-end;
    }
    &__box-image {
        background-color: #fff;
        border-radius: 50%;
        width: 44px;
        height: 44px;
    }
    &__image {
        max-width: 44px;
        height: auto;
    }
    &__value {
        font-size: 14px;
        font-weight: 500;
        color: #8c979b;
    }
    &__procent {
        font-size: 14px;
        font-weight: 500;
        &_positive {
            color: #579f15;
        }
        &_negative {
            color: #de2233;
        }
    }
    &__currency {
        font-size: 13px;
        font-family: "AvenirNext-DemiBold";
        text-transform: uppercase;
    }
}

.filters-box {
    .fm-container {
        display: flex;
        padding: 14px 15px;
    }
    &__content {
        margin-left: 13px;
        height: 42px;
        width: 100%;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
    }
    &__name {
        font-size: 15px;
        font-weight: 500;
    }
    &__currency {
        padding-top: 4px;
    }
}

.market-box {
    &__content {
        padding: 11.3px 15px 11.99px;
        &_bg {
            background-color: #f1f3f5;
        }
    }
    &__header {
        border-bottom: 1px solid #d1d4db;
        position: relative;
    }
    &__box-image {
        position: absolute;
        top: 14px;
        right: 13px;
    }
    &__item {
        margin-bottom: 15px;
        &:last-child {
            margin-bottom: 0;
        }
        &-value {
            height: 12px;
        }
    }
}

div.fm-container:hover {
    border-color: #fca032;
    cursor: pointer;
    background-color: #fff;
    .market-box__content_bg {
        background-color: #fca032;
        color: #fff;
    }
}

.block-content-header {
    font-size: 19px !important;
    padding: 0 !important;
}

div.fm-container {
    border: 1px solid #d1d4db;
    border-radius: 4px;
    background-color: #fff;
    padding: 0;
    &:hover {
        cursor: pointer;
    }

    .fm-title,
    .fm-volume,
    .fm-change {
        font-size: 0.85rem;
    }

    .fm-name {
        @include RobotoMedium;
        padding-bottom: 10px;
        font-size: 1.125rem;
    }

    .fm-volume,
    .fm-change {
        text-transform: lowercase;
        font-variant: small-caps;
        padding-top: 6px;
    }
}

.Dashboard__intro-text {
    padding: 20px;

    > p:nth-of-type(2) {
        padding-bottom: 1.25rem;
        border-bottom: 1px solid $border-color;
    }
}

.SimpleTrade__modal {
    // overflow: auto;
    .Modal__header {
        padding: 1rem;
    }
    .Modal__divider {
        margin: 0px;
    }
    .SimpleTrade__hide-zero {
        position: relative;
        top: -3px;
        cursor: pointer;
    }
    .SimpleTrade__withdraw-row {
        padding: 10px 0;
        > div > div.small-6 {
            display: inline-block;
            padding-bottom: 10px;
        }
    }
    .SimpleTrade__QR {
        > canvas {
            background: white;
            padding: 0.5rem;
            margin-bottom: 1.5rem;
        }
    }
    /* Buttons */
    .ActionButton_Close {
        padding: 10px 40px 10px 40px;
        border-radius: 5px 5px 5px 5px;
        color: rgb(242, 242, 242);
        background-color: rgb(245, 27, 27);
    }

    .SimpleTrade__copyIcon {
        padding: 0.5rem;
        margin-right: 15px;
        background-color: rgb(65, 116, 184);
    }

    .SimpleTrade__msg {
        text-align: right;
        font-size: 0.8rem;
    }
    .SimpleTrade__msg-error {
        > Icon {
            position: "relative";
            top: 0;
        }
    }
    .error-msg {
        font-style: normal;
        text-transform: uppercase;
        text-align: right;
        font-size: 90%;
        width: 100%;
        color: #ff3633;
        @include RobotoMedium;
        > span {
            vertical-align: top;
            top: 0px;
        }
        > svg {
            position: "relative";
            top: 0px;
        }
    }

    .SimpleTrade__deposit-info {
        margin-left: 0.2rem;
        overflow: "hidden";
        > .copyIcon {
            vertical-align: middle;
        }
        > .deposit-details {
            text-align: left;
            > div:nth-of-type(1) {
                font-size: 0.75rem;
                margin-bottom: 0.25rem;
            }
            > div:nth-of-type(2) {
                font-size: 1rem;
                color: lightblue;
            }
        }
    }
}

.balance-row.active {
    background-color: #3c3c3c;
}

.balance-row:hover {
    background-color: #3c3c3c;
}

.balance-row {
    margin-left: -10px;
    margin-right: -10px;
    padding: 10px 20px;
}

.balance-row:hover {
    cursor: pointer;
}

.SimpleTrade__help-text {
    font-style: italic;
    padding-top: 5px;
    font-size: 95%;
}

#simple_transfer_modal {
    max-width: 650px;
}

#simple_transfer_modal,
#simple_deposit_modal,
#simple_deposit_modal_ask,
#simple_withdraw_modal,
#simple_bridge_modal,
#simple_bridge_modal_ask,
#deposit_modal_new {
    padding: 0;
}

#simple_deposit_modal,
#simple_deposit_modal_ask,
#simple_withdraw_modal,
#simple_bridge_modal,
#simple_bridge_modal_ask,
#send_modal,
#send_modal_header,
#send_modal_portfolio,
#deposit_modal_new {
    overflow: auto !important;
    max-width: 500px;
}

#simple_deposit_modal label,
#simple_deposit_modal_ask label,
#simple_withdraw_modal label,
#simple_bridge_modal label,
#simple_bridge_modal_ask label {
    text-transform: none !important;
}

#simple_buy_modal,
#simple_sell_modal {
    overflow: auto !important;
    padding: 0;
}

div.button.create-account {
    font-size: 1.2rem;
}

div.input-wrapper {
    > div.input-right-symbol {
        font-size: 0.9rem;
        @include RobotoMedium;
        position: absolute;
        top: 12px;
        right: 8px;
    }
}

.SimpleDepositBridge__info-row {
    > div {
        padding-bottom: 5px;
        > div.float-right {
            clear: both;
            @include RobotoMedium;
        }
    }
}

// deposit Modal
#deposit_modal_new {
    .selectWrapper {
        border-radius: 5px;
    }
    .copyIcon {
        padding: 0.3rem;
        vertical-align: middle;
        background-color: rgb(65, 116, 184);
    }

    .DepositModal {
        .canvas {
            padding-top: 1rem;
            padding-left: 2rem;
            padding-right: 2rem;
            height: auto;
            min-height: 650px;

            .Modal__header {
                text-align: center;
                > img {
                    margin: 0px;
                    height: 80px;
                }
                > p {
                    font-size: 1.8rem;
                    @include RobotoMedium;
                    margin-bottom: 0px;
                }
            }

            .Modal__body {
                flex: 1;
                height: 90%;
                padding-top: 16px;
            }

            .container-row {
                padding-bottom: 1rem;
            }
            .floatRight {
                position: relative;
                float: right;
            }
            .error-msg {
                color: $alert-color;
                text-transform: uppercase;
            }

            .QR > canvas {
                background: white;
                padding: 0.5rem;
            }
            .ActionButton_Close {
                padding: 10px 40px 10px 40px;
                border-radius: 5px 5px 5px 5px;
                color: rgb(242, 242, 242);
                background-color: rgb(245, 27, 27);
            }

            .deposit-directly {
                text-align: center;
                > p:nth-of-type(1) {
                    font-size: 2rem;
                }
                > p:nth-of-type(2) {
                    font-size: 0.8rem;
                    width: 70%;
                    margin-right: auto;
                    margin-left: auto;
                }
            }
            .deposit-info {
                margin-left: 0.2rem;
                overflow: "hidden";

                > .maxDeposit {
                    @include RobotoMedium;
                    font-size: 0.9rem;
                }

                .deposit-details {
                    text-align: left;
                    > .copyIcon {
                        margin-right: 15px;
                    }
                    div:nth-of-type(2) > div:nth-of-type(1) {
                        font-size: 0.8rem;
                        margin-bottom: 0.25rem;
                    }
                    div:nth-of-type(2) > div:nth-of-type(2) {
                        font-size: 0.95rem;
                        color: #de2233;
                    }
                }
            }
        }
    }
    .close-button {
        display: none;
    }
}

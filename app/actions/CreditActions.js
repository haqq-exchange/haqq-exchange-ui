import alt from "alt-instance";
import ConfidentialWallet from "../stores/ConfidentialWallet";


class CreditActions {

    clearCredit() {
        return true;
    }
    setDataCredit(data) {
        return dispatch => {
            return new Promise(resolve => {
                dispatch(data);
                resolve(data);
            });
        };
    }
    removeParamsCredit(data) {
        return dispatch => {
            return new Promise(resolve => {
                dispatch(data);
                resolve(data);
            });
        };
    }

}

export default alt.createActions(CreditActions);

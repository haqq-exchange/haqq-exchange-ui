import alt from "alt-instance";

class WalletUnlockActions {
    /** If you get resolved then the wallet is or was just unlocked.  If you get
        rejected then the wallet is still locked.

        @return nothing .. Just test for resolve() or reject()
    */
    isUserActive = true;

    //передаю состояние юзера, активен - true, неактивен - false
    setUserAction(payload) {
        // console.log("начальное состояние", this.isUserActive)
        this.isUserActive = payload;
        return this.isUserActive;
    } 

    //получаю состояние юзера активен - true, неактивен - false
    getUserAction() {
        return this.isUserActive;
    }

    unlock() {
        return dispatch => {
            return new Promise((resolve, reject) => {
                // console.log('... AccountActions.transfer  dispatch');
                dispatch({resolve, reject});
            })
                .then(was_unlocked => {
                    //DEBUG
                    // console.log('... AccountActions.transfer WalletUnlockStore\tmodal unlock')
                    if (was_unlocked) WrappedWalletUnlockActions.change();
                })
                .catch(params => {
                    // console.log("AccountActions.transfer params", params)
                    throw params;
                });
        };
    }

    lock(number) {
        // console.log("ALT.DISPATCHER lock", number);
        return dispatch => {
            return new Promise(resolve => {
                dispatch({resolve});
            }).then(was_unlocked => {
                if (was_unlocked) WrappedWalletUnlockActions.change();
            });
        };
    }

    cancel() {
        return true;
    }

    change() {
        return true;
    }

    checkLock() {
        return true;
    }
    setTimeOut() {
        return true;
    }
}

var WrappedWalletUnlockActions = alt.createActions(WalletUnlockActions);
export default WrappedWalletUnlockActions;

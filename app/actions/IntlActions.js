import alt from "alt-instance";

// import localeCodes from "assets/locales";

var locales = {};
/*
if (__ELECTRON__) {
    localeCodes.forEach(locale => {
        locales[
            locale
        ] = require(`json-loader!assets/locales/locale-${locale}.json`);
    });
}
*/

class IntlActions {
    switchLocale(locale) {
        console.log("locale", locale);
        return dispatch => {
            console.log("locale", locale)
            fetch(`/locales/locale-${locale}.json`)
                .then(result=>result.json())
                .then(result => {
                    dispatch({
                        locale,
                        localeData: result
                    });
                })

                .catch(err => {
                    console.log("fetch locale error:", err);
                    return dispatch => {
                        dispatch({locale: "en"});
                    };
                });
        };
    }

    getLocale(locale) {
        return locale;
    }
}

export default alt.createActions(IntlActions);

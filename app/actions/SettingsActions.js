import alt from "alt-instance";

class SettingsActions {
    changeSetting(value) {
        
        return value;
    }

    changeViewSetting(value) {
        return value;
    }

    changeChartsObjectList_(value) {
        console.log("chartsMarketsList action", value);
        return value;
    }

    changeChartsObjectList(value) {
        //return value;
        return dispatch => {
            return new Promise(resolve => {
                setTimeout(function() { // Run after dispatcher has finished
                    dispatch({ value, resolve});
                }, 0);
            });
        };
    }

    setTopHour24Volume(data) {
        return dispatch => {
            return new Promise(resolve => {
                setTimeout(function() { // Run after dispatcher has finished
                    dispatch({ data, resolve});
                }, 0);
            });
        };
    }

    changeMarketDirection(value) {
        return value;
    }

    addStarMarket(quote, base) {
        return {quote, base};
    }

    removeStarMarket(quote, base) {
        return {quote, base};
    }

    clearStarredMarkets() {
        return true;
    }

    setUserMarket(quote, base, value) {
        return {quote, base, value};
    }

    addWS(ws) {
        return ws;
    }

    removeWS(index) {
        return index;
    }

    hideWS(url) {
        return url;
    }

    showWS(url) {
        return url;
    }

    hideAsset(id, status, accountName) {
        return {id, status, accountName};
    }

    favoriteAsset(id, status, accountName) {
        return {id, status, accountName};
    }

    hideMarket(id, status) {
        return {id, status};
    }

    clearSettings() {
        return dispatch => {
            return new Promise(resolve => {
                dispatch(resolve);
            });
        };
    }

    updateLatencies(latencies) {
        return latencies;
    }

    setExchangeLastExpiration(value) {
        return value;
    }
    deleteMarketTab(tab) {
        return tab;
    }
    addMarketTab(tab) {
        return tab;
    }

    changeAssetMarketTab(base, data) { return {data, base}; }

    changeDepositFavorite(data) { return data; }

    changeWithdrawFavorite(data) { return data; }

}

export default alt.createActions(SettingsActions);

import alt from "alt-instance";

class ModalActions {

    show(modalId, data = {}) {
        return dispatch => {
            return new Promise((resolve, reject) => {
                dispatch({resolve, reject, modalId, data });
            })
                .catch(params => {
                    throw params;
                });
        };
    }

    hide(modalId) {
        return dispatch => {
            console.log("ModalActions hide", dispatch);
            return new Promise((resolve, reject) => {
                dispatch({
                    resolve,
                    reject,
                    modalId
                });
            });
        };
    }

    toggle(){

    }
}

export default alt.createActions(ModalActions);

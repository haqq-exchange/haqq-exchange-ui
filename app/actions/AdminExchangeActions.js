import alt from "alt-instance";


class AdminExchangeActions {

    setReferralInfo(data) {
        return data;
    }

    setReferralUsers(users) {
        return users;
    }

    setExchangeAssets(assets) {
        return assets;
    }
    setSortTableCurrency(sort) {
        return sort;
    }
    setSortTable(sort) {
        return sort;
    }
    setReferralUsersStats(sort) {
        return sort;
    }
}

export default alt.createActions(AdminExchangeActions);

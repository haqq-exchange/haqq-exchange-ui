import React from "react";
import Bowser from "bowser";
import {ChainStore} from "deexjs";
import {Apis} from "deexjs-ws";
import AccountStore from "stores/AccountStore";
import NotificationStore from "stores/NotificationStore";
import {withRouter} from "react-router-dom";
import SyncError from "./components/SyncError";
import LoadingIndicator from "./components/LoadingIndicator";
import Header from "components/Layout/Header";
import Footer from "./components/Layout/Footer";
import ReactTooltip from "react-tooltip";
/* modals */
import BrowserNotifications from "./components/BrowserNotifications/BrowserNotificationsContainer";
import NotificationSystem from "react-notification-system";
import TransactionConfirm from "./components/Blockchain/TransactionConfirm";
import WalletUnlockModal from "./components/Public/WalletUnlockModal";
import WalletShowInfoSessionTimout from "./components/Modal/WalletShowInfoSessionTimout";
import WalletShowInfoSessionStop from "./components/Modal/WalletShowInfoSessionStop";
import BrowserSupportModal from "./components/Modal/BrowserSupportModal";
import ChangeDomenModal from "./components/Modal/ChangeDomentModal";
import CryptoplatIoBuy from "./components/Modal/CryptoplatIoBuy";
import TransferAssetModal from "Components/Modal/TransferAssetModal";
import SelectedDepositWithdraw from "Components/Modal/SelectedDepositWithdraw/SelectedDepositWithdraw";
/* modals */
import Deprecate from "./Deprecate";
//import AdBlockDetect from "Components/Layout/AdBlockDetect";
import Incognito from "./components/Layout/Incognito";
import {isIncognito} from "feature_detect";
import {updateGatewayBackers} from "common/gatewayUtils";
import WalletDb from "stores/WalletDb";
import configBrowser from "config/browser";
import RoutesPage from "./RoutesPage";
import ls from "App/lib/common/localStorage";
import configConst from "App/config/const";
import IdleTimer from 'react-idle-timer';
import WalletUnlockActions from "actions/WalletUnlockActions";
import "assets/stylesheets/app.scss";
import { values } from "mobx";

let ss = new ls(configConst.STORAGE_KEY);

class App extends React.Component {
    constructor() {
        super();
        this.timeout = null;
        let syncFail =
            ChainStore.subError &&
            ChainStore.subError.message ===
            "ChainStore sync error, please check your system clock"
                ? true
                : false;
        this.state = {
            loading: false,
            synced: this._syncStatus(),
            syncFail,
            incognito: false,
            incognitoWarningDismissed: false,
            height: window && window.innerHeight, 
        };

        this._rebuildTooltips = this._rebuildTooltips.bind(this);
        this._chainStoreSub = this._chainStoreSub.bind(this);
        this._syncStatus = this._syncStatus.bind(this);
        this._getWindowHeight = this._getWindowHeight.bind(this);
        //this.appUpdate = this.appUpdate.bind(this);

        // функции для таймера 'react-idle-timer'
        this.idleTimer = null
        this.handleOnAction = this.handleOnAction.bind(this)
        this.handleOnActive = this.handleOnActive.bind(this)
        this.handleOnIdle = this.handleOnIdle.bind(this)
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this._getWindowHeight);
        NotificationStore.unlisten(this._onNotificationChange);
        ChainStore.unsubscribe(this._chainStoreSub);
        clearInterval(this.syncCheckInterval);
    }

    /**
     * Returns the current blocktime, or exception if not yet available
     * @returns {Date}
     */

    getBlockTime() {
        let dynGlobalObject = ChainStore.getObject("2.1.0");
        if (dynGlobalObject) {
            let block_time = dynGlobalObject.get("time");
            if (!/Z$/.test(block_time)) {
                block_time += "Z";
            }
            return new Date(block_time);
        } else {
            throw new Error("Blocktime not available right now");
        }
    }

    /**
     * Returns the delta between the current time and the block time in seconds, or -1 if block time not available yet
     *
     * Note: Could be integrating properly with BlockchainStore to send out updates, but not necessary atp
     */

    getBlockTimeDelta() {
        try {
            let bt =
                (this.getBlockTime().getTime() +
                    ChainStore.getEstimatedChainTimeOffset()) /
                1000;
            let now = new Date().getTime() / 1000;
            return Math.abs(now - bt);
        } catch (err) {
            return -1;
        }
    }

    _syncStatus(setState = false) {
        let synced = this.getBlockTimeDelta() < 5;
        if (setState && synced !== this.state.synced) {
            this.setState({synced});
        }
        return synced;
    }

    _setListeners() {
        try {
            window.addEventListener("resize", this._getWindowHeight, {
                capture: false,
                passive: true
            });
            NotificationStore.listen(this._onNotificationChange.bind(this));
            ChainStore.subscribe(this._chainStoreSub);
            AccountStore.tryToSetCurrentAccount();
        } catch (e) {
            
        }
    }

    componentDidMount() {
        this._setListeners();
        this.syncCheckInterval = setInterval(
            this._syncStatus.bind(this, true),
            5000
        );
        const isShowChangeDomain = ss.get("change_domain_modal", {show: true});
        const browser = Bowser.getParser(window.navigator.userAgent);
        const isValidBrowser = browser.satisfies(configBrowser);

        if (
            !(
                window.electron || isValidBrowser
            )
        ) {
            this.browser_modal.show();
        }
        /* показ данных о смене домена */
        // if(isShowChangeDomain.show){
        //     this.change_domain_modal.show();
        // }

        this.props.history.listen(this._rebuildTooltips);

        this._rebuildTooltips();

        isIncognito(
            function(incognito) {
                this.setState({incognito});
            }.bind(this)
        );
        if(Apis.instance().chain_id) {
            updateGatewayBackers(Apis.instance().chain_id.substr(0, 8));
        }
    }

    componentDidUpdate(prevProps) {
        /*if (this.props.location !== prevProps.location) {
            this.onRouteChanged();
        }*/
    }

    _onIgnoreIncognitoWarning() {
        this.setState({incognitoWarningDismissed: true});
    }

    _rebuildTooltips() {
        if (this.rebuildTimeout) return;
        ReactTooltip.hide();

        this.rebuildTimeout = setTimeout(() => {
            if (this.tooltip) {
                this.tooltip.globalRebuild();
            }
            this.rebuildTimeout = null;
        }, 1500);
    }

    _chainStoreSub() {
        let synced = this._syncStatus();
        if (synced !== this.state.synced) {
            this.setState({synced});
        }
        if (
            ChainStore.subscribed !== this.state.synced ||
            ChainStore.subError
        ) {
            let syncFail =
                ChainStore.subError &&
                ChainStore.subError.message ===
                "ChainStore sync error, please check your system clock"
                    ? true
                    : false;
            this.setState({
                syncFail
            });
        }
    }

    /** Usage: NotificationActions.[success,error,warning,info] */
    _onNotificationChange() {
        let notification = NotificationStore.getState().notification;
        if (notification.autoDismiss === void 0) {
            notification.autoDismiss = 10;
        }
        if (this.notificationSystem)
            this.notificationSystem.addNotification(notification);
    }

    _getWindowHeight() {
        this.setState({height: window && window.innerHeight});
    }

    // /** Non-static, used by passing notificationSystem via react Component refs */
    // _addNotification(params) {
    //     
    //     this.refs.notificationSystem.addNotification(params);
    // }

    // код для таймаута (активен пользователь или нет)
    handleOnAction (event) {
        // console.log('user did something', event);
        console.log("Пользователь активен?", WalletUnlockActions.getUserAction())
        WalletUnlockActions.setUserAction(true);
    };
    
    handleOnActive (event) {
        // console.log('user is active', event)
        console.log("Пользователь активен?", WalletUnlockActions.getUserAction())
        // console.log('time remaining', this.idleTimer.getRemainingTime())
    };
    
    handleOnIdle (event) {
        // console.log('user is idle', event);
        let lastActive = (new Date()) - this.idleTimer.getLastActiveTime();
        // console.log('last active', lastActive)
        // console.log("Пользователь активен?", WalletUnlockActions.getUserAction())

        if(lastActive > 900000 && !WalletDb.isLocked()) { // если через 15 мин нет никаких действий, то происходит блок кода
            WalletUnlockActions.setUserAction(false);
        }
    };

    render() {
        let {incognito, incognitoWarningDismissed} = this.state;
        let {walletMode, theme, myHiddenAccounts, currentAccount , myActiveAccounts, passwordAccount, ...others} = this.props;
        let content = null;

        let accountCount =
            myActiveAccounts.size +
            myHiddenAccounts.size +
            (currentAccount || passwordAccount ? 1 : 0);

        if (this.state.syncFail) {
            content = <SyncError/>;
        } else if (this.state.loading) {
            content = (
                <div className="grid-frame vertical">
                    <LoadingIndicator
                        loadingText={"Connecting to APIs and starting app"}
                    />
                </div>
            );
        } else if (__DEPRECATED__) {
            content = <Deprecate {...this.props} />;
        } else {
            content = (
                <div className="grid-frame-app">
                    {/*<AdBlockDetect />*/}
                    <Header height={this.state.height} {...others} />
                    <div id="mainContainer" className={__SCROOGE_CHAIN__ ? "" : "grid-block-main"}>
                        <div className="grid-block-app vertical">
                            {/* All routes page */}
                            <RoutesPage {...this.props} />
                        </div>
                    </div>
                    {__SCROOGE_CHAIN__ ? null :
                    <Footer
                        synced={this.state.synced}
                        history={this.props.history}
                    />}
                    <ReactTooltip
                        ref={(r) => this.tooltip = r}
                        place="top"
                        type={theme === "lightTheme" ? "dark" : "light"}
                        effect="solid"
                    />
                </div>
            );
        }

        return (
            <div className={theme}>
                {/* код для таймаута*/}
                <IdleTimer
                    ref={ref => { this.idleTimer = ref }}
                    timeout={900000} // 15 min
                    onActive={this.handleOnActive}
                    onIdle={this.handleOnIdle}
                    onAction={this.handleOnAction}
                    debounce={250}
                />
                {walletMode && incognito && !incognitoWarningDismissed ? (
                    <Incognito
                        onClickIgnore={this._onIgnoreIncognitoWarning.bind(
                            this
                        )}
                    />
                ) : null}
                <div id="content-wrapper" className={"content-wrapper"}>
                    {content}
                    <NotificationSystem
                        ref={(c) => {
                            this.notificationSystem = c;
                        }}
                        allowHTML={true}
                        style={{
                            overrideStyle: {
                                NotificationItem: {
                                    height: "auto",
                                }
                            },
                            Containers: {
                                DefaultStyle: {
                                    width: "425px"
                                }
                            }
                        }}
                    />

                    <TransactionConfirm/>

                    {passwordAccount || currentAccount ? <BrowserNotifications/> : null}
                    {passwordAccount || currentAccount ? <SelectedDepositWithdraw account={passwordAccount || currentAccount} />: null}
                    {WalletDb.isLocked() && (walletMode || accountCount) ? <WalletUnlockModal/> : null}
                    {/* {WalletDb.isLocked() ? <WalletUnlockModal/> : null} */}
                    <BrowserSupportModal ref={(c) => {
                        this.browser_modal = c;
                    }}/>
                   
                   {/* окошко о смене домена */}
                    {/* <ChangeDomenModal ref={(c) => {
                        this.change_domain_modal = c;
                    }}/> */}

                    <WalletShowInfoSessionTimout />
                    <WalletShowInfoSessionStop/>
                    <TransferAssetModal />
                    <CryptoplatIoBuy />
                </div>
            </div>
        );
    }
}

export default withRouter(App);

import Alt from "alt";
var alt = new Alt();

if (__DEV__) {
    /* Delete debug on transfer */
    // Alt.debug("alt", alt);
    // DEBUG log all action events
    // alt.dispatcher.register(console.log.bind(console, "ALT.DISPATCHER"));
}

export default alt;

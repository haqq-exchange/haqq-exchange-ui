const aliasAsset = {
    "A2ASTEX":"A2A",
    "ARTEX":"ART",
    "BTCOLD":"BTCOLD",
    "CRYPTOBOSS":"CRYPTOBOSS",
    "CRYPTONOMICA":"CRYPTONOMICA",
    "DEEX.BCH":"BCH",
    "DEEX.BTC":"BTC",
    "DEEX.DASH":"DASH",
    "DEEX.DOGE":"DOGE",
    "DEEX.EOS":"EOS",
    "DEEX.ETH":"ETH",
    "DEEX.EURO":"EURS",
    "DEEX.KRM":"KRM",
    "DEEX.LTC":"LTC",
    "DEEX.MONERO":"XMR",
    "DEEX.NEM":"NEM",
    "DEEX.PRIZM":"PZM",
    "DEEX.SPARTA":"SPA",
    "DEEX.STEEM":"STEEM",
    "DEEX.ZEC":"ZEC",
    "DEEX.DEBITCOIN": "DBTC",
    "DEEX.MEC":"MEC",
    "DEEXBITCOEN":"BITCOEN",
    "DESCROW":"DES",
    "EMERTOKEN":"EMC",
    "EXENIUM":"XNT",
    "FIRELOTTO":"FLOT",
    "GRIN":"GRIN",
    "ICEROCK":"ROCK2",
    "LORDMANCER":"LC (Lord Coin)",
    "MAINCOIN":"MNC",
    "QIWIRUBLE":"RUB",
    "TRXTRON":"TRX",
    "XAVIERA":"XTS",
    "ZEROZED":"X0Z",
    "CRYPVISER":"CVN",
    "CBEXX": "CBEX",
    "USDQSTABLE": "USDQ",
    "KRWQSTABLE": "KRWQ",
    "USDTETHER":"USDT",
    "DISCODERY": "DISCOIN",
    "TRON888": "888",
    "AIVIA": "AIV",
    "ETH1D0": "ATTRACT.ETH"
};


export function getAssetsAlias (assets) {
    if( assets ) {
        return assets
            .map(asset => getAlias(asset));
    }
}

export function getAlias (name) {
    if( aliasAsset[name] ) {
        return aliasAsset[name];
    }
    return name;
}

export default {
    windows: {
        "internet explorer": ">10",
    },
    macos: {
        safari: ">11"
    },

    // per platform (mobile, desktop or tablet)
    mobile: {
        "safari": ">=9",
        "android browser": ">18.10"
    },

    // or in general
    chrome: ">=70",
    firefox: ">65",
    opera: ">=58",
};
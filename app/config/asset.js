export default {
    markets_c6930e40: {
        id: "1.3.165",
        symbol: __GBLTN_CHAIN__ ? "GBLTEST" : (__GBL_CHAIN__ ? "GBL" : (__SCROOGE_CHAIN__ ? "SCROOGE" : "HAQQ")),
        precision: 5,
    },
    markets_4018d784: {
        id: "1.3.165",
        symbol: __GBLTN_CHAIN__ ? "GBLTEST" : (__GBL_CHAIN__ ? "GBL" : (__SCROOGE_CHAIN__ ? "SCROOGE" : "HAQQ")),
        precision: 4,
    },
    markets_8d79be06 : {
        id: "1.3.165",
        symbol: __GBLTN_CHAIN__ ? "GBLTEST" : (__GBL_CHAIN__ ? "GBL" : (__SCROOGE_CHAIN__ ? "SCROOGE" : "HAQQ")),
        precision: 4,
    },
    markets_39f5e2ed: {
        id: "1.3.165",
        symbol: "TEST",
        precision: 4,
    },
    markets_8835e222: {
        id: "1.3.165",
        symbol: __GBLTN_CHAIN__ ? "GBLTEST" : (__GBL_CHAIN__ ? "GBL" : (__SCROOGE_CHAIN__ ? "SCROOGE" : "HAQQ")),
        precision: 4,
    },
    markets_0e01a817: {
        id: "1.3.165",
        symbol: __GBLTN_CHAIN__ ? "GBLTEST" : (__GBL_CHAIN__ ? "GBL" : (__SCROOGE_CHAIN__ ? "SCROOGE" : "HAQQ")),
        precision: 4,
    },
};

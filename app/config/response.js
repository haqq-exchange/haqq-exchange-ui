export default {
    mobile: {
        maxDeviceWidth: 767
    },
    notMobile: {
        minDeviceWidth: 768,
    },
    tablet: {
        minDeviceWidth: 768,
        maxDeviceWidth: 1024
    },
    desktop: {
        minDeviceWidth: 1025
    },
};
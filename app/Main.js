import "babel-polyfill";
require("./assets/loader");
import SettingsStore from "./stores/SettingsStore";



if (!window.Intl) {
    require.ensure(["intl"], require => {
        window.Intl = require("intl");
        let locale = SettingsStore.getState().settings.get("locale") || "en";

        Intl.__addLocaleData(require("assets/intl-data/" + locale + ".json"));
        require("./index.js");
    });
} else {
    require("./index.js");
}

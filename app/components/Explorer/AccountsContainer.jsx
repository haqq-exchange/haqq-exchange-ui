import React from "react";
import AccountStore from "stores/AccountStore";
import SettingsStore from "stores/SettingsStore";
import AltContainer from "alt-container";
import Accounts from "./Accounts";

class AccountsContainer extends React.Component {
    render() {
        return (
            <AltContainer
                component={Accounts}
                stores={[AccountStore, SettingsStore]}
                inject={{
                    searchAccounts: () => {
                        return AccountStore.getState().searchAccounts;
                    },
                    searchTerm: () => {
                        return AccountStore.getState().searchTerm;
                    },
                    countAccounts: () => {
                        return AccountStore.getState().countAccounts;
                    },
                    locale: () => {
                        return SettingsStore.getState().settings.get("locale");
                    },
                }}
            />
        );
    }
}

export default AccountsContainer;

import React from "react";
//import {Tabs, Tab} from "../Utility/Tabs";
import Translate from "react-translate-component";
import Witnesses from "./Witnesses";
import CommitteeMembers from "./CommitteeMembers";
import FeesContainer from "../Blockchain/FeesContainer";
import BlocksContainer from "./BlocksContainer";
import AssetsContainer from "./AssetsContainer";
import AccountsContainer from "./AccountsContainer";
import MarketsContainer from "./MarketsContainer";
// import MarketsContainer from "../Exchange/MarketsContainer";

import { Tabs } from "antd";
import "./explorer.scss";

const { TabPane } = Tabs;

class Explorer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tabs: [
                {
                    name: "blocks",
                    link: "/explorer/blocks",
                    translate: "explorer.blocks.title",
                    content: BlocksContainer
                },
                {
                    name: "assets",
                    link: "/explorer/assets",
                    translate: "explorer.assets.title",
                    content: AssetsContainer
                },
                {
                    name: "accounts",
                    link: "/explorer/accounts",
                    translate: "explorer.accounts.title",
                    content: AccountsContainer
                },
                {
                    name: "witnesses",
                    link: "/explorer/witnesses",
                    translate: "explorer.witnesses.title",
                    content: Witnesses
                },
                {
                    name: "committee_members",
                    link: "/explorer/committee-members",
                    translate: "explorer.committee_members.title",
                    content: CommitteeMembers
                },
                /*{
                    name: "markets",
                    link: "/explorer/markets",
                    translate: "markets.title",
                    content: MarketsContainer
                },*/
                {
                    name: "fees",
                    link: "/explorer/fees",
                    translate: "fees.title",
                    content: FeesContainer
                }
            ]
        };
    }


    onChangeTabs = active => {
        const {history} = this.props;
        history.push(`/explorer/${active}`);
    };


    render() {
        let {tab} = this.props.match.params;

        let tabs = [];

        for (var i = 0; i < this.state.tabs.length; i++) {
            let currentTab = this.state.tabs[i];

            let TabContent = currentTab.content;

            tabs.push(
                <TabPane key={currentTab.name} tab={<Translate content={currentTab.translate}/>} history={this.props.history}>
                    <TabContent />
                </TabPane>
            );
        }

        return (
            <div className={"account-tabs"} id={"account-tabs"}>
                <Tabs
                    type="card"
                    onChange={this.onChangeTabs}
                    activeKey={tab || "blocks"}
                    destroyInactiveTabPane={true}
                    segmented={false}
                    setting="explorer-tabs"
                    tabsStyle={{width: "100%", padding: "10px 20px"}}
                >
                    {tabs}
                </Tabs>
            </div>
        );
    }
}

export default Explorer;

import React from "react";
import PropTypes from "prop-types";
import AssetActions from "actions/AssetActions";
import SettingsActions from "actions/SettingsActions";
import {Link} from "react-router-dom";
import Immutable from "immutable";
import clnm from "classnames";
import Translate from "react-translate-component";
import LinkToAccountById from "../Utility/LinkToAccountById";
import assetUtils from "common/asset_utils";
import FormattedAsset from "../Utility/FormattedAsset";
import AssetName from "../Utility/AssetName";
import {ChainStore} from "deexjs";
import {Apis} from "deexjs-ws";
import utils from "common/utils";
import ls from "common/localStorage";
import Icon from "../Icon/Icon";
import {List, Radio, Select, Spin, Table} from "antd";
import SearchInput from "../Utility/SearchInput";
import configConst from "config/const";
import AssetStore from "stores/AssetStore";
let accountStorage = new ls(configConst.STORAGE_KEY);

class Assets extends React.Component {
    constructor(props) {
        super();

        let chainID = Apis.instance().chain_id;
        if (chainID) chainID = chainID.substr(0, 8);
        else chainID = "4018d784";

        this.state = {
            chainID,
            foundLast: false,
            lastAsset: "",
            isLoading: false,
            toggleSearchGroup: false,
            totalAssets:
                typeof accountStorage.get(`totalAssets_${chainID}`) != "object"
                    ? accountStorage.get(`totalAssets_${chainID}`)
                    : chainID && chainID === "c6930e40"
                        ? 0
                        : 50, // mainnet has 3000+ assets, other chains may not have that many
            assetsFetched: 0,
            activeFilter: "market",
            filterSearch: props.filterSearch || "",
            rowsOnPage: "25"
        };

        this._toggleFilter = this._toggleFilter.bind(this);
        this.handleRowsChange = this.handleRowsChange.bind(this);
        this.handleFilterChange = this.handleFilterChange.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (
            !Immutable.is(nextProps.assets, this.props.assets) ||
            !utils.are_equal_shallow(nextState, this.state)
        );
    }

    componentDidMount() {
        console.log("componentDidMount Assets");
        this._checkAssets(this.props.assets, true);
    }

    componentWillUnmount() {
        console.log("componentWillUnmount Assets");
    }

    handleFilterChange(e) {
        this.setState({
            filterSearch: (e.target.value || "").toUpperCase()
        });
    }

    handleRowsChange(rows) {
        this.setState({
            rowsOnPage: rows
        });
    }

    _checkAssets(assets, force) {
        this.setState({isLoading: true});
        let lastAsset = assets
            .sort((a, b) => {
                if (a.symbol > b.symbol) {
                    return 1;
                } else if (a.symbol < b.symbol) {
                    return -1;
                } else {
                    return 0;
                }
            })
            .last();

        if (assets.size === 0 || force) {
            AssetActions.getAssetList.defer("A", 100);
            this.setState({assetsFetched: 100});
        } else if (assets.size >= this.state.assetsFetched) {
            AssetActions.getAssetList.defer(lastAsset.symbol, 100);
            this.setState({assetsFetched: this.state.assetsFetched + 99});
        }

        if (assets.size > this.state.totalAssets) {
            accountStorage.set(
                `totalAssets_${this.state.chainID}`,
                assets.size
            );
        }

        console.log("assetsFetched", this.state);

        if (this.state.assetsFetched >= this.state.totalAssets - 100) {
            this.setState({isLoading: false});
        }
    }
    componentDidUpdate(prevProps) {
        if (prevProps.assets !== this.props.assets) {
            this._checkAssets(prevProps.assets);
        }
    }

    linkToAccount(name_or_id) {
        if (!name_or_id) {
            return <span>-</span>;
        }

        return <LinkToAccountById account={name_or_id} />;
    }

    _toggleFilter(e) {
        this.setState({
            activeFilter: e.target.value
        });
    }

    _onFilter(type, e) {
        this.setState({[type]: e.target.value.toUpperCase()});
        SettingsActions.changeViewSetting({
            [type]: e.target.value.toUpperCase()
        });
    }

    render() {
        let {assets, current_supply} = this.props;
        let {activeFilter, isLoading, toggleSearchGroup} = this.state;

        console.log("assets", assets)

        let coreAsset = ChainStore.getAsset("1.3.0");

        let pm;

        let dataSource = [];
        let columns = [];
        console.log("this.props", this.props, isLoading);

        // Default sorting of the ant table is defined through defaultSortOrder prop

        if (activeFilter === "user") {
            columns = [
                {
                    key: "assetId",
                    dataIndex: "assetId",
                    title: "Asset Id",
                },
                {
                    key: "symbol",
                    title: <Translate content="explorer.assets.symbol"/>,
                    dataIndex: "symbol",
                    defaultSortOrder: "ascend",
                    sorter: (a, b) => {
                        return a.symbol > b.symbol
                            ? 1
                            : a.symbol < b.symbol
                                ? -1
                                : 0;
                    },
                    render: item => {
                        return (
                            <Link to={`/account-asset/${item}`}>
                                <AssetName name={item} />
                            </Link>
                        );
                    }
                },
                {
                    key: "issuer",
                    title: <Translate content="explorer.assets.issuer"/>,
                    dataIndex: "issuer",
                    sorter: (a, b) => {
                        let issuerA = ChainStore.getAccount(a.issuer, false);
                        let issuerB = ChainStore.getAccount(b.issuer, false);
                        if (issuerA) issuerA = issuerA.get("name");
                        if (issuerB) issuerB = issuerB.get("name");
                        if (issuerA > issuerB) return 1;
                        if (issuerA < issuerB) return -1;
                        return 0;
                    },
                    render: item => {
                        return this.linkToAccount(item);
                    }
                },
                {
                    key: "currentSupply",
                    title: <Translate content="markets.supply" />,
                    dataIndex: "currentSupply",
                    sorter: (a, b) => {
                        a.currentSupply = parseFloat(a.currentSupply);
                        b.currentSupply = parseFloat(b.currentSupply);
                        return a.currentSupply > b.currentSupply
                            ? 1
                            : a.currentSupply < b.currentSupply
                                ? -1
                                : 0;
                    },
                    render: (item, record) => {
                        let asset_current_supply = record.assetId === "1.3.0" ?
                            current_supply :
                            record.currentSupply;
                        return (
                            <FormattedAsset
                                amount={asset_current_supply}
                                asset={record.assetId}
                                hide_asset={true}
                            />
                        );
                    }
                },
                {
                    key: "marketId",
                    title: "",
                    dataIndex: "marketId",
                    render: item => {
                        return (
                            <Link to={`/market/${item}`}>
                                <Icon name={"trade"} />{" "}
                                <Translate content="header.exchange" />
                            </Link>
                        );
                    }
                }
            ];

            assets
                .filter(a => {
                    return (
                        !a.market_asset &&
                        a.symbol.indexOf(this.state.filterSearch) !== -1
                    );
                })
                .map(asset => {
                    console.log("asset", asset)
                    let description = assetUtils.parseDescription(
                        asset.options.description
                    );
                    console.log("description", description)

                    let marketID =
                        asset.symbol +
                        "_" +
                        (description.market
                            ? description.market
                            : coreAsset
                                ? coreAsset.get("symbol")
                                : "BTS");

                    dataSource.push({
                        symbol: asset.symbol,
                        issuer: asset.issuer,
                        currentSupply: asset.dynamic.current_supply,
                        assetId: asset.id,
                        marketId: marketID
                    });
                });
        }

        if (activeFilter === "market") {
            columns = [
                {
                    key: "assetId",
                    dataIndex: "assetId",
                    title: "Asset Id",
                },
                {
                    key: "symbol",
                    title: <Translate content="explorer.assets.symbol"/>,
                    dataIndex: "symbol",
                    defaultSortOrder: "ascend",
                    sorter: (a, b) => {
                        return a.symbol > b.symbol
                            ? 1
                            : a.symbol < b.symbol
                                ? -1
                                : 0;
                    },
                    render: item => {
                        return (
                            <Link to={`/account-asset/${item}`}>
                                <AssetName name={item} />
                            </Link>
                        );
                    }
                },
                {
                    key: "issuer",
                    title: <Translate content="explorer.assets.issuer"/>,
                    dataIndex: "issuer",
                    sorter: (a, b) => {
                        let issuerA = ChainStore.getAccount(a.issuer, false);
                        let issuerB = ChainStore.getAccount(b.issuer, false);
                        if (issuerA) issuerA = issuerA.get("name");
                        if (issuerB) issuerB = issuerB.get("name");
                        if (issuerA > issuerB) return 1;
                        if (issuerA < issuerB) return -1;
                        return 0;
                    },
                    render: item => {
                        return this.linkToAccount(item);
                    }
                },
                {
                    key: "currentSupply",
                    title: <Translate content="markets.supply"/>,
                    dataIndex: "currentSupply",
                    sorter: (a, b) => {
                        a.currentSupply = parseFloat(a.currentSupply);
                        b.currentSupply = parseFloat(b.currentSupply);
                        return a.currentSupply > b.currentSupply
                            ? 1
                            : a.currentSupply < b.currentSupply
                                ? -1
                                : 0;
                    },
                    render: (item, record) => {
                        let asset_current_supply = record.assetId === "1.3.0" ?
                            current_supply :
                            record.current_supply;
                        return (
                            <FormattedAsset
                                amount={asset_current_supply}
                                asset={record.assetId}
                                hide_asset={true}
                            />
                        );
                    }
                },
                {
                    key: "marketId",
                    title: "",
                    dataIndex: "marketId",
                    render: item => {
                        return (
                            <Link to={`/market/${item}`}>
                                <Icon name={"trade"} />{" "}
                                <Translate content="header.exchange" />
                            </Link>
                        );
                    }
                }
            ];

            assets
                .filter(a => {
                    return (
                        a.bitasset_data &&
                        !a.bitasset_data.is_prediction_market &&
                        a.symbol.indexOf(this.state.filterSearch) !== -1
                    );
                })
                .map(asset => {
                    let description = assetUtils.parseDescription(
                        asset.options.description
                    );

                    let marketID =
                        asset.symbol +
                        "_" +
                        (description.market
                            ? description.market
                            : coreAsset
                                ? coreAsset.get("symbol")
                                : "BTS");

                    dataSource.push({
                        symbol: asset.symbol,
                        issuer: asset.issuer,
                        currentSupply: asset.dynamic.current_supply,
                        assetId: asset.id,
                        marketId: marketID
                    });
                });
        }

        if (activeFilter === "prediction") {
            pm = assets
                .filter(a => {
                    let description = assetUtils.parseDescription(
                        a.options.description
                    );

                    return (
                        a.bitasset_data &&
                        a.bitasset_data.is_prediction_market &&
                        (a.symbol
                            .toLowerCase()
                            .indexOf(this.state.filterSearch.toLowerCase()) !==
                            -1 ||
                            description.main
                                .toLowerCase()
                                .indexOf(
                                    this.state.filterSearch.toLowerCase()
                                ) !== -1)
                    );
                })
                .sort((a, b) => {
                    if (a.symbol < b.symbol) {
                        return -1;
                    } else if (a.symbol > b.symbol) {
                        return 1;
                    } else {
                        return 0;
                    }
                })
                .map(asset => {
                    let description = assetUtils.parseDescription(
                        asset.options.description
                    );
                    let marketID =
                        asset.symbol +
                        "_" +
                        (description.market
                            ? description.market
                            : coreAsset
                                ? coreAsset.get("symbol")
                                : "BTS");

                    return {
                        asset,
                        description,
                        marketID
                    };
                })
                .toArray();
        }

        return (
            <Spin spinning={this.state.isLoading}>
                <div className="explorer-block">
                    <div className="generic-bordered-box">
                        <div className={"assets-search"}>
                            <SearchInput
                                value={this.state.filterSearch}
                                onChange={this.handleFilterChange}
                            />
                            <Icon name={"filter-setting"} onClick={()=>this.setState({
                                toggleSearchGroup: !toggleSearchGroup
                            })} />

                            <div className={clnm("assets-search-group", {
                                active: toggleSearchGroup
                            })} style={{marginLeft: "20px"}}>
                                <Radio.Group
                                    value={this.state.activeFilter}
                                    onChange={this._toggleFilter}

                                >
                                    <Radio value={"market"}>
                                        <Translate content="explorer.assets.market" />
                                    </Radio>
                                    <Radio value={"user"}>
                                        <Translate content="explorer.assets.user" />
                                    </Radio>
                                    <Radio value={"prediction"}>
                                        <Translate content="explorer.assets.prediction" />
                                    </Radio>
                                </Radio.Group>

                                {/*style={{width: "150px", marginLeft: "24px"}}*/}
                                <Select
                                    style={{width: "150px"}}
                                    value={this.state.rowsOnPage}
                                    onChange={this.handleRowsChange}
                                    getPopupContainer={()=>document.getElementById("account-tabs")}
                                >
                                    {[10,25,50,100,200].map(item=>(
                                        <Select.Option key={item}>
                                            {item} rows
                                        </Select.Option>
                                    ))}
                                </Select>
                            </div>
                        </div>

                        {activeFilter === "prediction" ? (
                            <List
                                style={{paddingBottom: 20}}
                                size="large"
                                itemLayout="horizontal"
                                dataSource={pm}
                                renderItem={item => (
                                    <List.Item
                                        key={item.asset.id.split(".")[2]}
                                        actions={[
                                            <Link
                                                key={"link-"+item.asset.id.split(".")[2]}
                                                className="button outline"
                                                to={`/market/${item.marketID}`}>
                                                <Translate content="header.exchange" />
                                            </Link>
                                        ]}
                                    >
                                        <List.Item.Meta
                                            title={
                                                <div>
                                                    <span
                                                        style={{
                                                            paddingTop: 10,
                                                            fontWeight:
                                                                "bold"
                                                        }}
                                                    >
                                                        <Link to={`/account-asset/${item.asset.symbol}`}>
                                                            <AssetName name={item.asset.symbol}/>
                                                        </Link>
                                                    </span>
                                                    {item.description.condition ?
                                                        (<span>{" "}({item.description.condition})</span>) : null}
                                                </div>
                                            }
                                            description={
                                                <span>
                                                    {item.description ? (
                                                        <div
                                                            style={{
                                                                padding:
                                                                    "10px 20px 5px 0",
                                                                lineHeight:
                                                                            "18px"
                                                            }}
                                                        >
                                                            {
                                                                item
                                                                    .description
                                                                    .main
                                                            }
                                                        </div>
                                                    ) : null}
                                                    <span
                                                        style={{
                                                            padding:
                                                                "0 20px 5px 0",
                                                            lineHeight:
                                                                "18px"
                                                        }}
                                                    >
                                                        <LinkToAccountById
                                                            account={
                                                                item.asset
                                                                    .issuer
                                                            }
                                                        />
                                                        <span>
                                                            {" "}
                                                            -{" "}
                                                            <FormattedAsset
                                                                amount={
                                                                    item
                                                                        .asset
                                                                        .dynamic
                                                                        .current_supply
                                                                }
                                                                asset={
                                                                    item
                                                                        .asset
                                                                        .id
                                                                }
                                                            />
                                                        </span>
                                                        {item.description
                                                            .expiry ? (<span>
                                                                {" "}-{" "}{item
                                                            .description
                                                            .expiry}
                                                            </span>) : null}
                                                    </span>
                                                </span>
                                            }
                                        />
                                    </List.Item>
                                )}
                                pagination={{
                                    position: ["bottom"],
                                    pageSize: 6
                                }}
                            />
                        ) : (
                            <Table
                                style={{width: "100%", marginTop: "16px"}}
                                rowKey="symbol"
                                columns={columns}
                                scroll={{x: 800}}
                                pagination={{
                                    hideOnSinglePage: true,
                                    position: "bottom",
                                    pageSize: Number(this.state.rowsOnPage)
                                }}
                                dataSource={dataSource}
                            />
                        )}
                    </div>
                </div>
            </Spin>
        );
    }
}

Assets.defaultProps = {
    assets: {}
};

Assets.propTypes = {
    assets: PropTypes.object.isRequired
};

export default Assets;

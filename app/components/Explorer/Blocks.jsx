import React from "react";
import {Link} from "react-router-dom";
import { Card, Col, Row } from "antd";
import BlockchainActions from "actions/BlockchainActions";
import Translate from "react-translate-component";
import {FormattedDate} from "react-intl";
import Operation from "../Blockchain/Operation";
import LinkToWitnessById from "../Utility/LinkToWitnessById";
import ChainTypes from "../Utility/ChainTypes";
import BindToChainState from "../Utility/BindToChainState";
import AssetWrapper from "../Utility/AssetWrapper";
import TransactionChart from "./TransactionChart";
import BlocktimeChart from "./BlocktimeChart";
import classNames from "classnames";
import utils from "common/utils";
import Immutable from "immutable";
import TimeAgo from "../Utility/TimeAgo";
import FormattedAsset from "../Utility/FormattedAsset";
import Ps from "perfect-scrollbar";
import TransitionWrapper from "../Utility/TransitionWrapper";

require("../Blockchain/json-inspector.scss");

class BlockTimeAgo extends React.Component {
    static defaultProps = {
        isTitle: true
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.blockTime !== this.props.blockTime;
    }

    render() {
        let {blockTime, isTitle} = this.props;

        if(!blockTime) return null;
        // let timePassed = Date.now() - blockTime;
        let timePassed = new Date().getTime() - new Date(blockTime).getTime();

        let textClass = classNames(
            "txtlabel",
            {success: timePassed <= 6000},
            {info: timePassed > 6000 && timePassed <= 15000},
            {warning: timePassed > 15000 && timePassed <= 25000},
            {error: timePassed > 25000}
        );

        return isTitle ? (
            <h3 className={textClass}>
                <TimeAgo time={blockTime} />
            </h3>
        ) : <TimeAgo time={blockTime} className={textClass} />;
    }
}

class Blocks extends React.Component {
    static propTypes = {
        globalObject: ChainTypes.ChainObject.isRequired,
        dynGlobalObject: ChainTypes.ChainObject.isRequired
    };

    static defaultProps = {
        globalObject: "2.0.0",
        dynGlobalObject: "2.1.0",
        latestBlocks: {},
        assets: {},
        accounts: {},
        height: 1
    };

    constructor(props) {
        super(props);

        this.state = {
            animateEnter: false,
            operationsHeight: null,
            blocksHeight: null
        };

        this._updateHeight = this._updateHeight.bind(this);
    }

    _getBlock(height, maxBlock) {
        if (height) {
            height = parseInt(height, 10);
            BlockchainActions.getLatest(height, maxBlock);
        }
    }

    componentDidMount() {
        this._getInitialBlocks();
        let oc = this.refOperations;
        Ps.initialize(oc);
        let blocks = this.refBlocks;
        Ps.initialize(blocks);
        this._updateHeight();


        window.addEventListener("resize", this._updateHeight, {
            capture: false,
            passive: true
        });
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this._updateHeight);
    }

    UNSAFE_componentWillReceiveProps(nextProps) {

        if (nextProps.latestBlocks.size === 0) {
            return this._getInitialBlocks();
        } else if (!this.state.animateEnter) {
            this.setState({
                animateEnter: true
            });
        }

        let maxBlock = nextProps.dynGlobalObject.get("head_block_number");
        if (
            nextProps.latestBlocks.size >= 20 &&
            nextProps.dynGlobalObject.get("head_block_number") !==
                nextProps.latestBlocks.get(0).id
        ) {
            return this._getBlock(maxBlock, maxBlock);
        }
    }


    shouldComponentUpdate(nextProps, nextState) {
        return (
            !Immutable.is(nextProps.latestBlocks, this.props.latestBlocks) ||
            !utils.are_equal_shallow(nextState, this.state)
        );
    }

    componentDidUpdate() {
        this._updateHeight();
    }

    _getInitialBlocks() {
        let maxBlock = parseInt(
            this.props.dynGlobalObject.get("head_block_number"),
            10
        );
        if (maxBlock) {
            for (let i = 19; i >= 0; i--) {
                let exists = false;
                if (this.props.latestBlocks.size > 0) {
                    for (let j = 0; j < this.props.latestBlocks.size; j++) {
                        if (
                            this.props.latestBlocks.get(j).id ===
                            maxBlock - i
                        ) {
                            exists = true;
                            break;
                        }
                    }
                }
                if (!exists) {
                    this._getBlock(maxBlock - i, maxBlock);
                }
            }
        }
    }

    _updateHeight() {
        let containerHeight = this.outerWrapper.offsetHeight;
        let operationsTextHeight = this.operationsText.offsetHeight;

        /*this.setState(
            {
                operationsHeight: containerHeight - operationsTextHeight,
                blocksHeight: containerHeight
            },
            this.psUpdate
        );*/
    }

    psUpdate() {
        let oc = this.refOperations;
        Ps.update(oc);
        let blocks = this.refBlocks;
        Ps.update(blocks);
    }

    render() {
        let {
            latestBlocks,
            latestTransactions,
            globalObject,
            dynGlobalObject,
            coreAsset
        } = this.props;
        let {operationsHeight} = this.state;
        const dynamicObject = this.props.getDynamicObject(
            coreAsset.get("dynamic_asset_data_id")
        );
        let blocks = null,
            transactions = null;
        let headBlock = null;
        let trxCount = 0,
            blockCount = latestBlocks.size,
            trxPerSec = 0,
            blockTimes = [],
            avgTime = 0;

        if (latestBlocks && latestBlocks.size >= 20) {
            let previousTime;

            let lastBlock, firstBlock;

            // Map out the block times for the latest blocks and count the number of transactions
            latestBlocks
                .filter((a, index) => {
                    // Only use consecutive blocks counting back from head block
                    return (
                        a.id ===
                        dynGlobalObject.get("head_block_number") - index
                    );
                })
                .sort((a, b) => {
                    return a.id - b.id;
                })
                .forEach((block, index) => {
                    trxCount += block.transactions.length;
                    if (index > 0) {
                        blockTimes.push([
                            block.id,
                            (block.timestamp - previousTime) / 1000
                        ]);
                        lastBlock = block.timestamp;
                    } else {
                        firstBlock = block.timestamp;
                    }
                    previousTime = block.timestamp;
                });

            // Output block rows for the last 20 blocks
            blocks = latestBlocks
                .sort((a, b) => {
                    return b.id - a.id;
                })
                .take(10)
                .map(block => {
                    return (
                        <tr key={block.id}>
                            <td>
                                <Link to={`/block/${block.id}`}>
                                    #{utils.format_number(block.id, 0)}
                                </Link>
                            </td>
                            <td>
                                <FormattedDate
                                    value={block.timestamp}
                                    format="time"
                                />
                            </td>
                            <td>
                                <LinkToWitnessById witness={block.witness} />
                            </td>
                            <td>
                                {utils.format_number(
                                    block.transactions.length,
                                    0
                                )}
                            </td>
                        </tr>
                    );
                })
                .toArray();

            let trxIndex = 0;

            transactions = latestTransactions
                .sort((a, b) => {
                    return b.block_num - a.block_num;
                })
                .take(10)
                .map(trx => {
                    let opIndex = 0;
                    return trx.operations
                        .map(op => {
                            if (trxIndex > 15) return null;
                            return (
                                <Operation
                                    key={trxIndex++}
                                    op={op}
                                    result={trx.operation_results[opIndex++]}
                                    block={trx.block_num}
                                    hideFee={true}
                                    hideOpLabel={false}
                                    current={"1.2.0"}
                                    prefixCls={"dx-table-"}
                                    hideDate
                                    hidePending
                                />
                            );
                        })
                        .filter(a => !!a);
                })
                .toArray();

            headBlock = latestBlocks.first().timestamp;
            avgTime = blockTimes.reduce((previous, current, idx, array) => {
                return previous + current[1] / array.length;
            }, 0);

            trxPerSec = trxCount / ((lastBlock - firstBlock) / 1000);
        }

        return (
            <div ref={ref=>this.outerWrapper=ref} className="explorer-block">

                <Row gutter={[15, 15]}>
                    <Col xs={{ span: 24}} sm={{ span: 6}}>
                        <Card title={<Translate content="explorer.blocks.current_block"/>} bordered={false}>
                            #{utils.format_number(dynGlobalObject.get("head_block_number"), 0)}
                        </Card>
                    </Col>
                    <Col xs={{ span: 24}} sm={{ span: 6}}>
                        <Card title={<Translate content="explorer.blocks.last_block"/>} bordered={false}>
                            <span style={{
                                textTransform: "uppercase"
                            }}>
                                <BlockTimeAgo isTitle={false} blockTime={headBlock} />
                            </span>
                        </Card>
                    </Col>
                    <Col xs={{ span: 24}} sm={{ span: 6}}>
                        <Card title={<Translate content="explorer.blocks.trx_per_sec"/>} bordered={false}>
                            {utils.format_number(trxPerSec, 2)}
                        </Card>
                    </Col>
                    <Col xs={{ span: 24}} sm={{ span: 6}}>
                        <Card title={<Translate content="explorer.blocks.avg_conf_time"/>} bordered={false}>
                            {utils.format_number(avgTime / 2, 2)}s
                        </Card>
                    </Col>
                </Row>

                <Row gutter={[15, 15]}>
                    <Col xs={{ span: 24}} sm={{ span: 6}}>
                        <Card title={<Translate content="explorer.blocks.active_witnesses"/>} bordered={false}>
                            <span className={"txtlabel success"}>
                                {globalObject.get("active_witnesses").size}
                            </span>
                        </Card>

                    </Col>
                    <Col xs={{ span: 24}} sm={{ span: 6}}>
                        <Card title={<Translate content="explorer.blocks.active_committee_members"/>} bordered={false}>
                            <span className={"txtlabel success"}>
                                {globalObject.get("active_committee_members").size}
                            </span>
                        </Card>
                    </Col>
                    <Col xs={{ span: 24}} sm={{ span: 6}}>
                        <Card title={<Translate content="explorer.blocks.trx_per_block"/>} bordered={false}>
                            {utils.format_number(trxCount / blockCount || 0, 2)}
                        </Card>
                    </Col>
                    <Col xs={{ span: 24}} sm={{ span: 6}}>
                        <Card title={<Translate content="explorer.blocks.recently_missed_blocks"/>} bordered={false}>
                            <span className={"txtlabel warning"}>
                                {dynGlobalObject.get("recently_missed_count")}
                            </span>
                        </Card>
                    </Col>
                </Row>

                <Row gutter={[15, 15]}>
                    <Col xs={{ span: 24}} sm={{ span: 6}}>
                        {/*<Card title={<Translate content="explorer.asset.summary.current_supply"/>} bordered={false}>
                            {dynamicObject ? (
                                <FormattedAsset
                                    amount={dynamicObject.get("current_supply")}
                                    asset={coreAsset.get("id")}
                                    decimalOffset={5}
                                />
                            ) : null}
                        </Card>*/}
                        <Card title="" bordered={false}>

                        </Card>
                    </Col>
                    <Col xs={{ span: 24}} sm={{ span: 6}}>
                        <Card title={<Translate content="explorer.blocks.block_times"/>} className={"chart"} bordered={false}>
                            <BlocktimeChart
                                blockTimes={blockTimes}
                                head_block_number={dynGlobalObject.get(
                                    "head_block_number"
                                )}
                            />
                        </Card>
                    </Col>
                    <Col xs={{ span: 24}} sm={{ span: 6}}>
                        <Card title={<Translate content="explorer.blocks.trx_per_block"/>} className={"chart"} bordered={false}>
                            <TransactionChart
                                blocks={latestBlocks}
                                head_block={dynGlobalObject.get(
                                    "head_block_number"
                                )}
                            />
                        </Card>
                    </Col>
                    <Col xs={{ span: 24}} sm={{ span: 6}}>
                        <Card title={<Translate content="explorer.asset.summary.stealth_supply"/>} bordered={false}>
                            {dynamicObject ? (
                                <FormattedAsset
                                    amount={dynamicObject.get("confidential_supply")}
                                    asset={coreAsset.get("id")}
                                    decimalOffset={5}
                                />
                            ) : null}
                        </Card>
                    </Col>
                </Row>

                <br/>
                <br/>
                <br/>

                <Row gutter={15}>
                    <Col xs={{ span: 24}} sm={{ span: 12}} >
                        <div ref={ref=>this.operationsText=ref} >
                            <Translate className="explorer-block-header" components={"div"} content="account.recent" />

                            <table className="table fixed-height-2rem">
                                <thead>
                                    <tr>
                                        <th>
                                            <Translate content="account.votes.info" />
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div
                            className="grid-block"
                            style={{
                                minHeight: 200,
                                maxHeight: operationsHeight || "400px",
                                overflow: "hidden"
                            }}
                            ref={ref=>this.refOperations=ref}
                        >
                            <table className="table fixed-height-2rem">
                                <tbody>{transactions}</tbody>
                            </table>
                        </div>
                    </Col>
                    <Col xs={{ span: 24}} sm={{ span: 12}}>
                        <Translate className="explorer-block-header" components={"div"} content="explorer.blocks.recent" />

                        <div className="explorer-block-table" ref={ref=>this.refBlocks=ref}>
                            <table className="table fixed-height-2rem">
                                <thead>
                                    <tr>
                                        <th>
                                            <Translate
                                                component="span"
                                                content="explorer.block.id"
                                            />
                                        </th>
                                        <th>
                                            <Translate
                                                component="span"
                                                content="explorer.block.date"
                                            />
                                        </th>
                                        <th>
                                            <Translate
                                                component="span"
                                                content="explorer.block.witness"
                                            />
                                        </th>
                                        <th>
                                            <Translate
                                                component="span"
                                                content="explorer.block.count"
                                            />
                                        </th>
                                    </tr>
                                </thead>

                                <TransitionWrapper
                                    component="tbody"
                                    transitionName="newrow"
                                >
                                    {blocks}
                                </TransitionWrapper>
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

Blocks = BindToChainState(Blocks, {show_loader: true});
Blocks = AssetWrapper(Blocks, {
    propNames: ["coreAsset"],
    withDynamic: true
});
export default Blocks;

import React from "react";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import Immutable from "immutable";
import Translate from "react-translate-component";
import AccountActions from "actions/AccountActions";
import {debounce} from "lodash-es";
import Icon from "../Icon/Icon";
import BalanceComponent from "../Utility/BalanceComponent";
import AccountStore from "stores/AccountStore";
import LoadingIndicator from "../LoadingIndicator";
import {Table, Select} from "antd";
import SearchInput from "../Utility/SearchInput";
import {ChainStore} from "deexjs";
import clnm from "classnames";
import counterpart from "counterpart";

class Accounts extends React.Component {

    static defaultProps = {
        searchTerm: ""
    };

    constructor(props) {
        super();
        this.state = {
            searchTerm: props.searchTerm,
            isLoading: false,
            toggleSearchGroup: false,
            rowsOnPage: "10"
        };

        this._searchAccounts = debounce(this._searchAccounts, 200);
        this.handleRowsChange = this.handleRowsChange.bind(this);

        this.balanceObjects = [];
    }

    componentDidMount() {
        AccountActions.getCountAccount();
    }

    componentDidUpdate(prevProps) {
        if( prevProps.countAccounts !== this.props.countAccounts ) {
            this._searchAccounts("", this.props.countAccounts);
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log(nextProps.locale !== this.props.locale, nextProps.locale , this.props.locale);
        return (
            !Immutable.is(
                nextProps.searchAccounts,
                this.props.searchAccounts
            ) ||
            nextProps.locale !== this.props.locale ||
            nextProps.countAccounts !== this.props.countAccounts ||
            nextState.toggleSearchGroup !== this.state.toggleSearchGroup ||
            nextState.searchTerm !== this.state.searchTerm ||
            nextState.isLoading !== this.state.isLoading
        );
    }

    _onSearchChange(e) {
        let {value} = e.target;
        this.setState({
            searchTerm: e.target.value.toLowerCase(),
            isLoading: true
        });
        this._searchAccounts(value);


    }

    _searchAccounts(searchTerm, limit) {
        if( searchTerm.split(".").length === 3 ) {
            AccountActions.accountSearchId(searchTerm);
        } else {
            AccountActions.accountSearch(searchTerm, limit);
        }
        this.setState({isLoading: false});
    }

    _onAddContact(account, e) {
        e.preventDefault();
        AccountActions.addAccountContact(account);
        this.forceUpdate();
    }

    _onRemoveContact(account, e) {
        e.preventDefault();
        AccountActions.removeAccountContact(account);
        this.forceUpdate();
    }

    handleRowsChange(rows) {
        this.setState({
            rowsOnPage: rows
        });
        this.forceUpdate();
    }

    _ensureBalanceObject(object_id) {
        if (object_id && typeof object_id === "string") {
            if (!this.balanceObjects[object_id]) {
                this.balanceObjects[object_id] = parseFloat(
                    ChainStore.getObject(object_id).get("balance")
                );
            }
        }
        if (!this.balanceObjects[object_id]) {
            this.balanceObjects[object_id] = 0;
        }
    }

    render() {
        let {searchAccounts, locale} = this.props;
        let {searchTerm, toggleSearchGroup} = this.state;

        let dataSource = [];
        let columns = [];

        columns = [
            {
                title: (
                    <Translate content="explorer.assets.id" />
                ),
                dataIndex: "accountId",
                key: "accountId",
                defaultSortOrder: "ascend",
                sorter: (a, b) => {
                    return a.accountId > b.accountId
                        ? 1
                        : a.accountId < b.accountId
                            ? -1
                            : 0;
                },
                render: id => {
                    return <div>{id}</div>;
                }
            },
            {
                title: <Icon name="user" title="icons.user.account" />,
                dataIndex: "accountContacts",
                key: "accountContacts",
                render: (contacts, record) => {
                    return contacts.has(record.accountName) ? (
                        <div
                            onClick={this._onRemoveContact.bind(
                                this,
                                record.accountName
                            )}
                        >
                            <Icon
                                name="minus-circle"
                                title="icons.minus_circle.remove_contact"
                            />
                        </div>
                    ) : (
                        <div
                            onClick={this._onAddContact.bind(
                                this,
                                record.accountName
                            )}
                        >
                            <Icon
                                name="plus-circle"
                                title="icons.plus_circle.add_contact"
                            />
                        </div>
                    );
                }
            },
            {
                title: <Translate content="account.name" />,
                dataIndex: "accountName",
                key: "accountName",
                sorter: (a, b) => {
                    return a.accountName > b.accountName
                        ? 1
                        : a.accountName < b.accountName
                            ? -1
                            : 0;
                },
                render: name => {
                    return (
                        <div>
                            <Link to={`/account/${name}/overview`}>{name}</Link>
                        </div>
                    );
                }
            },
            {
                title: <Translate content="gateway.balance" />,
                dataIndex: "accountBalance",
                key: "accountBalance",
                sorter: (a, b) => {
                    this._ensureBalanceObject(a.accountBalance);
                    this._ensureBalanceObject(b.accountBalance);

                    return this.balanceObjects[a.accountBalance] >
                        this.balanceObjects[b.accountBalance]
                        ? 1
                        : this.balanceObjects[a.accountBalance] <
                          this.balanceObjects[b.accountBalance]
                            ? -1
                            : 0;
                },
                render: balance => {
                    return (
                        <div>
                            {!balance ? (
                                "n/a"
                            ) : (
                                <BalanceComponent balance={balance} />
                            )}
                        </div>
                    );
                }
            },
            {
                title: <Translate content="account.percent" />,
                dataIndex: "accountBalance",
                key: "accountBalancePercentage",
                sorter: (a, b) => {
                    this._ensureBalanceObject(a.accountBalance);
                    this._ensureBalanceObject(b.accountBalance);

                    return this.balanceObjects[a.accountBalance] >
                        this.balanceObjects[b.accountBalance]
                        ? 1
                        : this.balanceObjects[a.accountBalance] <
                          this.balanceObjects[b.accountBalance]
                            ? -1
                            : 0;
                },
                render: balance => {
                    return (
                        <div>
                            {!balance ? (
                                "n/a"
                            ) : (
                                <BalanceComponent
                                    balance={balance}
                                    asPercentage={true}
                                />
                            )}
                        </div>
                    );
                }
            }
        ];

        if (searchAccounts.size > 0) {
            searchAccounts
                .filter(a => {
                    /*
                     * This appears to return false negatives, perhaps from
                     * changed account name rules when moving to graphene?. Either
                     * way, trying to resolve invalid names fails in the ChainStore,
                     * which in turn breaks the BindToChainState wrapper
                     */
                    // if (!ChainValidation.is_account_name(a, true)) {
                    //     return false;
                    // }
                    return a.indexOf(searchTerm) !== -1;
                })
                .sort((a, b) => {
                    if (a > b) {
                        return 1;
                    } else if (a < b) {
                        return -1;
                    } else {
                        return 0;
                    }
                })
                .map((name, id) => {
                    let currentAccount = ChainStore.getAccount(
                        id.toLowerCase()
                    );
                    let balance = currentAccount
                        ? currentAccount.getIn(["balances", "1.3.0"]) || null
                        : null;

                    dataSource.push({
                        accountId: id,
                        accountContacts: AccountStore.getState()
                            .accountContacts,
                        accountName: name,
                        accountBalance: balance
                    });
                });
        }



        return (
            <div className="explorer-block">
                <div className="generic-bordered-box">
                    <div className={"assets-search"}>
                        <SearchInput
                            placeholderTranslate={"markets.search"}
                            value={this.state.searchTerm}
                            onChange={this._onSearchChange.bind(this)}
                        />
                        <Icon name={"filter-setting"} onClick={()=>this.setState({
                            toggleSearchGroup: !toggleSearchGroup
                        })} />

                        <div className={clnm("assets-search-group", {
                            active: toggleSearchGroup
                        })} style={{marginLeft: "20px"}}>
                            <Select
                                style={{width: "250px"}}
                                value={this.state.rowsOnPage}
                                onChange={this.handleRowsChange}
                                getPopupContainer={()=>document.getElementById("account-tabs")}
                            >
                                {[10,25,50,100,200].map(item=>(
                                    <Select.Option key={item}>
                                        {item} rows
                                    </Select.Option>
                                ))}
                            </Select>

                            <div
                                style={{
                                    display: "inline-block",
                                    marginLeft: "24px"
                                }}
                            >
                                {this.state.searchTerm &&
                                this.state.searchTerm.length == 0 ? (
                                    <Translate content="account.start_typing_to_search" />
                                ) : null}
                            </div>
                        </div>
                    </div>

                    <Table
                        style={{width: "100%", marginTop: "16px"}}
                        rowKey="accountId"
                        columns={columns}
                        dataSource={dataSource}
                        scroll={{x: 800}}
                        pagination={{
                            position: "bottom",
                            pageSize: Number(this.state.rowsOnPage)
                        }}
                    />
                    {this.state.isLoading ? (
                        <div style={{textAlign: "center", padding: 10}}>
                            <LoadingIndicator type="three-bounce" />
                        </div>
                    ) : null}
                </div>
            </div>
        );
    }
}

Accounts.defaultProps = {
    searchAccounts: {}
};

Accounts.propTypes = {
    searchAccounts: PropTypes.object.isRequired
};

export default Accounts;

import React from "react";
import MyMarketsContainer from "../Exchange/MarketsContainer";

class MarketsContainer extends React.Component {
    render() {
        return (
            <div className={"exchange-market"}>
                <MyMarketsContainer />
            </div>
        );
    }
}

export default MarketsContainer;

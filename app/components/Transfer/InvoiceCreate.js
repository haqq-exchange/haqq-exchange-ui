import React, {useState} from "react";
import Translate from "react-translate-component";
import {Form, Input, Icon, Button, Select} from "antd";
import {compress, decompress} from "lzma";
import bs58 from "common/base58";

const { Option } = Select;
let id=0;

import "./transfer.scss";
import CopyButton from "Utility/CopyButton";
import AccountApi from "api/accountApi";
import {connect} from "alt-react";
import GatewayStore from "stores/GatewayStore";
import AccountStore from "stores/AccountStore";
import {getAlias} from "config/alias";
import ImageLoad from "Utility/ImageLoad";

class InvoiceCreate extends React.Component {
    state = {
        linkTo: false
    };

    remove = k => {
        const {form} = this.props;
        // can use data-binding to get
        const keys = form.getFieldValue("keys");
        // We need at least one passenger
        if (keys.length === 1) {
            return;
        }

        // can use data-binding to set
        form.setFieldsValue({
            keys: keys.filter(key => key !== k)
        });
    };

    add = () => {
        const {form} = this.props;
        // can use data-binding to get
        const keys = form.getFieldValue("keys");
        const nextKeys = keys.concat(id++);
        // can use data-binding to set
        // important! notify form to detect changes
        form.setFieldsValue({
            keys: nextKeys
        });
    };

    checkAccountName = (value) => {
        return new Promise(resolve => {
            AccountApi.lookupAccounts(value, 30)
                .then(result => {
                    resolve(result.find(a => a[0] === value));
                });
        });
    };

    validateAccountName = (rule, value, callback) => {
        //const {form} = this.props;

        if (value ) {
            this.checkAccountName(value).then(result => {
                if( result ) {
                    callback();
                } else {
                    callback("Not found");
                }
            });

        } else {
            callback();
        }
    };

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const {keys, ...other} = values;
                if( other.line_items ) {
                    other.line_items = other.line_items.filter(a=>a);
                }
                //let keysMap = keys.map(key => names[key]);
                compress(JSON.stringify(other), 1, result=>{
                    const bytes = Buffer.from(result, "hex");
                    let compressed_data = bs58.encode(bytes);
                    this.setState({
                        linkTo: [window.location.origin, "invoice", compressed_data].join("/")
                    });
                });
            }
        });
    };

    render() {
        const {linkTo} = this.state;
        const {backedCoins, accountName} = this.props;
        const {getFieldDecorator, getFieldValue} = this.props.form;

        const optionCoinList = backedCoins.get("TDEX").map(coins=>
            <Option key={`option-${coins.symbol}`} value={coins.symbol}>
                <ImageLoad
                    customStyle={{
                        width: "20px",
                        heigth: "20px",
                        marginRight: "10px"
                    }}
                    imageName={`${coins.symbol.toLowerCase()}.png`} />
                {getAlias(coins.symbol)}
            </Option>
        );

        const formItemLayoutWithOutLabel = {
            wrapperCol: {
                xs: {span: 24, offset: 0},
                sm: {span: 20, offset: 4}
            }
        };
        getFieldDecorator("keys", {initialValue: []});
        const keys = getFieldValue("keys");

        const formItems = keys.map(k => (
            <li key={k}>
                <Form.Item required={false}>
                    {getFieldDecorator(`line_items[${k}].label`, {
                        validateTrigger: ["onChange", "onBlur"],
                        rules: [
                            {
                                required: true,
                                whitespace: true,
                                message: "Please input passenger's name or delete this field."
                            }
                        ]
                    })(
                        <Translate component="input" type="text"
                            style={{width: "50%", marginRight: 8}}
                            attributes={{ placeholder: "invoice.header_name" }}
                        />)}

                    {getFieldDecorator(`line_items[${k}].quantity`, {
                        validateTrigger: ["onChange", "onBlur"],
                        initialValue: 1,
                        rules: [
                            {
                                required: true
                            }
                        ]
                    })(
                        <Translate
                            component="input" type="text"
                            style={{width: "10%", marginRight: 8}}
                            attributes={{ placeholder: "invoice.header_count" }}
                        />)}
                    {getFieldDecorator(`line_items[${k}].price`, {
                        validateTrigger: ["onChange", "onBlur"],
                        rules: [
                            {
                                required: true
                            }
                        ]
                    })(
                        <Translate
                            component="input" type="text"
                            style={{width: "20%", marginRight: 8}}
                            attributes={{ placeholder: "invoice.header_price" }}
                        />)}


                    {keys.length > 1 ? (
                        <Icon
                            className="dynamic-delete-button"
                            type="minus-circle-o"
                            onClick={() => this.remove(k)}
                        />
                    ) : null}
                </Form.Item>
            </li>
        ));
        return (
            <div className={"Invoice-create"}>
                <Translate content={"invoice.title"} className={"Invoice-create-title"} component={"div"}  />

                <Form onSubmit={this.handleSubmit} >
                    <div className={"Invoice-create-top"}>

                        <Form.Item label={<Translate content={"invoice.name_invoice"} />}>
                            {getFieldDecorator("to_label", {
                                validateTrigger: ["onChange", "onBlur"],
                                rules: [
                                    {
                                        required: false
                                    }
                                ]
                            })(<Input />)}
                        </Form.Item>
                        <Form.Item label={<Translate content={"invoice.recipient"}  />}>
                            {getFieldDecorator("to", {
                                initialValue: accountName,
                                validateTrigger: ["onChange", "onBlur"],
                                rules: [
                                    {
                                        required: true
                                    },
                                    {
                                        validator: this.validateAccountName,
                                    }
                                ]
                            })(<Input />)}
                        </Form.Item>

                        <PaysBill {...this.props} validateAccountName={this.validateAccountName} />

                        <Form.Item label={<Translate content={"invoice.currency"} />}>
                            {getFieldDecorator("currency", {
                                initialValue: __GBL_CHAIN__ ? "GBL" : "DEEX",
                                rules: [{
                                    required: true,
                                    message: <Translate content={"invoice.errors.currency"}  />
                                }]
                            })(
                                <Select>{optionCoinList}</Select>,
                            )}
                        </Form.Item>
                        <MemoFields {...this.props} />
                    </div>


                    {formItems.length ? <HeaderFields /> : null}
                    <ol>
                        {formItems}
                    </ol>
                    <Form.Item {...formItemLayoutWithOutLabel}>
                        <Button type="dashed" className={"btn btn-green"} onClick={this.add} >
                            <Icon type="plus"/> <Translate content={"invoice.add_field"} />
                        </Button>
                    </Form.Item>
                    <Form.Item {...formItemLayoutWithOutLabel}>
                        {!linkTo ? <Button type="primary" className={"btn btn-red"} disabled={!formItems.length} htmlType="submit">
                            <Translate content={"invoice.create_invoice"} />
                        </Button> :
                            <>
                                <Input readOnly={true} value={linkTo} style={{width: "60%"}} addonAfter={<CopyButton
                                    text={linkTo}
                                    className={"ant-input-clipboard"}
                                    tip="tooltip.copy_password"
                                    dataPlace="top" />} />
                                <br/>
                                <Button type="primary" className={"btn btn-red"} onClick={()=>this.setState({linkTo: false})}>
                                    <Translate content={"invoice.reset"}  />
                                </Button>
                            </>
                        }
                    </Form.Item>
                    {/*<Input value={linkTo} readOnly={true}/>*/}
                </Form>
            </div>
        );
    }
}

const HeaderFields = () => {
    return (
        <div className={"Invoice-create-header-fields"}>
            <Translate content={"invoice.header_name"} style={{width: "50%", marginRight: 8}} />
            <Translate content={"invoice.header_count"} style={{width: "10%", marginRight: 8}}  />
            <Translate content={"invoice.header_price"} style={{width: "20%"}}  />
        </div>
    );
};

const MemoFields = props => {
    const [hasMemo, setMemo] = useState(false)
    return (
        <div>
            {hasMemo ?
                <Form.Item label={<Translate content={"invoice.memo"} />}>
                    {props.form.getFieldDecorator("memo", {
                        validateTrigger: ["onChange", "onBlur"],
                        rules: [
                            {
                                required: true
                            }
                        ]
                    })(<Input />)}
                    <Icon
                        className="dynamic-delete-button"
                        type="minus-circle-o"
                        onClick={() => setMemo(false)}
                    />
                </Form.Item> :
                <Form.Item >
                    <Button type="dashed" className={"btn btn-green"} onClick={()=>setMemo(true)} >
                        <Icon type="plus"/> <Translate content={"invoice.add_memo"}  />
                    </Button>
                </Form.Item>
            }
        </div>
    );
};

const PaysBill = props => {
    const [hasPays, setPaysBill] = useState(false);
    const [countPays, setCountPays] = useState(1);
    const countFields = Array.from({ length: countPays }, (v, k) => k);
    return (
        <div>
            {hasPays ?
                countFields.map(index => {
                    return <Form.Item key={`Form.Item.${index}`} label={<Translate content={"invoice.who_pays"} />}>
                        {
                            props.form.getFieldDecorator(`payer[${index}]`, {
                                validateTrigger: ["onChange", "onBlur"],
                                rules: [
                                    {
                                        validator: props.validateAccountName
                                    }
                                ]
                            })(<Input/>)
                        }
                        <Icon
                            className="dynamic-delete-button"
                            type="minus-circle-o"
                            onClick={() => {
                                let nextCount = countPays - 1;
                                setCountPays(nextCount);
                                setPaysBill(!!nextCount);

                            }}
                        />
                        <Icon
                            className="dynamic-delete-button"
                            type="plus-circle-o"
                            onClick={() => setCountPays(countPays + 1)}
                        />

                    </Form.Item>;
                })
                :
                <Form.Item >
                    <Button type="dashed" className={"btn btn-link"} onClick={()=>{
                        setCountPays(1);
                        setPaysBill(true);
                    }} >
                        <Icon type="plus"/> <Translate content={"invoice.add_pays"} />
                    </Button>
                </Form.Item>
            }
        </div>
    );
};

const WrappedDynamicFieldSet = Form.create({name: "dynamic_form_item"})(InvoiceCreate);

export default connect(
    WrappedDynamicFieldSet,
    {
        listenTo() {
            return [
                GatewayStore
            ];
        },
        getProps() {
            let {currentAccount, passwordAccount } = AccountStore.getState();
            return {
                backedCoins: GatewayStore.getState().backedCoins,
                accountName: currentAccount || passwordAccount
            };
        }
    }
);

//export default WrappedDynamicFieldSet;

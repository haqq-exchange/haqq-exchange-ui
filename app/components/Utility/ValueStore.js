import MarketStatsCheck from "Components/Utility/MarketStatsCheck";
import ChainTypes from "Components/Utility/ChainTypes";
import PropTypes from "prop-types";
import utils from "common/utils";
import ReactTooltip from "react-tooltip";
import marketUtils from "common/market_utils";
import {ChainStore} from "deexjs";
import counterpart from "counterpart";
import Translate from "react-translate-component";
import FormattedAsset from "Components/Utility/FormattedAsset";
import BindToChainState from "Components/Utility/BindToChainState";
import AssetWrapper from "Components/Utility/AssetWrapper";
import React from "react";
import {connect} from "alt-react";
import MarketsStore from "stores/MarketsStore";
import SettingsStore from "stores/SettingsStore";

class TotalValue extends MarketStatsCheck {
    static propTypes = {
        toAsset: ChainTypes.ChainAsset.isRequired,
        coreAsset: ChainTypes.ChainAsset.isRequired,
        inHeader: PropTypes.bool,
        label: PropTypes.string
    };

    static defaultProps = {
        inHeader: false,
        label: "",
        coreAsset: "1.3.0",
        collateral: {},
        openOrders: {},
        debt: {},
    };

    constructor() {
        super();
    }

    shouldComponentUpdate(np) {
        return (
            super.shouldComponentUpdate(np) ||
            !utils.are_equal_shallow(np.fromAssets, this.props.fromAssets) ||
            np.toAsset !== this.props.toAsset ||
            !utils.are_equal_shallow(np.balances, this.props.balances) ||
            !utils.are_equal_shallow(np.openOrders, this.props.openOrders) ||
            !utils.are_equal_shallow(np.collateral, this.props.collateral) ||
            !utils.are_equal_shallow(np.debt, this.props.debt)
        );
    }

    componentDidUpdate() {
        if (this.props.inHeader) {
            ReactTooltip.rebuild();
        }
    }

    _convertValue(amount, fromAsset, toAsset, marketStats, coreAsset) {
        if (!fromAsset || !toAsset) {
            return 0;
        }

        return marketUtils.convertValue(
            amount,
            toAsset,
            fromAsset,
            marketStats,
            coreAsset
        );
    }

    _assetValues(totals, amount, asset) {
        if (!totals[asset]) {
            totals[asset] = amount;
        } else {
            totals[asset] += amount;
        }

        return totals;
    }

    render() {
        let {
            fromAssets,
            toAsset,
            balances,
            marketStats,
            collateral,
            debt,
            openOrders,
            inHeader
        } = this.props;
        let coreAsset = ChainStore.getAsset("1.3.0");

        if (!coreAsset || !toAsset) {
            return null;
        }

        let assets = {};
        fromAssets.forEach(asset => {
            if (asset) {
                assets[asset.get("id")] = asset;
            }
        });

        let totalValue = 0;
        let assetValues = {};

        // console.log("collateral", collateral);
        // console.log("openOrders", openOrders);
        // console.log("debt", debt);

        // Collateral value
        for (let asset in collateral) {
            let fromAsset = assets[asset];
            if (fromAsset) {
                let collateralValue = this._convertValue(
                    collateral[asset],
                    fromAsset,
                    toAsset,
                    marketStats,
                    coreAsset
                );
                totalValue += collateralValue;
                assetValues = this._assetValues(
                    assetValues,
                    collateralValue,
                    fromAsset.get("id")
                );
            }
        }

        // Open orders value
        for (let asset in openOrders) {
            let fromAsset = assets[asset];
            if (fromAsset) {
                let orderValue = this._convertValue(
                    openOrders[asset],
                    fromAsset,
                    toAsset,
                    marketStats,
                    coreAsset
                );
                totalValue += orderValue;
                assetValues = this._assetValues(
                    assetValues,
                    orderValue,
                    fromAsset.get("id")
                );
            }
        }

        // Debt value
        for (let asset in debt) {
            let fromAsset = assets[asset];
            if (fromAsset) {
                let debtValue = this._convertValue(
                    debt[asset],
                    fromAsset,
                    toAsset,
                    marketStats,
                    coreAsset
                );
                totalValue -= debtValue;
                assetValues = this._assetValues(
                    assetValues,
                    -debtValue,
                    fromAsset.get("id")
                );
            }
        }

        // Balance value
        balances.forEach(balance => {
            let fromAsset = assets[balance.asset_id];
            if (fromAsset) {
                let eqValue =
                    fromAsset !== toAsset
                        ? this._convertValue(
                            balance.amount,
                            fromAsset,
                            toAsset,
                            marketStats,
                            coreAsset
                        )
                        : balance.amount;

                totalValue += eqValue;
                assetValues = this._assetValues(
                    assetValues,
                    eqValue,
                    fromAsset.get("id")
                );
            }
        });

        // Determine if higher precision should be displayed
        let hiPrec = false;
        for (let asset in assetValues) {
            if (assets[asset] && assetValues[asset]) {
                if (
                    Math.abs(
                        utils.get_asset_amount(assetValues[asset], toAsset)
                    ) < 100
                ) {
                    hiPrec = true;
                    break;
                }
            }
        }

        // Render each asset's balance, noting if there are any values missing
        const noDataSymbol = "**";
        const minValue = 1e-12;
        let missingData = false;
        let totalsTip = "<table><tbody>";
        for (let asset in assetValues) {
            if (assets[asset] && assetValues[asset]) {
                let symbol = assets[asset].get("symbol");
                let amount = utils.get_asset_amount(
                    assetValues[asset],
                    toAsset
                );
                if (amount) {
                    if (amount < minValue && amount > -minValue) {
                        // really close to zero, but not zero, probably a result of incomplete data
                        amount = noDataSymbol;
                        missingData = true;
                    } else if (hiPrec) {
                        if (amount >= 0 && amount < 0.01) amount = "<0.01";
                        else if (amount < 0 && amount > -0.01)
                            amount = "-0.01<";
                        else amount = utils.format_number(amount, 2);
                    } else {
                        if (amount >= 0 && amount < 1) amount = "<1";
                        else if (amount < 0 && amount > -0.01) amount = "-1<";
                        else amount = utils.format_number(amount, 0);
                    }
                } else {
                    amount = noDataSymbol;
                    missingData = true;
                }
                totalsTip += `<tr><td>${symbol}:&nbsp;</td><td style="text-align: right;">${amount} ${toAsset.get(
                    "symbol"
                )}</td></tr>`;
            }
        }

        // If any values are missing, let the user know.
        if (missingData)
            totalsTip += `<tr><td>&nbsp;</td><td style="text-align: right;">${noDataSymbol} no data</td></tr>`;

        totalsTip += '<tr><td colSpan="2">&nbsp;</td></tr>';
        totalsTip += `<tr><td colSpan="2">${counterpart.translate(
            "account.total_estimate"
        )}</td></tr>`;
        totalsTip += "</tbody></table>";

        // console.log("ValueStoreWrapper totalValue", totalValue, fromAssets)
        // console.log("ValueStoreWrapper toAsset", toAsset.toJS(), toAsset.get("id"));

        if (!inHeader) {
            return (
                <span>
                    {this.props.label ? (
                        <span className="font-secondary">
                            <Translate content={this.props.label} />:{" "}
                        </span>
                    ) : null}
                    <FormattedAsset
                        noTip={this.props.noTip}
                        noPrefix
                        hide_asset={this.props.hide_asset}
                        amount={totalValue}
                        asset={toAsset.get("id")}
                        decimalOffset={
                            toAsset.get("symbol").indexOf("BTC") === -1
                                ? toAsset.get("precision") - 2
                                : 4
                        }
                    />
                </span>
            );
        } else {
            return (
                <div
                    className="tooltip inline-block"
                    data-tip={totalsTip}
                    data-place="bottom"
                    data-html={true}
                >
                    {this.props.label ? (
                        <span className="font-secondary">
                            <Translate content={this.props.label} />:{" "}
                        </span>
                    ) : null}
                    <FormattedAsset
                        noTip
                        noPrefix
                        hide_asset={this.props.hide_asset}
                        amount={totalValue}
                        asset={toAsset.get("id")}
                        decimalOffset={
                            toAsset.get("symbol").indexOf("BTC") === -1
                                ? toAsset.get("precision") - 2
                                : 4
                        }
                    />
                </div>
            );
        }
    }
}
const TotalValueBind = BindToChainState(TotalValue, {keep_updating: true});
const TotalValueAsset = AssetWrapper(TotalValueBind, {
    propNames: ["fromAssets"],
    asList: true
});

class ValueStoreWrapper extends React.Component {
    render() {
        let preferredUnit = this.props.preferredUnit || this.props.settings.get("unit") || "1.3.0";
        //console.log("preferredUnit", preferredUnit)
        return <TotalValueAsset {...this.props} toAsset={preferredUnit} />;
    }
}

export default connect(
    ValueStoreWrapper,
    {
        listenTo() {
            return [MarketsStore, SettingsStore];
        },
        getProps() {
            return {
                marketStats: MarketsStore.getState().allMarketStats,
                settings: SettingsStore.getState().settings
            };
        }
    }
);

import React from "react";
import SettingsStore from "../../stores/SettingsStore";
import assetConfig from "../../config/asset";
import {ChainStore, FetchChain} from "deexjs";
import {Asset} from "../../lib/common/MarketClasses";
import PropTypes from "prop-types";
import {checkBalance, checkFeeStatusAsync, shouldPayFeeWithAssetAsync} from "../../lib/common/trxHelper";
import AccountActions from "../../actions/AccountActions";
import TransactionConfirmStore from "../../stores/TransactionConfirmStore";

export default function TransferWrapper(WrappedComponent, selectData) {

    return class extends React.Component {
        static propTypes = {
            memo: PropTypes.string, // asset id
            from_account: PropTypes.string.isRequired, // account id
            to_account: PropTypes.string, // account id
            asset:  PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.number
            ]),
            amount: PropTypes.number // count asset
        };
        static defaultProps = {
        };

        constructor(props) {
            super(props);
            this.mounting = null;

            this.updateFee = this.updateFee.bind(this);
            this.checkBalance = this.checkBalance.bind(this);
        }

        componentDidMount() {
            this.mounting = true;
            this.initGetState(this.props);
        }

        componentWillUnmount() {
            this.mounting = false;
        }

        componentDidUpdate(prevProps) {
            /*amount: 79.546
            asset: "QIWIRUBLE"
            memo: "3f189c8e-263c-42dd-ad4d-ea1c39a55e8c"
            loan: {id: "3f189c8e-263c-42dd-ad4d-ea1c39a55e8c", number: 3, state: {…}, created: "2019-12-24 09:56:38.282143", started: "2019-12-26 18:02:27.129216", …}
            to_account: "deex-lt-intres"
            from_account: "mirotincev2"*/
            if( prevProps.amount !== this.props.amount ||
                prevProps.asset !== this.props.asset ||
                prevProps.to_account !== this.props.to_account ||
                prevProps.from_account !== this.props.from_account ||
                prevProps.memo !== this.props.memo
            ) {
                this.initGetState(this.props);
            }
        }

        initGetState = async () => {
            await this.getInitialState(this.props)
                .then(result=>this.setState(result))
                .then(()=>this.updateFee());
        };

        getInitialState = (props) => {
            let assetKey = SettingsStore.getState().starredKey;
            const DEEX_ID = assetConfig[assetKey].id;

            const fee_asset     = FetchChain("getAsset", DEEX_ID);
            const transfer_asset     = props.asset ? FetchChain("getAsset", props.asset) : null;
            const from_account     = props.from_account ? FetchChain("getAccount", props.from_account): null;
            const to_account     = props.to_account ? FetchChain("getAccount", props.to_account) : null;

            return new Promise(resolve=>{
                Promise.all([
                    fee_asset,
                    transfer_asset,
                    from_account,
                    to_account,
                ]).then(result => {
                    const [fee_asset, transfer_asset, from_account, to_account] = result;

                    console.log("props.amount", props.amount, props)

                    let resolveState = {
                        from_account,
                        to_account,
                        amount: null,
                        memo: props.memo,
                        asset: transfer_asset,
                        feeAsset: fee_asset,
                        feeAmount: new Asset({
                            amount: 0,
                            asset_id: fee_asset.get("id")
                        }),
                        feeStatus: {},
                        error: null,
                    };

                    if( transfer_asset ) {
                        resolveState.amount = new Asset({
                            amount: props.amount,
                            real: props.amount,
                            asset_id: transfer_asset ? transfer_asset.get("id") : transfer_asset,
                            precision: transfer_asset ? transfer_asset.get("precision") : null
                        });
                    }

                    resolve(resolveState);
                })
            });
        };
        setToAccount = name => {
            const _this = this;
            if(name) {
                FetchChain("getAccount", name).then(account => {
                    _this.setState({
                        to_account: account
                    });
                });
            }
        };
        setAmount = value => {
            const _this = this;
            let {asset, amount} = _this.state;
            if(value) {
                console.log("_this.state", _this.state, amount);
                amount.setAmount({real: value});
                _this.setState({amount}, this.updateFee);
            }
        };
        setAsset = assetName => {
            const _this = this;
            const {asset} = _this.state;
            console.log("assetName", assetName);
            if(assetName) {
                FetchChain("getAsset", assetName).then(asset=>{
                    console.log("asset", asset);
                    _this.setState({asset}, _this.checkAccountTotal );
                });
            }
        };
        setMemo = memo => {
            const _this = this;
            if(memo) {
                _this.setState({memo}, _this.updateFee);
            }
        };

        setFeeAsset({ asset }) {
            const _this = this;
            _this.setState({feeAsset: asset}, _this.updateFee);
        }

        submitTransfer = (event, confirmTransfer) =>  {
            const _this = this;

            event ? event.preventDefault() : null;

            const {callBack}  = _this.props;
            const {from_account, to_account, memo, asset, feeAsset, amount}  = _this.state;
            _this.setState({
                error: null,
                hidden: null
            });

            console.log("_this.state", _this.state);
            console.log("confirmTransfer", event, confirmTransfer)
            TransactionConfirmStore.listen(_this.onTrxIncluded);

            return AccountActions.transfer(
                from_account.get("id"),
                to_account.get("id"),
                amount.getAmount({real: false}),
                asset.get("id"),
                memo,
                null,
                feeAsset.get("id"),
                confirmTransfer )
                .then(() => {

                    if( callBack ) callBack();
                    // await TransactionConfirmStore.unlisten(_this.onTrxIncluded);
                    // await TransactionConfirmStore.listen(_this.onTrxIncluded);
                })
                .catch(e => {
                    if( callBack ) callBack();
                    let msg = e.message ? e.message.split("\n")[1] || e.message : null;
                    _this.setState({
                        error: msg
                    });
                });
        };

        onTrxIncluded(confirm_store_state) {
            if (confirm_store_state.included && confirm_store_state.broadcasted_transaction) {
                TransactionConfirmStore.unlisten(this.onTrxIncluded);
                TransactionConfirmStore.reset();
            } else if (confirm_store_state.closed) {
                TransactionConfirmStore.unlisten(this.onTrxIncluded);
                TransactionConfirmStore.reset();
            }
        }

        async updateFee() {
            const {from_account, feeAsset, memo} = this.state;
            await checkFeeStatusAsync({
                accountID: from_account.get("id"),
                feeID: feeAsset.get("id"),
                options: ["price_per_kbyte"],
                data: {
                    type: "memo",
                    content: memo
                }
            }).then(({ fee, hasBalance, hasPoolBalance }) => {
                console.log("fee, hasBalance, hasPoolBalance", fee, hasBalance, hasPoolBalance);
                console.log("from_account, fee", from_account, fee);
                shouldPayFeeWithAssetAsync(from_account, fee).then(should => {
                    this.setState({
                        should,
                        feeAmount: fee,
                        fee_asset_id: fee.asset_id,
                        hasBalance,
                        hasPoolBalance
                    }, this.checkBalance);
                });
            });
        }

        checkAccountTotal() {
            let { from_account, feeAsset } = this.state;
            if (from_account && from_account.get("balances") ) {
                let account_balances = from_account.get("balances").toJS();
                this.setTotal(
                    feeAsset.get("id"),
                    account_balances[feeAsset.get("id")]
                );
            }
        }

        setTotal(asset_id, balance_id) {
            const _this = this;
            const { feeAmount } = this.state;

            FetchChain("getObject", [asset_id, balance_id], undefined, {}).then(data=> {
                const [transferAsset, balanceObject] = data;
                let balance = new Asset({
                    amount: balanceObject.get("balance"),
                    asset_id: transferAsset.get("id"),
                    precision: transferAsset.get("precision")
                });

                if (balanceObject) {
                    if (feeAmount.asset_id === balance.asset_id) {
                        balance.minus(feeAmount);
                    }
                    _this.setState({
                        maxAmount: true,
                        amount: balance
                    },  _this.updateFee);
                }
            });
        }

        checkBalance() {
            const { feeAmount, amount, from_account, asset } = this.state;
            //if (!this.mounting) return;
            console.log("checkBalance !this.mounting", !this.mounting);
            console.log("checkBalance this.state", this.state);
            if (!asset || !from_account || !feeAmount || !this.mounting) return;
            // this.updateFee();

            const balanceID    = from_account.getIn(["balances", asset.get("id")]);
            const feeBalanceID = from_account.getIn([
                "balances",
                feeAmount.asset_id
            ]);
            if (!asset || !from_account) return;
            if (!balanceID)
                return this.setState({
                    balanceError: true,
                    typeBalanceError: "balanceID"
                });
            let balanceObject    = ChainStore.getObject(balanceID);
            let feeBalanceObject = feeBalanceID ? ChainStore.getObject(feeBalanceID) : null;
            if (!feeBalanceObject)
                return this.setState({
                    balanceError: true,
                    typeBalanceError: "feeBalance"
                });
            if (!balanceObject || !feeAmount) return;
            if (!amount)
                return this.setState({
                    balanceError: false
                });

            let assetAmount = amount;
            if( "asset_id" in amount) {
                console.log(">>>>>");
            } else {
                assetAmount = new Asset({
                    amount: amount,
                    asset_id: asset.get("id"),
                    precision: asset.get("precision")
                });
            }

            const hasBalance = checkBalance(
                assetAmount.getAmount({real: true}),
                asset,
                feeAmount,
                balanceObject
            );
            console.log("amount", amount.getAmount({real: true}));
            console.log("feeAmount", asset);
            console.log("feeAmount", feeAmount);
            console.log("balanceObject", balanceObject);

            console.log("hasBalance", hasBalance);

            // if (!hasBalance) return;
            this.setState({
                balanceError: !hasBalance
            });
        }

        render() {

            const props = Object.assign({}, this.props, this.state, {
                getInitialState: props => this.getInitialState(props),
                setToAccount: name => this.setToAccount(name),
                setAmount: amount => this.setAmount(amount),
                setAsset: assetName => this.setAsset(assetName),
                setMemo: memo => this.setMemo(memo),
                setFeeAsset: asset => this.setFeeAsset(asset),
                submitTransfer: (event, confirm) => this.submitTransfer(event, confirm),
            });

            // ... и рендерит оборачиваемый компонент со свежими данными!
            // Обратите внимание, что мы передаём остальные пропсы
            return <WrappedComponent {...props} />;
        }

    };
}

import React from "react";
import PropTypes from "prop-types";
import Translate from "react-translate-component";
import {Input} from "antd";
import FormattedAsset from "./FormattedAsset";
import MenuDropdown from "./MenuDropdown";
import Immutable from "immutable";
import counterpart from "counterpart";
import AssetWrapper from "./AssetWrapper";
import utils from "common/utils";
import AssetSelect from "./AssetSelect";
import {getAlias} from "config/alias";

class AssetSelector extends React.Component {
    static propTypes = {
        value: PropTypes.string, // asset id
        onChange: PropTypes.func,
        scroll_length: PropTypes.number
    };

    shouldComponentUpdate(np) {
        return (
            !utils.are_equal_shallow(np.assets, this.props.assets) ||
            np.value !== this.props.value ||
            np.scroll_length !== this.props.scroll_length
        );
    }

    render() {
        if (!this.props.assets.length) return null;

        return (
            <MenuDropdown
                entries={this.props.assets
                    .map(a => a && a.get("symbol"))
                    .sort((a,b)=>{
                        return getAlias(a) > getAlias(b) ? 0 : -1;
                    })
                    .filter(a => !!a)}
                values={this.props.assets.reduce((map, a) => {
                    if (a && a.get("symbol")) map[a.get("symbol")] = a;
                    return map;
                }, {}) }
                singleEntry={
                    this.props.assets[0] ? (
                        <FormattedAsset
                            asset={this.props.assets[0].get("id")}
                            amount={0}
                            hide_amount={true}
                        />
                    ) : null
                }
                wrapperId={this.props.wrapperId}
                value={this.props.value}
                onChange={this.props.onChange}
                scroll_length={this.props.scroll_length}
            />
        );
    }
}

AssetSelector = AssetWrapper(AssetSelector, {asList: true});

class AmountSelector extends React.Component {
    static propTypes = {
        label: PropTypes.string, // a translation key for the label
        assets: PropTypes.array,
        amount: PropTypes.any,
        placeholder: PropTypes.string,
        onChange: PropTypes.func,
        tabIndex: PropTypes.number,
        error: PropTypes.string,
        scroll_length: PropTypes.number
    };

    static defaultProps = {
        disabled: false
    };

    componentDidMount() {
        this.onAssetChange(this.props.asset);
    }

    static formatAmount(v) {
        /*// TODO: use asset's precision to format the number*/
        if (!v) v = "";
        if (typeof v === "number") v = v.toString();
        return v.trim().replace(/,/g, "");
    }

    _onChange(event) {
        let amount = event.target.value;

        console.log("amount", amount.replace(",", ""));
        if (this.props.onChange)
            this.props.onChange({
                amount: amount.replace(",", ""),
                asset: this.props.asset
            });
    }

    onAssetChange(selected_asset) {
        if (this.props.onChange)
            this.props.onChange({
                amount: this.props.amount,
                asset: selected_asset
            });
    }

    render() {
        //console.log("Calling AmountSelector: " + this.props.label + this.props.asset + this.props.assets + this.props.amount + this.props.placeholder + this.props.error);
        let value = this.props.error
            ? counterpart.translate(this.props.error)
            : AmountSelector.formatAmount(this.props.amount);
        return (
            <div className="amount-selector" style={this.props.style}>
                <label className="right-label">
                    {this.props.display_balance}
                </label>
                <Translate
                    className="left-label"
                    component="label"
                    content={this.props.label}
                />
                
                {/* <div className="inline-label input-wrapper"> */}
    
                <Input.Group compact>
                    <Input
                        style={{
                            width: "calc(100% - 131px)",
                            height: "40px",
                            marginRight: "1px"
                        }}
                        disabled={this.props.disabled}
                        type="text"
                        value={value || ""}
                        placeholder={this.props.placeholder}
                        onChange={event=>this._onChange(event)}
                        tabIndex={this.props.tabIndex}
                    />
                    {/* <div className="form-label select floating-dropdown" id={"amount-selector"} style={{color: "#ffffff", width: "auto", padding: 0}}> */}
                    <AssetSelect
                        style={{width: "130px"}}
                        selectStyle={{width: "100%"}}
                        // wrapperId={"amount-selector"}
                        // ref={this.props.refCallback}
                        value={getAlias(this.props.asset.get("symbol"))}
                        assets={Immutable.List(this.props.assets)}
                        onChange={this.onAssetChange.bind(this)}
                        isAmount={true}
                        // scroll_length={this.props.scroll_length}
                    />
                    {/* </div> */}
                </Input.Group>
                {/* </div> */}
                {this.props.errorBlock ? this.props.errorBlock : null}
                {this.props.helpBlock ? this.props.helpBlock : null}
            </div>
        );
    }
}
AmountSelector = AssetWrapper(AmountSelector);

export default AmountSelector;

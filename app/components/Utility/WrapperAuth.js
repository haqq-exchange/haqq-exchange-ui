import React from "react";
import WalletDb from "stores/WalletDb";
import ModalActions from "actions/ModalActions";
import AccountActions from "actions/AccountActions";

export default class WrapperAuth extends React.Component{

    toggleLock = (event) => {
        if (WalletDb.isLocked()) {
            event.preventDefault();
            event.stopPropagation();
            ModalActions.show("unlock_wallet_modal_public").then(() => {
                AccountActions.tryToSetCurrentAccount();
                if(this.props.onClick) this.props.onClick(event);
            });
        } else {
            if(this.props.onClick) this.props.onClick(event);
        }
    };


    render() {
        const {component, children, ...other} = this.props;
        let Component = component || "span";
        return (
            <Component {...other} onClick={this.toggleLock}>
                {children}
            </Component>
        );
    }

}

import React from "react";
import {Link} from "react-router-dom";
import AssetWrapper from "./AssetWrapper";
import AssetName from "./AssetName";
import utils from "../../lib/common/utils";
import ImageLoad from "Utility/ImageLoad";

function importAll(r) {
    let images = {};
    r.keys().map(item => { images[item.replace("./", "")] = r(item); });
    return images;
}

class LinkToAssetById extends React.Component {

    static defaultProps = {
        prefixLink: "/asset"
    };

    render() {
        const { asset, showLogo, className, prefixLink } = this.props;
        const symbol = asset.get("symbol");
        let image = null,
            defClassName = className || "link-asset";
        const assetName = <AssetName name={symbol} customClass={defClassName+"-name"} noTip />;
        //const images = importAll(require.context("../../assets/asset-symbols", false, /\.png$/));
        let {name} = utils.replaceName(asset),
            imageName = [name, "png"].join(".").toLowerCase();
        if( showLogo) {
            image = <ImageLoad
                customClassName={defClassName + "-logo"}
                imageName={imageName} />
        }


        return this.props.noLink ? (
            <span className={defClassName + "-text"}>
                {image}
                {assetName}
            </span>
        ) : (
            <Link className={defClassName} to={`${prefixLink}/${symbol}/`}>
                {image}
                {assetName}
            </Link>
        );
    }
}

export default AssetWrapper(LinkToAssetById);

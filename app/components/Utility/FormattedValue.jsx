import React from "react";
import PropTypes from "prop-types";


/**
 *  Given an amount and an asset, render it with proper precision
 *
 *  Expected Properties:
 *     base_asset:  asset id, which will be fetched from the
 *     base_amount: the ammount of asset
 *     quote_asset:
 *     quote_amount: the ammount of asset
 *
 */

export default class FormattedValue extends React.Component {

    static propTypes = {
        locales: PropTypes.string,
        value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        maximumFractionDigits: PropTypes.number,
        minimumFractionDigits: PropTypes.number,
    };

    static defaultProps = {
        locales: "en",
        maximumFractionDigits: 4,
        minimumFractionDigits: 4,
    };
    render() {
        let {
            locales,
            maximumFractionDigits,
            minimumFractionDigits,
            value
        } = this.props;
        let localeNumber = (value).toLocaleString(locales, {
            maximumFractionDigits,
            minimumFractionDigits
        });
        return (
            <>{localeNumber}</>
        );
    }
}

import React from "react";
import cn from "classnames";


export default class ImageLoad extends React.Component{
    static defaultProps = {
        staticUrl: "https://static.haqq.exchange",
        staticPath: "images/assets",
        defaultImage: "https://static.haqq.exchange/images/assets/defaultLogo.png",
        defaultProps: {
            alt: ""
        },
        customClassName: {}
    };

    constructor(props) {
        super(props);
        this.state = {
            imageUrl: [props.staticUrl, props.staticPath, props.imageName].join("/"),
            imageStatus: "loading"
        };
    }

    handleImageLoaded = () => {
        this.setState({
            imageStatus: "loaded"
        });
    };

    handleImageErrored = () => {
        let { defaultImage } = this.props;
        this.setState({
            imageUrl: defaultImage,
            imageStatus: "filed"
        });
    };


    render() {
        let { customStyle, customClassName , defaultProps } = this.props;
        let { imageUrl , imageStatus} = this.state;
        let defaultClassName = cn(customClassName, imageStatus);

        return (
            <img
                src={imageUrl}
                onLoad={()=>this.handleImageLoaded()}
                onError={()=>this.handleImageErrored()}
                className={defaultClassName}
                style={customStyle}
                {...defaultProps}
            />
        );
    }

}
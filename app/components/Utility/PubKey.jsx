import React from "react";
import WalletDb from "stores/WalletDb";
import WalletUnlockActions from "actions/WalletUnlockActions";
import counterpart from "counterpart";
import Icon from "../Icon/Icon";
//import connectToStores from "alt-utils/lib/connectToStores";
import WalletUnlockStore from "stores/WalletUnlockStore";
import ConfidentialWallet from "stores/ConfidentialWallet";
import {connect} from "alt-react";
import utils from "common/utils";
import ModalActions from "../../actions/ModalActions";

class PubKey extends React.Component {

    static defaultProps = {
        fullLength: false
    };

    constructor(props) {
        super();
        this.state = {fullLength: props.fullLength};
    }

    shouldComponentUpdate(nextProps, nextState) {
        //console.warn("StealthAccount.js nextProps", nextProps.value, this.props.value);
        return nextProps.value !== this.props.value ||
               nextProps.wallet_locked !== this.props.wallet_locked ||
               nextProps.wallet_object !== this.props.wallet_object ||
               nextState.fullLength !== this.state.fullLength;
    }

    _toggleLock(e) {
        e.preventDefault();
        //WalletUnlockActions.unlock();
        ModalActions.show("unlock_wallet_modal_public");
    }

    _selectElementText(el) {
        const range = document.createRange();
        range.selectNodeContents(el);
        const selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
    }

    _copyToClipboard(e) {
        e.preventDefault();
        this.setState({fullLength: true});
        setTimeout(() => {
            const el = this.refs.value;
            this._selectElementText(el);
            document.execCommand("copy");
            window.getSelection().removeAllRanges();
        }, 500);
    }

    render() {
        let {wallet_locked, fullLength} = this.props;
        if (this.state.fullLength) fullLength = true;

        if (wallet_locked) {
            return (
                <div className="pubkey">
                    <span>{counterpart.translate("transfer.pubkey_unlock")} </span>
                    <span onClick={this._toggleLock.bind(this)}>
                        <Icon name="locked"/>
                    </span>
                </div>
            );
        }

        let value = this.props.getValue();
        if (!value) value = "(missing)";
        if (typeof value !== "string") value = value.toString();

        let full_value = value;
        if (value && !fullLength && value.length > 80) {
            value = value.substr(0, 80) + "...";
        }

        return (
            <div className="pubkey" data-tip={full_value !== value ? full_value : null} data-place="top" data-type="light">
                <code ref="value">
                    {value}
                </code>
                <span
                    onClick={this._copyToClipboard.bind(this)} data-tip="Copy to Clipboard" data-type="light">
                    <Icon name="clipboard-copy"/>
                </span>
            </div>
        );
    }
}

export default connect(
    PubKey,
    {
        listenTo() {
            return [
                WalletUnlockStore,
                ConfidentialWallet
            ];
        },
        getProps() {
            return {
                wallet_locked: WalletUnlockStore.getState().locked,
                wallet_object: ConfidentialWallet.getState().wallet_object
            };
        }
    }
);



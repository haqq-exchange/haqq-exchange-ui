import React from "react";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import ChainTypes from "./ChainTypes";
import BindToChainState from "./BindToChainState";

class LinkToAccountById extends React.Component {
    static propTypes = {
        account: ChainTypes.ChainAccountName.isRequired,
        subpage: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.bool
        ])
    };

    static defaultProps = {
        subpage: "",
        onClick: ()=>{}
    };

    shouldComponentUpdate(nextProps) {
        return nextProps.account !== this.props.account;
    }

    render() {
        let {account, noLink, subpage, onClick, ...other} = this.props;

        let account_name = typeof account === "string" ? account : account.get("name");

        return noLink ? (
            <span {...other}>{account_name}</span>
        ) : (
            <Link onClick={onClick} to={`/account/${account_name}/${subpage}`} {...other} > {account_name} </Link>
        );
    }
}

export default BindToChainState(LinkToAccountById, {autosubscribe: false});

import React from "react";
import PropTypes from "prop-types";
import FormattedAsset from "./FormattedAsset";
import ChainTypes from "./ChainTypes";
import BindToChainState from "./BindToChainState";

/**
 *  Given a balance_object, displays it in a pretty way
 *
 *  Expects one property, 'balance' which should be a balance_object id
 */

class BalanceComponent extends React.Component {
    static propTypes = {
        balance: ChainTypes.ChainObject.isRequired,
        assetInfo: PropTypes.node,
        hide_asset: PropTypes.bool
    };

    static defaultProps = {
        hide_asset: false
    };

    render() {
        const {balance} = this.props;
        let amount = 0;
        let type = __GBLTN_CHAIN__ ? "GBLTEST" : (__GBL_CHAIN__ ? "GBL" : "DEEX");
        if( balance && balance.has("balance")) {
            amount = Number(balance.get("balance"));
            type = balance.get("asset_type");
        }

        return (
            <FormattedAsset
                amount={amount}
                asset={type}
                {...this.props}
            />
        );
    }
}

export default BindToChainState(BalanceComponent, {keep_updating: true});

import React from "react";
import PropTypes from "prop-types";
import Translate from "react-translate-component";
import cnames from "classnames";
import {connect} from "alt-react";
import SettingsActions from "actions/SettingsActions";
import SettingsStore from "stores/SettingsStore";
import counterpart from "counterpart";
import { Select } from "antd";

const { Option } = Select;

/**
 *  Renders a tab layout, handling switching and optionally persists the currently open tab using the SettingsStore
 *
 *  props:
 *     setting: unique name to be used to remember the active tab of this tabs layout,
 *     tabsClass: optional classes for the tabs container div
 *     contentClass: optional classes for the content container div
 *
 *  Usage:
 *
 *  <Tabs setting="mySetting">
 *      <Tab title="locale.string.title1">Tab 1 content</Tab>
 *      <Tab title="locale.string.title2">Tab 2 content</Tab>
 *  </Tabs>
 *
 */

class Tab extends React.Component {
    static propTypes = {
        changeTab: PropTypes.func,
        isActive: PropTypes.bool.isRequired,
        index: PropTypes.number.isRequired,
        className: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
        tabClassName: PropTypes.object,
        isLinkTo: PropTypes.string,
        subText: PropTypes.oneOfType([PropTypes.object, PropTypes.string])
    };

    static defaultProps = {
        isActive: false,
        index: 0,
        tabClassName: {
            item: null,
            link: null,
            tabTitle: null,
            subText: null,
        },
        className: "",
        isLinkTo: "",
        subText: null
    };

    render() {
        let {
            isActive,
            index,
            changeTab,
            tabClassName,
            title,
            className,
            updatedTab,
            collapsed,
            disabled,
            subText
        } = this.props;
        let c = cnames({"is-active": isActive}, tabClassName.item, className);

        if (typeof title === "string" && title.indexOf(".") > 0) {
            title = counterpart.translate(title);
        }

        // console.log("this.props >>", this.props)

        // dont string concetenate subText directly within the rendering, subText can be an object without toString
        // implementation, but valid DOM (meaning, don't do subText + "someString"

        if (collapsed) {
            // if subText is empty, dont render it, we dont want empty brackets added
            if (typeof subText === "string") {
                subText = subText.trim();
            }

            return (
                <option value={index} data-is-link-to={this.props.isLinkTo}>
                    <span className="tab-title">
                        {title}
                        {updatedTab ? "*" : ""}
                        {subText && " ("+ subText +")"}
                    </span>
                </option>
            );
        }
        return (
            <li
                className={c}
                onClick={() =>
                    !disabled
                        ? changeTab(index, this.props.isLinkTo)
                        : {}
                }
            >
                <a className={cnames(tabClassName.link, {"is-active": isActive})}>
                    <span className={cnames(tabClassName.tabTitle || "tab-title")}>
                        {title}
                        {updatedTab ? "*" : ""}
                    </span>
                    {subText && <div className={cnames(tabClassName.subText || "tab-subtext")}>{subText}</div>}
                </a>
            </li>
        );
    }
}

class Tabs extends React.Component {
    static propTypes = {
        setting: PropTypes.string,
        defaultActiveTab: PropTypes.number,
        segmented: PropTypes.bool
    };

    static defaultProps = {
        active: 0,
        defaultActiveTab: 0,
        segmented: true,
        contentClass: "",
        style: {},
        tabsStyle: {}
    };

    static contextTypes = {
        router: PropTypes.object
    };

    constructor(props) {
        super();
        this.state = {
            activeTab: props.setting
                ? props.viewSettings.get(props.setting, props.defaultActiveTab)
                : props.defaultActiveTab,
            width: window.innerWidth
        };

        this._setDimensions = this._setDimensions.bind(this);
    }

    componentDidMount() {
        this._setDimensions();
        window.addEventListener("resize", this._setDimensions, {
            capture: false,
            passive: true
        });
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        let nextSetting = nextProps.viewSettings.get(nextProps.setting);
        if (nextSetting !== this.props.viewSettings.get(this.props.setting)) {
            this.setState({
                activeTab: nextSetting
            });
        }
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this._setDimensions);
    }

    _setDimensions() {
        let width = window.innerWidth;

        if (width !== this.state.width) {
            this.setState({width});
        }
    }

    _changeTab(value, isLinkTo) {
        if (value === this.state.activeTab) return;
        const {history} = this.props;
        // Persist current tab if desired

        // console.log("value, isLinkTo", value, isLinkTo, this.props)
        // console.log("window", history)

        if (isLinkTo !== "" && history) {
            history.push(isLinkTo);
            // this.props.history.push(isLinkTo);
            return;
        }

        if (this.props.setting) {
            SettingsActions.changeViewSetting({
                [this.props.setting]: value
            });
        }
        this.setState({activeTab: value});

        if (this.props.onChangeTab) this.props.onChangeTab(value);
    }

    render() {
        let {children, contentClass, tabsClass, style, segmented, collapsed = true, hasButtonGroup = true} = this.props;
        const collapseTabs = collapsed && this.state.width < 900 && React.Children.count(children) > 2;
        let activeContent = null;

        let tabs = React.Children.map(children, (child, index) => {
            if (!child) {
                return null;
            }
            if (collapseTabs && child.props.disabled) return null;
            let isActive = index === this.state.activeTab;
            if (isActive) {
                activeContent = child.props.children;
            }

            if( collapseTabs ) {
                return <option value={index}>{
                    counterpart.translate(child.props.title)
                }</option>;
            }

            return React.cloneElement(child, {
                collapsed: collapseTabs,
                isActive,
                changeTab: (value) => this._changeTab(value),
                index: index
            });
        }).filter(a => a !== null);

        if (!activeContent) {
            activeContent = tabs[0].props.children;
        }

        if( hasButtonGroup ) {
            tabsClass += " button-group no-margin";
        }

        return (
            <div
                style={this.props.tabsStyle}
                className={cnames(
                    {
                        "with-buttons" : this.props.actionButtons
                    },
                    this.props.className
                )}
            >
                <div className="service-selector">
                    <ul
                        ref={this.props.refProp}
                        style={style}
                        className={cnames(tabsClass, {
                            segmented
                        })}
                    >
                        {collapseTabs ? (
                            <li
                                style={{
                                    paddingLeft: 10,
                                    paddingRight: 10,
                                    minWidth: "15rem"
                                }}
                            >
                                <select
                                    value={this.state.activeTab}
                                    style={{marginTop: 10, marginBottom: 10}}
                                    className="bts-select"
                                    onChange={e => {
                                        let ind = parseInt(e.target.value, 10);
                                        this._changeTab(ind);
                                    }}
                                >
                                    {tabs}
                                </select>
                            </li>
                        ) : (
                            tabs
                        )}
                        {this.props.actionButtons ? (
                            <li className="tabs-action-buttons">
                                {this.props.actionButtons}
                            </li>
                        ) : null}
                    </ul>
                </div>
                <div className={cnames("tab-content", contentClass)}>
                    {activeContent}
                </div>
            </div>
        );
    }
}

Tabs = connect(
    Tabs,
    {
        listenTo() {
            return [SettingsStore];
        },
        getProps() {
            return {viewSettings: SettingsStore.getState().viewSettings};
        }
    }
);

export {Tabs, Tab};

import React from "react";
import PropTypes from "prop-types";
import utils from "common/utils";
import { Menu, Dropdown, Icon } from "antd";
import {getAlias} from "../../config/alias";

class FloatingDropdown extends React.Component {
    static propTypes = {
        scroll_length: PropTypes.number
    };

    static defaultProps = {
        scroll_length: 9
    };

    constructor(props) {
        const scroll_length = props.scroll_length;
        super(props);
        this.state = {
            active: false
        };

        this.listener = false;
        this.onBodyClick = this.onBodyClick.bind(this);
    }

    componentDidMount() {
        this._setListener();
    }

    shouldComponentUpdate(np, ns) {
        return (
            !utils.are_equal_shallow(np.entries, this.props.entries) ||
            !utils.are_equal_shallow(ns, this.state) ||
            np.value !== this.props.value
        );
    }

    _setListener(props = this.props) {
        if (props.entries.length > 1 && !this.listener) {
            this.listener = true;
            document.body.addEventListener("click", this.onBodyClick, {
                capture: false,
                passive: true
            });
        }
    }

    _removeListener() {
        document.body.removeEventListener("click", this.onBodyClick);
        this.listener = false;
    }

    UNSAFE_componentWillReceiveProps(np) {
        if (np.entries.length === 1) {
            this._removeListener();
        } else if (np.entries.length > 1) {
            this._setListener(np);
        }
    }

    componentWillUnmount() {
        this._removeListener();
    }

    onBodyClick(e) {
        let el = e.target;
        let insideActionSheet = false;

        do {
            if (
                el.classList &&
                el.classList.contains("dropdown") &&
                el.id === this.props.id
            ) {
                insideActionSheet = true;
                break;
            }
        } while ((el = el.parentNode));

        if (!insideActionSheet) {
            this.setState({active: false});
        } else {
            e.stopPropagation();
        }
    }

    onChange(value, e) {
        const {values} = this.props;
        this.props.onChange(values[value]);
        this.setState({
            active: false
        });
    }

    _toggleDropdown() {
        this.setState({
            active: !this.state.active
        });

    }

    render() {
        const {entries, value, wrapperId} = this.props;
        let {active} = this.state;
        if (entries.length === 0) return null;
        if (entries.length === 1) {
            return (
                this.props.singleEntry ? this.props.singleEntry : entries[0]
            );
        } else {
            const menu = (
                <Menu>
                    {entries.map(value => {
                        return (
                            <Menu.Item key={"menu-item-"+value.toLowerCase()} onClick={(event)=>this.onChange(value, event)}>
                                <span>{getAlias(value)}</span>
                            </Menu.Item>
                        );
                    })}
                </Menu>
            );

            return (
                <Dropdown
                    overlay={menu}
                    getPopupContainer={() => document.getElementById(wrapperId || "content-wrapper")}
                    trigger={["click"]} placement="bottomRight">
                    <span>
                        {value ? value : <span className="hidden">A</span>} <Icon type="down" />
                    </span>
                </Dropdown>
            );
        }
    }
}

export default FloatingDropdown;

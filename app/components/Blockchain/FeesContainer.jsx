import React from "react";
import SettingsStore from "stores/SettingsStore";
import AltContainer from "alt-container";
import Fees from "./Fees";

class FeesContainer extends React.Component {
    render() {
        //console.log("SettingsStore.getState()", SettingsStore.getState());
        return (
            <AltContainer
                stores={[SettingsStore]}
                inject={{
                    settings: SettingsStore.getState().settings
                }}
                component={Fees}
            />

        );
    }
}

export default FeesContainer;

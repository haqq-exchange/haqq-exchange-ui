import React from "react";
import Immutable from "immutable";
import counterpart from "counterpart";
import classNames from "classnames";
import Translate from "react-translate-component";
import HelpContent from "../Utility/HelpContent";
import ChainTypes from "../Utility/ChainTypes";
import BindToChainState from "../Utility/BindToChainState";
import FormattedAsset from "../Utility/FormattedAsset";
import {EquivalentValueComponent} from "../Utility/EquivalentValueComponent";
import {ChainStore, ChainTypes as grapheneChainTypes} from "deexjs";
import {Typography} from "antd";
const {operations} = grapheneChainTypes;
let ops = Object.keys(operations);

// Define groups and their corresponding operation ids
let fee_grouping = {
    general: [0, 25, 26, 27, 28, 32, 33, 37, 39, 41, 49, 50, 52],
    asset: [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 38, 43, 44, 47, 48],
    market: [1, 2, 3, 4, 45, 46],
    account: [5, 6, 7, 8, 9],
    business: [20, 21, 22, 23, 24, 29, 30, 31, 34, 35, 36]
};

// Operations that require LTM
let ltm_required = [5, 7, 20, 21, 34];

class FeeGroup extends React.Component {
    static propTypes = {
        core_asset: ChainTypes.ChainAsset.isRequired ,
        globalObject: ChainTypes.ChainObject.isRequired
    };

    static defaultProps = {
        globalObject: "2.0.0",
        core_asset: "1.3.0"
    };

    constructor(props) {
        super(props);
    }

    shouldComponentUpdate(nextProps) {
        return !Immutable.is(nextProps.globalObject, this.props.globalObject) ||
            !Immutable.is(nextProps.core_asset, this.props.core_asset);
    }

    render() {
        let {globalObject, settings, opIds, core_asset} = this.props;
        globalObject = globalObject.toJSON();

        console.log("this.props", this.props);
        console.log("settings", settings);
        console.log("core_asset", core_asset);

        let current_fees = globalObject.parameters.current_fees;
        let network_fee = globalObject.parameters.network_percent_of_fee / 1e4;
        let scale = current_fees.scale;
        let feesRaw = current_fees.parameters;
        let preferredUnit = __SCROOGE_CHAIN__ ? core_asset.get("symbol") : (settings.get("fee_asset") || core_asset.get("symbol"));

        let trxTypes = counterpart.translate("transaction.trxTypes");

        let fees = opIds.map(opID => {
            let feeIdx = feesRaw.findIndex(f => f[0] === opID);
            if (feeIdx === -1) {
                console.warn(
                    "Asking for non-existing fee id %d! Check group settings in Fees.jsx",
                    opID
                );
                return; // FIXME, if I ask for a fee that does not exist?
            }

            let feeStruct = feesRaw[feeIdx];

            let opId = feeStruct[0];
            let fee = feeStruct[1];
            let operation_name = ops[opId];
            let feename = trxTypes[operation_name];

            let feeRateForLTM = network_fee;
            if (opId === 10) {
                // See https://github.com/bitshares/bitshares-ui/issues/996
                feeRateForLTM = 0.5 + 0.5 * network_fee;
            }

            let rows = [];
            let headIncluded = false;
            let labelClass = classNames("label", "info");

            for (let key in fee) {
                let amount = (fee[key] * scale) / 1e4;
                let amountForLTM = amount * feeRateForLTM;
                let feeTypes = counterpart.translate("transaction.feeTypes");
                let assetAmount = amount ? (
                    <FormattedAsset amount={amount} asset="1.3.0" />
                ) : (
                    feeTypes["_none"]
                );
                let equivalentAmount = amount ? (
                    <EquivalentValueComponent
                        fromAsset="1.3.0"
                        fullPrecision={true}
                        amount={amount}
                        toAsset={preferredUnit}
                        fullDecimals={true}
                    />
                ) : (
                    feeTypes["_none"]
                );
                let assetAmountLTM = amountForLTM ? (
                    <FormattedAsset amount={amountForLTM} asset="1.3.0" />
                ) : (
                    feeTypes["_none"]
                );
                let equivalentAmountLTM = amountForLTM ? (
                    <EquivalentValueComponent
                        fromAsset="1.3.0"
                        fullPrecision={true}
                        amount={amountForLTM}
                        toAsset={preferredUnit}
                        fullDecimals={true}
                    />
                ) : (
                    feeTypes["_none"]
                );
                let title = null;

                if (!headIncluded) {
                    headIncluded = true;
                    title = (
                        <td rowSpan="6" style={{width: "15em"}}>
                            <b >{feename}</b>
                        </td>
                    );
                }

                if (ltm_required.indexOf(opId) < 0) {
                    if (feeTypes[key] != "Annual Membership") {
                        rows.push(
                            <tr key={opId.toString() + key}>
                                {title}
                                <td>{feeTypes[key]}</td>
                                <td style={{textAlign: "right"}}>
                                    {assetAmount}
                                    {amount !== 0 && preferredUnit !== "BTS" ? (
                                        <span>
                                            &nbsp;/&nbsp;
                                            {equivalentAmount}
                                        </span>
                                    ) : null}
                                </td>
                                {__SCROOGE_CHAIN__ ? null : <td style={{textAlign: "right"}}>
                                    {feeIdx !== 8 ? assetAmountLTM : null}
                                    {feeIdx !== 8 &&
                                    amount !== 0 &&
                                    preferredUnit !== "BTS" ? (
                                        <span>
                                            &nbsp;/&nbsp;
                                            {equivalentAmountLTM}
                                        </span>
                                    ) : null}
                                </td>}
                            </tr>
                        );
                    }
                } else {
                    rows.push(
                        <tr key={opId.toString() + key}>
                            {title}
                            <td>{feeTypes[key]}</td>
                            <td style={{textAlign: "right"}}>
                                - <sup>*</sup>
                            </td>
                            {__SCROOGE_CHAIN__ ? null : <td style={{textAlign: "right"}}>
                                {assetAmountLTM}
                                {amount !== 0 && preferredUnit !== "BTS" ? (
                                    <span>
                                        &nbsp;/&nbsp;
                                        {equivalentAmountLTM}
                                    </span>
                                ) : null}
                            </td>}
                        </tr>
                    );
                }
            }
            return <tbody key={feeIdx}>{rows}</tbody>;
        });

        console.log("this.props.title", this.props.title)

        return (
            <div className="asset-card">
                <Typography.Paragraph>{this.props.title.toUpperCase()}</Typography.Paragraph>

                <table className="table">
                    <thead>
                        <tr>
                            <th>
                                <Translate content={"explorer.block.op"} />
                            </th>
                            <th>
                                <Translate content={"explorer.fees.type"} />
                            </th>
                            <th style={{textAlign: "right"}}>
                                <Translate content={"explorer.fees.fee"} />
                            </th>
                            <th style={{textAlign: "right"}}>
                                <Translate content={__SCROOGE_CHAIN__ ? "" :"explorer.fees.feeltm"} />
                            </th>
                        </tr>
                    </thead>
                    {fees}
                </table>
            </div>
        );
    }
}
FeeGroup = BindToChainState(FeeGroup);

class Fees extends React.Component {
    render() {
        const {settings} = this.props;

        console.log("Fees settings", settings);

        let FeeGroupsTitle = counterpart.translate("transaction.feeGroups");
        let feeGroups = [];

        for (let groupName in fee_grouping) {
            let groupNameText = FeeGroupsTitle[groupName];
            let feeIds = fee_grouping[groupName];
            feeGroups.push(
                <FeeGroup
                    key={groupName}
                    settings={settings}
                    opIds={feeIds}
                    title={groupNameText}
                />
            );
        }

        return (
            <div className="grid-block vertical fees" style={{overflow: "visible"}}>
                <div
                    className="grid-block small-12 shrink"
                    style={{overflow: "visible"}}
                >
                    <HelpContent path={"components/Fees"} />
                </div>
                <br/>
                <br/>
                <div
                    className="grid-block small-12 "
                    style={{overflow: "visible"}}
                >
                    <div className="grid-content">{feeGroups}</div>
                </div>
            </div>
        );
    }
}

export default Fees;

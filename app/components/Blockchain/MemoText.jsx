import React, {useState} from "react";
import PrivateKeyStore from "stores/PrivateKeyStore";
import WalletUnlockActions from "actions/WalletUnlockActions";
import counterpart from "counterpart";
import Icon from "../Icon/Icon";
import {connect} from "alt-react";
import WalletUnlockStore from "stores/WalletUnlockStore";
import utils from "common/utils";
import ReactTooltip from "react-tooltip";
import {Tooltip} from "antd";
import xss from "xss";

class MemoText extends React.Component {
    static defaultProps = {
        fullLength: false
    };

    shouldComponentUpdate(nextProps) {
        return (
            !utils.are_equal_shallow(nextProps.memo, this.props.memo) ||
            nextProps.wallet_locked !== this.props.wallet_locked
        );
    }

    componentDidMount() {
        ReactTooltip.rebuild();
    }

    _toggleLock(e) {
        e.preventDefault();
        WalletUnlockActions.unlock()
            .then(() => {
                ReactTooltip.rebuild();
            })
            .catch(() => {});
    }

    render() {
        let {memo, fullLength} = this.props;
        if (!memo) {
            return null;
        }

        let {text, isMine} = PrivateKeyStore.decodeMemo(memo);
        if (!text ) {
            return null;
        }
        if (!text && isMine) {
            return (
                <div className="memo">
                    <span>
                        {counterpart.translate("transfer.memo_unlock")}{" "}
                    </span>
                    <a onClick={this._toggleLock.bind(this)}>
                        <Icon name="locked" title="icons.locked.action" />
                    </a>
                </div>
            );
        }

        console.log("text", text);

        text = xss(text, {
            whiteList: [], // empty, means filter out all tags
            stripIgnoreTag: true // filter out all HTML not in the whilelist
        });

        console.log("text", text)

        let full_memo = text;
        if (text && !fullLength && text.length > 15) {
            text = text.substr(0, 15) + "...";
        }

        if (text) {
            return (
                <RenderMemo full_memo={full_memo} text={text} />
            );
        } else {
            return null;
        }
    }
}

const RenderMemo = props => {
    const [isShowFullText, setShowFullText] = useState(false);
    return (
        <div className="memo" style={{paddingTop: 5, cursor: "pointer"}} onClick={()=>setShowFullText(!isShowFullText)}>
            {isShowFullText ? props.full_memo : props.text}
        </div>
    );
};

class MemoTextStoreWrapper extends React.Component {
    render() {
        return <MemoText {...this.props} />;
    }
}

export default connect(
    MemoTextStoreWrapper,
    {
        listenTo() {
            return [WalletUnlockStore];
        },
        getProps() {
            return {
                wallet_locked: WalletUnlockStore.getState().locked
            };
        }
    }
);

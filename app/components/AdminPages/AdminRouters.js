import React from "react";
import counterpart from "counterpart";
import {Route, Switch} from "react-router-dom";
import Loadable from "react-loadable";
import LoadingIndicator from "Components/LoadingIndicator";
import {async_fetch, getRequestAddress, getLoadedLocales} from "api/apiConfig";
import {Page404, AdminExchange, MainAdmin} from "RoutesLink";
import accountUtils from "../../lib/common/account_utils";
import RefUtils from "../../lib/common/ref_utils";
import {connect} from "alt-react";
import SettingsStore from "../../stores/SettingsStore";
// import AdminExchangeStore from "../../stores/AdminExchangeStore";
// import WalletUnlockStore from "../../stores/WalletUnlockStore";
// import AccountStore from "../../stores/AccountStore";


class AdminRouters extends React.Component{

    static defaultProps = {
        getUserSign: accountUtils.getUserSign,
        refutils : new RefUtils({
            url: getRequestAddress("adminExchange")
        }),
        referralApi: new RefUtils({
            url: getRequestAddress("referral")
        })
    };

    constructor(props) {
        super(props);
        this.state = {
            localesLoaded: false
        }
    }

    componentDidMount() {
        this.getLoadedLocales();
    }


    getLoadedLocales = () => {
        let transAdmin = counterpart.translate("admin");
        if( typeof transAdmin !== "object" ) {
            getLoadedLocales("admin.json").then(() => this.setState({localesLoaded: true}));
        } else {
            this.setState({
                localesLoaded: true
            });
        }
    };

    render() {
        const {localesLoaded} = this.state;
        const {settings} = this.props;

        const admin_exchange = settings?.get("admin_exchange");

        if(!admin_exchange || !admin_exchange.valid) return  <Page404 />;

        if( !localesLoaded ) return  <LoadingIndicator />;
        return (
            <Switch>
                <Route path="/admin" exact component={Page404}/>
                <Route path="/admin/exchange" exact render={()=>(<AdminExchange
                    account={admin_exchange?.exchangeAccount}
                    {...this.props} />)}/>
                <Route path="/admin/main" exact render={()=>(
                    <MainAdmin {...this.props} />
                )}/>
            </Switch>
        )
    }
}

export default connect(AdminRouters, {
    listenTo() {
        return [SettingsStore];
    },
    getProps() {
        return {
            settings: SettingsStore.getState().settings,
        };
    }
});

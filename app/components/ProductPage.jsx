import React from "react";
import {connect} from "alt-react";
import AccountStore from "stores/AccountStore";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";
import {isIncognito} from "feature_detect";
import PublicHeader from "./Layout/PublicHeader";

var laptop = require("assets/img/laptop.png");

class ProductPage extends React.Component {
    componentDidUpdate() {
        const myAccounts = AccountStore.getMyAccounts();

        // use ChildCount to make sure user is on /create-account page except /create-account/*
        // to prevent redirect when user just registered and need to make backup of wallet or password
        const childCount = React.Children.count(this.props.children);

        // do redirect to portfolio if user already logged in
        if (
            this.props.router &&
            Array.isArray(myAccounts) &&
            myAccounts.length !== 0 &&
            childCount === 0
        )
            this.props.router.push("/account/" + this.props.currentAccount);
    }

    componentDidMount() {
        isIncognito(incognito => {
            this.setState({incognito});
        });
    }

    onSelect(route) {
        this.props.router.push("/login");
    }

    render() {
        const translator = require("counterpart");

        const childCount = React.Children.count(this.props.children);

        return (
            <div className="public-block">
                <PublicHeader logoPosition="left" />

                <div className="public-container grid-block">
                    <div className="large-6 small-12">
                        <div className="public-text-inner">
                            <Translate
                                content="public.deex_provides"
                                component="h2"
                            />

                            <Translate
                                className="public-gray-p"
                                content="public.deex_p1"
                                component="p"
                            />

                            <Translate
                                className="public-gray-p margin-0"
                                content="public.deex_p2"
                                component="p"
                            />

                            <div className="public-button-home">
                                <Link
                                    to="/authorization/login"
                                    className="button public-button"
                                >
                                    <Translate content="public.start_trading" />
                                </Link>
                            </div>
                        </div>
                    </div>

                    <div className="large-6 small-hidden">
                        <div className="public-laptop-inner">
                            <img src={laptop} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(ProductPage, {
    listenTo() {
        return [AccountStore];
    },
    getProps() {
        return {
            currentAccount:
                AccountStore.getState().currentAccount ||
                AccountStore.getState().passwordAccount
        };
    }
});

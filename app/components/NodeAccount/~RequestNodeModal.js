import React from "react";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
import DefaultModal from "Components/Modal/DefaultModal";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";
import {Checkbox} from "antd";
import Translate from "react-translate-component";
import classname from "classnames";
import AssetName from "Components/Utility/AssetName";
import Icon from "Components/Icon/Icon";



class RequestNodeModal extends DefaultBaseModal {

    constructor(){
        super();

        this.state = {
            error: null,
            node_name: null,
            error_user_name: true,
        };

    }

    closeModal = () => {
        const {resolve, reject, modalId} = this.props;
        ModalActions.hide(modalId)
            .then(()=>{
                this.setState({
                    error: null,
                    error_user_name: true,
                });
            })
            .then(reject);
    };

    usernameIsValid = (username) => {
        return /^[0-9a-z_]+$/.test(username) && username.length <= 32;
    };

    submitRequestNode = (event) => {
        event.preventDefault();
        const {error_user_name, node_name} = this.state;
        const {account_name, fetchNodes} = this.props;
        if( !error_user_name ) {
            console.log("submitRequestNode", this.props);
            let sendData = {
                account: account_name,
                node_name
            };
            this.props.getUserSign(sendData).then(sign => {
                fetchNodes
                    .postTemplate("/request_node", sendData, sign)
                    .then((result)=>{
                        console.log("result", result);
                        if( result.status === 200 ) {
                            this.props.setNodeStatus("loading");
                        } else {
                            this.setState(result.content);
                        }
                    });
            }).catch(this.props.catchUserSign);
        }

    };

    setNodeName = (event) => {
        let value = event.target.value;

        this.setState({
            node_name: value,
            error_user_name: !this.usernameIsValid(value)
        });
    };

    render () {
        const {isOpenModal, error_user_name, error } = this.state;
        const {modalId, modalIndex} = this.props;

        return (
            <DefaultModal
                id={modalId}
                isOpen={isOpenModal}
                className={["CenterModal",modalId].join(" ")}
                onRequestClose={this.closeModal}
                customStyle={{zIndex: modalIndex[modalId]}}
            >
                <div className="modal fadeIn is-active">
                    <div className="modal-header">
                        <span onClick={this.closeModal} className="close-button">×</span>
                    </div>
                    <div className="modal-content">
                        <div className="request-node">
                            <form onSubmit={this.submitRequestNode} noValidate={true}>

                                <label className={"request-node-label"}>
                                    <Translate component={"div"} className={"request-node-title"} content={"node.node_name"} />
                                    <input type="text" onChange={this.setNodeName}/>
                                    {error_user_name && (<Translate component={"div"} className={"request-node-error"} content={"node.error_node_name"} />)}
                                    {error ? error : null}
                                </label>
                                <Translate
                                    type={"submit"}
                                    component={"button"}
                                    onClick={this.confirmModal}
                                    className={"btn btn-red"}
                                    disabled={error_user_name}
                                    content={"node.info_confirm_node.confirm"} />
                            </form>
                        </div>
                    </div>
                </div>

            </DefaultModal>
        );
    }
}

RequestNodeModal.defaultProps = {
    modalId: "request_node_modal"
};

export default class RequestNodeModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    modalIndex: () => ModalStore.getState().modalIndex,
                    resolve: () => ModalStore.getState().resolve,
                }}
            >
                <RequestNodeModal {...this.props} />
            </AltContainer>
        );
    }
}
import React from "react";
import ModalActions from "actions/ModalActions";
import Translate from "react-translate-component";



export default class AuthLogin extends React.Component {

    showModalLogin = event => {
        event.preventDefault();

        ModalActions.show("unlock_wallet_modal_public").then(() => {
            this.props.setNodeStatus("loading");
        });
    };

    render(){
        return(
            <div className={"node"}>
                <div className={"node-block"}>
                    <div className={"node-block-add"}>
                        <Translate className={"node-title"} component={"div"} content={"node.title"}/>
                        <Translate className={"node-unlock-info"} component={"div"} content={"node.login_info"}/>
                        <button type={"button"} onClick={this.showModalLogin} className={"btn btn-red btn-auth"} >
                            <Translate content={"header.unlock_short"}/>
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}


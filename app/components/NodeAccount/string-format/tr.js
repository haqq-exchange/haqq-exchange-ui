// Turkish
const strings = {
    minutes: function minutes() {
        return "%d dakika";
    },
    hours: function hours() {
        return "%d saat";
    },
    days: function days() {
        return "%d gün";
    },
    months: function months() {
        return "%d ay";
    },
    years: function years() {
        return "%d yıl";
    }
};
export default strings;
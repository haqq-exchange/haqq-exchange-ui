
const strings = {
    minutes: function minutes() {
        return "%d minutes";
    },
    hours: function hours() {
        return "%d hours";
    },
    days: function days() {
        return "%d days";
    },
    months: function months() {
        return "%d months";
    },
    years: function years() {
        return "%d years";
    }
};
export default strings;
import React from "react";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
import DefaultModal from "Components/Modal/DefaultModal";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";
import {Checkbox} from "antd";
import Translate from "react-translate-component";
import classname from "classnames";
import AssetName from "Components/Utility/AssetName";
import Icon from "Components/Icon/Icon";



class DisabledNodeModal extends DefaultBaseModal {


    closeModal = () => {
        const {reject, modalId} = this.props;
        ModalActions.hide(modalId)
            .then(reject);
    };

    confirmModal = () => {
        const {resolve, modalId} = this.props;
        ModalActions.hide(modalId)
            .then(resolve);
    };


    render () {
        const {isOpenModal} = this.state;
        const {modalId, modalIndex, data} = this.props;

        let content = data && data.content;

        let dataInfo = {
            sum: content ? content.price / 100 * content.penalty : null,
        };

        return (
            <DefaultModal
                id={modalId}
                isOpen={isOpenModal}
                onAfterOpen={this.getCurrentPrice}
                className={["CenterModal",modalId].join(" ")}
                onRequestClose={this.closeModal}
                customStyle={{zIndex: modalIndex[modalId]}}
            >
                <div className="modal fadeIn is-active" >
                    <div className="modal-header">
                        <span onClick={this.closeModal} className="close-button">×</span>
                    </div>
                    <div className="modal-content">

                        <div className="info-node">
                            <Translate
                                component={"div"}
                                className={"info-node-title"} content={"node.delete_nodes"} />


                            <div className={"info-node-wrap"}>
                                <Translate
                                    component={"p"}
                                    className={"info-node-text"} content={"node.sure_stop"} />

                            </div>
                            <div className={"info-node-btn-group"}>
                                <Translate
                                    component={"button"}
                                    onClick={this.closeModal}
                                    className={"btn btn-gray"}
                                    content={"node.cancel_node"} />
                                <Translate
                                    component={"button"}
                                    onClick={this.confirmModal}
                                    className={"btn btn-red"}
                                    content={"node.confirm"} />
                            </div>
                        </div>


                    </div>
                </div>

            </DefaultModal>
        );
    }
}

DisabledNodeModal.defaultProps = {
    modalId: "delete_node_modal"
};

export default class DisabledNodeModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    data: () => ModalStore.getState().data["delete_node_modal"],
                    modalIndex: () => ModalStore.getState().modalIndex,
                    resolve: () => ModalStore.getState().resolve,
                }}
            >
                <DisabledNodeModal {...this.props} />
            </AltContainer>
        );
    }
}
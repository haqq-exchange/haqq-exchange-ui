import React from "react";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
import DefaultModal from "Components/Modal/DefaultModal";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";
import {Checkbox} from "antd";
import Translate from "react-translate-component";
import classname from "classnames";
import AssetName from "Components/Utility/AssetName";
import Icon from "Components/Icon/Icon";



class InfoNodeModal extends DefaultBaseModal {


    closeModal = () => {
        const {reject, modalId} = this.props;
        ModalActions.hide(modalId)
            .then(reject);
    };

    confirmModal = () => {
        const {resolve, modalId} = this.props;
        ModalActions.hide(modalId)
            .then(resolve);
    };


    render () {
        const {isOpenModal} = this.state;
        const {modalId, modalIndex, data} = this.props;

        let content = data && data.content;

        let dataInfo = {
            penalty_percent: content ? [content.penalty, "%"].join("") : null,
            penalty_sum: content ? content.price / 100 * content.penalty : null,
            interest_percent: content ? [content.interest, "%"].join("") : null,
            interest_sum: content ? content.price / 100 * content.interest : null,
            price: content ? content.price : null
        };

        window.console.log({zIndex: modalIndex[modalId]});

        return (
            <DefaultModal
                id={modalId}
                isOpen={isOpenModal}
                onAfterOpen={this.getCurrentPrice}
                className={["CenterModal",modalId].join(" ")}
                onRequestClose={this.closeModal}
                customStyle={{zIndex: modalIndex[modalId]}}
            >
                <div className="modal fadeIn is-active" >
                    <div className="modal-header">
                        <span onClick={this.closeModal} className="close-button">×</span>
                    </div>
                    <div className="modal-content">

                        <div className="info-node">
                            <Translate unsafe component={"div"} className={"info-node-title"} content={"node.info_confirm_node.title"} />
                            <div className={"info-node-wrap"}>
                                <ol>
                                    <li>
                                        <Translate component={"div"} className={"info-node-sub-title"} content={"node.info_confirm_node.sub_title1"} />
                                        <ol>
                                            <Translate component={"li"} className={"info-node-text"} content={"node.info_confirm_node.text_11"} />
                                            <Translate component={"li"} className={"info-node-text"} content={"node.info_confirm_node.text_12"} />
                                            <Translate component={"li"} className={"info-node-text"} content={"node.info_confirm_node.text_13"} />
                                        </ol>
                                    </li>
                                    <li>
                                        <Translate component={"div"} className={"info-node-sub-title"} content={"node.info_confirm_node.sub_title2"} />
                                        <ol>
                                            <Translate component={"li"} className={"info-node-text"} content={"node.info_confirm_node.text_21"} {...dataInfo} />
                                            <Translate component={"li"} className={"info-node-text"} content={"node.info_confirm_node.text_22"} />
                                            <Translate component={"li"} className={"info-node-text"} content={"node.info_confirm_node.text_23"} />
                                            <Translate component={"li"} className={"info-node-text"} content={"node.info_confirm_node.text_24"} />
                                        </ol>
                                    </li>
                                    <li>
                                        <Translate component={"div"} className={"info-node-sub-title"} content={"node.info_confirm_node.sub_title3"} />
                                        <ol>
                                            <Translate component={"li"} className={"info-node-text"} content={"node.info_confirm_node.text_31"} />
                                            <Translate component={"li"} className={"info-node-text"} content={"node.info_confirm_node.text_32"} {...dataInfo} />
                                            <Translate component={"li"} className={"info-node-text"} content={"node.info_confirm_node.text_33"} />
                                            <Translate component={"li"} className={"info-node-text"} content={"node.info_confirm_node.text_34"} />
                                        </ol>
                                    </li>
                                    <li>
                                        <Translate component={"div"} className={"info-node-sub-title"} content={"node.info_confirm_node.sub_title4"} />
                                        <ol>
                                            <Translate component={"li"} className={"info-node-text"} content={"node.info_confirm_node.text_41"} {...dataInfo} />
                                            <Translate component={"li"} className={"info-node-text"} content={"node.info_confirm_node.text_42"} {...dataInfo} />
                                        </ol>
                                    </li>
                                    <li>
                                        <Translate component={"div"} className={"info-node-sub-title"} content={"node.info_confirm_node.sub_title5"} />
                                        <ol>
                                            <Translate component={"li"} className={"info-node-text"} content={"node.info_confirm_node.text_51"} />
                                            <Translate component={"li"} className={"info-node-text"} content={"node.info_confirm_node.text_52"} />
                                            <Translate component={"li"} className={"info-node-text"} content={"node.info_confirm_node.text_53"} />
                                            <Translate component={"li"} className={"info-node-text"} content={"node.info_confirm_node.text_54"} />
                                            <Translate component={"li"} className={"info-node-text"} content={"node.info_confirm_node.text_55"} />
                                        </ol>
                                    </li>
                                </ol>
                            </div>
                            <Translate
                                component={"button"}
                                onClick={this.confirmModal}
                                className={"btn btn-red"}
                                content={"node.info_confirm_node.confirm"} />
                        </div>


                    </div>
                </div>

            </DefaultModal>
        );
    }
}

InfoNodeModal.defaultProps = {
    modalId: "info_node_modal"
};

export default class InfoNodeModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    data: () => ModalStore.getState().data["info_node_modal"],
                    modalIndex: () => ModalStore.getState().modalIndex,
                    resolve: () => ModalStore.getState().resolve,
                }}
            >
                <InfoNodeModal {...this.props} />
            </AltContainer>
        );
    }
}
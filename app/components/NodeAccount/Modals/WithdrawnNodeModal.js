import React from "react";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
import DefaultModal from "Components/Modal/DefaultModal";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";
import ChangeAccountName from "../ChangeAccountName";
import Translate from "react-translate-component";
// import classname from "classnames";
// import AssetName from "Components/Utility/AssetName";
// import Icon from "Components/Icon/Icon";
import FormattedAsset from "Utility/FormattedAsset";


class WithdrawnNodeModal extends DefaultBaseModal {
    closeModal = () => {
        const {reject, modalId} = this.props;
        ModalActions.hide(modalId).then(reject);
    };

    confirmModal = () => {
        const {resolve, modalId} = this.props;
        ModalActions.hide(modalId).then(resolve);
    };


    render() {
        const {isOpenModal} = this.state;
        const {
            modalId,
            modalIndex,
            data,
            account_name,
            withdraw_account,
            isValidName
        } = this.props;
        // debugger;
        let content = data && data.content;

        let dataInfo = {
            amount:
                content && content.assetWithdrawn
                    ? content.assetWithdrawn.getAmount({real: true})
                    : null,
            name: withdraw_account || account_name
        };

        console.log("this.props", this.props);

        return (
            <DefaultModal
                id={modalId}
                isOpen={isOpenModal}
                onAfterOpen={this.getCurrentPrice}
                className={["CenterModal", modalId].join(" ")}
                onRequestClose={this.closeModal}
                customStyle={{zIndex: modalIndex[modalId]}}
            >
                <div className="modal fadeIn is-active">
                    <div className="modal-header">
                        <span
                            onClick={this.closeModal}
                            className="close-button"
                        >
                            ×
                        </span>
                    </div>
                    <div className="modal-content">
                        <div className="info-node">
                            <Translate
                                component={"div"}
                                className={"info-node-title"}
                                content={"node.withdrawn_node"}
                            />

                            <div className={"info-node-wrap"}>
                                <Translate
                                    unsafe
                                    component={"div"}
                                    className={"info-node-text-wrap sum"}
                                    content={"node.withdrawn_sum"}
                                    with={dataInfo}
                                />
                                {content?.assetDeposit ? (
                                    <div className={"info-node-text-wrap sum"}>
                                        <Translate
                                            content={"node.table.deposit"}
                                        />
                                        :
                                        <FormattedAsset
                                            component={"b"}
                                            decimalOffset={4}
                                            amount={content.assetDeposit.getAmount(
                                                {real: false}
                                            )}
                                            asset={
                                                content.assetDeposit.asset_id
                                            }
                                        />
                                    </div>
                                ) : null}
                                <div
                                    className={"info-node-text-wrap to_account"}
                                >
                                    <Translate
                                        unsafe
                                        component={"div"}
                                        className={"info-node-text-label"}
                                        content={"node.table.to_account"}
                                    />
                                    <ChangeAccountName
                                        validName={isValidName}
                                        name={withdraw_account}
                                        setNameWithdraw={
                                            this.props.setNameWithdraw
                                        }
                                    />
                                </div>
                            </div>
                            <div className={"info-node-btn-group"}>
                                <Translate
                                    component={"button"}
                                    onClick={this.confirmModal}
                                    disabled={
                                        !isValidName || (withdraw_account && !isValidName)
                                    }
                                    className={"btn btn-red"}
                                    content={"node.table.withdrawn"}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </DefaultModal>
        );
    }
}

WithdrawnNodeModal.defaultProps = {
    modalId: "withdrawn_node_modal"
};

export default class WithdrawnNodeModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    data: () =>
                        ModalStore.getState().data["withdrawn_node_modal"],
                    modalIndex: () => ModalStore.getState().modalIndex,
                    resolve: () => ModalStore.getState().resolve
                }}
            >
                <WithdrawnNodeModal {...this.props} />
            </AltContainer>
        );
    }
}

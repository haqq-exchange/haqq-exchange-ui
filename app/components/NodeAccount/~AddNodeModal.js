import React from "react";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
import DefaultModal from "Components/Modal/DefaultModal";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";
import {Checkbox} from "antd";
import Translate from "react-translate-component";
import classname from "classnames";
import AssetName from "Components/Utility/AssetName";
import Icon from "Components/Icon/Icon";
import AmountSelector from "Components/Utility/AmountSelector";
import {checkFeeStatusAsync, shouldPayFeeWithAssetAsync} from "common/trxHelper";
import {ChainStore, FetchChain} from "deexjs";
import {Asset} from "common/MarketClasses";
import AccountActions from "../../actions/AccountActions";
import TransactionConfirmStore from "../../stores/TransactionConfirmStore";



class AddDepositNodeModal extends DefaultBaseModal {

    constructor(){
        super();

        this.state = {
            //node_name: null,
            //amount: "100000",
            // amount: "1",
            asset_id: "1.3.0",
            fee_asset_id: "1.3.0",
            fee_asset_types: ["1.3.0"],
            feeAmount: new Asset(),
            error_user_name: true,
        };
    }

    componentDidMount(){
        this.getAccount();
    }

    closeModal = () => {
        const {reject, modalId} = this.props;
        ModalActions.hide(modalId)
            .then(reject);
    };

    getAccount = () => {
        const _this = this;
        const {account_name, content} = this.props;
        const {deposit_account} = content.info;

        console.log("getAccount this.props", this.props );

        FetchChain("getAccount", [account_name, deposit_account]).then(result => {
            let [from_account, to_account] = result;
            _this.setState({
                error_user_name: !from_account.get("id"),
                from_name: from_account.get("name"),
                from_account,
                to_account
            }, _this.getFee );
        });
    };

    setComponentUpdate(){
        console.log("setComponentUpdate");
        this.getFee();
    }

    getFee() {
        let { fee_asset_id, from_account} = this.state;
        let { modalsData } = this.props;

        if (!from_account) return null;

        checkFeeStatusAsync({
            accountID: from_account.get("id"),
            feeID: fee_asset_id,
            options: ["price_per_kbyte"],
            data: {
                type: "memo",
                content: modalsData ? modalsData.node_name : null
            }
        }).then(({ fee, hasBalance, hasPoolBalance }) => {
            shouldPayFeeWithAssetAsync(from_account, fee)
                .then(should => {
                    console.log("should", should);
                    this.setState({
                        feeAmount: fee,
                        fee_asset_id: fee.asset_id,
                        hasBalance,
                        hasPoolBalance,
                        error: !hasBalance || !hasPoolBalance
                    }, this.getBalance );
                });
        });
    }

    getBalance = () => {
        const {from_account, asset_id, feeAmount} = this.state;
        const {modalsData} = this.props;
        if( from_account && from_account.get("balances") ) {
            let account_balances = from_account.get("balances").toJS();
            // modalsData.node_price.price
            if( account_balances[asset_id] ) {
                FetchChain("getAsset", [account_balances[asset_id], asset_id])
                    .then(result => {
                        let [assetB, assetD] = result;
                        let assetBalance = new Asset({
                            asset_id,
                            "amount": assetB.get("balance"),
                            "precision": assetD.get("precision")
                        });
                        let sendAsset = new Asset({
                            asset_id,
                            amount: modalsData && modalsData.node_price ? modalsData.node_price.price : 0,
                            "precision": assetD.get("precision")
                        });
                        let cloneAsset = sendAsset.clone();
                        cloneAsset.plus(feeAmount);

                        this.setState({
                            sendAsset,
                            assetBalance,
                            activeBalance: cloneAsset.lte(assetBalance)
                        });
                        // console.log("account_balances", account_balances);
                        // console.log("account_balances assetB, assetD", assetB, assetD);
                        // console.log("account_balances asset", asset);
                        // console.log("account_balances asset", cloneAsset.lte(assetBalance), cloneAsset.lt(assetBalance));
                        // console.log("account_balances assetBalance", assetBalance);
                    });
                //let asset = ChainStore.getAsset(account_balances[asset_id]);
            }

        }
    };


    showNodeModal = event => {
        event.preventDefault();
        const {account_name, fetchNodes} = this.props;

        this.props.getUserSign({account: account_name}).then(sign => {
            fetchNodes
                .postTemplate("/get_current_price", {
                    account: account_name
                }, sign)
                .then(({content})=>{
                    ModalActions.show("info_node_modal", {content: content}).then(()=>{});
                });
        }, this.props.catchUserSign);


    };

    onSubmit = event => {
        event.preventDefault();
        this.setState({ error: null });
        let { modalsData, resolve } = this.props;
        const {from_account, to_account, sendAsset, fee_asset_id, asset_id} = this.state;
        sendAsset.setAmount({real: modalsData.node_price.price});
        this.setState({ hidden: true });

        AccountActions.transfer(
            from_account.get("id"),
            to_account.get("id"),
            sendAsset.getAmount(),
            asset_id,
            modalsData ? new Buffer(modalsData.node_name, "utf-8") : null,
            null,
            fee_asset_id
        )
            .then(() => {
                TransactionConfirmStore.unlisten(this.onTrxIncluded);
                TransactionConfirmStore.listen(this.onTrxIncluded);
                resolve();
            })
            .catch(e => {
                let msg = e.message ? e.message.split("\n")[1] || e.message : null;
                console.log("error: ", e, msg);
                this.setState({ error: msg });
            });

    };

    onTrxIncluded(confirm_store_state) {
        if (
            confirm_store_state.included &&
            confirm_store_state.broadcasted_transaction
        ) {
            // this.setState(Transfer.getInitialState());
            TransactionConfirmStore.unlisten(this.onTrxIncluded);
            TransactionConfirmStore.reset();
        } else if (confirm_store_state.closed) {
            TransactionConfirmStore.unlisten(this.onTrxIncluded);
            TransactionConfirmStore.reset();
        }
    }

    render () {
        const {isOpenModal, error_user_name, activeBalance , feeAmount , isConfirm , amount, asset_id , to_account } = this.state;
        const {modalId, modalIndex, modalsData} = this.props;
        if(!to_account) {
            return null;
        }

        let fee = feeAmount.getAmount({ real: true });

        let isDisabledSubmit = error_user_name || !activeBalance || !isConfirm;
        // console.log("isDisabledSubmit", isDisabledSubmit, error_user_name, !activeBalance, !isConfirm, this.state);

        return (
            <DefaultModal
                id={modalId}
                isOpen={isOpenModal}
                className={["CenterModal",modalId].join(" ")}
                onRequestClose={this.closeModal}
                customStyle={{zIndex: modalIndex[modalId]}}
            >
                <div className="modal fadeIn is-active">
                    <div className="modal-header">
                        <span onClick={this.closeModal} className="close-button">×</span>
                    </div>
                    <div className="modal-content">
                        <div className="add-node">
                            <Translate className={"add-node-title-node"} component={"div"} content={"node.table.title_node"}/>
                            <form onSubmit={this.onSubmit} noValidate={true}>



                                <Translate
                                    className={"add-node-to-account"}
                                    component={"div"}
                                    content={"node.table.to_account"}
                                    unsafe
                                    name={to_account.get("name")}
                                />



                                <AmountSelector
                                    disabled={true}
                                    amount={modalsData && modalsData.node_price ? modalsData.node_price.price : 0}
                                    asset={asset_id}
                                    assets={[asset_id]}
                                />

                                { !activeBalance ? <Translate
                                    className={"add-node-no-balance"}
                                    component={"div"}
                                    content={"exchange.no_balance"}  /> : null }

                                <Translate
                                    className={"add-node-fee-net"}
                                    component={"div"}
                                    content={"transfer.fee_net"} asset={__GBL_CHAIN__ ? [fee, "GBL"].join(" ")} : [fee, "DEEX"].join(" ")} />

                                <div className={"add-node-confirm"}>
                                    <Checkbox
                                        prefixCls={"ant-checkbox"}
                                        checked={isConfirm}
                                        onChange={(event)=>this.setState({ isConfirm: event.target.checked })} >
                                        <Translate content={"node.table.confirm_checkbox"} />
                                    </Checkbox>
                                    <Translate
                                        component={"div"}
                                        onClick={this.showNodeModal}
                                        className={"add-node-confirm-text"}
                                        content={"node.table.confirm_text"} />


                                </div>


                                <Translate
                                    type={"submit"}
                                    component={"button"}
                                    onClick={this.confirmModal}
                                    className={"btn btn-red"}
                                    disabled={isDisabledSubmit}
                                    content={"node.table.request_submit"} />
                            </form>
                        </div>
                    </div>
                </div>

            </DefaultModal>
        );
    }
}

AddDepositNodeModal.defaultProps = {
    modalId: "add_deposit_node_modal"
};

export default class RequestNodeModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modalIndex: () => ModalStore.getState().modalIndex,
                    modalsData: () => ModalStore.getState().data["add_deposit_node_modal"],
                    modals: () => ModalStore.getState().modals,
                    resolve: () => ModalStore.getState().resolve,
                }}
            >
                <AddDepositNodeModal {...this.props} />
            </AltContainer>
        );
    }
}

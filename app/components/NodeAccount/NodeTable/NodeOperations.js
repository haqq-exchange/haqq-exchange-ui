import React from "react";
import cn from "classnames";
import moment from "moment";
import Translate from "react-translate-component";
import FormattedAsset from "Components/Utility/FormattedAsset";

class NodeOperationsMobile  extends React.PureComponent {
    state = {
        isShowContent: false
    };

    render() {
        const { props } = this;
        const { isShowContent } = this.state;

        let d = new Date();
        let timeZone = -d.getTimezoneOffset()/60;

        return (
            <div className={"node-table-rows"}>
                <div className={"node-table-row-top"}>
                    <div className={"node-table-row"}>
                        <div className={"node-table-item-column"}>

                            <span className={"node-table-item-top"} >
                                <i className={cn("node-table-status", props.state)} />
                                {props.node_name}
                            </span>
                        </div>
                        <div className={"node-table-item-column"}>
                            <span className={"node-table-item "} >
                                <Translate className={"node-table-item-title"} content={"node.table.accrued"}/>
                                <FormattedAsset decimalOffset={4} amount={props.assetAmount.getAmount({real: false})} asset={"1.3.0"} />
                            </span>
                            <span className={cn("node-table-item close", {"down": isShowContent})} onClick={()=>this.setState({isShowContent : !isShowContent})} />
                        </div>
                    </div>
                </div>
                <div className={cn("node-table-row-bottom operation", {"hide": !isShowContent})}>
                    <div className={"node-table-row"}>
                        <span className={"node-table-item inline"} >
                            <Translate className={"node-table-item-title"} content={"node.table.date_added"}/>
                            {props.eventDT ? moment(props.eventDT).add(timeZone, "hour").format("DD.MM.YYYY, HH:mm") : "-" }
                        </span>
                    </div>
                    <div className={"node-table-row"}>
                        <span className={"node-table-item inline"} >
                            <Translate className={"node-table-item-title"} content={"node.table.description_transaction"}/>
                            <Translate 
                                unsafe={true}
                                className={"node-table-item-value"} 
                                content={["node","table", "transaction", props.type]} 
                                with={{
                                    name: props.withdrawal_account || ""
                                }} />
                        </span>
                    </div>
                </div>
            </div>
        );
    }
}
const NodeOperationsDesktop = props => {
    let d = new Date();
    let timeZone = -d.getTimezoneOffset()/60;

    let clnProperty = {
        notCompete: !props.state.complete,
        isActive: props.state.complete && !props.state.failed,
        isNotActive: props.state.complete && props.state.failed
    };

    return (
        <div className={"node-table-row"}>
            <span className={"node-table-item node_name"} >
                <i className={cn("node-table-status", clnProperty)} />
                {props.node_name}
            </span>
            <span className={"node-table-item date_added"} >
                {props.eventDT && (moment(props.eventDT).add(timeZone, "hour").format("DD.MM.YYYY, HH:mm")) }
            </span>

            <span className={"node-table-item "} >
                <FormattedAsset decimalOffset={4} amount={props.assetAmount.getAmount({real: false})} asset={"1.3.0"}/>
            </span>
            <Translate 
                unsafe={true}
                className={"node-table-item accrued"} 
                content={["node","table", "transaction", props.type]} 
                with={{
                    name: props.withdrawal_account || ""
                }} />
        </div>
    );
};

export {
    NodeOperationsMobile,
    NodeOperationsDesktop
};

import React from "react";
import cn from "classnames";
import moment from "moment";
import Translate from "react-translate-component";
import FormattedAsset from "Components/Utility/FormattedAsset";
import Icon from "Components/Icon/Icon";

class NodeItemDesktop extends React.Component {
    state = {
        isShowContent: false
    };
    render() {
        const {props} = this;
        const {isShowContent} = this.state;
        let isWithdrawn =
            props.formatDate &&
            props.formatDate.isWithdrawn &&
            props.assetWithdrawn.hasAmount();
        let pingsNodes = props.pingsNodes[props.name];
        let isCurrentActiveNode =
            pingsNodes && pingsNodes.url === props.settings.get("activeNode");
        return (
            <div className={"node-table-rows"}>
                <div className={"node-table-row-top"}>
                    <div className={"node-table-main"}>
                        <div className={"node-table-main-row"}>
                            <span className={"node-table-item-name"}>
                                <i
                                    className={cn(
                                        "node-table-status",
                                        props.state
                                    )}
                                />

                                <span
                                    className={"node-table-node-name"}
                                    style={{}}
                                >
                                    {props.name}
                                    <br />
                                </span>
                            </span>
                            {pingsNodes ? (
                                <span
                                    style={{
                                        marginLeft: "auto",
                                        marginRight: "10px",
                                        fontSize: "16px"
                                    }}
                                    className={cn(
                                        "node-table-item-ping",
                                        pingsNodes.status
                                    )}
                                >
                                    {pingsNodes.value} ms
                                </span>
                            ) : null}
                            {pingsNodes ? (
                                <Icon
                                    onClick={() =>
                                        props.activateNode(
                                            props.name,
                                            props.url
                                        )
                                    }
                                    name={
                                        isCurrentActiveNode
                                            ? "link-right-active"
                                            : "link-right"
                                    }
                                />
                            ) : null}
                        </div>
                        <div className={"node-table-main-row"}>
                            <span className={"node-table-item node-last"}>
                                {props.added
                                    ? props.formatLastDate.lastDate
                                    : "-"}
                            </span>
                            <span className={"node-table-item deposit"}>
                                <FormattedAsset
                                    decimalOffset={4}
                                    amount={props.assetWithdrawn.getAmount({
                                        real: false
                                    })}
                                    asset={(__DEEX_CHAIN__ || __SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__) ? "1.3.0" : "1.3.2230"}
                                />
                            </span>
                            <span className={"node-table-item btn-groups"}>
                                {!props.deposit && !props.isDisabled ? (
                                    <React.Fragment>
                                        <Translate
                                            component={"button"}
                                            className={
                                                "btn btn-green btn-add-deposit"
                                            }
                                            onClick={props.addDeposit}
                                            content={"node.table.add_deposit"}
                                        />
                                        <span
                                            className={cn(
                                                "node-table-toggle-show",
                                                {down: isShowContent}
                                            )}
                                            onClick={() =>
                                                this.setState({
                                                    isShowContent: !isShowContent
                                                })
                                            }
                                        />
                                    </React.Fragment>
                                ) : null}
                                {isWithdrawn ? (
                                    <React.Fragment>
                                        <Translate
                                            unsafe
                                            component={"button"}
                                            className={
                                                "btn btn-green btn-take-deposit"
                                            }
                                            onClick={() =>
                                                props.takeDeposit(props)
                                            }
                                            content={"node.table.take_deposit"}
                                        />
                                        <span
                                            className={cn(
                                                "node-table-toggle-show",
                                                {down: isShowContent}
                                            )}
                                            onClick={() =>
                                                this.setState({
                                                    isShowContent: !isShowContent
                                                })
                                            }
                                        />
                                    </React.Fragment>
                                ) : null}
                                {props.deposit && !props.isDisabled && !isWithdrawn ? (
                                    <React.Fragment>
                                        <Translate
                                            component={"button"}
                                            className={
                                                "btn btn-gray btn-stop-nodes-take"
                                            }
                                            onClick={props.disableDeposit}
                                            content={
                                                "node.table.stop_take_deposit"
                                            }
                                        />
                                        <span
                                            className={cn(
                                                "node-table-toggle-show",
                                                {down: isShowContent}
                                            )}
                                            onClick={() =>
                                                this.setState({
                                                    isShowContent: !isShowContent
                                                })
                                            }
                                        />
                                    </React.Fragment>
                                ) : null}
                            </span>
                        </div>
                    </div>
                </div>
                <div
                    className={cn("node-table-row-bottom", {
                        hide: !isShowContent
                    })}
                >
                    <div className={"node-table-row start"}>
                        <span className={"node-table-item auto"}>
                            <Translate
                                className={"node-table-item-title"}
                                content={"node.table.date_added"}
                            />
                            {props.added
                                ? moment(props.added).format(
                                      "DD.MM.YYYY, HH:mm"
                                  )
                                : "-"}
                        </span>
                        <span className={"node-table-item auto"}>
                            <Translate
                                className={"node-table-item-title"}
                                content={"node.table.date_withdrawn"}
                            />
                            {props.added
                                ? props.dateWithdrow.format("DD.MM.YYYY, HH:mm")
                                : "-"}
                        </span>
                        <span className={"node-table-item auto"}>
                            <Translate
                                className={"node-table-item-title"}
                                content={"node.table.deposit"}
                            />
                            <FormattedAsset
                                decimalOffset={4}
                                amount={props.assetDeposit.getAmount({
                                    real: false
                                })}
                                asset={(__DEEX_CHAIN__ || __SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__) ? "1.3.0" : "1.3.2230"}
                            />
                        </span>
                    </div>
                </div>
            </div>
        );
    }
}

class NodeItemMobile extends React.Component {
    state = {
        isShowContent: false
    };
    render() {
        const {props} = this;
        const {isShowContent} = this.state;
        let isWithdrawn = props.formatDate && props.formatDate.isWithdrawn;
        return (
            <div className={"node-table-rows"}>
                <div className={"node-table-row-top"}>
                    <div className={"node-table-row"}>
                        <div className={"node-table-item-column"}>
                            <span className={"node-table-item-top"}>
                                <i
                                    className={cn(
                                        "node-table-status",
                                        props.state
                                    )}
                                />
                                {props.name}
                            </span>
                        </div>
                        <div className={"node-table-item-column"}>
                            <span className={"node-table-item deposit"}>
                                <Translate
                                    className={"node-table-item-title"}
                                    content={"node.table.accrued"}
                                />
                                <FormattedAsset
                                    decimalOffset={4}
                                    amount={props.assetDeposit.getAmount({
                                        real: false
                                    })}
                                    asset={(__DEEX_CHAIN__ || __SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__) ? "1.3.0" : "1.3.2230"}
                                />
                            </span>
                            <span
                                className={cn("node-table-item close", {
                                    down: isShowContent
                                })}
                                onClick={() =>
                                    this.setState({
                                        isShowContent: !isShowContent
                                    })
                                }
                            />
                        </div>
                    </div>
                </div>
                <div
                    className={cn("node-table-row-bottom", {
                        hide: !isShowContent
                    })}
                >
                    <div className={"node-table-row"}>
                        <span className={"node-table-item date_added"}>
                            <Translate
                                className={"node-table-item-title"}
                                content={"node.table.date_added"}
                            />
                            {props.added
                                ? moment(props.added).format("DD.MM.YYYY")
                                : "-"}
                        </span>
                        <span className={"node-table-item date_added"}>
                            <Translate
                                className={"node-table-item-title"}
                                content={"node.table.date_withdrawn"}
                            />
                            {props.added
                                ? props.dateWithdrow.format("DD.MM.YYYY")
                                : "-"}
                        </span>
                    </div>
                    <div className={"node-table-row"}>
                        <span className={"node-table-item date_added"}>
                            <Translate
                                className={"node-table-item-title"}
                                content={"node.table.node_work"}
                            />
                            {props.added
                                ? props.formatLastDate.stringDate
                                : "-"}
                        </span>
                        <span className={"node-table-item deposit"}>
                            <Translate
                                className={"node-table-item-title"}
                                content={"node.table.deposit"}
                            />
                            <FormattedAsset
                                decimalOffset={4}
                                amount={props.assetDeposit.getAmount({
                                    real: false
                                })}
                                asset={(__DEEX_CHAIN__ || __SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__) ? "1.3.0" : "1.3.2230"}
                            />
                        </span>
                    </div>
                    <div className={"node-table-row"}>
                        {!props.deposit && !props.isDisabled ? (
                            <Translate
                                component={"button"}
                                className={"btn btn-green"}
                                onClick={props.addDeposit}
                                content={"node.table.add_deposit"}
                            />
                        ) : null}
                        {isWithdrawn ? (
                            <Translate
                                component={"button"}
                                className={"btn btn-green"}
                                onClick={() =>
                                    props.takeDeposit(props.assetWithdrawn)
                                }
                                content={"node.table.take_deposit"}
                            />
                        ) : null}
                        {props.deposit && !props.isDisabled && !isWithdrawn ? (
                            <Translate
                                component={"button"}
                                className={"btn btn-red"}
                                onClick={props.disableDeposit}
                                content={"node.stop_nodes"}
                            />
                        ) : null}
                    </div>
                </div>
            </div>
        );
    }
}

export {NodeItemDesktop, NodeItemMobile};

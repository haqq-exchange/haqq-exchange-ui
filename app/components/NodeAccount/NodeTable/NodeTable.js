import React from "react";
import Translate from "react-translate-component";

const NodeHead = () => {
    return (
        <div className="node-table-head">
            <div className="node-table-head-row">
                <Translate className={"node-table-item node_name"} content={"node.table.node_name"}/>

            </div>
            <div className="node-table-head-row">
                {/*<span className={"node-table-item status"} />*/}
                {/*<Translate className={"node-table-item date_added"} content={"node.table.date_added"}/>*/}
                {/*<Translate className={"node-table-item date_added"} content={"node.table.date_withdrawn"}/>*/}
                <Translate className={"node-table-item node_work"} content={"node.table.node_work"}/>
                {/*<Translate className={"node-table-item deposit"} content={"node.table.deposit"}/>*/}
                <Translate className={"node-table-item accrued"} content={"node.table.accrued"}/>
                <span className={"node-table-item btns"} >
                    <span className="btn  v-hidden" />
                    <span className="node-table-close v-hidden" />
                </span>
            </div>
        </div>
    );
};

const NodeHeadOperation = () => {
    return (
        <div className="node-table-head">
            <Translate className={"node-table-item node_name"} content={"node.table.node_name"}/>
            <Translate className={"node-table-item date_added"} content={"node.table.date_transaction"}/>
            <Translate className={"node-table-item date_added"} content={"node.table.amount_transaction"}/>
            <Translate className={"node-table-item node_work"} content={"node.table.description_transaction"}/>
        </div>
    );
};

export  {
    NodeHead,
    NodeHeadOperation
};
import React, {useState, useEffect} from "react";
import Translate from "react-translate-component";
import Icon from "Components/Icon/Icon";
import cln from "classnames";
import {Input} from "antd";

const ChangeAccountName = ({name, setNameWithdraw, validName}) => {
    const [isEdit, setEdit] = useState(false);
    const [isChangName, setChangeName] = useState(false);
    const [accountName, setAccountName] = useState(name);

    useEffect(() => {
        if( !name ) { setChangeName(true); }
    }, [accountName]);
    useEffect(() => {
        if( name && isEdit) {
            console.log('>>>>> 1');
            setNameWithdraw(null, true);
        }
        if( !name ) {
            if( isChangName ) {
                console.log('>>>>> 2');
                setNameWithdraw(accountName, isChangName);
            } else {
                console.log('>>>>> 3');
                setNameWithdraw(null, !name); //false
            }
        }
        
    }, [isEdit]);

    if (isEdit) {
        return (
            <div className={cln("info-node-editable-input")}>
                <Input
                    value={accountName}
                    
                    onChange={event => setAccountName(event.target.value)}
                    addonAfter={
                        <button onClick={() => setEdit(false)}>
                            <Icon name="send" />
                        </button>
                    }
                />
            </div>
        );
    } else {
        return (
            <div
                onClick={() => setEdit(true)}
                className={cln("info-node-editable-text", {
                    positive: validName,
                    negative: !validName
                })}
            >
                <span>{accountName}{" "}</span>
                <Icon name="pencil-alt"  />
                {!validName ? (
                    <Translate
                        component={"div"}
                        className={"info-node-text-error"}
                        content={"form.message.not_account_name"}
                    />
                ) : null}
            </div>
        );
    }
};

export default ChangeAccountName;

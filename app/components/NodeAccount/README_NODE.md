# Nodes Backend API

***Get nodes info***

  Gets nodes info for the selected account

- **URL**

  /nodes/get_info

- **Method:**

  `POST`

- **URL Params**

  None

- **Data Params**

  * `data: {"account":"user01"}`
  * `sign: IEGlzsdJ3UpHb7...Rk=`

* **Request Headers**

  * `Content-Type: application/x-www-form-urlencoded`

- **Success Response:**

  **Code:** 200 <br />
  **Content:** 

  ```json
  {
    "info": {
        "deposit_account": "deex-nodes", 
        "new_node_request_allowed": false, 
        "withdrawal_account": "somebody"
    },
    "nodes": [
        {
            "name": "the-asd-node",
            "state": "disabled",
            "added": "",
            "disabled": "05.02.2019 13:35",
            "deposit": 100,
            "price": 100,
            "annual_interest": 20,
            "penalty": 10,
            "withdrawable": 0,
            "withdrawn": 0
        },
        {
            "name": "the-qwe-node",
            "state": "active",
            "added": "",
            "disabled": "",
            "deposit": 100,
            "price": 100,
            "annual_interest": 20,
            "penalty": 10,
            "withdrawable": 0,
            "withdrawn": 0
        },
        {
            "name": "the-zxc-node",
            "state": "disabled",
            "added": "",
            "disabled": "05.02.2019 14:38",
            "deposit": 100,
            "price": 100,
            "annual_interest": 20,
            "penalty": 10,
            "withdrawable": 0,
            "withdrawn": 120
        }
    ],
    "operations": [
        {
            "eventDT": "2019-02-05 08:58:05.277562",
            "type": "withdrawal",
            "currency": "DEEX",
            "amount": 13.25,
            "withdrawal_account": "somebody",
            "state": {
                "name": "Initial",
                "complete": false,
                "failed": false
            }
        },
        {
            "eventDT": "2019-02-05 14:38:10.665724",
            "type": "withdrawal",
            "currency": "DEEX",
            "amount": 120,
            "withdrawal_account": "asdqwe",
            "state": {
                "name": "Succeeded",
                "complete": true,
                "failed": false
            }
        },
        {
            "eventDT": "2019-02-05 09:10:14.483712",
            "type": "deposit",
            "currency": "DEEX",
            "amount": 42,
            "state": {
                "name": "Confirmed",
                "complete": true,
                "failed": false
            }
        }
    ]
  }
  ```

- **Error Responses:**

| Code          | Content           |
| ------------- |:-------------|
| 500      | `{ error : "Internal server error" }`|
| 400      | `{ error : "Bad request data" }`      |
| 401 | `{ error : "Wrong signature" }`     |    
| 403 | `{ error : "Account has not been registered by DEEX" }`|
| 404 | `{ error : "Account does not exist" }` |
| 418 | `{ error : "Account does not participate in the partnership program" }`|



***Get current node price***

  Gets current nodes price

- **URL**

  /nodes/get_current_price

- **Method:**

  `POST`

- **URL Params**

  None

- **Data Params**

  * `data: {"account":"user01"}`
  * `sign: IEGlzsdJ3UpHb7...Rk=`

* **Request Headers**

  * `Content-Type: application/x-www-form-urlencoded`

- **Success Response:**

  **Code:** 200 <br />
  **Content:** 

  ```json
  {
    "price": 100,
    "interest": 20,
    "penalty": 10
  }
  ```

- **Error Responses:**

| Code          | Content           |
| ------------- |:-------------|
| 500      | `{ error : "Internal server error" }`|
| 400      | `{ error : "Bad request data" }`      |
| 401 | `{ error : "Wrong signature" }`     |    
| 403 | `{ error : "Account has not been registered by DEEX" }`|
| 404 | `{ error : "Account does not exist" }` |
| 418 | `{ error : "Account does not participate in the partnership program" }`|


***Request a new node***

  Requests a new node

- **URL**

  /nodes/request_node

- **Method:**

  `POST`

- **URL Params**

  None

- **Data Params**

  * `data: {"account":"user01","node_name":"node-asd"}`
  * `sign: IEGlzsdJ3UpHb7...Rk=`

* **Request Headers**

  * `Content-Type: application/x-www-form-urlencoded`

- **Success Response:**

  **Code:** 200 <br />
  **Content:** 

  ```json
  {}
  ```
  
  or
  
  **Code:** 409 <br />
  **Content:** 

  ```json
  {"error": "Node name already registered"}
  ```

- **Error Responses:**

| Code          | Content           |
| ------------- |:-------------|
| 500      | `{ error : "Internal server error" }`|
| 400      | `{ error : "Bad request data" }`      |
| 401 | `{ error : "Wrong signature" }`     |    
| 403 | `{ error : "Account has not been registered by DEEX" }`|
| 404 | `{ error : "Account does not exist" }` |
| 418 | `{ error : "Account does not participate in the partnership program" }`|



***Disable a node***

  Disables a node, and withdraws the deposited amount minus the penalty to the user's account

- **URL**

  /nodes/disable_node

- **Method:**

  `POST`

- **URL Params**

  None

- **Data Params**

  * `data: {"account":"user01","node_name":"node-asd"}`
  * `sign: IEGlzsdJ3UpHb7...Rk=`

* **Request Headers**

  * `Content-Type: application/x-www-form-urlencoded`

- **Success Response:**

  **Code:** 200 <br />
  **Content:** 

  ```json
  {}
  ```

- **Error Responses:**

| Code          | Content           |
| ------------- |:-------------|
| 500      | `{ error : "Internal server error" }`|
| 400      | `{ error : "Bad request data" }`      |
| 401 | `{ error : "Wrong signature" }`     |    
| 403 | `{ error : "Account has not been registered by DEEX" }`|
| 404 | `{ error : "Account does not exist" }` |
| 418 | `{ error : "Account does not participate in the partnership program" }`|



***Initiate a withdrawal***

  Initiates a withdrawal from the selected fulfilled node to the user's account.  
  After that, the node becomes disabled.

- **URL**

  /nodes/initiate_withdrawal

- **Method:**

  `POST`

- **URL Params**

  None

- **Data Params**

  * `data: {"account":"user01","node_name":"node-asd"}`
  * `sign: IEGlzsdJ3UpHb7...Rk=`

* **Request Headers**

  * `Content-Type: application/x-www-form-urlencoded`

- **Success Response:**

  **Code:** 200 <br />
  **Content:** 

  ```json
  {}
  ```
  
- **Error Responses:**

| Code          | Content           |
| ------------- |:-------------|
| 500      | `{ error : "Internal server error" }`|
| 400      | `{ error : "Bad request data" }`      |
| 401 | `{ error : "Wrong signature" }`     |    
| 403 | `{ error : "Account has not been registered by DEEX" }`|
| 404 | `{ error : "Account does not exist" }` |
| 418 | `{ error : "Account does not participate in the partnership program" }`|



***Set DEEX account name for withdrawals***

  Checks account name for existance and sets it for future withdrawals

- **URL**

  /nodes/set_withdrawal_account

- **Method:**

  `POST`

- **URL Params**

  None

- **Data Params**

  * `data: {"account":"user01","withdrawal_account":"deex-user01"}`
  * `sign: IEGlzsdJ3UpHb7...Rk=`

* **Request Headers**

  * `Content-Type: application/x-www-form-urlencoded`

- **Success Response:**

  **Code:** 200 <br />
  **Content:** 

  ```json
  {}
  ```
  
- **Error Responses:**

| Code          | Content           |
| ------------- |:-------------|
| 500      | `{ error : "Internal server error" }`|
| 400      | `{ error : "Bad request data" }`      |
| 401 | `{ error : "Wrong signature" }`     |    
| 403 | `{ error : "Account has not been registered by DEEX" }`|
| 404 | `{ error : "Account does not exist" }` |
| 418 | `{ error : "Account does not participate in the partnership program" }`|


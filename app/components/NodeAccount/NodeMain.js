import React from "react";
// import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import SettingsStore from "stores/SettingsStore";
import WalletUnlockStore from "stores/WalletUnlockStore";
import GatewayStore from "stores/GatewayStore";
// import ChainTypes from "../Utility/ChainTypes";
import BindToChainState from "../Utility/BindToChainState";
import {connect} from "alt-react";
import accountUtils from "common/account_utils";
// import {List} from "immutable";
// import {Route, Switch, Redirect} from "react-router-dom";
import Loadable from "react-loadable";
import LoadingIndicator from "Components/LoadingIndicator";
// import Translate from "react-translate-component";
//import RoutesLink from "RoutesLink";
//import AccountAssets from "./AccountAssets";
import ModalStore from "stores/ModalStore";
import InfoNodeModal from "./Modals/InfoNodeModal";
import "./NodeStyle.scss";
import RefUtils from "common/ref_utils";
import Pinger from "common/Pinger";
import {getRequestAddress} from "api/apiConfig";
// import Crypto from "../../lib/common/Crypto";

const AuthLoginAsync = Loadable({
    loader: () => import(/* webpackChunkName: "AuthLogin" */ "./AuthLogin"),
    loading: LoadingIndicator
});
const NodeTableAsync = Loadable({
    loader: () => import(/* webpackChunkName: "NodeTable" */ "./NodeTable"),
    loading: LoadingIndicator
});
const AddNewNodeAsync = Loadable({
    loader: () => import(/* webpackChunkName: "AddNewNode" */ "./AddNewNode"),
    loading: LoadingIndicator
});
// const Page404 = Loadable({
//     loader: () => import(/* webpackChunkName: "Page404" */ "Components/Page404/Page404"),
//     loading: LoadingIndicator
// });


class NodeMain extends React.Component {

    static defaultProps = {
        nodeMain: true,
        getUserSign: accountUtils.getUserSign,
        fetchNodes: new RefUtils({
            url: getRequestAddress("nodes")
        }),
        pinger: new Pinger({
            url: "wss://node2.deex.network/ws",
            urls: ["wss://node2.deex.network/ws"],
            closeCb: () => {console.warn("closeCb pinger");},
            urlChangeCallback: () => {console.warn("urlChangeCallback pinger");},
            optionalApis: {enableOrders: true}
        }),
        nodePage: {
            "page_404": AuthLoginAsync,
            "login": AuthLoginAsync,
            "loading": LoadingIndicator,
            "add_node": AddNewNodeAsync,
            "table_node": NodeTableAsync
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            status: "loading"
        };
        this.catchUserSign = this.catchUserSign.bind(this);
    }

    componentDidMount() {
        this.getNodeInfo();
    }

    componentDidUpdate(prevProps) {
        if( prevProps.wallet_locked !== this.props.wallet_locked && !this.props.wallet_locked ) {
            this.getNodeInfo();
        }
    }

    catchUserSign(res) {
        const {status} = this.state;
        console.log("catchUserSign", res);
        if( status !== "login" ) {
            this.setState({status: "login"});
        }

    }


    getNodeInfo = () => {
        const {account_name, fetchNodes} = this.props;

        this.props.getUserSign({account: account_name}).then(sign => {
            fetchNodes
                .postTemplate("/get_info", {
                    account: account_name
                }, sign)
                .then(({content})=>{
                    this.setState({
                        status: content.nodes.length ? "table_node" : "add_node",
                        content
                    });
                }).catch(this.catchUserSign);
        }, this.catchUserSign);
    };

    setNodeStatus = (status) => {
        this.setState({status}, this.getNodeInfo);
    };


    render() {
        let { status , content } = this.state;
        let { nodePage, settings} = this.props;

        /*if( !wallet_name || !passwordAccount ) {
            status = "page_404";
        }*/
        if( !status ) status = "login";

        if( !settings.get("is_service_node") ) {
            status = "page_404";
        }

        let ComponentLoaded = nodePage[status];

        return (
            <React.Fragment>
                <ComponentLoaded
                    catchUserSign={this.catchUserSign}
                    setNodeStatus={this.setNodeStatus}
                    content={content}
                    asset={"1.3.0"}
                    {...this.props} />
                <InfoNodeModal {...this.props} />
            </React.Fragment>
        );
    }
}
const NodeMainBind = BindToChainState(NodeMain, {});

class NodeMainStoreWrapper extends React.Component {
    render() {

        return <NodeMainBind {...this.props}  />;
    }
}

export default connect(
    NodeMainStoreWrapper,
    {
        listenTo() {
            return [
                AccountStore,
                SettingsStore,
                WalletUnlockStore,
                ModalStore,
                GatewayStore
            ];
        },
        getProps() {
            return {
                settings: SettingsStore.getState().settings,
                viewSettings: SettingsStore.getState().viewSettings,
                wallet_locked: WalletUnlockStore.getState().locked,
                passwordAccount: AccountStore.getState().passwordAccount,
                wallet_name: AccountStore.getState().wallet_name,
                account_name: AccountStore.getUserAccountName()
            };
        }
    }
);

import React from "react";
import moment from "moment";
import {Checkbox} from "antd";
import {Asset} from "common/MarketClasses";
import Translate from "react-translate-component";
import stringFormatEn from "./string-format/en";
import stringFormatRu from "./string-format/ru";
import stringFormatTr from "./string-format/tr";

import AddNewNode from "./AddNewNode";
import AddNodeModal from "./Modals/AddNodeModal";
import DisabledNodeModal from "./Modals/DisabledNodeModal";
import DeleteNodeModal from "./Modals/DeleteNodeModal";
import WithdrawnNodeModal from "./Modals/WithdrawnNodeModal";

import {NodeItemDesktop, NodeItemMobile} from "./NodeTable/NodeItem";
import {
    NodeOperationsMobile,
    NodeOperationsDesktop
} from "./NodeTable/NodeOperations";
import {NodeHead, NodeHeadOperation} from "./NodeTable/NodeTable";

import ModalActions from "actions/ModalActions";
import BindToChainState from "Components/Utility/BindToChainState";
import ChainTypes from "Components/Utility/ChainTypes";

import MediaQuery from "react-responsive";
import ResponseOption from "config/response";
import SettingsActions from "actions/SettingsActions";
import willTransitionTo from "../../routerTransition";

class NodeTable extends React.Component {
    static propTypes = {
        asset: ChainTypes.ChainAsset.isRequired
    };

    state = {
        pingsNodes: {},
        isValidName: false,
        withdraw_account: null
    };

    componentDidUpdate(prevProps) {
        // const { pingsNodes } = this.state;
        const {content} = this.props;
        const {content: prevContent} = prevProps;
        if (content.nodes !== prevContent.nodes) {
            this.pingNodes();
        }
        if (content.info !== prevContent.info) {
            this.setContentInfo();
        }
    }

    componentDidMount() {
        this.pingNodes();
        this.setContentInfo();
    }

    setContentInfo = () => {
        const {content} = this.props;
        if (content.info.withdrawal_account) {
            console.log("isValidName >>> ", !!content.info.withdrawal_account, content.info.withdrawal_account);
            this.setState({
                isValidName: !!content.info.withdrawal_account,
                withdraw_account: content.info.withdrawal_account
            });
        }
    };

    getActiveNodes = () => {
        const {content} = this.props;
        return content.nodes
            .map(node => (node.state === "active" ? node : null))
            .filter(node => node);
    };

    pingNodes = () => {
        const _this = this;
        const {pinger} = this.props;
        let nodes = this.getActiveNodes();

        pinger
            .addNodes(
                nodes.map(
                    node => node.url
                ) /*.concat(["wss://node1.deex.network/ws"])*/
            )
            .then(() => pinger.start(_this.setPing));
    };

    setPing = result => {
        let {pingsNodes} = this.state;
        let nodes = this.getActiveNodes();
        nodes.forEach(node => {
            console.log("pingNodes node", node, result);
            if (result.url === node.url) {
                console.log(
                    "pingsNodes Object.assign",
                    Object.assign(pingsNodes, {[node.name]: result})
                );

                this.setState({
                    pingsNodes: Object.assign(pingsNodes, {[node.name]: result})
                });
            }
        });
    };

    getValidDeexName = (name, countReload = 0) => {
        const _this = this;
        let requestData = {
            jsonrpc: "2.0",
            method: "get_account_by_name",
            params: [name],
            id: 1
        };
        try {
            let data = fetch("https://node2p.deexnet.com/ws", {
                method: "POST",
                body: JSON.stringify(requestData)
            });
            data.then(result => result.json())
                .then(data => {
                    const isValidName = !!data.result;
                    console.log('isValidName: !!data.result ', !!data.result);
                    _this.setState({
                        isValidName,
                        withdraw_account: name
                    });

                    if (isValidName) {
                        this.setWithdrawalAccount(name);
                    }
                })
                .catch(() => {
                    if (countReload <= 5) {
                        setTimeout(
                            () => _this.getValidDeexName(name, countReload++),
                            1000
                        );
                    }
                });
        } catch (e) {
            console.error(e);
        }
    };

    setNameWithdraw = async (name, isEditField) => {
        console.log('isEditField setNameWithdraw ', name, isEditField);
        this.setState({
            isValidName: !isEditField,
            withdraw_account: name
        });
        if (name) {
            return await this.getValidDeexName(name);
        }
    };

    setWithdrawalAccount = async name => {
        const {account_name, fetchNodes} = this.props;
        //return await this.getValidDeexName(name);
        // set_withdrawal_account
        this.props
            .getUserSign({
                account: account_name,
                withdrawal_account: name
            })
            .then(sign => {
                fetchNodes.postTemplate(
                    "/set_withdrawal_account",
                    {
                        account: account_name,
                        withdrawal_account: name
                    },
                    sign
                );
            }, this.props.catchUserSign);
    };

    render() {
        const {withdraw_account, isValidName} = this.state;
        const {content, asset} = this.props;

        return (
            <div className={"node"}>
                <div className="node-block">
                    <div className={"node-block-wrap"}>
                        <Translate
                            className={"node-page-title"}
                            component={"div"}
                            content={"node.title"}
                        />
                        <div className="node-wrap">
                            <Translate
                                className={"node-sub-title"}
                                component={"div"}
                                content={"node.title_my_node"}
                            />

                            <div className="node-table">
                                <MediaQuery {...ResponseOption.notMobile}>
                                    <NodeHead />
                                </MediaQuery>
                                <div className="node-table-body">
                                    {content &&
                                        content.nodes.map((node, index) => {
                                            if (
                                                !this.state.hiddenDisabled ||
                                                (this.state.hiddenDisabled &&
                                                    [
                                                        "disabled",
                                                        "disabling"
                                                    ].indexOf(node.state) ===
                                                        -1)
                                            ) {
                                                return (
                                                    <NodeItemBind
                                                        key={[
                                                            "nodes",
                                                            node.name,
                                                            index
                                                        ].join("_")}
                                                        account_name={
                                                            this.props
                                                                .account_name
                                                        }
                                                        info={content.info}
                                                        isValidName={
                                                            isValidName
                                                        }
                                                        withdraw_account={
                                                            withdraw_account
                                                        }
                                                        settings={
                                                            this.props.settings
                                                        }
                                                        setNodeStatus={
                                                            this.props
                                                                .setNodeStatus
                                                        }
                                                        getUserSign={
                                                            this.props
                                                                .getUserSign
                                                        }
                                                        fetchNodes={
                                                            this.props
                                                                .fetchNodes
                                                        }
                                                        pingsNodes={
                                                            this.state
                                                                .pingsNodes
                                                        }
                                                        asset={asset}
                                                        {...node}
                                                    />
                                                );
                                            }
                                        })}
                                    <div className={"node-table-row end"}>
                                        <Checkbox
                                            name={"hideNull"}
                                            onChange={event =>
                                                this.setState({
                                                    hiddenDisabled:
                                                        event.target.checked
                                                })
                                            }
                                            checked={this.state.hiddenDisabled}
                                        >
                                            <Translate content="node.table.hidden_disabled" />
                                        </Checkbox>
                                    </div>
                                </div>
                                {content && content.info.new_node_request_allowed ? (
                                    <AddNewNode
                                        {...this.props}
                                        nodeMain={false}
                                    />
                                ) : null}
                            </div>
                        </div>

                        <div className="node-wrap opacity">
                            <Translate
                                className={"node-sub-title"}
                                component={"div"}
                                content={"node.table.title_history"}
                            />

                            <div className="node-table">
                                <MediaQuery {...ResponseOption.notMobile}>
                                    <NodeHeadOperation />
                                </MediaQuery>
                                <div className="node-table-body">
                                    { content && content.operations.map((operation, index) => {
                                        return <NodeOperationsBind
                                            key={"operation-" + operation.name + index}
                                            asset={(__DEEX_CHAIN__ || __SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__) ? '1.3.0' : '1.3.2230'}
                                            settings={this.props.settings}
                                            {...operation}  />;
                                    }) }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <AddNodeModal {...this.props} />
                <DisabledNodeModal
                    isValidName={isValidName}
                    withdraw_account={withdraw_account}
                    setNameWithdraw={(name, isEdit) => this.setNameWithdraw(name, isEdit)}
                    {...this.props}
                />
                <DeleteNodeModal {...this.props} />
                <WithdrawnNodeModal
                    isValidName={isValidName}
                    withdraw_account={withdraw_account}
                    setNameWithdraw={(name, isEdit) => this.setNameWithdraw(name, isEdit)}
                    {...this.props}
                />
            </div>
        );
    }
}

class NodeOperations extends React.Component {
    static propTypes = {
        asset: ChainTypes.ChainAsset.isRequired
    };

    render() {
        const {asset, amount} = this.props;

        let assetAmount = new Asset({
            precision: asset.get("precision"),
            asset_id: asset.get("id")
        });

        assetAmount.setAmount({real: amount});

        return (
            <MediaQuery {...ResponseOption.mobile}>
                {matches => {
                    if (matches) {
                        return (
                            <NodeOperationsMobile
                                assetAmount={assetAmount}
                                {...this.props}
                            />
                        );
                    } else {
                        return (
                            <NodeOperationsDesktop
                                assetAmount={assetAmount}
                                {...this.props}
                            />
                        );
                    }
                }}
            </MediaQuery>
        );
    }
}

class NodeItemBind extends React.Component {
    /*static propTypes = {
        asset: ChainTypes.ChainAsset.isRequired
    };*/

    static defaultProps = {
        stringFormats: {
            ru: stringFormatRu,
            en: stringFormatEn,
            tr: stringFormatTr
        }
    };

    getFormatDate = (date, endDate) => {
        const {settings, stringFormats} = this.props;
        const stringsFormat = stringFormats[settings.get("locale")];
        let d = new Date();
        let timeZone = -d.getTimezoneOffset() / 60;

        let dateMoment = moment(date).add(timeZone, "hour");
        let endDateMoment = moment(endDate).add(timeZone, "hour");
        let diffYears = endDateMoment.diff(dateMoment, "years");
        let diffMonth = endDateMoment.diff(dateMoment, "month");
        let diffDays = endDateMoment.diff(dateMoment, "days");
        let diffHour = endDateMoment.diff(dateMoment, "hours");
        let diffMinute = endDateMoment.diff(dateMoment, "minutes");

        let dates = [];
        let datesLast = [];

        // console.log("FormatDate timeZone", timeZone);
        // console.log("FormatDate new Date(date)", date);
        // console.log("FormatDate new Date(endDate)", endDate);
        // console.log("FormatDate dateMoment", dateMoment.add(timeZone, "hour"));
        // console.log("FormatDate dateMoment timeZone", dateMoment.utcOffset(timeZone));
        // console.log("FormatDate endDateMoment", endDateMoment.add(timeZone, "hour"));
        // console.log("FormatDate endDateMoment", endDateMoment.utcOffset(timeZone));
        // console.log("FormatDate diffYears", diffYears);
        // console.log("FormatDate diffMonth", diffMonth);
        // console.log("FormatDate diffDays", diffDays);

        // console.log("FormatDate diffHour", diffHour);
        // console.log("FormatDate diffMinute", diffMinute);

        let hour = diffHour - diffDays * 24;
        if (diffYears > 0) {
            let month = diffMonth - diffYears * 12;
            dates.push(
                stringsFormat.years(diffYears).replace(/%d/g, diffYears)
            );
            if (month > 0) {
                dates.push(stringsFormat.months(month).replace(/%d/g, month));
            }
        } else if (!diffYears && diffMonth > 0) {
            dates.push(stringsFormat.days(diffDays).replace(/%d/g, diffDays));
            dates.push(stringsFormat.hours(hour).replace(/%d/g, hour));
        } else if (diffMinute > 0) {
            let minute = diffMinute - diffHour * 60;
            dates.push(stringsFormat.hours(diffHour).replace(/%d/g, diffHour));
            dates.push(stringsFormat.minutes(minute).replace(/%d/g, minute));
        }

        if (diffDays > 0) {
            datesLast.push(
                stringsFormat.days(diffDays).replace(/%d/g, diffDays)
            );
            datesLast.push(stringsFormat.hours(hour).replace(/%d/g, hour));
        }

        // console.log("datesLast", datesLast);
        // console.log("dates", dates);

        return {
            stringDate: dates.join(" "),
            lastDate: datesLast.join(" "),
            isWithdrawn: diffYears > 0,
            diffDays
        };
    };

    addDeposit = () => {
        const {name, node_price} = this.props;
        ModalActions.show("add_deposit_node_modal", {
            node_name: name,
            node_price
        }).then(() => {
            ModalActions.hide("add_deposit_node_modal");
        });
    };

    getAccruedVirtual = (lastDate, allDate) => {
        const {state, deposit, node_price} = this.props;
        if (["fulfilled", "active"].indexOf(state) !== -1) {
            let sumPercent = (deposit / 100) * node_price.annual_interest;
            return (sumPercent / allDate.diffDays) * lastDate.diffDays;
        }
        return 0;
    };

    takeDeposit = assetWithdrawn => {
        //debugger;
        const {name} = this.props;

        ModalActions.show("withdrawn_node_modal", {
            content: {
                name,
                ...assetWithdrawn
            }
        }).then(() => this.sendInitiateWithdrawal());
    };

    sendInitiateWithdrawal = () => {
        const {fetchNodes, name, account_name} = this.props;

        ModalActions.hide("disabled_node_modal");

        this.props
            .getUserSign({
                account: account_name,
                node_name: name
            })
            .then(sign => {
                fetchNodes
                    .postTemplate(
                        "/initiate_withdrawal",
                        {
                            account: account_name,
                            node_name: name
                        },
                        sign
                    )
                    .then(() => {
                        this.props.setNodeStatus("loading");
                    });
            }, this.props.catchUserSign);
    };

    disableDeposit = () => {
        const {
            account_name,
            fetchNodes,
            name,
            node_price,
            deposit
        } = this.props;
        let nameModal = deposit ? "disabled_node_modal" : "delete_node_modal";
        ModalActions.show(nameModal, {
            content: node_price
        }).then(() => {
            ModalActions.hide("disabled_node_modal");
            this.props
                .getUserSign({account: account_name, node_name: name})
                .then(sign => {
                    fetchNodes
                        .postTemplate(
                            "/disable_node",
                            {
                                account: account_name,
                                node_name: name
                            },
                            sign
                        )
                        .then(() => {
                            this.props.setNodeStatus("loading");
                        });
                }, this.props.catchUserSign);
        });
    };

    activateNode = (name, url) => {
        SettingsActions.addWS({
            location: name,
            url: url
        });

        SettingsActions.changeSetting({
            setting: "apiServer",
            value: url
        });

        setTimeout(() => {
            willTransitionTo(false);
        }, 50);
    };

    render() {
        const {
            added,
            asset,
            node_price,
            deposit,
            withdrawable,
            state
        } = this.props;

        let dateWithdrow, formatDate, endDate, formatLastDate;
        if (added) {
            dateWithdrow = moment(added);
            dateWithdrow.add(node_price.period_months, "months");

            // console.log("FormatDate added", added, moment(added).format() );
            // console.log("FormatDate added", dateWithdrow, new Date(dateWithdrow) );
            // console.log("FormatDate new Date()", new Date());

            formatDate = this.getFormatDate(
                moment(added).format(),
                moment().format()
            );
            endDate = this.getFormatDate(
                moment(added).format(),
                dateWithdrow.format()
            );
            formatLastDate = this.getFormatDate(
                moment().format(),
                dateWithdrow.format()
            );
        }
        // const asset = ChainStore.getAsset("1.3.0");

        let assetDeposit = new Asset({
                precision: asset.get("precision"),
                asset_id: asset.get("id")
            }),
            assetWithdrawn = assetDeposit.clone();
        if (deposit) {
            assetDeposit.setAmount({real: deposit});
            if (!withdrawable) {
                assetWithdrawn.setAmount({
                    real: this.getAccruedVirtual(formatDate, endDate)
                });
            } else {
                assetWithdrawn.setAmount({real: withdrawable - deposit});
            }
        }

        let isDisabled = ["disabled", "disabling"].indexOf(state) !== -1;

        // console.log("this.state.pingsNodes", this.props.pingsNodes);
        // console.log("this.state.pingsNodes props", this.props);

        const dataPropsChild = {
            activateNode: this.activateNode,
            addDeposit: this.addDeposit,
            takeDeposit: this.takeDeposit,
            disableDeposit: this.disableDeposit,
            setNameWithdraw: this.setNameWithdraw,
            assetDeposit: assetDeposit,
            assetWithdrawn: assetWithdrawn,
            formatDate: formatDate,
            formatLastDate: formatLastDate,
            dateWithdrow: dateWithdrow,
            isDisabled: isDisabled,
            ...this.props
        };

        return (
            <MediaQuery {...ResponseOption.mobile}>
                {matches => {
                    if (matches) {
                        return <NodeItemMobile {...dataPropsChild} />;
                    } else {
                        return <NodeItemDesktop {...dataPropsChild} />;
                    }
                }}
            </MediaQuery>
        );
    }
}

const NodeOperationsBind = BindToChainState(NodeOperations, {});

export default BindToChainState(NodeTable, {});

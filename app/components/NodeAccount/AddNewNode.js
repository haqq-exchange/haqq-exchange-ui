import React from "react";
import ModalActions from "actions/ModalActions";
import RequestNodeModal from "./Modals/RequestNodeModal";
import Translate from "react-translate-component";

const ConnectNode = (props) => {
    return (
        <React.Fragment>
            <button type={"button"} className={"btn btn-green btn-auth"} onClick={(event)=>props.addNode(event)}>
                <Translate content={"node.connect_node"}/>
            </button>
            <RequestNodeModal {...props}/>
        </React.Fragment>
    );
};

export default class AddNewNode extends React.Component {

    getCurrentPrice = () => {
        const {account_name, fetchNodes} = this.props;

        this.props.getUserSign({account: account_name}).then(sign => {
            fetchNodes
                .postTemplate("/get_current_price", {
                    account: account_name
                }, sign)
                .then(({content})=>{
                    //
                    
                    ModalActions.hide("request_node_modal");
                    this.setState({content}, this.addNode);
                });
        }, this.props.catchUserSign);
    };

    addNode = () => {
        //event.preventDefault();
        const {content} = this.state;
        ModalActions.show("info_node_modal", {content: content}).then(()=>{
            ModalActions.show("request_node_modal")
                .then(this.afterAddNode);
        });
    };

    beforeAddNode = () => {
        const {account_name, fetchNodes} = this.props;
        
        /*this.props.getUserSign({account: account_name}).then(sign => {
            
            fetchNodes
                .postTemplate("/initiate_withdrawal", {
                    account: account_name,
                    node_name: "node_name"
                }, sign)
                .then(()=>{
                    
                });
        }).catch(this.props.catchUserSign);*/
    };
    afterAddNode = () => {
        ModalActions.hide("request_node_modal");
    };

    render(){
        const {nodeMain, content} = this.props;
        

        if( nodeMain ) {
            if( !content.info.new_node_request_allowed ) {
                return(
                    <div className={"node"}>
                        <div className={"node-block"}>
                            <div className={"node-block-add"}>
                                <Translate className={"node-title"} component={"div"} content={"node.title"}/>
                                <Translate className={"node-unlock-info"} component={"div"} content={"node.node_stop"}/>

                            </div>
                        </div>
                    </div>
                );
            } else {
                return(
                    <div className={"node"}>
                        <div className={"node-block"}>
                            <div className={"node-block-add"}>
                                <Translate className={"node-title"} component={"div"} content={"node.title"}/>
                                <Translate className={"node-unlock-info"} component={"div"} content={"node.unlock_info"}/>
                                <ConnectNode {...this.props} addNode={this.getCurrentPrice}/>
                            </div>
                        </div>
                    </div>
                );
            }
        } else {
            return (
                <ConnectNode {...this.props} addNode={this.getCurrentPrice}/>
            );
        }
    }

}


import React from "react";
import Translate from "react-translate-component";
import {Link} from "react-router-dom";
import {ChainStore} from "deexjs";
import {BackLink} from "./MarketPlaceUtils";
import BalanceComponent from "../Utility/BalanceComponent";
import ModalActions from "actions/ModalActions";
import AccountActions from "actions/AccountActions";
import "./MarketPlace.scss";

export default class Product extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            thisAsset: {},
            description: {
                main: "",
                nft_object: {
                    artist: "",
                    attestation: "",
                    license: "",
                    media_png: "",
                    narrative: "",
                    title: "",
                    type: ""
                }
            },
            balances: {},
            issuerBalance: {},
        };
    }

    componentDidMount() {
        this.getThisAsset();
        this.getUserBalance();
    }

    getThisAsset = () => {
        const {nftAssets, location} = this.props;
        const assetName = location.pathname.replace(
            "/marketplace/product/",
            ""
        );
        nftAssets.filter(asset => {
            if (assetName === asset.symbol) {
                this.setState({
                    issuerBalance: ChainStore.getAccount(asset.issuer),
                    thisAsset: asset,
                    description: JSON.parse(asset.options.description)
                });
            }
        });
    };

    getUserBalance = () => {
        const {accountName} = this.props;
        if (accountName != "committee-account") {
            let balanceObj = ChainStore.getAccount(accountName);
            setTimeout(() => {
                this.setState({balances: balanceObj.get("balances").toJS()});
            }, 1000);
        }
    };

    render() {
        const {thisAsset, description, balances, issuerBalance} = this.state;
        const {accountName} = this.props;
        let media =
            description.nft_object?.media_jpeg ||
            description.nft_object?.media_png ||
            description.nft_object?.media_gif || "";
        let sell = false;

        return (
            <div className="market-place">
                <BackLink link={"/marketplace"} />
                <div className="nft-item">
                    <div className="nft-wrap nft-product">
                        <div className="nft-info">
                            <Link
                                to="/marketplace/nft"
                                className="store-link product-mobile"
                            >
                                <Translate content="marketplace.nft_store" />
                            </Link>
                            <div className="product-title product-mobile product-mobile-title">
                                {description.nft_object?.title
                                    ? description.nft_object.title
                                    : thisAsset.symbol}
                            </div>
                            <div className="product-img">
                                <img
                                    src={
                                        thisAsset.symbol === "BALLS" ? 
                                            "https://static.haqq.exchange/gbl/images/ball.png"
                                            : (media.length > 0 ? window.atob(media) : description.image)
                                    }
                                    onError={(e)=>{
                                        console.log("Picture error")
                                        e.target.src="https://static.haqq.exchange/images/nft/no-image.jpg"
                                    }}
                                />
                            </div>
                            {issuerBalance ? (
                                <div>
                                    {Object.keys(issuerBalance).length > 0 ? (
                                    <>
                                        {issuerBalance.get("orders").toJS().length > 0 ?
                                        <div style={{display: "flex", flexDirection: "column"}}>
                                            <p className="product-price-list">
                                                Стоимость:
                                            </p>
                                            {issuerBalance.get("orders").toJS().map(item => {
                                                let order = ChainStore.getObject(item).toJS();
                                                let price =order.sell_price.quote.amount;
                                                let coinObj = ChainStore.getObject(order.sell_price.quote.asset_id);
                                                let strPrice = String(price);
                                                    if (order.sell_price.base.asset_id ===thisAsset.id) {
                                                        sell = true;
                                                        return (
                                                            <span
                                                                className="product-price"
                                                                style={{margin:"0 0 10px 0"}}
                                                                key={Math.random()}
                                                            >
                                                                {strPrice.substr(
                                                                    0,
                                                                    strPrice.length -
                                                                        coinObj.get(
                                                                            "precision"
                                                                        )
                                                                )}{" "}
                                                                {coinObj.get("symbol")}
                                                            </span>
                                                        );
                                                    } else { null}
                                            })}
                                        </div> : null}
                                        {sell ? null :
                                        <span
                                            className="product-price"
                                            style={{
                                            margin:"40px 0"}}
                                        >
                                            <Translate content="marketplace.not_for_sale" />
                                        </span>}
                                    </>
                                    ) : 
                                    <span
                                        className="product-price"
                                        style={{
                                        margin:
                                            "40px 0"
                                        }}
                                    >
                                        <Translate content="marketplace.not_for_sale" />
                                    </span>}
                                </div>
                            ) : (
                                <div style={{marginTop: "30px"}}>
                                    {/* <span
                                        className="product-price"
                                        style={{margin: "40px 0"}}
                                    >
                                        <Translate content="marketplace.login" />
                                    </span>
                                    <div className="registration-buttons">
                                        <Link
                                            to="/create-account/password"
                                            className="btn-blue btns"
                                            style={{
                                                height: "70px",
                                                maxWidth: "160px",
                                                display: "flex",
                                                alignItems: "center",
                                                ustifyContent: "center",
                                                borderRadius: "3px",
                                                marginRight: "30px"
                                            }}
                                        >
                                            <Translate content="header.create_account" />
                                        </Link>
                                        <Link
                                            to="/authorization"
                                            className="btn-blue btns"
                                            style={{
                                                height: "70px",
                                                maxWidth: "160px",
                                                display: "flex",
                                                alignItems: "center",
                                                justifyContent: "center",
                                                borderRadius: "3px"
                                            }}
                                        >
                                            <Translate content="header.unlock_short" />
                                        </Link>
                                    </div> */}
                                </div>
                            )}
                        </div>
                        <div className="product-wrap">
                            <Link
                                to="/marketplace/nft"
                                className="store-link product-desktop"
                            >
                                <Translate content="marketplace.nft_store" />
                            </Link>
                            <div className="product-title product-desktop">
                                {description.nft_object?.title
                                    ? description.nft_object.title
                                    : thisAsset.symbol}
                            </div>
                            <div
                                className="nft-item-description product-desktop"
                                style={{padding: "20px 0 0 0"}}
                            >
                                {description.main}
                            </div>
                            {accountName != "committee-account" ? (
                                <div
                                    className="nft-wrap product-desktop"
                                    style={{
                                        alignItems: "center",
                                        margin: "40px 0"
                                    }}
                                >
                                    {Object.keys(balances).length ==
                                    0 ? null : (
                                        <table>
                                            <thead>
                                                <tr>
                                                    <td>
                                                        <p className="product-specifications-title">
                                                            <Translate content="exchange.balance" />
                                                        </p>
                                                    </td>
                                                    <td className="nft-balance-element">
                                                        <Link
                                                            to={`/account/${accountName}`}
                                                        >
                                                            <Translate content="marketplace.deposit" />
                                                        </Link>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {Object.entries(balances).map(
                                                    item => {
                                                        let balanceAmount = ChainStore.getObject(
                                                            item[0]
                                                        );
                                                        let name = "";
                                                        if (
                                                            balanceAmount !=
                                                            undefined
                                                        ) {
                                                            name = balanceAmount.get(
                                                                "symbol"
                                                            );
                                                        }

                                                        return (
                                                            <tr
                                                                className="nft-item-balance"
                                                                key={
                                                                    name +
                                                                    Math.random()
                                                                }
                                                            >
                                                                <td>
                                                                    <span>
                                                                        {name}:
                                                                    </span>
                                                                </td>
                                                                <td className="nft-balance-element">
                                                                    <BalanceComponent
                                                                        balance={
                                                                            item[1]
                                                                        }
                                                                        hide_asset
                                                                    />
                                                                </td>
                                                            </tr>
                                                        );
                                                    }
                                                )}
                                            </tbody>
                                        </table>
                                    )}
                                </div>
                            ) : null}
                            <Link
                                className="btn-green to-buy btns"
                                style={{width: "200px"}}
                                to={`/market/${thisAsset.symbol}_${__GBL_CHAIN__ ? "GBL" : (__SCROOGE_CHAIN__ ? "SCROOGE" : "DEEX")}`}
                            >
                                <svg
                                    width="23"
                                    height="20"
                                    viewBox="0 0 23 20"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        fillRule="evenodd"
                                        clipRule="evenodd"
                                        d="M7.33264 13.3335H7.3316C6.96476 13.3344 6.66667 13.633 6.66667 14C6.66667 14.3675 6.9658 14.6667 7.33333 14.6667H19.4222C19.7905 14.6667 20.0889 14.9651 20.0889 15.3333C20.0889 15.7016 19.7905 16 19.4222 16H18.0889H8.66667H7.33333C6.23056 16 5.33333 15.1028 5.33333 14C5.33333 13.1793 5.83038 12.4729 6.53889 12.1649L4.13194 1.33333H0.666667C0.298438 1.33333 0 1.0349 0 0.666667C0 0.298438 0.298438 0 0.666667 0H4.66667C4.97917 0 5.24965 0.217014 5.31753 0.522049L5.7941 2.66667H22.0889C22.2981 2.66667 22.4951 2.76493 22.6212 2.93194C22.747 3.09896 22.7873 3.31528 22.7299 3.51649L20.0632 12.8498C19.9814 13.1359 19.7198 13.3333 19.4222 13.3333H7.33524L7.33264 13.3335ZM8.66667 16C9.76944 16 10.6667 16.8972 10.6667 18C10.6667 19.1028 9.76944 20 8.66667 20C7.56389 20 6.66667 19.1028 6.66667 18C6.66667 16.8972 7.56389 16 8.66667 16ZM18.0889 16C19.1917 16 20.0889 16.8972 20.0889 18C20.0889 19.1028 19.1917 20 18.0889 20C16.9861 20 16.0889 19.1028 16.0889 18C16.0889 16.8972 16.9861 16 18.0889 16ZM18.9193 12L21.205 4H6.09028L7.86806 12H18.9193ZM9.33333 18C9.33333 17.6325 9.0342 17.3333 8.66667 17.3333C8.29913 17.3333 8 17.6325 8 18C8 18.3675 8.29913 18.6667 8.66667 18.6667C9.0342 18.6667 9.33333 18.3675 9.33333 18ZM18.0889 17.3333C18.4564 17.3333 18.7556 17.6325 18.7556 18C18.7556 18.3675 18.4564 18.6667 18.0889 18.6667C17.7214 18.6667 17.4222 18.3675 17.4222 18C17.4222 17.6325 17.7214 17.3333 18.0889 17.3333Z"
                                        fill="white"
                                    />
                                </svg>
                                <Translate
                                    content="exchange.buy"
                                    style={{marginLeft: "8px"}}
                                />
                            </Link>
                            <div className="product-specifications">
                                <div
                                    className="nft-item-description product-mobile"
                                    style={{padding: "20px 0 0 0"}}
                                >
                                    {description.main}
                                </div>

                                {description.nft_object?.artist.length > 0 ||
                                description.nft_object?.narrative.length > 0 ? (
                                    <div>
                                        <h1 className="nft-title">
                                            <Translate content="marketplace.description" />
                                        </h1>
                                        {description.nft_object?.artist.length >
                                        0 ? (
                                            <div
                                                className="nft-item-description"
                                                style={{marginBottom: "20px"}}
                                            >
                                                <p className="product-specifications-title">
                                                    <Translate content="marketplace.artist" />
                                                </p>
                                                {description.nft_object.artist}
                                            </div>
                                        ) : null}
                                        {description.nft_object?.narrative
                                            .length > 0 ? (
                                            <div className="nft-item-description">
                                                <p className="product-specifications-title">
                                                    <Translate content="marketplace.narrative" />
                                                </p>
                                                {
                                                    description.nft_object
                                                        .narrative
                                                }
                                            </div>
                                        ) : null}
                                    </div>
                                ) : null}

                                <h1 className="nft-title">
                                    <Translate content="marketplace.attributes" />
                                </h1>
                                <div className="nft-item-description">
                                    <p className="product-specifications-title">
                                        <Translate content="account.user_issued_assets.symbol" />{" "}
                                        : {thisAsset.symbol}
                                    </p>
                                </div>
                                <div className="nft-item-description">
                                    <p className="product-specifications-title">
                                        <Translate content="marketplace.asset_id" />{" "}
                                        : {thisAsset.id}
                                    </p>
                                </div>
                                {thisAsset.options ? (
                                    <div className="nft-item-description">
                                        <p className="product-specifications-title">
                                            <Translate content="markets.supply" />{" "}
                                            : {thisAsset.options.max_supply}
                                        </p>
                                    </div>
                                ) : null}

                                {description.nft_object?.attestation.length >
                                    0 ||
                                description.nft_object?.license.length > 0 ? (
                                    <div>
                                        <h1 className="nft-title">
                                            <Translate content="marketplace.legal" />
                                        </h1>
                                        {description.nft_object?.attestation
                                            .length > 0 ? (
                                            <div
                                                className="nft-item-description"
                                                style={{marginBottom: "20px"}}
                                            >
                                                <p className="product-specifications-title">
                                                    <Translate content="marketplace.attestation" />
                                                </p>
                                                {
                                                    description.nft_object
                                                        .attestation
                                                }
                                            </div>
                                        ) : null}
                                        {description.nft_object?.license.length >
                                        0 ? (
                                            <div className="nft-item-description">
                                                <p className="product-specifications-title">
                                                    <Translate content="marketplace.license" />
                                                </p>
                                                {description.nft_object.license}
                                            </div>
                                        ) : null}
                                    </div>
                                ) : null}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

import React from "react";
import ProductsList from "./ProductsList";
import {BackLink} from "./MarketPlaceUtils";
import "./MarketPlace.scss";

export default class Products extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="market-place">
                <BackLink link={"/marketplace"} />
                <ProductsList nftAssets={this.props.nftAssets} />
            </div>
        );
    }
}

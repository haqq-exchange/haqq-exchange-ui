import React from "react";
import Translate from "react-translate-component";
import {Link} from "react-router-dom";
import ProductsList from "./ProductsList";
import "./MarketPlace.scss";

export default class MainMarkets extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeTab: "markets",
        };
    }

    render() {
        const {activeTab} = this.state;

        return (
            <div className="market-place">
                <div className="market-place-header">
                    <Translate
                        content="marketplace.header_title"
                        component="div"
                        className="market-place-header-title"
                    />
                    {this.props.locked ? (
                        <div className="header-btns">
                            <Translate
                                content="account.login_for_create_asset"
                                style={{color: "#515b5e"}}
                            />
                        </div>
                    ) : (
                        <div className="header-btns">
                            <Link
                                className="btn-green btn-create-asset btns"
                                to={`/account/${
                                    this.props.accountName
                                }/create-asset`}
                                style={{
                                    width: "200px",
                                    marginLeft: "auto",
                                    marginRight: "20px"
                                }}
                            >
                                <Translate content="header.create_asset" />
                            </Link>
                            <button
                                className="btn-green btns"
                                style={{width: "200px"}}
                            >
                                <Translate content="marketplace.to_be_seller" />
                            </button>
                        </div>
                    )}
                </div>
                <nav>
                    <ul className="market-place-nav">
                        <li
                            onClick={() =>
                                this.setState({activeTab: "markets"})
                            }
                        >
                            <Translate
                                content="marketplace.markets"
                                className={
                                    activeTab === "markets" ? "active" : ""
                                }
                            />
                        </li>
                        <li
                            onClick={() =>
                                this.setState({activeTab: "products"})
                            }
                        >
                            <Translate
                                content="marketplace.products"
                                className={
                                    activeTab === "products" ? "active" : ""
                                }
                            />
                        </li>
                    </ul>
                </nav>
                {activeTab === "markets" ? (
                    <div className="market-place-item">
                        <img src="https://static.haqq.exchange/images/nft/nft-big.png" />
                        <div className="market-place-item-title">
                            <Translate content="marketplace.nft_store" />
                        </div>
                        <div className="market-place-item-description">
                            <Translate content="marketplace.about_nft_store" />
                        </div>
                        <div className="market-place-btns-wrap">
                            <Link
                                to="/marketplace/products"
                                className="btn-green btn-products btns"
                            >
                                <Translate content="marketplace.products" />
                            </Link>
                            <Link
                                to="/marketplace/nft"
                                className="btn-blue btn-markets_info btns"
                            >
                                <Translate content="marketplace.markets_info" />
                            </Link>
                        </div>
                    </div>
                ) : (
                   <ProductsList nftAssets={this.props.nftAssets} {...this.props} />
                )}
            </div>
        );
    }
}

import React from "react";
import Translate from "react-translate-component";
import {Link} from "react-router-dom";
import {ChainStore} from "deexjs";
import "./MarketPlace.scss";

export default class ProductsList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {nftAssets} = this.props;

        return (
            <div className="product-list-wrap">
                <div className="product">
                    {nftAssets.map(asset => {
                        let assetObj = ChainStore.getAsset(asset?.options?.core_exchange_rate?.base?.asset_id);
                        let baseAsset = assetObj ? assetObj.get("symbol") : "";
                        const description = JSON.parse(
                            asset.options.description
                        );
                        const media = description.nft_object?.media_jpeg ||
                        description.nft_object?.media_png ||
                        description.nft_object?.media_gif || "";
                        return (
                            <div
                                className="market-place-product"
                                key={asset.symbol + asset.id}
                            >
                                <Link
                                    to="/marketplace/nft"
                                    className="store-link"
                                >
                                    <Translate content="marketplace.nft_store" />
                                </Link>
                                <div className="nft-item-img">
                                    <img src={
                                            asset.symbol === "BALLS"
                                                ? "https://static.haqq.exchange/gbl/images/ball.png"
                                                : (media.length > 0 ? window.atob(media) : description.image)}
                                        style={{maxHeight: "240px"}}
                                        onError={(e)=>{
                                            console.log("Picture error")
                                            e.target.src="https://static.haqq.exchange/images/nft/no-image.jpg"
                                        }}
                                    />
                                </div>
                                <div className="product-title">
                                    {description.nft_object?.title
                                        ? description.nft_object.title
                                        : asset.symbol}
                                </div>
                                <div
                                    className="nft-item-description"
                                    style={{
                                        textAlign: "center",
                                        lineHeight: "27px",
                                        padding: "20px 0",
                                        marginBottom: "20px"
                                    }}
                                >
                                    {description.main}
                                </div>
                                <table className="product-list">
                                    <tbody>
                                        <tr>
                                            <td className="product-list-item">
                                                <Translate content="explorer.asset.permissions.max_supply" />{" "}
                                                :
                                            </td>
                                            <td className="product-list-item product-list-element">
                                                {asset.options.max_supply}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td className="product-list-item">
                                                <Translate content="marketplace.currency" />
                                            </td>
                                            <td className="product-list-item product-list-element">
                                                {baseAsset}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <span className="product-price" />
                                <Link
                                    to={`/marketplace/product/${asset.symbol}`}
                                    className="btn-blue btns"
                                    style={{marginTop: "auto"}}
                                >
                                    <Translate content="account.transactions.info" />
                                </Link>
                            </div>
                        );
                    })}
                </div>
            </div>
        );
    }
}

import React from "react";
import Translate from "react-translate-component";
import {Link} from "react-router-dom";
import {BackLink} from "./MarketPlaceUtils";
import "./MarketPlace.scss";

export default class NFT extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="market-place">
                <BackLink link={"/marketplace"} />
                <div className="market-place-item">
                    <img src="https://static.haqq.exchange/images/nft/nft-big.png" />
                    <div
                        className="market-place-item-title"
                        style={{marginBottom: "30px"}}
                    >
                        <Translate content="marketplace.nft_store" />
                    </div>
                    <div
                        className="nft-item-description"
                        style={{textAlign: "center"}}
                    >
                        <Translate content="marketplace.about_nft_store" />
                    </div>
                    <Link
                        to="/marketplace/products"
                        className="btn-green btns"
                        style={{width: "200px", marginTop: "30px"}}
                    >
                        <Translate content="marketplace.products" />
                    </Link>
                </div>
            </div>
        );
    }
}

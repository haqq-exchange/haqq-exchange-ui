import React from "react";
import counterpart from "counterpart";
import cn from "classnames";
import Translate from "react-translate-component";
// import SettingsActions from "actions/SettingsActions";
// import AssetName from "../Utility/AssetName";
import Icon from "Components/Icon/Icon";
import Notify from "notifyjs";
import {Checkbox} from "antd";
import {getAlias} from "config/alias";

class SettingsEntry extends React.Component {
    constructor() {
        super();

        this.state = {
            message: null,
            changeNotification: false,
            transferNotifications: false
        };

        this.handleNotificationChange = this.handleNotificationChange.bind(this);
    }

    _setMessage(key) {
        this.setState({
            message: counterpart.translate(key)
        });

        this.timer = setTimeout(() => {
            this.setState({message: null});
        }, 4000);
    }

    componentWillUnmount() {
        clearTimeout(this.timer);
    }

    handleNotificationChange(path, evt) {
        this.props.onNotificationChange(path, !!evt.target.checked);
    }

    render() {
        let {defaults, setting, settings, saveSettings} = this.props;
        let options,
            optional,
            confirmButton,
            value,
            input,
            selected = settings.get(setting);
        let noHeader = false;
        let component = null;

        let iconToThemes = {
            "moon": "darkTheme",
            "sun": "lightTheme",
        };
        if( setting === "showAssetPercent" ) {
            console.log("this.props", this.props, selected);
            console.log("setting", setting);
        }

        switch (setting) {
            case "locale":
                value = selected;
                options = defaults
                    .filter(item => {
                        return ["en", "ru", "tr", "zh"].includes(item);
                    })
                    .map(entry => {
                        let translationKey = "languages." + entry;
                        let value = counterpart.translate(translationKey);

                        return (
                            <option key={entry} value={entry}>
                                {value}
                            </option>
                        );
                    });

                break;

            case "themes":
                value = selected;
                noHeader = true;

                /*options = defaults.map(entry => {
                    let translationKey = "settings." + entry;
                    let value = counterpart.translate(translationKey);

                    return (
                        <option key={entry} value={entry}>
                            {value}
                        </option>
                    );
                });*/


                component = __GBL_CHAIN__ || __GBLTN_CHAIN__ ? null : (<div className={"settings-entry-themes"}>
                    <Translate content={`settings.${setting}`} component={"span"}/>
                    <div className={"settings-entry-themes-wrap"}>
                        {Object.keys(iconToThemes).map(icon=>{
                            return (<Icon
                                key={icon}
                                name={icon}
                                className={cn("settings-entry-themes-item", {active: iconToThemes[icon] === selected})}
                                onClick={(event)=>this.props.onChange(setting, Object.assign(event, {target: {value: iconToThemes[icon]}})) }/>);
                        })}
                    </div>
                    {this.props.saveSettings == "themes" ? 
                        <Translate content={"settings.save"} component="div" className="show-save-setting"/>
                    : null}
                </div>);

                break;

            case "showSettles":
            case "showAssetPercent":
                value = selected;
                noHeader = true;

                component = (<div key={setting} className={"settings-entry-bolean"}>
                    <div className={"settings-entry-themes-wrap"} style={{display: "flex", alignItems: "center"}}>
                        <Checkbox
                            style={{margin: 0}}
                            prefixCls={"ant-checkbox-switch"}
                            name={setting}
                            defaultChecked={value}
                            onChange={(event)=>{
                                this.props.onChange(setting, event )}} >
                            <Translate content={`settings.${setting}`} />
                        </Checkbox>
                        {(this.props.saveSettings == "showAssetPercent" && setting == "showAssetPercent") ? 
                            <Translate content={"settings.save"} component="div" className="show-save-setting"/>
                        : null}
                        {(this.props.saveSettings == "showSettles" && setting == "showSettles") ? 
                            <Translate content={"settings.save"} component="div" className="show-save-setting"/>
                        : null}
                    </div>
                </div>);

                break;

            case "browser_notifications":
                value = selected;
                noHeader = true;

                component = (<div key={setting} className={"settings-entry-bolean"}>
                    <div className={"settings-entry-themes-wrap"}>
                        <div className="setting-block">
                            <Checkbox
                                prefixCls={"ant-checkbox-switch"}
                                name={setting}
                                defaultChecked={value.allow}
                                onChange={(event)=>{
                                    this.handleNotificationChange("allow", event);
                                    this.setState({changeNotification: value});
                                    let timerFirst = setTimeout(() => {
                                        this.setState({changeNotification: false});
                                        clearTimeout(timerFirst);
                                    }, 2000)
                                }} >
                                <Translate content={`settings.${setting}`} />
                            </Checkbox>
                            {this.state.changeNotification ? 
                                <Translate content={"settings.save"} component="div" className="show-save-setting" style={{marginBottom: "10px"}}/>
                            : null}
                        </div>
                        {!!value.allow &&
                        Notify.needsPermission && (
                            <div className={"settings-entry-wrap"}>
                                <Translate
                                    component="a"
                                    href="https://goo.gl/zZ7NHY"
                                    target="_blank"
                                    className={"settings-entry-link"}
                                    content="settings.browser_notifications_disabled_by_browser_notify"
                                />
                            </div>
                        )}
                    </div>
                    <div className={"settings-entry-themes-wrap"} style={{display: "flex", alignItems: "center"}}>
                        <Checkbox
                            style={{marginBottom: 0}}
                            prefixCls={"ant-checkbox-switch"}
                            name={setting}
                            defaultChecked={value.additional.transferToMe}
                            onChange={(event)=>{
                                this.handleNotificationChange("additional.transferToMe", event)
                                this.setState({transferNotifications: value});
                                    let timerSecond = setTimeout(() => {
                                        this.setState({transferNotifications: false});
                                        clearTimeout(timerSecond);
                                    }, 2000)
                                }} >
                            <Translate content={"settings.browser_notifications_additional_transfer_to_me"}/>
                        </Checkbox>
                        {this.state.transferNotifications ? 
                            <Translate content={"settings.save"} component="div" className="show-save-setting"/>
                        : null}
                    </div>
                </div>);

                break;
            case "browser_notifications1":
                value = selected;
                noHeader = true;

                component = (
                    <div className="settings--notifications">
                        <div className="settings--notifications--group">
                            <div className="settings--notifications--item">
                                <input
                                    type="checkbox"
                                    id="browser_notifications.allow"
                                    checked={!!value.allow}
                                    onChange={this.handleNotificationChange(
                                        "allow"
                                    )}
                                />
                                <label htmlFor="browser_notifications.allow">
                                    {counterpart.translate(
                                        "settings.browser_notifications_allow"
                                    )}
                                </label>
                            </div>
                            <div className="settings--notifications--group">
                                <div className="settings--notifications--item">
                                    <input
                                        type="checkbox"
                                        id="browser_notifications.additional.transferToMe"
                                        disabled={!value.allow}
                                        checked={
                                            !!value.additional.transferToMe
                                        }
                                        onChange={this.handleNotificationChange(
                                            "additional.transferToMe"
                                        )}
                                    />
                                    <label htmlFor="browser_notifications.allow">
                                        {counterpart.translate(
                                            "settings.browser_notifications_additional_transfer_to_me"
                                        )}
                                    </label>
                                </div>
                            </div>
                        </div>
                        {!!value.allow &&
                        Notify.needsPermission && (
                            <a
                                href="https://goo.gl/zZ7NHY"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                <Translate
                                    component="div"
                                    className="settings--notifications--no-browser-support"
                                    content="settings.browser_notifications_disabled_by_browser_notify"
                                />
                            </a>
                        )}
                    </div>
                );

                break;

            case "defaultMarkets":
                options = null;
                value = null;
                break;

            case "walletLockTimeout":
                value = selected;
                input = (
                    <div className={"settings-entry-wrap"}>
                        <div style={{display: "flex"}}>
                            <input
                                type="text"
                                className="settings-input"
                                value={selected}
                                onChange={(event)=>{this.props.onChange(setting, event)}}
                            />
                            {this.props.saveSettings == "walletLockTimeout" ? 
                                <Translate content={"settings.save"} component="div" className="show-save-setting"/>
                            : null}
                        </div>
                        <Translate  className={"settings-entry-help"} content={"settings.walletLockTimeoutHelp"} />
                    </div>
                );
                break;

            default:
                if (typeof selected === "number") {
                    value = defaults[selected];
                } else if (typeof selected === "boolean") {
                    if (selected) {
                        value = defaults[0];
                    } else {
                        value = defaults[1];
                    }
                } else if (typeof selected === "string") {
                    value = selected;
                }

                if (defaults) {
                    // console.log("value", value);
                    // console.log("defaults", defaults);
                    options = defaults.map(entry => {
                        let option = entry.translate
                            ? counterpart.translate(`settings.${entry.translate}`)
                            : getAlias(entry);

                        let key = entry.translate ? entry.translate : entry;

                        return (
                            <option
                                value={
                                    entry.translate ? entry.translate : entry
                                }
                                key={key}
                            >
                                {option}
                            </option>
                        );
                    });
                } else {
                    input = (
                        <input
                            className="settings-input"
                            type="text"
                            defaultValue={value}
                            onBlur={this.props.onChange.bind(this, setting)}
                        />
                    );
                }
                break;
        }
        //if (typeof value !== "number" && !value && !options) return null;

        if (value && value.translate) {
            value = value.translate;
        }


        return (
            <section className="block-list no-border-bottom">
                {noHeader ? null : (<Translate content={`settings.${setting}`}  className={"settings-entry-title"} component={"div"}/>)}
                {options && (
                    <div style={{display: "flex"}}>
                        <ul style={{margin: "0"}}>
                            <li className="with-dropdown">
                                {optional}
                                <select
                                    value={value}
                                    className="settings-select"
                                    onChange={this.props.onChange.bind(
                                        this,
                                        setting
                                    )}
                                >
                                    {options}
                                </select>
                                {confirmButton}
                            </li>
                        </ul>
                        {(this.props.saveSettings == "unit" && setting == "unit") ? 
                            <Translate content={"settings.save"} component="div" className="show-save-setting"/>
                        : null}
                         {(this.props.saveSettings == "locale" && setting == "locale") ? 
                            <Translate content={"settings.save"} component="div" className="show-save-setting"/>
                        : null}
                    </div>
                )}
                {input ? (
                    <ul>
                        <li>{input}</li>
                    </ul>
                ) : null}

                {component ? component : null}
                <div className="facolor-success" style={{marginTop: "8px"}}>{this.state.message}</div>
            </section>
        );
    }
}

export default SettingsEntry;

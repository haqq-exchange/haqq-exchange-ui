import React from "react";
import PropTypes from "prop-types";
import counterpart from "counterpart";
import IntlActions from "actions/IntlActions";
import Translate from "react-translate-component";
import SettingsActions from "actions/SettingsActions";

import Loadable from "react-loadable";
import LoadingIndicator from "Components/LoadingIndicator";


// import RestoreSettings from "./RestoreSettings";
import WebsocketAddModal from "./WebsocketAddModal";

/*const WebsocketAddModal = Loadable({
    loader: () => import(/!* webpackChunkName: "WebsocketAddModal" *!/ "./WebsocketAddModal"),
    loading: LoadingIndicator
});*/
const SettingsEntry = Loadable({
    loader: () => import(/* webpackChunkName: "SettingsEntry" */ "./SettingsEntry"),
    loading: LoadingIndicator
});
const AccountsSettings = Loadable({
    loader: () => import(/* webpackChunkName: "AccountsSettings" */ "./AccountsSettings"),
    loading: LoadingIndicator
});
const WalletSettings = Loadable({
    loader: () => import(/* webpackChunkName: "WalletSettings" */ "./WalletSettings"),
    loading: LoadingIndicator
});
const PasswordSettings = Loadable({
    loader: () => import(/* webpackChunkName: "PasswordSettings" */ "./PasswordSettings"),
    loading: LoadingIndicator
});

const BackupSettings = Loadable({
    loader: () => import(/* webpackChunkName: "BackupSettings" */ "./BackupSettings"),
    loading: LoadingIndicator
});

const AccessSettings = Loadable({
    loader: () => import(/* webpackChunkName: "AccessSettings" */ "./AccessSettings"),
    loading: LoadingIndicator
});

const MobileSettings = Loadable({
    loader: () => import(/* webpackChunkName: "MobileSettings" */ "./MobileSettings"),
    loading: LoadingIndicator
});

// import ApplicationApi from "api/ApplicationApi";
// import {Apis} from "deexjs-ws";
import { set } from "lodash";
import { SSL_OP_TLS_D5_BUG } from "constants";
// import AccountStore from "stores/AccountStore";
//import {getAlias} from "config/alias";

const WrapComponent = props => {
    return (
        <div className={props.className}>{props.children}</div>
    );
};

class Settings extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor(props) {
        super();

        let menuEntries = this._getMenuEntries(props);
        let activeSetting = 0;

        let tabIndex = props.match.params.tab
            ? menuEntries.indexOf(props.match.params.tab)
            : props.viewSettings.get("activeSetting", 0);
        if (tabIndex >= 0) activeSetting = tabIndex;

        this.state = {
            apiServer: props.settings.get("apiServer"),
            activeSetting,
            menuEntries,
            saveSettings: false,
            settingEntries: {
                general: [
                    "locale",
                    "unit",
                    "walletLockTimeout",
                    "browser_notifications",
                    "showSettles",
                    "showAssetPercent",
                    "themes",
                    /*"passwordLogin",*/
                    //"reset"
                ],
                access: ["apiServer"/*, "faucet_address"*/]
            }
        };

        this._handleNotificationChange = this._handleNotificationChange.bind(this);
        //this.setBlindAccount = this.setBlindAccount.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.tab !== this.props.match.params.tab) {
            this._onChangeMenu(this.props.match.params.tab);
        }
    }

    UNSAFE_componentWillReceiveProps(np) {
        if (
            np.settings.get("passwordLogin") !==
            this.props.settings.get("passwordLogin")
        ) {
            const currentEntries = this._getMenuEntries(this.props);
            const menuEntries = this._getMenuEntries(np);
            const currentActive = currentEntries[this.state.activeSetting];
            const newActiveIndex = menuEntries.indexOf(currentActive);
            const newActive = menuEntries[newActiveIndex];
            this.setState({
                menuEntries
            });
            if (newActiveIndex && newActiveIndex !== this.state.activeSetting) {
                this.setState({
                    activeSetting: menuEntries.indexOf(currentActive) || 0
                });
            } else if (
                !newActive ||
                this.state.activeSetting > menuEntries.length - 1
            ) {
                this.setState({
                    activeSetting: 0
                });
            }
        }
    }

    _getMenuEntries(props) {
        if (props.deprecated) {
            return ["wallet", "backup"];
        }
        let menuEntries = [
            "general",
            // "wallet",
            //"access_mobile",
            // "accounts",
            // "password",
            // "backup",
            //"restore",
            "access",
            //"faucet_address",
            //"reset"
        ];

        console.log("props.settings", props.settings.toJS());


            // if (props.currentAccount) {
            //     // menuEntries.splice(4, 1);
            //     // menuEntries.splice(3, 1);
            //     if(!__SCROOGE_CHAIN__ && !__GBL_CHAIN__ && !__GBLTN_CHAIN__) {
            //     menuEntries.push("access_mobile");
            //     }
            // }
        return menuEntries;
    }

    triggerModal(event, ...args) {
        this.refWs_modal.show(event, ...args);
    }

    _handleNotificationChange(path, value) {
        // use different change handler because checkbox doesn't work
        // normal with e.preventDefault()
        let updatedValue = set(
            this.props.settings.get("browser_notifications"),
            path,
            value
        );   
        SettingsActions.changeSetting({
            setting: "browser_notifications",
            value: updatedValue
        });
    }

    _showSaveSatting(setting) {
        this.setState({saveSettings: setting});
        let timer = setTimeout(() => {
            this.setState({saveSettings: false});
            clearTimeout(timer);
        }, 2000)
    }

    _onChangeSetting(setting, e) {
        //e.preventDefault();

        let {defaults} = this.props;
        let value = null;

        function findEntry(targetValue, targetDefaults) {
            if (!targetDefaults) return targetValue;
            if (targetDefaults[0].translate) {
                for (var i = 0; i < targetDefaults.length; i++) {
                    if (
                        counterpart.translate(
                            `settings.${targetDefaults[i].translate}`
                        ) === targetValue
                    ) {
                        return i;
                    }
                }
            } else {
                return targetDefaults.indexOf(targetValue);
            }
        }

        switch (setting) {
            case "locale":
                let myLocale = counterpart.getLocale();
                if (e.target.value !== myLocale) {
                    IntlActions.switchLocale(e.target.value);
                    this._showSaveSatting("locale")
                    SettingsActions.changeSetting({
                        setting: "locale",
                        value: e.target.value
                    });
                }
                break;

            case "themes":
                this._showSaveSatting("themes");
                SettingsActions.changeSetting({
                    setting: "themes",
                    value: e.target.value
                });
                break;

            case "defaultMarkets":
                break;

            case "walletLockTimeout":
                let newValue = parseInt(e.target.value, 10);
                if (isNaN(newValue)) newValue = 0;
                if (!isNaN(newValue) && typeof newValue === "number") {
                    this._showSaveSatting("walletLockTimeout");
                    SettingsActions.changeSetting({
                        setting: "walletLockTimeout",
                        value: newValue
                    });
                }
                break;

            case "inverseMarket":
            case "confirmMarketOrder":
                value = findEntry(e.target.value, defaults[setting]) === 0; // USD/BTS is true, BTS/USD is false
                break;

            case "apiServer":
                SettingsActions.changeSetting({
                    setting: "apiServer",
                    value: e.target.value
                });
                this.setState({
                    apiServer: e.target.value
                });
                break;

            case "showSettles":
                this._showSaveSatting("showSettles");
                SettingsActions.changeSetting({
                    setting: "showSettles",
                    value: e.target.checked
                });
                break;

            case "showAssetPercent":
                this._showSaveSatting("showAssetPercent");
                SettingsActions.changeSetting({
                    setting: "showAssetPercent",
                    value: e.target.checked
                });
                break;


            case "passwordLogin":
                let reference = defaults[setting][0];
                console.log("reference", e.target.value);
                if (reference.translate) reference = reference.translate;
                SettingsActions.changeSetting({
                    setting,
                    value: e.target.checked
                });
                break;

            case "unit":
                let index = findEntry(e.target.value, defaults[setting]);
                this._showSaveSatting("unit");
                SettingsActions.changeSetting({
                    setting: setting,
                    value: defaults[setting][index]
                });
                break;

            default:
                value = findEntry(e.target.value, defaults[setting]);
                break;
        }

        if (value !== null) {
            SettingsActions.changeSetting({setting: setting, value: value});
        }
    }

    onReset() {
        SettingsActions.clearSettings();
    }

    _redirectToEntry(entry) {
        this.props.history.push("/settings/" + entry);
    }

    _onChangeMenu(entry) {
        let index = this.state.menuEntries.indexOf(entry);
        let activeSetting = index >= 0 ? index : 0;
        this.setState({activeSetting});

        SettingsActions.changeViewSetting({activeSetting});
    }

    /*setBlindAccount() {
        let { currentAccount } = AccountStore.getState();
        console.log("setBlindAccount");


         //ApplicationApi.create_blind_account(currentAccount);
    }*/

    render() {
        let {settings, defaults} = this.props;
        const {menuEntries, activeSetting, settingEntries, saveSettings} = this.state;
        let entries;
        let activeEntry = menuEntries[activeSetting] || menuEntries[0];

        switch (activeEntry) {
            case "accounts":
                entries = <AccountsSettings />;
                break;

            case "wallet":
                entries = <WalletSettings {...this.props} />;
                break;

            case "password":
                entries = <PasswordSettings {...this.props}  />;
                break;

            case "backup":
                entries = <BackupSettings />;
                break;

            // case "access_mobile":
            //     entries = <MobileSettings />;
            //     break;

            /*case "restore":
panel-bg-color                entries = (
                    <RestoreSettings
                        passwordLogin={this.props.settings.get("passwordLogin")}
                    />
                );
                break;*/

            case "access":
                entries = (
                    <AccessSettings
                        faucet={settings.get("faucet_address")}
                        nodes={defaults.apiServer}
                        onChange={this._onChangeSetting}
                        triggerModal={(event, data)=>this.triggerModal(event,data)}
                    />
                );
                break;
            /*case "faucet_address":
                entries = (
                    <input
                        type="text"
                        className="settings-input"
                        defaultValue={settings.get("faucet_address")}
                        onChange={this._onChangeSetting.bind(
                            this,
                            "faucet_address"
                        )}
                    />
                );
                break;*/

            /*case "reset":
                entries = <ResetSettings />;
                break;*/

            default:
                // console.log("settings", settings);
                // console.log("defaults", defaults);
                entries = <WrapComponent className={"settings-menu-content-wrap"}>
                    {
                        settingEntries[activeEntry].map(setting => {
                            return (
                                <SettingsEntry
                                    key={setting}
                                    setting={setting}
                                    settings={settings}
                                    defaults={defaults[setting]}
                                    onChange={this._onChangeSetting.bind(this)}
                                    saveSettings={saveSettings}
                                    onNotificationChange={
                                        this._handleNotificationChange
                                    }
                                    locales={this.props.localesObject}
                                    {...this.state}
                                />
                            );
                        })
                    }
                </WrapComponent> ;
                break;
        }

        return (
            <div className={this.props.deprecated ? "" : "grid-block settings-menu"}>
                <div className="settings-menu-wrap">
                    <div className="settings-menu-asaid">
                        <Translate
                            component="h3"
                            content="header.settings"
                            className={"panel-bg-color"}
                        />
                        <div className="settings-menu-asaid-wrap">
                            <ul>
                                {menuEntries.map((entry, index) => {
                                    return (
                                        <li
                                            className={
                                                index === activeSetting
                                                    ? "active"
                                                    : ""
                                            }
                                            onClick={this._redirectToEntry.bind(
                                                this,
                                                entry
                                            )}
                                            key={entry}
                                        >
                                            <Translate
                                                content={"settings." + entry}
                                            />
                                        </li>
                                    );
                                })}
                            </ul>
                        </div>
                    </div>
                    <div className="settings-menu-content">
                        <div className="grid-block small-12 no-margin vertical">
                            <Translate
                                component="h3"
                                content={
                                    "settings." + menuEntries[activeSetting]
                                }
                            />
                            {entries}
                        </div>
                    </div>
                </div>
                <WebsocketAddModal
                    ref={ref=>this.refWs_modal=ref}
                    refs={
                        {
                            add_modal: (ref)=>this.refWs_modal_add=ref,
                            remove_modal: (ref)=>this.refWs_modal_remove=ref
                        }
                    }
                    apis={defaults["apiServer"]}
                    api={defaults["apiServer"]
                        .filter(a => {
                            return a.url === this.state.apiServer;
                        })
                        .reduce((a, b) => {
                            return b && b.url;
                        }, null)}
                    changeConnection={apiServer => {
                        this.setState({apiServer});
                    }}
                />
            </div>
        );
    }
}

export default Settings;

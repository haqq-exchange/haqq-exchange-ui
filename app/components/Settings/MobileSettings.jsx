import React from "react";
import QRCode from "qrcode.react";
import { saveAs } from "file-saver/FileSaver";
import Translate from "react-translate-component";
// import CryptoJS from "crypto-js";

import {Blowfish} from "javascript-blowfish";
import ChainTypes from "Components/Utility/ChainTypes";
import BindToChainState from "Components/Utility/BindToChainState";
import {connect} from "alt-react";
import AccountStore from "../../stores/AccountStore";
import IntlStore from "../../stores/IntlStore";
import WalletUnlockStore from "../../stores/WalletUnlockStore";
import WalletDb from "../../stores/WalletDb";
import WrapperAuth from "Utility/WrapperAuth";
import {Input} from "antd";


class MobileSettings extends React.Component {

    static propTypes = {
        account: ChainTypes.ChainAccount.isRequired
    };


    constructor() {
        super();

        this.state = {
            password: ""
        };
    }


    getValidPassword = (name) => {
        /* a-zA-Z */
        return name && /^[a-zA-Z0-9]+$/.test(name) && name.length <= 32 && name.length >= 4;
    };

    setPassword = (password) => {
        if ( password && this.getValidPassword(password) || !password ) {
            this.setState({password});
        }
    };


    render() {
        let { password } = this.state;

        let dataProps = {
            setPassword: this.setPassword,
            validPassword: this.getValidPassword,
            password,
            ...this.props
        };

        return (
            <>
                {__SCROOGE_CHAIN__ ? null :
                <div className={"mobile-settings"}>
                    { this.getValidPassword(password) ?
                        <ShowQrCode {...dataProps} /> :
                        <ShowInputForm {...dataProps} />
                    }
                </div>
                }
            </>
        );
    }
}

class ShowInputForm extends React.Component {

    static defaultProps = {
        androidUrl: "https://play.google.com/store/apps/details?id=deex.exchange.android"
    }

    constructor(props) {
        super(props);
        this.state = {
            androidAppShow: true,
            hasValidPass: false
        };
    }


    componentDidMount() {
        const _this = this;
        try {
            const {androidUrl} = this.props;
            fetch(androidUrl)
                .then(() => {
                    _this.setState({
                        androidAppShow: false
                    });
                })
                .catch(() => {
                    _this.setState({
                        androidAppShow: true

                    });
                });
        } catch (err) {

        }
    }

    goToAndroid = () => {
        const {androidUrl} = this.props;
        window.open(androidUrl, "_blank");
    };

    setPassword = event =>{
        const {value} = event.target;

        this.setState({
            pass: value,
            hasValidPass: this.props.validPassword(value)
        });
    };


    render(){
        const {androidAppShow, hasValidPass} = this.state;
        return (
            <div className={"mobile-settings-wrap"}>
                <div className={"mobile-settings-row"}>
                    <Translate unsafe content={"settings.mobile-access.step1"}  />
                    <div>
                        <Translate unsafe content={__SCROOGE_CHAIN__ ? "settings.mobile-access.step1_text_scrooge" : "settings.mobile-access.step1_text"} component={"div"} style={{
                            margin: "0 0 20px 0"
                        }}  />

                        <button type={"button"} onClick={()=>this.goToAndroid()} disabled={androidAppShow} className={"btn btn-blue"}>
                            <img src={require("Assets/img/google_play.png")} alt=""/>
                            <Translate content={"settings.mobile-access.download_app"}/>
                        </button>
                    </div>
                </div>
                <div className={"mobile-settings-row"}>
                    <Translate unsafe content={"settings.mobile-access.step2"}  />
                    <div>
                        <Translate unsafe content={"settings.mobile-access.step2_text"} component={"div"}  />

                        <form className={"mobile-settings-form"}>
                            <Translate content={"settings.mobile-access.add_code"} component={"label"}  />
                            <Input.Password
                                type="text"
                                className={"mobile-settings-form-input settings-input"}
                                onChange={this.setPassword}/>

                            <Translate content={"settings.mobile-access.code_help"} className={"mobile-settings-form-info"} component={"div"}  />

                            <WrapperAuth
                                component={"button"}
                                className={"btn btn-green"}
                                disabled={!hasValidPass}
                                type="button"
                                onClick={()=>this.props.setPassword(this.state.pass)}
                            >
                                <Translate content={"settings.mobile-access.create"}  />
                            </WrapperAuth>
                        </form>
                    </div>
                </div>

            </div>
        );
    }
}

class ShowQrCode extends React.Component {

    constructor(){
        super();
        this.state = {
            encryptedKeys: ""
        };
    }

    componentDidMount(){
        this.getEncryptedKeys();
    }

    componentDidUpdate(prevProps){
        console.log("prevProps", prevProps, this.props);
        if( prevProps.password !== this.props.password ) {
            this.getEncryptedKeys();
        }
    }

    byteCount = (s) => {
        return encodeURI(s).split(/%..|./).length - 1;
    };

    saveQrCode = () => {
        const {encryptedKeys} = this.state;
        const  {account} = this.props;
        if( encryptedKeys ) {
            var canvas = document.getElementById("qrcode");
            canvas.toBlob(function(blob) {
                saveAs(blob, [account.get("name"),"_access_mobile", "png"].join("."));
            });
        }
    };

    getEncryptedKeys = () => {
        const  {password, account, wallet_locked} = this.props;

        if( wallet_locked || !account ) return null;
        //debugger;

        let memo_privkey, dataSave, privateKeys = [];
        let memo_public = account.getIn(["options", "memo_key"]);
        let getWallet = WalletDb.getWallet();

        if( getWallet.keys ) {
            privateKeys = Object.values(getWallet.keys).map(keys => keys.private_wif).reverse();
            if(!privateKeys[1]){
                privateKeys.push(privateKeys[0]);
            }
        }
        if( memo_public ) {
            memo_privkey = WalletDb.getPrivateKey(memo_public);
        }


        if( memo_privkey ) {
            dataSave = {
                name: account.get("name"),
                private_keys: [memo_public].concat(privateKeys),
            };
        }


        let iv = memo_public.substr(7, 8);
        const bf = new Blowfish(password);
        const encrypted2 = bf.encrypt(JSON.stringify(dataSave), iv);
        let encryptedKeys = bf.base64Encode(encrypted2);

        this.setState({
            encryptedKeys: [iv,encryptedKeys].join(":")
        });
    };

    render(){

        // console.log("render");

        const {encryptedKeys} = this.state;
        // const {wallet_locked} = this.props;

        // console.log("render encryptedKeys", encryptedKeys);
        // console.log("render wallet_locked", wallet_locked);

        //if( wallet_locked ) return null;


        // console.log("stringPubKey, publicKey ", stringPubKey, publicKey );

        return (
            <div className={"mobile-settings-wrap"}>
                <div className={"mobile-settings-row"}>
                    <Translate unsafe content={"settings.mobile-access.step3"}  />
                    <div>
                        <Translate unsafe content={"settings.mobile-access.step3_text"} component={"div"} style={{
                            margin: "0 0 20px 0"
                        }}  />
                        <div className={"mobile-settings-qrcode"}>
                            <div className={"mobile-settings-qrcode-wrap"}>
                                <QRCode
                                    id={"qrcode"}
                                    className={"mobile-settings-qrcode-img"}
                                    size={230}
                                    value={encryptedKeys}/>
                                {/*<div className={"mobile-settings-qrcode-links"}>
                                    <Translate
                                        content={"wallet.save_qrcode"}
                                        onClick={this.saveQrCode} className="btn btn-green"
                                        component={"button"}/>
                                    <Translate
                                        content={"transfer.cancel"}
                                        onClick={()=>this.props.setPassword()} className="btn btn-gray"
                                        component={"button"}/>
                                </div>*/}
                            </div>
                        </div>

                    </div>
                </div>
                <div className={"mobile-settings-hr"}>
                    <Translate unsafe content={"settings.mobile-access.step4"} component={"div"} />
                    <Translate unsafe content={"settings.mobile-access.step4_text"}  />
                </div>

            </div>
        );
    }
}

const MobileSettingsBind = BindToChainState(MobileSettings, {
    show_loader: true
});


export default connect(
    MobileSettingsBind,
    {
        listenTo() {
            return [
                AccountStore,
                IntlStore,
                WalletUnlockStore
            ];
        },
        getProps() {
            return {
                account: AccountStore.getState().passwordAccount,
                locale: IntlStore.getState().currentLocale,
                wallet_locked: WalletUnlockStore.getState().locked,
            };
        }
    }
);

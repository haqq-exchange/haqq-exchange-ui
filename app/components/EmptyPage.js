import React, {Component} from "react";

export default class EmptyPage extends Component {
    render() {
        return (
            <div className="grid-container page-layout help-content-layout">
                <h1>Empty Page</h1>
            </div>
        );
    }
}

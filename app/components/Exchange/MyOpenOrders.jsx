import React from "react";
import PropTypes from "prop-types";
// import {Link} from "react-router-dom";
// import counterpart from "counterpart";
// import Ps from "perfect-scrollbar";
// import OpenSettleOrders from "./OpenSettleOrders";
import utils from "common/utils";
import Translate from "react-translate-component";
import PriceText from "../Utility/PriceText";
import TransitionWrapper from "../Utility/TransitionWrapper";
import SettingsActions from "actions/SettingsActions";
import AssetName from "../Utility/AssetName";
import WrapperAuth from "../Utility/WrapperAuth";
import cnames from "classnames";
// import Icon from "../Icon/Icon";
import {ChainStore} from "deexjs";
import {LimitOrder, CallOrder} from "common/MarketClasses";
// import {EquivalentValueComponent} from "../Utility/EquivalentValueComponent";
// import {MarketPrice} from "../Utility/MarketPrice";
// import FormattedPrice from "../Utility/FormattedPrice";
import ResponseOption from "config/response";
import MediaQuery from "react-responsive";
import {getAlias} from "../../config/alias";

class TableHeader extends React.Component {
    render() {
        let {
            baseSymbol,
            quoteSymbol
        } = this.props;

        return (
            <div className={"table-head1"}>
                <div>
                    <div>
                        <Translate
                            className="header-sub-title"
                            content="exchange.price"
                        />
                    </div>
                    <div>
                        {baseSymbol ? (
                            <span className="header-sub-title">
                                <AssetName dataPlace="top" name={quoteSymbol} />
                            </span>
                        ) : null}
                    </div>
                    <div>
                        {baseSymbol ? (
                            <span className="header-sub-title">
                                <AssetName dataPlace="top" name={baseSymbol} />
                            </span>
                        ) : null}
                    </div>
                    <div>
                        <Translate
                            className="header-sub-title"
                            content="transaction.expiration"
                        />
                    </div>
                    <div style={{width: "6%"}} />
                </div>
            </div>
        );
    }
}

TableHeader.defaultProps = {
    quoteSymbol: null,
    baseSymbol: null
};

class OrderRow extends React.Component {
    shouldComponentUpdate(nextProps) {
        return (
            nextProps.order.for_sale !== this.props.order.for_sale ||
            nextProps.order.id !== this.props.order.id ||
            nextProps.quote !== this.props.quote ||
            nextProps.base !== this.props.base ||
            nextProps.order.market_base !== this.props.order.market_base
        );
    }

    render() {
        let {
            base,
            quote,
            order,
            settings
        } = this.props;
        const isBid = order.isBid();
        const isCall = order.isCall();

        let timeOptions = {
            year: "numeric", month: "numeric", day: "numeric",
            hour: "numeric", minute: "numeric", second: "numeric",
            hour12: false
        };


        let orderExpiration = new Date(order.expiration + " Z") ;
        if( !orderExpiration.getDate() ) {
            orderExpiration = new Date(order.expiration) ;
        }

        let expiration = new Intl
            .DateTimeFormat(settings.get("locale"), timeOptions)
            .format(orderExpiration);


        return (
            <div className={cnames("open-orders-row", {
                "order-call": isCall,
                "order-bid": !isCall && isBid,
                "order-ask": !isCall && !isBid
            })} key={order.id}>
                <div className="open-orders-top">
                    <span>
                        <Translate content="exchange.price"/>
                        {" "}
                        <PriceText
                            className={"open-orders-price-text"}
                            price={order.getPrice()}
                            base={base}
                            quote={quote}
                        />
                    </span>

                    {!isCall ? (
                        <WrapperAuth component={"a"} className={"open-orders-cancel"} onClick={this.props.onCancel} >
                            <Translate content="transfer.cancel"/>
                        </WrapperAuth>
                    ) : null }
                </div>
                <div className="open-orders-bottom">
                    <div className={"open-orders-row-item"}>
                        <div className={"open-orders-item"}>
                            <span>{getAlias(base.get("symbol"))} </span>
                            <span>
                                {utils.format_number(
                                    order[!isBid ? "amountToReceive" : "amountForSale"]().getAmount({real: true}),
                                    base.get("precision")
                                )}
                            </span>
                        </div>
                        <div className={"open-orders-item"}>
                            <span>{getAlias(quote.get("symbol"))}</span>
                            <span>
                                {utils.format_number(
                                    order[!isBid ? "amountForSale" : "amountToReceive"]().getAmount({real: true}),
                                    quote.get("precision")
                                )}
                            </span>
                        </div>
                    </div>
                    <div className={"open-orders-row-item"}>
                        <div className={"open-orders-item"}>
                            <Translate content="transaction.expiration"/>
                            <span>
                                {isCall
                                    ? null
                                    : expiration }
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

OrderRow.defaultProps = {
    showSymbols: false
};

class MyOpenOrders extends React.Component {
    constructor(props) {
        super();
        this.state = {
            activeTab: props.activeTab
        };
        this._getOrders = this._getOrders.bind(this);
        this.empty_orders = true;
    }

    componentDidMount() {
        // let contentContainer = this.refContainer;
        // if (contentContainer) Ps.initialize(contentContainer);
    }

    componentDidUpdate() {
        // let contentContainer = this.refContainer;
        // if (contentContainer) Ps.update(contentContainer);
    }

    _getOrders() {
        const {currentAccount, base, quote, feedPrice} = this.props;
        const orders = currentAccount.get("orders"),
            call_orders = currentAccount.get("call_orders");
        // console.log("orders",orders);
        const baseID = base.get("id"),
            quoteID = quote.get("id");
        const assets = {
            [base.get("id")]: {precision: base.get("precision")},
            [quote.get("id")]: {precision: quote.get("precision")}
        };
        let limitOrders = orders
            .toArray()
            .map(order => {
                let o = ChainStore.getObject(order);
                if (!o) return null;
                let sellBase = o.getIn(["sell_price", "base", "asset_id"]),
                    sellQuote = o.getIn(["sell_price", "quote", "asset_id"]);
                if (
                    (sellBase === baseID && sellQuote === quoteID) ||
                    (sellBase === quoteID && sellQuote === baseID)
                ) {
                    return new LimitOrder(o.toJS(), assets, quote.get("id"));
                }
            })
            .filter(a => !!a);

        // console.log("limitOrders",limitOrders);

        let callOrders = call_orders
            .toArray()
            .map(order => {
                try {
                    let o = ChainStore.getObject(order);
                    if (!o) return null;
                    let sellBase = o.getIn(["call_price", "base", "asset_id"]),
                        sellQuote = o.getIn([
                            "call_price",
                            "quote",
                            "asset_id"
                        ]);
                    if (
                        (sellBase === baseID && sellQuote === quoteID) ||
                        (sellBase === quoteID && sellQuote === baseID)
                    ) {
                        return feedPrice
                            ? new CallOrder(
                                  o.toJS(),
                                  assets,
                                  quote.get("id"),
                                  feedPrice
                              )
                            : null;
                    }
                } catch (e) {
                    return null;
                }
            })
            .filter(a => !!a)
            .filter(a => {
                try {
                    return a.isMarginCalled();
                } catch (err) {
                    return false;
                }
            });
        // console.log("callOrders",callOrders);
        return limitOrders.concat(callOrders);
    }

    _changeTab(tab) {
        SettingsActions.changeViewSetting({
            ordersTab: tab
        });
        this.setState({
            activeTab: tab
        });

        // Ensure that focus goes back to top of scrollable container when tab is changed
        // let contentContainer = this.refContainer;
        // contentContainer.scrollTop = 0;
        // Ps.update(contentContainer);

        // setTimeout(ReactTooltip.rebuild, 1000);
    }

    render() {
        let {base, quote, settings} = this.props;

        if (!base || !quote) return null;

        let contentContainer;


        const orders = this._getOrders();
        // console.log("Exchange.jsx render", orders);
        let emptyRow = (
            <div className="open-orders-empty">
                <Translate content="account.no_orders" />
            </div>
        );

        let bids = orders
            .filter(a => {
                return a.isBid();
            })
            .sort((a, b) => {
                return b.getPrice() - a.getPrice();
            })
            .map(order => {
                let price = order.getPrice();
                return (
                    <OrderRow
                        price={price}
                        key={order.id}
                        order={order}
                        base={base}
                        quote={quote}
                        settings={settings}
                        onCancel={this.props.onCancel.bind(this, order.id)}
                    />
                );
            });

        let asks = orders
            .filter(a => {
                return !a.isBid();
            })
            .sort((a, b) => {
                return a.getPrice() - b.getPrice();
            })
            .map(order => {
                let price = order.getPrice();
                return (
                    <OrderRow
                        price={price}
                        key={order.id}
                        order={order}
                        base={base}
                        quote={quote}
                        settings={settings}
                        onCancel={this.props.onCancel.bind(this, order.id)}
                    />
                );
            });

        let rows = [];

        if (asks.length) {
            rows = rows.concat(asks);
        }

        if (bids.length) {
            rows = rows.concat(bids);
        }



        rows.sort((a, b) => {
            return a.props.price - b.props.price;
        });
        this.empty_orders = !rows.length;

        // console.log("this.empty_orders", this.empty_orders, activeTab)

        contentContainer = (
            <TransitionWrapper className={"open-orders-container"} component="div" transitionName="newrow">
                {rows.length ? rows : emptyRow}
            </TransitionWrapper>
        );


        return (
            <div key="open_orders" className={this.props.className}>
                <div className="open-orders-wrap">
                    <MediaQuery {...ResponseOption.mobile}>
                        {(matches) => {
                            if (!matches) {
                                return (
                                    <Translate component={"div"} className={"open-orders-title"} content="exchange.my_orders" />
                                );
                            } else {
                                return null;
                            }
                        }}
                    </MediaQuery>

                    <div className="open-orders-table">
                        {contentContainer}
                    </div>
                </div>
            </div>
        );
    }
}

MyOpenOrders.defaultProps = {
    base: {},
    quote: {},
    orders: {},
    quoteSymbol: "",
    baseSymbol: ""
};

MyOpenOrders.propTypes = {
    base: PropTypes.object.isRequired,
    quote: PropTypes.object.isRequired,
    orders: PropTypes.object.isRequired,
    quoteSymbol: PropTypes.string.isRequired,
    baseSymbol: PropTypes.string.isRequired
};

export {OrderRow, TableHeader, MyOpenOrders};

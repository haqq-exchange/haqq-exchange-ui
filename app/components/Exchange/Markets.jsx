import React from "react";
import MyMarkets from "./MyMarkets/index";

class Markets extends React.Component {

    static defaultProps = {
        marketsObject: {}
    };

    constructor() {
        super();
        this.state = {
            height: null
        };

        this._setDimensions = this._setDimensions.bind(this);
    }

    UNSAFE_componentWillMount() {
        window.addEventListener("resize", this._setDimensions, {
            capture: false,
            passive: true
        });
    }

    componentDidMount() {
        this._setDimensions();
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this._setDimensions);
    }

    _setDimensions() {
        let height = this.refsWrapper.offsetHeight;

        if (height !== this.state.height) {
            this.setState({height});
        }
    }

    render() {
        const {marketsObject} = this.props;
        const markets = {
            ...{
                className: "no-overflow",
                listHeight: null,
                style: {width: "100%", padding: 20},
                headerStyle: {paddingTop: 0, borderTop: "none"},
                columns: [
                    {name: "star", index: 1},
                    {name: "market", index: 2},
                    {name: "quoteSupply", index: 3},
                    {name: "vol", index: 4},
                    {name: "price", index: 5},
                    {name: "change", index: 6}
                ]
            },
            ...marketsObject
        };
        if( this.state.height ) {
            markets["listHeight"] = this.state.height;
        }

        console.log("this.props", this.props);
        console.log("marketsObject", marketsObject);
        console.log("marketsObject", markets);
        return (
            <div ref={ref=>this.refsWrapper=ref} className="grid-block page-layout no-overflow">
                <MyMarkets {...markets} />
            </div>
        );
    }
}

export default Markets;

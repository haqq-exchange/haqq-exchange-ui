import React from "react";
import PropTypes from "prop-types";
import Immutable from "immutable";
import Ps from "perfect-scrollbar";
import Translate from "react-translate-component";
import market_utils from "common/market_utils";
import PriceText from "../Utility/PriceText";
import cnames from "classnames";
import SettingsActions from "actions/SettingsActions";
import SettingsStore from "stores/SettingsStore";
import {connect} from "alt-react";
import TransitionWrapper from "../Utility/TransitionWrapper";
import AssetName from "../Utility/AssetName";
import {ChainTypes as grapheneChainTypes} from "deexjs";
const {operations} = grapheneChainTypes;
import BlockDate from "../Utility/BlockDate";
import counterpart from "counterpart";
import ReactTooltip from "react-tooltip";
import getLocale from "browser-locale";
import {FillOrder} from "common/MarketClasses";

class MarketHistory extends React.Component {
    constructor(props) {
        super();
        this.state = {
            activeTab: props.viewSettings.get("historyTab", "history")
        };
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (
            !Immutable.is(nextProps.history, this.props.history) ||
            nextProps.baseSymbol !== this.props.baseSymbol ||
            nextProps.quoteSymbol !== this.props.quoteSymbol ||
            nextProps.className !== this.props.className ||
            nextState.activeTab !== this.state.activeTab ||
            nextProps.currentAccount !== this.props.currentAccount
        );
    }

    componentDidMount() {
        // let historyContainer = this.refs.history;
        // Ps.initialize(historyContainer);
        setTimeout(ReactTooltip.rebuild, 1000);
    }

    componentDidUpdate() {
        // let historyContainer = this.refs.history;
        // Ps.update(historyContainer);
    }

    _changeTab(tab) {
        SettingsActions.changeViewSetting({
            historyTab: tab
        });
        this.setState({
            activeTab: tab
        });

        // Ensure that focus goes back to top of scrollable container when tab is changed
        // let historyNode = this.refs.history;
        // historyNode.scrollTop = 0;
        // Ps.update(historyNode);

        setTimeout(ReactTooltip.rebuild, 1000);
    }

    render() {
        let {
            history,
            myHistory,
            base,
            quote,
            baseSymbol,
            quoteSymbol,
            isNullAccount
        } = this.props;
        let {activeTab} = this.state;
        let historyRows = <div />;

        if (isNullAccount) {
            activeTab = "history";
        }

        const assets = {
            [quote.get("id")]: {
                precision: quote.get("precision")
            },
            [base.get("id")]: {
                precision: base.get("precision")
            }
        };
        if (activeTab === "my_history" && (myHistory && myHistory.size)) {
            historyRows = myHistory
                .filter(a => {
                    let opType = a.getIn(["op", 0]);
                    return opType === operations.fill_order;
                })
                .filter(a => {
                    let quoteID = quote.get("id");
                    let baseID = base.get("id");
                    let pays = a.getIn(["op", 1, "pays", "asset_id"]);
                    let receives = a.getIn(["op", 1, "receives", "asset_id"]);
                    let hasQuote = quoteID === pays || quoteID === receives;
                    let hasBase = baseID === pays || baseID === receives;
                    return hasQuote && hasBase;
                })
                .sort((a, b) => {
                    return b.get("block_num") - a.get("block_num");
                })
                .map(trx => {
                    let fill = new FillOrder(
                        trx.toJS(),
                        assets,
                        quote.get("id")
                    );

                    return (
                        <div className={"table-history-row"} key={fill.id}>
                            <div className={"table-history-item " + fill.className}>
                                <PriceText
                                    price={fill.getPrice()}
                                    base={this.props.base}
                                    quote={this.props.quote}
                                />
                            </div>
                            <div className={"table-history-item amount-receive"}>{fill.amountToReceive()}</div>
                            <div className={"table-history-item amount-pay"}>{fill.amountToPay()}</div>
                            <BlockDate
                                className={"table-history-item time"}
                                component="div"
                                block_number={fill.block}
                                tooltip
                            />
                        </div>
                    );
                })
                .toArray();
        } else if (history && history.size) {
            let index = 0;
            historyRows = this.props.history
                .filter(() => {
                    index++;
                    return index % 2 === 0;
                })
                .take(100)
                .map(fill => {
                    return (
                        <div className={"table-history-row"} key={"history_" + fill.id}>
                            <div className={"table-history-item " + fill.className}>
                                <PriceText
                                    price={fill.getPrice()}
                                    base={this.props.base}
                                    quote={this.props.quote}
                                />
                            </div>
                            <div className={"table-history-item amount-receive"}>{fill.amountToReceive()}</div>
                            <div className={"table-history-item amount-pay"}>{fill.amountToPay()}</div>
                            <div
                                className={"table-history-item time"}
                                data-tip={fill.time}
                            >
                                {counterpart.localize(fill.time, {
                                    type: "date",
                                    format: "market_history_small"
                                })}
                            </div>
                        </div>
                    );
                })
                .toArray();
        }

        let hc = this.props.className+"-tab";

        return (
            <div className={this.props.className}>
                <div className={this.props.className+"-wrap"}>
                    <div className={this.props.className+"-top"}>
                        <div className={this.props.className+"-tabs"}>
                            <div
                                className={cnames(hc, {
                                    disabled: isNullAccount,
                                    clickable: !isNullAccount,
                                    active: activeTab !== "history"
                                })}
                                onClick={this._changeTab.bind(
                                    this,
                                    "my_history"
                                )}
                            >
                                <Translate content="exchange.my_history" />
                            </div>
                            <div
                                className={cnames(hc, {
                                    active: activeTab === "history",
                                    clickable: true
                                })}
                                onClick={this._changeTab.bind(this, "history")}
                            >
                                <Translate content="exchange.history" />
                            </div>
                        </div>
                    </div>
                    <div className={"table-history"}>
                        <div className={"table-history-header"}>
                            <div className={"table-history-header-row"}>
                                <Translate
                                    component={"div"}
                                    className="table-history-header-item exchange-price"
                                    content="exchange.price"
                                />
                                <div className="table-history-header-item asset-name">
                                    <AssetName
                                        dataPlace="top"
                                        name={quoteSymbol}
                                    />
                                </div>
                                <div className="table-history-header-item asset-name">
                                    <AssetName
                                        dataPlace="top"
                                        name={baseSymbol}
                                    />
                                </div>
                                <div className="table-history-header-item block-date">
                                    <Translate
                                        className="header-sub-title"
                                        content="explorer.block.date"
                                    />
                                </div>
                            </div>
                        </div>
                        <TransitionWrapper
                            component="div"
                            className={"table-history-body"}
                            transitionName="newrow">
                            {historyRows}
                        </TransitionWrapper>
                    </div>

                </div>
            </div>
        );
    }
}

MarketHistory.defaultProps = {
    history: []
};

MarketHistory.propTypes = {
    history: PropTypes.object.isRequired
};

export default connect(
    MarketHistory,
    {
        listenTo() {
            return [SettingsStore];
        },
        getProps() {
            return {
                viewSettings: SettingsStore.getState().viewSettings
            };
        }
    }
);

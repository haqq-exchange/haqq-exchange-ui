import React from "react";
import PropTypes from "prop-types";
import MarketsActions from "actions/MarketsActions";
import {MyOpenOrders} from "./MyOpenOrders";
import {debounce} from "lodash";
import notify from "actions/NotificationActions";
import AccountNotifications from "../Notifier/NotifierContainer";
import Ps from "perfect-scrollbar";
import {ChainStore, FetchChain} from "deexjs";
import SettingsActions from "actions/SettingsActions";
import cnames from "classnames";
import utils from "common/utils";
import market_utils from "common/market_utils";
import {Asset, LimitOrderCreate, Price} from "common/MarketClasses";
import ConfirmOrderModal from "./ConfirmOrderModal";
import Translate from "react-translate-component";
import {Apis} from "deexjs-ws";
import {checkFeeStatusAsync} from "common/trxHelper";
import LoadingIndicator from "../LoadingIndicator";
import moment from "moment";
import loadable from "loadable-components";
import {getAlias} from "config/alias";
import "./Exchange.scss";
import {ymSend} from "../../lib/common/metrika";
import TransactionConfirmStore from "../../stores/TransactionConfirmStore";

const BuySell = loadable(() => import("./BuySell"));
const OrderBook = loadable(() => import("./OrderBook"));

class Buy extends React.Component {
    static propTypes = {
        marketCallOrders: PropTypes.object.isRequired,
        viewSettings: PropTypes.object.isRequired,
    };

    static defaultProps = {
        marketCallOrders: [],
        viewSettings: {},
    };

    constructor(props) {
        super();
        this.state = {
            ...this._initialState(props),
            expirationType: {
                bid: props.exchange.getIn(["lastExpiration", "bid"]) || "YEAR",
                ask: props.exchange.getIn(["lastExpiration", "ask"]) || "YEAR"
            },
            expirationCustomTime: {
                bid: moment().add(1, "day"),
                ask: moment().add(1, "day")
            },
            showSell: true,
            feeStatus: {},

            sellFeeAsset: null,
            sellFeeAssets: null,
            sellFee: null,
            buyFeeAsset: null,
            buyFeeAssets: null,
            buyFee: null,
            createOrder: false
        };

        this._setDefaultPrice = debounce(this._setDefaultPrice.bind(this), 500);
        this._getWindowSize = debounce(this._getWindowSize.bind(this), 150);
        this._currentPriceClick = this._currentPriceClick.bind(this);
        //this._checkFeeStatus = this._checkFeeStatus.bind(this);

        // this._handleExpirationChange = this._handleExpirationChange.bind(this);
        this._handleCustomExpirationChange = this._handleCustomExpirationChange.bind(
            this
        );
        this.psInit = true;
    }

    _handleExpirationChange(type, e) {
        let expirationType = {
            ...this.state.expirationType,
            [type]: e.target.value
        };

        if (e.target.value !== "SPECIFIC") {
            SettingsActions.setExchangeLastExpiration({
                ...((this.props.exchange.has("lastExpiration") &&
                    this.props.exchange.get("lastExpiration").toJS()) ||
                    {}),
                [type]: e.target.value
            });
        }

        this.setState({
            expirationType: expirationType
        });
    }

    _handleCustomExpirationChange(type, time) {
        let expirationCustomTime = {
            ...this.state.expirationCustomTime,
            [type]: time
        };
        this.setState({
            expirationCustomTime: expirationCustomTime
        });
    }

    EXPIRATIONS = {
        HOUR: {
            title: "1 hour",
            get: () =>
                moment()
                    .add(1, "hour")
                    .valueOf()
        },
        "12HOURS": {
            title: "12 hours",
            get: () =>
                moment()
                    .add(12, "hour")
                    .valueOf()
        },
        "24HOURS": {
            title: "24 hours",
            get: () =>
                moment()
                    .add(1, "day")
                    .valueOf()
        },
        "7DAYS": {
            title: "7 days",
            get: () =>
                moment()
                    .add(7, "day")
                    .valueOf()
        },
        MONTH: {
            title: "30 days",
            get: () =>
                moment()
                    .add(30, "day")
                    .valueOf()
        },
        YEAR: {
            title: "1 year",
            get: () =>
                moment()
                    .add(1, "year")
                    .valueOf()
        },
        SPECIFIC: {
            title: "Specific",
            get: type => {
                return this.state.expirationCustomTime[type].valueOf();
            }
        }
    };

    _initialState(props) {
        let ws = props.viewSettings;
        let bid = {
            forSaleText: "",
            toReceiveText: "",
            priceText: "",
            for_sale: new Asset({
                asset_id: props.baseAsset.get("id"),
                precision: props.baseAsset.get("precision")
            }),
            to_receive: new Asset({
                asset_id: props.quoteAsset.get("id"),
                precision: props.quoteAsset.get("precision")
            })
        };
        bid.price = new Price({base: bid.for_sale, quote: bid.to_receive});
        let ask = {
            forSaleText: "",
            toReceiveText: "",
            priceText: "",
            for_sale: new Asset({
                asset_id: props.quoteAsset.get("id"),
                precision: props.quoteAsset.get("precision")
            }),
            to_receive: new Asset({
                asset_id: props.baseAsset.get("id"),
                precision: props.baseAsset.get("precision")
            })
        };
        ask.price = new Price({base: ask.for_sale, quote: ask.to_receive});

        return {
            history: [],
            buySellOpen: ws.get("buySellOpen", true),
            bid,
            ask,
            flipBuySell: ws.get("flipBuySell", false),
            favorite: false,
            leftOrderBook: ws.get("leftOrderBook", false),
            buyDiff: false,
            sellDiff: false,
            buySellTop: ws.get("buySellTop", true),
            buyFeeAssetIdx: ws.get("buyFeeAssetIdx", 0),
            sellFeeAssetIdx: ws.get("sellFeeAssetIdx", 0),
            height: window.innerHeight,
            width: window.innerWidth,
            hidePanel: false,
            currentPeriod: ws.get("currentPeriod", 3600 * 24 * 30 * 3) // 3 months
        };
    }

    _getLastMarketKey() {
        const chainID = Apis.instance().chain_id;
        return `lastMarket${chainID ? "_" + chainID.substr(0, 8) : ""}`;
    }

    componentDidMount() {
        this._checkFeeStatus();
        this._setDefaultPrice();

        SettingsActions.changeViewSetting.defer({
            [this._getLastMarketKey()]:
                this.props.quoteAsset.get("symbol") +
                "_" +
                this.props.baseAsset.get("symbol")
        });

        window.addEventListener("resize", this._getWindowSize, {
            capture: false,
            passive: true
        });
    }

    _getWindowSize() {
        let {innerHeight, innerWidth} = window;
        if (
            innerHeight !== this.state.height ||
            innerWidth !== this.state.width
        ) {
            this.setState({
                height: innerHeight,
                width: innerWidth
            });
            let centerContainer = this.refsCenter;
            if (centerContainer) {
                Ps.update(centerContainer);
            }
        }
    }

    shouldComponentUpdate(np, ns) {
        if (!np.marketReady && !this.props.marketReady) {
            return false;
        }
        let propsChanged = false;
        for (let key in np) {
            if (np.hasOwnProperty(key)) {
                propsChanged =
                    propsChanged ||
                    !utils.are_equal_shallow(np[key], this.props[key]);
                if (propsChanged) break;
            }
        }
        return propsChanged || !utils.are_equal_shallow(ns, this.state);
    }

    async _checkFeeStatus(
        assets = [
            this.props.coreAsset,
            this.props.baseAsset,
            this.props.quoteAsset
        ],
        account = this.props.currentAccount
    ) {
        const {feeSettingsAssets} = this.props;
        const [coreAsset, baseAsset, quoteAsset] = assets;
        let feeStatus = {};
        let p = [];
        let feeAssets = await FetchChain("getAsset", feeSettingsAssets);
        let listAssets = assets.concat(feeAssets.toArray());
        await utils.asyncForEach(listAssets, async asset => {
            if (asset) {
                return p.push(
                    await checkFeeStatusAsync({
                        accountID: account.get("id"),
                        feeID: asset.get("id") || null,
                        type: "limit_order_create"
                    })
                );
            }
        });

        listAssets.forEach((a, idx) => {
            feeStatus[a.get("id")] = p[idx];
        });
        if (!utils.are_equal_shallow(this.state.feeStatus, feeStatus)) {
            this.setState({
                feeStatus
            });
            Promise.all(p)
                .then(status => {

                    assets.forEach((a, idx) => {
                        feeStatus[a.get("id")] = status[idx];
                    });
                    if (!utils.are_equal_shallow(this.state.feeStatus, feeStatus)) {
                        this.setState({
                            feeStatus
                        });
                    }
                })
                .catch(err => {
                    this.setState({feeStatus: {}});
                });
        }
        this._getFeeAssets(quoteAsset, baseAsset, coreAsset);
    }

    componentDidUpdate(nextProps) {
        this._initPsContainer();
        let changeAssets =
            !nextProps.quoteAsset.equals(this.props.quoteAsset) ||
            !nextProps.baseAsset.equals(this.props.baseAsset) ||
            !nextProps.coreAsset.equals(this.props.coreAsset);
        let changeAccount =
            nextProps.currentAccount.get("name") !==
            this.props.currentAccount.get("name");

        if (changeAssets || changeAccount) {
            this._checkFeeStatus(
                [
                    nextProps.coreAsset,
                    nextProps.baseAsset,
                    nextProps.quoteAsset
                ],
                nextProps.currentAccount
            );

            this.setState(this._initialState(nextProps));

            if (changeAssets) {
                this._setDefaultPrice();
                SettingsActions.changeViewSetting({
                    [this._getLastMarketKey()]: [
                        nextProps.quoteAsset.get("symbol"),
                        nextProps.baseAsset.get("symbol")
                    ].join("_")
                });
            }
        }
    }

    _initPsContainer() {
        if (this.refsCenter && this.psInit) {
            let centerContainer = this.refsCenter;
            if (centerContainer) {
                Ps.initialize(centerContainer);
                this.psInit = false;
            }
        }
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this._getWindowSize);
    }

    async _getFeeAssets(quote, base, coreAsset) {
        let {currentAccount, feeSettingsAssets} = this.props;
        const {feeStatus} = this.state;

        function addMissingAsset(target, asset) {
            if (target.indexOf(asset) === -1) {
                target.push(asset);
            }
        }

        function hasFeePoolBalance(id) {
            return feeStatus[id] && feeStatus[id].hasPoolBalance;
        }

        function hasBalance(id) {
            return feeStatus[id] && feeStatus[id].hasBalance;
        }

        let feeAssets = await FetchChain("getAsset", feeSettingsAssets);

        let filterUnique = (obj, asset) => {
            if (!obj[asset.get("id")]) {
                obj[asset.get("id")] = asset;
            }
            return obj;
        };
        let sellAssets = (__DEEX_CHAIN__ || __SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__) ? (feeAssets
            .toArray()
            .concat([quote.get("id") === coreAsset.get("id") ? base : quote])
            .reduce(filterUnique, {})
        ) : [coreAsset, quote.get("id") === coreAsset.get("id") ? base : quote];
        let buyAssets = (__DEEX_CHAIN__ || __SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__) ? (feeAssets
            .toArray()
            .concat([base.get("id") === coreAsset.get("id") ? quote : base])
            .reduce(filterUnique, {})
        ) : [coreAsset, base.get("id") === coreAsset.get("id") ? quote : base];

        sellAssets = Object.values(sellAssets);
        buyAssets = Object.values(buyAssets);



        addMissingAsset(sellAssets, quote);
        addMissingAsset(sellAssets, base);
        // let sellFeeAsset;
        addMissingAsset(buyAssets, quote);
        addMissingAsset(buyAssets, base);
        // let buyFeeAsset;

        let balances = {};

        let assetsId = feeAssets
            .toArray()
            .map(asset => asset.get("id"))
            .concat([quote.get("id"), base.get("id")]);
        currentAccount
            .get("balances", [])
            .filter((balance, id) => {
                return assetsId.includes(id);
            })
            .map((balance, id) => {
                let balanceObject = ChainStore.getObject(balance);
                balances[id] = {
                    balance: balanceObject
                        ? parseInt(balanceObject.get("balance"), 10)
                        : 0,
                    fee: this._getFee(ChainStore.getAsset(id))
                };
                return balances;
            });

        function filterAndDefault(assets, balances, idx) {
            let asset;
            /* Only keep assets for which the user has a balance larger than the fee, and for which the fee pool is valid */
            assets = assets.filter(a => {
                if (!balances[a.get("id")]) {
                    return false;
                }
                return (
                    hasFeePoolBalance(a.get("id")) && hasBalance(a.get("id"))
                );
            });


            /* If the user has no valid balances, default to core fee */
            if (!assets.length) {
                asset = coreAsset;
                assets.push(coreAsset);
                /* If the user has balances, use the stored idx value unless that asset is no longer available*/
            } else {
                asset = assets.find(item => item.get("symbol") === idx);
                if (!asset) {
                    asset = assets[0];
                }
            }

            return {assets, asset};
        }

        let {assets: sellFeeAssets, asset: sellFeeAsset} = filterAndDefault(
            sellAssets,
            balances,
            this.state.sellFeeAssetIdx
        );

        let {assets: buyFeeAssets, asset: buyFeeAsset} = filterAndDefault(
            buyAssets,
            balances,
            this.state.buyFeeAssetIdx
        );

        if (!sellFeeAsset || !buyFeeAsset) {
            if (!sellFeeAsset) {
                this.onChangeFeeAsset("sell", sellFeeAssets[0].get("symbol"));
            }
            if (!buyFeeAsset) {
                this.onChangeFeeAsset("buy", buyFeeAssets[0].get("symbol"));
            }
            return false;
        }

        let sellFee = await this._getFee(sellFeeAsset);
        let buyFee = await this._getFee(buyFeeAsset);

        this.setState({
            sellFeeAsset,
            sellFeeAssets,
            sellFee,
            buyFeeAsset,
            buyFeeAssets,
            buyFee
        });
    }

    _getFee(asset = this.props.coreAsset) {
        return (
            this.state.feeStatus[asset.get("id")] &&
            this.state.feeStatus[asset.get("id")].fee
        );
    }

    _verifyFee(fee, sell, sellBalance, coreBalance) {
        let coreFee = this._getFee();

        if (fee.asset_id === "1.3.0") {
            if (coreFee.getAmount() <= coreBalance) {
                return "1.3.0";
            } else {
                return null;
            }
        } else {
            let sellSum =
                sell.asset_id === fee.asset_id
                    ? fee.getAmount() + sell.getAmount()
                    : sell.getAmount();
            if (sellSum <= sellBalance) {
                // Sufficient balance in asset to pay fee
                return fee.asset_id;
            } else if (
                coreFee.getAmount() <= coreBalance &&
                fee.asset_id !== "1.3.0"
            ) {
                // Sufficient balance in core asset to pay fee
                return "1.3.0";
            } else {
                return null; // Unable to pay fee
            }
        }
    }

    _sendBuySell = (type, asset1, asset2) => {
        try {
            let sendData = [
                type.toUpperCase(),
                asset1.get("symbol").toUpperCase(),
                asset2.get("symbol").toUpperCase()
            ].join("_");
            const pairAssets = [
                "SELL_BTS_DEEX",
                "BUY_BTS_DEEX",
                "SELL_DEEX_BTS",
                "BUY_DEEX_BTS",
                "SELL_DEEX.ETH_BTS",
                "BUY_DEEX.ETH_BTS",
                "SELL_BTS_DEEX.ETH",
                "BUY_BTS_DEEX.ETH"
            ];
            if (pairAssets.includes(sendData)) {
                window.console.log("sendData", sendData);
                ymSend("reachGoal", sendData);
            }
        } catch (e){
            console.log(e);
        }
    };

    _createLimitOrderConfirm(
        buyAsset,
        sellAsset,
        sellBalance,
        coreBalance,
        feeAsset,
        type,
        short = true,
        e
    ) {
        let {highestBid, lowestAsk} = this.props.marketData;
        let current = this.state[type === "sell" ? "ask" : "bid"];
        let chainSellBalance = ChainStore.getObject(sellBalance);
        let chainCoreBalance = ChainStore.getObject(coreBalance);

        sellBalance = current.for_sale.clone(
            sellBalance && chainSellBalance
                ? parseInt(chainSellBalance.toJS().balance, 10)
                : 0
        );
        coreBalance = new Asset({
            amount:
                coreBalance && chainCoreBalance
                    ? parseInt(chainCoreBalance.toJS().balance, 10)
                    : 0
        });

        let fee = this._getFee(feeAsset);
        let feeID = this._verifyFee(
            fee,
            current.for_sale,
            sellBalance.getAmount(),
            coreBalance.getAmount()
        );
        if (!feeID) {
            return notify.addNotification({
                message: "Insufficient funds to pay fees",
                level: "error"
            });
        }

        if (type === "buy" && lowestAsk) {
            let diff = this.state.bid.price.toReal() / lowestAsk.getPrice();
            if (diff > 1.2) {
                this.refBuy.show();
                return this.setState({
                    buyDiff: diff
                });
            }
        } else if (type === "sell" && highestBid) {
            let diff =
                1 / (this.state.ask.price.toReal() / highestBid.getPrice());
            if (diff > 1.2) {
                this.refSell.show();
                return this.setState({
                    sellDiff: diff
                });
            }
        }

        let isPredictionMarket = sellAsset.getIn([
            "bitasset",
            "is_prediction_market"
        ]);

        if (current.for_sale.gt(sellBalance) && !isPredictionMarket) {
            return notify.addNotification({
                message:
                    "Insufficient funds to place order, you need at least " +
                    current.for_sale.getAmount({real: true}) +
                    " " +
                    sellAsset.get("symbol"),
                level: "error"
            });
        }
        //
        if (
            !(
                current.for_sale.getAmount() > 0 &&
                current.to_receive.getAmount() > 0
            )
        ) {
            return notify.addNotification({
                message: "Please enter a valid amount and price",
                level: "error"
            });
        }
        //
        if (type === "sell" && isPredictionMarket && short) {
            return this._createPredictionShort(feeID);
        }

        this.setState({createOrder: true});
        this._createLimitOrder(type, feeID, {buyAsset, sellAsset});
    }

    _createLimitOrder(type, feeID, data) {
        let actionType = type === "sell" ? "ask" : "bid";

        let current = this.state[actionType];

        let expirationTime = null;
        if (this.state.expirationType[actionType] === "SPECIFIC") {
            expirationTime = this.EXPIRATIONS[
                this.state.expirationType[actionType]
            ].get(actionType);
        } else {
            expirationTime = this.EXPIRATIONS[
                this.state.expirationType[actionType]
            ].get();
        }

        const order = new LimitOrderCreate({
            for_sale: current.for_sale,
            expiration: new Date(expirationTime || false),
            to_receive: current.to_receive,
            seller: this.props.currentAccount.get("id"),
            fee: {
                asset_id: feeID,
                amount: 0
            }
        });
        const {marketName, first} = market_utils.getMarketName(
            this.props.baseAsset,
            this.props.quoteAsset
        );
        const inverted = this.props.marketDirections.get(marketName);
        const shouldFlip =
            (inverted && first.get("id") !== this.props.baseAsset.get("id")) ||
            (!inverted && first.get("id") !== this.props.baseAsset.get("id"));
        if (shouldFlip) {
            let setting = {};
            setting[marketName] = !inverted;
            SettingsActions.changeMarketDirection(setting);
        }

        TransactionConfirmStore.listen(event =>
            this.onFinishConfirm(event, {type, ...data})
        );

        return MarketsActions.createLimitOrder2(order)
            .then(result => {
                if (result.error) {
                    if (result.error.message !== "wallet locked")
                        notify.addNotification({
                            message:
                                "Unknown error. Failed to place order for " +
                                current.to_receive.getAmount({real: true}) +
                                " " +
                                current.to_receive.asset_id,
                            level: "error"
                        });
                }
            })
            .catch(e => {
                // console.log("order failed:", e);
            });
    }

    onFinishConfirm(confirm_store_state, data) {
        const {createOrder} = this.state;
        if (
            confirm_store_state.included &&
            confirm_store_state.broadcasted_transaction &&
            createOrder
        ) {
            this.setState({createOrder: false});
            this._sendBuySell(data.type, data.buyAsset, data.sellAsset);
        }
    }

    _forceBuy(type, feeAsset, sellBalance, coreBalance) {
        let current = this.state[type === "sell" ? "ask" : "bid"];
        // Convert fee to relevant asset fee and check if user has sufficient balance
        sellBalance = current.for_sale.clone(
            sellBalance
                ? parseInt(ChainStore.getObject(sellBalance).get("balance"), 10)
                : 0
        );
        coreBalance = new Asset({
            amount: coreBalance
                ? parseInt(ChainStore.getObject(coreBalance).toJS().balance, 10)
                : 0
        });
        let fee = this._getFee(feeAsset);
        let feeID = this._verifyFee(
            fee,
            current.for_sale,
            sellBalance.getAmount(),
            coreBalance.getAmount()
        );

        if (feeID) {
            this._createLimitOrder(type, feeID);
        } else {
            console.error("Unable to pay fees, aborting limit order creation");
        }
    }

    _forceSell(type, feeAsset, sellBalance, coreBalance) {
        let current = this.state[type === "sell" ? "ask" : "bid"];
        // Convert fee to relevant asset fee and check if user has sufficient balance
        sellBalance = current.for_sale.clone(
            sellBalance
                ? parseInt(ChainStore.getObject(sellBalance).get("balance"), 10)
                : 0
        );
        coreBalance = new Asset({
            amount: coreBalance
                ? parseInt(ChainStore.getObject(coreBalance).toJS().balance, 10)
                : 0
        });
        let fee = this._getFee(feeAsset);
        let feeID = this._verifyFee(
            fee,
            current.for_sale,
            sellBalance.getAmount(),
            coreBalance.getAmount()
        );

        if (feeID) {
            this._createLimitOrder(type, feeID);
        } else {
            console.error("Unable to pay fees, aborting limit order creation");
        }
    }

    _cancelLimitOrder(orderID, e) {
        e.preventDefault();
        let {currentAccount} = this.props;
        MarketsActions.cancelLimitOrder(
            currentAccount.get("id"),
            orderID // order id to cancel
        );
    }

    _flipBuySell() {
        this.setState({
            flipBuySell: !this.state.flipBuySell
        });

        SettingsActions.changeViewSetting({
            flipBuySell: !this.state.flipBuySell
        });
    }

    _toggleOpenBuySell() {
        SettingsActions.changeViewSetting({
            buySellOpen: !this.state.buySellOpen
        });

        this.setState({buySellOpen: !this.state.buySellOpen});
    }

    _moveOrderBook() {
        SettingsActions.changeViewSetting({
            leftOrderBook: !this.state.leftOrderBook
        });

        this.setState({leftOrderBook: !this.state.leftOrderBook});
    }

    _currentPriceClick(type, price) {
        const isBid = type === "bid";
        let current = this.state[type];
        current.price = price[isBid ? "invert" : "clone"]();
        current.priceText = current.price.toReal();
        if (isBid) {
            this._setForSale(current, isBid) ||
                this._setReceive(current, isBid);
        } else {
            this._setReceive(current, isBid) ||
                this._setForSale(current, isBid);
        }
        this.forceUpdate();
    }

    _orderbookClick(order) {
        const isBid = order.isBid();

        /*
         * Because we are using a bid order to construct an ask and vice versa,
         * totalToReceive becomes forSale, and totalForSale becomes toReceive
         */
        let forSale = order.totalToReceive({noCache: true});

        // let toReceive = order.totalForSale({noCache: true});
        let toReceive = forSale.times(order.sellPrice());

        let newPrice = new Price({
            base: isBid ? toReceive : forSale,
            quote: isBid ? forSale : toReceive
        });
        let current = this.state[isBid ? "bid" : "ask"];
        current.price = newPrice;
        current.priceText = newPrice.toReal();

        let newState = {
            // If isBid is true, newState defines a new ask order and vice versa
            [isBid ? "ask" : "bid"]: {
                for_sale: forSale,
                forSaleText: forSale.getAmount({real: true}),
                to_receive: toReceive,
                toReceiveText: toReceive.getAmount({real: true}),
                price: newPrice,
                priceText: newPrice.toReal(),
                idOrder: order.id
            }
        };

        this.setState(newState, () => {
            try {
                if (isBid) {
                    this._setForSale(current, isBid) ||
                        this._setReceive(current, isBid);
                } else {
                    this._setReceive(current, isBid) ||
                        this._setForSale(current, isBid);
                }
            } catch (e) {
                console.log("e", e);
            }
        });
    }

    _borrowBase() {
        this.refs.borrowBase.show();
    }

    _getSettlementInfo() {
        let {lowestCallPrice, feedPrice, quoteAsset} = this.props;

        let showCallLimit = false;
        if (feedPrice) {
            if (feedPrice.inverted) {
                showCallLimit = lowestCallPrice <= feedPrice.toReal();
            } else {
                showCallLimit = lowestCallPrice >= feedPrice.toReal();
            }
        }
        return !!(
            showCallLimit &&
            lowestCallPrice &&
            !quoteAsset.getIn(["bitasset", "is_prediction_market"])
        );
    }

    async onChangeFeeAsset(type, value) {
        if (type === "buy") {
            this.setState({
                buyFeeAssetIdx: value
            });

            await SettingsActions.changeViewSetting({
                buyFeeAssetIdx: value
            });
        } else {
            this.setState({
                sellFeeAssetIdx: value
            });

            await SettingsActions.changeViewSetting({
                sellFeeAssetIdx: value
            });
        }

        this._getFeeAssets(
            this.props.quoteAsset,
            this.props.baseAsset,
            this.props.coreAsset
        );
    }

    _toggleBuySellPosition() {
        this.setState({
            buySellTop: !this.state.buySellTop
        });

        SettingsActions.changeViewSetting({
            buySellTop: !this.state.buySellTop
        });
    }
    
    _setDefaultPrice(){
        const {marketData} = this.props;
        const {lowestAsk, highestBid} = marketData;

        this._onInputPrice("ask", highestBid.getPrice());
        this._onInputPrice("bid", lowestAsk.getPrice());
    }

    _setReceive(state) {
        if (state.price.isValid() && state.for_sale.hasAmount()) {
            state.to_receive = state.for_sale.times(state.price);
            state.toReceiveText = state.to_receive
                .getAmount({real: true})
                .toString();
            return true;
        }
        return false;
    }

    _setForSale(state) {
        if (state.price.isValid() && state.to_receive.hasAmount()) {
            state.for_sale = state.to_receive.times(state.price, true);
            state.forSaleText = state.for_sale
                .getAmount({real: true})
                .toString();
            return true;
        }
        return false;
    }

    _setPrice(state) {
        if (state.for_sale.hasAmount() && state.to_receive.hasAmount()) {
            state.price = new Price({
                base: state.for_sale,
                quote: state.to_receive
            });
            console.log("priceText", state.price.toReal().toString());
            state.priceText = state.price.toReal().toString();
            return true;
        }
        return false;
    }

    _setPriceText(state, isBid) {
        const currentBase = state[isBid ? "for_sale" : "to_receive"];
        const currentQuote = state[isBid ? "to_receive" : "for_sale"];
        if (currentBase.hasAmount() && currentQuote.hasAmount()) {
            let priceText = new Price({
                base: currentBase,
                quote: currentQuote
            })
                .toReal()
                .toString();
            
            console.log("priceText", priceText);
            state.priceText = this._checkPriceValue(priceText);
        }
    }

    _checkPriceValue = (priceValue) => {
        if( priceValue ) {
            const {floatPriceOrderSell, baseAsset, quoteAsset} = this.props;
            let decimal = priceValue.toString().split(".");
            let decimalCount = decimal?.[1] ? decimal[1].length : 0;
            if (
                (floatPriceOrderSell.includes(baseAsset.get("symbol")) ||
                    floatPriceOrderSell.includes(quoteAsset.get("symbol"))) &&
                decimalCount > 1
            ) {
                priceValue = parseFloat(priceValue).toFixed(2);
            }
        }

        return priceValue;
    }

    _onInputPrice(type, value) {
        let priceValue = this._checkPriceValue(value);
        let current = this.state[type];
        const isBid = type === "bid";
        current.price = new Price({
            base: current[isBid ? "for_sale" : "to_receive"],
            quote: current[isBid ? "to_receive" : "for_sale"],
            real: parseFloat(priceValue) || 0
        });

        if (isBid) {
            this._setForSale(current, isBid) ||
            this._setReceive(current, isBid);
        } else {
            this._setReceive(current, isBid) ||
            this._setForSale(current, isBid);
        }
        current.priceText = priceValue;
        this.forceUpdate();
    }

    _onInputSell(type, isBid, e) {
        let current = this.state[type];
        current.for_sale.setAmount({real: parseFloat(e.target.value) || 0});
        if (current.price.isValid()) {
            this._setReceive(current, isBid);
        } else {
            this._setPrice(current);
        }

        current.forSaleText = e.target.value;
        this._setPriceText(current, type === "bid");

        this.forceUpdate();
    }

    _onInputReceive(type, isBid, e) {
        let current = this.state[type];
        // const isBid = type === "bid";
        current.to_receive.setAmount({real: parseFloat(e.target.value) || 0});

        if (current.price.isValid()) {
            this._setForSale(current, isBid);
        } else {
            this._setPrice(current);
        }

        current.toReceiveText = e.target.value;
        this._setPriceText(current, type === "bid");
        this.forceUpdate();
    }

    isMarketFrozen() {
        let {baseAsset, quoteAsset} = this.props;

        let baseWhiteList = baseAsset
            .getIn(["options", "whitelist_markets"])
            .toJS();
        let quoteWhiteList = quoteAsset
            .getIn(["options", "whitelist_markets"])
            .toJS();
        let baseBlackList = baseAsset
            .getIn(["options", "blacklist_markets"])
            .toJS();
        let quoteBlackList = quoteAsset
            .getIn(["options", "blacklist_markets"])
            .toJS();

        if (
            quoteWhiteList.length &&
            quoteWhiteList.indexOf(baseAsset.get("id")) === -1
        ) {
            return {isFrozen: true, frozenAsset: quoteAsset.get("symbol")};
        }
        if (
            baseWhiteList.length &&
            baseWhiteList.indexOf(quoteAsset.get("id")) === -1
        ) {
            return {isFrozen: true, frozenAsset: baseAsset.get("symbol")};
        }

        if (
            quoteBlackList.length &&
            quoteBlackList.indexOf(baseAsset.get("id")) !== -1
        ) {
            return {isFrozen: true, frozenAsset: quoteAsset.get("symbol")};
        }
        if (
            baseBlackList.length &&
            baseBlackList.indexOf(quoteAsset.get("id")) !== -1
        ) {
            return {isFrozen: true, frozenAsset: baseAsset.get("symbol")};
        }

        return {isFrozen: false};
    }
 
    render() {
        let {
            currentAccount,
            marketLimitOrders,
            marketCallOrders,
            marketData,
            invertedCalls,
            quoteAsset,
            baseAsset,
            marketReady,
            marketSettleOrders,
            bucketSize,
            totals,
            buckets,
            coreAsset,
            floatPriceOrderSell
        } = this.props;

        const {combinedBids, combinedAsks, lowestAsk, highestBid} = marketData;

        let {
            bid,
            ask,
            leftOrderBook,
            buyDiff,
            sellDiff,
            width,
            buySellTop,
            sellFeeAsset,
            buyFeeAsset,
            buyFeeAssets,
            buyFee,
            assets
        } = this.state;

        const {isFrozen, frozenAsset} = this.isMarketFrozen();

        let base = null,
            quote = null,
            accountBalance = null,
            quoteBalance = null,
            baseBalance = null,
            coreBalance = null,
            quoteSymbol,
            baseSymbol,
            latest,
            changeClass;

        if (quoteAsset.size && baseAsset.size && currentAccount.size) {
            base = baseAsset;
            quote = quoteAsset;
            baseSymbol = base.get("symbol");
            quoteSymbol = quote.get("symbol");

            accountBalance = currentAccount.get("balances").toJS();

            if (accountBalance) {
                for (let id in accountBalance) {
                    if (id === quote.get("id")) {
                        quoteBalance = accountBalance[id];
                    }
                    if (id === base.get("id")) {
                        baseBalance = accountBalance[id];
                    }
                    if (id === "1.3.0") {
                        coreBalance = accountBalance[id];
                    }
                }
            }

            showCallLimit = this._getSettlementInfo();
        }

        let baseIsBitAsset = !!baseAsset.get("bitasset_data_id");

        // Fees
        if (!coreAsset || !Object.keys(this.state.feeStatus).length) {
            return null;
        }

        // Decimals
        let smallScreen = false;
        if (width < 1000) {
            smallScreen = true;
            leftOrderBook = false;
        }

        let expirationType = this.state.expirationType;
        let expirationCustomTime = this.state.expirationCustomTime;

        window.console.log("1", [baseSymbol, quoteSymbol])
        window.console.log("2",this.props.locale)
        window.console.log("3",this.props.dataFeed)
        window.console.log("5",leftOrderBook)
        window.console.log("6",marketReady)
        window.console.log("7",this.props.settings.get("themes"))
        window.console.log("8",buckets)
        window.console.log("9",bucketSize)
        window.console.log("10",this.state.currentPeriod)

        const assetOptions = quoteAsset.get("options"); 
        const assetDescription = JSON.parse(assetOptions.get("description"));
        const assetPicture = assetDescription.image;
        const assetMainDescription = assetDescription.main;

        return (
            <div className="exchange-wrapper">
                {!marketReady ? <LoadingIndicator /> : null}
                {isFrozen ? <FrozenInfo asset={frozenAsset} /> : null}

                <div className="exchange-page">
                    {console.log(
                        ">>>>>>>>>>>>>>",
                        `${quoteSymbol}_${baseSymbol}`
                    )}

                    <div style={{width: "100%"}}>
                    {combinedAsks.length > 0 ?    
                        <div className="chart-group buy-sell-group">
                            <div className="buy-sell-nft">
                                <div style={{backgroundImage: "url("+ `${assetPicture}` +")", height: "390px"}} />
                                <div style={{margin: "20px", textAlign: "center"}}>{quoteAsset.get("symbol")}</div>
                                <div style={{margin: "20px"}}>{assetMainDescription}</div>
                            </div>
                            <div className="buy-sell">                            
                                    <div className="buy-sell-form">
                                        <Translate
                                            component={"div"}
                                            className={"buy-sell-header"}
                                            asset={getAlias(
                                                quote.get("symbol")
                                            )}
                                            content="exchange.buy_asset"
                                        />
                                        <BuySell
                                            floatPriceOrderSell={floatPriceOrderSell}
                                            onBorrow={
                                                baseIsBitAsset
                                                    ? this._borrowBase.bind(
                                                          this
                                                      )
                                                    : null
                                            }
                                            currentAccount={currentAccount}
                                            backedCoin={this.props.backedCoins.find(
                                                a =>
                                                    a.symbol ===
                                                    base.get("symbol")
                                            )}
                                            currentBridges={
                                                this.props.bridgeCoins.get(
                                                    base.get("symbol")
                                                ) || null
                                            }
                                            smallScreen={smallScreen}
                                            isOpen={this.state.buySellOpen}
                                            onToggleOpen={this._toggleOpenBuySell.bind(
                                                this
                                            )}
                                            className={cnames("buy-sell")}
                                            type="bid"
                                            expirationType={
                                                expirationType["bid"]
                                            }
                                            expirations={this.EXPIRATIONS}
                                            expirationCustomTime={
                                                expirationCustomTime["bid"]
                                            }
                                            onExpirationTypeChange={this._handleExpirationChange.bind(
                                                this,
                                                "bid"
                                            )}
                                            onExpirationCustomChange={this._handleCustomExpirationChange.bind(
                                                this,
                                                "bid"
                                            )}
                                            amount={bid.toReceiveText}
                                            price={bid.priceText}
                                            total={bid.forSaleText}
                                            quote={quote}
                                            base={base}
                                            amountChange={event =>
                                                this._onInputReceive(
                                                    "bid",
                                                    true,
                                                    event
                                                )
                                            }
                                            priceChange={event =>
                                                this._onInputPrice("bid", event.target.value)
                                            }
                                            setPrice={this._currentPriceClick}
                                            totalChange={event =>
                                                this._onInputSell(
                                                    "bid",
                                                    false,
                                                    event
                                                )
                                            }
                                            balance={baseBalance}
                                            balanceId={base.get("id")}
                                            onSubmit={event =>
                                                this._createLimitOrderConfirm(
                                                    quote,
                                                    base,
                                                    baseBalance,
                                                    coreBalance,
                                                    buyFeeAsset,
                                                    "buy",
                                                    event
                                                )
                                            }
                                            balancePrecision={base.get(
                                                "precision"
                                            )}
                                            quotePrecision={quote.get(
                                                "precision"
                                            )}
                                            totalPrecision={base.get(
                                                "precision"
                                            )}
                                            currentPrice={lowestAsk.getPrice()}
                                            currentPriceObject={lowestAsk}
                                            account={currentAccount.get("name")}
                                            fee={buyFee}
                                            hasFeeBalance={
                                                this.state.feeStatus?.[
                                                    buyFee?.asset_id
                                                ]?.hasBalance
                                            }
                                            feeAssets={buyFeeAssets}
                                            feeAsset={buyFeeAsset}
                                            onChangeFeeAsset={value =>
                                                this.onChangeFeeAsset(
                                                    "buy",
                                                    value
                                                )
                                            }
                                            isPredictionMarket={base.getIn([
                                                "bitasset",
                                                "is_prediction_market"
                                            ])}
                                            onFlip={
                                                this.state._flipBuySell
                                                    ? null
                                                    : this._flipBuySell.bind(
                                                          this
                                                      )
                                            }
                                            onTogglePosition={
                                                !this.state
                                                    ._toggleBuySellPosition
                                                    ? this._toggleBuySellPosition.bind(
                                                          this
                                                      )
                                                    : null
                                            }
                                            halperBuy={"halperBuy"}
                                        />
                                    </div>
                                    <div className="my_orders">
                                        <MyOpenOrders
                                            smallScreen={this.props.smallScreen}
                                            className={cnames("open-orders")}
                                            key="open_orders"
                                            orders={marketLimitOrders}
                                            settleOrders={marketSettleOrders}
                                            currentAccount={currentAccount}
                                            base={base}
                                            quote={quote}
                                            settings={this.props.settings}
                                            baseSymbol={baseSymbol}
                                            quoteSymbol={quoteSymbol}
                                            activeTab={this.props.viewSettings.get(
                                                "ordersTab"
                                            )}
                                            onCancel={this._cancelLimitOrder.bind(this)}
                                            flipMyOrders={this.props.viewSettings.get(
                                                "flipMyOrders"
                                            )}
                                            feedPrice={this.props.feedPrice}
                                        />
                                </div>    
                            </div>
                            {/* {console.log("marketLimitOrders", marketLimitOrders)} */}
                            {marketLimitOrders.size > 0 && base && quote ? 
                            <div className="order-book order-book-nft">
                                <OrderBook
                                    latest={latest && latest.getPrice()}
                                    changeClass={changeClass}
                                    orders={marketLimitOrders}
                                    calls={marketCallOrders}
                                    invertedCalls={invertedCalls}
                                    combinedBids={combinedBids}
                                    combinedAsks={combinedAsks}
                                    highestBid={highestBid}
                                    lowestAsk={lowestAsk}
                                    totalBids={totals.bid}
                                    totalAsks={totals.ask}
                                    totalCall={totals.call}
                                    base={base}
                                    quote={quote}
                                    currentOrder={{bid, ask}}
                                    baseSymbol={baseSymbol}
                                    quoteSymbol={quoteSymbol}
                                    onClick={event =>
                                        this._orderbookClick(event)
                                    }
                                    horizontal={!leftOrderBook}
                                    moveOrderBook={() =>
                                        this._moveOrderBook(this)
                                    }
                                    flipOrderBook={this.props.viewSettings.get(
                                        "flipOrderBook"
                                    )}
                                    marketReady={marketReady}
                                    wrapperClass={`order-${
                                        buySellTop ? 3 : 1
                                    } xlarge-order-${buySellTop ? 4 : 1}`}
                                    currentAccount={this.props.currentAccount.get(
                                        "id"
                                    )}
                                    halperBuy={"halperBuy"}
                                />
                            </div>: null}

                        </div>
                    : <Translate className="not_sell" content="markets.not_for_sell" component="div" />}
                    </div>
                </div>

                <AccountNotifications />

                <ConfirmOrderModal
                    type="buy"
                    ref={ref => (this.refBuy = ref)}
                    onForce={() =>
                        this._forceBuy(
                            "buy",
                            buyFeeAsset,
                            baseBalance,
                            coreBalance
                        )
                    }
                    diff={buyDiff}
                    hasOrders={combinedAsks.length > 0}
                />

                <ConfirmOrderModal
                    type="sell"
                    ref={ref => (this.refSell = ref)}
                    onForce={() =>
                        this._forceSell(
                            "sell",
                            sellFeeAsset,
                            quoteBalance,
                            coreBalance
                        )
                    }
                    diff={sellDiff}
                    hasOrders={combinedBids.length > 0}
                />
            </div>
        );
    }
}

const FrozenInfo = props => {
    return (
        <Translate content="exchange.market_frozen" component="p" {...props} />
    );
};

export default Buy;

import React from "react";
import AssetName from "Components/Utility/AssetName";
import {Menu, Dropdown} from "antd";
import cnames from "classnames";
import counterpart from "counterpart";

export default class MyMarketsForm extends React.PureComponent {
    constructor(props) {
        super();
        this.state = {
            myMarketFilter: "",
            visibleDropDown: false,
            activeMarketTab: props.viewSettings.get("activeMarketTab", 0)
        };
    }

    handleSearchUpdate = e => {
        if (e.target) {
            let _value = (e.target.value && e.target.value.toUpperCase()) || "";
            this.setState({
                myMarketFilter: _value
            });
            console.log("_value >>> ", _value);
            this.props.searchUpdate(_value);
        }
    };

    toggleActiveMarketTab = index => {
        this.setState({
            activeMarketTab: index
        });
        this.props.activeMarketTab(index);
    };

    clearInput = () => {
        this.setState({
            myMarketFilter: ""
        });
        this.props.searchUpdate("");
    };

    render() {
        const {preferredBases, preferredTopMarkets} = this.props;
        const {activeMarketTab, myMarketFilter} = this.state;

        let selectedAsset = preferredTopMarkets.map((base, index) => {
            if (activeMarketTab === index) {
                return <AssetName key={"active-market-tab" + base} name={base} dataPlace="left" />;
            }
        });

        const menu = (
            <Menu >
                {preferredTopMarkets.map((base, index) => {
                    if (!base) return null;
                    return (
                        <Menu.Item
                            key={"dropdown-option-" + base + index}
                            onClick={()=>this.toggleActiveMarketTab(index)}
                            className={cnames("market-search-dropdown-option", {
                                active: activeMarketTab === index
                            })} >
                            <AssetName
                                name={base}
                                dataPlace="left"
                            />
                        </Menu.Item>
                    );
                })}
            </Menu>
        );



        return (
            <div className={cnames("market-search-tabs", {
                "hide": this.props.hideMarketSearch
            })}>

                <div className="market-search-dropdown" id={"exchange-dropdown"}>
                    <Dropdown
                        getPopupContainer={() => document.getElementById("exchange-dropdown")}
                        prefixCls={"exchange-dropdown"} overlay={menu} trigger={["click"]} placement="bottomRight">
                        {selectedAsset ?  <span>{selectedAsset}</span> : null}
                    </Dropdown>
                </div>
                <div className="market-search-form">
                    <span className="market-search-input-icon" />
                    <input
                        autoComplete="off"
                        type="text"
                        className="market-search-input"
                        placeholder={counterpart.translate(
                            "exchange.search"
                        )}
                        maxLength="16"
                        name="focus"
                        required="required"
                        value={myMarketFilter}
                        onChange={this.handleSearchUpdate}
                    />
                    {myMarketFilter ? <button
                        className="market-search-reset"
                        type="button"
                        onClick={this.clearInput}
                    />: null}
                </div>
            </div>
        );
    }
}
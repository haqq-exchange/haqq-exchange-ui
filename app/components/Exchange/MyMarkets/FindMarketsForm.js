import React from "react";
import {ChainValidation} from "deexjs";
//import {ChainValidation} from "bitsharesjs";
import {Menu, Dropdown} from "antd";
import cnames from "classnames";
import AssetName from "Components/Utility/AssetName";
import {getAlias} from "config/alias";
import counterpart from "counterpart";

export default class FindMarketsForm extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            activeFindBase: __SCROOGE_CHAIN__ ? "SCROOGE" : "DEEX",
            inputValue: "",
            assetNameError: false
        };
        this._onFoundBaseAsset = this._onFoundBaseAsset.bind(this);
        this._onInputName = this._onInputName.bind(this);
    }

    componentDidMount(){
        const _this = this;
        setTimeout(()=>{
            _this.props.lookupAssets(_this.state.activeFindBase, true);
        }, 250);
    }

    _onFoundBaseAsset = value => {
        console.log("_onFoundBaseAsset value", value );
        if (value) {
            this.setState({
                activeFindBase: value
            }, () => this._onInputName(this.refFindInput));
            this.props.onFoundBaseAsset(value);
        }
    };
    _onInputName = target => {
        let inputValue = target.value.trim().toUpperCase();
        let isValidName = !ChainValidation.is_valid_symbol_error(
            inputValue,
            true
        );

        this.setState({
            inputValue
        });

        /* Don't lookup invalid asset names */
        if (inputValue && isValidName) {
            this.props.lookupAssets(inputValue);
        }
    };

    render() {
        const {defaultBases} = this.props;
        const {inputValue, activeFindBase} = this.state;

        // console.log("defaultBases", defaultBases);
        // console.log("activeFindBase", activeFindBase);
        const menu = (
            <Menu >
                {defaultBases.map((base, index) => {
                    if (!base) return null;
                    // if (base == "undefined") return base = "SCROOGE";
                    return (
                        <Menu.Item
                            key={"dropdown-option-" + base + index}
                            onClick={()=>this._onFoundBaseAsset(base)}
                            className={cnames("market-search-dropdown-option", {
                                active: activeFindBase && activeFindBase === base
                            })} >
                            {getAlias(base)}
                        </Menu.Item>
                    );
                })}
            </Menu>
        );

        return (
            <div className={"market-search-tabs"}>
                <div className="market-search-dropdown" id={"exchange-dropdown-find"}>
                    <Dropdown
                        getPopupContainer={() => document.getElementById("exchange-dropdown-find")}
                        prefixCls={"exchange-dropdown"} overlay={menu} trigger={["click"]} placement="bottomRight">
                        {defaultBases.includes(activeFindBase) ? <span>{getAlias(activeFindBase)}</span> : <span /> }
                    </Dropdown>
                </div>
                <div className="market-search-form">
                    <span className="market-search-input-icon" />
                    <input
                        ref={ref=>this.refFindInput=ref}
                        autoComplete="off"
                        type="text"
                        className="market-search-input"
                        placeholder={counterpart.translate(
                            "exchange.search"
                        )}
                        maxLength="16"
                        name="focus"
                        value={inputValue}
                        onChange={event => this._onInputName(event.target)}
                    />
                </div>


            </div>
        );
    }
}
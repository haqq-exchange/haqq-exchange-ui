import React from "react";
import PropTypes from "prop-types";
import Immutable from "immutable";
import utils from "common/utils";
import Translate from "react-translate-component";
import {connect} from "alt-react";
import SettingsStore from "stores/SettingsStore";
import MarketsStore from "stores/MarketsStore";
import AssetStore from "stores/AssetStore";
// import ChainTypes from "../../Utility/ChainTypes";
import BindToChainState from "../../Utility/BindToChainState";

import SettingsActions from "actions/SettingsActions";
import AssetActions from "actions/AssetActions";
import cnames from "classnames";
import {debounce} from "lodash";
import LoadingIndicator from "../../LoadingIndicator";
import FindMarketsForm from "./FindMarketsForm";
import MyMarketsForm from "./MyMarketsForm";
import MarketGroup from "./MarketGroup";

import ResponseOption from "config/response";
import MediaQuery from "react-responsive";
const translator = require("counterpart");
let lastLookup = new Date();


class MyMarkets extends React.Component {
    static propTypes = {
        //core: ChainTypes.ChainAsset.isRequired
    };

    static defaultProps = {
        activeTab1: "my-market",
        core: "1.3.2230",
        setMinWidth: false,
        tabsMarket: [
            {
                ref: "myMarkets",
                tab: "my-market",
                intro: "walkthrough.my_markets_tab",
                translate: "exchange.market_name"
            },
            {
                ref: "findMarkets",
                tab: "find-market",
                intro: "walkthrough.find_markets_tab",
                translate: "exchange.more"
            }
        ]
    };

    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);

        let activeTab = props.activeTab || props.viewSettings.get("favMarketTab", "my-market");

        this.state = {
            inverseSort: props.viewSettings.get("myMarketsInvert", true),
            sortBy: props.viewSettings.get("myMarketsSort", "volume"),
            activeTab: activeTab,
            activeMarketTab: props.viewSettings.get("activeMarketTab", 0),
            lookupQuote: null,
            lookupBase: null,
            inputValue: "",
            minWidth: "100%",
            findBaseInput: "USD",
            activeFindBase: __SCROOGE_CHAIN__ ? "SCROOGE" : "DEEX"
        };
        this._setMinWidth = this._setMinWidth.bind(this);
        this.toggleActiveMarketTab = this.toggleActiveMarketTab.bind(this);
        this._onInputBaseAsset = this._onInputBaseAsset.bind(this);
        this._onFoundBaseAsset = this._onFoundBaseAsset.bind(this);
        this.getAssetList = debounce(AssetActions.getAssetList.defer, 150);
    }

    shouldComponentUpdate(nextProps, nextState) {
        /* Trigger a lookup when switching tabs to find-market */
        if (
            this.state.activeTab !== "find-market" &&
            nextState.activeTab === "find-market" &&
            !nextProps.searchAssets.size
        ) {
            this._lookupAssets(__SCROOGE_CHAIN__ ? "SCROOGE" : "DEEX", true);
        }

        return (
            !Immutable.is(nextProps.searchAssets, this.props.searchAssets) ||
            !Immutable.is(nextProps.markets, this.props.markets) ||
            !Immutable.is(
                nextProps.starredMarkets,
                this.props.starredMarkets
            ) ||
            !Immutable.is(
                nextProps.defaultMarkets,
                this.props.defaultMarkets
            ) ||
            !Immutable.is(nextProps.marketStats, this.props.marketStats) ||
            !utils.are_equal_shallow(nextState, this.state) ||
            nextProps.current !== this.props.current ||
            nextProps.minWidth !== this.props.minWidth ||
            nextProps.listHeight !== this.props.listHeight ||
            nextProps.preferredBases !== this.props.preferredBases ||
            nextProps.onlyStars !== this.props.onlyStars ||
            nextProps.assetsLoading !== this.props.assetsLoading ||
            nextProps.hideMarketSearch !== this.props.hideMarketSearch ||
            nextProps.userMarkets !== this.props.userMarkets
        );
    }

    UNSAFE_componentWillMount() {
        if (this.props.setMinWidth) {
            window.addEventListener("resize", this._setMinWidth, {
                capture: false,
                passive: true
            });
        }

        if (this.props.currrent) {
            const currentBase = this.props.current.split("_")[1];
            const currentIndex = this.props.preferredBases.findIndex(
                a => a === currentBase
            );
            if (
                currentIndex !== -1 &&
                currentIndex !== this.state.activeMarketTab
            ) {
                this.setState({activeMarketTab: currentIndex});
            }
        }
    }

    componentDidMount() {
        //Ps.initialize(this.refFavorite);

        this._setMinWidth();

        // console.log("this.state.activeTab", this.state.activeTab);

        if (this.state.activeTab === "find-market") {
            this._lookupAssets(__SCROOGE_CHAIN__ ? "SCROOGE" : "DEEX", true);
        }
    }

    componentWillUnmount() {
        if (this.props.setMinWidth) {
            window.removeEventListener("resize", this._setMinWidth);
        }
    }

    _setMinWidth() {
        if (
            this.props.setMinWidth &&
            this.refFavorite &&
            this.props.activeTab === "my-market"
        ) {
            if (this.state.minWidth !== this.refFavorite.offsetWidth) {
                this.setState({
                    minWidth: this.refFavorite.offsetWidth
                });
            }
        }
    }

    componentDidUpdate() {
        if (this.refFavorite) {
            //Ps.update(this.refFavorite);
        }
    }

    _inverseSort() {
        SettingsActions.changeViewSetting({
            myMarketsInvert: !this.state.myMarketsInvert
        });
        this.setState({
            inverseSort: !this.state.inverseSort
        });
    }

    _changeSort(type) {
        if (type !== this.state.sortBy) {
            SettingsActions.changeViewSetting({
                myMarketsSort: type
            });
            this.setState({
                sortBy: type
            });
        } else {
            this._inverseSort();
        }
    }

    _changeTab(tab) {

        SettingsActions.changeViewSetting({
            favMarketTab: tab
        });

        this.setState({
            activeTab: tab
        });

        this._setMinWidth();
    }

    _lookupAssets(value, force = false) {
        console.log("__lookupAssets", value, force);
        if (!value && value !== "") {
            return;
        }
        let now = new Date();
        // debugger;
        let symbols = value.toUpperCase().split(":");
        let quote = symbols[0];
        let base = symbols.length === 2 ? symbols[1] : null;

        this.setState({
            lookupQuote: quote,
            lookupBase: base
        });

        SettingsActions.changeViewSetting.defer({
            marketLookupInput: value.toUpperCase()
        });
        if (this.state.lookupQuote !== quote || force) {
            if (quote.length < 1 || now - lastLookup <= 250) {
                return false;
            }
            // console.log("__lookupAssets getAssetList quote", quote);
            this.getAssetList(quote, 50 , true);
        } else {
            if (base && this.state.lookupBase !== base) {
                if (base.length < 1 || now - lastLookup <= 250) {
                    return false;
                }
                // console.log("__lookupAssets getAssetList base", base);
                this.getAssetList(base, 50, true);
            }
        }
    }

    toggleActiveMarketTab(index) {
        SettingsActions.changeViewSetting({
            activeMarketTab: index
        });

        this.setState({
            activeMarketTab: index
        });
    }

    _onInputBaseAsset(asset) {
        this.setState({
            findBaseInput: asset.toUpperCase(),
            error: null
        });
    }

    _onFoundBaseAsset(activeFindBase) {
        if (activeFindBase) {
            this.setState({activeFindBase});
        }
    }

    handleSearchUpdate = value => {
        this.setState({
            myMarketFilter: value
        });
    };

    render() {
        let {
            tabsMarket,
            starredMarkets,
            defaultMarkets,
            marketStats,
            columns,
            searchAssets,
            assetsLoading,
            preferredBases,
            preferredTopMarkets,
            current,
            viewSettings,
            listHeight,
            onlyStars,
            userMarkets
        } = this.props;

        let {
            activeFindBase,
            activeMarketTab,
            activeTab,
            lookupQuote,
            lookupBase,
            myMarketFilter
        } = this.state;


        let otherMarkets = <div />;
        const myMarketTab = activeTab === "my-market";


        let defaultBases = preferredTopMarkets.map(a => a);

        // console.log("defaultBases", defaultBases.toJS());


        if (!myMarketTab) {
            preferredBases = preferredBases.clear();
            preferredBases = preferredBases.push(this.state.activeFindBase);
        }

        // Add some default base options
        // let preferredBases = [coreSymbol, "BTC", "USD", "CNY"];
        let baseGroups = {};

        let bases = [];
        let searchBase = [];
        /* By default, show the DEEX assets */
        if (!lookupQuote) lookupQuote = __SCROOGE_CHAIN__ ? "SCROOGE" : "DEEX";

        /* In the find-market tab, only use market tab 0 */
        if (!myMarketTab) activeMarketTab = 0;
        // console.clear()
        // console.log("searchAssets", searchAssets.toJS());
        // console.log("searchAssets lookupQuote", lookupQuote );
        // console.log("searchAssets lookupBase", lookupBase );
        // console.log("searchAssets activeFindBase", activeFindBase );
        searchAssets
            .filter(a => {
                if (lookupBase && lookupBase.length) {
                    return a.symbol.indexOf(lookupBase) === 0;
                }
                return a.symbol.indexOf(lookupQuote) !== -1;
            })
            .forEach(asset => {

                const [quoteItem, baseItem] =  asset.symbol.split(".");
                searchBase.push(baseItem || quoteItem);
                // console.log("searchAssets asset", baseItem, asset);

                if (lookupBase && lookupBase.length) {
                    if (asset.symbol.indexOf(lookupBase) === 0) {
                        bases.push(asset.symbol);
                    }
                } else if (preferredTopMarkets.includes(asset.symbol)) {
                    if (
                        asset.symbol.length >= lookupQuote.length &&
                        asset.symbol.length < lookupQuote.length + 3
                    ) {
                        bases.push(asset.symbol);
                    }
                }
            });

        bases = bases.concat(
            preferredTopMarkets
                .filter(a => {
                    if (!lookupBase || !lookupBase.length) {
                        return true;
                    }
                    return a.indexOf(lookupBase) === 0;
                })
                .toArray()
        );

        bases = bases.filter(base => {
            if (lookupBase && lookupBase.length > 1) {
                return base.indexOf(lookupBase) === 0;
            } else {
                return true;
            }
        });

        let allMarkets = [];

        if (searchAssets.size) {
            searchAssets
                .filter(a => {
                    try {
                        if (a.options.description) {
                            let description = JSON.parse(a.options.description);
                            if ("visible" in description) {
                                if (!description.visible) return false;
                            }
                        }
                    } catch (e) {}

                    return (
                        a.symbol.indexOf(lookupQuote) !== -1 &&
                        a.symbol.length >= lookupQuote.length
                    );
                })
                .forEach(asset => {
                    bases.forEach(base => {
                        let marketID = asset.symbol + "_" + base;

                        allMarkets.push([
                            marketID,
                            {quote: asset.symbol, base: base}
                        ]);
                        if (base !== asset.symbol) {
                        }
                    });
                });
        }
        // console.log("searchAssets >> ", searchAssets.toJS(), bases);
        allMarkets = allMarkets.filter(a => {
            // If a base asset is specified, limit the quote asset to the exact search term
            if (lookupBase) {
                return a[1].quote === lookupQuote;
            }
            return true;
        });

        allMarkets = Immutable.Map(allMarkets);


        let activeMarkets = myMarketTab ? defaultMarkets : allMarkets;
        if (myMarketTab && userMarkets.size) {
            userMarkets.forEach((market, key) => {
                activeMarkets = activeMarkets.set(key, market);
            });
        }

        if (activeMarkets.size > 0) {
            otherMarkets = activeMarkets
                .filter(a => {
                    if (!myMarketTab) {
                        if (lookupQuote.length < 1) {
                            return false;
                        }

                        return a.quote.indexOf(lookupQuote) !== -1;
                    } else {
                        const ID = a.quote + "_" + a.base;
                        if (!!myMarketFilter) {
                            return a.quote.indexOf(myMarketFilter) !== -1;
                        } else if (onlyStars && !starredMarkets.has(ID)) {
                            return false;
                        }
                        return true;
                    }
                })
                .map(market => {
                    let marketID = market.quote + "_" + market.base;
                    if (preferredTopMarkets.includes(market.base)) {
                        if (!baseGroups[market.base]) baseGroups[market.base] = [];

                        baseGroups[market.base].push({
                            id: marketID,
                            quote: market.quote,
                            base: market.base
                        });
                        return null;
                    } else {
                        return {
                            id: marketID,
                            quote: market.quote,
                            base: market.base
                        };
                    }
                })
                .filter(a => {
                    return a !== null;
                })
                .take(myMarketTab ? 100 : 20)
                .toArray();
        }


        const hasOthers = otherMarkets && otherMarkets.length;

        let listStyle = {
            minWidth: this.state.minWidth,
            minHeight: "6rem"
        };
        if (listHeight) {
            listStyle.height = listHeight;
        }


        // console.log("views userMarkets", userMarkets.size, userMarkets.toJS());
        // console.log("views defaultMarkets", defaultMarkets.size, defaultMarkets.toJS());
        // console.log("views allMarkets", allMarkets.size, allMarkets.toJS());
        // console.log("views otherMarkets", otherMarkets );
        // console.log("views activeMarkets", activeMarkets.size, activeMarkets.toJS());
        // console.log("views searchAssets", searchAssets.size, searchAssets.toJS());
        // console.log("views baseGroups[base]", baseGroups);
        // console.log("views baseGroups[base]", activeMarketTab);
        // console.log("views baseGroups[base]", preferredTopMarkets.size, preferredTopMarkets.toJS());

        return (
            <div className={this.props.className}>

                <div className={this.props.className + "-wrap"}>
                    <MediaQuery {...ResponseOption.mobile}>
                        {(matches) => {
                            if (matches) {
                                /* mobile */
                                return  null;
                            } else {
                                return  <div className={this.props.className + "-tabs-markets"}>
                                    {tabsMarket.map(data => {
                                        return (
                                            <div
                                                ref={data.ref}
                                                key={"tab" + data.tab}
                                                className={cnames(this.props.className + "-item-markets", {
                                                    active: data.tab === activeTab
                                                })}
                                                onClick={()=>this._changeTab(data.tab)}
                                                data-intro={translator.translate(
                                                    data.intro
                                                )}
                                            >
                                                <Translate content={data.translate} />
                                            </div>
                                        );
                                    })}
                                </div>;
                            }
                        }}
                    </MediaQuery>

                    {/*{console.log("activeMarkets defaultBases", defaultBases.toJS())}*/}
                    {/*{console.log("activeMarkets preferredBases", preferredBases.toJS())}*/}
                    {/*{console.log("activeMarkets preferredTopMarkets", preferredTopMarkets.toJS())}*/}

                    {myMarketTab ? (
                        <MyMarketsForm
                            activeMarketTab={this.toggleActiveMarketTab}
                            searchUpdate={this.handleSearchUpdate}
                            preferredBases={preferredBases}
                            preferredTopMarkets={preferredTopMarkets}
                            {...this.props}
                        />
                    ) : (
                        <FindMarketsForm
                            onFoundBaseAsset={this._onFoundBaseAsset}
                            onInputBaseAsset={this._onInputBaseAsset}
                            lookupAssets={this._lookupAssets.bind(this)}
                            defaultBases={Object.keys(baseGroups) || defaultBases || []}
                            {...this.props}
                        />
                    )}
                </div>
                <div className={this.props.className + "-list"} ref={(ref)=>this.refFavorite=ref}>
                    {assetsLoading && <LoadingIndicator type="three-bounce" />}
                    {preferredTopMarkets
                        .filter(a => {
                            return a === preferredTopMarkets.get(activeMarketTab);
                        })
                        .map((base, index) => {
                            return (
                                <MarketGroup
                                    userMarkets={userMarkets}
                                    defaultMarkets={defaultMarkets}
                                    index={index}
                                    allowChange={false}
                                    key={base}
                                    current={current}
                                    starredMarkets={starredMarkets}
                                    marketStats={marketStats}
                                    viewSettings={viewSettings}
                                    columns={
                                        myMarketTab
                                            ? columns
                                            : this.props.findColumns || columns
                                    }
                                    markets={baseGroups?.[activeFindBase] || baseGroups[base]}
                                    base={base}
                                    maxRows={myMarketTab ? 20 : 10}
                                    findMarketTab={!myMarketTab}
                                    className={this.props.className}
                                />
                            );
                        })}
                    {activeMarketTab === preferredTopMarkets.size + 1 &&
                    myMarketTab && hasOthers && (
                        <MarketGroup
                            userMarkets={userMarkets}
                            index={preferredTopMarkets.size}
                            current={current}
                            starredMarkets={starredMarkets}
                            marketStats={marketStats}
                            viewSettings={viewSettings}
                            columns={columns}
                            markets={otherMarkets}
                            base="others"
                            maxRows={myMarketTab ? 20 : 10}
                            findMarketTab={!myMarketTab}
                        />
                    )}
                </div>
            </div>

        );
    }
}

MyMarkets = BindToChainState(MyMarkets);

class MyMarketsWrapper extends React.Component {
    render() {
        return <MyMarkets {...this.props} />;
    }
}

export default connect(
    MyMarketsWrapper,
    {
        listenTo() {
            return [SettingsStore, MarketsStore, AssetStore];
        },
        getProps() {
            //console.log("SettingsStore.getState().userMarkets", SettingsStore.getState().userMarkets);
            return {
                starredMarkets: SettingsStore.getState().starredMarkets,
                defaultMarkets: SettingsStore.getState().defaultMarkets,
                viewSettings: SettingsStore.getState().viewSettings,
                preferredBases: SettingsStore.getState().preferredBases,
                preferredTopMarkets: SettingsStore.getState().preferredTopMarkets,
                marketStats: MarketsStore.getState().allMarketStats,
                userMarkets: SettingsStore.getState().userMarkets,
                searchAssets: AssetStore.getState().assets,
                onlyStars: MarketsStore.getState().onlyStars,
                assetsLoading: AssetStore.getState().assetsLoading
            };
        }
    }
);

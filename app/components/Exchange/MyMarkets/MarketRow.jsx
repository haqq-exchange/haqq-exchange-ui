import React from "react";
import PropTypes from "prop-types";
import FormattedAsset from "Utility/FormattedAsset";
import AssetWrapper from "Utility/AssetWrapper";
import AccountName from "Utility/AccountName";
import utils from "common/utils";
import Icon from "../../Icon/Icon";
import MarketsActions from "actions/MarketsActions";
import SettingsActions from "actions/SettingsActions";
import classnames from "classnames";
import {getAlias} from "config/alias";

class MarketRow extends React.Component {
    static defaultProps = {
        noSymbols: false
    };

    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    constructor() {
        super();

        this.statsInterval = null;
    }

    _onClick(marketID) {
        const newPath = `/market/${marketID}`;
        if (newPath !== this.context.router.location.pathname) {
            MarketsActions.switchMarket();
            this.context.router.push(`/market/${marketID}`);
        }
    }

    componentDidMount() {
        const {base, quote} = this.props;
        if(!base || !quote) return null;
        // MarketsActions.getMarketStats(this.props.base, this.props.quote);
        this.statsChecked = new Date();
        this.statsInterval = MarketsActions.getMarketStatsInterval(
            35 * 1000,
            base, quote
        );
    }

    componentWillUnmount() {
        if (this.statsInterval) this.statsInterval();
    }

    shouldComponentUpdate(nextProps) {
        return !utils.are_equal_shallow(nextProps, this.props);
    }

    _onStar(quote, base) {
        //e.preventDefault();
        if (!this.props.starred) {
            SettingsActions.addStarMarket(quote, base);
        } else {
            SettingsActions.removeStarMarket(quote, base);
        }
    }

    render() {
        let {quote, base, stats, starred} = this.props;

        if (!quote || !base) {
            return null;
        }

        let marketID = quote.get("symbol") + "_" + base.get("symbol");
        let marketName = quote.get("symbol") + ":" + base.get("symbol");
        let dynamic_data = this.props.getDynamicObject(
            quote.get("dynamic_asset_data_id")
        );
        let base_dynamic_data = this.props.getDynamicObject(
            base.get("dynamic_asset_data_id")
        );


        let price = utils.convertPrice(quote, base);

        //console.log("quote, base", quote.toJS(), base.toJS(), price);
        let rowStyles = {};
        if (this.props.leftAlign) {
            rowStyles.textAlign = "left";
        }

        let buttonClass = "button outline";
        let buttonStyle = null;
        if (this.props.compact) {
            buttonClass += " no-margin";
            buttonStyle = {
                marginBottom: 0,
                fontSize: "0.75rem",
                padding: "4px 10px",
                borderRadius: "0px",
                letterSpacing: "0.05rem"
            };
        }

        let columns = this.props.columns
            .map(column => {
                switch (column.name) {
                    case "star":
                        let starClass = classnames({
                            "gold-star": starred,
                            "grey-star": !starred
                        });
                        return (
                            <div
                                className={"table-body-star"}
                                onClick={()=>this._onStar(
                                    quote.get("symbol"),
                                    base.get("symbol")
                                )}
                                key={column.index} >
                                <Icon
                                    className={starClass}
                                    name="sinactive"
                                />
                            </div>
                        );

                    case "vol":
                        let amount = stats ? stats.volumeBase : 0;
                        return (
                            <MarketTd
                                key={column.index}
                                marketID={marketID}
                                {...this.context}
                                className={"table-body-vol"}
                            >
                                {utils.format_volume(amount)}
                            </MarketTd>
                        );

                    case "change":
                        let change = utils.format_number(
                            stats && stats.change ? stats.change : 0,
                            2
                        );

                        let changeClass = classnames("table-body-change", {
                            "change-up": change > 0,
                            "change-down": change < 0
                        });

                        return (
                            <MarketTd
                                key={column.index}
                                marketID={marketID}
                                {...this.context}
                                className={changeClass}
                            >
                                {change + "%"}
                            </MarketTd>
                        );

                    case "marketName":
                        return (
                            <MarketTd
                                key={column.index}
                                marketID={marketID}
                                className={"table-body-marketName"}
                                {...this.context}
                            >
                                <div
                                    className={buttonClass}
                                    style={buttonStyle}
                                >
                                    {marketName}
                                </div>
                            </MarketTd>
                        );

                    case "market":
                        return (
                            <MarketTd
                                key={column.index}
                                className={"table-body-market"}
                                marketID={marketID}
                                {...this.context}
                            >
                                {getAlias(this.props.name)}
                            </MarketTd>
                        );

                    case "price":
                        let finalPrice =
                            stats && stats.price
                                ? stats.price.toReal()
                                : stats &&
                                  stats.close &&
                                  (stats.close.quote.amount &&
                                      stats.close.base.amount)
                                    ? utils.get_asset_price(
                                          stats.close.quote.amount,
                                          quote,
                                          stats.close.base.amount,
                                          base,
                                          true
                                      )
                                    : utils.get_asset_price(
                                          price.quote.amount,
                                          quote,
                                          price.base.amount,
                                          base,
                                          true
                                      );

                        let highPrecisionAssets = [
                            "BTC",
                            "OPEN.BTC",
                            "TRADE.BTC",
                            "GOLD",
                            "SILVER"
                        ];
                        let precision = 6;
                        if (
                            highPrecisionAssets.indexOf(base.get("symbol")) !==
                            -1
                        ) {
                            precision = 8;
                        }

                        return (
                            <MarketTd
                                className={"table-body-price"}
                                key={column.index}
                                marketID={marketID}
                                {...this.context}
                            >
                                {utils.format_number(
                                    finalPrice,
                                    finalPrice > 1000
                                        ? 0
                                        : finalPrice > 10
                                            ? 2
                                            : precision
                                )}
                            </MarketTd>
                        );

                    case "quoteSupply":
                        return (
                            <MarketTd
                                key={column.index}
                                marketID={marketID}
                                className={"table-body-quote"}
                                {...this.context}
                            >
                                {dynamic_data ? (
                                    <FormattedAsset
                                        style={{fontWeight: "bold"}}
                                        amount={parseInt(
                                            dynamic_data.get("current_supply"),
                                            10
                                        )}
                                        asset={quote.get("id")}
                                    />
                                ) : null}
                            </MarketTd>
                        );

                    case "baseSupply":
                        return (
                            <MarketTd
                                key={column.index}
                                marketID={marketID}
                                className={"table-body-base"}
                                {...this.context}
                            >
                                {base_dynamic_data ? (
                                    <FormattedAsset
                                        style={{fontWeight: "bold"}}
                                        amount={parseInt(
                                            base_dynamic_data.get(
                                                "current_supply"
                                            ),
                                            10
                                        )}
                                        asset={base.get("id")}
                                    />
                                ) : null}
                            </MarketTd>
                        );

                    case "issuer":
                        return (
                            <MarketTd
                                key={column.index}
                                marketID={marketID}
                                className={"table-body-issuer"}
                                {...this.context}
                            >
                                <AccountName account={quote.get("issuer")} />
                            </MarketTd>
                        );

                    case "add":
                        return (
                            <div
                                style={{textAlign: "right"}}
                                key={column.index}
                                className={"table-body-add"}
                                onClick={()=>this.props.onCheckMarket(marketID)}
                            >
                                <input
                                    type="checkbox"
                                    defaultChecked={
                                        !!this.props.isChecked ||
                                        this.props.isDefault
                                    }
                                    disabled={this.props.isDefault}
                                    data-tip={
                                        this.props.isDefault
                                            ? "This market is a default market and cannot be removed"
                                            : null
                                    }
                                />
                            </div>
                        );

                    case "remove":
                        return (
                            <div
                                key={column.index}
                                className={"table-body-remove clickable"}
                                onClick={this.props.removeMarket}
                            >
                                <span
                                    style={{
                                        marginBottom: "6px",
                                        marginRight: "6px",
                                        zIndex: 999
                                    }}
                                    className="text float-right remove"
                                >
                                    –
                                </span>
                            </div>
                        );

                    default:
                        break;
                }
            })
            .sort((a, b) => {
                return a.key > b.key;
            });

        let classNameTr = classnames("clickable table-body-row", {
            activeMarket: this.props.current
        });

        return (
            <div className={classNameTr}>
                {columns}
            </div>
        );
    }
}

class MarketTd extends React.PureComponent {
    _onClick = () => {
        const {router, marketID} = this.props;
        const newPath = ["", "market", marketID].join("/");
        if (newPath !== router.route.match.url) {
            MarketsActions.switchMarket();
            router.history.push(newPath);
        }
    };

    render() {
        return (
            <div className={this.props.className} onClick={this._onClick}>
                {this.props.children}
            </div>
        );
    }
}

export default AssetWrapper(MarketRow, {
    propNames: ["quote", "base"],
    defaultProps: {
        tempComponent: "div"
    },
    withDynamic: true
});

import React from "react";
import Translate from "react-translate-component";
import utils from "../../../lib/common/utils";
import SettingsActions from "../../../actions/SettingsActions";
import AssetName from "Components/Utility/AssetName";
import cnames from "classnames";
import {getAlias} from "../../../config/alias";
import MarketRow from "./MarketRow";
import Icon from "Components/Icon/Icon";

export default class MarketGroup extends React.Component {
    static defaultProps = {
        maxRows: 20
    };

    constructor(props) {
        super(props);

        this.state = {
            showFavorite: false,
            showFilterDropdown: true,
            ... MarketGroup._getInitialState(props)
        };

    }

    static _getInitialState(props) {
        let open = props.findMarketTab
            ? true
            : props.viewSettings.get(`myMarketsBase_${props.index}`);
        return {
            open: open !== undefined ? open : true,
            inverseSort: props.viewSettings.get("myMarketsInvert", true),
            sortBy: props.viewSettings.get("myMarketsSort", "volume"),
            inputValue: ""
        };
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.findMarketTab !== this.props.findMarketTab) {
            this.setState(MarketGroup._getInitialState(nextProps));
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (!nextProps.markets || !this.props.markets) {
            return true;
        }
        return (
            !utils.are_equal_shallow(nextState, this.state) ||
            !utils.are_equal_shallow(nextProps.markets, this.props.markets) ||
            nextProps.starredMarkets !== this.props.starredMarkets ||
            nextProps.marketStats !== this.props.marketStats ||
            nextProps.userMarkets !== this.props.userMarkets
        );
    }

    _inverseSort() {
        SettingsActions.changeViewSetting({
            myMarketsInvert: !this.state.myMarketsInvert
        });
        this.setState({
            inverseSort: !this.state.inverseSort
        });
    }

    _getFavorite() {
        let {showFavorite} = this.state;
        this.setState({
            showFavorite: !showFavorite
        });
    }

    _changeSort(type) {
        if (type !== this.state.sortBy) {
            SettingsActions.changeViewSetting({
                myMarketsSort: type
            });
            this.setState({
                sortBy: type
            });
        } else {
            this._inverseSort();
        }
    }



    _onToggleUserMarket(market) {
        // console.log("_onToggleUserMarket market", market)
        if( market && market.indexOf("_")) {
            let [base, quote] = market.split("_");
            let newValue = !this.props.userMarkets.get(market);
            SettingsActions.setUserMarket(base, quote, newValue);
        }
    }

    render() {
        let {
            columns,
            markets,
            base,
            marketStats,
            starredMarkets,
            current,
            findMarketTab
        } = this.props;
        let {sortBy, inverseSort, open, showFavorite} = this.state;

        if (!markets || !markets.length) {
            return null;
        }

        let headers = columns.map(header => {
            switch (header.name) {
                case "market":
                    return (
                        <div
                            key={header.name}
                            className={"clickable table-header-"+header.name}
                            onClick={()=>this._changeSort("name")}
                        >
                            <Translate content="exchange.market" />
                        </div>
                    );

                case "vol":
                    return (
                        <div
                            key={header.name}
                            className={"clickable table-header-"+header.name}
                            onClick={()=>this._changeSort("volume")}>
                            <Translate content="exchange.vol_short" />
                        </div>
                    );

                case "price":
                    return (
                        <div key={header.name} className={"table-header-"+header.name}>
                            <Translate content="exchange.price" />
                        </div>
                    );

                case "quoteSupply":
                    return (
                        <div key={header.name} className={"table-header-"+header.name} >
                            <Translate content="exchange.quote_supply" />
                        </div>
                    );

                case "baseSupply":
                    return (
                        <div key={header.name} className={"table-header-"+header.name}>
                            <Translate content="exchange.base_supply" />
                        </div>
                    );

                case "change":
                    return (
                        <div
                            key={header.name}
                            className={"clickable table-header-"+header.name}
                            onClick={()=>this._changeSort("change")} >
                            <Translate content="exchange.change" />
                        </div>
                    );

                case "issuer":
                    return (
                        <div key={header.name}  className={"table-header-"+header.name} >
                            <Translate content="explorer.assets.issuer" />
                        </div>
                    );

                case "add":
                    return (
                        <div key={header.name} className={"table-header-"+header.name} >
                            {/*<Translate content="account.perm.confirm_add" />*/}
                        </div>
                    );

                case "star":
                    return <div
                        onClick={()=>this._getFavorite()}
                        className={"clickable table-header-"+header.name} key={header.name} >
                        <Icon
                            className={cnames({
                                "gold-star": showFavorite,
                                "grey-star": !showFavorite
                            })}
                            name="sinactive"
                        />
                    </div>;
            }
        });

        let marketRows = markets
            .map(market => {
                if (!market.quote || !market.base) {
                    return null;
                }
                if( !showFavorite || ( showFavorite && starredMarkets.has(market.id) )  ) {
                    return (
                        <MarketRow
                            key={market.id}
                            name={
                                base === "others" ? (
                                    <span>
                                        <AssetName name={market.quote} />:
                                        <AssetName name={market.base} />
                                    </span>
                                ) : (
                                    <AssetName
                                        dataPlace="left"
                                        name={market.quote}
                                    />
                                )
                            }
                            quote={market.quote}
                            base={market.base}
                            columns={columns}
                            leftAlign={true}
                            compact={true}
                            noSymbols={true}
                            stats={marketStats.get(market.id)}
                            starred={starredMarkets.has(market.id)}
                            current={current === market.id}
                            isChecked={this.props.userMarkets.has(market.id)}
                            isDefault={
                                this.props.defaultMarkets &&
                                this.props.defaultMarkets.has(market.id)
                            }
                            onCheckMarket={(market)=>this._onToggleUserMarket(market)}
                        />
                    );
                }
            })
            .filter(a => {
                return a !== null;
            })
            .sort((a, b) => {
                let a_symbols = a.key.split("_");
                let b_symbols = b.key.split("_");
                // console.log("a_symbols", a_symbols);
                // console.log("b_symbols", b_symbols);
                let aStats = marketStats.get(a_symbols[0] + "_" + a_symbols[1]);
                let bStats = marketStats.get(b_symbols[0] + "_" + b_symbols[1]);
                let aliasA0 = getAlias(a_symbols[0]);
                let aliasA1 = getAlias(a_symbols[1]);
                let aliasB0 = getAlias(b_symbols[0]);
                let aliasB1 = getAlias(b_symbols[1]);
                switch (sortBy) {
                    case "name":
                        if (aliasA0 > aliasB0) {
                            return inverseSort ? -1 : 1;
                        } else if (aliasA0 < aliasB0) {
                            return inverseSort ? 1 : -1;
                        } else {
                            if (aliasA1 > aliasB1) {
                                return inverseSort ? -1 : 1;
                            } else if (aliasA1 < aliasB1) {
                                return inverseSort ? 1 : -1;
                            } else {
                                return 0;
                            }
                        }

                    case "volume":
                        if (aStats && bStats) {
                            if (inverseSort) {
                                return bStats.volumeBase - aStats.volumeBase;
                            } else {
                                return aStats.volumeBase - bStats.volumeBase;
                            }
                        } else {
                            return 0;
                        }

                    case "change":
                        if (aStats && bStats) {
                            if (inverseSort) {
                                return bStats.change - aStats.change;
                            } else {
                                return aStats.change - bStats.change;
                            }
                        } else {
                            return 0;
                        }
                }
            });

        // let caret = open ? <span>&#9660;</span> : <span>&#9650;</span>;

        return (
            <div className={this.props.className + "-table"}>
                {open ? (
                    <div className={cnames("table", {
                        "find-market" : this.props.findMarketTab
                    })}>
                        <div className={"table-header"}>
                            <div className={"table-header-row"}>{headers}</div>
                        </div>
                        {marketRows && marketRows.length
                            ?  <div className={cnames("table-body") }>{marketRows}</div>
                            : null }
                    </div>
                ) : null}
            </div>
        );
    }
}
import React from "react";
import PropTypes from "prop-types";
import Ps from "perfect-scrollbar";
import utils from "common/utils";
import Translate from "react-translate-component";
import SettingsActions from "actions/SettingsActions";
import classnames from "classnames";
import PriceText from "../Utility/PriceText";
import TransitionWrapper from "../Utility/TransitionWrapper";
import AssetName from "../Utility/AssetName";
import FormattedValue from "../Utility/FormattedValue";
import { StickyTable } from "react-sticky-table";
import Icon from "../Icon/Icon";
import "react-sticky-table/dist/react-sticky-table.css";

class OrderBookRowVertical extends React.Component {
    shouldComponentUpdate(np) {
        if (np.order.market_base !== this.props.order.market_base) return false;
        return (
            np.order.ne(this.props.order) ||
            np.index !== this.props.index ||
            np.currentAccount !== this.props.currentAccount
        );
    }

    render() {
        let {order, quote, base, final} = this.props;
        const isBid = order.isBid();
        const isCall = order.isCall();
        let integerClass = isCall
            ? "orderHistoryCall"
            : isBid
                ? "orderHistoryBid text-right-imp"
                : "orderHistoryAsk text-left";

        let price = (
            <PriceText price={order.getPrice()} quote={quote} base={base} />
        );
        return (
            <div
                onClick={this.props.onClick}
                className={classnames(
                    "sticky-table-row order-row",
                    {"final-row": final},
                    {"my-order": order.isMine(this.props.currentAccount)}
                )}
            >
                <div className="cell left">
                    {utils.format_number(
                        order[
                            isBid ? "amountForSale" : "amountToReceive"
                        ]().getAmount({real: true}),
                        base.get("precision")
                    )}
                </div>
                <div className="cell">
                    {utils.format_number(
                        order[
                            isBid ? "amountToReceive" : "amountForSale"
                        ]().getAmount({real: true}),
                        quote.get("precision")
                    )}
                </div>
                <div className={`cell ${integerClass} right`}>{price}</div>
            </div>
        );
    }
}

class OrderBookRowHorizontal extends React.Component {
    constructor(props) {
        super();

        // this.state = {schowBtn: true}
    }
    /*shouldComponentUpdate(np) {
        return (
            np.order.ne(this.props.order) ||
            np.position !== this.props.position ||
            np.index !== this.props.index ||
            np.currentAccount !== this.props.currentAccount
        );
    }*/
    // onClick = () => {
    //     if(this.props.halper === "halper") {
    //         this.props.onClick();
    //         this.setState({schowBtn: false});
    //     } else {
    //         this.props.onClick();
    //     }
    // }

    render() {
        let {order, quote, base, position, totalValue} = this.props;
        const isBid = order.isBid();
        const isCall = order.isCall();

        let integerClass = isCall
            ? "orderHistoryCall"
            : isBid
                ? "~orderHistoryBid text-right-imp"
                : "~orderHistoryAsk text-left";

        let price = (
            <PriceText price={order.getPrice()} quote={quote} base={base} />
        );
        if(!isBid) {
            //console.log("totalForSale",isCall, isBid, order);
        }
        //console.log("totalToReceive", order.totalToReceive().getAmount({real: true}));
        let orderReal = isBid ? order.totalForSale().getAmount({real: true}) : order.totalToReceive().getAmount({real: true});
        let amount = isBid
            ? utils.format_number(
                order.amountToReceive().getAmount({real: true}),
                quote.get("precision")
            ) : utils.format_number(
                order.amountForSale().getAmount({real: true}),
                quote.get("precision")
            );
        let value = isBid
            ? utils.format_number(
                order.amountForSale().getAmount({real: true}),
                base.get("precision")
            )
            : utils.format_number(
                order.amountToReceive().getAmount({real: true}),
                base.get("precision")
            );
        let total = isBid ? utils.format_number(
            orderReal, base.get("precision")
        ) : utils.format_number(
            orderReal,
            base.get("precision")
        );
        let percentValue = orderReal * 100 / totalValue;
        //console.log("totalValue", orderReal, totalValue, percentValue);
        let opacityValue = Math.ceil(percentValue)/100;
        opacityValue = opacityValue > 0.6 ? 0.6 : opacityValue;


        let bgColor = isBid ? "rgba(99, 218, 50, 0.3": "rgba(255, 39, 65, 0.2)";

        let styleBg = {
            width: percentValue > 100 ? 100 + "%" : percentValue + "%",
            opacity: 1 - opacityValue ,
            position: "absolute",
            height: "100%",
            background: bgColor,
            top: 0
        };
        //console.log("styleBg", styleBg);
        styleBg[isBid ? "right": "left"] = 0;
        let propsRow = {
            onClick: this.props.onClick,
            className: classnames("order-book-body-row", {
                "my-order": order.isMine(this.props.currentAccount)
            })
        };

        if( position === "left" ) {
            return (
                <div {...propsRow}>
                    <div className={"order-book-body-row-wrap"}>
                        <div className={"order-book-body-item price"}>
                            {price}
                        </div>
                        <div className={"order-book-body-item asset-base"}>{value}</div>
                        <div className={"order-book-body-item asset-quote"}>{amount}</div>
                        <div className={"order-book-body-item total"}>{total}</div>
                        <div className={"order-book-body-item"} style={styleBg} />
                        {/* {(this.props.halper == "halper" && this.state.schowBtn) ? */}
                        {(this.props.halper == "halper") ?
                        // <div className="btn btn-green"><Translate content="exchange.sell"/></div> : null}
                        <div style={{fontSize: "16px", fontWeight: "bold"}}><Translate content="exchange.select"/></div>
                        : null}
                    </div>
                </div>
            );
        } else {
            return (
                <div {...propsRow}>
                    <div className={"order-book-body-row-wrap"}>
                        <div className={"order-book-body-item price"}>
                            {price}
                        </div>
                        <div className={"order-book-body-item asset-base"}>{value}</div>
                        <div className={"order-book-body-item asset-quote"}>{amount}</div>
                        <div className={"order-book-body-item total"}>{total}</div>
                        <div className={"order-book-body-item"} style={styleBg} />
                    </div>
                </div>
            );
        }
    }
}

class OrderBook extends React.Component {
    constructor(props) {
        super();
        this.state = {
            flip: props.flipOrderBook,
        };
    }

    render() {
        let {
            combinedBids,
            combinedAsks,
            quote,
            base,
            totalAsks,
            totalBids,
            quoteSymbol,
            baseSymbol,
            horizontal,
            currentOrder
        } = this.props;

        let {bid, ask} = currentOrder;

        let bidRows = null,
            askRows = null;
        let lastCombinedBids = combinedBids[combinedBids.length-1];
        let lastCombinedAsks = combinedAsks[combinedAsks.length-1];
        let firstCombinedBids = combinedBids[0];
        let firstCombinedAsks = combinedAsks[0];

        if (base && quote) {
            /*if( firstCombinedBids && !bid.idOrder && !ask.idOrder ) {
                this.props.onClick(firstCombinedBids);
            }
            if( firstCombinedAsks && !bid.idOrder && !ask.idOrder ) {
                setTimeout(()=>{
                    this.props.onClick(firstCombinedAsks);
                }, 10);
            }*/

            bidRows = combinedBids.map((order, index) => {
                return (
                    <OrderBookRowHorizontal
                        index={index}
                        key={order.getPrice() + (order.isCall() ? "_call" : "")}
                        order={order}
                        onClick={()=>this.props.onClick(order)}
                        base={base}
                        quote={quote}
                        totalValue={totalBids}
                        position={!this.state.flip ? "left" : "right"}
                        currentAccount={this.props.currentAccount}
                        halper = {this.props.halper}
                    />
                );
            });

            let tempAsks = combinedAsks;
            if (!horizontal) {
                tempAsks.sort((a, b) => {
                    return b.getPrice() - a.getPrice();
                });
            }

            //console.log("combinedAsks", combinedAsks);

            askRows = tempAsks.map((order, index) => {
                return (
                    <OrderBookRowHorizontal
                        index={index}
                        key={order.getPrice() + (order.isCall() ? "_call" : "")}
                        order={order}
                        onClick={this.props.onClick.bind(this, order)}
                        base={base}
                        quote={quote}
                        totalValue={totalAsks}
                        type={order.type}
                        position={!this.state.flip ? "right" : "left"}
                        currentAccount={this.props.currentAccount}
                        halper = {this.props.halper}
                    />
                );
            });
        }

        // console.log("totalBids", totalBids);
        // console.log("totalAsks", totalAsks);

        return (
            <div className={classnames("order-book-wrap-item")}>
                {this.props.halperBuy === "halperBuy" ? null :
                <OrderItem
                    key={"order-item-bids"}
                    type={"bids"}
                    headerTitle={(this.props.halper == "halper") ? "exchange.request_to_buy" : "exchange.bids"}
                    headerSubTitle={"exchange.total"}
                    totalValue={totalBids}
                    totalPrecision={base.get("precision")}
                    totalBids={totalBids}
                    totalAsks={totalAsks}
                    _totalValue={utils.format_number(
                        totalBids,
                        base.get("precision")
                    )}
                    base={base}
                    quote={quote}
                    flip={!this.state.flip}
                    baseSymbol={baseSymbol}
                    quoteSymbol={quoteSymbol}
                    bodyRows={bidRows}
                    onToggleShowAll={()=>this._onToggleShowAll("bids")}
                    halper = {this.props.halper}
                    halperBuy = {this.props.halperBuy}
                />}
                {this.props.halper == "halper" ? null :
                <OrderItem
                    key={"order-item-asks"}
                    type={"asks"}
                    headerTitle={(this.props.halperBuy === "halperBuy") ? "exchange.request_to_sell" : "exchange.asks"}
                    headerSubTitle={"exchange.total"}
                    totalValue={totalAsks}
                    totalBids={totalBids}
                    totalAsks={totalAsks}
                    totalPrecision={quote.get("precision")}
                    _totalValue={utils.format_number(
                        totalAsks,
                        quote.get("precision")
                    )}
                    flip={this.state.flip}
                    base={base}
                    quote={quote}
                    baseSymbol={baseSymbol}
                    quoteSymbol={quoteSymbol}
                    bodyRows={askRows}
                    onToggleShowAll={()=>this._onToggleShowAll("asks")}
                    halper = {this.props.halper}
                    halperBuy = {this.props.halperBuy}
                />}
            </div>
        );
    }
}

class OrderItem extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            rowCount: 20,
        };
    }

    render() {
        const { headerTitle, totalValue, type, baseSymbol, quoteSymbol, bodyRows, flip , totalPrecision, totalAsks, totalBids} = this.props;
        const { rowCount, showall } = this.state;

        let totalLength = bodyRows.length;
        /*if (!showall) {
            bodyRows.splice(rowCount, bodyRows.length);
        }*/
        let showSymbol = flip ? baseSymbol : quoteSymbol;

        return (
            <div className={this.props.halper === "halper" || this.props.halperBuy === "halperBuy" ? "order-book-item order-book-item-nft" : "order-book-item"}>
                <div className="order-book-title">
                    <Translate content={headerTitle} />

                    <div className="order-book-title-sub">
                        <Translate content="exchange.total" />:
                        {" "}
                        {__SCROOGE_CHAIN__ ?
                            <div className="order-book-scrooge">
                                <div className="order-book-scrooge-item">
                                    <FormattedValue
                                        value={totalBids}
                                        minimumFractionDigits={Math.min(totalPrecision, 0)}
                                        maximumFractionDigits={Math.max(totalPrecision, 2)}
                                    />{" "}
                                    <AssetName name={baseSymbol} />
                                </div>
                                <div className="order-book-scrooge-item">
                                    <FormattedValue
                                        value={totalAsks}
                                        minimumFractionDigits={Math.min(totalPrecision, 0)}
                                        maximumFractionDigits={Math.max(totalPrecision, 2)}
                                    />{" "}
                                    <AssetName name={quoteSymbol} />
                                </div>
                            </div> :
                            <>
                                <FormattedValue
                                    value={totalValue}
                                    minimumFractionDigits={Math.min(totalPrecision, 0)}
                                    maximumFractionDigits={Math.max(totalPrecision, 2)}
                                />{" "}
                                <AssetName name={ showSymbol } />
                            </>}
                    </div>
                </div>

                <OrderHeader  key={"order-header-" + type} type={type} baseSymbol={baseSymbol} quoteSymbol={quoteSymbol} reverse={!flip} halper={this.props.halper} />

                <TransitionWrapper
                    component="div"
                    className={classnames("order-book-body", {
                        "for-buy": flip,
                        "for-sell": !flip,
                    })}
                    transitionName="newrow">
                    {bodyRows}
                </TransitionWrapper>
                {/* @TODO скрыто по дизайну {totalLength > rowCount ? (
                    <div className="order-book-showall">
                        <a className="order-book-link" onClick={()=>this._onToggleShowAll(!showall)}>
                            <Translate
                                content={showall ? "exchange.hide" : "exchange.show_bids"}
                            />
                            {!showall ? (<span> ({totalLength})</span>) : null}
                        </a>
                    </div>
                ) : null}*/}
            </div>
        );
    }
}
class OrderHeader extends React.PureComponent {
    render () {
        const {baseSymbol, quoteSymbol, reverse, type} = this.props;

        let groupHead = [];
       
        groupHead.push(
            <div key={ "order-book-header-item-price-" + type} className="order-book-header-item price">
                <Translate className="header-sub-title" content="exchange.price"/>
            </div>
        );
        groupHead.push(
            <div key={ "order-book-header-item-asset-base-" + type} className="order-book-header-item asset-base">
                <span className="header-sub-title">
                    <AssetName dataPlace="top" name={baseSymbol} />
                </span>
            </div>
        );
        groupHead.push(
            <div key={ "order-book-header-item-asset-quote-" + type} className="order-book-header-item asset-quote">
                <span className="header-sub-title">
                    <AssetName dataPlace="top" name={quoteSymbol} />
                </span>
            </div>
        );
        groupHead.push(<div key={ "order-book-header-item-total-" + type} className="order-book-header-item total">
            <Translate
                className="header-sub-title"
                content="exchange.total"
            />
            <span className="header-sub-title">
                {" "}
                (<AssetName dataPlace="top" name={baseSymbol} />)
            </span>
        </div>);
        if(this.props.halper === "halper") {
            groupHead.push(<div key={"order-book-header-item"} className="order-book-header-item" />);
        } 

        // if( reverse ) {
        //     groupHead.reverse();
        // }


        return (
            <div key={ "order-book-header" + type} className={classnames("order-book-header", {
                "for-buy": !reverse,
                "for-sell": reverse,
            })}>
                <div className="order-book-header-row">
                    {groupHead}
                </div>
            </div>
        );
    }
}

OrderBook.defaultProps = {
    bids: [],
    asks: [],
    orders: {}
};

OrderBook.propTypes = {
    bids: PropTypes.array.isRequired,
    asks: PropTypes.array.isRequired,
    orders: PropTypes.object.isRequired
};

export default OrderBook;

import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import utils from "common/utils";
import Translate from "react-translate-component";
import counterpart from "counterpart";
import SettingsStore from "stores/SettingsStore";
import ChainTypes from "../Utility/ChainTypes";
import WrapperAuth from "../Utility/WrapperAuth";
import BindToChainState from "../Utility/BindToChainState";
import PriceText from "../Utility/PriceText";
import AssetName from "../Utility/AssetName";
import SimpleDepositWithdraw from "../Dashboard/SimpleDepositWithdraw";
import SimpleDepositBlocktradesBridge from "../Dashboard/SimpleDepositBlocktradesBridge";
import {Asset} from "common/MarketClasses";
import ExchangeInput from "./ExchangeInput";
import {getAlias} from "config/alias";
import {Select} from "antd";
import AssetSelect from "Components/Utility/AssetSelect";

import "react-datetime/css/react-datetime.css";
import Immutable from "immutable";

class BuySell extends React.Component {
    static propTypes = {
        balance: ChainTypes.ChainObject,
        type: PropTypes.string,
        amountChange: PropTypes.func.isRequired,
        priceChange: PropTypes.func.isRequired,
        onSubmit: PropTypes.func.isRequired,
        onExpirationTypeChange: PropTypes.func.isRequired,
        onExpirationCustomChange: PropTypes.func.isRequired
    };

    static defaultProps = {
        type: "bid"
    };

    state = {
        changePrice: false,
        yourPrice: false
    };

    shouldComponentUpdate(nextProps) {

        return (
            nextProps.amount !== this.props.amount ||
            nextProps.onBorrow !== this.props.onBorrow ||
            nextProps.total !== this.props.total ||
            nextProps.currentPrice !== this.props.currentPrice ||
            nextProps.price !== this.props.price ||
            nextProps.balance !== this.props.balance ||
            nextProps.account !== this.props.account ||
            nextProps.className !== this.props.className ||
            nextProps.quote.equals(this.props.quote)  ||
            (this.props.fee && !nextProps.fee.equals(this.props.fee) ) ||
            (this.props.feeAsset && !nextProps.feeAsset.equals(this.props.feeAsset)) ||
            !utils.are_equal_shallow(nextProps.feeAssets, this.props.feeAssets) ||
            nextProps.fee !== this.props.fee ||
            nextProps.quote !== this.props.quote ||
            nextProps.base !== this.props.base ||
            nextProps.isPredictionMarket !== this.props.isPredictionMarket ||
            nextProps.isOpen !== this.props.isOpen ||
            nextProps.hasFeeBalance !== this.props.hasFeeBalance ||
            nextProps.expirationType !== this.props.expirationType ||
            nextProps.expirationCustomTime !== this.props.expirationCustomTime
        );
    }

    _addBalance(balance) {
        if (this.props.type === "bid") {
            this.props.totalChange({
                target: {value: balance.getAmount({real: true}).toString()}
            });
        } else {
            this.props.amountChange({
                target: {value: balance.getAmount({real: true}).toString()}
            });
        }
    }

    _setPrice(price) {
        this.props.priceChange({target: {value: price.toString()}});
    }

    _onDeposit(e) {
        e.preventDefault();
        this.refs.deposit_modal.show();
    }

    _onBuy(e) {
        e.preventDefault();
        this.refs.bridge_modal.show();
    }

    render() {
        let {changePrice} = this.state;
        let {
            type,
            quote,
            base,
            amountChange,
            fee,
            isPredictionMarket,
            priceChange,
            onSubmit,
            balance,
            totalChange,
            balancePrecision,
            currentPrice,
            currentPriceObject,
            feeAsset,
            feeAssets,
            hasFeeBalance,
            backedCoin,
            floatPriceOrderSell
        } = this.props;

        if(!fee || !feeAsset) return null;

        let amount, price, total;

        if (this.props.amount) amount = this.props.amount;
        if (this.props.price) price = this.props.price;
        if (this.props.total) total = this.props.total;

        let balanceAmount = new Asset({
            amount: balance ? balance.get("balance") : 0,
            precision: balancePrecision,
            asset_id: this.props.balanceId
        });
        let plusFee = balanceAmount.equalsAsset(fee) ? fee.getAmount({real: true}) : 0;

        const isBid = type === "bid";

        let hasBalance = !isBid
            ? balanceAmount.getAmount({real: true}) >= parseFloat(amount) + parseFloat(plusFee)
            : balanceAmount.getAmount({real: true}) >= parseFloat(total) + parseFloat(plusFee);

        let noBalance = isPredictionMarket
            ? false
            : !(balanceAmount.getAmount() > 0 && hasBalance);

        let invalidPrice = !(price > 0);
        let invalidAmount = !(amount > 0);

        let buttonClass = classNames(this.props.className+"-btn btn", "btn-" + type);
        let balanceSymbol = !isBid ? quote.get("symbol") : base.get("symbol");
        let bidBalanceSymbol = isBid ? quote.get("symbol") : base.get("symbol");

        let disabledText = invalidPrice
            ? counterpart.translate("exchange.invalid_price")
            : invalidAmount
                ? counterpart.translate("exchange.invalid_amount")
                : noBalance
                    ? counterpart.translate("exchange.no_balance")
                    : null;


        // Subtract fee from amount to sell
        let balanceToAdd;

        if (feeAsset && feeAsset.get("symbol") === balanceSymbol) {
            balanceToAdd = balanceAmount.clone(
                balanceAmount.getAmount() - fee.getAmount()
            );
        } else {
            balanceToAdd = balanceAmount;
        }

        const maxBaseMarketFee = new Asset({
            amount: base.getIn(["options", "max_market_fee"]),
            asset_id: base.get("asset_id"),
            precision: base.get("precision")
        });
        const maxQuoteMarketFee = new Asset({
            amount: quote.getIn(["options", "max_market_fee"]),
            asset_id: quote.get("asset_id"),
            precision: quote.get("precision")
        });

        const baseMarketFeePercent = base.getIn(["options", "market_fee_percent"]) / 100 ;
        const quoteMarketFeePercent = quote.getIn(["options", "market_fee_percent"]) / 100 ;

        const quoteFee = !amount
            ? 0
            : Math.min(
                maxQuoteMarketFee.getAmount({real: true}),
                (amount * quote.getIn(["options", "market_fee_percent"])) /
                10000
            ).toFixed(maxQuoteMarketFee.precision);
        const baseFee = !amount
            ? 0
            : Math.min(
                maxBaseMarketFee.getAmount({real: true}),
                (total * base.getIn(["options", "market_fee_percent"])) /
                10000
            ).toFixed(maxBaseMarketFee.precision);


        let FieldsInputs = [
            <div key={`exchange${type}Total0`} className={this.props.className+"-fieldset"}>
                <Translate htmlFor={`${type}Amount`} className={this.props.className+"-label"} component={"label"} content={`exchange.${type}Amount`} />
                <div className={this.props.className+"-input-wrap"}>
                    <ExchangeInput
                        id={`${type}Amount`}
                        value={amount || ""}
                        onChange={amountChange}
                        className={this.props.className+"-input"}
                        autoComplete="off"
                        placeholder="0.0"
                        addonAfter={
                            <div className={this.props.className+"-input-info"}>
                                <AssetName
                                    dataPlace="right"
                                    name={quote.get("symbol")}
                                />
                            </div>
                        }
                    />
                </div>
            </div>,
            <div key={`exchange${type}Total1`} className={this.props.className+"-fieldset"}>
                <Translate htmlFor={`${type}Total`} className={this.props.className+"-label"} component={"label"} content={`exchange.${type}Total`} />
                <div className={this.props.className+"-input-wrap"}>
                    <ExchangeInput
                        id={`${type}Total`}
                        value={total || ""}
                        onChange={totalChange}
                        className={this.props.className+"-input"}
                        autoComplete="off"
                        placeholder="0.0"
                        addonAfter={
                            <div className={this.props.className+"-input-info"}>
                                <AssetName
                                    dataPlace="right"
                                    name={base.get("symbol")}
                                />
                            </div>
                        }
                    />

                </div>
            </div>
        ];

        return (
            <div className={this.props.className+"-wrap"}>
                <form
                    className={classNames(this.props.className + "-form", {
                        "hide": !this.props.isOpen
                    })}
                    noValidate
                >
                    <div className={this.props.className+"-fieldset"}>
                        {this.state.yourPrice ?
                        <Translate htmlFor={`${type}Price`} className={this.props.className+"-label"} style={{color: "#be3232", fontWeight: "bold"}} component={"label"} content="exchange.propose_price" />
                        : <Translate htmlFor={`${type}Price`} className={this.props.className+"-label"} component={"label"} content="exchange.price" />}
                        <div className={this.props.className+"-input-wrap"}>
                            <ExchangeInput
                                id={`${type}Price`}
                                value={utils.getParseBigNumber(price || (!changePrice && currentPrice) || "")}
                                onChange={priceChange}
                                onFocus={()=>this.setState({ changePrice: true })}
                                className={this.props.className+"-input"}
                                autoComplete="off"
                                placeholder="0.0"
                                addonAfter={<div className={this.props.className+"-input-info"}>
                                    <AssetName
                                        dataPlace="right"
                                        name={base.get("symbol")}
                                    />
                                    &nbsp;/&nbsp;
                                    <AssetName
                                        dataPlace="right"
                                        name={quote.get("symbol")}
                                    />
                                </div>}
                            />

                        </div>
                    </div>

                    {!isBid ? FieldsInputs : FieldsInputs.reverse()}

                    <div className={this.props.className+"-fieldset"}>
                        <Translate
                            className={this.props.className+"-label"}
                            content="transfer.fee"
                        />
                        <div className={this.props.className+"-input-wrap"}>

                            <ExchangeInput
                                id={`${type}Fee`}
                                placeholder="0.0"
                                value={fee.getAmount({real: true})}
                                defaultValue={
                                    !hasFeeBalance
                                        ? counterpart.translate("transfer.errors.insufficient")
                                        : fee.getAmount({real: true})
                                }
                                disabled
                                addonAfter={
                                    <AssetSelect
                                        style={{width: "130px"}}
                                        selectStyle={{width: "100%"}}
                                        selectId={"symbol"}
                                        value={feeAsset ? feeAsset.get("symbol") : null}
                                        assets={feeAssets ? feeAssets.map(asset=>asset.get("symbol")) : []}
                                        onChange={value=>this.props.onChangeFeeAsset(value)}
                                    />
                                }
                            />
                        </div>
                    </div>

                    <div className={this.props.className+"-info"}>
                        <div className={this.props.className+"-info-row"}>
                            { isBid && quoteMarketFeePercent ? (
                                <>
                                    <Translate className={this.props.className+"-info-name"} content="transfer.market_fee" percent={quoteMarketFeePercent+ "%"} />
                                    <span className={this.props.className+"-info-value"}>
                                        {quoteFee} <AssetName name={bidBalanceSymbol} />
                                    </span>
                                </>
                            ) : null }
                            { !isBid && baseMarketFeePercent ? (
                                <>
                                    <Translate className={this.props.className+"-info-name"} content="transfer.market_fee" percent={baseMarketFeePercent+ "%"} />
                                    <span className={this.props.className+"-info-value"}>
                                        {baseFee} <AssetName name={bidBalanceSymbol} />
                                    </span>
                                </>
                            ) : null }
                        </div>


                        <div className={this.props.className+"-info-row"}>
                            <Translate className={this.props.className+"-info-name"} content="transfer.fee" />
                            <span className={this.props.className+"-info-value"}>
                                {!hasFeeBalance
                                    ? counterpart.translate("transfer.errors.insufficient")
                                    : [fee.getAmount({real: true}), feeAssets[0].get("symbol")].join(" ") }
                            </span>
                        </div>
                        <div className={this.props.className+"-info-row"}>
                            <Translate className={this.props.className+"-info-name"} content="exchange.balance" />
                            <span className={this.props.className+"-info-value"}>
                                <span className={this.props.className+"-info-link"} onClick={() => this._addBalance(balanceToAdd)}>
                                    {utils.format_number(balanceAmount.getAmount({real: true}), balancePrecision)}
                                    {" "}
                                    <AssetName name={balanceSymbol} />
                                </span>
                            </span>
                        </div>
                        {this.props.halper === "halper" ? null :
                        <div className={this.props.className+"-info-row"}>
                            <Translate className={this.props.className+"-info-name"} content={isBid ? "exchange.lowest_ask" : "exchange.highest_bid"} />

                            <span className={this.props.className+"-info-value"}>
                                {currentPrice ? (
                                    <span className={this.props.className+"-info-link"} onClick={()=>this.props.setPrice(type, currentPriceObject.sellPrice())}>
                                        <PriceText
                                            price={currentPrice}
                                            quote={quote}
                                            base={base}
                                        />{" "}
                                        <AssetName
                                            name={base.get(
                                                "symbol"
                                            )}
                                        />
                                        /
                                        <AssetName
                                            name={quote.get(
                                                "symbol"
                                            )}
                                        />
                                    </span>
                                ) : null}
                            </span>
                        </div>} 

                    </div>

                    {/* BUY/SELL button */}
                    <div data-tip={disabledText ? disabledText : ""} data-place="right">
                        <WrapperAuth
                            component={"button"}
                            className={buttonClass}
                            type="button"
                            disabled={!!disabledText}
                            onClick={(event)=>onSubmit(event, true)}
                        >
                            <Translate
                                asset={getAlias(this.props.quote.get("symbol"))}
                                content={isBid ? "exchange.buy_asset" : "exchange.sell_asset"}
                            />
                        </WrapperAuth>

                    </div>
                </form>
                {this.props.halperBuy === "halperBuy" && !this.state.yourPrice ?
                    <div className="btn btn-green" style={{width: "100%", marginTop: "4px", fontSize: "13px", fontWeight: "bold"}} onClick={() => this.setState({yourPrice: true})}>
                        <Translate content={"exchange.offer"}/>
                    </div> 
                : null}
                <SimpleDepositWithdraw
                    ref="deposit_modal"
                    action="deposit"
                    fiatModal={false}
                    account={this.props.currentAccount.get("name")}
                    sender={this.props.currentAccount.get("id")}
                    asset={this.props[isBid ? "base" : "quote"].get("id")}
                    modalId={
                        "simple_deposit_modal" + (type === "bid" ? "" : "_ask")
                    }
                    balances={[this.props.balance]}
                    {...backedCoin}
                />

                {/* Bridge modal */}
                <SimpleDepositBlocktradesBridge
                    ref="bridge_modal"
                    action="deposit"
                    account={this.props.currentAccount.get("name")}
                    sender={this.props.currentAccount.get("id")}
                    asset={this.props.balanceId}
                    modalId={
                        "simple_bridge_modal" + (type === "bid" ? "" : "_ask")
                    }
                    balances={[this.props.balance]}
                    bridges={this.props.currentBridges}
                />
            </div>
        );
    }
}

export default BindToChainState(BuySell, {keep_updating: true});

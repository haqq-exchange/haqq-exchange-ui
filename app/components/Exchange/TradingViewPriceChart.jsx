import React from "react";
//import TradingView from "tradingview";
const TradingView = require("../../assets/charting_library/charting_library.min");
import colors from "assets/colorsTrading";
import { diff } from "deep-object-diff";
import {getResolutionsFromBuckets, getTVTimezone} from "./tradingViewClasses";
import ls from "common/localStorage";
const ss = new ls("tvxwevents");
// import {connect} from "alt-react";
// import MarketsStore from "stores/MarketsStore";

export default class TradingViewPriceChart extends React.Component {

    isMount = false;
    
    loadTradingView(props) {
        const _this = this;
        const {dataFeed} = props;
        let themeColors = colors[props.theme];
        let symbolId = [props.quoteSymbol , props.baseSymbol].join("_");

        if (!dataFeed) return;
        if (this.tvWidget) return;


        // console.log("loadTradingView props ", props);

        if (__DEV__) {
            console.log(
                "currentResolution",
                getResolutionsFromBuckets([props.bucketSize])[0],
                "symbol",
                symbolId,
                "timezone:",
                getTVTimezone()
            );
        }

        dataFeed.update({
            resolutions: props.buckets,
            ticker: props.quoteSymbol + "_" + props.baseSymbol,
            interval: getResolutionsFromBuckets([props.bucketSize])[0]
        });

        if (__DEV__) console.log("*** Load Chart ***");
        // if (__DEV__) console.time("*** Chart load time: ");

        //console.log('themeColors', themeColors);
        // console.log("TradingView", TradingView);
        // console.log("dataFeed", dataFeed);

        _this.tvWidget = window.tvWidget = new TradingView.widget({
            fullscreen: false,
            symbol: symbolId,
            interval: getResolutionsFromBuckets([props.bucketSize])[0],
            library_path: "/charting_library/",
            datafeed: dataFeed,
            theme: props.theme === "lightTheme" ? "light" : "dark",
            custom_css_url: props.theme + ".css",
            container_id: "tv_chart",
            autosize: true,
            locale: props.locale,
            timezone: getTVTimezone(),

            //toolbar_bg: themeColors.toolBarbg,
            overrides: {
                "paneProperties.background": themeColors.toolBarbg,
                "paneProperties.horzGridProperties.color": themeColors.horzGridProperties.color,
                "paneProperties.vertGridProperties.color": themeColors.vertGridProperties.color,
            },
            enabled_features: ["keep_left_toolbar_visible_on_small_screens"],
            disabled_features: [
                "header_saveload",
                "symbol_info",
                "symbol_search_hot_key",
                "header_symbol_search",
                "header_compare"
            ],
            favorites: {
                chartTypes: ["Area", "Line"]
            },
            debug: false,
            chartOrigin: window.location.origin,
            // charts_storage_url: "https://saveload.tradingview.com",
            // charts_storage_api_version: "1.1",
            // client_id: window.location.hostname,
            // client: "tradingview.com",
            // user: "public_user_id",
            client_id: "client_id",
            user_id: "user_id",
            preset: this.props.mobile ? "mobile" : "",
            customFormatters: {
                timeFormatter: {
                    format: function(date, type) {
                        let timeOptions = {
                            hour: "numeric",
                            minute: "numeric",
                            second: "numeric",
                            hour12: false
                        };

                        //console.log('date, type', date, type)

                        /*return new Intl
                            .DateTimeFormat(props.locale, timeOptions)
                            .format(date);*/

                        return [
                            _this._pad(date.getUTCHours(), 2),
                            _this._pad(date.getUTCMinutes(), 2)
                        ].join(":");
                    }
                },
                dateFormatter: {
                    format: function(date) {
                        return new Intl.DateTimeFormat(props.locale).format(
                            date
                        );
                    }
                }
            }
        });

        //console.log("_this.tvWidget", _this.tvWidget);




        _this.tvWidget.onChartReady(() => {
            if (__DEV__) console.log("*** Chart Ready ***");
            // if (__DEV__) console.timeEnd("*** Chart load time: ");

            dataFeed.update({
                onMarketChange: () => _this._setSymbol(symbolId)
            });

            //console.log('_this.tvWidget', _this.tvWidget.activeChart());
            //_this.tvWidget.hideAllDrawingTools();
            _this.tvWidget.subscribe("onChartReady", function() {
                try {
                    let saveData  = ss.get(".saveData");
                    if( saveData && saveData.panes ) {
                        /*
                        * delete cache symbol
                        * */
                        if( saveData.panes[0].state.symbol ) {
                            delete saveData.panes[0].state.symbol;
                        }
                        _this.tvWidget.load(saveData);

                    }

                } catch (e) {}
            });
            _this.tvWidget.subscribe("onTick", function() {
                //console.log("realtime_tick event", event);
            });
            _this.tvWidget.subscribe("chart_loaded", function(event) {
                // console.log("chart_loaded event", event);
            });
            _this.tvWidget.subscribe("onAutoSaveNeeded", function() {
                _this.tvWidget.save(data => {

                    ss.set(".saveData", data.charts[0]);
                });
            });

            // console.log("props.saveData", ss.get(".saveData"), props.theme);

            if( ss.get(".theme") !== props.theme ) {
                _this._changeTheme(props);
            }


        });


    }

    _pad(num, size) {
        let s = num + "";
        while (s.length < size) s = "0" + s;
        return s;
    }

    componentDidUpdate(prevProps) {
        // console.log("TradingViewPriceChart componentDidUpdate", this.tvWidget );
        // console.log("prevProps", prevProps);
        // console.log("this.props", this.props);
        // console.log("snapshot", prevProps.priceData !== this.props.priceData);
        if( this.isMount ) {
            if( prevProps.priceData !== this.props.priceData || this.props.marketReady !== prevProps.marketReady ) {
                this.loadTradingView(this.props);
            }
        }
    }

    UNSAFE_componentWillReceiveProps(np) {
        if( this.isMount ) {
            if( this.tvWidget ) {
                const options = this.tvWidget._options;
                const propsSymbol = [np.quoteSymbol, np.baseSymbol].join("_");
                const { theme } = diff(np, this.props);
    
                if (!np.marketReady) return;
                if (!this.props.dataFeed && np.dataFeed) {
                    this.loadTradingView(this.props);
                }
                if (theme) {
                    /* Reload chart */
                    this._changeTheme(np);
                }
    
                // console.log("options.symbol !== propsSymbol", options.symbol , propsSymbol)
                if (options.symbol !== propsSymbol) {
                    this._setSymbol(propsSymbol);
                }
            }
        }
    }

    _changeSymbol(props) {
        const options = this.tvWidget._options;
        const propsSymbol = [props.quoteSymbol, props.baseSymbol].join("_");

        this._setSymbol(propsSymbol);
    }

    _changeTheme(np) {
        ss.set(".theme", np.theme);
        let themeColors = colors[np.theme];

        this.tvWidget.changeTheme( np.theme === "lightTheme" ? "light" : "dark" );
        this.tvWidget.addCustomCSSFile( np.theme + ".css");


        this.tvWidget.applyOverrides({
            "paneProperties.background": themeColors.bgColor,
            "paneProperties.horzGridProperties.color": themeColors.horzGridProperties.color,
            "paneProperties.vertGridProperties.color": themeColors.vertGridProperties.color,
        });
    }
    _setSymbol(ticker) {
        // console.log("onMarketChange _setSymbol", ticker, this.tvWidget);
        if (this.tvWidget && ticker) {


            try {
                //ticker = ticker.split("_").map(asset=>getAlias(asset)).join("_");
                this.tvWidget.setSymbol(
                    ticker,
                    getResolutionsFromBuckets([this.props.bucketSize])[0]
                );
                this.tvWidget._options.symbol = ticker;
            } catch (e) {
                console.error(e);
            }
        }
    }

    componentDidMount() {
        this.isMount = true;
        console.log("TradingViewPriceChart componentDidMount", this.props);
        if( this.props.marketReady ) {
            this.loadTradingView(this.props);
        }
    }


    componentWillUnmount() {
        console.log("this.tvWidget", this.tvWidget)
        this.props.dataFeed.clearSubs();
        this.isMount = false;
    }

    /*shouldComponentUpdate(np) {
        console.log("priceData", np.priceData, this.props.priceData);
        if (np.chartHeight !== this.props.chartHeight) return true;
        if (this.tvWidget) return false;
        if (!np.marketReady) return false;
        return true;
    }*/

    render() {
        return (
            <div className="small-12" >
                <div
                    className="exchange-bordered"
                    style={{
                        marginTop: 10,
                        marginBottom: 10,
                        height: this.props.chartHeight + "px"
                    }}
                    id="tv_chart"
                />
            </div>
        );
    }
}

import React from "react";
import PropTypes from "prop-types";
import MarketsActions from "actions/MarketsActions";
import {MyOpenOrders} from "../MyOpenOrders";
// import MarketPicker from "../MarketPicker";
import utils from "common/utils";
import {cloneDeep, debounce} from "lodash";
import notify from "actions/NotificationActions";
// import AccountNotifications from "Components/Notifier/NotifierContainer";
import Ps from "perfect-scrollbar";
import {ChainStore, FetchChain} from "deexjs";
import SettingsActions from "actions/SettingsActions";
import cnames from "classnames";
import market_utils from "common/market_utils";
import {Asset, LimitOrderCreate, Price} from "common/MarketClasses";
import Translate from "react-translate-component";
import {Apis} from "deexjs-ws";
import {checkFeeStatusAsync} from "common/trxHelper";
import {getAlias} from "config/alias";
import Icon from "Components/Icon/Icon";
import moment from "moment";
import guide from "intro.js";
import translator from "counterpart";
import loadable from "loadable-components";
import MarketDeals from "./MarketDeals";
import ExchangeHeader from "../ExchangeHeader";

const MobileModal = loadable(() => import("Components/Modal/MobileModal"));
const OrderBook = loadable(() => import("./OrderBook"));
const MyMarkets = loadable(() => import("../MyMarkets"));
const BuySell = loadable(() => import("../BuySell"));
const TradingViewPriceChart = loadable(() => import("../TradingViewPriceChart"));
import "../Exchange.scss";
import ModalActions from "actions/ModalActions";

class Exchange extends React.Component {
    static propTypes = {
        marketCallOrders: PropTypes.object.isRequired,
        activeMarketHistory: PropTypes.object.isRequired,
        viewSettings: PropTypes.object.isRequired,
        priceData: PropTypes.array.isRequired,
        volumeData: PropTypes.array.isRequired
    };

    static defaultProps = {
        marketCallOrders: [],
        activeMarketHistory: {},
        viewSettings: {},
        priceData: [],
        volumeData: [],
        expirations: {
            HOUR: {
                title: "1 hour",
                get: () =>
                    moment()
                        .add(1, "hour")
                        .valueOf()
            },
            "12HOURS": {
                title: "12 hours",
                get: () =>
                    moment()
                        .add(12, "hour")
                        .valueOf()
            },
            "24HOURS": {
                title: "24 hours",
                get: () =>
                    moment()
                        .add(1, "day")
                        .valueOf()
            },
            "7DAYS": {
                title: "7 days",
                get: () =>
                    moment()
                        .add(7, "day")
                        .valueOf()
            },
            MONTH: {
                title: "30 days",
                get: () =>
                    moment()
                        .add(30, "day")
                        .valueOf()
            },
            YEAR: {
                title: "1 year",
                get: () =>
                    moment()
                        .add(1, "year")
                        .valueOf()
            },
            SPECIFIC: {
                title: "Specific",
                get: type => {
                    return this.state.expirationCustomTime[type].valueOf();
                }
            }
        }

    };

    constructor(props) {
        super();
        this.state = {
            ...this._initialState(props),
            expirationType: {
                bid: props.exchange.getIn(["lastExpiration", "bid"]) || "YEAR",
                ask: props.exchange.getIn(["lastExpiration", "ask"]) || "YEAR"
            },
            expirationCustomTime: {
                bid: moment().add(1, "day"),
                ask: moment().add(1, "day")
            },
            showSell: true,
            feeStatus: {},
            visibleBody: {
                trading: true
            }
        };

        this._getWindowSize = debounce(this._getWindowSize.bind(this), 150);
        this._currentPriceClick = this._currentPriceClick.bind(this);
        //this._checkFeeStatus = this._checkFeeStatus.bind(this);

        // this._handleExpirationChange = this._handleExpirationChange.bind(this);
        this._handleCustomExpirationChange = this._handleCustomExpirationChange.bind(this);

        this.psInit = true;
    }

    _changeSellBlock(value) {
        this.setState({
            showSell: value
        });
    }

    _handleExpirationChange(type, e) {
        let expirationType = {
            ...this.state.expirationType,
            [type]: e.target.value
        };

        if (e.target.value !== "SPECIFIC") {
            SettingsActions.setExchangeLastExpiration({
                ...((this.props.exchange.has("lastExpiration") &&
                    this.props.exchange.get("lastExpiration").toJS()) ||
                    {}),
                [type]: e.target.value
            });
        }

        this.setState({
            expirationType: expirationType
        });
    }

    _handleCustomExpirationChange(type, time) {
        let expirationCustomTime = {
            ...this.state.expirationCustomTime,
            [type]: time
        };

        this.setState({
            expirationCustomTime: expirationCustomTime
        });
    }


    _initialState(props) {
        let ws = props.viewSettings;
        let bid = {
            forSaleText: "",
            toReceiveText: "",
            priceText: "",
            for_sale: new Asset({
                asset_id: props.baseAsset.get("id"),
                precision: props.baseAsset.get("precision")
            }),
            to_receive: new Asset({
                asset_id: props.quoteAsset.get("id"),
                precision: props.quoteAsset.get("precision")
            })
        };
        bid.price = new Price({base: bid.for_sale, quote: bid.to_receive});
        let ask = {
            forSaleText: "",
            toReceiveText: "",
            priceText: "",
            for_sale: new Asset({
                asset_id: props.quoteAsset.get("id"),
                precision: props.quoteAsset.get("precision")
            }),
            to_receive: new Asset({
                asset_id: props.baseAsset.get("id"),
                precision: props.baseAsset.get("precision")
            })
        };
        ask.price = new Price({base: ask.for_sale, quote: ask.to_receive});

        return {
            hideMarketSearch: true,
            history: [],
            buySellOpen: ws.get("buySellOpen", true),
            bid,
            ask,
            flipBuySell: ws.get("flipBuySell", false),
            favorite: false,
            showDepthChart: ws.get("showDepthChart", false),
            leftOrderBook: ws.get("leftOrderBook", false),
            buyDiff: false,
            sellDiff: false,
            buySellTop: ws.get("buySellTop", true),
            buyFeeAssetIdx: ws.get("buyFeeAssetIdx", 0),
            sellFeeAssetIdx: ws.get("sellFeeAssetIdx", 0),
            height: window.innerHeight,
            width: window.innerWidth,
            hidePanel: false,
            chartHeight: ws.get("chartHeight", 310),
            currentPeriod: ws.get("currentPeriod", 3600 * 24 * 30 * 3) // 3 months
        };
    }

    _getLastMarketKey() {
        const chainID = Apis.instance().chain_id;
        return `lastMarket${chainID ? "_" + chainID.substr(0, 8) : ""}`;
    }

    UNSAFE_componentWillMount() {
        this._checkFeeStatus();
    }

    componentDidMount() {
        SettingsActions.changeViewSetting.defer({
            [this._getLastMarketKey()]:
            this.props.quoteAsset.get("symbol") +
            "_" +
            this.props.baseAsset.get("symbol")
        });

        window.addEventListener("resize", this._getWindowSize, {
            capture: false,
            passive: true
        });
    }

    shouldComponentUpdate(np, ns) {

        if (!np.marketReady && !this.props.marketReady) {
            return false;
        }
        // let prevOrders = np.currentAccount.get("orders");
        // let currentOrders = this.props.currentAccount.get("orders");
        // console.log("shouldComponentUpdate prevOrders", prevOrders);
        // console.log("shouldComponentUpdate currentOrders", currentOrders);
        // console.log("shouldComponentUpdate marketLimitOrders", this.props, np);
        let propsChanged = false;
        for (let key in np) {
            if (np.hasOwnProperty(key)) {
                propsChanged = propsChanged || !utils.are_equal_shallow(np[key], this.props[key]);
                if (propsChanged) break;
            }
        }
        // console.log("shouldComponentUpdate is => ", propsChanged || !utils.are_equal_shallow(ns, this.state));
        return propsChanged
            || !utils.are_equal_shallow(ns, this.state)
            || utils.are_equal_shallow(ns.visibleBody, this.state.visibleBody);
    }

    _checkFeeStatus(
        assets = [
            this.props.coreAsset,
            this.props.baseAsset,
            this.props.quoteAsset
        ],
        account = this.props.currentAccount
    ) {
        let feeStatus = {};
        let p = [];
        assets.forEach(a => {
            p.push(
                checkFeeStatusAsync({
                    accountID: account.get("id"),
                    feeID: a.get("id"),
                    type: "limit_order_create"
                })
            );
        });
        Promise.all(p)
            .then(status => {
                assets.forEach((a, idx) => {
                    feeStatus[a.get("id")] = status[idx];
                });
                if (!utils.are_equal_shallow(this.state.feeStatus, feeStatus)) {
                    this.setState({
                        feeStatus
                    });
                }
            })
            .catch(err => {
                this.setState({feeStatus: {}});
            });
    }

    _getWindowSize() {
        let {innerHeight, innerWidth} = window;
        if (
            innerHeight !== this.state.height ||
            innerWidth !== this.state.width
        ) {
            this.setState({
                height: innerHeight,
                width: innerWidth
            });
            let centerContainer = this.refsCenter;
            if (centerContainer) {
                Ps.update(centerContainer);
            }
        }
    }

    componentDidUpdate(prevProps, prevState) {
        this._initPsContainer();
        if (
            !this.props.exchange.get("tutorialShown") &&
            prevProps.coreAsset &&
            prevState.feeStatus
        ) {
            if (!this.tutorialShown) {
                this.tutorialShown = true;
                const theme = this.props.settings.get("themes");

                guide
                    .introJs()
                    .setOptions({
                        tooltipClass: theme,
                        highlightClass: theme,
                        showBullets: false,
                        hideNext: true,
                        hidePrev: true,
                        nextLabel: translator.translate(
                            "walkthrough.next_label"
                        ),
                        prevLabel: translator.translate(
                            "walkthrough.prev_label"
                        ),
                        skipLabel: translator.translate(
                            "walkthrough.skip_label"
                        ),
                        doneLabel: translator.translate(
                            "walkthrough.done_label"
                        )
                    });
                //.start();

                //SettingsActions.setExchangeTutorialShown.defer(true);
            }
        }
    }

    _initPsContainer() {
        if (this.refsCenter && this.psInit) {
            let centerContainer = this.refsCenter;
            if (centerContainer) {
                Ps.initialize(centerContainer);
                this.psInit = false;
            }
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        this._initPsContainer();
        if (
            nextProps.quoteAsset !== this.props.quoteAsset ||
            nextProps.baseAsset !== this.props.baseAsset ||
            nextProps.currentAccount !== this.props.currentAccount
        ) {
            this._checkFeeStatus(
                [
                    nextProps.coreAsset,
                    nextProps.baseAsset,
                    nextProps.quoteAsset
                ],
                nextProps.currentAccount
            );
        }
        if (
            nextProps.quoteAsset.get("symbol") !==
            this.props.quoteAsset.get("symbol") ||
            nextProps.baseAsset.get("symbol") !==
            this.props.baseAsset.get("symbol")
        ) {
            this.setState(this._initialState(nextProps));

            return SettingsActions.changeViewSetting({
                [this._getLastMarketKey()]:
                nextProps.quoteAsset.get("symbol") +
                "_" +
                nextProps.baseAsset.get("symbol")
            });
        }

        // if (this.props.sub && nextProps.bucketSize !== this.props.bucketSize) {
        //     return this._changeBucketSize(nextProps.bucketSize);
        // }
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this._getWindowSize);
    }

    _getFeeAssets(quote, base, coreAsset) {
        let {currentAccount} = this.props;
        const {feeStatus} = this.state;

        function addMissingAsset(target, asset) {
            if (target.indexOf(asset) === -1) {
                target.push(asset);
            }
        }

        function hasFeePoolBalance(id) {
            return feeStatus[id] && feeStatus[id].hasPoolBalance;
        }

        function hasBalance(id) {
            return feeStatus[id] && feeStatus[id].hasBalance;
        }

        let sellAssets = [coreAsset, quote === coreAsset ? base : quote];
        addMissingAsset(sellAssets, quote);
        addMissingAsset(sellAssets, base);
        // let sellFeeAsset;

        let buyAssets = [coreAsset, base === coreAsset ? quote : base];
        addMissingAsset(buyAssets, quote);
        addMissingAsset(buyAssets, base);
        // let buyFeeAsset;

        let balances = {};

        currentAccount
            .get("balances", [])
            .filter((balance, id) => {
                return (
                    ["1.3.0", quote.get("id"), base.get("id")].indexOf(id) >= 0
                );
            })
            .forEach((balance, id) => {
                let balanceObject = ChainStore.getObject(balance);
                balances[id] = {
                    balance: balanceObject
                        ? parseInt(balanceObject.get("balance"), 10)
                        : 0,
                    fee: this._getFee(ChainStore.getAsset(id))
                };
            });

        function filterAndDefault(assets, balances, idx) {
            let asset;
            /* Only keep assets for which the user has a balance larger than the fee, and for which the fee pool is valid */

            assets = assets.filter(a => {
                if (!balances[a.get("id")] || "1.3.0" !== a.get("id")) {
                    return false;
                }
                return (
                    hasFeePoolBalance(a.get("id")) && hasBalance(a.get("id"))
                );
            });

            /* If the user has no valid balances, default to core fee */
            if (!assets.length) {
                asset = coreAsset;
                assets.push(coreAsset);
                /* If the user has balances, use the stored idx value unless that asset is no longer available*/
            } else {
                asset = assets[Math.min(assets.length - 1, idx)];
            }

            return {assets, asset};
        }

        // console.log("sellAssets", sellAssets, balances)
        // console.log("buyAssets", sellAssets, balances)

        let {assets: sellFeeAssets, asset: sellFeeAsset} = filterAndDefault(
            sellAssets,
            balances,
            this.state.sellFeeAssetIdx
        );
        let {assets: buyFeeAssets, asset: buyFeeAsset} = filterAndDefault(
            buyAssets,
            balances,
            this.state.buyFeeAssetIdx
        );

        // console.log("sellFeeAssets", sellFeeAssets);
        // console.log("buyFeeAssets", buyFeeAssets);

        let sellFee = this._getFee(sellFeeAsset);
        let buyFee = this._getFee(buyFeeAsset);

        return {
            sellFeeAsset,
            sellFeeAssets,
            sellFee,
            buyFeeAsset,
            buyFeeAssets,
            buyFee
        };
    }

    _getFee(asset = this.props.coreAsset) {
        return (
            this.state.feeStatus[asset.get("id")] &&
            this.state.feeStatus[asset.get("id")].fee
        );
    }

    _verifyFee(fee, sell, sellBalance, coreBalance) {
        let coreFee = this._getFee();

        if (fee.asset_id === "1.3.0") {
            if (coreFee.getAmount() <= coreBalance) {
                return "1.3.0";
            } else {
                return null;
            }
        } else {
            let sellSum =
                sell.asset_id === fee.asset_id
                    ? fee.getAmount() + sell.getAmount()
                    : sell.getAmount();
            if (sellSum <= sellBalance) {
                // Sufficient balance in asset to pay fee
                return fee.asset_id;
            } else if (
                coreFee.getAmount() <= coreBalance &&
                fee.asset_id !== "1.3.0"
            ) {
                // Sufficient balance in core asset to pay fee
                return "1.3.0";
            } else {
                return null; // Unable to pay fee
            }
        }
    }

    _createLimitOrderConfirm(
        buyAsset,
        sellAsset,
        sellBalance,
        coreBalance,
        feeAsset,
        type,
        short = true,
        e
    ) {

        //console.log('e', e)

        //e.preventDefault();
        let {highestBid, lowestAsk} = this.props.marketData;
        let current = this.state[type === "sell" ? "ask" : "bid"];
        let chainSellBalance = ChainStore.getObject(sellBalance);
        let chainCoreBalance = ChainStore.getObject(coreBalance);

        sellBalance = current.for_sale.clone(
            sellBalance && chainSellBalance
                ? parseInt(chainSellBalance.toJS().balance, 10)
                : 0
        );
        coreBalance = new Asset({
            amount: coreBalance && chainCoreBalance
                ? parseInt(chainCoreBalance.toJS().balance, 10)
                : 0
        });

        let fee = this._getFee(feeAsset);
        let feeID = this._verifyFee(
            fee,
            current.for_sale,
            sellBalance.getAmount(),
            coreBalance.getAmount()
        );
        if (!feeID) {
            return notify.addNotification({
                message: "Insufficient funds to pay fees",
                level: "error"
            });
        }

        if (type === "buy" && lowestAsk) {
            let diff = this.state.bid.price.toReal() / lowestAsk.getPrice();
            if (diff > 1.2) {
                if(this.refBuy) this.refBuy.show();
                return this.setState({
                    buyDiff: diff
                });
            }
        } else if (type === "sell" && highestBid) {
            let diff =
                1 / (this.state.ask.price.toReal() / highestBid.getPrice());
            if (diff > 1.2) {
                if(this.refSell) this.refSell.show();
                return this.setState({
                    sellDiff: diff
                });
            }
        }

        let isPredictionMarket = sellAsset.getIn([
            "bitasset",
            "is_prediction_market"
        ]);

        if (current.for_sale.gt(sellBalance) && !isPredictionMarket) {
            return notify.addNotification({
                message:
                    "Insufficient funds to place order, you need at least " +
                    current.for_sale.getAmount({real: true}) +
                    " " +
                    sellAsset.get("symbol"),
                level: "error"
            });
        }
        //
        if (
            !(
                current.for_sale.getAmount() > 0 &&
                current.to_receive.getAmount() > 0
            )
        ) {
            return notify.addNotification({
                message: "Please enter a valid amount and price",
                level: "error"
            });
        }
        //
        if (type === "sell" && isPredictionMarket && short) {
            return this._createPredictionShort(feeID);
        }

        this._createLimitOrder(type, feeID);
    }

    _createLimitOrder(type, feeID) {
        let actionType = type === "sell" ? "ask" : "bid";

        let current = this.state[actionType];

        let expirationTime = null;
        if (this.state.expirationType[actionType] === "SPECIFIC") {
            expirationTime = this.props.expirations[this.state.expirationType[actionType]].get(actionType);
        } else {
            expirationTime = this.props.expirations[this.state.expirationType[actionType]].get();
        }

        const order = new LimitOrderCreate({
            for_sale: current.for_sale,
            expiration: new Date(expirationTime || false),
            to_receive: current.to_receive,
            seller: this.props.currentAccount.get("id"),
            fee: {
                asset_id: feeID,
                amount: 0
            }
        });
        const {marketName, first} = market_utils.getMarketName(
            this.props.baseAsset,
            this.props.quoteAsset
        );
        const inverted = this.props.marketDirections.get(marketName);
        const shouldFlip =
            (inverted && first.get("id") !== this.props.baseAsset.get("id")) ||
            (!inverted && first.get("id") !== this.props.baseAsset.get("id"));
        if (shouldFlip) {
            let setting = {};
            setting[marketName] = !inverted;
            SettingsActions.changeMarketDirection(setting);
        }
        return MarketsActions.createLimitOrder2(order)
            .then(result => {
                if (result.error) {
                    if (result.error.message !== "wallet locked")
                        notify.addNotification({
                            message:
                                "Unknown error. Failed to place order for " +
                                current.to_receive.getAmount({real: true}) +
                                " " +
                                current.to_receive.asset_id,
                            level: "error"
                        });
                }
            })
            .catch(e => {
            });
    }

    _createPredictionShort(feeID) {
        let current = this.state.ask;
        const order = new LimitOrderCreate({
            for_sale: current.for_sale,
            to_receive: current.to_receive,
            seller: this.props.currentAccount.get("id"),
            fee: {
                asset_id: feeID,
                amount: 0
            }
        });

        Promise.all([
            FetchChain(
                "getAsset",
                this.props.quoteAsset.getIn([
                    "bitasset",
                    "options",
                    "short_backing_asset"
                ])
            )
        ]).then(assets => {
            let [backingAsset] = assets;
            let collateral = new Asset({
                amount: order.amount_for_sale.getAmount(),
                asset_id: backingAsset.get("id"),
                precision: backingAsset.get("precision")
            });

            MarketsActions.createPredictionShort(order, collateral).then(
                result => {
                    if (result.error) {
                        if (result.error.message !== "wallet locked")
                            notify.addNotification({
                                message:
                                    "Unknown error. Failed to place order for " +
                                    buyAssetAmount +
                                    " " +
                                    buyAsset.symbol,
                                level: "error"
                            });
                    }
                }
            );
        });
    }

    _forceBuy(type, feeAsset, sellBalance, coreBalance) {
        let current = this.state[type === "sell" ? "ask" : "bid"];
        // Convert fee to relevant asset fee and check if user has sufficient balance
        sellBalance = current.for_sale.clone(
            sellBalance
                ? parseInt(ChainStore.getObject(sellBalance).get("balance"), 10)
                : 0
        );
        coreBalance = new Asset({
            amount: coreBalance
                ? parseInt(ChainStore.getObject(coreBalance).toJS().balance, 10)
                : 0
        });
        let fee = this._getFee(feeAsset);
        let feeID = this._verifyFee(
            fee,
            current.for_sale,
            sellBalance.getAmount(),
            coreBalance.getAmount()
        );

        if (feeID) {
            this._createLimitOrder(type, feeID);
        } else {
            console.error("Unable to pay fees, aborting limit order creation");
        }
    }

    _forceSell(type, feeAsset, sellBalance, coreBalance) {
        let current = this.state[type === "sell" ? "ask" : "bid"];
        // Convert fee to relevant asset fee and check if user has sufficient balance
        sellBalance = current.for_sale.clone(
            sellBalance
                ? parseInt(ChainStore.getObject(sellBalance).get("balance"), 10)
                : 0
        );
        coreBalance = new Asset({
            amount: coreBalance
                ? parseInt(ChainStore.getObject(coreBalance).toJS().balance, 10)
                : 0
        });
        let fee = this._getFee(feeAsset);
        let feeID = this._verifyFee(
            fee,
            current.for_sale,
            sellBalance.getAmount(),
            coreBalance.getAmount()
        );

        if (feeID) {
            this._createLimitOrder(type, feeID);
        } else {
            console.error("Unable to pay fees, aborting limit order creation");
        }
    }

    _cancelLimitOrder(orderID, e) {
        e.preventDefault();
        let {currentAccount} = this.props;
        MarketsActions.cancelLimitOrder(
            currentAccount.get("id"),
            orderID // order id to cancel
        );
    }

    _depthChartClick(base, quote, power, e) {  console.log("_depthChartClick")
        e.preventDefault();
        let {bid, ask} = this.state;

        bid.price = new Price({
            base: this.state.bid.for_sale,
            quote: this.state.bid.to_receive,
            real: e.xAxis[0].value / power
        });
        bid.priceText = bid.price.toReal();

        ask.price = new Price({
            base: this.state.ask.to_receive,
            quote: this.state.ask.for_sale,
            real: e.xAxis[0].value / power
        });
        ask.priceText = ask.price.toReal();
        let newState = {
            bid,
            ask,
            depthLine: bid.price.toReal()
        };

        this._setForSale(bid, true) || this._setReceive(bid, true);
        this._setReceive(ask) || this._setForSale(ask);

        this._setPriceText(bid, true);
        this._setPriceText(ask, false);
        // if (bid.for_sale.)
        this.setState(newState);
    }

    _flipBuySell() {
        this.setState({
            flipBuySell: !this.state.flipBuySell
        });

        SettingsActions.changeViewSetting({
            flipBuySell: !this.state.flipBuySell
        });
    }

    _toggleOpenBuySell() {
        SettingsActions.changeViewSetting({
            buySellOpen: !this.state.buySellOpen
        });

        this.setState({buySellOpen: !this.state.buySellOpen});
    }

    _toggleCharts() {
        SettingsActions.changeViewSetting({
            showDepthChart: !this.state.showDepthChart
        });

        this.setState({showDepthChart: !this.state.showDepthChart});
    }

    _toggleMarketPicker(asset) {
        let showMarketPicker = !!asset;
        this.setState({
            showMarketPicker,
            marketPickerAsset: asset
        });
    }

    _moveOrderBook() {
        SettingsActions.changeViewSetting({
            leftOrderBook: !this.state.leftOrderBook
        });

        this.setState({leftOrderBook: !this.state.leftOrderBook});
    }

    _currentPriceClick(type, price) {  console.log("_currentPriceClick")
        console.log("type, price", type, price, this.state);
        const isBid = type === "bid";
        let current = this.state[type];
        current.price = price[isBid ? "invert" : "clone"]();
        current.priceText = current.price.toReal();
        if (isBid) {
            this._setForSale(current, isBid) ||
            this._setReceive(current, isBid);
        } else {
            this._setReceive(current, isBid) ||
            this._setForSale(current, isBid);
        }
        console.log("current", current)
        this.forceUpdate();

    }

    _orderbookClick(order) { console.log("_orderbookClick", order)
        const isBid = order.isBid();
        /*
        * Because we are using a bid order to construct an ask and vice versa,
        * totalToReceive becomes forSale, and totalForSale becomes toReceive
        */
        let forSale = order.totalToReceive({noCache: true});
        // let toReceive = order.totalForSale({noCache: true});
        let toReceive = forSale.times(order.sellPrice());

        let newPrice = new Price({
            base: isBid ? toReceive : forSale,
            quote: isBid ? forSale : toReceive
        });

        let current = this.state[isBid ? "bid" : "ask"];
        current.price = newPrice;
        current.priceText = newPrice.toReal();

        let newState = {
            // If isBid is true, newState defines a new ask order and vice versa
            [isBid ? "ask" : "bid"]: {
                for_sale: forSale,
                forSaleText: forSale.getAmount({real: true}),
                to_receive: toReceive,
                toReceiveText: toReceive.getAmount({real: true}),
                price: newPrice,
                priceText: newPrice.toReal()
            }
        };

        if (isBid) {
            this._setForSale(current, isBid) ||
            this._setReceive(current, isBid);
        } else {
            this._setReceive(current, isBid) ||
            this._setForSale(current, isBid);
        }
        this.setState(newState);
    }

    _borrowQuote() {
        if(this.refs.borrowQuote) this.refs.borrowQuote.show();
    }

    _borrowBase() {
        if(this.refs.borrowBase) this.refs.borrowBase.show();
    }

    _onSelectIndicators() {
        this.refs.indicators.show();
    }

    _getSettlementInfo() {
        let {lowestCallPrice, feedPrice, quoteAsset} = this.props;

        let showCallLimit = false;
        if (feedPrice) {
            if (feedPrice.inverted) {
                showCallLimit = lowestCallPrice <= feedPrice.toReal();
            } else {
                showCallLimit = lowestCallPrice >= feedPrice.toReal();
            }
        }
        return !!(
            showCallLimit &&
            lowestCallPrice &&
            !quoteAsset.getIn(["bitasset", "is_prediction_market"])
        );
    }

    _changeIndicator(key) {
        let indicators = cloneDeep(this.state.indicators);
        indicators[key] = !indicators[key];
        this.setState({
            indicators
        });

        SettingsActions.changeViewSetting({
            indicators
        });
    }

    _changeIndicatorSetting(key, e) {
        e.preventDefault();
        let indicatorSettings = cloneDeep(this.state.indicatorSettings);
        let value = parseInt(e.target.value, 10);
        if (isNaN(value)) {
            value = 1;
        }
        indicatorSettings[key] = value;

        this.setState({
            indicatorSettings: indicatorSettings
        });

        SettingsActions.changeViewSetting({
            indicatorSettings: indicatorSettings
        });
    }

    onChangeFeeAsset(type, e) {
        e.preventDefault();
        if (type === "buy") {
            this.setState({
                buyFeeAssetIdx: e.target.value
            });

            SettingsActions.changeViewSetting({
                buyFeeAssetIdx: e.target.value
            });
        } else {
            this.setState({
                sellFeeAssetIdx: e.target.value
            });

            SettingsActions.changeViewSetting({
                sellFeeAssetIdx: e.target.value
            });
        }
    }

    onChangeChartHeight({value, increase}) {
        const newHeight = value
            ? value
            : this.state.chartHeight + (increase ? 20 : -20);
        this.setState({
            chartHeight: newHeight
        });

        SettingsActions.changeViewSetting({
            chartHeight: newHeight
        });
    }

    _toggleBuySellPosition() {
        this.setState({
            buySellTop: !this.state.buySellTop
        });

        SettingsActions.changeViewSetting({
            buySellTop: !this.state.buySellTop
        });
    }

    _setReceive(state) {
        if (state.price.isValid() && state.for_sale.hasAmount()) {
            state.to_receive = state.for_sale.times(state.price);
            state.toReceiveText = state.to_receive
                .getAmount({real: true})
                .toString();
            return true;
        }
        return false;
    }

    _setForSale(state) {
        if (state.price.isValid() && state.to_receive.hasAmount()) {
            state.for_sale = state.to_receive.times(state.price, true);
            state.forSaleText = state.for_sale
                .getAmount({real: true})
                .toString();
            return true;
        }
        return false;
    }

    _setPrice(state) {  console.log("_setPrice")
        if (state.for_sale.hasAmount() && state.to_receive.hasAmount()) {
            state.price = new Price({
                base: state.for_sale,
                quote: state.to_receive
            });
            state.priceText = state.price.toReal().toString();
            return true;
        }
        return false;
    }

    _setPriceText(state, isBid) { console.log("_setPriceText")
        const currentBase = state[isBid ? "for_sale" : "to_receive"];
        const currentQuote = state[isBid ? "to_receive" : "for_sale"];
        if (currentBase.hasAmount() && currentQuote.hasAmount()) {
            state.priceText = new Price({
                base: currentBase,
                quote: currentQuote
            })
                .toReal()
                .toString();
        }
    }

    _onInputPrice(type, e) {  console.log("_onInputPrice")
        console.log("type, e", type, e.target.value);
        let current = this.state[type];
        const isBid = type === "bid";
        current.price = new Price({
            base: current[isBid ? "for_sale" : "to_receive"],
            quote: current[isBid ? "to_receive" : "for_sale"],
            real: parseFloat(e.target.value) || 0
        });

        if (isBid) {
            this._setForSale(current, isBid) ||
            this._setReceive(current, isBid);
        } else {
            this._setReceive(current, isBid) ||
            this._setForSale(current, isBid);
        }

        current.priceText = e.target.value;
        this.forceUpdate();
    }

    _onInputSell(type, isBid, e) { console.log("_onInputSell")
        let current = this.state[type];
        // const isBid = type === "bid";
        current.for_sale.setAmount({real: parseFloat(e.target.value) || 0});
        if (current.price.isValid()) {
            this._setReceive(current, isBid);
        } else {
            this._setPrice(current);
        }

        current.forSaleText = e.target.value;
        this._setPriceText(current, type === "bid");

        this.forceUpdate();
    }

    _onInputReceive(type, isBid, e) { console.log("_onInputReceive")
        let current = this.state[type];
        // const isBid = type === "bid";
        current.to_receive.setAmount({real: parseFloat(e.target.value) || 0});

        if (current.price.isValid()) {
            this._setForSale(current, isBid);
        } else {
            this._setPrice(current);
        }

        current.toReceiveText = e.target.value;
        this._setPriceText(current, type === "bid");
        this.forceUpdate();
    }

    isMarketFrozen() {
        let {baseAsset, quoteAsset} = this.props;

        let baseWhiteList = baseAsset
            .getIn(["options", "whitelist_markets"])
            .toJS();
        let quoteWhiteList = quoteAsset
            .getIn(["options", "whitelist_markets"])
            .toJS();
        let baseBlackList = baseAsset
            .getIn(["options", "blacklist_markets"])
            .toJS();
        let quoteBlackList = quoteAsset
            .getIn(["options", "blacklist_markets"])
            .toJS();

        if (
            quoteWhiteList.length &&
            quoteWhiteList.indexOf(baseAsset.get("id")) === -1
        ) {
            return {isFrozen: true, frozenAsset: quoteAsset.get("symbol")};
        }
        if (
            baseWhiteList.length &&
            baseWhiteList.indexOf(quoteAsset.get("id")) === -1
        ) {
            return {isFrozen: true, frozenAsset: baseAsset.get("symbol")};
        }

        if (
            quoteBlackList.length &&
            quoteBlackList.indexOf(baseAsset.get("id")) !== -1
        ) {
            return {isFrozen: true, frozenAsset: quoteAsset.get("symbol")};
        }
        if (
            baseBlackList.length &&
            baseBlackList.indexOf(quoteAsset.get("id")) !== -1
        ) {
            return {isFrozen: true, frozenAsset: baseAsset.get("symbol")};
        }

        return {isFrozen: false};
    }

    _toggleMiniChart() {
        SettingsActions.changeViewSetting({
            miniDepthChart: !this.props.miniDepthChart
        });
    }

    _onGroupOrderLimitChange(e) {
        let groupLimit;
        if (e) {
            e.preventDefault();
            groupLimit = parseInt(e.target.value);
        }
        MarketsActions.changeCurrentGroupLimit(groupLimit);
        if (groupLimit !== this.props.currentGroupOrderLimit) {
            MarketsActions.changeCurrentGroupLimit(groupLimit);
            let currentSub = this.props.sub.split("_");
            MarketsActions.unSubscribeMarket(currentSub[0], currentSub[1]).then(
                () => {
                    this.props.subToMarket(
                        this.props,
                        this.props.bucketSize,
                        groupLimit
                    );
                }
            );
        }
    }

    changeVisibleItemBody = (name) => {
        let { visibleBody } = this.state;
        this.setState({
            visibleBody: Object.assign(visibleBody, {
                [name]: !visibleBody[name]
            })
        });
    };

    render() {
        let {
            currentAccount,
            marketLimitOrders,
            marketCallOrders,
            marketData,
            activeMarketHistory,
            invertedCalls,
            starredMarkets,
            quoteAsset,
            baseAsset,
            lowestCallPrice,
            marketStats,
            marketReady,
            marketSettleOrders,
            bucketSize,
            totals,
            feedPrice,
            buckets,
            coreAsset
        } = this.props;

        const {
            combinedBids,
            combinedAsks,
            lowestAsk,
            highestBid,
        } = marketData;

        let {
            visibleBody,
            bid,
            ask,
            leftOrderBook,
            showDepthChart,
            chartHeight,
            buyDiff,
            sellDiff,
            width,
            showMarketPicker,
            buySellTop
        } = this.state;
        const {isFrozen, frozenAsset} = this.isMarketFrozen();

        let base = null,
            quote = null,
            accountBalance = null,
            quoteBalance = null,
            baseBalance = null,
            coreBalance = null,
            quoteSymbol,
            baseSymbol,
            showCallLimit = false,
            latest,
            changeClass;

        const showVolumeChart = this.props.viewSettings.get(
            "showVolumeChart",
            true
        );


        if (quoteAsset.size && baseAsset.size && currentAccount.size) {
            base = baseAsset;
            quote = quoteAsset;
            baseSymbol = base.get("symbol");
            quoteSymbol = quote.get("symbol");

            accountBalance = currentAccount.get("balances").toJS();

            if (accountBalance) {
                for (let id in accountBalance) {
                    if (id === quote.get("id")) {
                        quoteBalance = accountBalance[id];
                    }
                    if (id === base.get("id")) {
                        baseBalance = accountBalance[id];
                    }
                    if (id === "1.3.0") {
                        coreBalance = accountBalance[id];
                    }
                }
            }

            showCallLimit = this._getSettlementInfo();
        }

        let quoteIsBitAsset = !!quoteAsset.get("bitasset_data_id");
        let baseIsBitAsset = !!baseAsset.get("bitasset_data_id");


        // Latest price
        if (activeMarketHistory.size) {
            let latest_two = activeMarketHistory.take(2);
            latest = latest_two.first();
            let second_latest = latest_two.last();
            changeClass =
                latest.getPrice() === second_latest.getPrice()
                    ? ""
                    : latest.getPrice() - second_latest.getPrice() > 0
                    ? "change-up"
                    : "change-down";
        }

        // Fees
        if (!coreAsset || !Object.keys(this.state.feeStatus).length) {
            return null;
        }

        let {
            sellFeeAsset,
            sellFeeAssets,
            sellFee,
            buyFeeAsset,
            buyFeeAssets,
            buyFee
        } = this._getFeeAssets(quote, base, coreAsset);

        // Decimals
        let hasPrediction =
            base.getIn(["bitasset", "is_prediction_market"]) ||
            quote.getIn(["bitasset", "is_prediction_market"]);


        let smallScreen = false;
        if (width < 1000) {
            smallScreen = true;
            leftOrderBook = false;
        }

        let orderMultiplier = leftOrderBook ? 2 : 1;
        let expirationType = this.state.expirationType;
        let expirationCustomTime = this.state.expirationCustomTime;

        console.log("visibleBody", visibleBody);


        return (
            <div className="exchange-wrapper">
                <div className="exchange-item">
                    <div className="exchange-item-title" onClick={()=>this.changeVisibleItemBody("trading")}>
                        <Translate content="exchange.price_history" />
                        <i className={cnames("exchange-item-arrow", {
                            "arrow-up": !visibleBody.trading,
                            "arrow-dowm": visibleBody.trading})
                        } />
                    </div>
                    <div className={cnames("exchange-item-body", {"hide": visibleBody.trading})}>
                        <ExchangeHeader
                            account={this.props.currentAccount}
                            quoteAsset={quoteAsset}
                            baseAsset={baseAsset}
                            hasPrediction={hasPrediction}
                            starredMarkets={starredMarkets}
                            lowestAsk={lowestAsk}
                            highestBid={highestBid}
                            lowestCallPrice={lowestCallPrice}
                            showCallLimit={showCallLimit}
                            feedPrice={feedPrice}
                            marketReady={marketReady}
                            latestPrice={latest && latest.getPrice()}
                            showDepthChart={showDepthChart}
                            showMarketPicker={showMarketPicker}
                            onSelectIndicators={this._onSelectIndicators.bind(
                                this
                            )}
                            marketStats={marketStats}
                            onToggleCharts={this._toggleCharts.bind(
                                this
                            )}
                            onToggleMarketPicker={this._toggleMarketPicker.bind(
                                this
                            )}
                            showVolumeChart={showVolumeChart}
                        />
                        <TradingViewPriceChart
                            locale={this.props.locale}
                            dataFeed={this.props.dataFeed}
                            priceData={this.props.priceData}
                            baseSymbol={baseSymbol}
                            quoteSymbol={quoteSymbol}
                            leftOrderBook={leftOrderBook}
                            marketReady={marketReady}
                            theme={this.props.settings.get("themes")}
                            buckets={buckets}
                            bucketSize={bucketSize}
                            currentPeriod={this.state.currentPeriod}
                            chartHeight={310}
                            mobile={true}
                        />
                    </div>
                </div>

                <div className="exchange-item">
                    <div className="exchange-item-title" onClick={()=>this.changeVisibleItemBody("orders")}>
                        <Translate content="exchange.book_orders" />
                        <i className={cnames("exchange-item-arrow", {
                            "arrow-up": !visibleBody.orders,
                            "arrow-dowm": visibleBody.orders})
                        } />
                    </div>
                    <div className={cnames("exchange-item-body", {"hide": visibleBody.orders})}>
                        <OrderBook
                            combinedBids={combinedBids}
                            combinedAsks={combinedAsks}
                            base={base}
                            quote={quote}
                            totalBids={totals.bid}
                            totalAsks={totals.ask}
                            baseSymbol={baseSymbol}
                            quoteSymbol={quoteSymbol}
                            currentAccount={this.props.currentAccount.get("id")}
                            onClick={(event)=>this._orderbookClick(event)}
                            flipOrderBook={this.props.viewSettings.get("flipOrderBook")}
                        />
                    </div>
                </div>


                <div className="exchange-item">
                    <div className="exchange-item-title" >
                        <Translate content="exchange.market" onClick={()=>this.changeVisibleItemBody("market")} />


                        <Icon
                            name={"search"}
                            onClick={()=>{
                                ModalActions.show("my_markets_find").then(() => {});
                            }}
                            className={cnames("exchange-item-find-market", {})}  />


                        <Icon
                            name={"filter-setting"}
                            onClick={()=>this.setState({
                                hideMarketSearch: !this.state.hideMarketSearch
                            })}
                            className={cnames("exchange-item-setting", {
                                "hide": visibleBody.market
                            })}  />

                        <i className={cnames("exchange-item-arrow", {
                            "arrow-up": !visibleBody.market,
                            "arrow-dowm": visibleBody.market})
                        } onClick={()=>this.changeVisibleItemBody("market")}
                        />
                    </div>
                    <div className={cnames("exchange-item-body", {"hide": visibleBody.market})}>

                        <MyMarkets
                            className="exchange-market"
                            hideMarketSearch={this.state.hideMarketSearch}
                            showTab={"my-market"}
                            columns={[
                                {name: "star", index: 1},
                                {name: "market", index: 2},
                                {name: "vol", index: 3},
                                {name: "price", index: 4},
                                {name: "change", index: 5}
                            ]}

                            current={`${quoteSymbol}_${baseSymbol}`}
                        />



                        <MobileModal modalId={"my_markets_find"}>
                            <div className="exchange-item-title" >
                                <Translate content={"exchange.more"} component={"div"} className={""} />
                            </div>
                            <MyMarkets
                                className="exchange-market"
                                hideMarketSearch={false}
                                showTab={"find-market"}
                                columns={[
                                    {name: "star", index: 1},
                                    {name: "market", index: 2},
                                    {name: "vol", index: 3},
                                    {name: "price", index: 4},
                                    {name: "change", index: 5}
                                ]}
                                findColumns={[
                                    {name: "market", index: 1},
                                    {name: "issuer", index: 2},
                                    {name: "vol", index: 3},
                                    {name: "add", index: 4}
                                ]}
                                current={`${quoteSymbol}_${baseSymbol}`}
                            />
                        </MobileModal>

                    </div>
                </div>


                <div className="exchange-item">
                    <div className="exchange-item-title" onClick={()=>this.changeVisibleItemBody("openOrders")}>
                        <Translate content="exchange.my_orders" />
                        <i className={cnames("exchange-item-arrow", {
                            "arrow-up": !visibleBody.openOrders,
                            "arrow-dowm": visibleBody.openOrders})
                        } />
                    </div>
                    <div className={cnames("exchange-item-body", {"hide": visibleBody.openOrders})}>
                        {marketLimitOrders.size > 0 && base && quote ? (
                            <MyOpenOrders
                                smallScreen={this.props.smallScreen}
                                className={cnames("open-orders")}
                                key="open_orders"
                                orders={marketLimitOrders}
                                settleOrders={marketSettleOrders}
                                currentAccount={currentAccount}
                                base={base}
                                quote={quote}
                                settings={this.props.settings}
                                baseSymbol={baseSymbol}
                                quoteSymbol={quoteSymbol}
                                activeTab={this.props.viewSettings.get(
                                    "ordersTab"
                                )}
                                onCancel={this._cancelLimitOrder.bind(this)}
                                flipMyOrders={this.props.viewSettings.get(
                                    "flipMyOrders"
                                )}
                                feedPrice={this.props.feedPrice}
                            />
                        ) : null}
                    </div>
                </div>


                <div className="exchange-item">
                    <div className="exchange-item-title" onClick={()=>this.changeVisibleItemBody("my_history")}>
                        <Translate content="exchange.my_history" />
                        <i className={cnames("exchange-item-arrow", {
                            "arrow-up": !visibleBody.my_history,
                            "arrow-dowm": visibleBody.my_history})
                        } />
                    </div>
                    <div className={cnames("exchange-item-body", {"hide": visibleBody.my_history})}>

                        <MarketDeals
                            className={cnames("market-history")}
                            currentAccount={currentAccount}
                            userHistory={currentAccount.get("history")}
                            base={base}
                            quote={quote}
                            baseSymbol={baseSymbol}
                            quoteSymbol={quoteSymbol} />

                    </div>
                </div>

                <div className="exchange-item">
                    <div className="exchange-item-title" onClick={()=>this.changeVisibleItemBody("history")}>
                        <Translate content="exchange.history" />
                        <i className={cnames("exchange-item-arrow", {
                            "arrow-up": !visibleBody.history,
                            "arrow-dowm": visibleBody.history})
                        } />
                    </div>
                    <div className={cnames("exchange-item-body", {"hide": visibleBody.history})}>
                        <MarketDeals
                            className={cnames("market-history")}
                            history={activeMarketHistory}
                            currentAccount={currentAccount}
                            base={base}
                            quote={quote}
                            baseSymbol={baseSymbol}
                            quoteSymbol={quoteSymbol} />

                    </div>
                </div>

                <div className="exchange-item-fixed">
                    <Translate component={"button"} onClick={()=>{
                        ModalActions.show("buy_modal_asset").then(() => {});
                    }} className={"btn btn-green"} asset={getAlias(quote.get("symbol"))} content="exchange.buy_asset" />
                    <Translate component={"button"} onClick={()=>{
                        ModalActions.show("sell_modal_asset").then(() => {});
                    }} className={"btn btn-red"}  asset={getAlias(quote.get("symbol"))} content="exchange.sell_asset"/>

                    <MobileModal modalId={"buy_modal_asset"}>
                        <BuySell
                            onBorrow={baseIsBitAsset ? this._borrowBase.bind(this) : null}
                            currentAccount={currentAccount}
                            backedCoin={this.props.backedCoins.find(
                                a => a.symbol === base.get("symbol")
                            )}
                            currentBridges={
                                this.props.bridgeCoins.get(base.get("symbol")) || null
                            }
                            smallScreen={smallScreen}
                            isOpen={this.state.buySellOpen}
                            onToggleOpen={this._toggleOpenBuySell.bind(this)}
                            className={cnames("buy-sell")}
                            type="bid"
                            expirationType={expirationType["bid"]}
                            expirations={this.EXPIRATIONS}
                            expirationCustomTime={expirationCustomTime["bid"]}
                            onExpirationTypeChange={this._handleExpirationChange.bind(
                                this,
                                "bid"
                            )}
                            onExpirationCustomChange={this._handleCustomExpirationChange.bind(
                                this,
                                "bid"
                            )}
                            amount={bid.toReceiveText}
                            price={bid.priceText}
                            total={bid.forSaleText}
                            quote={quote}
                            base={base}
                            amountChange={(event)=>this._onInputReceive("bid", true, event)}
                            priceChange={(event)=>this._onInputPrice("bid", event)}
                            setPrice={this._currentPriceClick}
                            totalChange={(event)=>this._onInputSell("bid", false, event)}
                            balance={baseBalance}
                            balanceId={base.get("id")}
                            onSubmit={(event)=>this._createLimitOrderConfirm(
                                quote,
                                base,
                                baseBalance,
                                coreBalance,
                                buyFeeAsset,
                                "buy",
                                event
                            )}
                            balancePrecision={base.get("precision")}
                            quotePrecision={quote.get("precision")}
                            totalPrecision={base.get("precision")}
                            currentPrice={lowestAsk.getPrice()}
                            currentPriceObject={lowestAsk}
                            account={currentAccount.get("name")}
                            fee={buyFee}
                            hasFeeBalance={this.state.feeStatus[buyFee.asset_id].hasBalance}
                            feeAssets={buyFeeAssets}
                            feeAsset={buyFeeAsset}
                            onChangeFeeAsset={this.onChangeFeeAsset.bind(this, "buy")}
                            isPredictionMarket={base.getIn([
                                "bitasset",
                                "is_prediction_market"
                            ])}
                            onFlip={
                                this.state._flipBuySell
                                    ? null
                                    : this._flipBuySell.bind(this)
                            }
                            onTogglePosition={
                                !this.state._toggleBuySellPosition
                                    ? this._toggleBuySellPosition.bind(this)
                                    : null
                            }
                        />
                    </MobileModal>
                    <MobileModal modalId={"sell_modal_asset"}>
                        <BuySell
                            onBorrow={quoteIsBitAsset ? this._borrowQuote.bind(this) : null}
                            currentAccount={currentAccount}
                            backedCoin={this.props.backedCoins.find(
                                a => a.symbol === quote.get("symbol")
                            )}
                            currentBridges={
                                this.props.bridgeCoins.get(quote.get("symbol")) || null
                            }
                            smallScreen={smallScreen}
                            isOpen={this.state.buySellOpen}
                            onToggleOpen={this._toggleOpenBuySell.bind(this)}
                            className={cnames("buy-sell")}
                            type="ask"
                            amount={ask.forSaleText}
                            price={ask.priceText}
                            total={ask.toReceiveText}
                            quote={quote}
                            base={base}
                            expirationType={expirationType["ask"]}
                            expirations={this.EXPIRATIONS}
                            expirationCustomTime={expirationCustomTime["ask"]}
                            onExpirationTypeChange={()=>this._handleExpirationChange("ask")}
                            onExpirationCustomChange={()=>this._handleCustomExpirationChange("ask")}
                            amountChange={(event)=>this._onInputSell("ask", false, event)}
                            priceChange={(event)=>this._onInputPrice("ask", event)}
                            setPrice={this._currentPriceClick}
                            totalChange={(event)=>this._onInputReceive("ask", true, event)}
                            balance={quoteBalance}
                            balanceId={quote.get("id")}
                            onSubmit={(event)=>this._createLimitOrderConfirm(
                                base,
                                quote,
                                quoteBalance,
                                coreBalance,
                                sellFeeAsset,
                                "sell",
                                event
                            )}
                            balancePrecision={quote.get("precision")}
                            quotePrecision={quote.get("precision")}
                            totalPrecision={base.get("precision")}
                            currentPrice={highestBid.getPrice()}
                            currentPriceObject={highestBid}
                            account={currentAccount.get("name")}
                            fee={sellFee}
                            hasFeeBalance={
                                this.state.feeStatus[sellFee.asset_id].hasBalance
                            }
                            feeAssets={sellFeeAssets}
                            feeAsset={sellFeeAsset}
                            onChangeFeeAsset={()=>this.onChangeFeeAsset("sell")}
                            isPredictionMarket={quote.getIn([
                                "bitasset",
                                "is_prediction_market"
                            ])}
                            onFlip={
                                !this.state._flipBuySell
                                    ? this._flipBuySell.bind(this)
                                    : null
                            }
                            onTogglePosition={
                                !this.state._toggleBuySellPosition
                                    ? this._toggleBuySellPosition.bind(this)
                                    : null
                            }
                        />
                    </MobileModal>
                </div>
            </div>
        );
    }
}

const FrozenInfo = (props) => {
    return (
        <Translate
            content="exchange.market_frozen"
            component="p"
            {...props}
        />
    );
};

export default Exchange;

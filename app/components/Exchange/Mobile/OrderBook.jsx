import React from "react";
import PropTypes from "prop-types";
import utils from "common/utils";
import Translate from "react-translate-component";
import classnames from "classnames";
import PriceText from "Utility/PriceText";
import TransitionWrapper from "Utility/TransitionWrapper";
import AssetName from "Utility/AssetName";
import {getAlias} from "config/alias";

class OrderBookRowHorizontal extends React.Component {


    render() {
        let {order, quote, base, position, totalValue} = this.props;
        const isBid = order.isBid();
        const isCall = order.isCall();



        let price = (
            <PriceText price={order.getPrice()} quote={quote} base={base} />
        );
        let orderReal = isBid ? order.totalForSale().getAmount({real: true}) : order.totalToReceive().getAmount({real: true});
        let amount = isBid
            ? utils.format_number(
                order.amountToReceive().getAmount({real: true}),
                quote.get("precision")
            ) : utils.format_number(
                order.amountForSale().getAmount({real: true}),
                quote.get("precision")
            );
        let value = isBid
            ? utils.format_number(
                order.amountForSale().getAmount({real: true}),
                base.get("precision")
            )
            : utils.format_number(
                order.amountToReceive().getAmount({real: true}),
                base.get("precision")
            );
        let total = isBid ? utils.format_number(
            orderReal, base.get("precision")
        ) : utils.format_number(
            orderReal,
            base.get("precision")
        );
        let percentValue = orderReal * 100 / totalValue;
        let opacityValue = Math.ceil(percentValue)/100;
        opacityValue = opacityValue > 0.6 ? 0.6 : opacityValue;
        console.log("opacityValue", opacityValue);
        let bgColor = isBid ? "rgba(99, 218, 50, 0.3": "rgba(255, 39, 65, 0.2)";

        let styleBg = {
            width: percentValue > 100 ? 100 : percentValue + "%",
            opacity: 1 - opacityValue ,
            position: "absolute",
            height: "100%",
            background: bgColor,
            top: 0
        };
        styleBg[isBid ? "right": "left"] = 0;
        let propsRow = {
            onClick: this.props.onClick,
            className: classnames("order-book-body-row", {
                "my-order": order.isMine(this.props.currentAccount)
            })
        };
        
        if( position === "left" ) {
            return (
                <div {...propsRow}>
                    <div className={"order-book-body-row-wrap"}>
                        <div className={"order-book-body-item asset-quote"}>{amount}</div>
                        <div className={"order-book-body-item price"}>
                            {price}
                        </div>
                    </div>
                    <div className={"order-book-body-item"} style={styleBg} />
                </div>
            );
        } else {
            return (
                <div {...propsRow}>
                    <div className={"order-book-body-row-wrap"}>
                        <div className={"order-book-body-item asset-quote"}>{amount}</div>
                        <div className={"order-book-body-item price"}>
                            {price}
                        </div>
                    </div>
                    <div className={"order-book-body-item"} style={styleBg} />
                </div>
            );
        }
    }
}

class OrderBook extends React.Component {
    constructor(props) {
        super();
        this.state = {
            flip: props.flipOrderBook,
        };
    }

    render() {
        let {
            combinedBids,
            combinedAsks,
            quote,
            base,
            totalAsks,
            totalBids,
            quoteSymbol,
            baseSymbol,
            horizontal
        } = this.props;

        let bidRows = null,
            askRows = null;
        if (base && quote) {
            bidRows = combinedBids.map((order, index) => {
                return (
                    <OrderBookRowHorizontal
                        index={index}
                        key={order.getPrice() + (order.isCall() ? "_call" : "")}
                        order={order}
                        onClick={()=>this.props.onClick(order)}
                        base={base}
                        quote={quote}
                        totalValue={totalBids}
                        position={!this.state.flip ? "left" : "right"}
                        currentAccount={this.props.currentAccount}
                    />
                );
            });

            //let tempAsks = combinedAsks;
            /*if (!horizontal) {
                tempAsks.sort((a, b) => {
                    return b.getPrice() - a.getPrice();
                });
            }*/


            askRows = combinedAsks.map((order, index) => {
                return (
                    <OrderBookRowHorizontal
                        index={index}
                        key={order.getPrice() + (order.isCall() ? "_call" : "")}
                        order={order}
                        onClick={this.props.onClick.bind(this, order)}
                        base={base}
                        quote={quote}
                        totalValue={totalAsks}
                        type={order.type}
                        position={!this.state.flip ? "right" : "left"}
                        currentAccount={this.props.currentAccount}
                    />
                );
            });
        }

        return (
            <div className={classnames("order-book-wrap-item")}>
                <OrderItem
                    key={"order-item-bids"}
                    type={"bids"}
                    headerTitle={"exchange.bids"}
                    headerSubTitle={"exchange.total"}
                    totalValue={utils.format_number(
                        totalBids,
                        base.get("precision")
                    )}
                    base={base}
                    quote={quote}
                    flip={!this.state.flip}
                    baseSymbol={baseSymbol}
                    quoteSymbol={quoteSymbol}
                    bodyRows={bidRows}
                    onToggleShowAll={()=>this._onToggleShowAll("bids")}
                />
                <OrderItem
                    key={"order-item-asks"}
                    type={"asks"}
                    headerTitle={"exchange.asks"}
                    headerSubTitle={"exchange.total"}
                    totalValue={utils.format_number(
                        totalAsks,
                        quote.get("precision")
                    )}
                    flip={this.state.flip}
                    base={base}
                    quote={quote}
                    baseSymbol={baseSymbol}
                    quoteSymbol={quoteSymbol}
                    bodyRows={askRows}
                    onToggleShowAll={()=>this._onToggleShowAll("asks")}
                />
            </div>
        );
    }
}

class OrderItem extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            rowCount: 20,
        };
    }

    render() {
        const { type, baseSymbol, quoteSymbol, bodyRows, flip } = this.props;

        return (
            <div className={"order-book-item"}>


                <OrderHeader  key={"order-header-" + type} type={type} baseSymbol={baseSymbol} quoteSymbol={quoteSymbol} reverse={!flip}/>

                <TransitionWrapper
                    component="div"
                    className={classnames("order-book-body", {
                        "for-buy": flip,
                        "for-sell": !flip,
                    })}
                    transitionName="newrow">
                    {bodyRows}
                </TransitionWrapper>

            </div>
        );
    }
}
class OrderHeader extends React.PureComponent {
    render () {
        const {quoteSymbol, reverse, type} = this.props;
        let groupHead = [];

        groupHead.push(
            <div key={ "order-book-header-item-asset-quote-" + type} className="order-book-header-item asset-quote">
                <span className="header-sub-title">
                    <AssetName dataPlace="top" name={quoteSymbol} />
                </span>
            </div>
        );
        groupHead.push(
            <div key={ "order-book-header-item-price-" + type} className="order-book-header-item price">
                <Translate className="header-sub-title" content="exchange.price"/>
            </div>
        );

        // if( reverse ) {
        //     groupHead.reverse();
        // }


        return (
            <div key={ "order-book-header" + type} className={classnames("order-book-header", {
                "for-buy": !reverse,
                "for-sell": reverse,
            })}>
                <div className="order-book-header-row">
                    {groupHead}
                </div>
            </div>
        );
    }
}

OrderBook.defaultProps = {
    bids: [],
    asks: []
};

OrderBook.propTypes = {
    bids: PropTypes.array.isRequired,
    asks: PropTypes.array.isRequired,
};

export default OrderBook;

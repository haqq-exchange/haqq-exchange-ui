import React from "react";
import {Select} from "antd";
import {getAlias} from "config/alias";

const RenderSelect = props => {
    let elementId = props.elementId || "Credit";
    return  (props?.options ?
            <Select
                getPopupContainer={()=>document.getElementById(elementId)}
                onChange={props.onChange}
                defaultValue={props?.value} >
                {props?.options.map(currency=>{
                    return <Select.Option key={'option-' + currency} value={currency}>
                        {getAlias(currency)}
                    </Select.Option>
                })}
            </Select> : null
    )
};

export default RenderSelect;

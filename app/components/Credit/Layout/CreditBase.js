import React from "react";
import CreditActions from "actions/CreditActions";
import {FetchChain} from "deexjs";

export default function CreditBase(WrappedComponent, config) {
    return class extends React.Component {
        static propTypes = {
           /* memo: PropTypes.string, // asset id
            from_account: PropTypes.string.isRequired, // asset id
            to_account: PropTypes.string, // asset id
            asset:  PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.number
            ]),
            amount: PropTypes.number*/
        };
        static defaultProps = {};

        constructor(props) {
            super(props);
        }


        componentDidMount() {
            if( config?.loading ) {
                this.getActiveTerms()
            }
        }

        componentDidUpdate(prevProps) {}

        componentWillUnmount() {
            CreditActions
                .removeParamsCredit([
                    "loan_asset",
                    "collateral_asset"
                ]);
        }

        getActiveTerms = (loan) => {
            const {Api} = this.props;
            let loan_currency, collateral_currency;
            if( loan ) {
                loan_currency = loan.loan_currency;
                collateral_currency = loan.collateral_currency;
            }

            return new Promise(resolve=>{
                Api.postTemplate("/loans/get_active_terms", {})
                    .then(({content})=>{
                        let setData = {
                            active_terms: content.active_terms[0]
                        };
                        this.setState(setData, () => {
                            this.getRate({
                                loan: loan_currency,
                                collateral: collateral_currency
                            });
                            CreditActions
                                .setDataCredit(setData)
                                .then(resolve);
                        });
                    });
            })
        };

        getRate = ({loan, collateral}) => {
            const {Api} = this.props;
            const {active_terms} = this.state;

            const loan_currency = loan ? loan : Object.keys(active_terms?.loan_currencies)[0];
            const collateral_currency = collateral ? collateral : active_terms?.collateral_currencies?.[0];

            return new Promise(resolve=>{
                FetchChain("getAsset", [loan_currency, collateral_currency]).then(assets=>{
                    const [loan_asset, collateral_asset] = assets;
                    Api.postTemplate("/loans/get_rate", {
                        loan_currency,
                        collateral_currency
                    })
                        .then(({content})=>{
                            let setData = {
                                exchange_rate: content.exchange_rate,
                                loan_asset,
                                collateral_asset
                            };
                            console.log("setData", setData);
                            CreditActions.setDataCredit(setData);
                            this.setState(setData, resolve)
                        });
                });
            })

        };

        render() {

            const props = Object.assign({}, this.props, this.state, {
                getActiveTerms: (loan)=>this.getActiveTerms(loan),
                getRate: (loan, collateral)=>this.getRate(loan, collateral),
            });

            // ... и рендерит оборачиваемый компонент со свежими данными!
            // Обратите внимание, что мы передаём остальные пропсы
            return <WrappedComponent {...props} />;
        }

    };
}

import Translate from "react-translate-component";
import translator from "counterpart";
import React from "react";
import {cn} from "@bem-react/classname";
import BindToChainState from "Utility/BindToChainState";
import ChainTypes from "Utility/ChainTypes";
import Utils from "common/utils";



class ViewBlockInfo extends React.Component{
    static propTypes = {
        loan_currency: ChainTypes.ChainAsset,
        collateral_currency: ChainTypes.ChainAsset,
    };

    render() {
        const {cln, loan_currency, collateral_currency, isApp } = this.props;
        const clnItem = cn(cln || "BlockCredit");
        const statusName = isApp ? "credit.my.app_state" : "credit.my.credit_state"
        console.log("ViewBlockInfo this.props", this.props);
        return (
            <div className={clnItem("oferta-list")}>
                {this.props.loan_amount ? <div className={clnItem("oferta-item")}>
                    <Translate content={"credit.calculator.form.sum_credit"} />
                    <span>{Utils.asset_amount(loan_currency, {real: this.props.loan_amount})}</span>
                </div> : null}
                {this.props.loan_term ? <div className={clnItem("oferta-item")}>
                    <Translate content={"credit.calculator.form.loan_period"} with={{
                        name: translator(`credit.status.repayment_interval.${this.props?.repayment_interval?.name}`)
                    }}/>
                    <span>{this.props.loan_term}</span>
                </div> : null}
                {this.props.collateral_amount || this.props.required_collateral_amount ? <div className={clnItem("oferta-item")}>
                    <Translate content={"credit.calculator.form.sum_zalog"}/>
                    <span>{Utils.asset_amount(collateral_currency, {real: this.props.required_collateral_amount || this.props.collateral_amount})}</span>
                </div> : null}

                {this.props.loan_repayment_type ? <div className={clnItem("oferta-item")}>
                    <Translate content={"credit.calculator.form.frequency_repayment"}/>
                    <Translate content={`credit.status.loan_repayment_type.${this.props.loan_repayment_type.name || this.props.loan_repayment_type}`}/>
                </div> : null}

                {this.props.daily_interest_rate ? <div className={clnItem("oferta-item")}>
                    <Translate content={"credit.calculator.form.interest_rate"}/>
                    <span>{this.props.daily_interest_rate}</span>
                </div> : null}
                {this.props.security_coefficient ? <div className={clnItem("oferta-item")}>
                    <Translate content={"credit.calculator.form.security_coefficient"}/>
                    <span>{this.props.security_coefficient}</span>
                </div> : null}
                {!this.props.security_coefficient && this.props.current_security_ratio_value ? <div className={clnItem("oferta-item")}>
                    <Translate content={"credit.calculator.form.security_coefficient"}/>
                    <span>{this.props.current_security_ratio_value}</span>
                </div> : null}

                {this.props?.state?.name ? <Translate
                    content={statusName}
                    component={"div"}
                    unsafe
                    className={clnItem("oferta-item")}
                    with={{
                        status: translator(`credit.status.${this.props?.state?.name}`, {})
                    }}
                />: null}

                {this.props.current_security_ratio ? <div className={clnItem("oferta-item")}>
                    <Translate
                        unsafe
                        content={"credit.my.margin_call"}
                        className={clnItem("oferta-item")}
                        component={"div"}
                        with={{
                            status: translator(`credit.my.status.${this.props?.current_security_ratio?.name}`, {})
                        }}/>
                </div> : null}


            </div>
        )
    }
}

export default BindToChainState(ViewBlockInfo);

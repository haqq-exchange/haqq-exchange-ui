import * as React from "react";
import {Route, Switch} from "react-router-dom"
import {connect} from "alt-react";
import SettingsStore from "../../stores/SettingsStore";
// @ts-ignore
import {CreditCalculator, MyCredit, ViewCredit, Page404} from "../../RoutesLink";
import accountUtils from "../../lib/common/account_utils";
import RefUtils from "../../lib/common/ref_utils";
// import {getRequestAddress, getLoadedLocales} from "api/apiConfig";
import counterpart from "counterpart";
// import LoadingIndicator from "Components/LoadingIndicator";

import "./style.scss";


export interface Props {
    history: object,
    location: object,
    match: object
}

export default class CreditPages extends React.Component<Props, {}>{

    static defaultProps: Object = {
        getUserSign: accountUtils.getUserSign,
        Api: new RefUtils({
           // url: getRequestAddress("api")
        })
    };
    private __DEV__: any;

    constructor(props) {
        super(props);
        this.state = {
            localesLoaded: false
        }
    }

    componentDidMount() {
        this.getLoadedLocales();
    }


    getLoadedLocales = () => {
        let trans = counterpart.translate("credit");
        let staticPath;
        if (this.__DEV__) {
            staticPath = "http://localhost:8081";
        }
        if( typeof trans !== "object" ) {
            getLoadedLocales("credit.json", undefined, staticPath)
                .then(this.setLoadedLocales);
        } else {
            this.setLoadedLocales();
        }
    };

    setLoadedLocales = () => {
        this.setState({localesLoaded: true});
        // this.hasExchanger(this.getRefInfo);
        // this.getAssets();
    };

    render() : String {
        console.log("Props", this.props);
        return (""
           /* <Switch>
                <Route path="/credit" exact component={Page404}/>
            </Switch>*/
        )
    }
}

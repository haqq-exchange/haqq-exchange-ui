import React from "react";
import Translate from "react-translate-component";

import {Col, Checkbox, Row, Typography, Spin} from "antd";
import {Redirect} from "react-router-dom";
import WrapperAuth from "Utility/WrapperAuth";
import ViewBlockInfo from "../Layout/ViewBlockInfo"
import Utils from "common/utils"
import ReactMarkdown from "markdown-react-js";

import {getLoadedLocales} from "api/apiConfig";
import {cn} from "@bem-react/classname";
import {getAlias} from "../../../config/alias";
import debounce from "lodash-es/debounce";
import BindToChainState from "Utility/BindToChainState";
import counterpart from "counterpart";
const {Paragraph} = Typography;
const cln = cn("Credit");

class CreditStepTwo extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            disableBtn: true,
            loading: false,
            redirect: false,
            description: null,
            payments: []
        };

        //this.makeNewLoan = debounce(this.makeNewLoan, 300)
    }

    componentDidMount() {
        this.getDescription();
        this.getCalculatePayments()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.locale !== this.props.locale) {
            this.getDescription();
        }
    }

    getCalculatePayments = event => {
        const {Api, credit} = this.props;
        const {daily_interest_rate, loan_amount, loan_term, loan_repayment_type, repayment_interval} = credit;

        Api.postTemplate("/loans/calculate_payments", {
            daily_interest_rate,
            loan_amount,
            loan_term,
            loan_repayment_type,
            repayment_interval
        })
            .then(({content})=>{
                console.log("content", content);
                if( !content.error ) {
                    this.setState(content)
                }
            });
    };

    makeNewLoan = event => {
        const {getUserSign, Api, credit} = this.props;
        //event.defaultPrevented();
        //console.log("makeNewLoan", this.props);
        this.setState({
            loading: true,
            disableBtn: true
        });
        getUserSign(credit).then(sign => {
            Api.postTemplate("/loans/make_new_loan", credit, sign)
                .then(({content})=>{
                    console.log("content", content);
                    if( !content.error ) {
                        this.setState({
                            redirect: true,
                            loading: false,
                            disableBtn: false,
                            loans: content.loans
                        })
                    }
                }).catch(error=>{
                    this.setState({
                        loading: false ,
                        error: true
                    });
                    console.log("error", error);
                });
        }).catch(error=>{
            this.setState({
                loading: false ,
                error: true
            });
            console.log("error", error);
        });
    };

    onchangeAccess = event => {
        this.setState({
            disableBtn:  !event.target.checked
        })
    };

    getDescription = () => {
        const {locale} = this.props;

        console.log("getDescription", this.props );
        getLoadedLocales(`credit-description-${locale}.md`).then(result=>{
            console.log("result", result);
            this.setState({
                description: result
            })
        }).catch(error=>{
            console.log("error", error);
        });
    };

    render() {
        const {disableBtn, redirect, loans, payments, description} = this.state;
        const {loan_asset} = this.props;
        const {credit, active_terms} = this.props;
        // console.log("CreditStepTwo", this.props);
        // console.log("CreditStepTwo loan_asset", loan_asset);
        if (redirect) {
            return (
                <Redirect to={`/credit/app/${loans.id}`} from={`/credit`}/>
            );
        }
        //console.log(counterpart.translate("credit.calculator.oferta.description", {}))
        return (
            <div className={cln("oferta")}>
                <Translate content={"credit.calculator.oferta.title"} className={cln("oferta-title")} component={"div"}  />
                <div className={cln("oferta-info")}>
                    <Row gutter={8}>
                        <Col xs={{ span: 23, offset: 0 }} sm={{ span: 8, offset: 8 }} > {/* span={8} offset={8} */}
                            <div className={cln("oferta-wrap")}>
                                <ViewBlockInfo
                                    {...credit}
                                    loan_repayment_type={active_terms.loan_repayment_type}
                                    repayment_interval={active_terms.repayment_interval}

                                    cln={"Credit"}/>
                            </div>
                            {payments.length ? <div className={cln("oferta-wrap")}>
                                <Translate content={"credit.my.graphic_payment"} component={Typography.Title} level={4} />
                                <br/>
                                {payments.map((payment, index)=>{
                                    return <Paragraph key={`Paragraph-${index}`}>
                                        <Translate unsafe content={"credit.my.next_payment"}
                                                   component={"div"} with={{
                                            sum: Utils.asset_amount(loan_asset, {real: payment.amount}),
                                            date: Utils.format_date(payment.date)
                                        }} />
                                    </Paragraph>
                                })}
                            </div>: null}
                        </Col>

                        <Col span={24} className={cln("oferta-description-wrap")}>
                            <Translate content={"credit.calculator.oferta.description_title"} className={cln("oferta-description-title")} component={"div"}  />
                            {description ? <ReactMarkdown text={description} /> : null}
                        </Col>
                        <Col span={24} className={cln("oferta-description-checkbox")}>
                            <Checkbox name={"access"} onChange={value=>this.onchangeAccess(value)}>
                                <Translate content={"credit.calculator.oferta.access"} />
                            </Checkbox>
                        </Col>
                        {/* span={6} offset={9} */}
                        <Col xs={{ span: 23, offset: 0 }} sm={{ span: 6, offset: 9 }}  className={cln("oferta-description-checkbox")}>
                            <Row type={"flex"} align={"middle"} justify={"center"} style={{marginTop: 10}}>
                                <Translate
                                    component={"button"}
                                    className={"btn btn-gray"} type={"button"} onClick={()=>this.props.changeStep(1)}
                                    content={"credit.calculator.form.reset_btn"} />

                                <Spin spinning={this.state.loading} >
                                    <WrapperAuth
                                        disabled={disableBtn}
                                        component={"button"}
                                        className={"btn btn-green"} type={"button"}
                                        onClick={this.makeNewLoan}
                                    >
                                        <Translate content={"credit.calculator.form.submit_btn"} />
                                    </WrapperAuth>
                                </Spin>
                            </Row>
                            <div className={"btn-group"}>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}
export default CreditStepTwo;
// export default BindToChainState(CreditStepTwo);

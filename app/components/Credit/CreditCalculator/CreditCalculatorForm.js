import React from "react";
import Translate from "react-translate-component";
import {Col, Form, Input, Row, Select, Typography, InputNumber, Spin} from "antd";
import counterpart from "counterpart";
import {Asset} from "common/MarketClasses";
import RenderSelect from "../Layout/RenderSelect";
import {getAlias} from "../../../config/alias";
const translator = require('counterpart');
const { Text, Paragraph } = Typography;

class CreditCalculatorForm extends React.Component {

    static defaultProps = {
        loan_period: [...Array(12).keys()]
    };

    state = {
        loading: false,
        collateral: null,
        loan: null,
    };

    componentDidMount() {
        // this.props.form.validateFields();
    }

    getShowError = (name) => {
        const {isFieldTouched, getFieldError}= this.props.form;
        const hasValueError = isFieldTouched(name) && getFieldError(name);
        return {
            validateStatus: hasValueError ? "error" : "",
            help: hasValueError || ""
        };
    };

    calculateAmount = (value, type) => {
        const {exchange_rate, form, active_terms , loan_asset, collateral_asset} = this.props;

        let loanAsset = new Asset({
                asset_id: loan_asset.get("id"),
                precision: loan_asset.get("precision"),
            }),
            collateralAsset = new Asset({
                asset_id: collateral_asset.get("id"),
                precision: collateral_asset.get("precision"),
            });

        if( type === "loan_amount" ) {
            loanAsset.setAmount({real: value});
            collateralAsset.setAmount({real: (value / exchange_rate) * active_terms.min_security_ratio})
        } else if (type === "collateral_amount") {
            collateralAsset.setAmount({real: value});
            loanAsset.setAmount({real: (value * exchange_rate) / active_terms.min_security_ratio })
        }

        form.setFieldsValue({
            loan_amount: loanAsset.hasAmount() ? loanAsset.getAmount({real: true}) : 0,
            collateral_amount: collateralAsset.hasAmount() ? collateralAsset.getAmount({real: true}) : 0,
        });
    };

    changeAsset = (value, type) => {
        const {form} = this.props;
        this.setState({loading: true})
        let reType = type === "collateral_amount" ? "loan_amount" : "collateral_amount";
        const getValueRate = type => type === "collateral_amount" ? "collateral" : "loan";
        let value1 = this.getSelectedValue(reType);
        let sendRate = {
            [getValueRate(type)]: value,
            [getValueRate(reType)]: value1,
        };
        this.props.getRate(sendRate).then(()=>{
            const fieldValue1 = form.getFieldValue(reType);
            this.calculateAmount(parseFloat(fieldValue1) || 0, reType);
            this.setState({loading: false})
        })
    };

    submitForm = (event) => {
        const {account, history} = this.props;
        event.preventDefault();
        console.log("this.props", this.props)
        this.props.form.validateFields((err, values) => {
            if (!err) {
                if( account ) {
                    this.props.changeStep(2, values);
                } else {
                    history.push("/authorization")
                }
            }
        });
    };

    getSelectedValue = type => {
        const {collateral_asset, active_terms, loan_asset} = this.props;

        const loan_currencies_keys = active_terms?.loan_currencies ? Object.keys(active_terms?.loan_currencies) : null;

        let loan_currencies = loan_asset && loan_asset?.size ? loan_asset.get("symbol") : loan_asset?.symbol;
        if( !loan_currencies )
            loan_currencies = loan_currencies_keys ? loan_currencies_keys?.[0] : null;
        let collateral_currencies = collateral_asset && collateral_asset?.size
            ? collateral_asset.get("symbol") : collateral_asset?.symbol
                ? collateral_asset?.symbol  : active_terms?.collateral_currencies?.[0];
        const selectedValue = {
            "loan_amount": loan_currencies,
            "collateral_amount": collateral_currencies
        };
        return type ? selectedValue?.[type] : selectedValue;
    };

    getRules = type => {
        const {active_terms} = this.props;
        const currencies = this.getSelectedValue(type);
        const valueRules = active_terms?.loan_currencies?.[currencies];
        let rulesItem = {
        };
        if( valueRules ) {
            for(let key in valueRules) {
                if( valueRules.hasOwnProperty(key) && valueRules[key] ) {
                    let propName = key.split("_").shift();
                    rulesItem = Object.assign(rulesItem, {
                        [propName]: valueRules[key]
                    });
                }
            }
        }
        return rulesItem;
    };

    validator = (rule, value, callback) => {
        let valueRules = this.getRules("loan_amount");
        if( value < valueRules?.min || value > valueRules?.max) {
            callback(counterpart.translate("credit.error.min_max", valueRules));
        }
        callback();
    };

    render() {
        const {form, account} = this.props;
        const {active_terms, loan_asset} = this.props;
        const {getFieldDecorator, getFieldsError} = form;
        const loan_currencies_keys = active_terms?.loan_currencies ? Object.keys(active_terms?.loan_currencies) : null;
        const loan_currencies = this.getSelectedValue("loan_amount");
        const collateral_currencies = this.getSelectedValue("collateral_amount");
        let valueRules = this.getRules("loan_amount");
        let errors = Object.values(getFieldsError(["loan_amount", "collateral_amount", "loan_term"])).filter(a=>a);


        return (
            <Spin spinning={this.state.loading}>
                <Form onSubmit={this.submitForm} noValidate >
                    {getFieldDecorator("loan_terms", {initialValue: active_terms?.id})(<Input type={"hidden"}/>)}
                    {getFieldDecorator("repayment_interval", {initialValue: active_terms?.repayment_interval?.id})(<Input type={"hidden"}/>)}
                    {getFieldDecorator("loan_repayment_type", {initialValue: active_terms?.loan_repayment_type?.id})(<Input type={"hidden"}/>)}
                    {getFieldDecorator("daily_interest_rate", {initialValue: active_terms?.daily_interest_rate.toString()})(<Input type={"hidden"}/>)}
                    {/* loan_currency */}
                    {getFieldDecorator("loan_currency", {initialValue: loan_currencies})(<Input type={"hidden"}/>)}
                    {/* collateral_currency */}
                    {getFieldDecorator("collateral_currency", {initialValue: collateral_currencies})(<Input type={"hidden"}/>)}
                    {account && account.has("name") ? getFieldDecorator("account", {initialValue: account.get("name")})(<Input type={"hidden"}/>) : null}

                    <Row >
                        {/*span={8} offset={4}*/}
                        <Col xs={{ span: 23, offset: 0 }} sm={{ span: 8, offset: 4 }} >
                            <Row>
                                <Col>
                                    <Form.Item
                                        label={counterpart.translate("credit.calculator.form.sum_credit")}>
                                        {getFieldDecorator(`loan_amount`, {
                                            rules: [
                                                {
                                                    required: true,
                                                    message: "required"
                                                }, {
                                                    validator: this.validator
                                                }
                                            ],
                                        })(<Input
                                            readOnly={!loan_currencies_keys?.length}
                                            onChange={(event)=>this.calculateAmount(parseFloat(event.target.value), "loan_amount")}
                                            addonAfter={<RenderSelect
                                                value={loan_currencies}
                                                onChange={value=>this.changeAsset(value,  "loan_amount")}
                                                options={loan_currencies_keys} />} /> ) }
                                    </Form.Item>
                                </Col>

                                <Col>
                                    <Form.Item label={counterpart.translate(
                                        "credit.calculator.form.sum_zalog"
                                    )}>
                                        {getFieldDecorator(`collateral_amount`, {
                                            rules: [
                                                {
                                                    required: true
                                                }/*,{
                                                    validator: this.validator
                                                }*/
                                            ],
                                        })(<Input
                                            readOnly={!active_terms?.collateral_currencies?.length}
                                            onChange={(event)=>this.calculateAmount(parseFloat(event.target.value), "collateral_amount")}
                                            addonAfter={<RenderSelect
                                                value={collateral_currencies}
                                                onChange={value=>this.changeAsset(value, "collateral_amount")}
                                                options={active_terms?.collateral_currencies} />}/>)}
                                    </Form.Item>
                                </Col>

                                <Col>
                                    <Form.Item label={counterpart.translate(
                                        "credit.calculator.form.loan_period", {
                                            name: translator(`credit.status.repayment_interval.${active_terms?.repayment_interval?.name}`)
                                        }
                                    )}>
                                        {getFieldDecorator(`loan_term`, {
                                            initialValue: active_terms?.loan_term.max || 0,
                                            normalize: value => value && value.toString(),
                                            rules: [
                                                {
                                                    required: true,
                                                    message: 'Please input your loan period',
                                                },
                                            ],
                                        })(<InputNumber
                                            min={active_terms?.loan_term.min}
                                            max={active_terms?.loan_term.max} />)}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row style={{"textAlign": "center"}}>
                                <Translate
                                    content={"credit.calculator.steps.step_1_btn"}
                                    disabled={errors.length}
                                    type={"submit"} className={"btn btn-green"}  component={"button"}  />
                            </Row>
                        </Col>
                        {/*span={10} offset={2}*/}
                        <Col xs={{ span: 23, offset: 0 }} sm={{ span: 10, offset: 2 }} style={{padding: "20px 0 0 0"}}>
                            <Paragraph>
                                <Translate content={"credit.calculator.form.interest_rate"}  />&nbsp;
                                 - {active_terms?.daily_interest_rate}
                            </Paragraph>
                            <Paragraph>
                                <Translate content={"credit.calculator.form.security_coefficient"}  />&nbsp;
                                 - {active_terms?.min_security_ratio}
                            </Paragraph>
                            <Paragraph>
                                <Translate content={"credit.calculator.form.frequency_repayment"}  />&nbsp;
                                 - <Translate content={`credit.status.repayment_interval.${active_terms?.repayment_interval?.name}`}  />
                            </Paragraph>
                            { valueRules.min ? <Paragraph>
                                <Translate content={"credit.calculator.form.min_credit"}  />&nbsp; - {valueRules.min} {getAlias(loan_currencies)}
                            </Paragraph> : null}
                            { valueRules.max ? <Paragraph>
                                <Translate content={"credit.calculator.form.max_credit"}  />&nbsp;- {valueRules.max} {getAlias(loan_currencies)}
                            </Paragraph> : null}
                        </Col>

                    </Row>

                </Form>
            </Spin>
        )
    }
}
export default Form.create()(CreditCalculatorForm);



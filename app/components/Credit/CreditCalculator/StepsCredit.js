import React from "react";
import Translate from "react-translate-component";

import {cn} from "@bem-react/classname";
const cln = cn("Credit");
export default class StepsCredit extends React.PureComponent {

    static defaultProps = {
        titles: ["step_1", "step_2", "step_3"],
    };

    render() {
        const {titles, currentStep} = this.props;
        return (
            <div className={cln("steps-list")}>
                {titles.map((title, index)=>{
                    let indexItem = index + 1;
                    return (
                        <div className={cln("steps-items", {[indexItem]: true, active: indexItem < currentStep})} key={`step-item-${title}`}>
                            <Translate content={`credit.calculator.steps.${title}`} className={cln("steps-items-title", {[indexItem]: true})}   />
                            <Translate content={`credit.calculator.steps.${title}_info`} className={cln("steps-items-info")}   />
                        </div>
                    )
                })}
            </div>
        );
    }
}

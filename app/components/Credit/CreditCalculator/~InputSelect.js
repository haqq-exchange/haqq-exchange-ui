import React from "react";
import {Input, Select} from "antd";

export default class InputSelect extends React.Component {
    handleNumberChange = e => {
        const number = parseInt(e.target.value || 0, 10);
        if (isNaN(number)) {
            return;
        }
        this.triggerChange({number});
    };

    handleCurrencyChange = currency => {
        this.triggerChange({currency});
    };

    triggerChange = changedValue => {
        const {form, id} = this.props;
        /*if (onChange) {
            onChange({
                ...value,
                ...changedValue
            });
        }*/
        form.setFieldsValue({
            [id]: changedValue,
        });
    };

    render() {
        const {size, value, currencySelect} = this.props;

        console.log("this.props", this.props)

        return (
            <span>
                <Input
                    type="text"
                    size={size}
                    value={value?.number || 0}
                    onChange={this.handleNumberChange}
                    style={{width: "65%", marginRight: "3%"}}
                />
                <Select
                    getPopupContainer={()=>document.getElementById("Credit")}
                    value={value?.currency}
                    size={size}
                    style={{width: "32%"}}
                    onChange={this.handleCurrencyChange}>
                    {currencySelect.map(currency=>{
                        return <Select.Option key={'option-' + currency} value={currency}>{currency}</Select.Option>
                    })}
                </Select>
            </span>
        );
    }
}

import React from "react";
import Translate from "react-translate-component";
import CreditCalculatorForm from "Components/Credit/CreditCalculator/CreditCalculatorForm";

import {cn} from "@bem-react/classname";
const cln = cn("Credit");

const CreditStepOne = props => {
    return (
        <>
            <Translate component={"div"} className={cln("form-title")} content={"credit.calculator.form.title"} />
            <Translate component={"div"} className={cln("form-info")} content={"credit.calculator.form.info"} />
            <CreditCalculatorForm {...props} />
        </>
    )
};

export default CreditStepOne;


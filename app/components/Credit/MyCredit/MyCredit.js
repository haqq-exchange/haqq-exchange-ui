import React from "react";
import {Row, Col, Typography, Icon} from "antd";
import Translate from "react-translate-component";
import GivenCredits from "./GivenСredits"
import AppCredits from "./AppСredits"
import GivenCreditsFinal from "./GivenСreditsFinal"
import PaymentSchedule from "./PaymentSchedule"
import PaymentHistory from "./PaymentHistory"

import {cn} from "@bem-react/classname";
import {Link} from "react-router-dom";
import CreditActions from "../../../actions/CreditActions";
import ErrorReporter from "react-error-reporter";



const cln = cn("MyCredit");

class MyCredit extends React.Component{

    static defaultProps = {
        switchState: {
            "init": {
                "app":"credit.calculator.app_data_loading",
                "credit":"credit.calculator.credit_data_loading"
            },
            "sync": {
                "app":"credit.calculator.app_data_loading",
                "credit":"credit.calculator.credit_data_loading"
            },
            "update": {
                "app":"credit.calculator.not_credit_app",
                "credit":"credit.calculator.not_credit"
            },
            "error": {
                "app":"credit.calculator.not_credit_app",
                "credit":"credit.calculator.not_credit"
            },
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            loading: "init",
            loans: {
                app: [],
                credit: [],
                creditFinal: [],
            }
        }
    }

    componentDidMount() {
        const {creditStore} = this.props;
        console.log("creditStore", creditStore);
        //this.setState(creditStore, this.getLoadedMyCredit);
        this.getLoadedMyCredit()
    }

    getLoadedMyCredit = () => {
        const _this = this;
        const {getUserSign, Api, account} = this.props;
        const dataSend = {
            account: account.get("name")
        };
        _this.setState({
            loading: "sync",
        }, () => {

            getUserSign(dataSend).then(sign => {
                Api.postTemplate("/loans/get_info", dataSend, sign)
                    .then(({content})=>{
                        // console.log("content", content);
                        if( !content.error ) {
                            let app = content.loans.filter(loans=> loans.state.name.indexOf("application_") === 0);
                            let credit = content.loans.filter( loans =>
                                loans.state.name.indexOf("application_") !== 0 && !loans.state.is_final
                            );
                            let creditFinal = content.loans.filter( loans => loans.state.is_final);
                            let setData = {
                                system: content.system,
                                loans: {
                                    app,
                                    credit,
                                    creditFinal
                                },
                            };
                            CreditActions.setDataCredit(setData);
                            _this.setState({
                                loading: "update",
                                ...setData
                            })
                            /*_this.setState(setData, ()=>{
                                CreditActions.setDataCredit(setData);
                            })*/
                        }
                    }).catch(()=>{
                        _this.setState({
                            loading: "error"
                        })
                    });
            }).catch(error=>{
                console.log("error", error);
            });
        });

    };

    getChunkArray = array => {
        var i,j,

            tempArray = [],
            chunk = 2;
        for (i=0,j=array.length; i<j; i+=chunk) {
            tempArray.push(array.slice(i,i+chunk));
            // do whatever
        }
        return tempArray
    };

    render() {
        const {account, settings, switchState} = this.props;
        const {loans, system, loading} = this.state;

        const chunkLoansApp = this.getChunkArray(loans.app);
        const chunkLoansCredit = this.getChunkArray(loans.credit);
        const chunkLoansCreditFinal = this.getChunkArray(loans.creditFinal);

        let iconProps = {
            style: {
                fontSize: 13,
                cursor: "pointer",
            },
            onClick: () => this.getLoadedMyCredit()
        };
        if( loading === "sync" ) {
            iconProps.spin = true
        }

        return (
            <div className={cln()}>
                <Row>
                    <Col xs={{ span: 24 }} sm={{ span: 15 }} className={cln("left")} >
                        <Row>
                            <Translate

                                content={"credit.my.credits_title"}
                                with={{
                                    children: <Icon  type="sync" {...iconProps} />
                                }}
                                className={cln("title")}
                                component={Col} span={24}  />
                        </Row>
                        <Row className={cln("row")} >
                            {chunkLoansCredit.length ? chunkLoansCredit.map((chunk, index)=>{
                                return (
                                    <Row key={`row-index-${index}`} gutter={[0, 24]}>
                                        {chunk.map(loan=>{
                                            return <GivenCredits
                                                key={`given-сredit-${loan.id}-${loan.number}`}
                                                settings={settings}
                                                account={account}
                                                loan={loan}
                                                loan_currency={loan.loan_currency}
                                                collateral_currency={loan.collateral_currency}
                                            />
                                        })}
                                    </Row>
                                )
                            }) : <Translate content={switchState[loading].credit} component={Typography.Text}  />}
                        </Row>
                        <Row className={cln("row")} >
                            <Translate
                                content={"credit.my.app_credits"}
                                with={{
                                    children: <Icon type="sync" {...iconProps}  />
                                }}
                                className={cln("title")}
                                component={Col} span={24}  />
                            {chunkLoansApp.length ? chunkLoansApp.map((chunk, index)=>{
                                return (
                                    <Row key={`row-next-index-${index}`} gutter={[0, 24]} type="flex" align="top" style={{width: "100%"}}>
                                        {chunk.map(loan=>{
                                            return <AppCredits
                                                key={`app-сredit-${loan.id}-${loan.number}`}
                                                settings={settings}
                                                account={account}
                                                to_account={system?.collateral_amounts_keeper?.name}
                                                loan_currency={loan.loan_currency}
                                                collateral_currency={loan.collateral_currency}
                                                loan={loan} />
                                        })}
                                    </Row>
                                )
                            }) : <Translate content={switchState[loading].app} component={Typography.Text}  />}

                        </Row>

                        {chunkLoansCreditFinal.length ? <>
                            <Row>
                                <Translate

                                    content={"credit.my.credits_final_title"}
                                    with={{
                                        children: <Icon  type="sync" {...iconProps} />
                                    }}
                                    className={cln("title")}
                                    component={Col} span={24}  />
                            </Row>
                            <Row className={cln("row")} >
                                {chunkLoansCreditFinal.map((chunk, index)=>{
                                    return (
                                        <Row key={`row-index-${index}`} gutter={[0, 24]}>
                                            {chunk.map(loan=>{
                                                return <GivenCreditsFinal
                                                    key={`given-credit-${loan.id}-${loan.number}`}
                                                    settings={settings}
                                                    account={account}
                                                    loan={loan}
                                                    loan_currency={loan.loan_currency}
                                                    collateral_currency={loan.collateral_currency}
                                                />
                                            })}
                                        </Row>
                                    )
                                })}
                            </Row>
                        </> : null }
                    </Col>
                    <Col className={cln("right")} xs={{ span: 24 }} sm={{ span: 9 }}>
                        <Col span={24} className={cln("right-btn")} >
                            <Translate to={"/credit"} content={"credit.calculator.leave_request"} className={"btn btn-blue"} component={Link}  />
                        </Col>
                        <Col span={24} >
                            {loans.credit.length ? <PaymentSchedule
                                system={system}
                                loans={loans.credit}
                                {...this.props}/> : null}
                        </Col>
                        <Col span={24} >
                            <PaymentHistory
                                loans={loans.credit}
                                {...this.props}/>
                        </Col>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default class ErrorCredit extends React.Component {
    render() {
        return <ErrorReporter captureException={(error, ...args) => window.console.error(error, ...args)}>
            <MyCredit {...this.props} />
        </ErrorReporter>;
    }
}

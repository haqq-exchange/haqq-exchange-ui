import React from "react";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";

import {cn} from "@bem-react/classname";
import {Col, Row} from "antd";
//import {getAlias} from "../../../config/alias";
import BindToChainState from "Utility/BindToChainState";
import ChainTypes from "Utility/ChainTypes";
import Utils from "common/utils";
import translator from "counterpart";
const cln = cn("BlockCredit");

class GivenCreditsFinal extends React.Component{

    static propTypes = {
        loan_currency: ChainTypes.ChainAsset,
        collateral_currency: ChainTypes.ChainAsset,
    };

    render() {
        const {loan, account, settings, loan_currency, collateral_currency} = this.props;
        let timeOptions = {
            year: "numeric", month: "numeric", day: "numeric"
        };


        let sortPayment = loan.payments
            .sort((a,b)=> {
                return  (
                    a.due_dt < b.due_dt ? -1 :
                    a.due_dt > b.due_dt ? 1 : 0
                );
            });
        let nextPayment = sortPayment.find(payment=>{
                if( !payment.state.is_complete )
                    return payment
            });
        // console.log("nextPayment", nextPayment);
        // console.log("sortPayment", sortPayment[sortPayment.length - 1]);
        let createdDate = "";
        let lastPaymentDate = "";
        if( sortPayment[sortPayment.length - 1] ) {
            lastPaymentDate = new Intl
                .DateTimeFormat(settings.get("locale"), timeOptions)
                .format(new Date(sortPayment[sortPayment.length - 1].due_dt))
        }
        /* если нет значит все выплачено */
        if(nextPayment) {
            createdDate = new Intl
                .DateTimeFormat(settings.get("locale"), timeOptions)
                .format(new Date(nextPayment.due_dt));
        }


        // console.log("GivenСredits.js this.props", this.props);
        // console.log("loan", loan.current_security_ratio);

        return (
            <Col xs={{ span: 24 }} sm={{ span: 12 }}>
                <Link to={`/credit/view/${loan.id}`}  className={cln()} >
                    <Translate unsafe content={"credit.my.credit_number_date"} with={{
                        number: loan.number,
                        children: '',
                        date: lastPaymentDate
                    }} className={cln("title")} component={"div"}  />

                    <Row className={cln("row")}>
                        <Col span={12}>
                            <Translate content={"credit.my.sum_credit"} with={{children: ''}} component={"div"}  />
                            <b>{Utils.asset_amount(loan_currency, {real: loan.loan_amount})}</b>

                        </Col>
                        <Col span={12}>
                            <Translate
                                unsafe
                                content={"credit.my.credit_state"}
                                component={"div"}
                                with={{
                                    status: translator(`credit.status.${loan?.state?.name}`, {})
                                }}
                            />
                        </Col>
                    </Row>

                </Link>
            </Col>
        )
    }
}

export default BindToChainState(GivenCreditsFinal);

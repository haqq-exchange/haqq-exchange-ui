import React from "react";
import {Link} from "react-router-dom";
import {Checkbox, Col, Row, Divider} from "antd";
import Bail from "../Modals/Bail"
import Pay from "../Modals/Pay"

import Translate from "react-translate-component";
import {cn} from "@bem-react/classname";
import {getAlias} from "../../../config/alias";
import {ChainStore, FetchChain} from "deexjs";
import Utils from "../../../lib/common/utils";
const cln = cn("PSch");
export default class PaymentSchedule extends React.Component{


    constructor(props) {
        super(props);
        this.state = {
            sendPayment: null,
            assets: {},
            loan_currency: null,
            collateral_currency: null,
        };
    }

    isDisabledCheckbox = loan => {
        let {sendPayment} = this.state;
        if(!sendPayment) {
            return false
        } else {
            return sendPayment.number !== loan.number
        }
    };

    onChangeCheckbox = (event, payment) => {
        let {sendPayment} = this.state;
        const {value, checked} = event.target;
        if(checked ) {
            if( !sendPayment ) {
                sendPayment = payment.loan;
                sendPayment.payment_amount = 0
            }
            sendPayment.payment_amount += payment.required_payment_amount;
        } else {
            sendPayment.payment_amount -= payment.required_payment_amount;
        }
        if(!checked && !sendPayment.payment_amount) sendPayment = null;
        this.setState({sendPayment});
    };

    getLoadedAsset = asset => {
        const {assets} = this.state;
        if( !assets[asset] ) {
            FetchChain("getAsset", asset).then(result=>{
                assets[asset] = result;
                this.setState({assets})
            });
        }
    };

    paymentsMap = loan => {
        let loadedAsset = {};
        return loan.payments.map(payment=>{
            if(!loadedAsset[payment.currency]) {
                loadedAsset[payment.currency] = true;
                this.getLoadedAsset(payment.currency);
            }
            payment.loan = loan;
            return payment;
        });
    };

    getSortPayments = () => {
        const {loans, loan} = this.props;
        let paymentsArray = [];
        let loadedAsset = {};
        if( loan ) {
            paymentsArray = paymentsArray.concat(this.paymentsMap(loan));
        }
        if( loans ) {
            loans.map(loan=>{
                if( loan.payments.length ) {
                    paymentsArray = paymentsArray.concat(this.paymentsMap(loan))
                }
            });
        }
        return paymentsArray
            .sort((a, b) =>
                a.due_dt < b.due_dt ? -1 :
                a.due_dt > b.due_dt ? 1 : 0
            )
            .filter(a=>a);
    };

    isCompletePayments = payments => {
        return payments.filter(payment=>{
            if( payment?.state && !payment?.state?.is_complete )
                return payment
        }).length > 0
    };

    render() {
        const {account, settings, system} = this.props;
        const {sendPayment, assets, loan} = this.state;
        /*  checked={filters.filterFavorite} */
        let payments = this.getSortPayments();
        let timeOptions = {
            year: "numeric", month: "numeric", day: "numeric"
        };

        const postBailData = {
            amount: sendPayment?.payment_amount,
            asset: sendPayment?.loan_currency,
            memo: sendPayment?.id,
            loan: sendPayment,
            to_account: system?.interests_receiver?.name,
            from_account: account.get("name")
        };

        if( payments && !payments.hasOwnProperty('length') ) payments = [payments];

        let credit_content = loan?.status?.name === "credit_transferring" ?
            "credit.calculator.credit_transferring" :
            loan?.status?.name === "loan_complete" ?
                "credit.calculator.repaid_on_time" :
                "credit.calculator.not_credit_payments";

        //console.log("payments", payments);

        return (
            <div className={cln()}>
                <Translate content={"credit.my.graphic_payment"} className={cln("title")} component={"div"}  />
                <Divider />
                <div className={cln("wrap")}>
                    <Row className={cln("row")}>
                        {payments.length && this.isCompletePayments(payments) ? payments.map((payment,index)=>{
                            if( payment.state.is_complete ) return  null;
                            let createdDate = new Intl
                                .DateTimeFormat(settings.get("locale"), timeOptions)
                                .format(new Date(payment.due_dt));

                            return (
                                <Col key={`line-item-col-${payment.loan.id + index}`} className={cln("col")}>
                                    <Checkbox name={"pay"} disabled={this.isDisabledCheckbox(payment.loan)}  onChange={(event)=>this.onChangeCheckbox(event, payment)}>
                                        <Translate unsafe className={cln("col-item")} content="credit.my.credit_number_date_sum" with={{
                                            number: payment.loan.number,
                                            sum: Utils.asset_amount(assets?.[payment.currency], {real: payment.required_payment_amount}),
                                            date: createdDate,
                                        }}/>
                                    </Checkbox>
                                </Col>
                            );
                        }) : <Translate content={credit_content}  />}

                        <Col style={{"textAlign": "center", "margin": "20px 0 0 0"}}>
                            <Bail
                                btn={{
                                    className:"btn btn-green",
                                    disabled: !sendPayment
                                }}
                                {...postBailData} >
                                <Translate content={"credit.calculator.pay"}/>
                            </Bail>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

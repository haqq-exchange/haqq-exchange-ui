import React from "react";
import {Col, Row} from "antd";
import {Link} from "react-router-dom";
import {cn} from "@bem-react/classname";
import Translate from "react-translate-component";
import PostDeposit from "../Modals/PostDeposit"
import {getAlias} from "../../../config/alias";
import translator from "counterpart";
import Utils from "../../../lib/common/utils";
import ChainTypes from "Utility/ChainTypes";
import BindToChainState from "Utility/BindToChainState";

const cln = cn("BlockCredit");
class AppCredits extends React.Component{

    static propTypes = {
        loan_currency: ChainTypes.ChainAsset,
        collateral_currency: ChainTypes.ChainAsset,
    };

    render() {
        const {loan, to_account, account, settings, loan_currency, collateral_currency} = this.props;

        const postDepositData = {
            amount: loan.required_collateral_amount,
            asset: loan.collateral_currency,
            memo: loan.id,
            loan: loan,
            to_account: to_account,
            from_account: account.get("name")
        };

        let timeOptions = {
            year: "numeric", month: "numeric", day: "numeric"
        };
        let createdDate = new Intl
            .DateTimeFormat(settings.get("locale"), timeOptions)
            .format(new Date(loan.created));

        return (
            <Col xs={{ span: 24 }} sm={{ span: 12 }}>
                <div className={cln()} >
                    <Link to={`/credit/app/${loan.id}`}  className={cln("wrap")} >
                        <Translate unsafe content={"credit.my.app_credit"} with={{
                            number: loan.number,
                            children: "",
                            date: createdDate
                        }} className={cln("title")} component={"div"}  />

                        <Row className={cln("row")}>
                            <Col span={12}>
                                <Translate
                                    content={"credit.my.sum_credit"}
                                    with={{
                                        children: ""
                                    }}
                                    component={"div"}  />
                                {/*<b>{loan.loan_amount} {getAlias(loan.loan_currency)}</b>*/}
                                <b>{
                                    Utils.asset_amount(loan_currency, {
                                        real: loan.loan_amount
                                    })
                                }</b>
                            </Col>
                            <Col span={12}>
                                <Translate content={"credit.my.sum_zalog"} children={""} component={"div"}  />
                                {/*<b>{loan.required_collateral_amount} {getAlias(loan.collateral_currency)}</b>*/}
                                <b>{
                                    Utils.asset_amount(collateral_currency, {
                                        real: loan.required_collateral_amount
                                    })
                                }</b>
                            </Col>
                        </Row>
                    </Link>
                    <Row className={cln("row", {"status": true})}>

                        <Translate content={"credit.my.app_state"}
                                   unsafe
                                   className={""}
                                   component={"div"} with={{
                                        status: translator(`credit.status.${loan.state.name}`, {})
                                   }} />
                        {loan.state.name === "application_approved" ?
                            <PostDeposit
                                btn={{
                                    "className": "btn btn-green"
                                }}
                                {...postDepositData}  >
                                <Translate content={"credit.calculator.form.post_bail_btn"} component={"span"}  />
                            </PostDeposit> : null}

                    </Row>
                </div>
            </Col>
        )
    }
}

export default BindToChainState(AppCredits);

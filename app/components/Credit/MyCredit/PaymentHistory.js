import React from "react";
import {Link} from "react-router-dom";
import {Checkbox, Col, Row, Typography, Divider} from "antd";
import Bail from "../Modals/Bail"

import counterpart from "counterpart";
import Translate from "react-translate-component";
import {cn} from "@bem-react/classname";
import {getAlias} from "config/alias";
const cln = cn("PSch");

export default class PaymentHistory extends React.Component{

    state = {
        sendPayment: null
    };

    paymentsMap = loan => {
        return loan.payments.map(payment=>{
            if( payment.state.is_complete ) {
                payment.loan = loan;
                return payment;
            }
        });
    };

    getHistoryPayments = () => {
        const {loans, loan} = this.props;
        let paymentsArray = [];
        if( loan ) {
            paymentsArray = paymentsArray.concat(this.paymentsMap(loan))
        }
        if( loans ) {
            loans.map(loan=>{
                if( loan.payments.length ) {
                    paymentsArray = paymentsArray.concat(this.paymentsMap(loan))
                }
            });
        }
        return paymentsArray.filter(a=>a).sort((a, b) =>
            a.payment_dt > b.payment_dt ? -1 :
            a.payment_dt < b.payment_dt ? 1 : 0);
    };

    render() {
        const {account, settings, system} = this.props;
        const {sendPayment} = this.state;
        /*  checked={filters.filterFavorite} */
        const payments = this.getHistoryPayments();
        let timeOptions = {
            year: "numeric", month: "numeric", day: "numeric"
        };

        // console.log("this.props", this.props)
        // console.log("payments", payments);

        if(!payments.length)
            return null;


        return (
            <div className={cln()}>
                <Translate content={"credit.my.history_payment_title"} className={cln("title")} component={"div"}  />
                <Divider />
                <div className={cln("wrap")}>
                    <Row className={cln("row")}>
                        {payments.map((payment,index)=>{

                            let createdDate = new Intl
                                .DateTimeFormat(settings.get("locale"), timeOptions)
                                .format(new Date(payment.payment_dt));
                            let status = counterpart.translate(`credit.status.${payment.state.name}`);
                            let withTranslate = {
                                status,
                                amount: payment.payment_amount + " " + getAlias(payment.currency),
                                date: createdDate,
                            };
                            return (
                                <Col key={"payment-history-" + index } className={cln("col")}>
                                    <Translate content={"credit.modal.sub_title"} with={{
                                        number: payment.loan.number
                                    }} component={"h6"} className={"ant-typography"}  />
                                    <Translate
                                        unsafe
                                        className={cln("col-item")} content="credit.my.history_payment_item"
                                        with={withTranslate}/>
                                </Col>
                            )
                        })}
                    </Row>
                </div>
            </div>
        )
    }
}

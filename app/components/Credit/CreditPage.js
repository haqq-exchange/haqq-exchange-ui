import React from "react";
import {connect} from "alt-react";
import SettingsStore from "../../stores/SettingsStore";
import {Route, Switch, Redirect} from "react-router-dom";
import {CreditCalculator, MyCredit, ViewCredit, Page404} from "../../RoutesLink";
import accountUtils from "../../lib/common/account_utils";
import RefUtils from "../../lib/common/ref_utils";
import {getRequestAddress, getLoadedLocales} from "api/apiConfig";
import counterpart from "counterpart";

import LoadingIndicator from "Components/LoadingIndicator";

import AuthLoginPage from "Components/Layout/AuthLoginPage";

import "./style.scss";
import AccountStore from "stores/AccountStore";
import CreditStore from "stores/CreditStore";
import WalletUnlockStore from "stores/WalletUnlockStore";
import ChainTypes from "Utility/ChainTypes";
import BindToChainState from "Utility/BindToChainState";
import IntlStore from "../../stores/IntlStore";


const PrivateRoute = ({ render, ...options }) => {
    const { locked } = WalletUnlockStore.getState();
    const finalComponent = !locked  ? render : () => <AuthLoginPage title={"credit.calculator.auth_title"} info={"credit.calculator.auth_info"} />;

    return <Route {...options} render={finalComponent} />;
};

class CreditPage extends React.PureComponent {

    static propTypes = {
        account: ChainTypes.ChainAccount
    };


    static defaultProps = {
        getUserSign: accountUtils.getUserSign,
        Api: new RefUtils({
            url: getRequestAddress("api")
        })
    };

    constructor(props) {
        super(props);
        this.state = {
            localesLoaded: false
        }
    }

    /*componentDidUpdate(prevProps) {
        console.log("this.props.locked", this.props.locked, prevProps.locked )
        if( prevProps.locked !== this.props.locked ) {
            if( !this.props.locked ) {
                this.getLoadedLocales();
            }
        }
    }*/


    componentDidMount() {
        this.getLoadedLocales();
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        let {localesLoaded} = this.state;
        return nextProps.settings !== this.props.settings ||
            nextProps.location !== this.props.location ||
            nextProps.locale !== this.props.locale ||
            nextProps.locked !== this.props.locked ||
            nextState.localesLoaded !== localesLoaded
    }


    getLoadedLocales = () => {
        let trans = counterpart.translate("credit");
        let staticPath;
        if (__DEV__) {
            staticPath = "http://localhost:8081";
        }
        if( typeof trans !== "object" ) {
            getLoadedLocales("credit.json", undefined, staticPath)
                .then(this.setLoadedLocales);
        } else {
            this.setLoadedLocales();
        }
    };

    setLoadedLocales = () => {

        this.setState({localesLoaded: true});
        // this.hasExchanger(this.getRefInfo);
        // this.getAssets();
    };

    render() {
        const {localesLoaded} = this.state;

        if( !localesLoaded ) return  <LoadingIndicator />;

        return (
            <Switch>
                <Route path="/credit" exact render={(props)=>(<CreditCalculator {...this.props} {...props}  />)}/>
                <PrivateRoute path="/credit/my" exact render={(props)=>(<MyCredit {...this.props} {...props} />)}/>
                <PrivateRoute path="/credit/app/:loan_id" exact render={(props)=>(<ViewCredit {...this.props} {...props} app={true} />)}/>
                <PrivateRoute path="/credit/view/:loan_id" exact render={(props)=>(<ViewCredit {...this.props} {...props}  />)}/>
            </Switch>
        )
    }
}

const CreditPageBind = BindToChainState(CreditPage, {
    autosubscribe: false
});


export default connect(CreditPageBind, {
    listenTo() {
        return [SettingsStore, AccountStore, WalletUnlockStore, CreditStore, IntlStore];
    },
    getProps() {
        return {
            settings: SettingsStore.getState().settings,
            locked: WalletUnlockStore.getState().locked,
            locale: IntlStore.getState().currentLocale,
            account: AccountStore.getState().currentAccount || AccountStore.getState().passwordAccount,
            creditStore: CreditStore.getCreditStore()
        };
    }
});

import React from "react";
import {Col, Form, Input, InputNumber, Row, Select} from "antd";
import counterpart from "counterpart";
import {Asset} from "../../../lib/common/MarketClasses";
import RenderSelect from "../Layout/RenderSelect";
const {Option} = Select;

class CreditChangeForm extends React.Component {

    static defaultProps = {
        loan_period: [...Array(12).keys()]
    };

    state = {
        collateral: null,
        loan: null,
    };

    getShowError = (name) => {
        const {isFieldTouched, getFieldError}= this.props.form;
        const hasValueError = isFieldTouched(name) && getFieldError(name);
        return {
            validateStatus: hasValueError ? "error" : "",
            help: hasValueError || ""
        };
    };

    calculateAmount = (value, type) => {
        //const {form, creditStore} = this.props;
        const {exchange_rate, form, active_terms , loan_asset, collateral_asset} = this.props;

        let loanAsset = new Asset({
                asset_id: loan_asset.get("id"),
                precision: loan_asset.get("precision"),
                amount: 0
            }),
            collateralAsset = new Asset({
                asset_id: collateral_asset.get("id"),
                precision: collateral_asset.get("precision"),
                amount: 0
            });

        if( type === "loan_amount" ) {
            loanAsset.setAmount({real: value});
            collateralAsset.setAmount({real: (value / exchange_rate) * active_terms.min_security_ratio})
        } else if (type === "collateral_amount") {
            collateralAsset.setAmount({real: value});
            loanAsset.setAmount({real: (value * exchange_rate) / active_terms.min_security_ratio })
        }
        console.log("loanAsset", loanAsset);
        console.log("collateralAsset", collateralAsset);
        form.setFieldsValue({
            loan_amount: loanAsset.hasAmount() ? loanAsset.getAmount({real: true}) : 0,
            collateral_amount: collateralAsset.hasAmount() ? collateralAsset.getAmount({real: true}) : 0,
        });
    };

    /*changeAsset = (loan, collateral, type) => {
        //this.calculateAmount(0, type);
        this.props.getRate(loan, collateral).then(()=>{
            const {form} = this.props;
            let reType = type === "collateral_amount" ? "loan_amount" : "collateral_amount";
            let setName = type === "loan_amount" ? {"loan_currency": loan} : {"collateral_currency": collateral};
            const value = form.getFieldValue(type);
            const value1 = form.getFieldValue(reType);
            form.setFieldsValue(setName);
            this.calculateAmount(parseFloat(value1) || 0, reType);
        })
    };*/

    getSelectedValue = type => {
        const {collateral_asset, active_terms, loan_asset} = this.props;

        const loan_currencies_keys = active_terms?.loan_currencies ? Object.keys(active_terms?.loan_currencies) : null;

        let loan_currencies = loan_asset && loan_asset?.size ? loan_asset.get("symbol") : loan_asset?.symbol;
        if( !loan_currencies )
            loan_currencies = loan_currencies_keys ? loan_currencies_keys?.[0] : null;
        let collateral_currencies = collateral_asset && collateral_asset?.size
            ? collateral_asset.get("symbol") : collateral_asset?.symbol
                ? collateral_asset?.symbol  : active_terms?.collateral_currencies?.[0];
        const selectedValue = {
            "loan_amount": loan_currencies,
            "collateral_amount": collateral_currencies
        };
        return type ? selectedValue?.[type] : selectedValue;
    };

    changeAsset = (value, type) => {
        const {form} = this.props;
        this.setState({loading: true})
        let reType = type === "collateral_amount" ? "loan_amount" : "collateral_amount";
        const getValueRate = type => type === "collateral_amount" ? "collateral" : "loan";
        let value1 = this.getSelectedValue(reType);
        let sendRate = {
            [getValueRate(type)]: value,
            [getValueRate(reType)]: value1,
        };
        this.props.getRate(sendRate).then(()=>{
            const fieldValue1 = form.getFieldValue(reType);
            this.calculateAmount(parseFloat(fieldValue1) || 0, reType);
            this.setState({loading: false});
        })
    };

    submitForm = (event) => {
        const {getUserSign, Api} = this.props;
        event.preventDefault();
        this.props.form.validateFields((err, values) => {
            console.log("err, values", err, values)
            if (!err) {
                this.props.updateApp(values);
            }
        });
    };


    render() {
        const {form, account} = this.props;
        const {loan_period, active_terms, loan_asset, collateral_asset, loan} = this.props;
        const {getFieldDecorator} = form;
        const loan_currencies_keys = active_terms?.loan_currencies ? Object.keys(active_terms?.loan_currencies) : null;
        // let loan_currencies = loan_asset && loan_asset.get("symbol") || loan_currencies_keys ? loan_currencies_keys?.[0] : null;
        // let collateral_currencies = collateral_asset && collateral_asset.get("symbol") || active_terms?.collateral_currencies?.[0];
        // if(loan.loan_currency) loan_currencies = loan.loan_currency;
        // if(loan.collateral_currency) collateral_currencies = loan.collateral_currency;


        let loan_currencies = this.getSelectedValue("loan_amount");
        let collateral_currencies = this.getSelectedValue("collateral_amount");

        console.log("loan_currencies", loan_currencies);
        console.log("collateral_currencies", collateral_currencies);

        return (
            <>
                <Form onSubmit={this.submitForm} noValidate >
                    {getFieldDecorator("loan_id", {initialValue: loan?.id})(<Input type={"hidden"}/>)}
                    {loan?.daily_interest_rate ? getFieldDecorator("daily_interest_rate", {initialValue: loan.daily_interest_rate.toString()})(<Input type={"hidden"}/>) : null}
                    {getFieldDecorator("loan_terms", {initialValue: active_terms?.id})(<Input type={"hidden"}/>)}
                    {getFieldDecorator("loan_currency", {initialValue: loan_currencies})(<Input type={"hidden"}/>)}
                    {getFieldDecorator("repayment_interval", {initialValue: loan?.repayment_interval?.id})(<Input type={"hidden"}/>)}
                    {getFieldDecorator("loan_repayment_type", {initialValue: loan?.loan_repayment_type?.id})(<Input type={"hidden"}/>)}
                    {getFieldDecorator("collateral_currency", {initialValue: collateral_currencies})(<Input type={"hidden"}/>)}
                    {getFieldDecorator("account", {initialValue: account.get("name")})(<Input type={"hidden"}/>)}
                    <Row gutter={24}>
                        <Col>
                            <Form.Item label={counterpart.translate("credit.calculator.form.sum_credit")}>
                                {getFieldDecorator(`loan_amount`, {
                                    initialValue: loan?.loan_amount || 0,
                                    rules: [
                                        {
                                            required: true,
                                        },
                                    ],
                                })(<Input
                                    readOnly={!loan}
                                    onChange={(event)=>this.calculateAmount(parseFloat(event.target.value), "loan_amount")}
                                    addonAfter={<RenderSelect
                                        value={loan_currencies}
                                        onChange={value=>this.changeAsset(value,  "loan_amount")}
                                        options={loan_currencies_keys} />} /> ) }
                            </Form.Item>
                        </Col>

                        <Col>
                            <Form.Item label={counterpart.translate(
                                "credit.calculator.form.sum_zalog"
                            )}>
                                {getFieldDecorator(`collateral_amount`, {
                                    initialValue: loan?.required_collateral_amount || 0,
                                    rules: [
                                        {

                                            required: true,
                                            message: 'Please input your sum zalog',
                                        },
                                    ],
                                })(<Input
                                    readOnly={!loan}
                                    onChange={(event)=>this.calculateAmount(parseFloat(event.target.value), "collateral_amount")}
                                    addonAfter={<RenderSelect
                                        value={collateral_currencies}
                                        onChange={value=>this.changeAsset(value,  "collateral_amount")}
                                        options={active_terms?.collateral_currencies} />}/>)}
                            </Form.Item>
                        </Col>
                        <Col>
                            <Form.Item label={counterpart.translate(
                                "credit.calculator.form.loan_period", {
                                    name: counterpart.translate(`credit.status.repayment_interval.${active_terms?.repayment_interval?.name}`)
                                }
                            )}>
                                {getFieldDecorator(`loan_term`, {
                                    normalize: value => value && value.toString(),
                                    initialValue: loan?.loan_term || active_terms?.loan_term.max,
                                    rules: [
                                        {
                                            required: true,
                                            message: "Please input your loan period",
                                        },
                                    ],
                                })(<InputNumber

                                    /*onChange={value=>{
                                        console.log("value", value);
                                        // form.setFieldsValue("loan_term", value)
                                    }}*/
                                    min={active_terms?.loan_term.min}
                                    max={active_terms?.loan_term.max} />)}
                            </Form.Item>
                        </Col>





                    </Row>
                    <Row style={{"textAlign": "center"}}>
                        <button type={"submit"} className={"btn btn-green"} >Обновить заявку</button>
                    </Row>
                </Form>
            </>
        )
    }
}
export default Form.create()(CreditChangeForm);



import React from "react";
import {Col, Row} from "antd";
import {cn} from "@bem-react/classname";
import Translate from "react-translate-component";
import ViewBlockInfo from "../Layout/ViewBlockInfo";

const cln = cn("BlockCredit");
export default class ViewBlockApp extends React.Component {
    render() {
        const {settings, loan} = this.props;
        let timeOptions = {
            year: "numeric", month: "numeric", day: "numeric"
        };
        let lastPaymentsDate = loan.payments.map(payment => {
            if (!payment?.state?.is_complete)
                return payment.due_dt;
        }).filter(a => a);
        let afterPaymentsDate = loan.payments.map(payment => {
            if (payment?.state?.is_complete)
                return payment.due_dt;
        }).filter(a => a);
        let createdDate = "";
        if (lastPaymentsDate.length || afterPaymentsDate.length) {
            createdDate = new Intl
                .DateTimeFormat(settings.get("locale"), timeOptions)
                .format(new Date(lastPaymentsDate.pop() || afterPaymentsDate.pop()))
        }

        return (
            <Col span={24}>
                <div className={cln()}>
                    <Translate unsafe content={"credit.my.credit_number_date"} with={{
                        number: loan.number || "",
                        children: "",
                        date: createdDate
                    }} className={cln("title")} component={"div"}/>

                    <Row className={cln("row")}>
                        <Translate
                            content={"credit.my.credits_info_title"} className={cln("row-title")}
                            component={Col}/>

                        <ViewBlockInfo {...loan} />
                    </Row>

                </div>
            </Col>
        )
    }
}

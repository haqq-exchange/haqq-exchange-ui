import React from "react";
import {Col, Row} from "antd";
import {Link} from "react-router-dom";
import {cn} from "@bem-react/classname";
import Translate from "react-translate-component";
import ViewBlockInfo from "../Layout/ViewBlockInfo";
import CreditChangeForm from "./CreditChangeForm";
import CreditBase from "../Layout/CreditBase";
const translator = require('counterpart')
// import CreditActions from "../../../actions/CreditActions";
// import LoadingIndicator from "Components/LoadingIndicator";
// import PostDeposit from "Components/Credit/Modals/PostDeposit";

const cln = cn("BlockCredit");

class ViewBlockApp extends React.Component{

    state = {
        blockEdit: false
    };

    changeApp = blockEdit => {
        const {loan, getActiveTerms} = this.props;
        console.log("changeApp this.props", this.props)
        if(blockEdit) {
            getActiveTerms(loan).then(()=>{
                this.setState({blockEdit})
            });
        }
    };

    updateApp = values => {
        const {getUserSign, Api, account} = this.props;
        console.log("values", values);

        const dataSend = {
            account: account.get("name"),
            ...values
        };

        getUserSign(dataSend).then(sign => {
            Api.postTemplate("/loans/change_application", dataSend, sign)
                .then(({content})=>{
                    if(!content.error) {
                        console.log("content", content);
                        this.setState({
                            blockEdit: false
                        }, this.props.getLoadedCredit)
                    }
                });
        }).catch(error=>{
            console.log("error", error);
        });
    };


    render() {
        const {blockEdit} = this.state;
        const {loan, settings} = this.props;

        let timeOptions = {
            year: "numeric", month: "numeric", day: "numeric"
        };
        let createdDate = new Intl
            .DateTimeFormat(settings.get("locale"), timeOptions)
            .format(new Date(loan.created));

        let childrenTitle = [
            `<small style="margin-right: auto; color: green; line-height: 16px;margin-left: 20px"> ${translator( "credit.status." + loan.state.name)} </small>`,
        ];


        return (
            <>
                <Col span={blockEdit ? 16 : 24}>
                    <div  className={cln()} >
                        <Translate unsafe content={"credit.my.app_credit"} with={{
                            number: loan.number,
                            children: childrenTitle.join(" "),
                            date: createdDate
                        }} className={cln("title")} component={"div"}  />

                        <Row className={cln("row")}>
                            <Col className={cln("row-title")} >
                                <Translate content={"credit.my.app_info_title"}  /> &nbsp;
                                <Translate content={"credit.my.app_info_edit"} style={{cursor: "pointer"}} component={"small"} onClick={()=>this.changeApp(!blockEdit)} />
                            </Col>

                            <ViewBlockInfo
                                isApp={true}
                                {...loan} />

                        </Row>
                    </div>
                </Col>
                <Col span={blockEdit ? 8 : 0}>
                    <div  className={cln()} >
                        <CreditChangeForm
                            updateApp={this.updateApp}
                            {...this.props}/>
                    </div>
                </Col>
            </>
        )
    }
}

export default CreditBase(ViewBlockApp, {
    loading: false
})

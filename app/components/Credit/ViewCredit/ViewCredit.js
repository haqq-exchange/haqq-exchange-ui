import React, {useState} from "react";
import {Col, Row, Modal} from "antd";
import {Link} from "react-router-dom";
import ViewBlockApp from "./ViewBlockApp";
import Translate from "react-translate-component";
import ViewBlockCredit from "./ViewBlockCredit";
import PaymentSchedule from "Components/Credit/MyCredit/PaymentSchedule";
import PaymentHistory from "Components/Credit/MyCredit/PaymentHistory";


import {cn} from "@bem-react/classname";
import {Redirect} from "react-router";
import LoadingIndicator from "Components/LoadingIndicator";

import PostDeposit from "Components/Credit/Modals/PostDeposit";
import IncreaseCoefficient from "Components/Credit/Modals/IncreaseCoefficient";
import ReturnZalog from "Components/Credit/Modals/ReturnZalog";
import Page404 from "Components/Page404/Page404";

const { confirm } = Modal;
const cln = cn("ViewCredit");

export default class ViewCredit extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            error: false,
            loading: false,
            redirect: false,
            "system":{
            },
            "loan":{
            }
        }
    }

    componentDidMount() {
        this.getLoadedFullCredit();
    }

    getLoadedFullCredit = () => {
        const {getUserSign, Api, account, match, app} = this.props;

        const dataSend = {
            account: account.get("name"),
            ...match.params
        };

        getUserSign(dataSend).then(sign => {
            Api.postTemplate("/loans/get_loan_info", dataSend, sign)
                .then(({content, status})=>{
                    console.log("content, status", content, status);
                    if( status === 200 ) {
                        this.setState({
                            loan: content.loan,
                            system: content.system,
                            loading: false,
                            redirect: content.loan.state.name.indexOf("application_") === -1 && app
                        })
                    } else {
                        this.setState({
                            status,
                            ...content,
                        })
                    }
                })
                .catch(error=>{
                    console.log("error", error);
                    /*this.setState({
                        error: true,
                        loading: false
                    })*/
                });
        }).catch(error=>{
            console.log("error", error);
            this.setState({
                error: true,
                loading: false
            })
        });
    };

    render() {
        const {redirect, loan, error, status} = this.state;
        const {app, location} = this.props;

        if(redirect) {
            return <Redirect from={location.pathname} to={"/credit/my"} />
        }

        if(status === 404) return <Page404 />;
        if(error || !loan.id) return <LoadingIndicator />;

        return (
            <div className={cln()} id={"Credit"}>
                {app ?
                    <LayoutApp
                        {...this.props}
                        getLoadedCredit={this.getLoadedFullCredit}
                        callBack={data => this.setState(data)}
                        loan={this.state.loan}
                        system={this.state.system}
                    /> :
                    <LayoutCredit
                        {...this.props}
                        system={this.state.system}
                        loan={this.state.loan} />
                }
            </div>
        )
    }
}

class LayoutCredit extends React.Component {
    state = {
        isShowModalIncreaseCoefficient: false,
        isShowModalReturnZalog: false,
    };

    changeStateModal = data =>{
        this.setState(data)
    };

    render() {
        const {loan} = this.props;
        const {isShowModalIncreaseCoefficient, isShowModalReturnZalog} = this.state;
        return (
            <Row>
                <Col className={cln("top")} span={24}>
                    <Col xs={{ span: 24 }} sm={{ span: 12 }}>
                        <Translate
                            to={'/credit/my'}
                            content={"credit.calculator.back"}
                            className={cln("back")}  component={Link}  />
                    </Col>
                    <Col xs={{ span: 24 }} sm={{ span: 12 }} className={cln("right-btn")} >
                        <div className="btn-group">
                            {loan?.current_security_ratio?.name !== "low_margin_call_probability" ?
                            <Translate type={"button"} onClick={()=>this.changeStateModal({
                                isShowModalIncreaseCoefficient: !isShowModalIncreaseCoefficient
                            })} content={"credit.calculator.form.up_margin_btn"} className={"btn btn-blue"} component={"button"}  /> : null}
                            { ["collateral_refund", "credit_transferred"].indexOf(loan.state.name) !== -1   ?
                                <Translate
                                    type={"button"}
                                    onClick={()=>this.changeStateModal({
                                        isShowModalReturnZalog: !isShowModalReturnZalog
                                    })}
                                    content={"credit.calculator.form.return_deposit_btn"}
                                    className={"btn btn-blue"} component={"button"}  />: null}

                        </div>
                    </Col>
                </Col>
                <Col className={cln("left")} xs={{ span: 24 }} sm={{ span: 15 }}>
                    <ViewBlockCredit {...this.props}/>
                </Col>
                <Col className={cln("right")} xs={{ span: 24 }} sm={{ span: 9 }}>
                    <Col span={24} >
                        <PaymentSchedule {...this.props}/>
                    </Col>
                    <Col span={24} >
                        <PaymentHistory {...this.props}/>
                    </Col>
                </Col>
                <IncreaseCoefficient
                    {...this.props}
                    callBack={()=>this.changeStateModal({
                        isShowModalIncreaseCoefficient: false
                    })}
                    visible={isShowModalIncreaseCoefficient} />
                <ReturnZalog
                    {...this.props}
                    callBack={()=>this.changeStateModal({
                        isShowModalReturnZalog: false
                    })}
                    visible={isShowModalReturnZalog} />
            </Row>
        )
    }
}

const LayoutApp = props => {

    const postDepositData = {
        amount: props.loan.required_collateral_amount,
        asset: props.loan.collateral_currency,
        memo: props.loan.id,
        loan: props.loan,
        to_account: props.system?.collateral_amounts_keeper?.name,
        from_account: props.account.get("name"),
        callBack: () => {
            props.callBack({
                redirect: true
            })
        }
    };
    return (
        <Row>
            <Col span={12}>
                <Translate to={'/credit/my'} content={"credit.calculator.back"} className={cln("back")}  component={Link}  />
            </Col>
            <Col span={12} className={cln("right-btn")} >
                <div className="btn-group">
                    <Translate
                        type={"button"}
                        onClick={()=>showDeleteConfirm(props)}
                        content={"credit.calculator.cancel_application"} className={"btn btn-gray"} component={"button"}  />
                    {props.loan.state.name === "application_approved" ? <PostDeposit
                        btn={{
                            "className": "btn btn-green"
                        }}
                        {...postDepositData}  >
                        <Translate content={"credit.calculator.form.post_bail_btn"} component={"span"}  />
                    </PostDeposit> : null}
                    <Translate type={"button"} to={'/credit'} content={"credit.calculator.leave_request"} className={"btn btn-blue"} component={Link}  />
                </div>
            </Col>

            <Col className={cln("left")} span={24}>
                <ViewBlockApp {...props}/>
            </Col>
        </Row>
    )
};

const showDeleteConfirm = (props) => {

    confirm({
        title: 'Are you sure delete this app?',
        content: 'Some descriptions',
        okText: 'Yes',
        okType: 'danger',
        cancelText: 'No',
        onOk() {
            console.log('OK', props);

            const {getUserSign, Api, account, match} = props;

            const dataSend = {
                account: account.get("name"),
                ...match.params
            };

            getUserSign(dataSend).then(sign => {
                Api.postTemplate("/loans/cancel_application", dataSend, sign)
                    .then(({content})=>{
                        console.log("content", content);
                        if( props.callBack ) props.callBack({
                            redirect: true
                        });
                    });
            }).catch(error=>{
                console.log("error", error);
            });
        },
        onCancel() {
            console.log('Cancel');
        },
    });
};

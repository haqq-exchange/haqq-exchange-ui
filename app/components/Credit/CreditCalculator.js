import React from "react";
import {Link} from "react-router-dom"
import Translate from "react-translate-component";
import CreditBase from "./Layout/CreditBase";

import StepsCredit from "./CreditCalculator/StepsCredit"
import CreditStepOne from "./CreditCalculator/CreditStepOne"
import CreditStepTwo from "./CreditCalculator/CreditStepTwo"


import {cn} from "@bem-react/classname";
const cln = cn("Credit");


class CreditCalculator extends React.Component {

    constructor(props) {
        super(props);

        const {creditStore} = props;

        this.state = {
            step: 1,
            ...creditStore
        }
    }


    changeStep = (step, credit) => {
        this.setState({
            step,
            credit
        })
    };

    renderSwitch = () => {
        //console.log("CreditStepTwo", this.state, this.props);
        switch(this.state.step) {
            case 1:
                return <CreditStepOne
                    getRate={this.getRate}
                    changeStep={this.changeStep}
                    {...this.state}
                    {...this.props}
                />;
            case 2:
                return <CreditStepTwo
                    changeStep={this.changeStep}
                    {...this.state}
                    {...this.props}
                />;
            case 3:
                return <Error  />;

        }
    };

    render() {


        return (
            <div className={cln()} id={"Credit"}>
                <div className={cln("info")} >

                    <Translate component={"div"} className={cln("title")} content={"credit.calculator.title"} />
                    <Translate component={"div"} className={cln("info")} content={"credit.calculator.info"} />
                    <Translate component={Link} to={"/credit/my"} className={"btn btn-blue"} content={"credit.calculator.my_credit"} />
                </div>

                <div className={cln("steps")}>
                    <Translate component={"div"} className={cln("steps-title")} content={"credit.calculator.steps.title"} />
                    <StepsCredit {...this.props}/>
                </div>
                <div className={cln("form")}>
                    {this.renderSwitch()}
                </div>
            </div>
        )
    }
}

export default CreditBase(CreditCalculator, {
    loading: true
})

import React from "react";
// import {Link} from "react-router-dom";
import {Modal, Divider, Spin} from "antd";
import Translate from "react-translate-component";
import TransferWrapper from "Utility/TransferWrapper"
// import BalanceComponent from "Utility/BalanceComponent";
// import ReturnBodyCredit from "./ReturnBodyCredit"

import {cn} from "@bem-react/classname";
import {Asset} from "../../../lib/common/MarketClasses";
// import counterpart from "counterpart";
import PropTypes from "prop-types";
// import ModalActions from "../../../actions/ModalActions";
// import TransactionConfirmStore from "../../../stores/TransactionConfirmStore";
import {getAlias} from "../../../config/alias";
import {FetchChain} from "deexjs";
const cln = cn("ModalPay");
const clnInfo = cn( "BlockCredit");
// const { Text , Paragraph} = Typography;
/*
* Реализовать залог
*  */
class SellCollateral extends React.Component {

    static propTypes = {
        callBack: PropTypes.func,
        children: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object
        ]).isRequired,
    };

    static defaultProps = {
        callBack: () => {}
    };

    state = {
        visible: false,
        loading: false,
        repayment_info: {}
    };

    componentDidMount() {
        console.log("this componentDidMount", this )
        this.getRepaymentInfo()
    }

    getRepaymentInfo = () => {
        const {getUserSign, Api, account, match, loan} = this.props;

        const dataSend = {
            account: account.get("name"),
            ...match.params
        };

        getUserSign(dataSend).then(sign => {
            Api.postTemplate("/loans/get_repayment_conditions", dataSend, sign)
                .then(({content})=>{
                    FetchChain("getAsset", loan.collateral_currency).then(currency=>{
                        this.setState({
                            repayment_info: content.repayment_conditions,
                            collateral_currency: currency.toJS()
                        })
                    });
                });
        }).catch(error=>{
            console.log("error", error);
            this.setState({
                error: true,
                loading: false
            })
        });
        console.log("loaded data", this.props)
    };
    showModal = () => {
        this.setState({
            visible: true,
        });
    };
    handleOk = e => {
        this.setState({
            visible: false,
        });
    };
    handleCancel = e => {
        const {callBack} = this.props;
        console.log(e);
        this.setState({
            visible: false,
            loading: false,
        }, callBack );
    };

    getAmountLoanAsset = real => {
        const {collateral_currency} = this.state;
        if( collateral_currency ) {
            let loanAsset = new Asset({
                asset_id: collateral_currency.id,
                precision: collateral_currency.precision,
            });
            loanAsset.setAmount({real});
            return loanAsset.getAmount({real: true});
        }

        return real;
    };

    sellCollateral = () => {
        const {getUserSign, Api, account, match, loan} = this.props;
        const {repayment_info} = this.state;

        this.setState({loading: true});
        const dataSend = {
            account: account.get("name"),
            repayment_conditions: repayment_info,
            ...match.params
        };

        getUserSign(dataSend).then(sign => {
            Api.postTemplate("/loans/initiate_repayment", dataSend, sign)
                .then(this.showSendInitiateRepayment);
        }).catch(error=>{
            console.log("error", error);
            this.setState({
                error: true,
                loading: false
            })
        });
    };

    showSendInitiateRepayment = () => {
        const _this = this;
        let secondsToGo = 5;
        this.setState({loading: false});
        const modal = Modal.success({
            title: 'Данные отправлены',
            content: `Заявка на возврат залога отправлена`,
            onOk() {
                _this.handleCancel();
            },
        });
        const timer = setInterval(() => {secondsToGo -= 1;}, 1000);
        setTimeout(() => {
            clearInterval(timer);
            modal.destroy();
        }, secondsToGo * 1000);
    };

    render() {
        const {repayment_info, collateral_currency} = this.state;
        const {children, btn, loan,  ...other} = this.props;

        return (
            <>
                <button onClick={()=>this.showModal()} {...btn} >
                    {children}
                </button>
                <Modal
                    width={430}
                    onCancel={this.handleOk}
                    getContainer={()=>document.getElementById("content-wrapper")}
                    footer={null}
                    visible={this.state.visible} >
                        <div className={cln("wrap")}>
                            <div className={cln("top")}>
                                <Translate content={"credit.modal.title.return_deposit"} className={cln("title")} component={"div"}  />
                                <Translate content={"credit.modal.sub_title"} with={{
                                    number: loan.number
                                }} className={cln("title", {sub: true})} component={"div"}  />
                            </div>
                            <div className={clnInfo("oferta-list")}>
                                {repayment_info.unpaid_interest_amount ? <div className={clnInfo("oferta-item")}>
                                    <Translate content={"credit.calculator.form.unpaid_interest_amount"} />
                                    <span>{repayment_info.unpaid_interest_amount} { getAlias(loan.loan_currency)}</span>
                                </div>: null}
                                {repayment_info.remaining_loan_amount ? <div className={clnInfo("oferta-item")}>
                                    <Translate content={"credit.calculator.form.remaining_loan_amount"} />
                                    <span>{this.getAmountLoanAsset(repayment_info.remaining_loan_amount)} { getAlias(loan.loan_currency)}</span>
                                </div>: null}
                                {repayment_info.collateral_amount_to_debit ? <div className={clnInfo("oferta-item")}>
                                    <Translate content={"credit.calculator.form.collateral_amount_to_debit_collateral_amount"} />
                                    <span>{this.getAmountLoanAsset(repayment_info.collateral_amount_to_debit + repayment_info.remaining_collateral_amount)} { getAlias(loan.collateral_currency)}</span>
                                </div>: null}
                                {repayment_info.collateral_amount_to_debit ? <div className={clnInfo("oferta-item")}>
                                    <Translate content={"credit.calculator.form.collateral_amount_to_debit"} />
                                    <span>{this.getAmountLoanAsset(repayment_info.collateral_amount_to_debit)} { getAlias(loan.collateral_currency)}</span>
                                </div>: null}
                                {repayment_info.remaining_collateral_amount ? <div className={clnInfo("oferta-item")}>
                                    <Translate content={"credit.calculator.form.remaining_collateral_amount"} />
                                    <span>{this.getAmountLoanAsset(repayment_info.remaining_collateral_amount)} { getAlias(loan.collateral_currency)}</span>
                                </div>: null}
                            </div>
                            <Divider />
                            <Spin spinning={this.state.loading} >
                                <div className="btn-group btn-group_center btn-group_flex">
                                    <Translate
                                        type={"button"}
                                        onClick={()=>this.sellCollateral()}
                                        className={"btn btn-blue"} content={"credit.calculator.form.cell_bail_btn"} component={"span"}  />
                                </div>
                            </Spin>
                        </div>
                </Modal>
            </>
        );
    }
}



// export default SellCollateral;
export default TransferWrapper(SellCollateral, {});

import React from "react";
import {Col, Form, Input, Modal, Row, Typography, Spin} from "antd";
import Translate from "react-translate-component";
import TransferWrapper from "Utility/TransferWrapper"

import {cn} from "@bem-react/classname";
import {Asset} from "../../../lib/common/MarketClasses";
import counterpart from "counterpart";
import BalanceComponent from "Utility/BalanceComponent";
import {getAlias} from "../../../config/alias";
import ModalActions from "../../../actions/ModalActions";
const cln = cn("ModalPay");
const { Text } = Typography;


/*
* Оплата очередного платежа
* Оплата очередного платежа
* */
class Bail extends React.Component {

    state = {
        visible: false
    };

    componentDidMount() {
        console.log("this componentDidMount", this )
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    handleOk = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleCancel = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    render() {
        // const {amount} = this.props;
        const {children, btn , ...other} = this.props;

        if(!other.loan) return null;

        return (
            <>

                <button onClick={this.showModal} {...btn} >
                    {children}
                </button>
                <Modal
                    width={430}
                    onCancel={this.handleCancel}
                    getContainer={()=>document.getElementById("content-wrapper")}
                    footer={null}
                    visible={this.state.visible} >
                        <div className={cln("wrap")}>
                            <div className={cln("top")}>
                                <Translate content={"credit.modal.title.pay"} className={cln("title")} component={"div"}  />
                                <Translate content={"credit.modal.sub_title"} with={{
                                    number: other.loan.number
                                }} className={cln("title", {sub: true})} component={"div"}  />
                            </div>
                            <PayFormCreate
                                closeModal={this.handleCancel}
                                {...other} />
                        </div>
                </Modal>
            </>
        );
    }
}


class PayForm extends React.Component {

    static defaultProps = {
    };

    state = {
        collateral: null,
        loading: false,
        loan: null,
    };

    getShowError = (name) => {
        const {isFieldTouched, getFieldError}= this.props.form;
        const hasValueError = isFieldTouched(name) && getFieldError(name);
        return {
            validateStatus: hasValueError ? "error" : "",
            help: hasValueError || ""
        };
    };


    submitForm = (event) => {
        const { submitTransfer , closeModal } = this.props;
        this.setState({loading: true});
        submitTransfer(event, false)
            .then(closeModal)
            .catch(error=>{
                console.log("error", error)
                this.setState({loading: false});
            })
    };

    showModalDW = (asset, typeWindow) => {
        const { from_account , closeModal } = this.props;
        closeModal();
        let dataModal = {
            account: from_account.get("name"),
            asset: asset.get("id"),
            asset_id: asset.get("id"),
            onShowForm: () => {
                ModalActions.hide("transfer_asset_modal");
            }
        };

        console.log("dataModal", dataModal);

        ModalActions.show("transfer_asset_modal", {
            typeWindow,
            dataModal
        });
    };

    render() {
        const {form, balanceError, from_account, asset, amount} = this.props;
        console.log("this.props", this.props)
        const {getFieldDecorator} = form;
        let account_balances;
        if( typeof from_account === "object") {
            account_balances = from_account.get("balances").toJS();
        }

        console.log("this.props", this.props);

        return (
            <div>
                <Spin spinning={this.state.loading} >
                    <Form onSubmit={(event)=>this.submitForm(event)} noValidate >

                        <Row>
                            <Col>
                                <Form.Item label={counterpart.translate(
                                    "credit.calculator.form.payment_amount"
                                )} >
                                    {getFieldDecorator(`amount`, {
                                        initialValue: amount ? amount.getAmount({real: true}) : null,
                                        rules: [
                                            {
                                                required: true
                                            },
                                        ],
                                    })(<Input
                                        onChange={event=>this.props.setAmount(event.target.value)}
                                        addonAfter={<Text>{getAlias(asset.get("symbol"))}</Text>  } />) }
                                </Form.Item>
                            </Col>
                            {account_balances ? <Col>
                                {account_balances[asset.get("id")] ? <Text type="secondary">
                                    <Translate content="modal.withdraw.available" />
                                    <BalanceComponent balance={account_balances[asset.get("id")]} />
                                </Text> :  <Text type="secondary">
                                        Денег нет
                                </Text>}
                            </Col> : null}
                        </Row>
                        <Row>
                            <Col className={cln("info-text")}>
                                {balanceError ? <Text type="secondary">
                                    На вашем кошельке недостаточно средсв для оплаты очередного платежа, рекомендуем пополнить свой баланс.
                                </Text> : null}
                            </Col>
                        </Row>

                        <Row style={{
                            "textAlign": "center",
                            "marginTop": "30px",
                        }}>
                            {balanceError ?
                                <Translate onClick={()=>this.showModalDW(asset, "depositAddress")} content={"modal.deposit.submit"} className={"btn btn-green"} component={"span"}  />:
                                <Translate type={"submit"} content={"credit.calculator.pay"} className={"btn btn-green"} component={"button"}  />}
                        </Row>
                    </Form>
                </Spin>
            </div>
        )
    }
}
const PayFormCreate = Form.create()(PayForm);

export default TransferWrapper(Bail, {});

import React from "react";
import {Divider, Modal, Typography} from "antd";
import Translate from "react-translate-component";
import ViewBlockInfo from "../Layout/ViewBlockInfo";
import SellCollateral from "Components/Credit/Modals/SellCollateral";
import ReturnBodyCredit from "Components/Credit/Modals/ReturnBodyCredit";

import {cn} from "@bem-react/classname";

const cln = cn("ModalPay");
const {Text} = Typography;

export default class ReturnZalog extends React.Component {

    static defaultProps = {
        callBack: () => {
        }
    };

    state = {visible: false};

    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    handleCancel = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleOk = e => {
        const {callBack} = this.props;

        console.log("ReturnZalog handleCancel", this.props);

        this.setState({
            visible: false,
        }, callBack);
    };

    render() {
        const {content, visible, callBack, account, system, loan, ...other} = this.props;

        console.log("ReturnZalog.js this.props", this.props)

        let returnBodyData = {
            all_return_credit: true,
            asset: loan.loan_currency,
            amount: loan.loan_amount,
            memo: loan.id,
            from_account: account.get("name"),
            to_account: system?.loans_operator?.name,
            callBack: event => this.handleOk(event),
            ...this.props
        };
        let postDepositData = {
            amount: loan.required_collateral_amount,  /* сумма довнесения */
            asset: loan.collateral_currency,
            memo: loan.id,
            loan: loan,
            system: system,
            to_account: system?.collateral_amounts_keeper?.name,
            from_account: account.get("name"),
            callBack: event => this.handleOk(event),
            ...this.props
        };

        console.log("returnBodyData", returnBodyData);

        return (
            <>
                <Modal
                    width={440}
                    onCancel={this.handleOk}
                    getContainer={() => document.getElementById("content-wrapper")}
                    footer={null}
                    visible={visible}>
                    <div className={cln("wrap")}>
                        <div className={cln("top")}>
                            <Translate content={"credit.modal.title.return_deposit"} className={cln("title")}
                                       component={"div"}/>
                            <Translate content={"credit.modal.sub_title"} with={{
                                number: loan.number
                            }} className={cln("title", {sub: true})} component={"div"}/>
                        </div>
                        <div className={cln("body")}>
                            <ViewBlockInfo
                                loan_amount={loan.loan_amount}
                                loan_currency={loan.loan_currency}
                                collateral_amount={loan.collateral_amount}
                                collateral_currency={loan.collateral_currency}
                                security_coefficient={loan.security_coefficient}
                                current_security_ratio={loan.current_security_ratio}
                                cln={"BlockCredit"}/>

                                <Divider />
                                <div className={cln("btn-group")}>
                                    <div className="btn-group btn-group_center btn-group_flex">
                                        <SellCollateral
                                            btn={{
                                                "className": "btn btn-green"
                                            }}
                                            {...postDepositData}
                                             >
                                            <Translate content={"credit.calculator.form.cell_bail_btn"} component={"span"}  />
                                        </SellCollateral>
                                        <ReturnBodyCredit
                                            btn={{
                                                "className": "btn btn-green"
                                            }}
                                            {...returnBodyData}
                                        >
                                            <Translate content={"credit.calculator.form.return_credit_btn"} component={"span"}  />
                                        </ReturnBodyCredit>

                                </div>
                            </div>
                        </div>
                    </div>
                </Modal>
            </>
        );
    }
}



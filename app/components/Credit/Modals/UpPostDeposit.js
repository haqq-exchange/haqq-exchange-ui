import React from "react";
import {Link} from "react-router-dom";
import {Col, Form, Input, Modal, Row, Typography, Divider, Spin} from "antd";
import Translate from "react-translate-component";
import TransferWrapper from "Utility/TransferWrapper"
import BalanceComponent from "Utility/BalanceComponent";
import ReturnBodyCredit from "./ReturnBodyCredit"

import {cn} from "@bem-react/classname";
import {Asset} from "../../../lib/common/MarketClasses";
import counterpart from "counterpart";
import PropTypes from "prop-types";
import ModalActions from "../../../actions/ModalActions";
import TransactionConfirmStore from "../../../stores/TransactionConfirmStore";
import {getAlias} from "../../../config/alias";
const cln = cn("ModalPay");
const { Text , Paragraph} = Typography;
/*
* ДоВнесение залога
*  */
class UpPostDeposit extends React.Component {

    static propTypes = {
        callBack: PropTypes.func,
        children: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object
        ]).isRequired,
    };

    static defaultProps = {
        callBack: () => {}
    };

    state = {
        visible: false
    };

    componentDidMount() {
        console.log("this componentDidMount", this )
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    };
    handleOk = e => {
        this.setState({
            visible: false,
        });
    };
    handleCancel = e => {
        const {callBack} = this.props;
        console.log(e);
        this.setState({
            visible: false,
        }, callBack );
    };

    render() {
        // const {amount} = this.props;
        const {children, btn, loan,  ...other} = this.props;
        return (
            <>
                <button onClick={this.showModal} {...btn} >
                    {children}
                </button>
                <Modal
                    width={445}
                    onCancel={this.handleOk}
                    getContainer={()=>document.getElementById("content-wrapper")}
                    footer={null}
                    visible={this.state.visible} >
                        <div className={cln("wrap")}>
                            <div className={cln("top")}>
                                <Translate content={"credit.modal.title.up_post_bail"} className={cln("title")} component={"div"}  />
                                <Translate content={"credit.modal.sub_title"} with={{
                                    number: loan.number
                                }} className={cln("title", {sub: true})} component={"div"}  />
                            </div>
                            <PayFormCreate
                                {...other}
                                loan={loan}
                                closeModal={this.handleCancel}
                            />
                        </div>
                </Modal>
            </>
        );
    }
}


class PayForm extends React.Component {

    static defaultProps = {
    };

    state = {
        collateral: null,
        loading: false,
        loan: null,
    };

    getShowError = (name) => {
        const {isFieldTouched, getFieldError}= this.props.form;
        const hasValueError = isFieldTouched(name) && getFieldError(name);
        return {
            validateStatus: hasValueError ? "error" : "",
            help: hasValueError || ""
        };
    };


    submitForm = (event) => {
        const { submitTransfer , closeModal } = this.props;
        this.setState({loading: true});
        submitTransfer(event, false)
            .then(closeModal)
            .catch(error=>{
                console.log("error", error)
                this.setState({loading: false});
            })
    };

    showModalDW = (asset, typeWindow) => {
        const { from_account, closeModal } = this.props;
        closeModal();
        let dataModal = {
            account: from_account.get("name"),
            asset: asset.get("id"),
            asset_id: asset.get("id"),
            onShowForm: () => {
                ModalActions.hide("transfer_asset_modal");
            }
        };

        ModalActions.show("transfer_asset_modal", {
            typeWindow,
            dataModal
        });
    };




    render() {
        const {form, amount, balanceError, loan, system, to_account, from_account, asset} = this.props;
        const {getFieldDecorator} = form;

        console.log("this.props", this.props);

        let account_balances = from_account.get("balances").toJS();
        let postDepositData = {
            amount: loan.loan_amount / 2,  /* сумма довнесения -- половина */
            asset: loan.loan_currency,
            memo: loan.id,
            loan: loan,
            to_account: system?.loans_operator?.name,
            from_account: from_account.get("name")
        };

        return (
            <Spin spinning={this.state.loading} >
                <Form onSubmit={(event)=>this.submitForm(event)} noValidate >
                    <Row>
                        <Col>
                            <Form.Item label={counterpart.translate(
                                "credit.calculator.form.recommended_amount"
                            )} >
                                {getFieldDecorator(`amount`, {
                                    initialValue: amount ? amount.getAmount({real: true}) : null,
                                    rules: [
                                        {
                                            required: true
                                        },
                                    ],
                                })(<Input
                                    onChange={event => this.props.setAmount(event.target.value)}
                                    addonAfter={<Text>{getAlias(asset.get("symbol"))}</Text>  } />) }
                            </Form.Item>
                        </Col>
                        <Col>
                            {account_balances[asset.get("id")] ?
                                <Text type="secondary">
                                    <Translate content="modal.withdraw.available" />
                                    <BalanceComponent balance={account_balances[asset.get("id")]} />
                                </Text>
                                : null }
                        </Col>
                    </Row>
                    <Row>
                        <Col className={cln("info-text")}>
                            {balanceError ? <>
                                <Translate content={"credit.calculator.info3"}  type="secondary" component={Text}  />
                                <Divider />
                                <Translate content={"credit.calculator.info2"} component={Paragraph}  />
                            </> : null}
                        </Col>

                    </Row>

                    <Row style={{
                        "textAlign": "center",
                        "marginTop": "30px",
                    }}>
                        {balanceError ?
                            <div className={"btn-group btn-group_center btn-group_flex"}>

                                <Translate onClick={()=>this.showModalDW(asset, "depositAddress")} content={"modal.deposit.submit"} className={"btn btn-green"} component={"span"}  />

                            </div>
                            :
                            <Translate type={"submit"} content={"credit.calculator.form.up_post_bail_btn"} className={"btn btn-green"} component={"button"}  />
                        }

                    </Row>
                </Form>
            </Spin>
        )
    }
}
const PayFormCreate = Form.create()(PayForm);

export default TransferWrapper(UpPostDeposit, {});

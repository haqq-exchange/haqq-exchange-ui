import React from "react";
import {Col, Form, Input, Modal, Row, Typography} from "antd";
import Translate from "react-translate-component";

import {cn} from "@bem-react/classname";
import counterpart from "counterpart";
const cln = cn("ModalPay");
const { Text } = Typography;

export default class Pay extends React.Component {
    state = { visible: false };

    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    handleOk = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleCancel = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    render() {
        const {content, children, btn, ...other} = this.props;

        console.log("other", other);

        return (
            <>
                <button onClick={this.showModal} {...btn} >
                    {children}
                </button>
                <Modal
                    width={430}
                    onCancel={this.handleOk}
                    getContainer={()=>document.getElementById("content-wrapper")}
                    footer={null}
                    visible={this.state.visible} >
                        <div className={cln("wrap")}>
                            <div className={cln("top")}>
                                <Translate content={"credit.modal.title.pay"} className={cln("title")} component={"div"}  />
                                <Translate content={"credit.modal.sub_title"} with={{
                                    number: "НОМЕР"
                                }} className={cln("title", {sub: true})} component={"div"}  />
                            </div>
                            <PayFormCreate {...other} />
                        </div>
                </Modal>
            </>
        );
    }
}


class PayForm extends React.Component {

    static defaultProps = {
    };

    state = {
        collateral: null,
        loan: null,
    };

    getShowError = (name) => {
        const {isFieldTouched, getFieldError}= this.props.form;
        const hasValueError = isFieldTouched(name) && getFieldError(name);
        return {
            validateStatus: hasValueError ? "error" : "",
            help: hasValueError || ""
        };
    };


    submitForm = (event) => {
        event.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log("values", values)
            }
        });
    };


    render() {
        const {form, loan_period, active_terms, loan_asset, collateral_asset, account} = this.props;
        const {getFieldDecorator} = form;
        const loan_currencies_keys = active_terms?.loan_currencies ? Object.keys(active_terms?.loan_currencies) : null;
        const loan_currencies = loan_asset && loan_asset.get("symbol") || loan_currencies_keys ? loan_currencies_keys?.[0] : null;
        const collateral_currencies = collateral_asset && collateral_asset.get("symbol") || active_terms?.collateral_currencies?.[0];

        console.log("this.props", this.props)

        return (
            <div>
                <Form onSubmit={this.submitForm} noValidate >
                    <Row>
                        <Col>
                            <Form.Item label={counterpart.translate(
                                "credit.calculator.form.sum_credit"
                            )} >
                                {getFieldDecorator(`amount`, {
                                    initialValue: 111,
                                    rules: [
                                        {
                                            required: true
                                        },
                                    ],
                                })(<Input addonAfter={"RUB"} />) }
                            </Form.Item>
                        </Col>
                        <Col>
                            <Text type="secondary">
                                Доступно 10 дикс
                            </Text>
                        </Col>
                    </Row>
                    <Row>
                        <Col className={cln("info-text")}>
                            <Text type="secondary">
                                На вашем кошельке недостаточно средсв для оплаты очередного платежа, рекомендуем пополнить свой баланс.
                            </Text>
                        </Col>
                    </Row>

                    <Row style={{
                        "textAlign": "center",
                        "marginTop": "30px",
                    }}>
                        <Translate type={"submit"} content={"credit.calculator.pay"} className={"btn btn-green"} component={"button"}  />
                    </Row>
                </Form>
            </div>
        )
    }
}
const PayFormCreate = Form.create()(PayForm);

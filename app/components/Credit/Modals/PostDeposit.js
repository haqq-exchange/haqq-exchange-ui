import React from "react";
import {Link} from "react-router-dom";
import {Col, Divider, Form, Input, Modal, Row, Spin, Typography} from "antd";
import Translate from "react-translate-component";
import TransferWrapper from "Utility/TransferWrapper"
import BalanceComponent from "Utility/BalanceComponent";

import {cn} from "@bem-react/classname";
import {Asset} from "../../../lib/common/MarketClasses";
import counterpart from "counterpart";
import PropTypes from "prop-types";
import ModalActions from "../../../actions/ModalActions";
import {getAlias} from "../../../config/alias";
//import TransactionConfirmStore from "../../../stores/TransactionConfirmStore";
const cln = cn("ModalPay");
const { Text , Paragraph} = Typography;
/*
* Внесение депозита
* Внесение залога
*  */
class PostDeposit extends React.Component {

    static propTypes = {
        callBack: PropTypes.func,
        children: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object
        ]).isRequired,
    };

    static defaultProps = {
        callBack: () => {}
    };

    state = {
        visible: false
    };

    componentDidMount() {
        console.log("this componentDidMount", this )
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    };
    handleCancel = e => {
        const {callBack} = this.props;
        console.log(e);
        this.setState({
            visible: false,
        }, callBack );
    };

    render() {
        // const {amount} = this.props;
        const {children, btn, loan,  ...other} = this.props;

        return (
            <>
                <button onClick={this.showModal} {...btn} >
                    {children}
                </button>
                <Modal
                    width={430}
                    onCancel={this.handleCancel}
                    getContainer={()=>document.getElementById("content-wrapper")}
                    footer={null}
                    visible={this.state.visible} >
                        <div className={cln("wrap")}>
                            <div className={cln("top")}>
                                <Translate content={"credit.modal.title.post_bail"} className={cln("title")} component={"div"}  />
                                <Translate content={"credit.modal.sub_title"} with={{
                                    number: loan.number
                                }} className={cln("title", {sub: true})} component={"div"}  />
                            </div>
                            <PayFormCreate
                                {...other}
                                closeModal={this.handleCancel}
                            />
                        </div>
                </Modal>
            </>
        );
    }
}


class PayForm extends React.Component {

    static defaultProps = {};

    state = {
        collateral: null,
        loading: false,
        loan: null,
    };

    getShowError = (name) => {
        const {isFieldTouched, getFieldError}= this.props.form;
        const hasValueError = isFieldTouched(name) && getFieldError(name);
        return {
            validateStatus: hasValueError ? "error" : "",
            help: hasValueError || ""
        };
    };


    submitForm = (event) => {
        const { submitTransfer , closeModal } = this.props;
        this.setState({loading: true});
        submitTransfer(event, false)
            .then(closeModal)
            .catch(error=>{
                console.log("error", error)
                this.setState({loading: false});
            });
    };

    showModalDW = (asset, typeWindow) => {
        const { from_account , closeModal } = this.props;
        closeModal();
        let dataModal = {
            account: from_account.get("name"),
            asset: asset.get("id"),
            asset_id: asset.get("id"),
            onShowForm: () => {
                ModalActions.hide("transfer_asset_modal");
            }
        };

        ModalActions.show("transfer_asset_modal", {
            typeWindow,
            dataModal
        });
    };




    render() {
        const {form, amount, balanceError, from_account, asset} = this.props;
        const {getFieldDecorator} = form;

        let account_balances = from_account.get("balances").toJS();

        return (
            <Spin spinning={this.state.loading} >
                <Form onSubmit={(event)=>this.submitForm(event)} noValidate >
                    <Row>
                        <Col>
                            <Form.Item label={counterpart.translate(
                                "credit.calculator.form.collateral_amount"
                            )} >
                                {getFieldDecorator(`amount`, {
                                    initialValue: amount ? amount.getAmount({real: true}) : null,
                                    rules: [
                                        {
                                            required: true
                                        },
                                    ],
                                })(<Input readOnly={true} addonAfter={<Text>{getAlias(asset.get("symbol"))}</Text>  } />) }
                            </Form.Item>
                        </Col>
                        <Col>
                            {account_balances[asset.get("id")] ?
                                <Text type="secondary">
                                    <Translate content="modal.withdraw.available" />
                                    <BalanceComponent balance={account_balances[asset.get("id")]} />
                                </Text>
                                : null }
                        </Col>
                    </Row>
                    <Row>
                        <Col className={cln("info-text")}>
                            {balanceError ? <>
                                <Translate content={"credit.calculator.info3"}  type="secondary" component={Text}  />
                                {/*<Divider />
                                <Translate content={"credit.calculator.info2"} component={Paragraph}  />*/}
                            </> : null}
                        </Col>
                    </Row>

                    <Row style={{
                        "textAlign": "center",
                        "marginTop": "30px",
                    }}>
                        {balanceError ?
                            <Translate onClick={()=>this.showModalDW(asset, "depositAddress")} content={"modal.deposit.submit"} className={"btn btn-green"} component={"span"}  />:
                            <Translate type={"submit"} content={"credit.calculator.form.post_bail_btn2"} className={"btn btn-green"} component={"button"}  />}

                    </Row>
                </Form>
            </Spin>
        )
    }
}
const PayFormCreate = Form.create()(PayForm);

export default TransferWrapper(PostDeposit, {});

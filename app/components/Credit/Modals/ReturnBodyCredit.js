import React from "react";
import {Link} from "react-router-dom";
import {Col, Divider, Form, Input, Modal, Row, Spin, Typography} from "antd";
import Translate from "react-translate-component";
import TransferWrapper from "Utility/TransferWrapper"
import BalanceComponent from "Utility/BalanceComponent";

import {cn} from "@bem-react/classname";
import counterpart from "counterpart";
import PropTypes from "prop-types";
import ModalActions from "../../../actions/ModalActions";
import {getAlias} from "../../../config/alias";
const cln = cn("ModalPay");
const { Text , Paragraph} = Typography;

/*
* Внесение депозита
* Внесение залога
*  */
class ReturnBodyCredit extends React.Component {

    static propTypes = {
        callBack: PropTypes.func,
        children: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object
        ]).isRequired,
    };

    static defaultProps = {
        callBack: () => {}
    };

    state = {
        visible: false
    };

    componentDidMount() {
        console.log("this componentDidMount", this )
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    };
    handleCancel = e => {
        const {callBack} = this.props;
        console.log("ReturnBodyCredit handleCancel",  this.props);
        this.setState({
            visible: false,
        }, callBack );
    };
    handleOk = e => {
        this.setState({
            visible: false,
        });
    };

    render() {
        // const {amount} = this.props;
        const {children, btn, loan,  ...other} = this.props;

        console.log("ReturnBodyCredit other", other);
        console.log("ReturnBodyCredit this.props", this.props);

        let titleContent = other.all_return_credit ? "credit.modal.title.return_all_credit" : "credit.modal.title.return_body_credit"

        return (
            <>
                <button onClick={this.showModal} {...btn} >
                    {children}
                </button>
                <Modal
                    width={430}
                    onCancel={this.handleOk}
                    getContainer={()=>document.getElementById("content-wrapper")}
                    footer={null}
                    visible={this.state.visible} >
                        <div className={cln("wrap")}>
                            <div className={cln("top")}>
                                <Translate
                                    content={titleContent}
                                    className={cln("title")} component={"div"}  />
                                <Translate content={"credit.modal.sub_title"} with={{
                                    number: loan.number
                                }} className={cln("title", {sub: true})} component={"div"}  />
                            </div>
                            <PayFormCreate
                                {...other}
                                loan={loan}
                                closeModal={this.handleCancel}
                            />
                        </div>
                </Modal>
            </>
        );
    }
}

class PayForm extends React.Component {

    static defaultProps = {
    };

    state = {
        collateral: null,
        loading: false,
        loan: null,
    };

    getShowError = (name) => {
        const {isFieldTouched, getFieldError}= this.props.form;
        const hasValueError = isFieldTouched(name) && getFieldError(name);
        return {
            validateStatus: hasValueError ? "error" : "",
            help: hasValueError || ""
        };
    };


    submitForm = (event) => {
        const { submitTransfer , closeModal } = this.props;
        this.setState({loading: true});
        submitTransfer(event, false)
            .then(closeModal)
            .catch(error=>{
                console.log("error", error)
                this.setState({loading: false});
            })
    };

    showModalDW = (asset, typeWindow) => {
        const { from_account, closeModal } = this.props;
        let dataModal = {
            account: from_account.get("name"),
            asset: asset.get("id"),
            asset_id: asset.get("id"),
            onShowForm: () => {
                ModalActions.hide("transfer_asset_modal");
            }
        };
        closeModal();
        ModalActions.show("transfer_asset_modal", {
            typeWindow,
            dataModal
        });
    };




    render() {
        const {form, amount, balanceError, from_account, asset, all_return_credit} = this.props;
        const {getFieldDecorator} = form;

        console.log("ReturnBodyCredit.js this.props", this.props)

        let account_balances = from_account.get("balances").toJS();
        let valueAmount = amount ? amount.getAmount({real: true}) / 2 : null;
        if(all_return_credit && valueAmount) {
            valueAmount = amount.getAmount({real: true});
        }

        return (
            <Spin spinning={this.state.loading} >
                <Form onSubmit={(event)=>this.submitForm(event)} noValidate >
                    <Row>
                        <Col>
                            <Form.Item label={counterpart.translate(
                                "credit.calculator.form.recommended_amount"
                            )} >
                                {getFieldDecorator(`amount`, {
                                    initialValue: valueAmount,
                                    rules: [
                                        {
                                            required: true
                                        },
                                    ],
                                })(<Input addonAfter={<Text>{getAlias(asset.get("symbol"))}</Text>  } />) }
                            </Form.Item>
                        </Col>
                        {amount && amount.getAmount({real: true}) > parseFloat(form.getFieldValue("amount")) ? <Col onClick={()=>form.setFieldsValue({
                            amount: amount.getAmount({real: true})
                        })} style={{"cursor": "pointer"}}>
                            <Text type="secondary" >
                                Полностью {amount.getAmount({real: true})} {getAlias(asset.get("symbol"))}
                            </Text>
                        </Col> : null}
                        <Col>
                            {account_balances[asset.get("id")] ?
                                <Text type="secondary">
                                    <Translate content="modal.withdraw.available" />
                                    <BalanceComponent balance={account_balances[asset.get("id")]} />
                                </Text>
                                : null }
                        </Col>
                    </Row>
                    <Row>
                        <Col className={cln("info-text")}>
                            {balanceError ? <>
                                <Translate content={"credit.calculator.info3"}  type="secondary" component={Text}  />
                                <Divider />
                                <Translate content={"credit.calculator.info2"} component={Paragraph}  />
                            </> : null}
                        </Col>
                    </Row>

                    <Row style={{
                        "textAlign": "center",
                        "marginTop": "30px",
                    }}>
                        {balanceError ?
                            <Translate onClick={()=>this.showModalDW(asset, "depositAddress")} content={"modal.deposit.submit"} className={"btn btn-green"} component={"span"}  />:
                            <Translate type={"submit"} content={"credit.calculator.pay"} className={"btn btn-green"} component={"button"}  />}

                    </Row>
                </Form>
            </Spin>
        )
    }
}
const PayFormCreate = Form.create()(PayForm);

export default TransferWrapper(ReturnBodyCredit, {});
// export default ReturnBodyCredit;

import React from "react";
import PropTypes from "prop-types";
import Translate from "react-translate-component";
import {ChainStore} from "deexjs";
import ChainTypes from "components/Utility/ChainTypes";
import BindToChainState from "components/Utility/BindToChainState";
import TDexWithdrawModal from "./TDexWithdrawModal";
import Modal from "react-foundation-apps/src/modal";
import Trigger from "react-foundation-apps/src/trigger";
import ZfApi from "react-foundation-apps/src/utils/foundation-api";
import AccountBalance from "../../Account/AccountBalance";
import TDexDepositAddressCache from "common/TDexDepositAddressCache";
import AssetName from "components/Utility/AssetName";
import LinkToAccountById from "components/Utility/LinkToAccountById";
import {requestDepositAddress} from "common/TDexMethods";
import utils from "common/utils";
import counterpart from "counterpart";
import QRCode from "qrcode.react";
import LoadingIndicator from "../../LoadingIndicator";
import notify from "actions/NotificationActions";
import WalletDb from "stores/WalletDb";
import Icon from "components/Icon/Icon";
import WalletUnlockActions from "actions/WalletUnlockActions";

class TDexGatewayDepositRequest extends React.Component {
    static propTypes = {
        gateway: PropTypes.string,
        deposit_coin_type: PropTypes.string,
        deposit_asset_name: PropTypes.string,
        deposit_account: PropTypes.string,
        receive_coin_type: PropTypes.string,
        account: ChainTypes.ChainAccount,
        issuer_account: ChainTypes.ChainAccount,
        deposit_asset: PropTypes.string,
        deposit_wallet_type: PropTypes.string,
        receive_asset: ChainTypes.ChainAsset,
        deprecated_in_favor_of: ChainTypes.ChainAsset,
        deprecated_message: PropTypes.string,
        action: PropTypes.string,
        supports_output_memos: PropTypes.bool.isRequired,
        min_amount: PropTypes.string, //PropTypes.number,
        asset_precision: PropTypes.number
    };

    constructor(props) {
        super(props);
        this.deposit_address_cache = new TDexDepositAddressCache();

        this.state = {
            receive_address: null,
            isAvailable: true
        };
        this.addDepositAddress = this.addDepositAddress.bind(this);

        this._copy = this._copy.bind(this);
        //document.addEventListener("copy", this._copy);
    }

    _copy(e) {
        console.log("_copy", e.target, e.currentTarget);
        try {
            if (this.state.clipboardText)
                e.clipboardData.setData("text/plain", this.state.clipboardText);
            else
                e.clipboardData.setData(
                    "text/plain",
                    counterpart.translate("gateway.use_copy_button")
                );
            e.preventDefault();
        } catch (err) {
            console.error(err);
        }
    }

    _getDepositObject() {
        return {
            inputCoinType: this.props.deposit_coin_type,
            outputCoinType: this.props.receive_coin_type,
            account: this.props.account.get("name"),
            user_id: this.props.account.get("id")
        };
    }

    _getDepositAddress() {
        let _this = this;
        requestDepositAddress(this._getDepositObject()).then(res => {
            if (res && res.address) {
                let account_name = this.props.account.get("name");
                _this.deposit_address_cache.cacheInputAddress(
                    this.props.gateway,
                    account_name,
                    this.props.deposit_coin_type,
                    this.props.receive_coin_type,
                    res.address,
                    ""
                );
                console.warn(82, res, this.props);
                _this.setState({receive_address: res.address});
            } else {
                console.warn(86, res);
            }
        });
    }

    UNSAFE_componentWillMount() {
        let account_name = this.props.account.get("name");
        let receive_address = this.deposit_address_cache.getCachedInputAddress(
            this.props.gateway,
            account_name,
            this.props.deposit_coin_type,
            this.props.receive_coin_type
        );
        console.log(92, receive_address, this.state, this.props);
        if (!receive_address || !this.state.receive_address) {
            this._getDepositAddress();
        } else this.setState({receive_address});
    }

    UNSAFE_componentWillReceiveProps(np) {
        if (
            np.user_id !== this.props.user_id ||
            np.action !== this.props.action ||
            np.deposit_coin_type !== this.props.deposit_coin_type ||
            np.receive_coin_type !== this.props.receive_coin_type
        ) {
            this._getDepositAddress();
        }
    }

    componentWillUnmount() {
        document.removeEventListener("copy", this._copy);
    }

    addDepositAddress(receive_address) {
        let account_name = this.props.account.get("name");
        this.deposit_address_cache.cacheInputAddress(
            this.props.gateway,
            account_name,
            this.props.deposit_coin_type,
            this.props.receive_coin_type,
            receive_address.address,
            receive_address.memo
        );

        this.setState({receive_address});
    }

    getWithdrawModalId() {
        // console.log( "this.props.issuer: ", this.props.issuer_account.toJS() )
        // console.log( "this.receive_asset.issuer: ", this.props.receive_asset.toJS() )
        return (
            "withdraw_asset_" +
            this.props.issuer_account.get("name") +
            "_" +
            this.props.receive_asset.get("symbol")
        );
    }

    onWithdraw() {
        if (WalletDb.isLocked()) {
            WalletUnlockActions.unlock().then(() => {
                ZfApi.publish(this.getWithdrawModalId(), "open");
            });
        } else {
            ZfApi.publish(this.getWithdrawModalId(), "open");
        }
    }

    toClipboard(clipboardText, copyType = "button") {
        try {
            this.setState({clipboardText}, () => {
                var textarea = document.createElement("textarea");
                textarea.textContent = clipboardText;
                document.body.appendChild(textarea);

                var selection = document.getSelection();
                var range = document.createRange();
                range.selectNode(textarea);
                selection.removeAllRanges();
                selection.addRange(range);

                document.execCommand("copy");
                selection.removeAllRanges();
                document.body.removeChild(textarea);

                notify.addNotification({
                    message: `${copyType} copied`,
                    level: "success",
                    autoDismiss: 1
                });
            });
        } catch (err) {
            console.error(err);
        }
    }

    render() {
        //console.log(138,this.state);
        let emptyRow = <div style={{display: "none", minHeight: 150}} />;
        if (
            !this.props.account ||
            !this.props.issuer_account ||
            !this.props.receive_asset
        ) {
            //return emptyRow;
            return <LoadingIndicator />;
        }
        let account_balances_object = this.props.account.get("balances");

        let balance = "0 " + this.props.receive_asset.get("symbol");
        if (this.props.deprecated_in_favor_of) {
            let has_nonzero_balance = false;
            let balance_object_id = account_balances_object.get(
                this.props.receive_asset.get("id")
            );
            if (balance_object_id) {
                let balance_object = ChainStore.getObject(balance_object_id);
                if (balance_object) {
                    let balance = balance_object.get("balance");
                    if (balance != 0) has_nonzero_balance = true;
                }
            }
            if (!has_nonzero_balance) return emptyRow;
        }

        // let account_balances = account_balances_object.toJS();
        // let asset_types = Object.keys(account_balances);
        // if (asset_types.length > 0) {
        //     let current_asset_id = this.props.receive_asset.get("id");
        //     if( current_asset_id )
        //     {
        //         balance = (<span><Translate component="span" content="transfer.available"/>: <BalanceComponent balance={account_balances[current_asset_id]}/></span>);
        //     }
        // }

        let receive_address = this.state.receive_address;
        /*
	        этому тут не место... пока коментирую, по хорошему, надо переносить в мутацию стейта
        if (!receive_address) {
            let account_name = this.props.account.get("name");
            receive_address = this.deposit_address_cache.getCachedInputAddress(this.props.gateway, account_name, this.props.deposit_coin_type, this.props.receive_coin_type);
        }
		*/
        if (!receive_address) {
            console.error("TDexGatewayDepositRequest.jsx: receive_address is not defined");
            return emptyRow;
        }

        let withdraw_modal_id = this.getWithdrawModalId();
        let deposit_address_fragment = null;
        let deposit_memo = null;

        let clipboardText = "";
        let withdraw_memo_prefix = "";
        let memoText;
        if (receive_address) {
            //console.log(195,this.props.account,this.state,receive_address)
            deposit_address_fragment = <span>{receive_address}</span>;
            clipboardText = receive_address;
            memoText = "dex:" + this.props.account.get("name");
            deposit_memo = <span>{memoText}</span>;
            withdraw_memo_prefix = this.props.deposit_coin_type + ":";
        }

        if (this.props.action === "deposit") {
            return (
                <div className="tdex__gateway grid-block no-padding no-margin">
                    <div className="small-12 medium-5">
                        <Translate
                            component="h4"
                            content="gateway.deposit_summary"
                        />
                        <div className="small-12 medium-10">
                            <table className="table">
                                <tbody>
                                    <tr>
                                        <Translate
                                            component="td"
                                            content="gateway.asset_to_deposit"
                                        />
                                        <td
                                            style={{
                                                fontWeight: "bold",
                                                color: "#4A90E2",
                                                textAlign: "right"
                                            }}
                                        >
                                            {this.props.deposit_asset}
                                        </td>
                                    </tr>
                                    <tr>
                                        <Translate
                                            component="td"
                                            content="gateway.asset_to_receive"
                                        />
                                        <td
                                            style={{
                                                fontWeight: "bold",
                                                color: "#4A90E2",
                                                textAlign: "right"
                                            }}
                                        >
                                            <AssetName
                                                name={this.props.receive_asset.get(
                                                    "symbol"
                                                )}
                                                replace={false}
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <Translate
                                            component="td"
                                            content="gateway.intermediate"
                                        />
                                        <td
                                            style={{
                                                fontWeight: "bold",
                                                color: "#4A90E2",
                                                textAlign: "right"
                                            }}
                                        >
                                            <LinkToAccountById
                                                account={this.props.issuer_account.get(
                                                    "id"
                                                )}
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <Translate
                                            component="td"
                                            content="gateway.your_account"
                                        />
                                        <td
                                            style={{
                                                fontWeight: "bold",
                                                color: "#4A90E2",
                                                textAlign: "right"
                                            }}
                                        >
                                            <LinkToAccountById
                                                account={this.props.account.get(
                                                    "id"
                                                )}
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <Translate content="gateway.balance" />
                                            :
                                        </td>
                                        <td
                                            style={{
                                                fontWeight: "bold",
                                                color: "#4A90E2",
                                                textAlign: "right"
                                            }}
                                        >
                                            <AccountBalance
                                                account={this.props.account.get(
                                                    "name"
                                                )}
                                                asset={this.props.receive_asset.get(
                                                    "symbol"
                                                )}
                                                replace={false}
                                            />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="small-12 medium-7">
                        <Translate
                            component="h4"
                            content="gateway.deposit_inst"
                        />
                        <label className="left-label">
                            <Translate
                                content="gateway.deposit_to"
                                asset={this.props.deposit_asset}
                            />
                            :
                        </label>
                        <label className="left-label">
                            <b>
                                <Translate
                                    content="gateway.tdex.min_amount"
                                    minAmount={utils.format_number(
                                        this.props.min_amount /
                                            utils.get_asset_precision(
                                                this.props.asset_precision
                                            ),
                                        this.props.asset_precision,
                                        false
                                    )}
                                    symbol={this.props.deposit_coin_type}
                                />
                            </b>
                        </label>
                        <label className="left-label">
                            <b style={{color: "red"}}>
                                <Translate
                                    component="b"
                                    content={this.props.maintenanceMsg}
                                />
                            </b>
                        </label>
                        <div style={{padding: "10px 0", fontSize: "1.1rem"}}>


                            <div key={"table-address"} className="table-address" > {/*onCopy={this._copy}*/}
                                <div className={"table-address-row"}>
                                    <div>ADDRESS: </div>
                                    <div>
                                        <div className="group-copy">
                                            <input type="text" value={clipboardText} readOnly={true}/>
                                            <button
                                                className="btn btn-red"
                                                onClick={this.toClipboard.bind(
                                                    this,
                                                    clipboardText,
                                                    "address"
                                                )}
                                            >
                                                <Icon name="clippy" />
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                {deposit_memo ? (
                                    <div className={"table-address-row"}>
                                        <div>MEMO: </div>
                                        <div>
                                            <div className="group-copy">

                                                <input type="text" value={memoText} readOnly={true}/>
                                                <button
                                                    className="btn btn-red"
                                                    onClick={this.toClipboard.bind(
                                                        this,
                                                        memoText,
                                                        "memo"
                                                    )}
                                                >
                                                    <Icon name="clippy" />
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                ) : null}
                                {this.props.dataKey ? (
                                    <div className={"table-address-row"}>
                                        <div>PUBLIC KEY: </div>
                                        <div>
                                            <b>{this.props.dataKey}</b>
                                        </div>
                                    </div>
                                ) : null}
                            </div>


                            <div
                                className="small-12 medium-5"
                                style={{paddingTop: "10px"}}
                            >
                                {deposit_address_fragment &&
                                deposit_address_fragment.props &&
                                deposit_address_fragment.props.children &&
                                typeof deposit_address_fragment.props
                                    .children === "string" ? (
                                    <QRCode
                                        size={120}
                                        value={
                                            String(
                                                deposit_address_fragment.props
                                                    .children
                                            ) +
                                            (memoText ? "\n" + memoText : "") +
                                            (this.props.dataKey
                                                ? "\n" + this.props.dataKey
                                                : "") +
                                            (this.props.maintenanceMsg
                                                ? "\n" +
                                                  this.props.maintenanceMsg
                                                : "")
                                        }
                                    />
                                ) : null}
                            </div>
                            <div
                                className="button-group"
                                style={{paddingTop: 10}}
                            >
                                {this.props.dataKey ? (
                                    <button
                                        className="button m-btn"
                                        onClick={this.toClipboard.bind(
                                            this,
                                            this.props.dataKey,
                                            "key"
                                        )}
                                    >
                                        Copy key
                                    </button>
                                ) : null}

                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="tdex__gateway grid-block no-padding no-margin">
                    <div className="small-12 medium-5">
                        <Translate
                            component="h4"
                            content="gateway.withdraw_summary"
                        />
                        <div className="small-12 medium-10">
                            <table className="table">
                                <tbody>
                                    <tr>
                                        <Translate
                                            component="td"
                                            content="gateway.asset_to_withdraw"
                                        />
                                        <td
                                            style={{
                                                fontWeight: "bold",
                                                color: "#4A90E2",
                                                textAlign: "right"
                                            }}
                                        >
                                            <AssetName
                                                name={this.props.receive_asset.get(
                                                    "symbol"
                                                )}
                                                replace={false}
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <Translate
                                            component="td"
                                            content="gateway.asset_to_receive"
                                        />
                                        <td
                                            style={{
                                                fontWeight: "bold",
                                                color: "#4A90E2",
                                                textAlign: "right"
                                            }}
                                        >
                                            {this.props.deposit_asset}
                                        </td>
                                    </tr>
                                    <tr>
                                        <Translate
                                            component="td"
                                            content="gateway.intermediate"
                                        />
                                        <td
                                            style={{
                                                fontWeight: "bold",
                                                color: "#4A90E2",
                                                textAlign: "right"
                                            }}
                                        >
                                            <LinkToAccountById
                                                account={this.props.issuer_account.get(
                                                    "id"
                                                )}
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <Translate content="gateway.balance" />
                                            :
                                        </td>
                                        <td
                                            style={{
                                                fontWeight: "bold",
                                                color: "#4A90E2",
                                                textAlign: "right"
                                            }}
                                        >
                                            <AccountBalance
                                                account={this.props.account.get(
                                                    "name"
                                                )}
                                                asset={this.props.receive_asset.get(
                                                    "symbol"
                                                )}
                                                replace={false}
                                            />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        {/*<p>When you withdraw {this.props.receive_asset.get("symbol")}, you will receive {this.props.deposit_asset} at a 1:1 ratio (minus fees).</p>*/}
                    </div>
                    <div className="small-12 medium-7">
                        <Translate
                            component="h4"
                            content="gateway.withdraw_inst"
                        />
                        <label className="left-label">
                            <Translate
                                content="gateway.withdraw_to"
                                asset={this.props.deposit_asset}
                            />
                            :
                        </label>
                        <div className="button-group" style={{paddingTop: 20}}>
                            <button
                                className="button success"
                                style={{fontSize: "1.3rem"}}
                                onClick={this.onWithdraw.bind(this)}
                            >
                                <Translate content="gateway.withdraw_now" />
                            </button>
                        </div>
                    </div>
                    <Modal id={withdraw_modal_id} overlay={true}>
                        <Trigger close={withdraw_modal_id}>
                            <a href="#" className="close-button">
                                &times;
                            </a>
                        </Trigger>
                        <br />
                        <div className="grid-block vertical">
                            <TDexWithdrawModal
                                account={this.props.account.get("name")}
                                issuer={this.props.issuer_account.get("name")}
                                asset={this.props.receive_asset.get("symbol")}
                                output_coin_name={this.props.deposit_asset_name}
                                output_coin_symbol={this.props.deposit_asset}
                                output_coin_type={this.props.deposit_coin_type}
                                output_wallet_type={
                                    this.props.deposit_wallet_type
                                }
                                output_supports_memos={
                                    this.props.supports_output_memos
                                }
                                memo_prefix={withdraw_memo_prefix}
                                modal_id={withdraw_modal_id}
                                min_amount={this.props.min_amount}
                                asset_precision={this.props.asset_precision}
                                balance={
                                    this.props.account.get("balances").toJS()[
                                        this.props.receive_asset.get("id")
                                    ]
                                }
                            />
                        </div>
                    </Modal>
                </div>
            );
        }
    }
}

export default BindToChainState(TDexGatewayDepositRequest, {
    keep_updating: true
});

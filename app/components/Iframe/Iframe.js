import React from 'react';
import "./iframe.scss";


const Iframe = ({ source }) => {

    if (!source) {
        return (
            <div style={{width: "793px", height: "479px", display: "flex", alignItems: "center", justifyContent: "center"}}>
                <div>Loading...</div>
            </div>
        );
    }

    const src = source;     
    return (
        // basic bootstrap classes. you can change with yours.
        <div className="col-md-12">
            <div className="emdeb-responsive iframe-wrap">
                <iframe src={src} scrolling="no" className="main-iframe"></iframe>
            </div>
        </div>
    );
};

export default Iframe;
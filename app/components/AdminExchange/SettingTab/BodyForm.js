import React, {useState} from "react";
import {diff} from "deep-object-diff";
import {Checkbox, Form, Input, Select} from "antd";
import Icon from "Components/Icon/Icon";
import {cn} from "@bem-react/classname";
import AdminExchangeActions from "../../../actions/AdminExchangeActions";
const cln = cn("SettingTab");
const {Option} = Select;

class BodyForm extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            marketAllSource: {},
            isChecked: props?.markets?.active || false
        };
    }

    /* componentDidUpdate(prevProps) {
        const {markets} = this.props;
        console.log("componentDidUpdate markets", markets)
        console.log("componentDidUpdate prevProps?.markets", prevProps?.markets)
        if( markets?.id !== prevProps?.markets?.id ) {
            console.log("UPDATE COURSE");
        }
    }*/

    setChecked = isChecked => {
        this.setState({isChecked});
    };

    handleCheckbox = (e) => {
        const {setFieldsValue} = this.props.form;
        this.setChecked(e.target.checked);
        setFieldsValue({
            [e.target.name]: e.target.checked
        });
    };

    changeExchange = market_id => {
        console.log("item", market_id);

        const {currentAccount, getUserSign, catchUserSign, refutils, settings} = this.props;
        const {marketAllSource} = this.state;
        if( market_id ) {
            const admin_exchange = settings?.get("admin_exchange");
            const sendBody = {
                account_name:  currentAccount,
                exchanger_id: admin_exchange.id,
                market_id
            };
            getUserSign(sendBody).then(sign => {
                refutils
                    .postTemplate("/control/getMarketAllSourceRates", sendBody, sign)
                    .then(({content})=>{
                        if( content.success ) {
                            console.log("content", content);
                            new Promise(resolve=>{
                                content.data.map(item=>{
                                    if(!marketAllSource[market_id]) marketAllSource[market_id] = {};
                                    marketAllSource[market_id][item.sourceId] = item;
                                });
                                resolve(marketAllSource);
                            }).then(source=>{
                                this.setState({marketAllSource: source});
                            });
                        }
                    });
            }).catch(catchUserSign);
        }
    };


    render() {
        const {getFieldDecorator, isFieldsTouched, getFieldsValue} = this.props.form;
        const {isChecked, marketAllSource} = this.state;
        const {markets, hasNewForm, exchangeSources, marketsRates} = this.props;

        let currentRates, sumExchangeFee;
        let marketsChange = getFieldsValue();
        let getDiffValue = diff(marketsChange, markets);
        let hasBodyFormChanges = getDiffValue ? isFieldsTouched() && !!Object.keys(getDiffValue).length : false;
        if( marketsRates ) {
            currentRates = marketsRates.find(rates => markets.id === rates.id);
        }
        let lastChangeRate = hasBodyFormChanges ? marketAllSource?.[markets?.id] : null;
        if( lastChangeRate ) {
            let exchangeFee = parseFloat(marketsChange.exchangeFee);
            let exchangeSource = parseFloat(lastChangeRate?.[marketsChange.exchangeSource]?.rate);
            sumExchangeFee = exchangeSource + (exchangeSource  / 100 * exchangeFee);
        }

        let marketsExchangeSource = markets?.exchangeSource;
        if(!marketsExchangeSource && exchangeSources ) {
            marketsExchangeSource = exchangeSources[0].id;
        }


        // console.log("marketAllSource", marketAllSource);
        // console.log("lastChangeRate", lastChangeRate);
        // console.log("sumExchangeFee", sumExchangeFee);
        // console.log("markets", markets);
        // console.log("getFieldsValue", marketsChange);

        //console.log("marketsRates", this.props, markets?.id, currentRates);

        return (
            <>
                <div className={cln("add-pairs-item", {"min-max": true})}>
                    <Form.Item style={{"display": "none"}}>
                        {getFieldDecorator("minAmount", {
                            validateTrigger: ["onChange"],
                            initialValue: markets?.minAmount,
                            rules: [
                                {
                                    required: false,
                                    message: "Заполнить минимум"
                                }
                            ]
                        })(<Input/>)}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator("reverseMinAmount", {
                            validateTrigger: ["onChange"],
                            initialValue: markets?.reverseMinAmount,
                            rules: [
                                {
                                    required: true,
                                    message: "Заполнить минимум"
                                }
                            ]
                        })(<Input/>)}
                    </Form.Item>
                </div>

                <div className={cln("add-pairs-item", {"min-max": true})}>
                    <Form.Item style={{"display": "none"}}>
                        {getFieldDecorator("maxAmount", {
                            validateTrigger: ["onChange"],
                            initialValue: markets?.maxAmount,
                            rules: [
                                {
                                    required: false,
                                    message: "Заполнить maximum"
                                }
                            ]
                        })(<Input/>)}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator("reverseMaxAmount", {
                            validateTrigger: ["onChange"],
                            initialValue: markets?.reverseMaxAmount,
                            rules: [
                                {
                                    required: true,
                                    message: "Заполнить maximum"
                                }
                            ]
                        })(<Input/>)}
                    </Form.Item>
                </div>

                <div className={cln("add-pairs-item", {"course": true})} style={{position: "relative"}}>
                    <Form.Item className={cln("add-pairs-item-form", {"name": true})}>
                        {getFieldDecorator("exchangeSource", {
                            validateTrigger: ["onChange"],
                            initialValue: marketsExchangeSource,
                            rules: [
                                {
                                    required: true,
                                    message: "Заполнить maximum"
                                }
                            ]
                        })(<Select
                            onChange={(data)=>this.changeExchange(markets?.id, data)}
                            getPopupContainer={() => document.getElementById("pairs-wrap-to-exchange")}
                            name="" id="">
                            {exchangeSources?.map(sources => {
                                if( sources.active ) {
                                    return (
                                        <Option key={`option-exchange-source-${sources.id}`} value={sources.id}>{sources.fullName}</Option>
                                    );
                                }
                            })}
                        </Select>)}
                        <div className={cln("add-pairs-info", {})}>
                            <div className={cln("add-pairs-info-rate", {})}>{lastChangeRate?.[marketsChange?.exchangeSource]?.rate || currentRates?.rate?.originalRate}</div>
                            <div className={cln("add-pairs-info-item", {
                                "up": currentRates?.rate?.percentageChange > 0,
                                "down": currentRates?.rate?.percentageChange < 0,
                                "hidden": !currentRates?.rate?.percentageChange || !!lastChangeRate?.[marketsChange?.exchangeSource],
                            })}>{currentRates?.rate?.percentageChange}%</div>
                        </div>
                    </Form.Item>
                    <Form.Item style={{marginTop: "10px"}}> + </Form.Item>
                    <Form.Item className={cln("add-pairs-item-form", {"percent": true})}>
                        {getFieldDecorator("exchangeFee", {
                            validateTrigger: ["onChange"],
                            initialValue: markets?.exchangeFee,
                            rules: [
                                {
                                    required: true,
                                    message: "Заполнить процент"
                                }
                            ]
                        })(<Input addonAfter={<span className={cln("add-pairs-addon-after")}>%</span>}/>)}
                        <div className={cln("add-pairs-info", {})}>
                            <div className={cln("add-pairs-info-rate", {})}>
                                {sumExchangeFee ? sumExchangeFee.toFixed(6) : currentRates?.rate?.rate} {sumExchangeFee ? "*" : null}
                            </div>
                        </div>
                    </Form.Item>
                </div>

                <div className={cln("add-pairs-item", {
                    "active": true,
                    "touched": hasBodyFormChanges,
                    "new-form": hasNewForm
                })}>
                    {getFieldDecorator("active", {
                        valuePropName: "active",
                        initialValue: markets?.active
                    })(
                        <Checkbox
                            name={"active"}
                            prefixCls={"ant-checkbox"}
                            checked={isChecked} onChange={this.handleCheckbox}/>
                    )}
                    <button type={"submit"} className={"btn save"}>
                        <Icon name={"save"}/>
                    </button>
                    {markets?.id ? <span className={"btn delete"} onClick={() => this.props.deletePairs(markets?.id)}>
                        <Icon name={"delete_sweep"}/>
                    </span> : null}
                </div>
            </>
        );
    }
}

export default BodyForm;

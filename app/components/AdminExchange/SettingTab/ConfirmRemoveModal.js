import React from "react";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
import DefaultModal from "Components/Modal/DefaultModal";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";


class ConfirmRemoveModal extends DefaultBaseModal {

    constructor(){
        super();

        this.state = {

        };
    }

    componentDidMount(){
    }

    closeModal = () => {
        const {reject, modalId} = this.props;
        ModalActions.hide(modalId)
            .then(reject);
    };
    confirmModal = () => {
        const {resolve, modalId} = this.props;
        ModalActions.hide(modalId)
            .then(resolve);
    };




    render () {
        const {isOpenModal, } = this.state;
        const {modalId, modalIndex, modalsData} = this.props;

        return (
            <DefaultModal
                id={modalId}
                modalId={modalId}
                isOpen={isOpenModal}
                className={["CenterModal",modalId].join(" ")}
                onRequestClose={this.closeModal}
                customStyle={{zIndex: modalIndex[modalId] }}
            >
                <div className="modal fadeIn is-active">
                    <div className="modal-header">
                        <span onClick={this.closeModal} className="close-button">×</span>
                    </div>
                    <div className="modal-content">
                        ConfirmRemoveModal

                        <div>
                            <button className={"btn btn-red"} onClick={this.confirmModal}>confirmModal</button>
                        </div>
                    </div>
                </div>

            </DefaultModal>
        );
    }
}

ConfirmRemoveModal.defaultProps = {
    modalId: "confirm_remove_modal"
};

export default class ConfirmRemoveModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modalIndex: () => ModalStore.getState().modalIndex,
                    modalsData: () => {
                        return ModalStore.getState().data["confirm_remove_modal"];
                    },
                    modals: () => ModalStore.getState().modals,
                    resolve: () => ModalStore.getState().resolve,
                }}
            >
                <ConfirmRemoveModal {...this.props} />
            </AltContainer>
        );
    }
}
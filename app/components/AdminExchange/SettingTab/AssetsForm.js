import React from "react";
import {Form, Select} from "antd";
import {cn} from "@bem-react/classname";
const cln = cn("SettingTab");
const {Option} = Select;

const AssetsForm = props => {

    const {getFieldDecorator} = props.form;
    return (
        <>
            <div className={cln("add-pairs-item", {"asset": true})}>
                <Form.Item>
                    {getFieldDecorator("assetFrom", {
                        validateTrigger: ["onChange"],
                        rules: [
                            {
                                required: true,
                                message: "Выбрать валюта продажи"
                            }
                        ]
                    })(
                        <Select {...props.selectedData} placeholder={"Валюта продажи"}>
                            {Object.values(props.settingsAsset).map(assets => {
                                console.log("assets", assets, props.currentPairs);
                                if (assets.id !== props.currentPairs.to) {
                                    return (
                                        <Option
                                            key={`setting-asset-option-from-${assets.assetId}`}
                                            type={"from"}
                                            value={assets.id}>
                                            {assets.assetName}
                                        </Option>
                                    );
                                }
                            })}
                        </Select>
                    )}
                </Form.Item>

                <Form.Item>
                    {getFieldDecorator("assetTo", {
                        validateTrigger: ["onChange"],
                        rules: [
                            {
                                required: true,
                                message: "Выбрать валюта покупки"
                            }
                        ]
                    })(
                        <Select {...props.selectedData} placeholder={"Валюта покупки"} id="">
                            {Object.values(props.settingsAsset).map(assets => {
                                if (assets.id !== props.currentPairs.from) {
                                    return (
                                        <Option
                                            key={`setting-asset-option-to-${assets.assetId}`}
                                            type={"to"}
                                            value={assets.id}>{assets.assetName}
                                        </Option>
                                    );
                                }
                            })}
                        </Select>
                    )}
                </Form.Item>
            </div>

        </>
    );
};

export default AssetsForm;

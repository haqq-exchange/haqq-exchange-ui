import React from "react";
import {Form} from "antd";
import AssetsForm from "Components/AdminExchange/SettingTab/AssetsForm";
import BodyForm from "Components/AdminExchange/SettingTab/BodyForm";
import {cn} from "@bem-react/classname";
const cln = cn("SettingTab");

class AddPairsToExchange extends React.Component {

    state = {
        currentPairs: {
            from: null,
            to: null
        },
        minimum: {
            from: null,
            to: null
        },
        maximum: {
            from: null,
            to: null
        }
    };

    handleChange = (value, event) => {
        let {currentPairs} = this.state;
        let {props} = event;
        currentPairs[props.type] = props.value;
        this.setState({currentPairs: currentPairs}, this.checkMarkets);
    };

    checkMarkets = () => {
        let {activeMarkets} = this.props;
        let {currentPairs} = this.state;
        let errorPairs = false;
        if( activeMarkets ) {
            activeMarkets.map(markets=>{
                if( errorPairs ) return false;
                errorPairs = markets.assetFrom === currentPairs.from && markets.assetTo === currentPairs.to;
                return errorPairs;
            });
            this.setState({errorPairs});
        }
    };

    handleSubmit = (event) => {
        event.preventDefault();
        const {currentAccount, getUserSign, refutils, settings} = this.props;

        const admin_exchange = settings?.get("admin_exchange");
        this.props.form.validateFields((errors, values) => {
            if (errors) {
                return false;
            }
            values.account_name = currentAccount;
            values.exchanger_id = admin_exchange.id;

            getUserSign(values)
                .then(sign => {
                    refutils.postTemplate("/control/addMarket", values, sign)
                        .then(res => {
                            console.log("res addMarket", res, this.props);
                            this.props.callBack(res.content);
                        });
                });
        });
    };

    render() {
        let {currentPairs, errorPairs} = this.state;

        const selectedData = {
            getPopupContainer: () => document.getElementById("pairs-wrap-to-exchange"),
            onChange: this.handleChange,
            placeholder: "Выберите ассет"
        };
        return (
            <div className={cln("add-pairs")}>
                <Form onSubmit={this.handleSubmit}>
                    {errorPairs ? <div className={cln("add-pairs-item", {"error": true})}>
                        Такая пара есть
                    </div> : null}

                    <div className={cln("add-pairs-wrap")}>

                        <AssetsForm
                            currentPairs={currentPairs}
                            selectedData={selectedData}
                            {...this.props}  />

                        <BodyForm
                            hasNewForm={true}
                            {...this.props} />
                    </div>
                </Form>
            </div>
        );
    }
}

const AddPairsToExchangeForm = Form.create({name: "dynamic_form_item"})(AddPairsToExchange);

export default AddPairsToExchangeForm;

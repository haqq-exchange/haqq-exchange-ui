import React from "react";
import {Form, Input} from "antd";
import ModalActions from "actions/ModalActions";
import ImageLoad from "Utility/ImageLoad";
import Translate from "react-translate-component";
import BodyForm from "Components/AdminExchange/SettingTab/BodyForm";
import {cn} from "@bem-react/classname";
const cln = cn("SettingTab");

export default class BodyPairsToExchange extends React.Component {

    render() {
        const {activeMarkets, settingsAsset} = this.props;
        if (!activeMarkets) return null;

        return (
            <div className={cln("body-pairs")}>
                {activeMarkets.map(markets => {
                    const assetFrom = settingsAsset[markets.assetFrom];
                    const assetTo = settingsAsset[markets.assetTo];
                    return (
                        <BodyPairsToExchangeForm
                            markets={markets}
                            assetFrom={assetFrom}
                            assetTo={assetTo}
                            key={`body-form-${markets.id}`} {...this.props} />
                    );
                })}
            </div>
        );
    }
}

class BodyPairsToExchangeFormItem extends React.Component {
    deletePairs = (id) => {
        const {currentAccount, getUserSign, refutils, settings} = this.props;

        const admin_exchange = settings?.get("admin_exchange");
        let sendData = {
            account_name: currentAccount,
            exchanger_id: admin_exchange.id,
            id
        };

        ModalActions.show("confirm_remove_modal", {

        }).then(()=>{
            //ModalActions.hide("add_deposit_node_modal");
            // console.log("confirmModal !!!!!");
            getUserSign(sendData)
                .then(sign => {
                    refutils.postTemplate("/control/removeMarket", sendData, sign)
                        .then(res => {
                            this.props.callBack(res.content);
                        });
                });
        }).catch(()=>{
        });
    };

    handleUpdate = event => {
        event.preventDefault();
        const {currentAccount, getUserSign, refutils, settings} = this.props;
        const admin_exchange = settings?.get("admin_exchange");
        this.props.form.validateFields((errors, values) => {
            if (errors) {
                return false;
            }
            values.account_name = currentAccount;
            values.exchanger_id = admin_exchange.id;

            getUserSign(values)
                .then(sign => {
                    refutils.postTemplate("/control/updateMarket", values, sign)
                        .then(res => {
                            console.log("res addMarket", res, this.props);
                            this.props.callBack(res.content);
                        });
                });
        });
    };

    render() {
        const {assetFrom, assetTo, markets} = this.props;
        const {getFieldDecorator} = this.props.form;

        if( !assetFrom || !assetTo ) return null;

        // console.log("assetTo", assetTo);
        // console.log("assetFrom", assetFrom);

        return (
            <Form key={`body-form-${markets.id}`} onSubmit={this.handleUpdate}>
                <span>
                    {getFieldDecorator("assetTo", {initialValue: markets?.assetTo})(<Input type={"hidden"}/>)}
                    {getFieldDecorator("assetFrom", {initialValue: markets?.assetFrom})(<Input type={"hidden"}/>)}
                    {getFieldDecorator("to", {initialValue: markets?.to})(<Input type={"hidden"}/>)}
                    {getFieldDecorator("from", {initialValue: markets?.from})(<Input type={"hidden"}/>)}
                    {getFieldDecorator("id", {initialValue: markets?.id})(<Input type={"hidden"}/>)}
                </span>
                <div className={cln("body-pairs-wrap")}>
                    <div className={cln("body-pairs-item", {"asset-name": true})}>
                        <Form.Item>
                            <ImageLoad
                                customStyle={{maxWidth: 20, marginRight: 10, verticalAlign: "sub"}}
                                imageName={`${assetFrom.assetFullName.toLowerCase()}.png`}/>
                            <span style={{
                                "display": "inline-flex",
                                "flexDirection": "column",
                                "lineHeight": "20px",
                            }}>
                                {assetFrom.assetName}
                                <Translate content={["admin-exchange","SettingTab", "sale"]}  style={{"fontSize": "12px"}}  />
                            </span>
                        </Form.Item>
                        <Form.Item>
                            <ImageLoad
                                customStyle={{maxWidth: 20, marginRight: 10, verticalAlign: "sub"}}
                                imageName={`${assetTo.assetFullName.toLowerCase()}.png`}/>
                            <span style={{
                                "display": "inline-flex",
                                "flexDirection": "column",
                                "lineHeight": "20px"
                            }}>
                                {assetTo.assetName}
                                <Translate content={["admin-exchange","SettingTab", "purchase"]}  style={{"fontSize": "12px"}}  />
                            </span>
                        </Form.Item>
                    </div>

                    <BodyForm
                        key={`body-form-market-${markets.id}`}
                        deletePairs={this.deletePairs}
                        markets={markets}
                        {...this.props} />
                </div>
            </Form>
        );
    }
}

const BodyPairsToExchangeForm = Form.create({name: "dynamic_update_form_item"})(BodyPairsToExchangeFormItem);

import React from "react";
import {cn} from "@bem-react/classname";
import Translate from "react-translate-component";
const cln = cn("SettingTab");

const HeaderTableForm = () => {
    return (
        <div className={cln("header-form")}>
            <Translate className={cln("header-form-item", {"name": true})} content={["admin-exchange", "SettingTab" , "header-name"]} component={"span"} />
            <Translate className={cln("header-form-item", {"min-max": true})} content={["admin-exchange", "SettingTab" , "header-min"]} component={"span"} />
            <Translate className={cln("header-form-item", {"min-max": true})} content={["admin-exchange", "SettingTab" , "header-max"]} component={"span"} />
            <Translate className={cln("header-form-item", {"course": true})} content={["admin-exchange", "SettingTab" , "header-course"]} component={"span"} />
            <Translate className={cln("header-form-item", {"activity": true})} content={["admin-exchange", "SettingTab" , "header-activity"]} component={"span"} />
        </div>
    );
};

export default HeaderTableForm;

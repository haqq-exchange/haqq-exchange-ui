import React from "react";
import {cn} from "@bem-react/classname";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";
import Icon from "Components/Icon/Icon";
import LinkToAccountById from "Utility/LinkToAccountById";
// import BlockTime from "Components/Blockchain/BlockTime";
// import BindToChainState from "Utility/BindToChainState";
// import PropTypes from "prop-types";
import counterpart from "counterpart";
import TranslateWithLinks from "Utility/TranslateWithLinks";
import {Asset} from "../../../lib/common/MarketClasses";

const cln = cn("Transaction");

export default class TransactionItem extends React.Component {
    render() {

        return(
            <div>
                <TransactionHeader
                    setSortBy={this.props.setSortBy}
                    params={this.props.params}/>
                <TransactionBody {...this.props}/>
            </div>
        );
    }
}

class TransactionBody extends React.Component {
    render() {
        const {transaction, assets, checkboxes} = this.props;
        if( !transaction ) return  null;
        console.log("TransactionBody", this.props);
        return (
            <div className={cln("body")}>
                {transaction.map((line, index) => {

                    return (
                        <div key={`line-${index}`} className={cln("line")}>
                            {Object.keys(line).map(item=>{
                                let currentItem = line[item];
                                let hasIn = item === "in";
                                let checkboxesItem = hasIn ? "input" : "output";
                                //let currentCheckboxesItem = checkboxes[checkboxesItem];
                                let currentAssetFrom = assets[currentItem.symbol];

                                if(!currentAssetFrom) return  null; /* владелец удалили пару */

                                let assetFrom = new Asset({
                                    amount: currentItem.amount,
                                    asset_id: currentAssetFrom.get("id"),
                                    precision: currentAssetFrom.get("precision")
                                });

                                //if( !currentCheckboxesItem.checked ) return  null;
                                return (
                                    <div key={`item-${item}`} className={cln("item")}>
                                        <div className={cln("line-item", {"icon": true})}>
                                            <Icon name={hasIn ? "deposit3" : "withdraw2"} />
                                        </div>
                                        <div className={cln("line-item", {"id": true})}>
                                            <LinkToAccount account={currentItem.from} /> <br/>
                                            <LinkToAccount account={currentItem.to} />
                                        </div>

                                        <div className={cln("line-item", {"date": true})}>{
                                            counterpart.localize(new Date(currentItem?.date), {
                                                type: "date",
                                                format: "full"
                                            })
                                        }</div>

                                        <div className={cln("line-item", {"amount-big": true})}>
                                            &nbsp; {assetFrom.getAmount({real: 1})}
                                        </div>
                                        <div className={cln("line-item", {"symbol": true})}>
                                            &nbsp; {currentItem.symbol}
                                        </div>
                                        <div className={cln("line-item", {"info": true})}>
                                            {currentItem.isError ? <StatusError
                                                currentItem={currentItem}
                                                {...this.props} /> : null}
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    );
                })}
            </div>
        );
    }
}

const LinkToAccount = props => {
    return <Link to={`/account/${props.account}`}>{props.account}</Link>;
};

const StatusError = props => {

    const sendComplaint = id => {
        const {currentAccount, getUserSign, catchUserSign, refutils, settings} = props;
        const admin_exchange = settings?.get("admin_exchange");
        let sendBody = {
            "account_name": currentAccount,
            "id": id,
            "exchanger_id": admin_exchange.id
        };
        getUserSign(sendBody).then(sign => {
            refutils
                .postTemplate("/control/sendComplaint", sendBody, sign)
                .then(({content})=>{
                    console.log("content", content);
                });
        }).catch(catchUserSign);
    };

    return (
        <div className={cln("status-error")}>
            <Translate content={["admin-exchange","transaction", "error"]}  />
            <span onClick={()=>sendComplaint(props.currentItem.id)}>
                <Icon name={"Mail2"} />
                <Translate content={["admin-exchange","transaction", "support"]}  />

            </span>
        </div>
    )
};

const StatusName = props => {

    const nameArrayStatus = {
        "status": [],
        "errorCode": {
        }
    };
    if( !props.type || !props.code ) return null;
    const nameStringStatus = nameArrayStatus[props.type][props.code] || props.code.toString();
    const classSuffix = props.code === 23 ? "good" : props.code === 24 ? "error" : "waiting";
    return (
        <Translate content={["admin-exchange","transaction", props.type, nameStringStatus]} fallback={nameStringStatus} className={cln("status-" + classSuffix)}   />
    );
};

const TransactionHeader = ({params, setSortBy}) => {

    const mySetSortBy = type => {

        let direction = ["ASC", "DESC"];
        let indexDirection = params.sortBy !== type ? 0
            : direction.indexOf(params.sortDirection) === 0 ? 1 : 0;
        params.sortDirection = direction[indexDirection] || "DESC";
        params.sortBy = type;
        setSortBy(params);
    };

    const getModsBlock = data => {
        let keys = Object.keys(data);
        if( keys.includes(params.sortBy) ) {
            data.sortBy = true;
            data.sortDirection = params.sortDirection;
        }
        return  data;
    };
    return (
        <div className={cln("header")}>
            <span className={cln("header-item", getModsBlock({"icon": true}))} />
            <span onClick={()=>mySetSortBy("from")} className={cln("header-item", getModsBlock({"from": true, "id": true, "sort": true}))}>
                <Translate content={["admin-exchange","transaction", "from_to"]} />
            </span>
            <span onClick={()=>mySetSortBy("date")} className={cln("header-item", getModsBlock({"date": true, "sort": true}))}>
                <Translate content={["admin-exchange","transaction", "date"]} />
            </span>
            <span onClick={()=>mySetSortBy("amount")} className={cln("header-item", getModsBlock({"amount": true, "amount-big": true, "sort": true}))}>
                <Translate content={["admin-exchange","transaction", "amount"]} />
            </span>
            <span className={cln("header-item", getModsBlock({"symbol": true}))} />
            <Translate content={["admin-exchange","transaction", "state"]} className={cln("header-item", getModsBlock({"info": true}))} />
        </div>
    );
};

import React from "react";
import {DatePicker, Input} from "antd";
import {cn} from "@bem-react/classname";

const cln = cn("Transaction");

export default class TransactionDatePicker extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            endOpen: false,
            ...props.dateRange
        };
    }


    disabledStartDate = startValue => {
        const { endValue } = this.state;
        if (!startValue || !endValue) {
            return false;
        }
        return startValue.valueOf() > endValue.valueOf();
    };

    disabledEndDate = endValue => {
        const { startValue } = this.state;
        if (!endValue || !startValue) {
            return false;
        }
        return endValue.valueOf() <= startValue.valueOf();
    };

    onChange = (field, value) => {

        this.setState({
            [field]: value,
        });

        this.props.callBackDate({
            field, value
        });
    };

    onStartChange = value => {
        this.onChange("startValue", value);
    };

    onEndChange = value => {
        this.onChange("endValue", value);
    };

    handleStartOpenChange = open => {
        if (!open) {
            this.setState({ endOpen: false });
        }
    };

    handleEndOpenChange = open => {
        this.setState({ endOpen: open });
    };

    render() {
        const { startValue, endValue, endOpen } = this.state;
        return (
            <div className={cln("date-picker")} >
                <div className={cln("date-picker-wrap")} id={"transaction-date-picker1"}>
                    <DatePicker
                        prefixCls={"Transaction-calendar"}
                        getCalendarContainer={()=>document.getElementById("transaction-date-picker1")}
                        disabledDate={this.disabledStartDate}
                        format="DD-MM-YYYY"
                        value={startValue}
                        placeholder="Start date"
                        onChange={this.onStartChange}
                        onOpenChange={this.handleStartOpenChange}
                        showToday={false}
                    />
                </div>
                <div className={cln("date-picker-wrap")} id={"transaction-date-picker2"}>
                    <DatePicker
                        prefixCls={"Transaction-calendar"}
                        getCalendarContainer={()=>document.getElementById("transaction-date-picker2")}
                        disabledDate={this.disabledEndDate}
                        format="DD-MM-YYYY"
                        value={endValue}
                        placeholder="End date"
                        onChange={this.onEndChange}
                        open={endOpen}
                        onOpenChange={this.handleEndOpenChange}
                        showToday={false}
                    />
                </div>
                <div className={cln("input-wrap")}>
                    <Input onChange={event=>this.props.setSortBy({
                        search: event.target.value
                    })} placeholder={'Search'} />
                </div>

            </div>
        );
    }
}

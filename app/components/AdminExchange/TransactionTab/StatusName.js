import Translate from "react-translate-component";
import React from "react";
import {cn} from "@bem-react/classname";
import counterpart from "counterpart";
const cln = cn("Transaction");

const StatusName = props => {

    const nameArrayStatus = {
        "status": [],
        "errorCode": {
        }
    };
    if( !props.type || !props.code ) return null;
    const nameStringStatus = nameArrayStatus[props.type][props.code] || props.code.toString();
    const classSuffix = props.code === 23 ? "good" : props.code === 24 ? "error" : "waiting";
    const keyTranslate = ["admin-exchange","transaction", props.type, nameStringStatus]
    // console.log("csv props", props);
    if( props.onlyText || true ) {
        //console.log("csv keyTranslate", keyTranslate, counterpart.translate(["admin-exchange", "transaction", "status", "23"] ))
        return  counterpart.translate(keyTranslate)
    }

    return (
        <Translate content={keyTranslate} fallback={nameStringStatus} className={cln("status-" + classSuffix)}   />
    );
};

export default StatusName;

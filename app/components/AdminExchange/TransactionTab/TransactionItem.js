import React from "react";
import {Link} from "react-router-dom";
import Popover from "react-popover";
import {cn} from "@bem-react/classname";
import Translate from "react-translate-component";
import Icon from "Components/Icon/Icon";
import counterpart from "counterpart";
import TranslateWithLinks from "Utility/TranslateWithLinks";
import {Asset} from "../../../lib/common/MarketClasses";
import FormattedAsset from "Utility/FormattedAsset";
import StatusName from "./StatusName";

const cln = cn("Transaction");

export default class TransactionItem extends React.Component {
    render() {

        return(
            <div>
                <TransactionHeader />
                <TransactionBody {...this.props}/>
            </div>
        );
    }
}

class TransactionBody extends React.Component {

    state = {
        isShowPopUp: {}
    };

    sendReplay = id => {
        const {currentAccount, getUserSign, catchUserSign, refutils, settings} = this.props;
        const admin_exchange = settings?.get("admin_exchange");
        let sendBody = {
            "account_name": currentAccount,
            "exchanger_id": admin_exchange.id,
            "id": id,
        };
        getUserSign(sendBody).then(sign => {
            refutils
                .postTemplate("/control/replayTransaction", sendBody, sign)
                .then(({content})=>{
                    console.log("content", content);
                    this.props.getHistory();
                });
        }).catch(catchUserSign);
    };

    hasIsOpenPopOver = item => {
        const {isShowPopUp} = this.state;
        return isShowPopUp[item.id] || false;
    };
    closePopover = item => {
        let {isShowPopUp} = this.state;
        isShowPopUp[item.id] = false;
        this.setState({isShowPopUp})
    };
    openPopover = item => {
        let {isShowPopUp} = this.state;
        isShowPopUp[item.id] = true;
        this.setState({isShowPopUp});
    };
    togglePopover = item => {
        let {isShowPopUp} = this.state;
        isShowPopUp[item.id] = !isShowPopUp[item.id];
        this.setState({isShowPopUp});
    };

    render() {
        const {isShowPopUp} = this.state;
        const {transaction, assets, checkboxes} = this.props;
        if( !transaction ) return  null;
        console.log("TransactionBody!!", this.props);
        return (
            <div className={cln("body")}>
                {transaction.map((line, index) => {
                    return (
                        <div key={`line-${index}`} className={cln("line")}>
                            {["in", "out"].map(item=>{
                                let currentItem = line[item];
                                let summary = line["summary"];
                                let lineIn = line["in"];
                                let lineOut = line["out"];
                                let hasIn = item === "in";
                                let checkboxesItem = hasIn ? "input" : "output";
                                let currentCheckboxesItem = checkboxes[checkboxesItem];
                                let currentAssetFrom = assets[currentItem.asset_from];
                                let currentAssetTo = assets[currentItem.asset_to];

                                // console.log("summary", summary);
                                // console.log("currentAssetTo", currentAssetTo);
                                // console.log("currentAssetFrom", currentAssetFrom);

                                if(!currentAssetTo || !currentAssetFrom) return  <EmptyLinet
                                    currentItem={!hasIn ? lineIn : lineOut}
                                    hasIn={hasIn}
                                    key={`item-${item}`} />; /* владелец удалили пару */

                                let assetFrom = new Asset({
                                    amount: lineIn.amount,
                                    asset_id: currentAssetFrom.get("id"),
                                    precision: currentAssetFrom.get("precision")
                                });

                                let assetTo = new Asset({
                                    amount: lineOut.amount,
                                    asset_id: currentAssetTo.get("id"),
                                    precision: currentAssetTo.get("precision")
                                });
                                const popover_body = <TranslateWithLinks
                                    string="operation.limit_order_buy"
                                    subpage={"activity"}
                                    keys={[
                                        {
                                            type: "account",
                                            value: currentItem.from,
                                            arg: "account"
                                        },
                                        {
                                            type: "amount",
                                            value: assetTo,
                                            arg: "amount",
                                            decimalOffset: null
                                        },
                                        {
                                            type: "amount",
                                            value: assetFrom,
                                            arg: "price"
                                        }
                                    ]}
                                />;
                                if( !currentCheckboxesItem.checked ) return  null;
                                return (
                                    <div key={`item-${item}`} className={cln("item")}>
                                        <div className={cln("line-item", {"icon": true})}>
                                            <Icon name={hasIn ? "deposit3" : "withdraw2"} />
                                        </div>
                                        <div className={cln("line-item", {"id": true})}>
                                            {hasIn ? currentItem.from : <LinkToAccount account={currentItem.to} />}
                                        </div>

                                        <div className={cln("line-item", {"date": true})}>{
                                            counterpart.localize(new Date(currentItem?.date), {
                                                type: "date",
                                                format: "full"
                                            })
                                        }</div>

                                        <div className={cln("line-item", {"amount": true})}>
                                            {hasIn ? assetFrom.getAmount({real: 1}) : assetTo.getAmount({real: 1})}
                                            &nbsp; {hasIn ? currentItem.asset_alias_from : currentItem.asset_alias_to}
                                        </div>


                                        <div className={cln("line-item", {
                                            "code": true,
                                            "canReplay": currentItem.canReplay
                                        })}>
                                            {hasIn ? currentItem.exchangeRate : <StatusName
                                                type={currentItem.errCode ? "errorCode" : "status"}
                                                code={currentItem.errCode || currentItem.status} />}

                                            {currentItem.canReplay ? <>
                                                    <Translate content={["admin-exchange","transaction", "replay"]} className={cln("replay")} onClick={()=>this.sendReplay(currentItem.id)} component={"div"}  />
                                                </> : ""}
                                        </div>
                                        <div className={cln("line-item", {"code": true})}>
                                            {hasIn ? summary.exchangeRateOriginal || "0" : null}
                                        </div>
                                        <div className={cln("line-item", {"code": true})}>
                                            {hasIn ? summary.exchangeSubRate : null}
                                        </div>
                                        <div className={cln("line-item", {"code": true})}>
                                            {hasIn ? summary.full_exchange_fee + " DEEX" : null}
                                        </div>
                                        <div className={cln("line-item", {"i": true})}>


                                            {hasIn ? <Popover
                                                appendTarget={document.getElementById("content-wrapper")}
                                                className={"referrals-table-popover"}
                                                place={"below"}
                                                preferPlace={"below"}
                                                isOpen={this.hasIsOpenPopOver(currentItem)}
                                                onOuterAction={()=>this.closePopover(currentItem)}
                                                body={popover_body} >
                                                <Icon
                                                    style={{marginLeft: 5}}
                                                    name="info_outline"
                                                    onClick={()=>this.togglePopover(currentItem)}
                                                />
                                            </Popover>: null}
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    );
                })}
            </div>
        );
    }
}


const LinkToAccount = props => {
    return <Link to={`/account/${props.account}`}>{props.account}</Link>;
};
const EmptyLinet = props => {
    return <div className={cln("item")}>
        <div className={cln("line-item")}>
        </div>
        <div className={cln("line-item", {"icon": true})}>
            <Icon name={props.hasIn ? "deposit3" : "withdraw2"} />
        </div>
        <div className={cln("line-item", {"id": true})} />
        <div className={cln("line-item", {"date": true})} />
        <div className={cln("line-item", {"amount": true})} />
        <div className={cln("line-item", {"code": true})}>
            {props.hasIn ? props.currentItem.exchangeRate : <StatusName
                type={props.currentItem.errCode ? "errorCode" : "status"}
                code={props.currentItem.errCode || props.currentItem.status} />}
        </div>
        <div className={cln("line-item", {"code": true})} />
        <div className={cln("line-item", {"code": true})} />
        <div className={cln("line-item", {"i": true})} />
    </div>;
};

const TransactionHeader = () => {
    return (
        <div className={cln("header")}>
            <span className={cln("header-item", {"icon": true})} />
            <Translate content={["admin-exchange","transaction", "id_operation"]} className={cln("header-item", {"id": true})}  />
            <Translate content={["admin-exchange","transaction", "date"]} className={cln("header-item", {"date": true})}  />
            <Translate content={["admin-exchange","transaction", "amount"]} className={cln("header-item", {"amount": true})}  />
            <Translate content={["admin-exchange","transaction", "course_state"]} className={cln("header-item", {"code": true})}  />
            <Translate content={["admin-exchange","transaction", "course_exchange"]} className={cln("header-item", {"code": true})}  />
            <Translate content={["admin-exchange","transaction", "course_cb"]} className={cln("header-item", {"code": true})}  />
            <Translate content={["admin-exchange","transaction", "fee_exchange"]} className={cln("header-item", {"code": true})}  />
            <span className={cln("header-item", {"i": true})} />
            {/*<span className={cln("header-item", {"course": true})}>Курс</span>*/}
        </div>
    );
};

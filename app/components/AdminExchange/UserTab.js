import React from "react";
import UserTabInfo from "./UserTab/UserTabInfo";
import UserTabTable from "./UserTab/UserTabTable";
import {cn} from "@bem-react/classname";
import {debounce} from "lodash";
import AdminExchangeActions from "actions/AdminExchangeActions";
const cln = cn("UserTab");

export default class UserTab extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            settingPagination: {
                page: 1,
                sort_by: "referrals desc",
                pageSize: 20,
                totalReferral: 0
            }
        };

        this.searchAccount = debounce(this.searchAccount, 500);

    }

    componentDidMount() {
        this.setInitPagination();
    }

    componentDidUpdate(prevProps) {
        if( prevProps.referralInfo?.users !== this.props.referralInfo?.users ) {
            this.setInitPagination();
        }
        if( this.props.sort !== prevProps?.sort ) {
            this.getUserTree();
        }
    }

    setInitPagination = (totalReferral) =>{
        let {settingPagination} = this.state;
        if( settingPagination.totalReferral !== totalReferral || !settingPagination.totalReferral ) {
            settingPagination.totalReferral = totalReferral || this.props.referralInfo?.users;
            this.setState({
                settingPagination
            }, this.getUserTree);
        }
    };

    getStatByLogin = (referralUsers) => {
        const {currentAccount, getUserSign, catchUserSign, refutils, settings} = this.props;
        if( referralUsers.length ) {
            const admin_exchange = settings?.get("admin_exchange");
            const sendBody = {
                account_name:  currentAccount,
                exchanger_id: admin_exchange.id,
                logins: referralUsers.map(users=>users.name)
            };
            getUserSign(sendBody).then(sign => {
                refutils
                    .postTemplate("/control/stat/getStatByLogin", sendBody, sign)
                    .then(({content})=>{
                        if( content.success ) {
                            new Promise(resolve=>{
                                resolve(referralUsers
                                    .map(user=> Object.assign(user, content.data[user.name])));
                            }).then(nextUsersState=>{
                                console.log("nextUsersState", nextUsersState);
                                AdminExchangeActions.setReferralUsers(nextUsersState);
                            });
                        }
                    });
            }).catch(catchUserSign);
        }
    };

    getUserTree = () => {
        const {settingPagination} = this.state;
        const {currentAccount, getUserSign, catchUserSign, referralApi, referralInfo, settings, sort} = this.props;

        console.log("getUserTree", referralInfo);

        if(referralInfo?.users) {
            let sendBody = {
                "account": currentAccount,
                "root_account": currentAccount,
                "page-size": settingPagination.pageSize,
                "page": settingPagination.page,
                "system": settings.get("admin_exchange")["servicePrefix"],
                "filter": settingPagination.filter,
                ...sort,
            };
            getUserSign(sendBody).then(sign => {
                referralApi
                    .postTemplate("/ref/get_users", sendBody, sign)
                    .then(({content})=>{
                        this.getStatByLogin(content.users);
                        this.setInitPagination(content.total);
                    });
            }).catch(catchUserSign);
        }
    };


    changePagination = (page, pageSize) => {
        console.log("changePagination");
        let {settingPagination} = this.state;
        settingPagination.page = page;
        settingPagination.pageSize = pageSize;

        this.setState({
            settingPagination
        }, this.getUserTree);
    };

    searchAccount = target => {
        let {settingPagination} = this.state;
        settingPagination.filter = target.value || "";
        settingPagination.page = 1;
        this.setState({
            settingPagination
        }, this.getUserTree);
    };

    render() {
        const {referralUsers} = this.props;

        console.log("referralUsers", referralUsers);

        return(
            <div className={cln()}>
                <UserTabInfo {...this.props}/>
                {referralUsers ? <UserTabTable
                    searchAccount={this.searchAccount}
                    changePagination={this.changePagination}
                    settingPagination={this.state.settingPagination}
                    {...this.props} /> : null}
            </div>
        );
    }
}

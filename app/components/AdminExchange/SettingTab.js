import React from "react";
import Translate from "react-translate-component";
import {checkFeeStatusAsync} from "lib/common/trxHelper";
import {Asset} from "lib/common/MarketClasses";
import {cn} from "@bem-react/classname";
import ConfirmRemoveModal from "./SettingTab/ConfirmRemoveModal";
import HeaderTableForm from "./SettingTab/HeaderTableForm";
import BodyPairsToExchange from "./SettingTab/BodyPairsToExchange";
import AddPairsToExchangeForm from "./SettingTab/AddPairsToExchange";
import SettingsActions from "../../actions/SettingsActions";

const cln = cn("SettingTab");

export default class SettingTab extends React.Component {

    state = {
        showAddPairs: false,
        marketsRates:[]
    };

    constructor(props){
        super(props);

        this.marketsRates = null;
    }

    componentDidMount() {
        this.getMarkets();
        this.getExchangeSources();
        this.getTransfer();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.asset !== this.props.asset) {
            this.getTransfer();
        }
    }

    componentWillUnmount() {
        this.endGetMarketsRates();
    }

    getAssets = () => {
        const _this = this;
        let {settingsAsset = {}} = this.state;
        const {currentAccount, getUserSign, refutils, settings} = this.props;
        const admin_exchange = settings?.get("admin_exchange");
        let dataSend = {
            account_name: currentAccount,
            exchanger_id: admin_exchange.id
        };
        getUserSign(dataSend)
            .then(sign => {
                refutils.postTemplate("/control/getAssets", dataSend, sign)
                    .then(result => {
                        try {
                            const content = result.content;
                            content.data.map(asset => {
                                if (asset) {
                                    settingsAsset[asset.id] = asset;
                                }
                            });
                            _this.setState({settingsAsset}, this.getMarkets);
                        } catch (e) {
                            console.log(e);
                        }
                    });
            });
    };

    getMarkets = () => {
        const _this = this;
        const {currentAccount, getUserSign, refutils, settings} = this.props;
        const admin_exchange = settings?.get("admin_exchange");
        let dataSend = {
            account_name: currentAccount,
            exchanger_id: admin_exchange.id
        };
        getUserSign(dataSend)
            .then(sign => {
                refutils.postTemplate("/control/getMarkets", dataSend, sign)
                    .then(result => {
                        const content = result.content;
                        _this.endGetMarketsRates();
                        _this.setState({
                            activeMarkets: content.data
                        }, _this.startGetMarketsRates);
                    });
            });
    };

    endGetMarketsRates = () => {
        if( this.marketsRates ) {
            clearInterval(this.marketsRates);
        }
    }

    startGetMarketsRates = () => {
        this.getMarketsRates();
        this.marketsRates = setInterval(()=>this.getMarketsRates(), 20 * 1000);
    };

    getMarketsRates = () => {
        const _this = this;
        const {currentAccount, getUserSign, refutils, settings} = this.props;
        const admin_exchange = settings?.get("admin_exchange");
        let dataSend = {
            account_name: currentAccount,
            exchanger_id: admin_exchange.id
        };
        getUserSign(dataSend)
            .then(sign => {
                refutils.postTemplate("/control/getMarketsRates", dataSend, sign)
                    .then(({content}) => {
                        //let marketsRates = {};
                        /*content.data.map(item=>{
                            marketsRates[item.id] = item;
                        });*/
                        // console.log("content", marketsRates);
                        _this.setState({
                            marketsRates: content.data
                        });
                    });
            });
    };

    getExchangeSources = () => {
        const _this = this;
        const {currentAccount, getUserSign, refutils , settings} = this.props;
        const admin_exchange = settings?.get("admin_exchange");
        let dataSend = {
            account_name: currentAccount,
            exchanger_id: admin_exchange.id
        };
        getUserSign(dataSend)
            .then(sign => {
                refutils.postTemplate("/control/getExchangeSources", dataSend, sign)
                    .then(result => {
                        const content = result.content;
                        _this.setState({
                            exchangeSources: content.data
                        });
                    });
            });
    };

    getTransfer = () => {
        const {account, asset} = this.props;
        if (asset && asset?.size && account?.has("id")) {
            checkFeeStatusAsync({
                accountID: account.get("id"),
                feeID: asset.get("id")
            }).then(result => {
                let feeAsset = new Asset(result.fee);
                this.setState({feeAsset});
            });
        }
    };

    onChangeAddPairs = () => {
        this.getMarkets();
        this.props.hasExchanger();
        this.setState({
            showAddPairs: false
        });
    };

    render() {
        const {activeMarkets, feeAsset, showAddPairs, exchangeSources, marketsRates} = this.state;
        console.log("SettingTab.js this.props", this.props);
        const {exchangeAssets} = this.props;
        if (!exchangeAssets) return null;
        let hasActiveMarkets = !!activeMarkets?.length;

        return (
            <div className={cln()} id={"pairs-wrap-to-exchange"}>
                <div className={cln("top")}>
                    {feeAsset ? <Translate content={["admin-exchange","SettingTab", "fee-transaction"]} component={"span"} value={feeAsset.getAmount({real: true})} asset={__GBL_CHAIN__ ? "GBL" : "DEEX"}  /> : null}

                    <Translate
                        className={"btn btn-blue"}
                        onClick={() => this.setState({showAddPairs: true})}
                        content={["admin-exchange","SettingTab", "add-pairs"]} component={"button"} type={"button"}  />
                </div>
                <HeaderTableForm {...this.props}  />
                {/* Форма добавления пары */}
                {showAddPairs || !hasActiveMarkets
                    ? <AddPairsToExchangeForm
                        callBack={this.onChangeAddPairs}
                        activeMarkets={activeMarkets}
                        exchangeSources={exchangeSources}
                        settingsAsset={exchangeAssets} {...this.props}  />
                    : null}
                {/* Форма изменения пар */}
                {hasActiveMarkets ? <BodyPairsToExchange
                    callBack={this.onChangeAddPairs}
                    marketsRates={marketsRates}
                    activeMarkets={activeMarkets}
                    exchangeSources={exchangeSources}
                    settingsAsset={exchangeAssets} {...this.props} /> : null}

                {/* Модалка подтверждения удаления */}
                <ConfirmRemoveModal />
            </div>
        );
    }
}


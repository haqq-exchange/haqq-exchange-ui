import React from "react";
import {diff} from "deep-object-diff";
import {FetchChain} from "deexjs";
import Translate from "react-translate-component";
import {Asset} from "../../../lib/common/MarketClasses";
import ValueStore from "Utility/ValueStore";
import {cn} from "@bem-react/classname";
import AdminExchangeActions from "../../../actions/AdminExchangeActions";
const cln = cn("Referral");
class ReferralInfo extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            hasShort: true,
            re_surrect_withdrawals: false,
            totalAssetValue: {},
            remaining_amounts: {},
            failed_withdrawals: {},
            succeeded_withdrawals: {}
        };
    }

    componentDidMount(){
        this.getTotalValue();
    }
    componentDidUpdate(prevProps){
        let diffReferrals = diff(prevProps.referralInfo, this.props.referralInfo);
        if(Object.keys(diffReferrals).length) {
            this.getTotalValue();
        }
    }

    getTotalValue = () => {
        ["succeeded_withdrawals","remaining_ref_amounts", "failed_withdrawals"].map(name => {
            let referralInfo = this.props?.referralInfo?.[name];
            if( referralInfo && referralInfo.length > 0 ) this.setTotalValueWithName(name, referralInfo);
        });
    };

    setTotalValueWithName = (name, data) => {
        let {totalAssetValue} = this.state;
        let assetObject = {};
        totalAssetValue[name] = {
            balances: [],
            fromAssets: []
        };
        data.map(total=>assetObject[total.currency] = total.amount);
        FetchChain("getAsset", Object.keys(assetObject)).then(assets => {
            assets.map(assetItem => {
                let asset = new Asset({
                    real: assetObject[assetItem.get("symbol")],
                    asset_id: assetItem.get("id"),
                    precision: assetItem.get("precision")
                });
                totalAssetValue[name].fromAssets.push(assetItem.get("id"));
                totalAssetValue[name].balances.push({
                    asset_id: assetItem.get("id"),
                    precision: assetItem.get("precision"),
                    symbol: assetItem.get("symbol"),
                    amount: parseInt(asset.getAmount(), 10)
                });
            });
            this.setState({totalAssetValue});
        });
    };

    reSurrectWithdrawals = () => {
        const {re_surrect_withdrawals} = this.state;
        const {currentAccount, getUserSign, referralApi, settings} = this.props;
        console.log("admin_exchange", settings.get("admin_exchange")["servicePrefix"], settings.getIn(["admin_exchange"]));
        if( !re_surrect_withdrawals ) {
            const sendData = {
                account: currentAccount,
                system: settings?.get("admin_exchange")?.["servicePrefix"]
            };
            getUserSign(sendData).then(sign => {
                referralApi.postTemplate("/ref/resurrect_withdrawals", sendData, sign)
                    .then(({content})=>{
                        this.setState({re_surrect_withdrawals: true});
                    });
            }).catch(this.catchUserSign);
        }
    };



    render() {
        const {totalAssetValue, hasShort, re_surrect_withdrawals} = this.state;
        return (
            <div className={cln()}>

                <Translate
                    component={"div"}
                    className={cln("title")}
                    content={["admin-exchange", "InfoTab" , "referral", "title"]}
                    dataSpan={<Translate component={"small"} className={cln("toggle-info")} onClick={()=>this.setState({hasShort: !hasShort})} content={["admin-exchange", "InfoTab" , "referral", !hasShort ? "short" : "detailed"]} />}
                />
                <div className={cln("list")}>
                    {Object.keys(totalAssetValue).map(assetName=>{
                        const hasReTransfer = assetName === "failed_withdrawals";
                        let dataStoreValue =  totalAssetValue[assetName];

                        return (
                            <div key={assetName} className={cln("item")}>
                                <div className={cln("label")}>
                                    <Translate content={["admin-exchange", "InfoTab" , "referral", assetName]} component={"div"} />
                                    {hasReTransfer ?
                                        !re_surrect_withdrawals ? <Translate
                                            className={cln("link")}
                                            onClick={()=>this.reSurrectWithdrawals()}
                                            content={["admin-exchange", "InfoTab" , "referral", "re_transfer"]}
                                            component={"span"} /> : <Translate
                                            content={["admin-exchange", "InfoTab" , "referral", "re_surrect"]}
                                            component={"span"} /> : null }
                                </div>
                                <div className={cln("values")}>
                                    {hasShort ?
                                        <ValueStore
                                            noTip={true}
                                            className={cln("values-store")}
                                            hide_asset={false}
                                            inHeader={false}
                                            preferredUnit={"USD"}
                                            balances={dataStoreValue.balances}
                                            fromAssets={dataStoreValue.fromAssets}
                                        /> : <ListBalances {...dataStoreValue} /> }
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        );
    }
}

const ListBalances = props => {
    return (
        props.balances.map(asset=>{
            let assetItem = new Asset({
                asset_id: asset.asset_id,
                precision: asset.precision
            });
            assetItem.setAmount({sats: asset.amount});
            return (
                <div key={`list-balances-${asset.asset_id}`} className={cln("balance")}>
                    {assetItem.getAmount({real: true})} <span>{asset.symbol}</span>
                </div>
            );
        })
    );
};

export default ReferralInfo;

import React from "react";
import Translate from "react-translate-component";
import InfoTabItem from "./InfoTabItem";
import ReferralInfo from "./ReferralInfo";
import {cn} from "@bem-react/classname";
import SettingsActions from "../../../actions/SettingsActions";
const cln = cn("InfoTab");

export default class InfoTab extends React.Component {

    toggleWorkStatus = (status) => {
        const _this = this;
        const {currentAccount, getUserSign, refutils, settings} = this.props;

        const admin_exchange = settings?.get("admin_exchange");
        const dataSend = {
            account_name: currentAccount,
            exchanger_id: admin_exchange.id,
            status
        };
        getUserSign(dataSend)
            .then(sign => {
                refutils.postTemplate("/control/updateExchanger", dataSend, sign)
                    .then(result => {
                        const {content} = result;
                        SettingsActions.changeSetting({
                            setting: "admin_exchange",
                            value: content.data
                        });
                    });
            });
    };

    render() {
        const {referralInfo, settings} = this.props;
        const admin_exchange = settings.get("admin_exchange");
        // console.log("this.props", this.props);
        // console.log("settings", settings.toJS());
        let elementNameMods = admin_exchange.active ? "status-active" : "status-not-active";
        let translName = admin_exchange.active ? "exchange-off" : "exchange-on";

        return(
            <div className={cln()}>
                <div className={cln("status")}>
                    <Translate content={["admin-exchange", "InfoTab" , "status"]} component={"span"} />
                    &nbsp;<Translate className={cln(elementNameMods)} content={["admin-exchange", "InfoTab" , elementNameMods]} component={"span"} />
                    &nbsp;
                    <Translate
                        onClick={()=>this.toggleWorkStatus(!admin_exchange.active)}
                        className={cln("status-link")} content={["admin-exchange", "InfoTab" , translName]} component={"span"} />
                </div>
                <div className={cln("referral")}>
                    <ReferralInfo {...this.props} />
                </div>
                <div className={cln("list")}>
                    <InfoTabItem count={referralInfo?.users || "0"} type={"all-users"} activeTab={1} />
                    <InfoTabItem count={admin_exchange?.stat?.totalOperations || "0"} type={"all-exchanges"} activeTab={3} />
                    <InfoTabItem count={admin_exchange?.stat?.totalMarkets || "0"} type={"all-pair"} activeTab={2} />
                </div>
            </div>
        );
    }
}



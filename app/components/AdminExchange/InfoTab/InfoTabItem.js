import React from "react";
//import cln from "classnames";
import Translate from "react-translate-component";
import {cn} from "@bem-react/classname";
import SettingsActions from "../../../actions/SettingsActions";
const cln = cn("InfoTab");

const InfoTabItem = props => {

    return (
        <div className={cln("item", {
            [props.type]: true,
            "has-click": !!props.activeTab
        })}>
            {props.count ? <div className={cln("item-count")} onClick={()=>{
                if( props.activeTab )
                    SettingsActions.changeViewSetting({
                        "adminExchange": props.activeTab
                    });
            }}>
                {props.count}
                <Translate content={["admin-exchange",props.type]}  />
            </div> : props.children}
        </div>
    );
};

export default InfoTabItem;

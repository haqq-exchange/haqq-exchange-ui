import React from "react";
import {Link} from "react-router-dom";
import ReferralInfo from "../InfoTab/ReferralInfo";

import {cn} from "@bem-react/classname";
import {debounce} from "lodash";
import {Input, Pagination} from "antd";
import Translate from "react-translate-component";
const cln = cn("Referral");


export default class ReferralsIndex extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            total: 0,
            withdrawals: null,
            settingPagination: {
                "page": 1,
                "sort_by": "name desc",
                "filter": "",
                "page-size": 20
            }
        };

        this.searchAccount = debounce(this.searchAccount, 500);
    }

    componentDidMount() {
        this.getUserTree();
    }

    componentDidUpdate(prevProps) {
        if( prevProps.referralInfo?.users !== this.props.referralInfo?.users ) {
            this.getUserTree();
        }
    }

    getUserTree = () => {
        const {settingPagination} = this.state;
        const {currentAccount, getUserSign, catchUserSign, referralApi, referralInfo, settings, sort} = this.props;

        //console.log("getUserTree");

        if(referralInfo?.users) {
            let sendBody = {
                "account": currentAccount,
                "system": settings.get("admin_exchange")["servicePrefix"],
                ...settingPagination,
            };
            getUserSign(sendBody).then(sign => {
                referralApi
                    .postTemplate("/ref/list_system_withdrawals", sendBody, sign)
                    .then(({content})=>{
                        // this.getStatByLogin(content.users);
                        // this.setInitPagination(content.total);
                        this.setState(content)
                    });
            }).catch(catchUserSign);
        }
    };


    changePagination = (page, pageSize) => {
        let {settingPagination} = this.state;
        settingPagination["page"] = page;
        settingPagination["page-size"] = pageSize;

        this.setState({
            settingPagination
        }, this.getUserTree);
    };

    changeSortBy = (sort_by) => {
        let {settingPagination} = this.state;
        settingPagination.sort_by = sort_by;
        this.setState({
            settingPagination
        }, this.getUserTree);
    };

    searchAccount = target => {
        console.log("searchAccount");
        let {settingPagination} = this.state;
        settingPagination.filter = target.value || "";
        settingPagination.page = 1;
        console.log("settingPagination", settingPagination);
        this.setState({
            settingPagination
        }, this.getUserTree);
    };


    render() {
        const {withdrawals, settingPagination, totals} = this.state;
        return (
            <div className={cln("wrapper")}>
                <ReferralInfo {...this.props} />

                <UserTableSearch searchAccount={this.searchAccount} />

                <WithdrawalsHeader
                    changeSortBy={this.changeSortBy}
                    {...settingPagination} />

                <div className={cln("table-body")}>
                    {withdrawals ? withdrawals.map((withdrawals, index)=><Withdrawals key={`withdrawals-${index}`} {...withdrawals} id={index+1}/>) : null}
                </div>

                <div className={cln("table-footer")}>
                    <Pagination
                        current={settingPagination.page}
                        pageSize={settingPagination["page-size"]}
                        defaultCurrent={1}

                        /*showSizeChanger*/
                        hideOnSinglePage={!totals || totals<=settingPagination["page-size"]}
                        pageSizeOptions={["10","20","30","40","50"]}
                        onChange={this.changePagination}
                        total={totals} />
                </div>
            </div>
        );
    }
}

const UserTableSearch = props => {
    return (
        <div className={cln("table-search")}>
            <div className={cln("table-search-wrap")}>
                <Input type={"search"} placeholder={"Search name"} onChange={event=>props.searchAccount(event.target)} />
            </div>
        </div>
    );
};

const Withdrawals = props =>{
    return (
        <div className={cln("withdrawals-line")}>
            <div className={cln("withdrawals-item", {id: true})}>{props.id}</div>
            <div className={cln("withdrawals-item", {dates: true})}>{props.eventDT}</div>
            <div className={cln("withdrawals-item", {account: true})}>
                <Link to={`/account/${props.account}`}>{props.account}</Link>
            </div>
            <div className={cln("withdrawals-item", {info: true})} >
                {props.amounts.map(amounts=>{
                    return (
                        <div className={cln("withdrawals-info")} >
                            <div className={cln("withdrawals-info-item", {amount: true})}>{amounts.amount}</div>
                            <div className={cln("withdrawals-info-item", {currency: true})}>{amounts.currency}</div>
                            <div className={cln("withdrawals-info-item", {state: true})}>{amounts.state.name}</div>
                            <div className={cln("withdrawals-info-item", {block_num: true})}>
                                {amounts.block_num ? <Link to={`/block/${amounts.block_num}`}>#{amounts.block_num}</Link> : null}
                            </div>
                        </div>
                    )
                })}
            </div>
        </div>
    )
};


const WithdrawalsHeader = props => {
    let sort_by = props.sort_by.split(" ");
    let hasDate = sort_by.includes("date");
    let hasName = sort_by.includes("name");


    return (
        <div className={cln("withdrawals-header-line")}>
            <span className={cln("withdrawals-header-item", {id: true})}>#</span>
            <span onClick={()=>props.changeSortBy(props.sort_by.indexOf("date desc") !== -1 ? "date" : "date desc")}
                  className={cln("withdrawals-header-item", {
                      dates: true,
                      [sort_by.join(" ")]: hasDate
                  })}>

                <Translate content={["admin-exchange", "Referrals" , "date"]} />
            </span>
            <span  onClick={()=>props.changeSortBy(props.sort_by.indexOf("name desc") !== -1 ? "name" : "name desc")}
                   className={cln("withdrawals-header-item", {
                       account: true,
                       [sort_by.join(" ")]: hasName
                   })}>

                <Translate content={["admin-exchange", "Referrals" , "login"]} />
            </span>

            <div className={cln("withdrawals-header-item", {info: true})} >
                <div className={cln("withdrawals-header-info")} >
                    <Translate className={cln("withdrawals-header-info-item", {"amount": true})} content={["admin-exchange", "Referrals" , "amount"]} />
                    <Translate className={cln("withdrawals-header-info-item", {"currency": true})} content={["admin-exchange", "Referrals" , "currency"]} />
                    <Translate className={cln("withdrawals-header-info-item", {"state": true})} content={["admin-exchange", "Referrals" , "state"]} />
                    <Translate className={cln("withdrawals-header-info-item", {"block_num": true})} content={["admin-exchange", "Referrals" , "block_num"]} />
                </div>
            </div>
        </div>
    )
};


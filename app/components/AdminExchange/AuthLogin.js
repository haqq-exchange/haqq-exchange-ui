import React from "react";
import ModalActions from "actions/ModalActions";
import Translate from "react-translate-component";
import {cn} from "@bem-react/classname";


const cln = cn("AuthLogin");

export default class AuthLogin extends React.Component {

    showModalLogin = event => {
        event.preventDefault();

        ModalActions.show("unlock_wallet_modal_public").then(() => {
            if(this?.props?.setNodeStatus) this.props.setNodeStatus("loading");
        });
    };

    render(){
        return(
            <div className={cln()}>
                <div className={cln("block")}>
                    <div className={cln("wrap")}>
                        <Translate className={cln("title")} component={"div"} content={"admin-exchange.AuthLogin.title"}/>
                        <Translate className={cln("unlock-info")} component={"div"} content={"admin-exchange.AuthLogin.login_info"}/>
                        <button type={"button"} onClick={this.showModalLogin} className={"btn btn-red btn-auth"} >
                            <Translate content={"header.unlock_short"}/>
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}


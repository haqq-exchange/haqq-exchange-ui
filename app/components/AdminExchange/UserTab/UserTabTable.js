import React from "react";

import {cn} from "@bem-react/classname";
import ChainTypes from "Utility/ChainTypes";
import BindToChainState from "Utility/BindToChainState";
import BlockTime from "Components/Blockchain/BlockTime";
import ClickOutside from "react-click-outside";
import AdminExchangeActions from "actions/AdminExchangeActions";
import {getAlias} from "config/alias";
import BalanceComponent from "Utility/BalanceComponent";
import LinkToAccountById from "Utility/LinkToAccountById";
import {Pagination, Input} from "antd";
import Translate from "react-translate-component";
const cln = cn("UserTab");

export default class UserTabTable extends React.Component {

    componentDidMount() {
        this.setSortTables();
    }

    componentDidUpdate() {
        const {sortTableCurrency} = this.props;
        if( !sortTableCurrency ) {
            setTimeout(this.setSortTables, 0);
        }
    }

    setSortTables = () => {
        const {sortTableCurrency, exchangeAssets} = this.props;
        if(!sortTableCurrency && exchangeAssets ) {
            let firstAssets = Object.values(exchangeAssets);
            AdminExchangeActions.setSortTableCurrency(firstAssets.shift());
        }
    };

    render() {
        const {referralUsers, sortTableCurrency, changePagination, settingPagination} = this.props;
        // console.log("settingPagination.totalReferral", settingPagination);
        // console.log("referralUsers", referralUsers);
        return(
            <div className={cln("table")}>
                <UserTableSearch {...this.props} />

                <UserTableHeader {...this.props} />



                <div className={cln("table-body")}>
                    {referralUsers.map((user, index)=>{
                        return (
                            <LineTableBind
                                key={`line-table-${user.name}`}
                                sortTableCurrency={sortTableCurrency}
                                lineNumber={index+1}
                                referalUser={user}
                                user={user.name} />
                        );
                    })}
                </div>
                <div className={cln("table-footer")}>
                    <Pagination
                        current={settingPagination.page}
                        pageSize={settingPagination.pageSize}
                        defaultCurrent={1}

                        /*showSizeChanger*/
                        hideOnSinglePage={!settingPagination.totalReferral || settingPagination.totalReferral<=settingPagination.pageSize}
                        pageSizeOptions={["10","20","30","40","50"]}
                        onChange={changePagination}
                        total={settingPagination.totalReferral} />
                </div>
            </div>
        );
    }
}

const UserTableSearch = props => {
    return (
        <div className={cln("table-search")}>
            <div className={cln("table-search-wrap")}>
                <Input type={"search"} placeholder={"Search name"} onChange={event=>props.searchAccount(event.target)} />
            </div>
        </div>
    );
};

class UserTableHeader extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isActive: false
        };
    }

    _onClickLink = (name) => {
        console.log("name", name);
        AdminExchangeActions.setSortTableCurrency(name);
    };
    _onClickSortable = (name) => {
        const {sort} = this.props;
        let sort_by = sort.sort_by === name ? name + " desc" : name;
        console.log("name", name, sort, {sort_by});
        AdminExchangeActions.setSortTable({sort_by});
    };

    _toggleDropdown = () => {
        this.setState({
            isActive: !this.state.isActive
        });
    };
    _closeDropDown = () => {
        if (this.state.isActive) {
            this.setState({isActive: false});
        }
    };

    render() {
        const {exchangeAssets, sortTableCurrency, sort} = this.props;
        if(!exchangeAssets) return null;

        return (
            <div className={cln("table-header")}>
                <span className={cln("table-item", {"number": true})}>#</span>
                <Translate className={cln("table-item", {"date": true})} content={["admin-exchange", "UserTab" , "date-reg"]} />
                <Translate
                    onClick={()=>this._onClickSortable("name")}
                    className={cln("table-item", {
                        [sort.sort_by.split().join("_")]: true
                    })}
                    content={["admin-exchange", "UserTab" , "login"]} />
                <div className={cln("table-item", {"currency": true})}>
                    <ClickOutside
                        className={cln("dropdown")}
                        onClickOutside={this._closeDropDown} >
                        <div onClick={this._toggleDropdown} className={cln(
                            "dropdown-wrapper",
                            {active: this.state.isActive} )} >
                            <div className={cln("dropdown-name")}>
                                {getAlias(sortTableCurrency?.assetFullName)} <span className={cln("dropdown-arrow")}/>
                            </div>
                            <ul className={cln("dropdown-list")}>
                                {Object.values(exchangeAssets).map(item => (
                                    <li key={item.id} className={cln("dropdown-item")}>
                                        <span onClick={()=>this._onClickLink(item)} >
                                            {item.assetName}
                                        </span>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </ClickOutside>
                </div>
                <div className={cln("table-item", {"currency": true})}>DEEX</div>
                <Translate className={cln("table-item", {"changes": true})} content={["admin-exchange", "UserTab" , "all-exchange"]} />
                <Translate className={cln("table-item", {"id": true})} content={["admin-exchange", "UserTab" , "id-user"]} />
            </div>
        );
    }
}

class LineTable extends React.Component {
    static propTypes = {
        user: ChainTypes.ChainAccount,
    };

    static defaultProps = {
        deexAsset: "1.3.2230"
    };

    render() {
        const {user, lineNumber, sortTableCurrency, deexAsset, referalUser} = this.props;
        if(!user) return null;
        // console.log("user", user.toJS(), user.get("history") );
        let userHistory = user.get("history");
        let balanceSelected = user.getIn(["balances", sortTableCurrency.assetId]);
        let balanceDeex = user.getIn(["balances", deexAsset]);
        // console.log("referalUser", referalUser);
        return(
            <div className={cln("table-line")} >
                <div className={cln("table-item", {"number": true})}>{lineNumber}</div>
                <div className={cln("table-item", {"date": true})}>
                    {userHistory ? <BlockTime
                        fullDate={true}
                        block_number={userHistory.last().get("block_num")} /> : null}
                </div>
                <div className={cln("table-item", {"login": true})}>
                    <LinkToAccountById account={user.get("id")} subpage={false} target={"_blank"} />
                </div>

                <div className={cln("table-item", {"currency": true})}>
                    {balanceSelected ? <BalanceComponent balance={balanceSelected} hide_asset /> : "0"}
                </div>
                <div className={cln("table-item", {"currency": true})}>
                    {balanceDeex ? <BalanceComponent balance={balanceDeex} hide_asset />: "0"}
                </div>
                <div className={cln("table-item", {"changes": true})}>
                    {referalUser.count}
                </div>
                <div className={cln("table-item", {"id": true})}>
                    {user.get("id")}
                </div>
                {/*- {historyLast.getIn(["op", 0])}*/}
            </div>
        );
    }
}

const LineTableBind = BindToChainState(LineTable, {});

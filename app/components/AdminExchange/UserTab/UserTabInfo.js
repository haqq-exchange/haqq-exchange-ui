import React from "react";
import InfoTabItem from "../InfoTab/InfoTabItem";

import {cn} from "@bem-react/classname";
import Translate from "react-translate-component";
const cln = cn("UserTab");

export default class UserTabInfo extends React.Component {

    render() {
        const {referralInfo, settings} = this.props;
        const admin_exchange = settings.get("admin_exchange");
        return(
            <div className={cln("info")}>
                <InfoTabItem count={referralInfo?.users || "0"} type={"all-users"} />
                <InfoTabItem count={admin_exchange?.stat?.totalOperations || "0"} type={"all-exchanges"} activeTab={2} />
                <InfoTabItem type={"children"}>
                    {/*<div className={cln("info-btn")}>
                        <Translate className={"btn btn-blue"} type={"button"} component={"button"} content={["admin-exchange", "InfoTab" , "save-csv"]} />
                    </div>*/}
                </InfoTabItem>
            </div>
        );
    }
}

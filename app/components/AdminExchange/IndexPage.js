import React from "react";
import {connect} from "alt-react";
import counterpart from "counterpart";
import Loadable from "react-loadable";
import Icon from "Components/Icon/Icon";
import {async_fetch, getLoadedLocales, getRequestAddress} from "api/apiConfig";
import ChainTypes from "Utility/ChainTypes";
import {Tab, Tabs} from "Components/Utility/Tabs";
import Translate from "react-translate-component";
import BindToChainState from "Utility/BindToChainState";
import LoadingIndicator from "Components/LoadingIndicator";

import SettingsStore from "stores/SettingsStore";
import AdminExchangeStore from "stores/AdminExchangeStore";
import AccountStore from "stores/AccountStore";
import WalletUnlockStore from "stores/WalletUnlockStore";

import "./admin-exchange.scss";
// import accountUtils from "../../lib/common/account_utils";
// import RefUtils from "../../lib/common/ref_utils";
import AdminExchangeActions from "actions/AdminExchangeActions";
import BalanceComponent from "Utility/BalanceComponent";
import SettingsActions from "../../actions/SettingsActions";
import debounce from "lodash-es/debounce";
//import loadable from "loadable-components";

const InfoTab = Loadable({
    loader: () => import(/* webpackChunkName: "AdminExchangeInfoTab" */ "./InfoTab/InfoTab"),
    loading: LoadingIndicator
});

const ReferralsTab = Loadable({
    loader: () => import(/* webpackChunkName: "AdminExchangeInfoTab" */ "./Referrals/ReferralsIndex"),
    loading: LoadingIndicator
});
const SettingTab = Loadable({
    loader: () => import(/* webpackChunkName: "AdminExchangeInfoTab" */ "./SettingTab"),
    loading: LoadingIndicator
});
const TransactionTab = Loadable({
    loader: () => import(/* webpackChunkName: "AdminExchangeTransactionTab" */ "./TransactionTab"),
    loading: LoadingIndicator
});
const GatewayTransactionTab = Loadable({
    loader: () => import(/* webpackChunkName: "AdminExchangeTransactionTab" */ "./GatewayTransactionTab"),
    loading: LoadingIndicator
});
const UserTab = Loadable({
    loader: () => import(/* webpackChunkName: "AdminExchangeUserTab" */ "./UserTab"),
    loading: LoadingIndicator
});


const AuthLoginAsync = Loadable({
    loader: () => import(/* webpackChunkName: "AuthLogin" */ "./AuthLogin"),
    loading: LoadingIndicator
});

class IndexPage extends React.Component {

    static propTypes = {
        account: ChainTypes.ChainAccount,
        asset: ChainTypes.ChainAsset,
    };

    static defaultProps = {
        asset: "1.3.2230",
    };

    constructor(props) {
        super(props);

        this.state = {
            localesLoaded: false
        };
        this.setLoadedLocales = debounce(this.setLoadedLocales, 300);
    }


    setCache = (state) => {
        const {cache} = this.state;
        let newState = Object.assign({}, cache, state);
        this.setState({
            cache: newState
        });
    };

    catchUserSign = error => {
        console.log("error", error)
    };

    componentDidMount() {
        this.getLoadedLocales();
    }

    componentDidUpdate(prevProps) {
        console.log("this.props.locked", this.props.locked, prevProps.locked )
        if( prevProps.locked !== this.props.locked ) {
            if( !this.props.locked ) {
                this.getLoadedLocales();
            }
        }
    }


    getLoadedLocales = () => {
        let transIEO = counterpart.translate("admin-exchange");
        let staticPath, path = "locales";
        if (__DEV__) {
            staticPath = "http://localhost:8081";
            path = "static"
        }
        if( typeof transIEO !== "object" ) {
            getLoadedLocales("admin-exchange.json", path, staticPath)
                .then(this.setLoadedLocales);
        } else {
            this.setLoadedLocales();
        }
    };

    setLoadedLocales = () => {
        this.setState({localesLoaded: true});
        this.hasExchanger(this.getRefInfo);
        this.getAssets();
    };

    getRefInfo = () => {
        const {currentAccount, getUserSign, referralApi, settings} = this.props;
        // console.log("admin_exchange", settings.get("admin_exchange")["servicePrefix"], settings.getIn(["admin_exchange"]));

        const sendData = {
            account: currentAccount,
            system: settings?.get("admin_exchange")?.["servicePrefix"]
        };
        getUserSign(sendData).then(sign => {
            referralApi.postTemplate("/ref/get_sys_info", sendData, sign)
                .then(({content})=>{
                    AdminExchangeActions.setReferralInfo(content);
                });
        }).catch(this.catchUserSign);
    };

    getAssets = () => {
        let settingsAsset = {};
        const {currentAccount, getUserSign, refutils, settings} = this.props;
        const admin_exchange = settings?.get("admin_exchange");
        let dataSend = {
            account_name: currentAccount,
            exchanger_id: admin_exchange.id
        };
        getUserSign(dataSend)
            .then(sign => {
                refutils.postTemplate("/control/getAssets", dataSend, sign)
                    .then(result => {
                        try {
                            const content = result.content;
                            content.data.map(asset => {
                                if (asset) {
                                    settingsAsset[asset.id] = asset;
                                }
                            });
                            AdminExchangeActions.setExchangeAssets(settingsAsset);
                        } catch (e) {
                            console.log(e);
                        }
                    });
            }).catch(this.catchUserSign);
    };

    hasExchanger = calback => {
        if( !__SCROOGE_CHAIN__) {
        const {currentAccount, getUserSign, refutils, settings} = this.props;
        const admin_exchange = settings?.get("admin_exchange");
        let dataSend = {
            account_name: currentAccount,
            exchanger_id: admin_exchange.id
        };

            getUserSign(dataSend).then(sign => {
                refutils.postTemplate("/hasExchanger", dataSend, sign )
                    .then(({content})=>{
                        SettingsActions.changeSetting({
                            setting: "admin_exchange",
                            value: content.data
                        });
                        if( calback ) calback.call(content.data);
                    }).catch(error=>{
                    console.log("is_service_node", error);
                    if( calback ) calback.call();
                });
            }, () => {
                if( calback ) calback.call();
            });
        }
        
    };

    reloadTab = () => {
        this.setState({
            localesLoaded: false
        }, ()=>{
            setTimeout(()=>{
                this.setState({
                    localesLoaded: true
                })
            }, 1000)
        })
    };

    render() {

        const {localesLoaded} = this.state;
        const {locked, account} = this.props;
        console.log("locked", locked);
        console.log("account", account);
        console.log("localesLoaded", localesLoaded);
        if( locked ) {
            return <AuthLoginAsync />;
        }

        if( !localesLoaded ) return  <LoadingIndicator />;

        let tabClassName = {
            item: "admin-exchange-tab-item",
            link: "admin-exchange-tab-link",
            tabTitle: "admin-exchange-tab-title",
            subText: "admin-exchange-tab-subtext",
        };
        let balance;
        if( account?.hasIn(["balances", "1.3.2230"]) ) {
            balance = account.getIn(["balances", "1.3.2230"]);
            console.log("balance", balance, account.toJS());
        }


        const dataProps = {
            hasExchanger: this.hasExchanger,
            catchUserSign: this.catchUserSign,
            ...this.props
        };

        return(
            <div className={"admin-exchange-wrap"}>
                <div style={{
                    "display": "flex",
                    "justifyContent": "space-between",
                    "alignItems": "top",
                }}>
                    <Translate
                        content={"admin-exchange.title-page"}
                        component={"div"}
                        className={"admin-exchange-title-page"} />

                    {balance ? <Translate
                        content={"admin-exchange.title-page-sub"}
                        component={"div"}
                        balance={<BalanceComponent balance={balance}  />}
                        className={"admin-exchange-title-page-sb"} /> : null }

                </div>


                <Tabs
                    defaultActiveTab={1}
                    segmented={false}
                    collapsed={false}
                    hasButtonGroup={false}
                    refProp={ref=>this.childTabs = ref}
                    setting="adminExchange"
                    className="admin-exchange"
                    contentClass="admin-exchange-tabs-content"
                    tabsClass="admin-exchange-tabs"
                >
                    <Tab title={"admin-exchange.tabs.info"} tabClassName={tabClassName}>
                        <InfoTab {...dataProps} />
                    </Tab>
                    <Tab title={"admin-exchange.tabs.user"} tabClassName={tabClassName}>
                        <UserTab {...dataProps} />
                    </Tab>
                    <Tab title={"admin-exchange.tabs.setting"} tabClassName={tabClassName}>
                        <SettingTab {...dataProps} />
                    </Tab>
                    <Tab title={"admin-exchange.tabs.transaction"} tabClassName={tabClassName}>
                        <TransactionTab {...dataProps} />
                    </Tab>
                    <Tab title={"admin-exchange.tabs.gateway_transaction"} tabClassName={tabClassName}>
                        <GatewayTransactionTab {...dataProps} />
                    </Tab>
                    <Tab title={"admin-exchange.tabs.referrals"} tabClassName={tabClassName}>
                        <ReferralsTab {...dataProps} />
                    </Tab>

                   <Icon onClick={()=>this.reloadTab()} className={"is-active admin-exchange-tab-item reload"} name={"reload"}  />
                </Tabs>
            </div>
        );
    }
}

const IndexPageBind = BindToChainState(IndexPage, {});


export default connect(IndexPageBind, {
    listenTo() {
        return [SettingsStore, AdminExchangeStore, WalletUnlockStore];
    },
    getProps() {
        console.log("AdminExchangeStore.getState()", AdminExchangeStore.getState());
        return {
            currentAccount: AccountStore.getState().passwordAccount || AccountStore.getState().currentAccount,
            //account: AccountStore.getState().passwordAccount || AccountStore.getState().currentAccount,
            settings: SettingsStore.getState().settings,
            locked: WalletUnlockStore.getState().locked,

            sortTableCurrency: AdminExchangeStore.getState().sortTableCurrency,
            sort: AdminExchangeStore.getState().sort,
            exchangeAssets: AdminExchangeStore.getState().exchangeAssets,
            referralInfo: AdminExchangeStore.getState().referralInfo,
            referralUsers: AdminExchangeStore.getState().referralUsers,
        };
    }
});

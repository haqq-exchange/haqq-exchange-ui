import React from "react";
import {cn} from "@bem-react/classname";
import Translate from "react-translate-component";
import TransactionDatePicker from "./TransactionTab/TransactionDatePicker";
import TransactionItem from "./TransactionTab/TransactionItem";
import {Checkbox, Pagination, Tooltip} from "antd";
import moment from "moment";
import {FetchChain} from "deexjs";
import {saveAs} from "file-saver";
import counterpart from "counterpart";
import {Asset} from "../../lib/common/MarketClasses";
// import StatusName from "./TransactionTab/StatusName";


const cln = cn("Transaction");


function textContent(n) {
    return n ? `"${n.textContent.replace(/[\s\t\r\n]/gi, " ")}"` : "";
}

export default class TransactionTab extends React.Component {


    static defaultProps = {

    };

    constructor(props) {
        super(props);
        this.state = {
            summary: {
                page: 1,
                pages: 1,
                total: 1,
            },
            dateRange: this.createDateRange(),
            checkboxes: {
                input: {
                    type: "input",
                    checked: true,
                },
                output:{
                    type:  "output",
                    checked: true,
                }
            }
        };
    }

    componentDidMount() {
        this.getHistory();
    }
    componentDidUpdate(prevProps) {

        if( prevProps.locked !== this.props.locked && !this.props.locked
        || prevProps.referralInfo?.users !== this.props.referralInfo?.users
        )
            this.getHistory();
    }

    createDateRange = () => {
        let endValue = new Date();
        let startValue = new Date();
        startValue.setMonth(endValue.getMonth() - 1);

        return {
            endValue: moment(endValue),
            startValue: moment(startValue),
        };
    };


    onChangeCheckbox = event =>{
        let {checkboxes} = this.state;
        const {target} = event;
        Object.values(checkboxes).map(checkbox=>{
            if( checkbox.type === target.name ) {
                this.setState({
                    checkboxes: Object.assign({}, checkboxes, {
                        [checkbox.type]: {
                            type: checkbox.type,
                            checked: target.checked
                        }
                    })
                });
            }
            return checkbox;
        });

    };

    onChangeDate = (dateValue) => {
        let {dateRange} = this.state;
        const {field, value} = dateValue;
        this.setState({
            dateRange: Object.assign({}, dateRange, {[field]: value})
        }, this.getHistory);
    };

    changePagination = (page, pageSize) => {
        let {summary} = this.state;
        summary.page = page;
        summary.pageSize = pageSize;
        this.setState({
            summary
        }, this.getHistory);
    };

    getHistory = () => {
        let {dateRange, summary} = this.state;

        const {currentAccount, getUserSign, catchUserSign, refutils, referralInfo, settings} = this.props;
        const admin_exchange = settings?.get("admin_exchange");

        if(referralInfo?.users) {
            let sendBody = {
                "account_name": currentAccount,
                "date_from": dateRange.startValue.format("DD-MM-YYYY 00:00:01"),
                "date_to": dateRange.endValue.format("DD-MM-YYYY 23:59:59"),
                "page": summary.page,
                "exchanger_id": admin_exchange.id
            };
            getUserSign(sendBody).then(sign => {
                refutils
                    .postTemplate("/control/getHistory", sendBody, sign)
                    .then(({content})=>{
                        console.log("content", content);
                        let assets = [];
                        content.data.map(data=>{
                            Object.keys(data).map(dataKey=>{
                                let item = data[dataKey];
                                if( !assets.includes(item.asset_from) && typeof item.asset_from === "string" ) assets.push(item.asset_from);
                                if( !assets.includes(item.asset_to) && typeof item.asset_to === "string") assets.push(item.asset_to);
                            });
                        });
                        return {
                            transaction: content.data,
                            summary: content.summary,
                            assets
                        };
                    }).then(result=>{
                        console.log("result", result);
                        let assets = {};
                        FetchChain("getAsset", result.assets).then(asset => {
                            asset.map(assetItem => {
                                if( assetItem ) {
                                    console.log("assetItem", assetItem);
                                    assets[assetItem.get("symbol")] = assetItem;
                                }
                            });
                            this.setState({
                                transaction: result.transaction,
                                summary: result.summary,
                                assets
                            });
                        });
                    });
            }).catch(catchUserSign);
        }
        console.log("dateRange", dateRange);
    };


    saveCsv = () => {
        let {dateRange, summary, assets, checkboxes} = this.state;

        const {currentAccount, getUserSign, catchUserSign, refutils, referralInfo, settings} = this.props;

        if(referralInfo?.users) {
            const admin_exchange = settings?.get("admin_exchange");
            let sendBody = {
                "account_name": currentAccount,
                "date_from": dateRange.startValue.format("DD-MM-YYYY 00:00:01"),
                "date_to": dateRange.endValue.format("DD-MM-YYYY 23:59:59"),
                "limitPerPage": -1,
                "exchanger_id": admin_exchange.id
            };
            getUserSign(sendBody).then(sign => {
                refutils
                    .postTemplate("/control/getHistory", sendBody, sign)
                    .then(({content})=>{
                        let csv = [
                            "type",
                            "ID операции",
                            "Дата",
                            "Сумма",
                            "Курс/Статус",
                            "Курс биржи",
                            "Комиссия обмена"
                        ].join(";");
                        content.data.map(line=>{
                            ["in", "out"].map(item=>{
                                if (csv !== "") csv += "\n";
                                let currentItem = line[item];
                                let summary = line["summary"];
                                let lineIn = line["in"];
                                let lineOut = line["out"];
                                let hasIn = item === "in";
                                let currentAssetFrom = assets[currentItem.asset_from];
                                let currentAssetTo = assets[currentItem.asset_to];
                                let checkboxesItem = hasIn ? "input" : "output";
                                let currentCheckboxesItem = checkboxes[checkboxesItem];

                                let assetFrom = new Asset({
                                    amount: lineIn.amount,
                                    asset_id: currentAssetFrom.get("id"),
                                    precision: currentAssetFrom.get("precision")
                                });

                                let assetTo = new Asset({
                                    amount: lineOut.amount,
                                    asset_id: currentAssetTo.get("id"),
                                    precision: currentAssetTo.get("precision")
                                });


                                let currentItemDate = counterpart.localize(new Date(currentItem?.date), {
                                    type: "date",
                                    format: "full"
                                });

                                let statusCode = currentItem.errCode || currentItem.status;
                                let statusType = currentItem.errCode ? "errorCode" : "status";
                                const keyTranslate = ["admin-exchange","transaction", statusType, statusCode.toString()];
                                if( currentCheckboxesItem.checked ) {
                                    csv += [
                                        hasIn ? "deposit" : "withdraw",
                                        hasIn ? currentItem.from : currentItem.to,
                                        currentItemDate,
                                        hasIn ?
                                            assetFrom.getAmount({real: 1}) + " " + currentItem.asset_alias_from :
                                            assetTo.getAmount({real: 1}) + " "+ currentItem.asset_alias_to,
                                        hasIn ? currentItem.exchangeRate : counterpart.translate(keyTranslate),
                                        hasIn ? summary.exchangeRateOriginal || "0" : null,
                                        hasIn ? summary.exchangeSubRate : null
                                    ].join(";");
                                }

                            })
                        });

                        var blob = new Blob([csv], {type: "text/csv;charset=utf-8"});
                        var today = new Date();
                        saveAs(
                            blob,
                            "History exchange" +
                            " from " +
                            dateRange.startValue.format("DD-MM-YYYY") +
                            " to " +
                            dateRange.endValue.format("DD-MM-YYYY") +
                            ".csv"
                        );
                    });
                }).catch(catchUserSign);
        }
    };

    render() {
        const {account, exchangeAssets} = this.props;
        const {checkboxes, dateRange, summary} = this.state;
        //console.log("TransactionTab", this.state , this.props);
        return(
            <div className={cln()} id={cln()}>
                <div className={cln("filter")}>
                    <div className={cln("filter-left")}>
                        <div className={cln("type")}>
                            {Object.values(checkboxes).map(checkbox=> {
                                return (
                                    <Checkbox
                                        key={`transaction-checkbox-${checkbox.type}`}
                                        prefixCls={cln("checkbox")}
                                        checked={checkbox.checked}
                                        name={checkbox.type} onChange={(event) => this.onChangeCheckbox(event)} >
                                        <Translate content={["admin-exchange","transaction", checkbox.type]} />
                                    </Checkbox>
                                );
                            })}
                        </div>
                        <TransactionDatePicker
                            callBackDate={this.onChangeDate}
                            dateRange={dateRange} />
                    </div>
                    {/*<= 100 */}

                    <Tooltip
                        trigger={"hover"}
                        title="Выгрузка возможна не более 100 записей"
                        //visible={!summary.total || summary.total > 100 }
                        getPopupContainer={() => document.getElementById("Transaction")} >
                        <Translate
                            className={"btn btn-blue"}
                            disabled={!summary.total || summary.total > 100 }
                            type={"button"} component={"button"}
                            onClick={()=>this.saveCsv()}
                            content={["admin-exchange", "InfoTab" , "save-csv"]} />
                    </Tooltip>
                </div>

                <TransactionItem
                    account={account}
                    getHistory={this.getHistory}
                    exchangeAssets={Object.values(exchangeAssets)}
                    {...this.props}
                    {...this.state}  />

                <div className={cln("table-footer")}>
                    <Pagination
                        current={summary.page}
                        pageSize={25}
                        defaultCurrent={1}

                        prevIcon={<span>2</span>}
                        nextIcon={<span>2</span>}
                        jumpPrevIcon={<span>1</span>}
                        jumpNextIcon={<span>1</span>}
                        /*showSizeChanger*/
                        /*itemRender={{
                            type: 'page' | 'prev' | 'next'
                        }}*/
                        hideOnSinglePage={summary.pages <= 1}
                        pageSizeOptions={["10","20","30","40","50"]}
                        onChange={this.changePagination}
                        total={summary.total} />
                </div>
            </div>
        );
    }
}

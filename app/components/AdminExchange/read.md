
****Admin panel****

***Has exchange***


- **URL**

  /hasExchanger

- **Method:**

  `POST`

- **URL Params**

  None

- **Data Params**

  * `data: {"account_name":"aa12345aa"}`
  * `sign: IEGlzsdJ3UpHb7...Rk=`

* **Request Headers**

  * `Content-Type: application/x-www-form-urlencoded`

- **Success Response:**

  **Code:** 200 <br />
  **Content:** 

  ```json
  {
    "success": true,
    "data": {
      "valid":true,
      "id":1
    }
  }
  ```

- **Error Responses:**

| Code          | Content           |
| ------------- |:-------------|
| 500 | `{ errorMessage / errorCode : "string" }`|
| 400 | `{ errorMessage / errorCode : "string" }`|
| 401 | `{ errorMessage / errorCode : "string" }`|    
| 403 | `{ errorMessage / errorCode : "string" }`|
| 404 | `{ errorMessage / errorCode : "string" }`|
| 418 | `{ errorMessage / errorCode : "string" }`|



***Get ASSETS***


- **URL**

  /getAssets

- **Method:**

  `GET`

- **URL Params**

  `exchangerId=1`

- **Data Params**

  None

* **Request Headers**

  None

- **Success Response:**

  **Code:** 200 <br />
  **Content:** 

  ```json
  {
  "success":true,
  "data":[{
      "id":1,
      "assetId":"1.3.4877",
      "precision":5,
      "assetFullName":"QIWIRUBLE",
      "assetName":"QIWI",
      "withdrawalMinAmount":"527500000",
      "withdrawalMaxAmount":"5000000000",
      "withdrawalStaticFee":null,
      "withdrawalPercentageFee":5,
      "depositStaticFee":5,
      "depositPercentageFee":null,
      "depositMinAmount":"526500000",
      "depositMaxAmount":"5000000000",
      "withdrawalAllowed":true,
      "depositAllowed":true,
      "withdrawMemoSupport":true,
      "gateway":"qiwi-ruble",
      "backingCoin":"rub"
    }]
  }
  ```
  
  ***Get MARKETS***
  
  
  - **URL**
  
    /getMarkets
  
  - **Method:**
  
    `GET`
  
  - **URL Params**
  
    `exchangerId=1`
  
  - **Data Params**
  
    None
  
  * **Request Headers**
  
    None
  
  - **Success Response:**
  
    **Code:** 200 <br />
    **Content:** 
  
    ```json
    {
    "success":true,
    "data":[{
        "id":1,
        "assetFrom":1,
        "assetTo":2,
        "from":"1.3.4877",
        "to":"1.3.3448",
        "minAmount":0,
        "maxAmount":0,
        "exchangeSource":1,
        "active":true,
        "exchangeFee":0
      }]
    }
    ```
      
   *** ADD Market ***
    
    
  - **URL**
    
     /control/addMarket

  - **Method:**
    
      `POST`
    
  - **URL Params**
    
      `sign`: (string)
      `data`: (string) - json-encoded string.
      
        keys:
            maxAmount: (not required) =0 as default(no limit)
            minAmount: (not required) =0 as default(no limit)
            assetFrom (required) asset from coins
            assetTo (required) asset from coins
            active (not requred) =0 as default. 1 active. 0 not
            exchangeFee (required)  FLOAT value
            exchangeSource
            reverseMinAmount
            reverseMaxAmount

    
    
  - **Success Response:**
    
      **Code:** 200 <br />
      **Content:** 
    
      ```json
      {
        success: true/false
        data: {
          id: 123
        }
      }
      ```
            
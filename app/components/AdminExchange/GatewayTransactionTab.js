import React from "react";
import {cn} from "@bem-react/classname";
import Translate from "react-translate-component";
import TransactionDatePicker from "./GatewayTransactionTab/TransactionDatePicker";
import TransactionItem from "./GatewayTransactionTab/TransactionItem";
import {Checkbox} from "antd";
import moment from "moment";
import {FetchChain} from "deexjs";


const cln = cn("Transaction");

export default class GatewayTransactionTab extends React.Component {


    static defaultProps = {

    };

    constructor(props) {
        super(props);
        this.state = {
            params: {
                sortDirection: "DESC",
                sortBy: "id",
                search: "",
                direction: null,
            },
            summary: {
                page: 1,
                pages: 1,
                total: 1,
            },
            dateRange: this.createDateRange(),
            checkboxes: {
                input: {
                    type: "input",
                    value: 1,
                    checked: true,
                },
                output:{
                    type:  "output",
                    value: 2,
                    checked: true,
                }
            }
        };
    }

    componentDidMount() {
        this.getHistory();
    }
    componentDidUpdate(prevProps) {

        console.log("prevProps.locked", prevProps.locked, this.props.locked, this.props.referralInfo?.users);

        if( prevProps.locked !== this.props.locked && !this.props.locked
        || prevProps.referralInfo?.users !== this.props.referralInfo?.users
        )
            this.getHistory();
    }

    createDateRange = () => {
        let endValue = new Date();
        let startValue = new Date();
        startValue.setMonth(endValue.getMonth() - 1);

        return {
            endValue: moment(endValue),
            startValue: moment(startValue),
        };
    };


    onChangeCheckbox = event =>{
        let {checkboxes} = this.state;
        const {target} = event;
        Object.values(checkboxes).map(checkbox=>{
            if( checkbox.type === target.name ) {

                console.log("checkbox.type", checkbox)
                console.log("target.type", target)

                this.setState({
                    checkboxes: Object.assign({}, checkboxes, {
                        [checkbox.type]: {
                            type: checkbox.type,
                            checked: target.checked,
                            value: target.value,
                        }
                    })
                }, this.checkboxChanges);
            }
            return checkbox;
        });

    };

    checkboxChanges = () => {
        let {checkboxes} = this.state;
        let direction = null;
        let checkedChbox = Object.values(checkboxes).map(chbox=>{
            if( chbox.checked ) return chbox.value
        }).filter(a=>{
            console.log("a", a, typeof a)
            if(typeof a === "number") return a;
        });

        if(checkedChbox.length === 1) {
            direction = checkedChbox[0];
        }
        console.log("checkedChbox", checkedChbox, direction);

        this.setSortBy({direction});
    };

    onChangeDate = (dateValue) => {
        let {dateRange} = this.state;
        const {field, value} = dateValue;
        this.setState({
            dateRange: Object.assign({}, dateRange, {[field]: value})
        }, this.getHistory);
    };

    changePagination = (page, pageSize) => {
        let {summary} = this.state;
        summary.page = page;
        summary.pageSize = pageSize;
        this.setState({
            summary
        }, this.getHistory);
    };



    getHistory = () => {
        let {dateRange, summary, params} = this.state;

        const {currentAccount, getUserSign, catchUserSign, settings, refutils, referralInfo} = this.props;

        if(referralInfo?.users) {
            const admin_exchange = settings?.get("admin_exchange");
            let sendBody = {
                "account_name": currentAccount,
                "date_from": dateRange.startValue.format("DD-MM-YYYY 00:00:01"),
                "date_to": dateRange.endValue.format("DD-MM-YYYY 23:59:59"),
                "page": summary.page,
                "exchanger_id": admin_exchange.id,
                ...params
            };
            getUserSign(sendBody).then(sign => {
                refutils
                    .postTemplate("/control/getGatewayHistory", sendBody, sign)
                    .then(({content})=>{
                        console.log("content", content);
                        let assets = [];
                        content.data.map(data=>{
                            Object.keys(data).map(dataKey=>{
                                let item = data[dataKey];
                                if( !assets.includes(item.symbol) && typeof item.symbol === "string" ) assets.push(item.symbol);
                            });
                        });
                        return {
                            transaction: content.data,
                            summary: content.summary,
                            assets
                        };
                    }).then(result=>{
                        console.log("result", result);
                        let assets = {};
                        FetchChain("getAsset", result.assets).then(asset => {
                            asset.map(assetItem => {
                                if( assetItem ) {
                                    console.log("assetItem", assetItem);
                                    assets[assetItem.get("symbol")] = assetItem;
                                }
                            });
                            this.setState({
                                transaction: result.transaction,
                                summary: result.summary,
                                assets
                            });
                        });
                    });
            // }).catch(catchUserSign);
            }).catch(()=>{

            });
        }
        console.log("dateRange", dateRange);
    };

    setSortBy = params => {
        this.setState({params}, this.getHistory);
    };

    render() {
        const {account, exchangeAssets} = this.props;
        const {checkboxes, dateRange} = this.state;
        console.log("TransactionTab", this.state , this.props);
        return(
            <div className={cln()}>
                <div className={cln("type")}>
                    {Object.values(checkboxes).map(checkbox=> {
                        return (
                            <Checkbox
                                key={`transaction-checkbox-${checkbox.type}`}
                                prefixCls={cln("checkbox")}
                                checked={checkbox.checked}
                                value={checkbox.value}
                                name={checkbox.type} onChange={(event) => this.onChangeCheckbox(event)} >
                                <Translate content={["admin-exchange","transaction", checkbox.type]} />
                            </Checkbox>
                        );
                    })}
                </div>
                <TransactionDatePicker
                    callBackDate={this.onChangeDate}
                    setSortBy={this.setSortBy}
                    dateRange={dateRange} />


                <TransactionItem
                    account={account}
                    exchangeAssets={Object.values(exchangeAssets)}
                    setSortBy={this.setSortBy}
                    {...this.props}
                    {...this.state}  />

                <div className={cln("table-footer")}>
                    {/*<Pagination
                        current={summary.page}
                        pageSize={25}
                        defaultCurrent={1}

                        /*showSizeChanger * /
                        hideOnSinglePage={summary.pages <= 1}
                        pageSizeOptions={["10","20","30","40","50"]}
                        onChange={this.changePagination}
                        total={summary.total} />*/}
                </div>
            </div>
        );
    }
}

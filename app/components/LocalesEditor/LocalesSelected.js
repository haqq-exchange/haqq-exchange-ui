import React from "react";
import { Select } from "antd";
import cname from "classnames";
import Immutable from "immutable";

const Option = Select.Option;

export default class LocalesSelected extends React.PureComponent {
    static defaultProps = {
        rowsLocales: ["first","end"]
    };

    constructor(props){
        super(props);

        this.state = {
            selectedOption: Immutable.Map()
        };
    }

    getEditorOption = () => {
        const { locales } = this.props;
        const { selectedOption } = this.state;

        return locales.map(locale=>{
            if( selectedOption.toArray().indexOf(locale) === -1 )
                return locale;
        }).filter(a => a);
    };

    handleChange = (name, value) => {
        let { selectedOption } = this.state;
        const {rowsLocales}=this.props;
        if( value.indexOf(name) === -1 ) {
            selectedOption = selectedOption.set(name, value);
        } else {
            selectedOption = selectedOption.delete(name);
        }
        this.setState({selectedOption}, ()=>{
            const showLocales = rowsLocales.map(rows=>{
                return selectedOption.get(rows);
            });
            this.props.addShowLocales(showLocales);
        });
    };

    render(){
        const {rowsLocales}=this.props;
        return (
            <div className={"locales-editor-selected"} id={"locales-editor-selected"}>
                <div className={"locales-editor-title"}>
                    Выберите язык/перевод
                </div>
                <div className={"locales-editor-selected-rows"}>
                    {rowsLocales.map((index)=>{
                        return (
                            <div key={"selected-item-" + index } className={"locales-editor-selected-item"}>
                                <Select

                                    placeholder={"Выбрать язык"}
                                    getPopupContainer={()=>document.getElementById("locales-editor-selected")}
                                    onChange={(value)=>this.handleChange(index, value)}
                                    size={"default"} id={"selected-item-" + index} className={"locales-editor-selected-select"} >
                                    {this.getEditorOption().map(data=>{
                                        return (
                                            <Option key={"option-" + data} value={data}>{data}</Option>
                                        );
                                    })}
                                </Select>
                            </div>
                        )
                    })}
                </div>

            </div>
        );
    }
}
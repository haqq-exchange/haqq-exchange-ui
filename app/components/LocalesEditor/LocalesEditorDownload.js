import React from "react";
import {Link} from "react-router-dom";
import LocalesSelected from "Components/LocalesEditor/LocalesSelected";
import Immutable from "immutable";
import locales from "../../assets/locales/";
import "./style.scss";
import {saveAs} from "file-saver";
import {Input, Select} from "antd";
const { TextArea } = Input;
const { Option } = Select;

function textContent(n) {
    return n ? `"${n.replace(/[\s\t\r\n]/gi, " ")}"` : "";
}

export default class LocalesEditorDownload extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            local: null,
            saveLocales: Immutable.Map(),
            showLocales: Immutable.List(),
            allKeys: Immutable.Map(),
            showValue: Immutable.Map(),
            showPath: Immutable.List(["root"]),
        };
    }



    saveAsLocales = () => {
        //var blob = new Blob([csv], {type: "text/csv;charset=utf-8"});
        let {saveLocales, local} = this.state;
        /*return fetch("/save-locales/download", {
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json"
            }),
            body: JSON.stringify(saveLocales)
        });*/

        console.log("saveLocales", saveLocales);

        let csv = ["key", local, "translate"].join(";");
        for (let n in saveLocales.toJS()) {
            //console.log("-- RecentTransactions._downloadCSV -->", n);

            if (csv !== "") csv += "\n";
            csv += [
                textContent(n),
                textContent(saveLocales.get(n)),
                textContent(""),
            ].join(";");
        }
        console.log("csv", csv);
        var blob = new Blob([csv], {type: "text/csv;charset=utf-8"});
        var today = new Date();
        saveAs(
            blob,
            local +
            "-" +
            today.getFullYear() +
            "-" +
            ("0" + (today.getMonth() + 1)).slice(-2) +
            "-" +
            ("0" + today.getDate()).slice(-2) +
            "-" +
            ("0" + today.getHours()).slice(-2) +
            ("0" + today.getMinutes()).slice(-2) +
            ".csv"
        );
    };

    downloadLocales = (local) => {
        let {saveLocales} = this.state;
        let downLocales = locales[local];

        const setLocalesObject = (keyItem, data) => {
            for (let key in data) {
                let localKey = [keyItem, key].join(".");
                let localValue = data[key];
                if (typeof data == "object" && typeof localValue == "object")  {
                    setLocalesObject(localKey , localValue);
                } else {
                    if( typeof data == "string" ) {
                        saveLocales = saveLocales.set(keyItem, data);
                    } else {
                        saveLocales = saveLocales.set(localKey, localValue);
                    }
                    this.setState({saveLocales});
                }
            }
        };

        for (let key in downLocales) {
            setLocalesObject(key, downLocales[key]);
        }
        this.setState({
            local
        });


        console.log("local", local, downLocales);
        //console.log("saveLocales", saveLocales.toJS() );
    };

    render() {
        let {local, allKeys, showValue, showPath, showLocales} = this.state;
        console.log("locales", locales, this.state);

        let showValueJs,
            allKeysJs=[],
            currentKeys=[],
            selectedPath = showPath.toArray();

        selectedPath.shift();

        // console.log("this.state", this.state);
        // console.log("selectedPath", selectedPath );

        if( allKeys.hasIn(showPath.toArray()) ) {
            currentKeys = Object.keys(allKeys.getIn(showPath.toArray()).toJS());
        }
        if( showValue.hasIn(showPath.toArray()) ) {
            showValueJs = showValue.getIn(showPath.toArray()).toJS();
            allKeysJs = Object.keys(showValueJs);
        }

        return (

            <div className={"locales-editor"}>
                <h1>Locales Editor Download</h1>

                <div>
                    <Link to={"/locales-editor"}>Переводы</Link>{" "}
                    <Link to={"/locales-editor/save"}>Загрузить</Link>
                </div>



                <div className={"locales-editor-lang"}>
                    <LocalesSelected
                        rowsLocales={ ["first"]}
                        addShowLocales={this.downloadLocales} locales={Immutable.fromJS(Object.keys(locales))} />
                </div>

                <div  className={"locales-editor-wrap"}>
                    {showLocales.size > 0 && allKeys.has("root") ? showPath.toArray().map((path, index) => {
                        let pathItem = showPath.toArray().slice(0, index+1);
                        if( allKeys.hasIn(pathItem) ) {
                            const allKeysJs = Object.keys(allKeys.getIn(pathItem).toJS());
                            return (
                                <div key={"size1-" + path} className={"locales-editor-row"} id={"locales-editor-row-"+ path}>
                                    <div className={"locales-editor-title"}>
                                        {path}
                                    </div>
                                    {allKeysJs.length ?
                                        <div>
                                            <Select
                                                name="" id=""
                                                placeholder="Выбрать"
                                                getPopupContainer={()=>document.getElementById("locales-editor-row-"+ path)}
                                                onChange={(event)=>this.changePath(event, path)}>
                                                <Option key={path+"empty"} value={""}  >Выбрать</Option>
                                                {allKeysJs.sort().map((keys,index)=>{
                                                    return (<Option key={path+index} value={keys} data-path={path} >{keys}</Option>);
                                                })}
                                            </Select>
                                        </div>
                                        : null }
                                </div>
                            );
                        }
                    }) : null }
                </div>

                {local ? <div>
                    <button className={"btn btn-red"} onClick={this.saveAsLocales}>saveAsLocales</button>
                </div> : null}


            </div>
        );
    }

}
import React from "react";
import {Link} from "react-router-dom";
import cname from "classnames";
import {get, set, uniq} from "lodash";
import {Form, Input, Select, Typography } from "antd";
import unset from "lodash.unset";
import locales from "../../assets/locales/";
import Immutable from "immutable";
import LocalesSelected from "./LocalesSelected";
import Icon from "Components/Icon/Icon";
import CopyButton from "../Utility/CopyButton";
import {getLoadedLocales} from "api/apiConfig";

import "./style.scss";
import notify from "actions/NotificationActions";
import counterpart from "counterpart";
import {saveAs} from "file-saver";
const { TextArea } = Input;
const { Option } = Select;
const { Paragraph } = Typography;
class LocalesEditorWrapper extends React.PureComponent {
    _fetch(data) {
        return fetch("/save-locales", {
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json"
            }),
            body: JSON.stringify(data)
        });
    }
}


function textContent(n) {
    return n ? `"${n.replace(/[\s\t\r\n]/gi, " ")}"` : "";
}
class LocalesSave extends React.PureComponent {
    constructor(props){
        super(props);

        this.state = {
            saveLocales: Immutable.Map(),
            currentSaveLocale: Immutable.Map(),
        };
    }

    saveAsLocales = () => {
        //var blob = new Blob([csv], {type: "text/csv;charset=utf-8"});
        let {showLocales} = this.props;
        let {saveLocales, currentSaveLocale} = this.state;

        let csv = ["key"].concat(showLocales.toJS()).join(";");

        showLocales.map(local => {
            saveLocales.sort().map((value, key) => {
                if( key.indexOf(local) !== -1) {
                    let itemKey = key.split(local + ".").pop();
                    if( currentSaveLocale.has(itemKey) ) {
                        currentSaveLocale = currentSaveLocale.update(itemKey, data => data.concat(value));
                    } else {
                        currentSaveLocale = currentSaveLocale.set(itemKey, [value]);
                    }
                }

            });
        });


        for (let n in currentSaveLocale.toJS()) {
            if (csv !== "") csv += "\n";
            let currentCSV = [
                textContent(n),
            ].concat(currentSaveLocale.get(n).map(data=>textContent(data)));
            csv += currentCSV.join(";");
        }
        var blob = new Blob([ csv], {type: "text/csv;charset=utf-8"});
        var today = new Date();
        saveAs(
            blob,
            showLocales.toJS().join("_to_") +
            "-" +
            today.getFullYear() +
            "-" +
            ("0" + (today.getMonth() + 1)).slice(-2) +
            "-" +
            ("0" + today.getDate()).slice(-2) +
            "-" +
            ("0" + today.getHours()).slice(-2) +
            ("0" + today.getMinutes()).slice(-2) +
            ".csv"
        );

    };

    downloadLocales = () => {
        const {showPath, showLocales, locales } = this.props;
        let {saveLocales} = this.state;

        let selectedPath = showPath.toArray();
        selectedPath.shift();
        let downLocales = {};
        showLocales.map(locale => {
            if( locale ) {
                //console.log("locales", locales.getIn([locale].concat(selectedPath)).toJS() );
                downLocales[locale] = locales.getIn([locale].concat(selectedPath)).toJS();
                //return locales.getIn([locale].concat(selectedPath)).toJS();
            }
        });

        const setLocalesObject = (keyItem, data) => {
            for (let key in data) {
                let localKey = [keyItem, key].join(".");
                let localValue = data[key];
                if (typeof data == "object" && typeof localValue == "object")  {
                    setLocalesObject(localKey , localValue);
                } else {
                    if( typeof data == "string" ) {
                        saveLocales = saveLocales.set(keyItem, data);
                    } else {
                        saveLocales = saveLocales.set(localKey, localValue);
                    }
                    this.setState({saveLocales});
                }
            }
        };

        new Promise(resolve => {
            for (let key in downLocales) {
                setLocalesObject(key, downLocales[key]);
            }
            resolve();
        }).then(this.saveAsLocales);


        console.log("showPath, showLocales, locales", showPath.toJS(), showLocales.toJS(), locales.toJS());
    };

    render() {

        console.log("LocalesSave props", this.props )

        return (
            <div style={{margin: "0 10px 0 0"}}>
                <button type={"button"} onClick={()=>this.downloadLocales()} className={"btn btn-gray"}>
                    Выгрузить перевод
                </button>
            </div>
        );
    }

}

class LocalesAddKey extends React.PureComponent {
    constructor(props){
        super(props);

        this.state = {
            selectedOption: "",
            showForm: false
        };
    }

    submit = (event) =>{
        const {selectedOption}= this.state;
        event.preventDefault();
        if( selectedOption ) {
            this.props.submit(event);
            this.setState({showForm: false});
        } else {
            notify.addNotification({
                message: "Выбрать тип ключа",
                level: "error",
                autoDismiss: 10
            });
        }
    };

    handleOptionChange = (event) => {
        this.setState({
            selectedOption: event.target.value
        });
    };

    render(){
        const {showForm, selectedOption}= this.state;
        return(
            <div className={"locales-editor-keys"} id={"locales-editor-add-keys"}>
                { showForm
                    ? (<form className={"locales-editor-keys-form"} onSubmit={this.submit}>
                        <div className={"locales-editor-wrap"} >
                            <label key={"string"}>
                                <input
                                    type="radio"
                                    checked={selectedOption === "OptionString"}
                                    onChange={this.handleOptionChange}
                                    name={"type_key"} value={"OptionString"} />
                                string
                            </label>&nbsp;&nbsp;
                            <label key={"object"}>
                                <input
                                    type="radio"
                                    checked={selectedOption === "OptionObject"}
                                    onChange={this.handleOptionChange}
                                    name={"type_key"} value={"OptionObject"}/>
                                object
                            </label>
                        </div>

                        <input type="text" name={"new_key"} placeholder={"Новый ключ"} className={"locales-editor-keys-input"} />

                        <div className={"locales-editor-keys-btn-group"} >
                            <button type={"cancel"} className={"btn btn-gray"} onClick={()=>this.setState({showForm: false})}>отмена</button>
                            <button type={"submit"} className={"btn btn-green"}>добавить</button>
                        </div>
                    </form>)
                    : <button type={"button"} onClick={()=>this.setState({showForm: true, selectedOption: ""})} className={"btn btn-green"}>
                        Добавить ключ
                    </button>
                }
            </div>
        );
    }
}
class LocalesRemoveKey extends React.PureComponent {
    constructor(props){
        super(props);

        this.state = {
            showForm: false
        };
    }

    submit = (event) => {
        event.preventDefault();
        this.props.submit(event);
        this.setState({showForm: false});
    };

    render(){
        const {allKeys}= this.props;
        const {showForm}= this.state;
        return(
            <div className={"locales-editor-keys"} id={"locales-editor-remove-keys"}>
                { showForm
                    ? (<form key={"locales-editor-remove-keys-form"} className={"locales-editor-keys-form"}  onSubmit={this.submit}>
                        <input type="hidden" name={"remove_key"} defaultValue={this.state.removeKey}/>
                        <Select
                            placeholder="Выбрать ключ"
                            onChange={(value)=>this.setState({removeKey: value })}
                            getPopupContainer={()=>document.getElementById("locales-editor-remove-keys")}
                            size={"default"} id={"selected-remove-key"} className={"locales-editor-keys-selected"} >
                            {allKeys.sort().map(key => {
                                return (
                                    <Option key={key} value={key}>{key}</Option>
                                );
                            })}
                        </Select>
                        <div className={"locales-editor-keys-btn-group"} >
                            <button type={"cancel"} onClick={()=>this.setState({showForm: false})} className={"btn btn-gray"}>cancel</button>
                            <button type={"submit"} className={"btn btn-green"}>удалить</button>
                        </div>
                    </form>)
                    : <button type={"button"} onClick={()=>this.setState({showForm: true})} className={"btn btn-gray"}>
                        Удалить ключ
                    </button>
                }
            </div>
        );
    }
}

class LocalesTextarea extends React.PureComponent  {
    constructor(props){
        super(props);
        this.state = {
            showTextArea: false,
            valueTextArea: props.value,
            nameTextArea: props.name,
        };
    }

    submit = (event) =>{
        const _this = this;
        let { path } = this.props;
        const { nameTextArea } = this.state;
        event.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                path.shift();
                this.props
                    .saveData([this.props.locale], {
                        [path.concat(Object.keys(values)).join(".")] : values[nameTextArea],
                    })
                    .then(()=>{
                        // console.log("values", values)
                        _this.setState({
                            showTextArea: false,
                            valueTextArea: values[nameTextArea]
                        });
                    });
            }
        });
    };

    render(){
        const {getFieldDecorator, isFieldTouched}= this.props.form;
        const {showTextArea, nameTextArea , valueTextArea } = this.state;
        const textTouch = isFieldTouched(nameTextArea);
        return(
            <div className={"locales-editor-area"}>
                { showTextArea
                    ? (<Form onSubmit={this.submit} style={{width: "100%"}}>
                        {getFieldDecorator(nameTextArea, {
                            rules: [{ required: true }],
                            initialValue: valueTextArea
                        })(
                            <TextArea ref={(input) => this.textArea = input } />
                        )}
                        <div className={"locales-editor-area-btn-group"} >
                            {textTouch ? <button type={"submit"} tabIndex={0} className={"btn btn-green"}>изменить</button>:null}
                            <button type={"cancel"} className={"btn btn-gray"} tabIndex={-1} onClick={()=>this.setState({showTextArea: false})}>отмена</button>
                        </div>
                    </Form>)
                    : (
                        <div className={"locales-editor-area"} onClick={()=>{this.setState({showTextArea: true}, () => this.textArea.focus());}} >
                            {valueTextArea}
                            <Icon name={"pencil-alt"}  />
                        </div>
                    )
                }
            </div>
        );
    }
}
const LocalesTextareaForm = Form.create()(LocalesTextarea);


const ToggleNewKey = (props) => {
    // console.log("ToggleNewKey props", props)
    return (
        <div className={"locales-editor-change-key"}>
            {props.allKeysJs ? <LocalesRemoveKey
                submit={props.removeJsonKey}
                allKeys={uniq(props.allKeysJs.concat(props.currentKeys))}
                showValue={props.showValue} /> : null}
            {props.downloadLocales ? <LocalesSave downloadLocales={props.downloadLocales} {...props} /> : null}
            <LocalesAddKey submit={props.addJsonKey} />
        </div>
    );
};


export default class LocalesEditor extends LocalesEditorWrapper {

    static defaultProps = {
        showCountLocal: 2
    };

    constructor(props){
        super(props);

        console.log("locales", locales);

        this.state = {
            locales: Immutable.fromJS(locales),
            showLocales: Immutable.List(),
            allKeys: Immutable.Map(),
            showValue: Immutable.Map(),
            showPath: Immutable.List(["root"]),
        };
    }

    componentDidMount(){
    }



    addShowLocales = (locale) => {
        this.setState({
            showLocales: Immutable.List(Immutable.fromJS(locale))
        }, this.addLocalesKeys );
    };

    addLocalesKeys = () => {
        let {locales, allKeys, showValue, showPath} = this.state;
        let selectedPath = showPath.toArray();
        selectedPath.shift();
        if( !allKeys.has(showPath.toArray()) ) {
            allKeys = allKeys.set(showPath.toArray(), Immutable.Map());
            showValue = showValue.set(showPath.toArray(), Immutable.Map());
        }

        locales.map((locale)=>{
            let localeJS = selectedPath.length ? get(locale.toJS(), selectedPath, {}) : locale.toJS();
            let keys = Object.keys(localeJS);
            keys.map(key=>{
                const currentKeyPath = showPath.toArray().concat([key]);
                if( !allKeys.hasIn(currentKeyPath) ){
                    if( typeof get(localeJS, key) === "string" ) {
                        showValue = showValue.setIn(currentKeyPath , key );
                    } else {
                        allKeys = allKeys.setIn(currentKeyPath, Immutable.fromJS({}) );
                    }
                }
            });
        });
        this.setState({allKeys, showValue});
    };

    changePath = (event, path) => {
        let {showPath} = this.state;
        showPath = showPath.slice(0, showPath.indexOf(path) +1 );
        if( event ) {
            showPath = showPath.push(event);
        }
        this.setState({showPath}, this.addLocalesKeys);
    };

    _getFormData = (event) => {
        return Array.from(event.target.elements)
            .filter(el => {
                if( el.type === "radio" ) {
                    if( el.checked )
                        return el.name;
                } else {
                    return el.name;
                }
            })
            .reduce((a, b) => ({...a, [b.name]: b.value}), {});
    };

    removeJsonKey = (event) => {
        event.preventDefault();
        const _this = this;
        let {locales, showPath} = this.state;
        let selectedPath = showPath.toArray();
        let keyLocales = Object.keys(locales.toJS());
        const formData = this._getFormData(event);
        selectedPath.shift();
        selectedPath = selectedPath.concat(Object.values(formData));

        this._fetch({
            files: keyLocales,
            unset: [selectedPath.join(".")]
        }).then(reply =>
            reply.json().then(result => {
                return result;
            })
        ).then(result => {
            if( result.succes ) {
                keyLocales.map(local=>{
                    let localesJS = locales.toJS();
                    unset(localesJS, [local].concat(selectedPath));
                    _this.setState({
                        showRemoveKey: false,
                        showValue: Immutable.Map(),
                        "locales": Immutable.fromJS(localesJS)
                    }, _this.addLocalesKeys);
                });
            }
        });

    };

    saveFormData = (files, data) => {
        return this._fetch({
            files,
            set: data
        }).then(reply =>
            reply.json().then(result => {
                return result;
            })
        );
    };

    downloadLocales = (event) => {};

    addJsonKey = (event) => {
        event.preventDefault();
        const _this = this;
        let {locales, showPath} = this.state;
        let selectedPath = showPath.toArray();
        let keyLocales = Object.keys(locales.toJS());
        const formData = this._getFormData(event);
        if( !formData.new_key ) {
            notify.addNotification({
                message: "Добавить ключ перевода",
                level: "error",
                autoDismiss: 10
            });
            return false;
        }

        let objectValue = formData.type_key === "OptionObject" ? {} : "";
        selectedPath.shift();
        selectedPath = selectedPath.concat([formData.new_key]);

        this.saveFormData(keyLocales, {
            [selectedPath.join(".")]: objectValue
        }).then(result => {
            if( result.succes ) {
                keyLocales.map(local=>{
                    let localesJS = locales.toJS();
                    set(localesJS, [local].concat(selectedPath) , objectValue);
                    _this.setState({
                        showAddNewKey: false,
                        "locales": Immutable.fromJS(localesJS)
                    }, _this.addLocalesKeys);
                });
            }
        });
    };

    loadedWithServer = name => {
        let  { locales } = this.state;

        getLoadedLocales(`${name}.json`)
            .then(result=>{
                let localesJs = locales.toJS();
                Object.keys(result).map(lang => {
                    Object.assign(localesJs[`locale-${lang}`], result[lang]);
                });
                this.setState({
                    locales: Immutable.fromJS(localesJs)
                })
            });

    };

    render() {
        const  { locales, showPath, showLocales, allKeys, showValue} = this.state;
        let showValueJs,
            allKeysJs=[],
            currentKeys=[],
            selectedPath = showPath.toArray();

        selectedPath.shift();

        // console.log("this.state", this.state);
        // console.log("selectedPath", selectedPath );

        if( allKeys.hasIn(showPath.toArray()) ) {
            currentKeys = Object.keys(allKeys.getIn(showPath.toArray()).toJS());
        }
        if( showValue.hasIn(showPath.toArray()) ) {
            showValueJs = showValue.getIn(showPath.toArray()).toJS();
            allKeysJs = Object.keys(showValueJs);
        }

        return(
            <div className={"locales-editor"}>
                <h1>Locales Editor</h1>

               {/* <div>
                    <Link to={"/locales-editor/download"}>Выгрузить</Link>{" "}
                    <Link to={"/locales-editor/save"}>Загрузить</Link>
                </div>*/}
                <br/>
                <Paragraph editable={{ onChange: this.loadedWithServer }}>
                    admin-exchnage/credit
                </Paragraph>
                {/*<span onClick={()=>this.loadedWithServer("admin-exchange")}>
                    admin-exchnage
                </span>*/}
                <br/>


                <div className={"locales-editor-lang"}>
                    <LocalesSelected
                        addShowLocales={this.addShowLocales}
                        locales={Immutable.fromJS(Object.keys(locales.toJS()))} />
                </div>


                <div  className={"locales-editor-wrap"}>
                    {showLocales.size > 0 && allKeys.has("root") ? showPath.toArray().map((path, index) => {
                        let pathItem = showPath.toArray().slice(0, index+1);
                        if( allKeys.hasIn(pathItem) ) {
                            const allKeysJs = Object.keys(allKeys.getIn(pathItem).toJS());
                            return (
                                <div key={"size1-" + path} className={"locales-editor-row"} id={"locales-editor-row-"+ path}>
                                    <div className={"locales-editor-title"}>
                                        {path}
                                    </div>
                                    {allKeysJs.length ?
                                        <div>
                                            <Select
                                                name="" id=""
                                                placeholder="Выбрать"
                                                getPopupContainer={()=>document.getElementById("locales-editor-row-"+ path)}
                                                onChange={(event)=>this.changePath(event, path)}>
                                                <Option key={path+"empty"} value={""}  >Выбрать</Option>
                                                {allKeysJs.sort().map((keys,index)=>{
                                                    return (<Option key={path+index} value={keys} data-path={path} >{keys}</Option>);
                                                })}
                                            </Select>
                                        </div>
                                        : null }
                                </div>
                            );
                        }
                    }) : null }
                </div>

                {showLocales.size ? <ToggleNewKey
                    allKeysJs={allKeysJs}
                    currentKeys={currentKeys}
                    addJsonKey={this.addJsonKey}
                    removeJsonKey={this.removeJsonKey}
                    showValue={showValueJs} /> : null}

                    <div style={{
                        margin: "20px 0"
                    }}>
                        %(asset)s
                    </div>

                <div  className={"locales-editor-wrap"}>
                    {showLocales.size > 0 && showValue.has("root") ? showLocales.toArray().map(locale => {
                        if( allKeysJs && locale ) {
                            return (
                                <div key={"size1-" + locale} className={cname("locales-editor-row", {
                                    "one": showLocales.toArray().length === 1,
                                    "two": showLocales.toArray().length === 2,
                                    "three":showLocales.toArray().length === 3
                                })}>
                                    <div className={"locales-editor-title-locale"}>
                                        {locale}
                                    </div>
                                    <table className="table table-hover text-left">
                                        <thead>
                                            <tr>
                                                <th style={{width: "30%", fontSize: "15px"}}>Имя</th>
                                                <th style={{width: "70%", fontSize: "15px"}}>Значение</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {allKeysJs.sort().map(key => {
                                                if( typeof  showValueJs[key] === "string" ) {
                                                    let currentPath = selectedPath.slice(0)
                                                    currentPath.push(key);
                                                    return (
                                                        <tr key={"path1-value-"+key} >
                                                            <td  style={{width: "30%"}}>
                                                                <div>
                                                                    <CopyButton
                                                                        className={"btn btn-grey"}
                                                                        text={currentPath.join(".")}
                                                                        tip="tooltip.copy_password"
                                                                        dataPlace="top"
                                                                    />
                                                                    {key}
                                                                </div>
                                                            </td>
                                                            <td  style={{width: "70%"}}>
                                                                <LocalesTextareaForm
                                                                    saveData={this.saveFormData}
                                                                    locale={locale}
                                                                    path={showPath.toArray()}
                                                                    name={key}
                                                                    value={locales.getIn([locale].concat(selectedPath).concat(key))} />
                                                            </td>
                                                        </tr>
                                                    );
                                                }
                                            })}
                                        </tbody>
                                    </table>
                                    <div className={"locales-editor-title-locale"}>
                                        {locale}
                                    </div>
                                </div>
                            );
                        }
                    }) : null }
                </div>

                {showLocales.size ? <ToggleNewKey
                    allKeysJs={allKeysJs}
                    currentKeys={currentKeys}
                    addJsonKey={this.addJsonKey}
                    downloadLocales={this.downloadLocales}
                    removeJsonKey={this.removeJsonKey}
                    showValue={showValueJs}
                    {...this.state}
                /> : null}
            </div>
        );
    }
}


import React from "react";
import {Switch, Route} from "react-router-dom";

import LocalesEditorDownload from "./LocalesEditorDownload";
import LocalesEditorSave from "./LocalesEditorSave";

export default class LocalesEditorPage extends React.Component {
    render() {
        return (
            <Switch>
                <Route
                    path={`/locales-editor/download`}
                    exact
                    render={() => <LocalesEditorDownload {...this.props} />}
                />
                <Route
                    path={`/locales-editor/save`}
                    exact
                    render={() => <LocalesEditorSave {...this.props} />}
                />
            </Switch>
        );
    }

}
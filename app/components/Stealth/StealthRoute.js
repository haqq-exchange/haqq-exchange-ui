import LoadingIndicator from "Components/LoadingIndicator";
import Loadable from "react-loadable";


const StealthMain = Loadable({
    loader: () => import(/* webpackChunkName: "StealthMain" */ "./StealthMain"),
    loading: LoadingIndicator
});
const StealthContact = Loadable({
    loader: () => import(/* webpackChunkName: "StealthContact" */ "./StealthContact"),
    loading: LoadingIndicator
});
const StealthAccount = Loadable({
    loader: () => import(/* webpackChunkName: "StealthContact" */ "./StealthAccount"),
    loading: LoadingIndicator
});
const Page404 = Loadable({
    loader: () => import(/* webpackChunkName: "StealthContact" */ "Components/Page404/Page404"),
    loading: LoadingIndicator
});


export default {
    Page404,
    StealthMain,
    StealthAccount,
    StealthContact
};
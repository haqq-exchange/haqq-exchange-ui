import React from "react";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
import Translate from "react-translate-component";
import DefaultModal from "Components/Modal/DefaultModal";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";


class ShowConfirmationModal extends DefaultBaseModal {

    // state = { value: null };

    closeModal = () => {
        const {modalId} = this.props;
        ModalActions.hide(modalId);
    };

    render () {
        const {isOpenModal} = this.state;
        const {modalId, data} = this.props;

        let confirmation;
        if( data && data.confirmation && data.confirmation.length ) {
            confirmation = data.confirmation[0];
        }

        // console.log("PrivateAccountModal isOpenModal", this.props);
        // console.log("PrivateAccountModal isOpenModal", this.state);


        return (
            <DefaultModal
                id={modalId}
                isOpen={isOpenModal}
                className={modalId}
                onRequestClose={this.closeModal}
            >
                <div className="modal fadeIn is-active">
                    <div className="modal-header">
                        <button type={"button"} onClick={this.closeModal} className="close-button">×</button>
                    </div>
                    <div className="modal-content">
                        <Translate content={"Stealth.modal_title_show_confirm"} component={"h3"}  />

                        <textarea  id="" cols="50" rows="5" defaultValue={confirmation} />

                        <Translate content={"Stealth.modal_info_text_show_confirm"} component={"div"}  />
                    </div>
                </div>
            </DefaultModal>
        );
    }
}

ShowConfirmationModal.defaultProps = {
    modalId: "show_confirmation_modal"
};


class ShowConfirmationModalContainer extends React.Component {
    render() {
        // console.log("ModalStore.getState()", ModalStore.getState());
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    data: () => ModalStore.getState().data["show_confirmation_modal"],
                    resolve: () => ModalStore.getState().resolve
                }}
            >  
                <ShowConfirmationModal {...this.props} />
            </AltContainer>
        );
    }
}
export default ShowConfirmationModalContainer;
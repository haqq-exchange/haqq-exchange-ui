import React from "react";
import Translate from "react-translate-component";
import { FetchChain, ChainStore } from "deexjs";
import AmountSelector from "components/Utility/AmountSelector";
import AccountStore from "stores/AccountStore";
import ConfidentialWallet from "stores/ConfidentialWallet";
import AccountSelector from "components/Account/AccountSelector";
import TransactionConfirmStore from "stores/TransactionConfirmStore";
import { Asset } from "common/MarketClasses";
import { debounce, isNaN } from "lodash-es";
import { checkBalance, checkFeeStatusAsync, shouldPayFeeWithAssetAsync } from "common/trxHelper";
import BalanceComponent from "components/Utility/BalanceComponent";
import utils from "common/utils";
import counterpart from "counterpart";
import { connect } from "alt-react";
import classnames from "classnames";
import PropTypes from "prop-types";
import BindToChainState from "Components/Utility/BindToChainState";
import SettingsStore from "stores/SettingsStore";
import assetConfig from "config/asset";
import ModalActions from "actions/ModalActions";
import LoadingIndicator from "Components/LoadingIndicator";
import FormattedAsset from "Components/Utility/FormattedAsset";
import cookies from "cookies-js";

class SendTransfer extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    static defaultProps = {

    };

    constructor(props) {
        super(props);
        this.state     = SendTransfer.getInitialState(props);
        this.nestedRef = null;

        this.onTrxIncluded = this.onTrxIncluded.bind(this);

        this._updateFee      = debounce(this._updateFee.bind(this), 250);
        this._checkFeeStatus = this._checkFeeStatus.bind(this);
        this._checkBalance   = this._checkBalance.bind(this);
        this.toChanged       = this.toChanged.bind(this);
        this.onAmountChanged = this.onAmountChanged.bind(this);
        this.onFeeChanged    = this.onFeeChanged.bind(this);


    }

    static getInitialState(props) {

        let assetKey = SettingsStore.getState().starredKey;
        const DEEX_ID = assetConfig[assetKey].id;
        let asset     = ChainStore.getAsset(props.asset_id || DEEX_ID);
        let assetProps     = ChainStore.getAsset(props.asset_id || DEEX_ID);

        console.log("asset", asset);
        console.log("props", props);
        console.log("DEEX_ID", DEEX_ID);

        return {
            from_name: "",
            to_name: "",
            from_account: null,
            to_account: null,
            orig_account: null,
            amount: "",
            asset_id: assetProps.get("id"),
            asset: assetProps,
            asset_types: [],
            memo: "",
            error: null,
            knownScammer: null,
            feeAsset: asset,
            fee_asset_id: asset.get("id"),
            fee_asset_types: [asset.get("id")],

            feeAmount: new Asset({
                asset_id: asset.get("id"),
                precision: asset ? asset.get("precision") : 4,
                amount: 0
            }),
            feeStatus: {},
            maxAmount: false,
            hidden: false,
            loading: false
        };
    }

    componentDidMount(){
        this._initForm();
    }

    isBlindTransfer(from, to) {
        let account_type = AccountStore.getAccountType(from);
        if (account_type === "private_account" || account_type === "private_contact") return true;
        account_type = AccountStore.getAccountType(to);
        return account_type === "private_account" || account_type === "private_contact";
    }

    blindTransfer (amount, asset) {
        const { stealthUpdate, getCurrentReceipts, queryBlindBalance } = this.props;
        const {from_name, to_name , from_account , to_account , feeAmount } = this.state;
        const from_account_type = AccountStore.getAccountType(from_name);
        const to_account_type = AccountStore.getAccountType(to_name);
        this.setState({loading: true});

        // console.log("blindTransfer this.props", this.props);
        // debugger;

        if( from_account_type === "my_account" && this.isPrivateAccount(to_account_type) ) {
            const to = to_name.slice(1);

            ConfidentialWallet.blindToTransfer(from_account.get("id"), asset.get("id"), [[to, parseFloat(amount)]], true, feeAmount)
                .then(result => {
                    ModalActions.hide("send_stealth_modal").then(()=>{
                        this.setState({loading: false});
                        getCurrentReceipts();
                        ModalActions.show("show_confirmation_modal", {
                            confirmation: result.confirmation_receipts
                        });
                    });
                });
        } else if ( this.isPrivateAccount(from_account_type) && this.isPrivateAccount(to_account_type) ) {
            const from = from_name.slice(1);
            const to = to_name.slice(1);
            ConfidentialWallet.blindTransfer(from, to, parseFloat(amount), asset.get("id"), true)
                .then(res => {
                    // console.log("-- blindTransfer res -->", res);
                    this.setState({loading: false});
                    queryBlindBalance();
                    getCurrentReceipts();
                    ModalActions.hide("send_stealth_modal").then(()=>{
                        ModalActions.show("show_confirmation_modal", {
                            confirmation: [res.confirmation_receipt]
                        });
                    });


                }).catch(error => {
                    console.error("-- blindTransfer error -->", error);
                    this.setState({error: error.message, loading: false});
                });

        } else if ( this.isPrivateAccount(from_account_type) && !this.isPrivateAccount(to_account_type) ) {
            const from = from_name.slice(1);
            // const to = to_name.slice(1);

            ConfidentialWallet.transferFromBlind(from, to_account.get("id"), parseFloat(amount), asset.get("id"), true)
                .then(res => {
                    // console.log("-- transferFromBlind res -->", res);
                    this.setState({loading: false});
                    ModalActions.hide("send_stealth_modal").then(()=>{
                        stealthUpdate(res);
                    });
                }).catch(error => {
                    console.error("-- transferFromBlind error -->", error);
                    this.setState({error: error.message, loading: false});
                    //TransactionConfirmActions.error(error.message);
                });
        }
    }

    isPrivateAccount = name => {
        return ["private_account", "private_contact"].indexOf(name) !== -1;
    };


    onSubmit(e) {
        e.preventDefault();
        this.setState({ error: null });

        const {
            from_name,
            to_name,
            asset}          = this.state;
        let { amount }   = this.state;




        if (this.isBlindTransfer(from_name, to_name)) {
            this.blindTransfer(amount, asset);
        }


    }

    _initForm () {
        const _this = this;
        const { to_name, from_name , currentAccount, asset_id } = this.props;
        //debugger;
        if (to_name && to_name !== from_name) {
            FetchChain("getAccount", to_name, undefined, {
                [to_name]: false
            }).then((_account) => {
                _this.setState({
                    to_name: _account ? _account.get("name") : to_name,
                    to_account: _account ,
                });
            });
        }

        if (from_name && !this.isPrivateAccount(from_name)) {
            FetchChain("getAccount", from_name, undefined, {
                [from_name]: false
            }).then((_account) => {
                _this.setState({
                    from_name: _account ? _account.get("name") : from_name,
                    from_account: _account
                }, () =>  _this.updateCurrency() );
            });
        } else {
            this.setState({ from_name: currentAccount });
        }

        if ( asset_id && this.state.asset_id !== asset_id) {

            FetchChain("getAsset", asset_id, undefined, {})
                .then(_asset => {
                    console.log("_asset", _asset);
                    this.setState({
                        asset_id: asset_id,
                        asset: _asset
                    });
                });
        }
    }

    // shouldComponentUpdate(np, ns) {
    // if (ns.open && !this.state.open) this._checkFeeStatus(ns);
    // if (!ns.open && !this.state.open) return false;
    // return true;
    // }

    // componentDidUpdate(np) {}

    _checkBalance() {
        const { feeAmount, amount, from_account, asset } = this.state;
        if (!asset || !from_account) return;
        this._updateFee();
        //debugger;

        // console.log("_checkBalance feeAmount", feeAmount)
        const balanceID    = from_account.getIn(["balances", asset.get("id")]);
        // console.log("_checkBalance balanceID", balanceID)
        const feeBalanceID = from_account.getIn([
            "balances",
            feeAmount.asset_id
        ]);
        // console.log("_checkBalance feeBalanceID", feeBalanceID);
        if (!asset || !from_account) return;
        if (!balanceID)
            return this.setState({
                balanceError: true,
                typeBalanceError: "balanceID"
            });

        let balanceObject    = ChainStore.getObject(balanceID);
        // console.log("_checkBalance balanceObject >>> ", balanceObject);
        // console.log("_checkBalance feeBalanceID >>> ", feeBalanceID);
        let feeBalanceObject = feeBalanceID ? ChainStore.getObject(feeBalanceID) : null;

        // console.log("_checkBalance feeBalanceObject", feeBalanceObject);
        if (!feeBalanceObject)
            return this.setState({
                balanceError: true,
                typeBalanceError: "feeBalance"
            });
        if (!balanceObject || !feeAmount) return;
        if (!amount)
            return this.setState({
                balanceError: false
            });

        console.log("_checkBalance hasBalance", amount,
            asset,
            feeAmount,
            balanceObject);

        const hasBalance = checkBalance(
            amount,
            asset,
            feeAmount,
            balanceObject
        );

        // console.log("_checkBalance hasBalance", hasBalance);

        if (hasBalance === null) return;
        this.setState({ balanceError: !hasBalance });
    }

    _checkFeeStatus(state = this.state) {
        let { from_account } = state;
        if (!from_account) return;

        const assets  = Object.keys(from_account.get("balances").toJS()).sort(
            utils.sortID
        );
        let feeStatus = {};
        let p         = [];
        assets.forEach(a => {
            p.push(
                checkFeeStatusAsync({
                    accountID: from_account.get("id"),
                    feeID: a,
                    options: ["price_per_kbyte"],
                    data: {
                        type: "memo",
                        content: this.state.memo
                    }
                })
            );
        });
        Promise.all(p)
            .then(status => {
                assets.forEach((a, idx) => {
                    feeStatus[a] = status[idx];
                });
                if (!utils.are_equal_shallow(this.state.feeStatus, feeStatus)) {
                    this.setState({
                        feeStatus
                    });
                }
                this._checkBalance();
            })
            .catch(err => {
                console.error(err);
            });
    }

    _setPrivateTotal(asset_id, balance, fee, fee_asset_id) {
        //debugger;
        const { feeAmount , asset } = this.state;
        let assetBalance = new Asset({
            asset_id,
            amount: balance,
            precision: asset.get("precision"),
        });
        console.log("_setPrivateTotal feeAmount" , asset_id, balance, fee, fee_asset_id );
        // console.log("_setPrivateTotal feeAmount", feeAmount);
        // console.log("_setPrivateTotal asset", asset);
        // console.warn("_setPrivateTotal assetBalance.equals(feeAmount)", assetBalance.equals(feeAmount));
        if( assetBalance.asset_id === feeAmount.asset_id ) {
            assetBalance.minus(feeAmount);
        }
        //let assetBalance2 = assetBalance.minus(feeAmount);
        // console.log("_setPrivateTotal feeAmount", feeAmount, assetBalance);
        // console.log("_setPrivateTotal feeAmount", assetBalance.getAmount({ real: true }));

        this.setState({
            amount: assetBalance.getAmount({ real: true })
        });

    }

    _setTotal(asset_id, balance_id) {
        const _this = this;
        const { feeAmount } = this.state;
        //debugger;
        // let balanceObject   = ChainStore.getObject(balance_id);
        // let transferAsset   = ChainStore.getObject(asset_id);

        FetchChain("getObject", [asset_id,balance_id], undefined, {}).then(data=> {
            let balanceObject   = data.get(1);
            let transferAsset   = data.get(0);
            let balance;
            balance = new Asset({
                amount: balanceObject.get("balance"),
                asset_id: transferAsset.get("id"),
                precision: transferAsset.get("precision")
            });

            if (balanceObject) {
                if (feeAmount.asset_id === balance.asset_id) {
                    balance.minus(feeAmount);
                }
                _this.setState({
                    maxAmount: true,
                    amount: balance.getAmount({ real: true })
                },  _this._checkBalance);
            }
        });
    }

    _getAvailableAssets(state = this.state) {
        const { from_account, from_error, fee_asset_id } = state;
        let asset_types                    = [],
            letDeexId                    = fee_asset_id,
            fee_asset_types              = [letDeexId];
        if (!(from_account && from_account.get("balances") && !from_error)) {
            return { asset_types, fee_asset_types };
        }
        let account_balances = state.from_account.get("balances").toJS();
        asset_types          = Object.keys(account_balances).sort(utils.sortID);
        for (let key in account_balances) {
            if (account_balances.hasOwnProperty(key)) {
                let balanceObject = ChainStore.getObject(account_balances[key]);
                if (balanceObject && balanceObject.get("balance") === 0) {
                    asset_types.splice(asset_types.indexOf(key), 1);
                }
            }
        }
        return { asset_types, fee_asset_types };
    }

    _updateFeeBlind() {
        const _this = this;
        const {from_name, to_name, fee_asset_types, fee_asset_id} = this.state;
        // debugger;
        let from_account_type = AccountStore.getAccountType(from_name);
        let to_account_type = AccountStore.getAccountType(to_name);

        /* public -> private */
        if( from_account_type === "my_account" && this.isPrivateAccount(to_account_type) ) {

            // console.log("_updateFeeBlind from_account_type");
            ConfidentialWallet.getFeeFromAccount(fee_asset_id).then(result => {
                console.log("_updateFeeBlind result getFeeToBlind", result);
                _this.setState({
                    feeAmount: new Asset(result)
                });
            });

        } else if ( this.isPrivateAccount(from_account_type) && this.isPrivateAccount(to_account_type) ) {
            /* private -> private */

            ConfidentialWallet.getFeeFromBlind(fee_asset_id).then(result => {
                console.log("_updateFeeBlind result getFeeToAccount", result);
                _this.setState({
                    feeAmount: new Asset(result)
                });
            });

        } else  {
            /* private -> public */
            // console.log("_updateFeeBlind private -> public");
            ConfidentialWallet.getFeeFromBlind(fee_asset_id).then(result => {
                console.log("_updateFeeBlind result getFeeToAccount", result);
                _this.setState({
                    feeAmount: new Asset(result)
                });
            });
        }

        /* if ( this.isPrivateAccount(from_account_type)  ) {

        } else if ( this.isPrivateAccount(to_account_type)  ) {
            console.log("_updateFeeBlind to_account_type");

        } else {
            this._updateFee(true);
        }*/
    }

    _updateFee(isUpdate) {
        // debugger;
        let { fee_asset_id, from_account, asset_id } = this.state;
        const { fee_asset_types } = this._getAvailableAssets(this.state);

        if (
            fee_asset_types.length === 1 &&
            fee_asset_types[0] !== fee_asset_id
        ) {
            fee_asset_id = fee_asset_types[0];
        }
        if (!from_account) return null;
        let from_account_type = AccountStore.getAccountType(from_account.get("name"));
        //debugger;


        if(isUpdate) {
            checkFeeStatusAsync({
                accountID: from_account.get("id"),
                feeID: fee_asset_id,
                options: ["price_per_kbyte"],
                data: {
                    type: "memo",
                    content: this.state.memo
                }
            }).then(({ fee, hasBalance, hasPoolBalance }) => {
                shouldPayFeeWithAssetAsync(from_account, fee).then(
                    should =>
                        should ?
                            this.setState({
                                fee_asset_id: asset_id,
                                hasPoolBalance,
                                hasBalance
                            }, this._updateFee) :
                            this.setState({
                                feeAmount: fee,
                                fee_asset_id: fee.asset_id,
                                hasBalance,
                                hasPoolBalance,
                                error: !hasBalance || !hasPoolBalance
                            })
                );
            });
        } else {
            this._updateFeeBlind();
        }
    }


    toChanged(to_name) {
        // console.log("to_name", to_name);
        this.setState({
            to_name,
            error: null
        }, this.updateCurrency);
    }

    onToAccountChanged(to_account) {
        this.setState({ to_account, error: null });
    }

    fromChanged(from_name) {
        let accountData = {
            from_name: null,
            from_account: null,
            error: null
        };
        // console.logconsole.log("from_name", from_name);
        if (from_name) {
            FetchChain("getAccount", from_name).then(account => {
                accountData = {
                    from_name,
                    from_account: account
                };
                this.setState(accountData, this.updateCurrency);
            }).catch(() => {
                this.setState(accountData);
            });
        } else {
            this.setState(accountData);
        }

    }

    /*
    * @description В зависимости от from_name делаем новый статус монет
    * */
    updateCurrency(){
        const {from_name, to_name, fee_asset_id, asset_id} = this.state;
        let to_account_type = AccountStore.getAccountType(to_name);
        let from_account_type = AccountStore.getAccountType(from_name);
        // console.log("updateCurrency", this.isPrivateAccount(from_account_type), from_name);
        if( this.isPrivateAccount(from_account_type) ) {
            // console.log("from_name", from_name);

            ConfidentialWallet.getBlindBalances(from_name.slice(1)).then(res => {
                let resJs = res.toJS(),
                    resJsKeys = Object.keys(resJs);
                console.log("-- getBlindBalances -->",resJsKeys, from_name, res.toJS() );
                this.setState({
                    fee: resJs[fee_asset_id],
                    //fee_asset_id: resJsKeys[0],
                    fee_asset_types: resJsKeys,

                    hasBalance: !!res,
                    hasPoolBalance:  !!res,
                    //asset_id: resJsKeys[0],
                    private_balance: res.get(asset_id),
                    asset_types: resJsKeys,
                }, this._updateFeeBlind);
            });
        } else if ( this.isPrivateAccount(to_account_type) ) {
            const {asset_types, fee_asset_types } = this._getAvailableAssets();

            this.setState({
                fee: fee_asset_id,
                hasBalance: true,
                hasPoolBalance:  true,
                asset_types,
                fee_asset_types
            }, this._updateFeeBlind);
        }
    }

    onFromAccountChanged(from_account) {
        console.log("from_account", from_account);
        try {
            if( from_account ) {
                this.setState({from_account, error: null}, () => {
                    //debugger;
                    this._updateFee();
                    this._checkFeeStatus();
                });
            }
        } catch (e) {

        }
    }

    onAmountChanged({ amount, asset }) {
        let { from_name , fee_asset_id } = this.state;
        let from_account_type = AccountStore.getAccountType(from_name);


        console.log("onAmountChanged amount, asset", amount, asset);
        if (!asset) { return; }

        this.setState({
            amount,
            asset,
            asset_id: asset.get("id"),
            fee_asset_id: this.isPrivateAccount(from_account_type) ? asset.get("id") : fee_asset_id ,
            error: null,
            maxAmount: false
        }, () => {

            // console.log("onAmountChanged state", this.state );
            if( this.isPrivateAccount(from_account_type) ) {
                this.updateCurrency();
                //this._updateFeeBlind();
            } else {
                this._checkBalance();
            }
        });
    }

    onFeeChanged({ asset }) {
        //debugger;
        let { from_name , asset_id } = this.state;
        let from_account_type = AccountStore.getAccountType(from_name);
        let assetProps     = ChainStore.getAsset(this.isPrivateAccount(from_account_type) ? asset.get("id") : asset_id);
        console.log("asset", asset);
        this.setState(
            {
                feeAsset: asset,
                fee_asset_id: asset.get("id"),
                asset_id: assetProps.get("id"),
                asset: assetProps,

                error: null
            },
            () => {
                // console.log("onAmountChanged state", this.state );
                if( this.isPrivateAccount(from_account_type) ) {
                    this.updateCurrency();
                    //this._updateFeeBlind();
                } else {
                    this._updateFee();
                }
            }
        );
    }

    onMemoChanged(e) {
        let { from_account, from_error, maxAmount , asset_id } = this.state;
        let assetKey = SettingsStore.getState().starredKey;
        let current_asset_id = assetConfig[assetKey].id;
        if (
            from_account &&
            from_account.get("balances") &&
            !from_error &&
            maxAmount &&
            asset_id === current_asset_id
        ) {
            let account_balances = from_account.get("balances").toJS();
            this._setTotal(
                current_asset_id,
                account_balances[current_asset_id]
            );
        }
        //debugger;
        this.setState({ memo: e.target.value }, this._updateFee);
    }

    onTrxIncluded(confirm_store_state) {
        if (
            confirm_store_state.included &&
            confirm_store_state.broadcasted_transaction
        ) {
            // this.setState(Transfer.getInitialState());
            TransactionConfirmStore.unlisten(this.onTrxIncluded);
            TransactionConfirmStore.reset();
        } else if (confirm_store_state.closed) {
            TransactionConfirmStore.unlisten(this.onTrxIncluded);
            TransactionConfirmStore.reset();
        }
    }

    render() {
        const {currentAccount, onShowForm } = this.props;
        let {

            to_name,
            from_name,

            from_account,
            to_account,
            asset,
            asset_id,
            feeAmount,
            amount,
            error,
            loading,

            asset_types,
            fee_asset_types,

            feeAsset,
            fee_asset_id,
            balanceError,
            hasPoolBalance,
            private_balance,
            hasBalance,
            hidden } = this.state;

        let cookiePrefix = cookies.get("log_c6930e40");

        console.log("render feeAmount", feeAmount);
        if(asset)  console.log("render asset", asset.toJS());

        // console.log("render this.props", this.props);
        // console.log("render this.state", this.state);

        let from_my_account = AccountStore.isMyAccount(from_account);
        let from_error = from_account && !from_my_account;

        let balance = null;
        let balance_fee = null;


        let from_account_type = AccountStore.getAccountType(from_name);
        let to_account_type = AccountStore.getAccountType(to_name);

        if( this.isPrivateAccount(from_account_type) && private_balance ) {
            balance = (
                <span className={"send-transfer-balance"}>
                    <Translate component="span" content="transfer.available"/>:{" "}
                    <span
                        className={classnames("send-transfer-balance-all", {"has-error": balanceError})}
                        onClick={() => this._setPrivateTotal(asset_id, private_balance, fee, fee_asset_id)}>
                        <FormattedAsset
                            amount={private_balance}
                            asset={asset_id}
                        />
                    </span>
                </span>
            );
        }

        // Estimate fee
        let fee = feeAmount.getAmount({ real: true });
        if (from_account && from_account.get("balances") ) {
            //debugger;
            let account_balances = from_account.get("balances").toJS();
            console.log("render account_balances", account_balances);
            let _error           = balanceError ? "has-error" : "";
            if (asset_types.length === 1)
                asset = ChainStore.getAsset(asset_types[0]);

            if (asset_types.length > 0) {
                let current_asset_id = asset ? asset.get("id") : asset_types[0];
                // console.log("render current_asset_id", current_asset_id);
                let feeID            = feeAsset ? feeAsset.get("id") : fee_asset_id;

                balance = (
                    <span className={"send-transfer-balance"}>
                        <Translate component="span" content="transfer.available"/>
                        :{" "}
                        <span className={classnames("send-transfer-balance-all" , {"has-error": balanceError})}
                            onClick={()=>this._setTotal(current_asset_id, account_balances[current_asset_id], fee, feeID)}>
                            <BalanceComponent balance={account_balances[current_asset_id]} />
                        </span>
                    </span>
                );

                // console.log("render feeID", feeID, current_asset_id, balanceError)

                if (feeID === current_asset_id && balanceError) {
                    balance_fee = (
                        <span className={classnames("send-transfer-balance-fee" , {"has-error": balanceError})}>
                            <Translate content="transfer.errors.insufficient"/>
                        </span>
                    );
                }
            } else {
                balance     = (
                    <span className={classnames({"has-error": balanceError})}>
                        <Translate content="transfer.errors.noFunds"/>
                    </span>
                );
                balance_fee = (
                    <span className={classnames({"has-error": balanceError})}>
                        <Translate content="transfer.errors.noFunds"/>
                    </span>
                );
            }
        }

        const amountValue      = parseFloat(
            String.prototype.replace.call(amount, /,/g, "")
        );
        const isAmountValid    = amountValue && !isNaN(amountValue);
        const isSendNotValid   =
                !from_account && to_account_type === "private_account" ||
                !to_account && to_account_type === "private_account" ||
                !from_account_type ||
                !to_account_type ||
                !isAmountValid ||
                !asset ||
                !hasBalance ||
                !hasPoolBalance ||
                from_error ||
                balanceError ;


        let tabIndex = this.props.tabIndex || 0; // Continue tabIndex on props count

        if( cookiePrefix ) {
            window.console.log("isSendNotValid", isSendNotValid);
            window.console.log("from_account", from_account);
            window.console.log("to_account", to_account);
            window.console.log("from_account_type", from_account_type);
            window.console.log("to_account_type", to_account_type);
            window.console.log("isAmountValid", isAmountValid);
            window.console.log("asset", asset);
            window.console.log("hasBalance", hasBalance);
            window.console.log("hasPoolBalance", hasPoolBalance);
            window.console.log("from_error", from_error);
            window.console.log("balanceError", balanceError);
        }

        return (
            <div className="send-transfer">
                {/*<div className="send-transfer-header">
                    <Translate unsafe component={"div"} className={"send-transfer-title"} content={"modal.send.header"} with={{ fromName: currentAccount }}/>
                    <Translate component={"div"} className={"send-transfer-subtitle"}  content="transfer.header_subheader"/>
                </div>*/}
                <form noValidate>
                    {/* FROM */}
                    <div className={"send-transfer-row from"} >
                        <AccountSelector
                            label="transfer.from"
                            allowUppercase={true}
                            accountName={from_name}
                            account={from_account}
                            onChange={(name)=>this.fromChanged(name)}
                            onAccountChanged={(name)=>this.onFromAccountChanged(name)}
                            size={60}
                            tabIndex={tabIndex++}
                            hideImage
                        />
                    </div>

                    <div className={"send-transfer-row to"} >
                        <AccountSelector
                            label="transfer.to"
                            allowUppercase={true}
                            accountName={to_name}
                            account={to_account}
                            onChange={this.toChanged}
                            onAccountChanged={this.onToAccountChanged.bind(
                                this
                            )}
                            size={60}
                            tabIndex={tabIndex++}
                            hideImage
                        />
                    </div>

                    <div className={"send-transfer-row amount"}>
                        {/*  A M O U N T  */}
                        {console.log("amount", amount)}
                        {console.log("asset_id", asset_id)}
                        {console.log("asset_types", asset_types)}
                        {console.log("balance", balance)}
                        <AmountSelector
                            label="transfer.amount"
                            amount={amount}
                            onChange={this.onAmountChanged}
                            asset={asset_id}
                            assets={asset_types}
                            display_balance={balance}
                            tabIndex={tabIndex++}
                        />
                    </div>{/*{asset_id ? asset_id : null }*/}


                    <div className={"send-transfer-row fee"}>
                        <div className="no-margin no-padding">
                            {/*  F E E  */}
                            <div id="txFeeSelector" className="small-12">
                                <AmountSelector
                                    label="transfer.fee"
                                    disabled={true}
                                    amount={fee}
                                    onChange={this.onFeeChanged}
                                    asset={fee_asset_id}
                                    assets={fee_asset_types}
                                    display_balance={
                                        balance_fee
                                    }
                                    tabIndex={tabIndex++}
                                    error={
                                        !this.state.hasPoolBalance ? "transfer.errors.insufficient" : null
                                    }
                                    scroll_length={2}
                                />
                            </div>
                            {!hasBalance && (
                                <div
                                    className="has-error"
                                    style={{ marginTop: 5 }}
                                >
                                    <Translate content="transfer.errors.noFeeBalance"/>
                                </div>
                            )}
                        </div>
                    </div>

                    <div className={"send-transfer-row group-button"}>
                        <button
                            className={classnames("btn btn-gray")}
                            type="button"
                            value="Cancel"
                            onClick={()=>ModalActions.hide("send_stealth_modal")} >
                            <Translate component="span" content="transfer.cancel"/>
                        </button>
                        <button
                            className={classnames("btn btn-red", {disabled: isSendNotValid})}
                            disabled={isSendNotValid || loading}
                            type="submit"
                            onClick={
                                !isSendNotValid ? this.onSubmit.bind(this) : null
                            }
                            tabIndex={tabIndex++} >
                            {!loading
                                ? <Translate component="span" content="transfer.send"/>
                                : <LoadingIndicator style={{ height: 17}} type={"three-bounce"}/> }

                        </button>
                    </div>
                </form>
            </div>
        );
    }
}

const SendTransferBind = BindToChainState(SendTransfer);

class SendTransferConnectWrapper extends React.Component {
    render() {
        return <SendTransferBind {...this.props} />;
    }
}

export default connect(
    SendTransferConnectWrapper,
    {
        listenTo() {
            return [AccountStore];
        },
        getProps() {
            return {
                currentAccount: AccountStore.getState().currentAccount
                    || AccountStore.getState().passwordAccount,
                passwordAccount: AccountStore.getState().passwordAccount
            };
        }
    }
);

import React from "react";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
// import AccountActions from "actions/AccountActions";
import Translate from "react-translate-component";
import DefaultModal from "Components/Modal/DefaultModal";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";
// import AccountNameInput from "./../Forms/AccountNameInput";
import SendStealth from "./SendStealth";



class PrivateContactModal extends DefaultBaseModal {

    constructor(props) {
        super(props);
        this.state = {
            label: null,
            key: null
        };

    }


    closeModal = () => {
        const {modalId} = this.props;
        ModalActions.hide(modalId);
    };



    render () {
        const {isOpenModal} = this.state;
        const {modalId, data} = this.props;


        console.log("modalId, data ", modalId, data);

        return (
            <DefaultModal
                id={modalId}
                isOpen={isOpenModal}
                className={modalId}
                onRequestClose={this.closeModal}
            >
                <div className="modal fadeIn is-active">
                    <div className="modal-header">
                        <span onClick={this.closeModal} className="close-button">×</span>
                    </div>
                    <div className="modal-content">
                        <SendStealth {...data} />
                    </div>
                </div>

            </DefaultModal>
        );
    }
}

PrivateContactModal.defaultProps = {
    modalId: "send_stealth_modal"
};


class PrivateContactModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    data: () => ModalStore.getState().data["send_stealth_modal"],
                    modals: () => ModalStore.getState().modals,
                    resolve: () => ModalStore.getState().resolve,
                }}
            >
                <PrivateContactModal {...this.props} />
            </AltContainer>
        );
    }
}
export default PrivateContactModalContainer;
import React from "react";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import SettingsStore from "stores/SettingsStore";
import WalletUnlockStore from "stores/WalletUnlockStore";
import WalletDb from "stores/WalletDb";
import BindToChainState from "../Utility/BindToChainState";
import {connect} from "alt-react";
import {default as Immutable, List} from "immutable";
import {Route, Switch} from "react-router-dom";
import ModalStore from "../../stores/ModalStore";
import StealthRoute from "./StealthRoute";

import PrivateAccountModal from "Components/Stealth/PrivateAccountModal";
import PrivateAccountContact from "Components/Stealth/PrivateContactModal";
import SendStelthModal from "Components/Stealth/SendStelthModal";
import DisclaimerModal from "Components/Stealth/DisclaimerModal";
import InformationModal from "Components/Stealth/InformationModal";

import ModalActions from "../../actions/ModalActions";
import utils from "../../lib/common/utils";
import {Asset, Price} from "../../lib/common/MarketClasses";
import ConfidentialWallet from "../../stores/ConfidentialWallet";

import "./stealth.scss";

class StealthPage extends React.Component {
    static propTypes = {
    };

    static defaultProps = {
    };

    componentDidMount() {
        const { settings } = this.props;
        console.log(settings.has("disclaimer_hidden"), settings.get("disclaimer_hidden"))
        if( !settings.has("disclaimer_hidden") || !settings.get("disclaimer_hidden") ) {
            ModalActions.show("disclaimer_stealth_modal");
        }
    }

    componentDidUpdate() {
        /*if( !prevProps.private_accounts.equals(this.props.private_accounts) ) {
            ModalActions.hide("add_private_account");
        }
        if( !prevProps.private_contacts.equals(this.props.private_contacts) ) {
            ModalActions.hide("add_private_contact");
        }*/
        // console.log("prevProps private_accounts", prevProps.private_accounts, this.props.private_accounts);
        // console.log("prevProps private_accounts 2", prevProps.private_accounts.equals(this.props.private_accounts));
        // console.log("prevProps private_contacts", prevProps.private_contacts, this.props.private_contacts);
    }


    render() {
        let {
            passwordAccount,
            account_name,
            account
        } = this.props;

        if(!passwordAccount) {
            return <StealthRoute.Page404 />;
        }
        //let isMyAccount = AccountStore.isMyAccount(account_name) && !wallet_locked;

        let passOnProps = {
            account,
            contained: true,
            ...this.props
        };


        return (
            <div className="grid-block page-layout">

                <Switch>

                    <Route
                        path={`/stealth/contact/${account_name}`}
                        exact
                        render={() => <StealthRoute.StealthContact {...passOnProps} />}
                    />
                    <Route
                        path={`/stealth/account/${account_name}`}
                        exact
                        render={() => <StealthRoute.StealthAccount {...passOnProps} />}
                    />
                    <Route
                        path={"/stealth"}
                        exact
                        render={() => <StealthRoute.StealthMain {...passOnProps} />}
                    />
                </Switch>

                <PrivateAccountModal />
                <PrivateAccountContact />
                <SendStelthModal />
                <DisclaimerModal />
                <InformationModal />
            </div>
        );
    }
}
const StealthPageBind = BindToChainState(StealthPage, {
    show_loader: true
});

class StealthPageStoreWrapper extends React.Component {
    render() {
        let account_name = this.props.match.params.name;

        return <StealthPageBind {...this.props} account_name={account_name} />;
    }
}

export default connect(
    StealthPageStoreWrapper,
    {
        listenTo() {
            return [
                WalletDb,
                AccountStore,
                SettingsStore,
                WalletUnlockStore,
                ModalStore,
            ];
        },
        getProps() {
            // console.log("WalletDb.getState()", WalletDb.getState())
            return {
                passwordAccount: AccountStore.getState().passwordAccount,
                myActiveAccounts: AccountStore.getState().myActiveAccounts,
                private_accounts: AccountStore.getState().privateAccounts,
                private_contacts: AccountStore.getState().privateContacts,
                wallet: WalletDb.getWallet(),
                settings: SettingsStore.getState().settings,
                wallet_locked: WalletUnlockStore.getState().locked,
                modals: ModalStore.getState().modals,
            };
        }
    }
);

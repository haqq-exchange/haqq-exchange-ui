import React from "react";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
import DefaultModal from "Components/Modal/DefaultModal";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";
import Translate from "react-translate-component";
import SettingsActions from "actions/SettingsActions";


class PrivateContactModal extends DefaultBaseModal {

    closeModal = () => {
        const {modalId} = this.props;
        ModalActions.hide(modalId);
    };

    hiddenModal = () => {
        this.closeModal();

        SettingsActions.changeSetting({
            setting: "disclaimer_hidden",
            value: true
        });
    };



    render () {
        const {isOpenModal} = this.state;
        const {modalId} = this.props;

        return (
            <DefaultModal
                id={modalId}
                isOpen={isOpenModal}
                className={modalId}
                shouldCloseOnOverlayClick={false}
                shouldCloseOnEsc={false}
                onRequestClose={this.closeModal}
            >
                <div className="modal fadeIn is-active">
                    <div className="modal-header">
                        <button type="button" onClick={this.closeModal} className="close-button">×</button>
                        <Translate content={"Stealth.disclaimer.title"} component={"h3"}  />
                    </div>
                    <div className="modal-content">
                        {/*<Translate content={"Stealth.disclaimer.text1"} component={"p"}  />
                        <Translate content={"Stealth.disclaimer.text2"} component={"p"}  />
                        <Translate content={"Stealth.disclaimer.text-warning"} className={"text-warning"} component={"p"}  />
                        <Translate content={"Stealth.disclaimer.text3"} component={"p"}  />*/}
                        <Translate unsafe className={"stealth-main-disclaimer"} content={__SCROOGE_CHAIN__ ? "Stealth.disclaimer.text_scrooge" : "Stealth.disclaimer.text"} component={"p"} />
                    </div>
                    <div className="modal-footer">
                        <Translate
                            content={"Stealth.disclaimer.hide_btn"}
                            className={"btn btn-green"}
                            onClick={this.hiddenModal}
                            component={"button"}  />
                    </div>
                </div>

            </DefaultModal>
        );
    }
}

PrivateContactModal.defaultProps = {
    modalId: "disclaimer_stealth_modal"
};


class PrivateContactModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    resolve: () => ModalStore.getState().resolve
                }}
            >
                <PrivateContactModal {...this.props} />
            </AltContainer>
        );
    }
}
export default PrivateContactModalContainer;
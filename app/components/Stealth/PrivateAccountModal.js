import React from "react";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
import AccountActions from "actions/AccountActions";
import DefaultModal from "Components/Modal/DefaultModal";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";
import AccountNameInput from "./../Forms/AccountNameInput";
import PrivateKeyInput from "./../Forms/PrivateKeyInput";
import {Checkbox} from "antd";
import Translate from "react-translate-component";
import AccountStore from "stores/AccountStore";
import ConfidentialWallet from "stores/ConfidentialWallet";
import {Redirect} from "react-router-dom";



class PrivateAccountModal extends DefaultBaseModal {

    constructor(props) {
        super(props);
        this.state = {
            isRedirect: false,
            label: null,
            key: null
        };

        this._onCreateClick = this._onCreateClick.bind(this);
        this._onLabelChange = this._onLabelChange.bind(this);
        this._onKeyChange = this._onKeyChange.bind(this);
    }



    closeModal = () => {
        const {modalId} = this.props;
        ModalActions.hide(modalId);
    };

    _onCreateClick(e) {
        if(e) e.preventDefault();

        const {label, private_key, public_key} = this.state;

        try {
            if( private_key ) {
                ConfidentialWallet.setKeyLabel(private_key, label, public_key);
            } else {
                AccountActions
                    .addPrivateAccount(label.slice(1), private_key)
                    .then(res => {
                        // console.log("AccountActions.addPrivateAccount res", res);
                        ModalActions.hide("add_private_account");
                        this.setState({isRedirect: true});
                    });
            }
        }
        catch (error) {
            console.error("-- CreatePrivateAccountModal._onCreateClick -->", error);
        }
    }

    _onLabelChange({value}) {
        if (!value) return;
        this.setState({label: value});
    }

    _onKeyChange(key) {
        this.setState({key});

    }

    render () {
        const {isOpenModal} = this.state;
        const {modalId} = this.props;


        // console.log("PrivateAccountModal isOpenModal", this.props)
        // console.log("PrivateAccountModal isOpenModal", this.state,  isOpenModal, modalId);


        return (
            <DefaultModal
                id={modalId}
                isOpen={isOpenModal}
                className={modalId}
                onRequestClose={this.closeModal}
            >
                <div className="modal fadeIn is-active">
                    <div className="modal-header">
                        <button type={"button"} onClick={this.closeModal} className="close-button">×</button>
                    </div>
                    <div className="modal-content">
                        <Translate content={"Stealth.modal_create_account"} component={"h3"}  />

                        <form  autoComplete="off">
                            <div className="form-group">
                                <AccountNameInput
                                    ref={ref=>this.refLabel=ref}
                                    cheapNameValidate={false}
                                    onChange={this._onLabelChange}
                                    /*onEnter={this._onCreateClick}*/
                                    accountShouldNotExist={false}
                                    className={"private-name"}
                                    prefixSymbol="~"
                                    labelMode
                                />
                                <Translate content={"Stealth.guide.private_account_info"} component={"div"} style={{marginTop: "-25px"}}  />
                            </div>
                            {/*<PrivateKeyInput ref="key" onChange={this._onKeyChange} />*/}
                            <div className="button-group">
                                <Translate
                                    onClick={this._onCreateClick}
                                    content={"Stealth.btn_create_account"} className={"btn btn-green"} component={"button"}  />
                            </div>
                        </form>
                    </div>
                </div>

            </DefaultModal>
        );
    }
}

PrivateAccountModal.defaultProps = {
    modalId: "add_private_account"
};


class PrivateAccountModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    resolve: () => ModalStore.getState().resolve
                }}
            >
                <PrivateAccountModal {...this.props} />
            </AltContainer>
        );
    }
}
export default PrivateAccountModalContainer;
import React from "react";
import moment from "moment";
import {Link} from "react-router-dom";
import Icon from "Components/Icon/Icon";
import { Asset } from "common/MarketClasses";
import ModalActions from "actions/ModalActions";
import PubKey from "Components/Utility/PubKey";
import Translate from "react-translate-component";
import ConfidentialWallet from "stores/ConfidentialWallet";
import ShowConfirmationModal from "./ShowConfirmationModal";
import StelthWrap from "./StelthWrap";
import FormattedAsset from "Components/Utility/FormattedAsset";
import translator from "counterpart";

export default class StealthContact extends StelthWrap{

    getConfirmation = (receipt) => {
        // console.log("receipt", receipt);
        let receipts = [{
            confirmation: receipt.conf,
            decrypted_memo: receipt.data,
            label: receipt.label,
            pub_key: receipt.to_key
        }];

        ModalActions.show("show_confirmation_modal", {
            confirmation: ConfidentialWallet.getConfirmationReceipts(receipts)
        });
        //console.log("ConfidentialWallet.getConfirmationReceipts(receipts)", ConfidentialWallet.getConfirmationReceipts(receipts));
    };


    render(){
        const _this = this;
        let { private_contacts, account_name , passwordAccount , settings} = this.props;
        let { receipts , publicKey , stringPubKey} = this.state;

        if( !private_contacts && !private_contacts.has(account_name) ) {
            return null;
        }




        // console.log("receipts", receipts );
        // console.log("this.props", this.props );

        // console.log("private_accounts, private_contacts ", private_contacts );

        return(
            <div className={"stealth-main"}>
                <div className="stealth-main-title">
                    <Link to={"/stealth"} style={{
                        marginRight: 10
                    }}>
                        <Icon name={"arrow-left"} />
                    </Link>
                    <Translate content={"Stealth.title_private_contact"} /> - { account_name }



                    <button
                        data-step={"1"}
                        data-intro={translator.translate("Stealth.guide.send_transaction", {locale: settings.get("locale")})}
                        type={"button"}
                        style={{marginLeft: 10}} className={"btn btn-red"} onClick={()=>this.createWindow("send_stealth_modal", {
                            to_name: `~${account_name}`,
                            from_name: passwordAccount,
                            queryBlindBalance: () => _this.queryBlindBalance(),
                            getCurrentReceipts: () => _this.getCurrentReceipts()
                        })}>
                        <Translate content={"Stealth.send_transaction"} />
                    </button>

                    <button className={"btn btn-guide"} onClick={this.startGuide}>?</button>
                </div>

                <div className={"stealth-main-wrap column"}>
                    {/* Stealth.guide.private_your_key */}
                    <div className="stealth-main-item full" data-step={"2"}
                        data-intro={translator.translate("Stealth.guide.account_your_key", {locale: settings.get("locale")})}>
                        <Translate content={"Stealth.title_account_key"} className={"stealth-main-title2"} component={"div"}  />
                        <ul className={"stealth-main-list"}>
                            <li  className={"stealth-main-list-row"}>
                                <PubKey getValue={() =>  publicKey } value={stringPubKey}/>
                            </li>
                        </ul>
                    </div>
                    {receipts && (<div className={"stealth-main-item full"} data-step={"3"} data-intro={"your transaction"}>
                        <Translate content={"Stealth.title_your_transaction"} className={"stealth-main-title2"} component={"div"}  />

                        <HeaderReceipts />

                        <ul className={"stealth-main-list"}>
                            {receipts.map((receipt, index)=>{
                                let asset = new Asset(receipt.amount);
                                let amount = asset.getAmount({ real: false });
                                return (
                                    <li key={"receipts-" + index} className={"stealth-main-list-row"} style={{marginTop: 20}}>
                                        <div className={"stealth-main-list-item"}><span>{moment(receipt.date).format("DD.MM.YYYY, HH:mm")}</span> </div>
                                        <div className={"stealth-main-list-item"}>
                                            <span>{receipt.from_label}</span>
                                        </div>
                                        <div className={"stealth-main-list-item"}><span>{receipt.to_label}</span> </div>
                                        <div className={"stealth-main-list-item"}>
                                            <FormattedAsset
                                                decimalOffset={asset.precision}
                                                amount={amount}
                                                asset={asset.asset_id}

                                            />
                                        </div>
                                        <div className={"stealth-main-list-item"}>
                                            <Translate
                                                onClick={()=>this.getConfirmation(receipt)}
                                                content={"Stealth.btn_get_check_sum"}
                                                component={"button"}
                                                className={"btn btn-link"}  />
                                        </div>
                                    </li>
                                );
                            })}
                        </ul>
                    </div>)}

                </div>




                <ShowConfirmationModal />
            </div>
        );
    }
}

const HeaderReceipts = () => {
    return (
        <div className={"stealth-main-header"}>
            <Translate content={"Stealth.table_head_date"} className={"stealth-main-header-item"}/>
            <Translate content={"Stealth.table_head_from_account"} className={"stealth-main-header-item"}/>
            <Translate content={"Stealth.table_head_to_label"} className={"stealth-main-header-item"}/>
            <Translate content={"Stealth.table_head_amount"} className={"stealth-main-header-item"}/>
            <span className={"stealth-main-header-item"} />
        </div>
    );
};
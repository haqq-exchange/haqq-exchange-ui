import React from "react";
import ConfidentialWallet from "../../stores/ConfidentialWallet";
import TransactionConfirmActions from "../../actions/TransactionConfirmActions";
import WalletDb from "../../stores/WalletDb";
import ModalActions from "../../actions/ModalActions";
import guide from "intro.js";
import translator from "counterpart";


export default class StealthWrap extends React.Component{



    constructor(props) {
        super(props);
        this.state = {
            balance: null,
            receipts: null,
            stringPubKey: null
        };
    }

    componentDidMount() {
        // console.log("componentDidMount")
        this.queryBlindBalance();
        this.getPublicKey()
            .then(()=>this.getCurrentReceipts());
    }


    getPublicKey() {
        const _this = this;
        let { account_name } = this.props;
        return new Promise(resolve => {

            try {
                let stringPubKey;
                let publicKey = ConfidentialWallet.getPublicKey(account_name);
                if( publicKey ) {
                    stringPubKey = publicKey.toPublicKeyString();
                }
                // console.log("getPublicKey stringPubKey", stringPubKey);
                // console.log("getPublicKey publicKey", publicKey);
                // console.log("getPublicKey _this", _this);
                _this.setState({
                    stringPubKey,
                    publicKey
                }, resolve );
            } catch (e) {

            }
        });
    }

    getCurrentReceipts() {
        let { wallet } = this.props;
        let { stringPubKey } = this.state;

        try {
            if (wallet) {
                // console.log("stringPubKey", stringPubKey);
                let currentReceipts = wallet.blind_receipts.map(receipts=>{
                    //debugger;
                    // console.log("getCurrentReceipts receipts", receipts, receipts.to_key );
                    // console.log("getCurrentReceipts stringPubKey", stringPubKey );
                    if(stringPubKey && receipts.to_key !== stringPubKey ) return null;
                    return receipts;
                }).filter(a=>a);

                this.setState({
                    receipts: currentReceipts.length ? currentReceipts : null
                });
            }
        } catch (e) {

        }
    }

    queryBlindBalance() {
        let { account_name } = this.props;
        // console.log("queryBlindBalance account_name", account_name);
        if (account_name) {
            // const from = from_name[0] === "~" ? from_name.slice(1) : `@${from_name}`;
            try {
                let fromHistory = ConfidentialWallet.blindHistory(account_name);
                // console.log("queryBlindBalance fromHistory", fromHistory);
                ConfidentialWallet.getBlindBalances(account_name).then(res => {
                    // console.log("queryBlindBalance res", res.toJS());
                    this.setState({
                        balances: res.toJS(),
                        fromHistory
                    });
                });
            } catch (error) {
                console.log("-- getBlindBalances error -->", account_name, error);
            }
        }
    }

    createWindow = ( type_window, data_type = {} ) => {
        if (WalletDb.isLocked()) {
            return ModalActions.show("unlock_wallet_modal_public").then(() => {
                return ModalActions.show(type_window, data_type);
            });
        } else {
            return  ModalActions.show(type_window, data_type);
        }
    };

    startGuide = () => {
        guide
            .introJs()
            .setOptions({
                tooltipClass: "dark",
                highlightClass: "dark",
                showBullets: false,
                hideNext: true,
                hidePrev: true,
                nextLabel: translator.translate(
                    "walkthrough.next_label"
                ),
                prevLabel: translator.translate(
                    "walkthrough.prev_label"
                ),
                skipLabel: translator.translate(
                    "walkthrough.skip_label"
                ),
                doneLabel: translator.translate(
                    "walkthrough.done_label"
                )
            })
            .start()
        /*.oncomplete(function() {

        })*/;
    };



}
import React from "react";
import {Link} from "react-router-dom";
import ModalActions from "actions/ModalActions";
import WalletDb from "stores/WalletDb";
import AccountActions from "actions/AccountActions";
import Translate from "react-translate-component";
import translator from "counterpart";
import guide from "intro.js";

export default class StealthMain extends React.Component{

    shouldComponentUpdate(prevProps){
        return prevProps.settings !== this.props.settings ||
            prevProps.private_accounts !== this.props.private_accounts ||
            prevProps.private_contacts !== this.props.private_contacts;
    }

    createPrivateWindow = ( type_window ) => {
        if (WalletDb.isLocked()) {
            ModalActions.show("unlock_wallet_modal_public").then(() => {
                ModalActions.show(type_window).then(() => {
                    // console.log(type_window, res);
                    console.log("type_window", type_window);
                });

            });
        } else {
            ModalActions.show(type_window).then(() => {
                console.log("type_window", type_window);
            });
        }
    };

    removeContact = (name) => {
        if (WalletDb.isLocked()) {
            ModalActions.show("unlock_wallet_modal_public").then(() => {
                AccountActions.removePrivateContact(name);
            });
        } else {
            AccountActions.removePrivateContact(name);
        }
    };

    startGuide = () => {
        guide
            .introJs()
            .setOptions({
                tooltipClass: "dark",
                highlightClass: "dark",
                showBullets: false,
                hideNext: true,
                hidePrev: true,
                nextLabel: translator.translate(
                    "walkthrough.next_label"
                ),
                prevLabel: translator.translate(
                    "walkthrough.prev_label"
                ),
                skipLabel: translator.translate(
                    "walkthrough.skip_label"
                ),
                doneLabel: translator.translate(
                    "walkthrough.done_label"
                )
            })
            .start()
            /*.oncomplete(function() {

            })*/;
    };

    render(){

        let { private_accounts, private_contacts, settings } = this.props;

        return(
            <div className={"stealth-main"}>
                <div className="stealth-main-title" >
                    <Translate content={"Stealth.name_page"} />

                    <div className="stealth-main-group-btn" >
                        <Translate component={"button"} content={"Stealth.info.instruction"} className={"btn btn-instruction"} onClick={()=>ModalActions.show("information_stealth_modal")}>

                        </Translate>
                        <button className={"btn btn-guide"} onClick={this.startGuide}>?</button>
                    </div>
                </div>

                <div className={"stealth-main-wrap"}>
                    <div className={"stealth-main-item"}>
                        <div className="stealth-main-title2">
                            <Translate content={"Stealth.title_private_account"} />
                            <Translate
                                data-step={"1"}
                                data-intro={translator.translate("Stealth.guide.main_btn_private_account", {locale: settings.get("locale")})}
                                className={"btn btn-green"}
                                type={"button"}
                                component={"button"}
                                onClick={()=>this.createPrivateWindow("add_private_account")}
                                content={"Stealth.btn_create_account"} />
                        </div>

                        <ul className={"stealth-main-list"}>
                            {private_accounts && (private_accounts.map(wallet=>{
                                return (
                                    <li key={wallet} className={"stealth-main-list-row"}>
                                        <Link to={"/stealth/account/" + wallet } className={"stealth-main-list-link"}>
                                            <span>{wallet}</span>
                                        </Link>
                                    </li>
                                );
                            }))}
                        </ul>
                    </div>

                    <div className={"stealth-main-item"}>
                        <div className="stealth-main-title2">
                            <Translate content={"Stealth.title_private_contact"} />
                            <Translate
                                data-step={"2"}
                                data-intro={translator.translate("Stealth.guide.main_btn_private_contact", {locale: settings.get("locale")})}
                                className={"btn btn-green"}
                                component={"button"}
                                onClick={()=>this.createPrivateWindow("add_private_contact")}
                                content={"Stealth.btn_create_contact"} />
                        </div>

                        <ul className={"stealth-main-list"}>
                            {private_contacts && (private_contacts.map(wallet=>{
                                return (
                                    <li key={wallet} className={"stealth-main-list-row"}>
                                        <Link to={"/stealth/contact/" + wallet }  className={"stealth-main-list-link"}>{wallet}</Link>
                                        <span className={"btn"} onClick={()=>this.removeContact(wallet)}>x</span>
                                    </li>
                                );
                            }))}
                        </ul>
                    </div>
                </div>

                <div className={"stealth-main-wrap"}>
                    <div className={"stealth-main-item full"}>
                        <Translate unsafe className={"stealth-main-disclaimer"} content={__SCROOGE_CHAIN__ ? "Stealth.disclaimer.text_scrooge" : "Stealth.disclaimer.text"} component={"p"} />
                    </div>
                </div>
            </div>
        );
    }
}
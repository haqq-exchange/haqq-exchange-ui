import React from "react";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
import DefaultModal from "Components/Modal/DefaultModal";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";
import Translate from "react-translate-component";
import SettingsActions from "actions/SettingsActions";
//import {Pi} from "antd";

const Image = (props) =>{
    return (
        <p><img alt={""} {...props}/></p>
    );
};

class PrivateContactModal extends DefaultBaseModal {

    closeModal = () => {
        const {modalId} = this.props;
        ModalActions.hide(modalId);
    };

    hiddenModal = () => {
        this.closeModal();

        SettingsActions.changeSetting({
            setting: "disclaimer",
            value: "hidden"
        });
    };



    render () {
        const {isOpenModal} = this.state;
        const {modalId} = this.props;

        return (
            <DefaultModal
                id={modalId}
                isOpen={isOpenModal}
                className={modalId}
                onRequestClose={this.closeModal}
            >
                <div className="modal fadeIn is-active">
                    <div className="modal-header">
                        <button type="button" onClick={this.closeModal} className="close-button">×</button>
                        <Translate content={"Stealth.info.title"} component={"h3"}  />
                        <Translate unsafe content={"Stealth.info.sub_title"} component={"h6"}  />
                    </div>
                    <div className="modal-content">
                        <Translate unsafe content={"Stealth.info.title_instruction"} component={"h2"}  />
                        <Translate unsafe content={__SCROOGE_CHAIN__ ? "Stealth.info.step1_scrooge" : "Stealth.info.step1"} component={"p"}  />
                        <Image src={require("./image/1.png")} />
                        <Translate unsafe content={"Stealth.info.step2"} component={"p"}  />
                        <Image src={require("./image/2.png")} />
                        <Translate unsafe content={"Stealth.info.step3"} component={"p"}  />
                        <Image src={require("./image/3.png")} />
                        <Translate unsafe content={"Stealth.info.step4"} component={"p"}  />
                        <Image src={require("./image/4.png")} />
                        <Translate unsafe content={"Stealth.info.step5"} component={"p"}  />
                        <Image src={require("./image/5.png")} />
                        <Translate unsafe content={"Stealth.info.step6"} component={"p"}  />
                        <Image src={require("./image/6.png")} />
                        <Translate unsafe content={"Stealth.info.step7"} component={"p"}  />
                        <Image src={require("./image/7.png")} />
                        <Translate unsafe content={"Stealth.info.step8"} component={"p"}  />
                        <Image src={require("./image/8.png")} />
                        <Translate unsafe content={"Stealth.info.step9"} component={"p"}  />
                        <Image src={require("./image/9.png")} />
                        <Translate unsafe content={"Stealth.info.step9-2"} component={"p"}  />
                        <Image src={require("./image/10.png")} />
                        <Translate unsafe content={"Stealth.info.step10"} component={"p"}  />
                        <Image src={require("./image/11.png")} />
                        <Image src={require("./image/12.png")} />
                        <Translate unsafe content={"Stealth.info.step11"} component={"p"}  />
                        <Image src={require("./image/13.png")} />
                        <Image src={require("./image/14.png")} />
                        <Image src={require("./image/15.png")} />
                        <Translate unsafe content={"Stealth.info.step12"} component={"p"}  />
                        <Image src={require("./image/17.png")} />
                        <Translate unsafe content={__SCROOGE_CHAIN__ ? "Stealth.info.step13_scrooge" : "Stealth.info.step13"} component={"p"}  />
                        <Image src={require("./image/18.png")} />
                        <Translate unsafe content={"Stealth.info.step14"} component={"p"}  />
                        <Image src={require("./image/19.png")} />
                        <Translate unsafe content={"Stealth.info.step15"} component={"p"}  />
                        <Image src={require("./image/20.png")} />
                    </div>
                    <div className="modal-footer">
                        <Translate
                            content={"Stealth.info.hide_btn"}
                            className={"btn btn-red"}
                            onClick={this.hiddenModal}
                            component={"button"}  />
                    </div>
                </div>

            </DefaultModal>
        );
    }
}

PrivateContactModal.defaultProps = {
    modalId: "information_stealth_modal"
};


class PrivateContactModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    resolve: () => ModalStore.getState().resolve
                }}
            >
                <PrivateContactModal {...this.props} />
            </AltContainer>
        );
    }
}
export default PrivateContactModalContainer;
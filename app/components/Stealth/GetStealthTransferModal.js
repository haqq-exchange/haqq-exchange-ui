import React from "react";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
import Translate from "react-translate-component";
import DefaultModal from "Components/Modal/DefaultModal";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";
import ConfidentialWallet from "stores/ConfidentialWallet";


class GetStealthTransferModal extends DefaultBaseModal {

    // state = { value: null };

    submitForm = (event) => {
        event.preventDefault();
        const {data} = this.props;
        const {value} = this.state;

        console.log("this.props", this.props);
        // console.log("value", value);

        ConfidentialWallet.receiveBlindTransfer(value)
            .then(receipts => {
                data.queryBlindBalance();
                data.getCurrentReceipts();
                ModalActions.hide("get_stealth_transfer");
            });

    };

    closeModal = () => {
        const {modalId} = this.props;
        ModalActions.hide(modalId);
    };

    render () {
        const {isOpenModal} = this.state;
        const {modalId} = this.props;


        console.log("PrivateAccountModal isOpenModal", this.props);
        // console.log("PrivateAccountModal isOpenModal", this.state);


        return (
            <DefaultModal
                id={modalId}
                isOpen={isOpenModal}
                className={modalId}
                onRequestClose={this.closeModal}
            >
                <div className="modal fadeIn is-active">
                    <div className="modal-header">
                        <button type={"button"} onClick={this.closeModal} className="close-button">×</button>
                    </div>
                    <div className="modal-content">
                        <Translate content={"Stealth.get_confirmation"} component={"h3"}  />

                        <form onSubmit={(event)=>this.submitForm(event)}>
                            <textarea  id="" cols="50" rows="5" onChange={(event)=>this.setState({value: event.currentTarget.value})} />
                            <Translate content={"Stealth.guide.confrim_transaction"}  />
                            <br/>
                            <br/>
                            <button type={"submit"} className={"btn btn-red"}>
                                <Translate content={"Stealth.btn_confirmation"}   />
                            </button>
                        </form>

                    </div>
                </div>

            </DefaultModal>
        );
    }
}

GetStealthTransferModal.defaultProps = {
    modalId: "get_stealth_transfer"
};


export default class GetStealthTransferModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    data: () => ModalStore.getState().data["get_stealth_transfer"],
                    resolve: () => ModalStore.getState().resolve
                }}
            >  
                <GetStealthTransferModal {...this.props} />
            </AltContainer>
        );
    }
}
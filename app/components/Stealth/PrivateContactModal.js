import React from "react";
import {Redirect} from "react-router-dom";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
import AccountActions from "actions/AccountActions";
import DefaultModal from "Components/Modal/DefaultModal";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";
import AccountNameInput from "./../Forms/AccountNameInput";
import PrivateKeyInput from "./../Forms/PrivateKeyInput";
import {PublicKey} from "deexjs";
import {Checkbox} from "antd";
import Translate from "react-translate-component";
import counterpart from "counterpart";
import StealthRoute from "Components/Stealth/StealthPage";
import {Route} from "react-router-dom";



class PrivateContactModal extends DefaultBaseModal {

    constructor(props) {
        super(props);
        this.state = {
            isRedirect: false,
            label: null,
            key: null
        };

        this._onCreateClick = this._onCreateClick.bind(this);
        this._onLabelChange = this._onLabelChange.bind(this);
        this._onKeyChange = this._onKeyChange.bind(this);
    }


    closeModal = () => {
        const {modalId} = this.props;
        ModalActions.hide(modalId);
    };

    _onCreateClick(e) {
        if(e) e.preventDefault();

        let {label, public_key} = this.state;

        if( label.indexOf("~") !== -1 ) {
            label = label.slice(1);
        }

        try {
            AccountActions.addPrivateContact(label , public_key).then(()=>{
                ModalActions.hide("add_private_contact");
                this.setState({isRedirect: true});

            });
        }
        catch (error) {
            console.error("-- CreatePrivateContactModal._onCreateClick -->", error);
        }
    }

    _onLabelChange({value}) {
        if (!value) return;
        this.setState({label: value});
    }

    _onKeyChange({ public_key }) {
        this.setState({ public_key });
    }

    render () {
        const {isOpenModal, public_key: pubkey , label , isRedirect } = this.state;
        const {modalId} = this.props;

        // console.logconsole.log("isOpenModal", isOpenModal);


        let error = PublicKey.fromPublicKeyString(pubkey) ? null : (pubkey ? "Not valid key" : null);

        return (
            <DefaultModal
                id={modalId}
                isOpen={isOpenModal}
                className={modalId}
                onRequestClose={this.closeModal}
            >
                <div className="modal fadeIn is-active">
                    <div className="modal-header">
                        <button type="button" onClick={this.closeModal} className="close-button">×</button>
                    </div>
                    <div className="modal-content">
                        <Translate content={"Stealth.modal_create_contact"} component={"h3"}  />
                        <form  autoComplete="off">
                            <div className="form-group">
                                <AccountNameInput
                                    ref={ref=>this.refLabel=ref}
                                    placeholder={counterpart.translate("account.privat_label")}
                                    cheapNameValidate={false}
                                    onChange={this._onLabelChange}
                                    /*onEnter={this._onCreateClick}*/
                                    accountShouldNotExist={false}
                                    className={"private-name"}
                                    prefixSymbol="~"
                                    labelMode
                                />
                            </div>
                            <div className="form-group">
                                <PrivateKeyInput
                                    ref={ref=>this.refPrivateKey=ref}
                                    onChange={this._onKeyChange}
                                    publicKeyOnly
                                    /*onEnter={this._onCreateClick}*/
                                    pubKeyError={error} />
                            </div>
                            {/*<PrivateKeyInput ref="key" onChange={this._onKeyChange} />*/}
                            <div className="button-group">

                                <Translate
                                    onClick={this._onCreateClick}
                                    content={"Stealth.btn_create_contact"} className={"btn btn-green"} component={"button"}  />
                            </div>
                        </form>
                    </div>
                </div>

            </DefaultModal>
        );
    }
}

PrivateContactModal.defaultProps = {
    modalId: "add_private_contact"
};


class PrivateContactModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    resolve: () => ModalStore.getState().resolve
                }}
            >
                <PrivateContactModal {...this.props} />
            </AltContainer>
        );
    }
}
export default PrivateContactModalContainer;
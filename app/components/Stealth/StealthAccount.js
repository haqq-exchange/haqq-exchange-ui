import React from "react";
import moment from "moment";
import {Link} from "react-router-dom";
import Icon from "Components/Icon/Icon";
import ModalActions from "../../actions/ModalActions";
import PubKey from "Components/Utility/PubKey";
import ConfidentialWallet from "../../stores/ConfidentialWallet";
import GetStealthTransferModal from "./GetStealthTransferModal";
import {Asset} from "../../lib/common/MarketClasses";
import utils from "../../lib/common/utils";
import BalanceComponent from "../Utility/BalanceComponent";
import FormattedAsset from "../Utility/FormattedAsset";
import Translate from "react-translate-component";
import StelthWrap from "Components/Stealth/StelthWrap";
import ShowConfirmationModal from "./ShowConfirmationModal";

import CryptoJS from "crypto-js";
import Blowfish from "javascript-blowfish";
import { Base64 } from "js-base64";
import translator from "counterpart";

const AssetBalance = ({onClick, balance, asset_id}) => {
    const balance_value = utils.is_object_id(balance) ? <BalanceComponent balance={balance}/> : <FormattedAsset amount={balance} asset={asset_id}/>;
    return <span style={{borderBottom: "#A09F9F 1px dotted", cursor: "pointer"}} onClick={onClick}><Translate component="span" content="transfer.available"/>: {balance_value}</span>
};

const SimpleAssetBalance = ({balance, asset_id}) => {
    return utils.is_object_id(balance) ? <BalanceComponent balance={balance}/> : <FormattedAsset amount={balance} asset={asset_id}/>;
};



export default class StealthAccount extends StelthWrap {

    stealthUpdate = () => {
        console.warn("stealthUpdate");
        this.queryBlindBalance();
        this.getCurrentReceipts();
    };

    showStealthTransfer = () => {
        const _this= this;
        //this.createWindow()
        this.createWindow("get_stealth_transfer", {
            queryBlindBalance: () => _this.queryBlindBalance(),
            getCurrentReceipts: () => _this.getCurrentReceipts()
        })
            .then(()=>{
                _this.queryBlindBalance();
                _this.getCurrentReceipts();
            });
    };


    render(){
        const _this = this;
        let { private_accounts, account_name, passwordAccount, settings } = this.props;
        let { balances ,  receipts, stringPubKey, publicKey } = this.state;
        let all_balances;
        if( !private_accounts.has(account_name) ) {
            return null;
        }

        /*var encrypted = CryptoJS.AES.encrypt("Hello my name id Dima =)", "MZygpewJsCpRrfOr");

        var bytes  = CryptoJS.AES.decrypt("45853f917b76ce5ef324532193b26d32", "MZygpewJsCpRrfOr");
        var bytes2  = CryptoJS.AES.decrypt(encrypted, "MZygpewJsCpRrfOr");

        var originalText = bytes.toString(CryptoJS.enc.Utf8);
        var originalText2 = bytes2.toString(CryptoJS.enc.Utf8);

        let finalEncrypted = CryptoJS.enc.Base64.stringify(
            encrypted.ciphertext
        );

        console.log("originalText ciphertext", encrypted.toString()); // 'my message'
        console.log("originalText finalEncrypted", finalEncrypted); // 'my message'
        // console.log("originalText", originalText); // 'my message'
        // console.log("originalText2", originalText2); // 'my message'

        let decrText = "RYU/kXt2zl7zJFMhk7JtMg==";
        // console.log("originalText Base64", Base64);
        // let base64Decoded = CryptoJS.enc.Base64.parse(decrText);
        let base64Decodedencode = Base64.decode(decrText);
        console.log("originalText base64Decoded", base64Decodedencode);
        // console.log("originalText base64Decodedencode", base64Decodedencode);
        var bytes3  = CryptoJS.AES.decrypt(base64Decodedencode, "MZygpewJsCpRrfOr");
        var originalText3 = bytes3.toString(CryptoJS.enc.Utf8);
        console.log("originalText bytes3", bytes3 );
        console.log("originalText originalText3", originalText3 );*/

        /*const key = "MZygpewJsCpRrfOr";
        const iv = "55555555";
        const bf = new Blowfish(key, "cbc");

        console.log("Blowfish encrypt text by key: " + key);

        // Encryption
        const encrypted2 = bf.encrypt("Hello. how your work?", iv);
        console.log("originalText encrypted2", encrypted2);
        let encryptedMime = bf.base64Encode(encrypted2);
        console.log("originalText encryptedMime", encryptedMime);

        // Decryption
        console.log(
            "originalText decrypted: ",
            bf.decrypt(
                bf.base64Decode(encryptedMime), iv
            )
        );*/


        // console.log("stringPubKey, publicKey ", stringPubKey, publicKey );

        if( balances ) {
            all_balances = Object.keys(balances).map(current_asset_id => {
                return <li className={"stealth-main-list-row"} key={current_asset_id}>
                    <SimpleAssetBalance balance={balances[current_asset_id]} asset_id={current_asset_id} />
                    <button
                        type={"button"} style={{marginLeft: 50}} className={"btn btn-green"}
                        onClick={()=>this.createWindow("send_stealth_modal", {
                            from_name: `~${account_name}`,
                            to_name: passwordAccount,
                            asset_id: current_asset_id,
                            queryBlindBalance: () => _this.queryBlindBalance(),
                            getCurrentReceipts: () => _this.getCurrentReceipts(),
                            stealthUpdate: this.stealthUpdate
                        })}>
                        <Translate content={"Stealth.send_transaction"} />
                    </button>
                </li>;
            });
        }
        
        

        // console.log("StelthWrap.js ", this.props, this.state );
        // console.log("receipts ", receipts );
        // console.log("balances ", balances );
        // console.log("stringPubKey, publicKey  ", stringPubKey, publicKey  );
        // console.log("this.state ", this.state );
        // console.log("private_accounts, private_contacts ", private_accounts );

        return(
            <div className={"stealth-main"}>
                <div className="stealth-main-title">
                    <Link to={"/stealth"} style={{
                        marginRight: 10
                    }}>
                        <Icon name={"arrow-left"} />
                    </Link>
                    <Translate content={"Stealth.title_private_account"}  /> - { account_name }



                    <button
                        data-step={"1"}data-intro={translator.translate("Stealth.guide.get_trasaction", {locale: settings.get("locale")})}
                        type={"button"} style={{marginLeft: 20, marginRight: 20,}} className={"btn btn-red"} onClick={()=>this.showStealthTransfer()}>
                        <Translate content={"Stealth.get_transaction"}/>
                    </button>

                    <button className={"btn btn-guide"} onClick={this.startGuide}>?</button>

                </div>

                <div className={"stealth-main-wrap column"}>
                    <div className="stealth-main-item full" data-step={"2"} data-intro={translator.translate("Stealth.guide.private_your_key", {locale: settings.get("locale")})}>
                        <Translate content={"Stealth.title_your_key"} className={"stealth-main-title2"} component={"div"}  />
                        <ul className={"stealth-main-list"}>
                            <li  className={"stealth-main-list-row"}>
                                <PubKey getValue={()=>publicKey}  value={stringPubKey}/>
                            </li>
                        </ul>
                    </div>

                    {all_balances && all_balances.length > 0 && (<div className="stealth-main-item full">
                        <Translate content={"Stealth.title_your_balance"} className={"stealth-main-title2"} component={"div"}  />
                        <ul className={"stealth-main-list"}>
                            {all_balances}
                        </ul>
                    </div>)}

                    {receipts && (<div className="stealth-main-item full">
                        <Translate content={"Stealth.title_your_transaction"} className={"stealth-main-title2"} component={"div"}  />

                        <HeaderReceipts />

                        <ul className={"stealth-main-list"}>
                            {
                                receipts.map((receipt, index)=>{
                                    let asset = new Asset(receipt.amount);
                                    let amount = asset.getAmount({ real: false });
                                    return (
                                        <li key={"receipts-" + index} className={"stealth-main-list-row"} style={{marginTop: 20}}>
                                            <div className={"stealth-main-list-item"}><span>{moment(receipt.date).format("DD.MM.YYYY, HH:mm")}</span> </div>
                                            <div className={"stealth-main-list-item"}>
                                                <span>{receipt.from_label || passwordAccount}</span> </div>
                                            <div className={"stealth-main-list-item"}>
                                                <span>{receipt.to_label}</span>
                                            </div>
                                            <div className={"stealth-main-list-item"}>
                                                <FormattedAsset
                                                    decimalOffset={asset.precision}
                                                    amount={amount}
                                                    asset={asset.asset_id}

                                                />
                                            </div>
                                        </li>
                                    );
                                })
                            }
                        </ul>
                    </div>)}
                </div>

                <GetStealthTransferModal />
                <ShowConfirmationModal />
            </div>
        );
    }
}

const HeaderReceipts = () => {
    return (
        <div className={"stealth-main-header"}>
            <Translate content={"Stealth.table_head_date"} className={"stealth-main-header-item"}/>
            <Translate content={"Stealth.table_head_from_account"} className={"stealth-main-header-item"}/>
            <Translate content={"Stealth.table_head_to_label"} className={"stealth-main-header-item"}/>
            <Translate content={"Stealth.table_head_amount"} className={"stealth-main-header-item"}/>
        </div>
    );
};
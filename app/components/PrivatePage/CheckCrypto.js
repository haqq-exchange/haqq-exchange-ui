import React from "react";
import crypto from "crypto";
// import crypt from "./crypt";


function testdecode(encodeKey, walletKey ) {
    window.console.log("encodeKey, walletKey", encodeKey, walletKey);
    try {
        let textParts = walletKey.split(":");
        window.console.log("textParts", textParts);
        let iv = Buffer.from(textParts.shift(), "hex");
        window.console.log("iv", iv);
        let encryptedText = Buffer.from(textParts.join(":"), "hex");
        window.console.log("encryptedText", encryptedText);
        let decipher = crypto.createDecipheriv("aes-256-cbc", Buffer.from(encodeKey), iv);
        window.console.log("decipher", decipher);
        let decrypted = decipher.update(encryptedText);
        window.console.log("decrypted", decrypted);
        decrypted = Buffer.concat([decrypted, decipher.final()]);
        window.console.log("decrypted", decrypted.toString());
        return decrypted;
    } catch (e) {
        window.console.error(e);
    }
}


export default class CheckCrypto extends React.Component {

    checkKeys = (event) => {
        event.preventDefault();
        const {encodeKey, walletKey} = this.state;
        window.console.log("encodeKey, walletKey", encodeKey, walletKey);
        testdecode(encodeKey, walletKey);
    };

    changeText = (name, value) => {
        this.setState({[name]: value });
    };

    render(){
        return (
            <form onSubmit={this.checkKeys} style={{
                width: "500px",
                margin: "20px auto"
            }}>
                <div>
                    <input type="text" placeholder={"encodeKey"} onChange={(event)=>this.changeText("encodeKey", event.target.value)} />
                </div>
                <div>
                    <input type="text" placeholder={"walletKey"} onChange={(event)=>this.changeText("walletKey", event.target.value)} />
                </div>
                <button className={"btn btn-green"} type={"submit"}>submit</button>
            </form>
        );
    }
}
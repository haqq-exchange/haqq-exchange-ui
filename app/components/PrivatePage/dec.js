const crypto = require("crypto");

function testdecode(encodeKey, walletKey ) {
    console.log("encodeKey, walletKey", encodeKey, walletKey);
    try {
        let textParts = walletKey.split(":");
        console.log("textParts", textParts);
        let iv = Buffer.from(textParts.shift(), "hex");
        console.log("iv", iv);
        let encryptedText = Buffer.from(textParts.join(":"), "hex");
        console.log("encryptedText", encryptedText);
        let decipher = crypto.createDecipheriv("aes-256-cbc", Buffer.from(encodeKey), iv);
        // let decipher = crypt.methods.decrypt(Buffer.from(encodeKey), iv);
        console.log("decipher", decipher);
        let decrypted = decipher.update(encryptedText);
        console.log("decrypted", decrypted);
        decrypted = Buffer.concat([decrypted, decipher.final()]);
        console.log("decrypted", decrypted.toString());
        return decrypted;
    } catch (e) {
        console.error(e);
    }
}

// testdecode("2WJG5TsH33fCjYxSSfgdTG9bSBM6mQ", "d5436033d1b559b05da0789c812ee838:c3159da24840d68965e55c9f28a070de1981ea85aba5ac2605e7f0e1e6737db4" );
testdecode("megapassmegapassmegapassmegapa", "52733af1f3b868cf638ec28cfe6f35ea:ac8953c5df89d2c2fb10d417b15b087b9a27643d0def33051129a6f8d6e9b70836268b64c5be6a0a9a7406bf7f36209234071179d3c31b9716406cd308c43447" );
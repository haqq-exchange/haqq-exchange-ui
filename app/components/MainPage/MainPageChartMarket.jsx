import React from "react";
import loadable from "loadable-components";
import Immutable from "immutable";
import {Carousel} from "antd";
import ResponseOption from "../../config/response";
import MediaQuery from "react-responsive";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";

const MarketCard = loadable(() => import("Components/Dashboard/MarketCard"));
const MarketAddCard = loadable(() => import("Components/Dashboard/DashboardPage/_MarketAddCard"));

export default class ChartMarket extends React.Component {

    // static defaultProps = __DEEX_CHAIN__ ? {
    //     chartsMap: Immutable.fromJS([
    //         {
    //             quote: "DEEX",
    //             base: "BTS",
    //         },{
    //             quote: "DEEX",
    //             base: "USDTETHER",
    //         },{
    //             quote: "DEEX",
    //             base: "CRYPTOMAT",
    //         },{
    //             quote: "ETH",
    //             base: "USDTETHER",
    //         },{
    //             quote: "DEEX.BTC",
    //             base: "USDTETHER",
    //         },{
    //             quote: "DEEX.BTC",
    //             base: "YANDEX",
    //         },{
    //             quote: "ETH",
    //             base: "YANDEX",
    //         }
    //     ])
    // } : {
    //     chartsMap: Immutable.fromJS([
    //         {
    //             "quote": "DEEX",
    //             "base": "YANDEX"
    //         },
    //         {
    //             "quote": "DEEX",
    //             "base": "BTC"
    //         },
    //         {
    //             "quote": "DEEX",
    //             "base": "USDT"
    //         },
    //         {
    //             "quote": "ETH",
    //             "base": "DEEX"
    //         }
    //     ])
    // };

    static defaultProps = {
        chartsMap: Immutable.fromJS(
            __SCROOGE_CHAIN__ ? [
                {
                    quote: "SCROOGE",
                    base: "BCH",
                },{
                    quote: "SCROOGE",
                    base: "USDT",
                },{
                    quote: "SCROOGE",
                    base: "BTC",
                },{
                    quote: "SCROOGE",
                    base: "DASH",
                },{
                    quote: "SCROOGE",
                    base: "DOGE",
                },{
                    quote: "SCROOGE",
                    base: "ETH",
                },{
                    quote: "SCROOGE",
                    base: "TRX",
                },{
                    quote: "SCROOGE",
                    base: "ZCASH",
                }
            ] :[
                {
                    "quote": __GBLTN_CHAIN__ ? "GBLTEST" : (__GBL_CHAIN__  ? "GBL" : "DEEX"),
                    "base": "YANDEX"
                },
                {
                    "quote": __GBLTN_CHAIN__ ? "GBLTEST" : (__GBL_CHAIN__ || __GBLTN_CHAIN__ ? "GBL" : "DEEX"),
                    "base": "BTC"
                },
                {
                    "quote": __GBLTN_CHAIN__ ? "GBLTEST" : (__GBL_CHAIN__? "GBL" : "DEEX"),
                    "base": "USDT"
                },
                {
                    "quote": "ETH",
                    "base": __GBLTN_CHAIN__ ? "GBLTEST" : (__GBL_CHAIN__ ? "GBL" : "DEEX")
                }
            ])
    };

    onChange = (a,b) => {
        console.log("onChange", a,b);
    };
    getChildrenMobileContent = (params) => {
        if (__DEEX_CHAIN__ || __SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__) {
            const {chartsMap} = this.props;
            return (
                <Carousel
                    {...params}
                    afterChange={this.onChange}  >
                    {chartsMap.map(chart=>{
                        if( chart.get("quote") && chart.get("base") ) {
                            let marketId = [chart.get("quote"), chart.get("base")].join("_");
                            return <MarketCard key={marketId} {...chart.toJS()} >
                                <Link to={["/market", marketId].join("/")} className={"chart-market-go"}>
                                    <Translate content={"public.go"}   />
                                </Link>
                            </MarketCard>;
                        }
                    })}
                </Carousel>
            );
        } else {
            const {main_markets} = this.props;
            console.log("main_markets", main_markets);

            if(!main_markets) return null;

            return (
                <Carousel
                    {...params}
                    afterChange={this.onChange}  >
                    {main_markets.map(chart=>{
                        if( chart.get("quote") && chart.get("base") ) {
                            let marketId = [chart.get("quote"), chart.get("base")].join("_");
                            return <MarketCard key={marketId} {...chart.toJS()} >
                                <Link to={["/market", marketId].join("/")} className={"chart-market-go"}>
                                    <Translate content={"public.go"}   />
                                </Link>
                            </MarketCard>;
                        }
                    })}
                </Carousel>
            );
        }
    };

    render(){

        return (
            <MediaQuery {...ResponseOption.mobile}>
                {(matches) => {
                    if (matches) {
                        return this.getChildrenMobileContent({
                            arrows:false,
                            dots:true,
                            slidesToShow:1,
                            slidesToScroll:1,
                            adaptiveHeight:false
                        });
                    } else {
                        return this.getChildrenMobileContent({
                            arrows:true,
                            dots:false,
                            slidesToShow:3,
                            slidesToScroll:3,
                            adaptiveHeight:false
                        });
                    }
                }}
            </MediaQuery>
        );
    }
}

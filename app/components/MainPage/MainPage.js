import React from "react";
import {connect} from "alt-react";
import {Link} from "react-router-dom";
import cn from "classnames";
import Translate from "react-translate-component";
import ImageLoad from "Utility/ImageLoad";
import AccountStore from "stores/AccountStore";
import SettingsStore from "stores/SettingsStore";
import {async_fetch} from "api/apiConfig";
import InfoModal from "Components/Account/CreateAccount/InfoModal";
import MainPageChartMarket from "Components/MainPage/MainPageChartMarket";
import SendSaveEmailModal from "Components/Modal/SendSaveEmailModal";
import ResponseOption from "config/response";
import MediaQuery from "react-responsive";
import ModalActions from "actions/ModalActions";
import Iframe from "Components/Iframe/Iframe";
import "./MainPage.scss";

const getContent = (data, settings) => {
    let locale = settings;
    if(data[locale] == undefined) {
        locale = "en"
    }  
    return typeof data === "string" ? data  : data[locale];
};

class MainPage extends React.Component{

    state = {
        showInfoModal: false,
        pageData: {}
    };

    componentDidMount() {
        const _this = this;
        let response = __GBL_CHAIN__ || __GBLTN_CHAIN__ ? async_fetch("https://static.haqq.exchange/gbl/locales/main_page.json") :
       (__SCROOGE_CHAIN__ ? async_fetch("https://static.scrooge.club/locales/main_page.json") : 
       async_fetch("https://static.haqq.exchange/locales/main_page.json"));
        
        response.then(pageData => _this.setState({pageData}));
    }

    infoModal = ( isReg ) => {
        if( isReg ) {
            this.props.history.push({
                pathname: "/create-account",
                search: this.props.location.search,
              });
        }
        this.setState({
            showInfoModal: false
        });
    };

    render() {
        const {pageData, showInfoModal} = this.state;

        return (
            <>
            {__SCROOGE_CHAIN__ ?
                <div className={"main-page"}>
                    <MainPageFirst setShowInfoModal={hasShow=>this.setState({showInfoModal: hasShow})} {...this.props} />
                    {/* <MainPageMarkets {...this.props} />
                    <MainPageWhy {...pageData} {...this.props} />
                    <MainPageScroogeClub {...this.props} />
                    <MainPageSupport {...this.props} />
                    <MainPageReferral {...this.props} />
                    <MainPageFooter {...this.props} /> */}
                    <InfoModal
                        resolve={this.infoModal}
                        isOpenModal={showInfoModal} />
                    {/* <SendSaveEmailModal  /> */}
                </div>
                :
                <div className={"main-page"}>
                    <MainPageFirst setShowInfoModal={hasShow=>this.setState({showInfoModal: hasShow})} {...this.props} />
                    {/* {__GBL_CHAIN__ || __GBLTN_CHAIN__ ? null :
                    <MainPageWhy {...pageData} {...this.props} />}

                    {__GBL_CHAIN__ || __GBLTN_CHAIN__ ? null : 
                    <MainPageMarkets {...this.props} />}
                    
                    {__GBL_CHAIN__ || __GBLTN_CHAIN__  ? null :
                    <MainPageB2b {...pageData} {...this.props} />}

                    {__GBL_CHAIN__ || __GBLTN_CHAIN__ ? null : 
                    <MainPageB2C {...pageData} {...this.props} />}
                
                    {__GBL_CHAIN__ || __GBLTN_CHAIN__ ? null :
                    <>
                    {!this.props.passwordAccount ?
                        <MainPageJoin setShowInfoModal={hasShow=>this.setState({showInfoModal: hasShow})}  {...this.props} />
                        : null}
                    </>} */}

                    <InfoModal
                        resolve={this.infoModal}
                        isOpenModal={showInfoModal} />

                    {/* <SendSaveEmailModal  /> */}
                </div>}
            </>
        );
    }
}


export default connect(
    MainPage,
    {
        listenTo() {
            return [SettingsStore, AccountStore];
        },
        getProps() {
            return {
                settings: SettingsStore.getState().settings,
                main_markets: SettingsStore.getState().viewSettings.get("main_markets"),
                passwordAccount: AccountStore.getState().passwordAccount
            };
        }
    }
);


const MainPageMarkets = (props) => {
    return (
        <>
        {__SCROOGE_CHAIN__ ?
        <div className={"main-page-markets-wrap"}>
            <Iframe source={"/market/USDT_BTC"} />
            <Link to="/market/USDT_BTC">
                <Translate content="mainPage.markets_start_trading" className={"main-page-first-scrooge-btn"} component={"div"} style={{margin: "20px auto 0 auto"}} />
            </Link>
        </div>
        :
        <div className={__GBL_CHAIN__ || __GBLTN_CHAIN__ ? "main-page-markets-gbl" : "main-page-markets"}>
            <div className={"main-page-markets-wrap"}>
                {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?
                <Translate content={"mainPage.markets_title_gbl"} style={{"textAlign": "center" ,"margin": "0 0 30px 0"  }} component={"h4"}  />
                : <Translate content={"mainPage.markets_title"} style={{"textAlign": "center" ,"margin": "0 0 30px 0"  }} component={"h4"}  />}
                <MainPageChartMarket {...props} />
            </div>
        </div>
        }
        </>
    );
};

const MainPageJoin = (props) => {
    return(
        <div className={"main-page-join"}>
            <div className={"main-page-join-wrap"}>
                <Translate content={__SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__ ? "mainPage.join_links_scrooge" : "mainPage.join_links"}  component={"b"}  />
                <button type={"button"} onClick={()=>props.setShowInfoModal(true)} className={"btn btn-green"}>
                    <Translate content="public.register" />
                </button>
            </div>
        </div>
    );
};

const MainPageB2C = (props) => {
    const {b2c, settings} = props;
    const locale = settings.get("locale");
    return (
        <div className={"main-page-b2c"}>
            <div className={"main-page-b2c-wrap"}>
                    <Translate content={__GBL_CHAIN__ || __GBLTN_CHAIN__ ? "" : "mainPage.b2c_title"} className={"main-page-title"} component={"div"}  />
                    <Translate content={__GBL_CHAIN__ || __GBLTN_CHAIN__ ? "" : "mainPage.b2c_desc"} className={"main-page-description"} component={"div"}  />
                    <div className={"main-page-b2c-body"}>
                        {b2c && b2c.map((item, index) => {
                            const itemLabel = item.label;
                            const title = getContent(item.title, locale);
                            const text = getContent(item.text, locale);
                            let link = [];
                            if( item.target ) {
                                item.target.map((target, i)=>{
                                    if( target.params.href ) {
                                        link.push(<div key={"main-page-b2c-link" + i + "-" + index } className={"main-page-b2c-link"}>
                                            <a {...target.params}>{getContent(target.title, locale)}</a>
                                        </div>);
                                    }
                                });
                            }
                            if( item.link ) {
                                item.link.map((target, i)=>{
                                    if( target.params.to ) {
                                        link.push(<div key={"main-page-b2c-link"  + i + "-" + index } className={"main-page-b2c-link"}>
                                            <Link {...target.params}>{getContent(target.title, locale)}</Link>
                                        </div>);
                                    }
                                });
                            }
                            return (
                                <MediaQuery key={`main-page-media-query-b2c-${index}`} {...ResponseOption.mobile}>
                                    {(matches) => {
                                        if (matches) {
                                            return (
                                                <div key={`main-page-b2c-${index}`} className={"main-page-b2c-item"}>
                                                    <span className={"main-page-b2c-image"}>
                                                        <ImageLoad staticPath={"images/main_page"} imageName={item.image} />
                                                        <div className={"main-page-b2c-title"}>{title}</div>
                                                    </span>
                                                    <div className={"main-page-b2c-content"}>
                                                        <div className={"main-page-b2c-text"}>{text}</div>
                                                        {link ? <div className={"main-page-b2c-links"}>{link}</div>: null}
                                                    </div>
                                                    {itemLabel ? <span className={"main-page-b2b-label"} style={itemLabel.style}>{getContent(itemLabel.text, locale)}</span> : null}
                                                </div>
                                            );
                                        } else {
                                            return (
                                                <div key={`main-page-b2c-${index}`} className={"main-page-b2c-item"}>
                                                    <span className={"main-page-b2c-image"}>
                                                        <ImageLoad staticPath={"images/main_page"} imageName={item.image} />
                                                    </span>
                                                    <div className={"main-page-b2c-content"}>
                                                        <div className={"main-page-b2c-title"}>{title}</div>
                                                        <div className={"main-page-b2c-text"}>{text}</div>
                                                        {link ? <div className={"main-page-b2c-links"}>{link}</div>: null}
                                                    </div>
                                                    {itemLabel ? <span className={"main-page-b2b-label"} style={itemLabel.style}>{getContent(itemLabel.text, locale)}</span> : null}
                                                </div>
                                            );
                                        }
                                    }}
                                </MediaQuery>
                            );

                        })}
                    </div>
            </div>
        </div>
    )
};

const MainPageB2b = (props) => {
    const {b2b, settings} = props;
    const locale = settings.get("locale");

    return (
        <div className={"main-page-b2b"}>
            <div className={"main-page-b2b-wrap"}>
                {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?
                <Translate content={"mainPage.b2b_title2_gbl"} className={"main-page-title"} component={"div"}  />
                : <Translate content={__SCROOGE_CHAIN__ ? "mainPage.b2b_title2_scrooge" : "mainPage.b2b_title2"} className={"main-page-title"} component={"div"}  />}
                {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?
                <Translate content={"mainPage.b2b_desc2_gbl"} className={"main-page-description"} component={"div"}  />
                :<Translate content={__SCROOGE_CHAIN__ ? "mainPage.b2b_desc2_scrooge" : "mainPage.b2b_desc2"} className={"main-page-description"} component={"div"}  />}
                    <br/>
                    <br/>

                    <Translate content={__GBL_CHAIN__ || __GBLTN_CHAIN__ ? "" : "mainPage.b2b_title"} className={"main-page-title"} component={"div"}  />
                    <Translate content={__GBL_CHAIN__ || __GBLTN_CHAIN__ ? "" : "mainPage.b2b_desc"} className={"main-page-description"} component={"div"}  />
                    {__GBL_CHAIN__ || __GBLTN_CHAIN__ ? null :
                    <div className={"main-page-b2b-body"}>
                        {b2b && b2b.map((item, index) => {
                            //console.log("item, index", item, index)
                            let itemLabel = item.label;
                            let button = item.button;
                            const title = getContent(item.title, locale);
                            const text = getContent(item.text, locale);
                            let link = [];
                            let btn = {};
                            if( item.target ) {
                                item.target.map((target,i)=>{
                                    if( target.params.href ) {
                                        link.push(<div key={`main-page-b2b-link-${i}-${index}`} className={"main-page-b2b-link"}>
                                            <a {...target.params}>{getContent(target.title, locale)}</a>
                                        </div>);
                                    }
                                });
                            }
                            if( item.link ) {
                                item.link.map((target, i)=>{
                                    if( target.params.to ) {
                                        link.push(<div key={`main-page-b2b-link-${i}-${index}`} className={"main-page-b2b-link"}>
                                            <Link {...target.params}>{getContent(target.title, locale)}</Link>
                                        </div>);
                                    }
                                });
                            }
                            if( button ) {
                                btn = {
                                    className: cn("btn", button.color),
                                    type: "button",
                                    onClick: () => {
                                        ModalActions.show("send_save_email", {
                                            "typeWindow": button.type,
                                            "service": button.service || "add_service_json"
                                        });
                                    }
                                };
                            }

                            return (
                                <MediaQuery key={`main-page-media-query-b2b-${index}`} {...ResponseOption.mobile}>
                                    {(matches) => {
                                        if (matches) {
                                            return (
                                                <div key={`main-page-b2b-${index}`} className={"main-page-b2b-item"}>
                                                    <span className={"main-page-b2b-image"}>
                                                        <ImageLoad staticPath={"images/main_page"} imageName={item.image} />
                                                        <div className={"main-page-b2b-title"}>{title}</div>
                                                    </span>
                                                    <div className={"main-page-b2b-content"}>
                                                        <div className={"main-page-b2b-text"}>{text}</div>
                                                        {link ? <div className={"main-page-b2b-links"}>{link}</div>: null}
                                                        {button ? <div className={"main-page-b2b-button"}><button {...btn}>{getContent(button.text, locale)}</button></div>: null}
                                                    </div>
                                                    {itemLabel ? <span className={"main-page-b2b-label"} style={itemLabel.style}>{getContent(itemLabel.text, locale)}</span> : null}
                                                </div>
                                            );
                                        } else {
                                            return (
                                                <div key={`main-page-b2b-${index}`} className={"main-page-b2b-item"}>
                                                    <span className={"main-page-b2b-image"}>
                                                        <ImageLoad staticPath={"images/main_page"} imageName={item.image} />
                                                    </span>
                                                    <div className={"main-page-b2b-content"}>
                                                        <div className={"main-page-b2b-title"}>{title}</div>
                                                        <div className={"main-page-b2b-text"}>{text}</div>
                                                        {link ? <div className={"main-page-b2b-links"}>{link}</div>: null}
                                                        {button ? <div className={"main-page-b2b-button"}><button {...btn}>{getContent(button.text, locale)}</button></div>: null}
                                                    </div>
                                                    {itemLabel ? <span className={"main-page-b2b-label"} style={itemLabel.style}>{getContent(itemLabel.text, locale)}</span> : null}
                                                </div>
                                            );
                                        }
                                    }}
                                </MediaQuery>
                            );
                        })}
                    </div>
                    }
            </div>
        </div>
    );
};

const MainPageWhy = (props) => {
    const {why, settings} = props;
    const locale = settings.get("locale");

    return (
        <div className={__SCROOGE_CHAIN__ ? "main-page-why-scrooge" : "main-page-why"}>
            {__SCROOGE_CHAIN__ ?
            <div className={"main-page-why-scrooge-wrap"}>
            <div className={locale === "ru" ? "main-page-title-scrooge-ru" : "main-page-title-scrooge-en"} />
            <div className={"main-page-why-body-scrooge"}>
                {why && why.map((item, index) => { 
                    const title = getContent(item.title, locale);
                    const text = getContent(item.text, locale);
                    const picture = getContent(item.picture, locale);
                    const littlePicture = getContent(item.little_picture, locale);
                    return (
                        <div className={"main-page-why-scrooge-item"} key={`main-page-why-${index}`}>
                            <div className={"main-page-why-scrooge-item-wrap"}>
                                <div className={"main-page-why-scrooge-little-pic"} style={{background: "url(" + `${littlePicture}` + ") no-repeat left"}} />
                                <div className={"main-page-why-scrooge-pic"} style={{background: "url(" + `${picture}` + ") no-repeat left"}} />
                                <div className={"main-page-why-scrooge-title"}>{title}</div>
                                <div className={"main-page-why-scrooge-text"}>{text}</div>
                            </div>
                        </div>
                    );
                })}
            </div>
            <div className={locale === "ru" ? "main-page-why-scrooge-currency-ru" : "main-page-why-scrooge-currency-en"} />
        </div>
        :
        <div className={"main-page-why-wrap"}>
            <Translate content={__GBL_CHAIN__ || __GBLTN_CHAIN__ ? "mainPage.why_title_gbl" : "mainPage.why_title"} className={"main-page-title"} component={"div"}  />
            <div className={"main-page-why-body"}>
                {why && why.map((item, index) => {
                    const num = ++index;
                    const title = getContent(item.title, locale);
                    const text = getContent(item.text, locale);
                    return (
                        <div className={"main-page-why-item"} key={`main-page-why-${index}`}>
                            <span className={"main-page-why-num"}>{num.toString().padLeft(2, 0)}</span>
                            <div className={"main-page-why-item-wrap"}>
                                <div className={"main-page-why-title"}>{title}</div>
                                <div className={"main-page-why-text"}>{text}</div>
                            </div>
                        </div>
                    );
                })}
            </div>
        </div>
        }
        </div>
    );
};

const MainPageFirst = (props) => {
    const {settings} = props;
    const locale = settings.get("locale");

    return (
        <div className={__SCROOGE_CHAIN__ ? "main-page-first-scrooge" : (__GBL_CHAIN__ || __GBLTN_CHAIN__ ? "main-page-first-gbl" : "main-page-first")}>
            {/* для scrooge chain */}
            {__SCROOGE_CHAIN__ ? 
                <div className={"main-page-first-scrooge-row"}>
                    <div className={"main-page-first-scrooge-left"}>
                        {/* <div className={"main-page-first-title main-page-first-scrooge-main-title"}>
                            <div className={locale === "ru" ? "main-page-first-scrooge-title-ru" : "main-page-first-scrooge-title-en"} /> 
                            <Translate content={"mainPage.first_title-scrooge"} className={"main-page-first-scrooge-text"} component={"div"} />
                            <Translate content={"mainPage.first_list-item-1"} className={"main-page-first-scrooge-notes"} component={"p"} />
                            <Translate content={"mainPage.first_list-item-2"} className={"main-page-first-scrooge-notes"} component={"p"} />
                            <Translate content={"mainPage.first_list-item-3"} className={"main-page-first-scrooge-notes"} component={"p"} />
                            <Translate content={"mainPage.first_list-item-4"} className={"main-page-first-scrooge-notes"} component={"p"} />
                            <Translate content={"mainPage.first_list-item-5"} className={"main-page-first-scrooge-notes"} component={"p"} />
                        </div> */}
                        <div className="main-page-first-buttons">
                            {!props.passwordAccount ?
                            <Link to={"/create-account"}>
                                <Translate content="public.register-scrooge" className={"main-page-first-scrooge-btn"} component={"div"} />
                            </Link>   
                            : null}
                        </div>
                    </div>
                </div>
            :
            <div className={__GBL_CHAIN__ || __GBLTN_CHAIN__ ? "main-page-first-row-gbl" : "main-page-first-row"}>
                {/* для всех остальных chain */}
                <div className={"main-page-first-left"}>
                    {/* {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?
                        <Translate unsafe content={"mainPage.first_gbl"} className={"main-page-first-title"} component={"div"}  /> 
                    : null}
                    {__GBL_CHAIN__ || __GBLTN_CHAIN__ || __SCROOGE_CHAIN__ ? null :
                        <Translate unsafe content={"mainPage.first_title"} className={"main-page-first-title"} component={"div"}  />}   */}
                    <div className="main-page-first-buttons">
                        <Link to={"/market"} className={"btn btn-gray"}>
                            <Translate content="public.view_demo" />
                        </Link>
                        {!props.passwordAccount ?
                            <>
                                {__GBL_CHAIN__ || __GBLTN_CHAIN__ ? 
                                <Link to={"/create-account/password"} className={"btn btn-green"}>
                                    <Translate content="public.register" />
                                </Link>:
                                <button type={"button"} onClick={()=>props.setShowInfoModal(true)} className={"btn btn-green"}>
                                    <Translate content="public.register" />
                                </button>}
                                <Link to={__GBL_CHAIN__ || __GBLTN_CHAIN__ ? "/authorization/password" : "/authorization"} className={"btn btn-blue"}>
                                    <Translate content="public.login" />
                                </Link>
                            </>
                            : null}
                    </div>
                </div>
                {__GBL_CHAIN__ || __GBLTN_CHAIN__ ? null :
                <div className={"main-page-first-right"}>
                    <ul className={"main-page-first-list"}>
                        <li className={"main-page-first-list-item"}>
                            1st Pre Alpha MVP of halal cryptoexchange by HCC (Halal Crypto Community)
                        </li>
                        {/* <li className={"main-page-first-list-item"}>
                            <div> &gt; 100 000 </div>
                            <Translate content={"mainPage.first_traders"}  />
                        </li>
                        <li className={"main-page-first-list-item"}>
                            <div> &gt; 50  </div>
                            <Translate content={"mainPage.first_coin"}  />
                        </li>
                        <li className={"main-page-first-list-item"}>
                            <div> &infin; </div>
                            <Translate unsafe content={"mainPage.first_infinite"}  />
                        </li> */}
                    </ul>
                </div>}
            </div>
        }
        </div>
    );
};

const MainPageScroogeClub = (props) => {
    const {settings} = props;
    const locale = settings.get("locale");
    
    return (
        <div className={"main-page-club"}>
            <div className={"main-page-club-title main-page-club-screen"} />
            <div className={"main-page-club-background"}>
                <div className={"main-page-club-title main-page-club-desctop"} />
                <Translate className={"main-page-club-text"} content={"mainPage.club"} />
                {locale === "ru" ? null :
                <>
                    <span className={"main-page-club-users-number"}>315 661</span>
                    <Translate className={"main-page-club-users"} content={"mainPage.club_users"} />
                </>}
                <div className="main-page-first-scrooge-buttons">
                    {!props.passwordAccount ?
                    <Link to={"/create-account"} className="main-page-first-scrooge-desctop">
                        <Translate content="public.register-scrooge" className={"main-page-first-scrooge-btn"} component={"div"} />
                    </Link>    
                    : null}
                </div>
            </div>
            <div className="main-page-first-buttons">
                {!props.passwordAccount ?
                <Link to={"/create-account"} className="main-page-first-scrooge-screen">
                    <Translate content="public.register-scrooge" className={"main-page-first-scrooge-btn"} component={"div"} />
                </Link>    
                : null}
            </div>
        </div>
    );
};

const MainPageSupport = (props) => {
    const {settings} = props;
    const locale = settings.get("locale");

    return (
        <div className={"main-page-support"}>
            <div className={locale === "ru" ? "main-page-support-pic-ru" : "main-page-support-pic-en"} /> 
            <Translate content="mainPage.support" className="main-page-support-text" component={"div"} />
            <div className={"main-page-support-btn"}>Telegram Bot</div>
        </div>
    );
};

const MainPageReferral = (props) => {
    const {settings} = props;
    const locale = settings.get("locale");
    return (
        <div className={"main-page-referral"}>
            <div className={locale === "ru" ? "main-page-referral-pic-ru main-page-referral-screen" : "main-page-referral-pic-en main-page-referral-screen"} />
            <div className={"main-page-referral-background"}>
                <div className={locale === "ru" ? "main-page-referral-pic-ru main-page-referral-desctop" : "main-page-referral-pic-en main-page-referral-desctop"} />
                <Translate className={"main-page-referral-title"} content="mainPage.referral_title" component={"div"} />
                <Translate className={"main-page-referral-text"} content="mainPage.referral_text" component={"p"} />
                <Translate className={"main-page-referral-btn"} content="mainPage.referral_btn" component={"div"} />
            </div>
        </div>
    );
};


const MainPageFooter = (props) => {
    const {settings} = props;
    const locale = settings.get("locale");
    return (
        <div className={"main-page-footer"}>
            <div className={"main-page-footer-news"}>
                <Translate className={"main-page-footer-news-pic-text"} content="mainPage.subscribe_text" component={"div"} />
                <div className={locale === "ru" ? "main-page-footer-news-pic-ru" : "main-page-footer-news-pic-en"} /> 
                <div className={"main-page-footer-subscribe"}>
                    <input className={"main-page-footer-placeholder"} placeholder={locale === "ru" ? "Введите ваш Email" : "Enter your Email"} />
                    <Translate className={"main-page-footer-subscribe-btn"} content="mainPage.subscribe_btn" component={"div"} />
                </div>
            </div>
            <div className={"main-page-footer-wrap"}>
                <div className={"main-page-footer-mobile"}>
                    <div style={{background: "url(" + `https://static.scrooge.club/images/scrooge-logo-white.png` +") no-repeat", height: "34px", width: "160px", marginBottom: "30px"}} /> 
                    <div className={"main-page-support-btn"}>Telegram Bot</div>
                </div>
                <ul className={"main-page-footer-list"}>
                    <li> 
                        <Link className={"main-page-footer-link"} to={"/market"}><Translate content="header.exchange"/></Link>
                    </li>
                    {props.passwordAccount ? 
                    <li>
                        <Link className={"main-page-footer-link"} to={["", "account", props.passwordAccount].join("/")}><Translate content="header.wallet"/></Link>
                    </li>
                    : null}
                    <li>
                        <Link className={"main-page-footer-link"} to={"/api"}>API</Link>
                    </li>
                </ul>
                <ul className={"main-page-footer-list"}>
                    <li><Translate className={"main-page-footer-doc"} content="mainPage.footer_item1"/></li>
                    <li><Translate className={"main-page-footer-doc"} content="mainPage.footer_item2"/></li>
                    <li><Translate className={"main-page-footer-doc"} content="mainPage.footer_item3"/></li>
                </ul>
                {!props.passwordAccount ?
                <div className={"main-page-footer-btns main-page-footer-btns-screen"}>
                    <Link to={"/authorization"} className={"main-page-footer-login main-page-footer-screen"}>
                        <Translate className={"main-page-footer-login-text"} content="public.login" />
                    </Link>
                    <Link to={"/create-account"} className={"main-page-footer-signin"}>
                        <Translate className={"main-page-footer-signin-text"} content="public.register" />
                    </Link>
                </div>
                : null}
            </div>
            <div className={"main-page-footer-copyright-wrap"}>
                <div className={"main-page-footer-copyright"}>COPYRIGHT © 2019-2021 SCROOGE EUROPE OÜ, HARJU MAAKOND, KESKLINNA LINNAOSA, VESIVÄRAVA 50-310, TALLINN 10152</div>
            </div>
        </div>
    );
};
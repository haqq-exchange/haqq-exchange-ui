import React from "react";
import Translate from "react-translate-component";
// import ChainTypes from "Utility/ChainTypes";
import cnames from "classnames";
import { diff } from "deep-object-diff";
import MediaQuery from "react-responsive";

class MarketsHeader extends React.PureComponent {
    static propTypes = {};

    constructor(props) {
        super(props);

        //this._getChangeProps = this._getChangeProps.bind(this);
    }


    getClassName = (elType) => {
        let { sortBy, sortDirection } = this.props;
        let classNameDiraction = {
            "up": sortDirection,
            "down": !sortDirection,
            "active": sortBy === elType,
        };
        if(  sortBy === elType ) {
            return classNameDiraction;
        }

        return {};
    };

    render() {
        let { handleFilterInput, onToggleSort } = this.props;

        return (
            <div className="markets-header">
                <div className="markets-filter">
                    <input
                        type="text"
                        placeholder="Filter"
                        onChange={handleFilterInput.bind(this)}
                    />
                </div>

                <div className="markets-table">
                    <div className="table-heads">
                        <div className="table-cell table-cell-star">
                            <i/>
                        </div>
                        <div className={cnames("table-cell table-cell-asset", this.getClassName("quote") )}>
                            <Translate component="a"
                                className={"filter-link"}
                                content="dashboard.pair"
                                onClick={onToggleSort.bind(this, "quote")} />
                        </div>
                        <div className={cnames("table-cell table-cell-price", this.getClassName("price"))}>
                            <Translate component="a"
                                className={"filter-link"}
                                content="dashboard.price"
                                onClick={onToggleSort.bind(this, "price")} />
                        </div>
                        <div className={cnames("table-cell table-cell-hour24Chanage", this.getClassName("marketStats.change"))}>
                            <Translate content="dashboard.chanage_24"
                                component="a"
                                className={"filter-link"}
                                onClick={onToggleSort.bind(this, "marketStats.change")} />
                        </div>
                        <MediaQuery minDeviceWidth={480}>
                            <div className="table-cell table-cell-hour24High">
                                <Translate component="span" content="dashboard.chanage_24_high"/>
                            </div>
                            <div className="table-cell table-cell-hour24Low">
                                <Translate component="span" content="dashboard.chanage_24_low"/>
                            </div>
                            <div className={cnames("table-cell table-cell-hour24Volume", this.getClassName("marketStats.volumeQuote"))}>
                                <Translate content="dashboard.chanage_24_volume"
                                    className={"filter-link"}
                                    component="a"
                                    onClick={onToggleSort.bind(this, "marketStats.volumeQuote")} />
                            </div>
                        </MediaQuery>
                    </div>
                </div>
            </div>
        );
    }
}

export default MarketsHeader;
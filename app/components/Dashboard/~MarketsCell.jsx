
import React from "react";
import ChainTypes from "../Utility/ChainTypes";
import Icon from "../Icon/Icon";
import AssetName from "../Utility/AssetName";
import BindToChainState from "../Utility/BindToChainState";
import MarketsActions from "actions/MarketsActions";
import SettingsActions from "actions/SettingsActions";
import {Link} from "react-router-dom";
import utils from "common/utils";
import cnames from "classnames";

class MarketsCell extends React.PureComponent {
    static propTypes = {
        quote: ChainTypes.ChainAsset.isRequired,
        base: ChainTypes.ChainAsset.isRequired
    };

    constructor() {
        super();

        this.statsInterval = null;
        this.state = {
            imgError: false
        };
    }

    shouldComponentUpdate(np, ns) {
        return (
            //utils.check_market_stats(np.marketStats, this.props.marketStats) ||
            np.base.get("id") !== this.props.base.get("id") ||
            np.quote.get("id") !== this.props.quote.get("id")
        );
    }

    componentDidMount() {
        this._setInterval();
    }

    componentWillUnmount() {
        this._clearInterval();
    }

    componentDidUpdate(nextProps) {
        if (
            nextProps.base.get("id") !== this.props.base.get("id") ||
            nextProps.quote.get("id") !== this.props.quote.get("id")
        ) {
            this._clearInterval();
            this._setInterval(nextProps);
        }
    }

    _setInterval(nextProps = null) {
        let {base, quote} = nextProps || this.props;
        console.log('base, quote', base, quote);
        this.statsChecked = new Date();
        this.statsInterval = MarketsActions.getMarketStatsInterval(
            35 * 1000,
            base,
            quote
        );
    }

    _clearInterval() {
        if (this.statsInterval) this.statsInterval();
    }

    _onError() {
        if (!this.state.imgError) {
            this.setState({
                imgError: true
            });
        }
    }

    _toggleFavoriteMarket(quote, base) {
        let marketID = `${quote}_${base}`;
        if (!this.props.starredMarkets.has(marketID)) {
            SettingsActions.addStarMarket(quote, base);
        } else {
            SettingsActions.removeStarMarket(quote, base);
        }
    }

    render() {
        let { quote } = this.props;

        return <AssetName dataPlace="top" name={quote.get("symbol")} />;
    }
}

export default BindToChainState(MarketsCell);
//export default MarketsRow;

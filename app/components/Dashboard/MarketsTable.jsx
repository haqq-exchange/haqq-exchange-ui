import React from "react";
import PropTypes from "prop-types";
import {connect} from "alt-react";
import {get} from "lodash-es";
import {ChainStore} from "deexjs";
//import {ChainStore} from "bitsharesjs";

import MarketsRow from "./MarketsRow";
import MarketsHeader from "./MarketsTable/MarketsHeader";

import SettingsActions from "actions/SettingsActions";
import MarketsStore from "stores/MarketsStore";
import SettingsStore from "stores/SettingsStore";
import utils from "common/utils";
import { diff } from "deep-object-diff";
import {getAlias} from "../../config/alias";
import GatewayStore from "../../stores/GatewayStore";


class MarketsTable extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor(props) {
        //console.log('MarketsTableInit');
        super(props);
        this.state = {
            filter: "",
            showFlip: true,
            showHidden: false,
            markets: [],
            sortBy: "marketStats.volumeBase",
            sortDirection: false,
            sorted: [{
                "id": "volumeBase",
                "desc": true
            }]
        };

        this._sortTableDefault = this._sortTableDefault.bind(this);
        this._sortTableChange = this._sortTableChange.bind(this);
        this._sortTablePrice = this._sortTablePrice.bind(this);
        this._onToggleSort = this._onToggleSort.bind(this);
        this.setTop24Volume = this.setTop24Volume.bind(this);
        this.update = this.update.bind(this);
        //this._handleHide = this._handleHide.bind(this);
        //this._handleFlip = this._handleFlip.bind(this);

    }

    componentDidMount() {
        this.update();
        setTimeout(this.setTop24Volume, 5000);
    }

    componentDidUpdate(prevProps) {
        //console.log('MarketsTableInit componentDidUpdate');
        let diffProps = diff(prevProps, this.props);

        if (this.hasUpdateMarkets()) {
            setTimeout(this.setTop24Volume, 5000);
        }
        if (Object.keys(diffProps).length > 0) {
            ChainStore.unsubscribe(this.update);
            this.update();
            ChainStore.subscribe(this.update);
        }
    }

    componentWillUnmount() {
        //console.log('MarketsTableInit componentWillUnmount');
        ChainStore.unsubscribe(this.update);
    }

    shouldComponentUpdate(nextProps, nextState) {
        let diffProps = diff(nextProps, this.props);
        let diffState = diff(nextState, this.state);
        // console.log('diffState', diffState, nextProps, nextState);

        return Object.keys(diffProps).length > 0
            || Object.keys(diffState).length > 0
            || this.hasUpdateMarkets();
    }

    hasUpdateMarkets = () => {
        const {viewMarketsCharts} = this.props;
        let keysCharts = Object.keys(viewMarketsCharts);
        let hasUpdate = !keysCharts.length || false;
        if(keysCharts.length > 0) {
            keysCharts.map(viewMarkets=>{
                let marketLine = viewMarketsCharts[viewMarkets];
                Object.keys(marketLine).map(line=>{
                    let marketItem = marketLine[line];
                    if( marketItem?.autoChart && !hasUpdate ) {
                        hasUpdate = true;
                    }
                });
            });
        }
        return hasUpdate;
    };

    async update(nextProps = null) {
        //console.log('MarketsTableInit update');
        let props = nextProps || this.props;
        let showFlip = props.forceDirection;
        if( showFlip !== this.state.showFlip ) {
            this.setState({showFlip});
        }

        let markets = [];
        if (props.markets && props.markets.length > 0) {
            markets = await props.markets
                .map(market => {
                    let quote = ChainStore.getAsset(market.quote);
                    let base = ChainStore.getAsset(market.base);

                    if (!base || !quote) return null;
                    let marketName = `${market.base}_${market.quote}`;
                    let marketStats = props.allMarketStats.get(marketName, {});


                    return {
                        key: marketName,
                        inverted: undefined,
                        quote: market.quote,
                        base: market.base,
                        quote_asset: quote,
                        base_asset: base,
                        isHidden: props.hiddenMarkets.includes(marketName),
                        isFavorite: props.isFavorite,
                        marketStats: marketStats,
                        isStarred: props.starredMarkets.has(marketName)
                    };
                })
                .filter(a => a !== null);
        }


        this.setState({markets});
    }

    setTop24Volume = () => {

        const {allMarketStats, backedCoins} = this.props;
        if( !backedCoins.size ) return false;

        // console.log("backedCoins", backedCoins);
        let topHour24Volume = [];
        let coinsName = backedCoins.get("TDEX").map(coins=>coins.symbol);
        let pairsMarkets = Object.keys(allMarketStats.toJS());
        // console.log("pairsMarkets", pairsMarkets);
        pairsMarkets.map(pairs => {
            let [quote,base] = pairs.split("_");
            let marketStats = allMarketStats.get(pairs, {});
            let validAsset = coinsName.includes(quote) || coinsName.includes(base);
            // console.log("pairsArray", pairsArray);
            // console.log("pairs", pairs);
            // console.log("validAsset", quote,base, validAsset);
            //console.log("coinsName", coinsName);
            // console.log("marketStats.volumeQuote", marketStats.volumeQuote);
            if( validAsset && marketStats.volumeQuote ) {
                topHour24Volume.push({
                    quote,
                    base,
                    volume: marketStats.volumeQuote
                });
            }
        });
        // console.log("topHour24Volume", topHour24Volume);
        if( topHour24Volume.length ) {
            SettingsActions.setTopHour24Volume(topHour24Volume).then(result=>{
                if( result ) {
                    SettingsActions.changeViewSetting(result);
                    SettingsActions.changeChartsObjectList(result.viewMarketsCharts)
                        .then(()=>SettingsStore.addChartsMarkets());
                    // SettingsActions.changeViewSetting(result);
                    // SettingsActions.changeChartsObjectList(result.viewMarketsCharts);
                    // SettingsStore.addChartsMarkets();
                }
            });
        }
    };

    _handleFilterInput(e) {
        e.preventDefault();
        this.setState({
            filter: e.target.value.toUpperCase()
        });
    }

    _onToggleSort = (key) => {
        const { sortBy, sortDirection } = this.state;
        this.setState({
            sortBy: key,
            sortDirection: key === sortBy ? !sortDirection : true
        });

    };

    _sortTableDefault = (floatA, floatB, direction) => {
        //console.log("_sortTableDefault", this.state.markets );
        // let floatB = typeof sortB === "string" ? parseFloat(sortB) : sortB,
        //     floatA = typeof sortA === "string" ? parseFloat(sortA) : sortA;
        if( direction ) {
            return floatA > floatB ? 1 : floatB > floatA ? -1 : 0 ;
        } else {
            return floatA > floatB ? -1 : floatB > floatA ? 1 : 0 ;
        }
    };

    _sortTableChange = ( sortA, sortB, direction) => {
        let floatB = typeof sortB === "string" ? parseFloat(sortB) : sortB,
            floatA = typeof sortA === "string" ? parseFloat(sortA) : sortA;

        if( direction ) {
            return floatA > floatB ? 1 : floatB > floatA ? -1 : 0 ;
        } else {
            return floatA > floatB ? -1 : floatB > floatA ? 1 : 0 ;
        }

    };

    _sortTablePrice = (sortA, sortB, direction) => {

        if (sortA && !sortB) {
            return 1;
        } else if (!sortA && sortB) {
            return -1;
        } else if (sortA && sortB) {
            let toRealA = sortA.toReal(),
                toRealB = sortB.toReal();
            if( direction ) {
                return toRealA > toRealB ? 1 : toRealB > toRealA ? -1 : 0 ;
            } else {
                return toRealA > toRealB ? -1 : toRealB > toRealA ? 1 : 0 ;
            }
        } else {
            return -1;
        }
    };

    _getMarketsRow(markets, filter) {
        const {sortBy, sortDirection} = this.state;
        return markets
            .filter(m => {
                if (!!filter || m.isStarred) return true;
                if (m.marketStats && m.marketStats.price) return true;
                return this.props.onlyLiquid;
            })
            .sort((a, b) => {
                //const {sortBy, sortDirection} = this.state;

                //console.log("b.marketStats", sortBy, get(a, sortBy), get(b, sortBy));

                switch (sortBy) {
                    case "quote":
                        return this._sortTableDefault(getAlias(get(a, sortBy)) , getAlias(get(b, sortBy)), sortDirection);
                    case "price":
                        return this._sortTablePrice(b.marketStats[sortBy], a.marketStats[sortBy], sortDirection);
                    case "change":
                        return this._sortTableChange(a.marketStats[sortBy], b.marketStats[sortBy], sortDirection);
                    case "marketStats.change":
                    case "marketStats.volumeBase":
                    case "marketStats.volumeQuote":
                        return this._sortTableChange(get(a, sortBy) , get(b, sortBy), sortDirection);
                    default:
                        return this._sortTableDefault(get(a, sortBy) , get(b, sortBy), sortDirection);
                }
            })
            .map(row => {
                let visible = true;

                if (row.isHidden !== this.state.showHidden) {
                    visible = false;
                } else if (filter) {
                    const quoteObject = ChainStore.getAsset(row.quote);
                    const baseObject = ChainStore.getAsset(row.base);

                    const {isBitAsset: quoteIsBitAsset} = utils.replaceName(
                        quoteObject
                    );
                    const {isBitAsset: baseIsBitAsset} = utils.replaceName(
                        baseObject
                    );

                    let quoteSymbol = row.quote;
                    let baseSymbol = row.base;

                    if (quoteIsBitAsset) {
                        quoteSymbol = "bit" + quoteSymbol;
                    }

                    if (baseIsBitAsset) {
                        baseSymbol = "bit" + baseSymbol;
                    }

                    const filterPair = filter.includes(":");

                    if (filterPair) {
                        const quoteFilter = filter.split(":")[0].trim();
                        const baseFilter = filter.split(":")[1].trim();

                        visible =
                            quoteSymbol
                                .toLowerCase()
                                .includes(String(quoteFilter).toLowerCase()) &&
                            baseSymbol
                                .toLowerCase()
                                .includes(String(baseFilter).toLowerCase());
                    } else {
                        let quoteLowerCase = quoteSymbol.toLowerCase();

                        visible =
                            quoteLowerCase.includes(String(filter).toLowerCase()) ||
                            baseSymbol.toLowerCase().includes(String(filter).toLowerCase());
                    }
                }
                if (!visible) return null;

                return row;
            }).filter(r => !!r);
    }

    render() {
        // console.log('MarketsTableInit render');
        let {markets, sortBy, sortDirection, filter} = this.state;
        this.loaded = true;
        const marketRows = this._getMarketsRow(markets, filter);
        console.log("marketRows", marketRows);

        return(
            <div className="grid-block tab-layout vertical">
                <MarketsHeader
                    handleFilterInput={this._handleFilterInput.bind(this)}
                    onToggleSort={this._onToggleSort}
                    sortBy={sortBy}
                    sortDirection={sortDirection}
                />

                {marketRows.map(market=>{
                    //return <div>{market.key}</div>;
                    return <MarketsRow
                        key={market.key}
                        {...market}
                        {...this.props}/>;
                })}
            </div>
        );
    }
}

export default connect(
    MarketsTable,
    {
        listenTo() {

            return [SettingsStore, MarketsStore];
        },
        getProps() {
            let {
                marketDirections,
                hiddenMarkets
            } = SettingsStore.getState();

            return {
                marketDirections,
                hiddenMarkets,
                allMarketStats: MarketsStore.getState().allMarketStats,
                backedCoins: GatewayStore.getState().backedCoins,
                starredMarkets: SettingsStore.getState().starredMarkets,
                chartsObjectList: SettingsStore.getState().chartsObjectList,
                onlyLiquid: SettingsStore.getState().viewSettings.get(
                    "onlyLiquid",
                    true
                ),
                viewMarketsCharts: SettingsStore.getState().viewSettings.get(
                    "viewMarketsCharts",
                    {}
                )
            };
        }
    }
);

import React from "react";
import FormattedValue from "../Utility/FormattedValue";
import {
    MarketsCellPair,
    MarketsCellPrice
} from "./MarketsCell/index";
import SettingsActions from "actions/SettingsActions";
import utils from "common/utils";
import cnames from "classnames";
import MediaQuery from "react-responsive";


const MarketsRow = props => {

    // console.log("props", props);

    const _toggleFavoriteMarket = (quote, base) => {
        let marketID = `${quote}_${base}`;
        if (!this.props.starredMarkets.has(marketID) ) {
            SettingsActions.addStarMarket(quote, base);
        } else {
            SettingsActions.removeStarMarket(quote, base);
        }
    };

    let {
        base,
        quote,
        quotes,
        marketStats,
        quote_asset,
        base_asset
    } = props;

    let cnChanges = {
        "positive": marketStats.change > 0,
        "negative": marketStats.change < 0
    };


    let volumeArray = quotes[0] === quote ? ["volumeBase", quote_asset] : ["volumeQuote", base_asset];


    return (
        <div className="table-body">
            <a
                onClick={()=>_toggleFavoriteMarket(quote, base)}
                className={cnames("table-cell table-cell-star", {
                    "is-active": props.starredMarkets.has([quote, base].join("_"))
                })}>
                <i/>
            </a>
            <div className={cnames("table-cell table-cell-asset" )}>
                <MarketsCellPair {...props} />
            </div>
            <div className={cnames("table-cell table-cell-price", cnChanges)}>
                <MarketsCellPrice {...props} />
            </div>
            <div className={cnames("table-cell table-cell-hour24Chanage", cnChanges)}>
                {marketStats && marketStats.change ? marketStats.change + "%" : "-"}
            </div>
            <MediaQuery minDeviceWidth={480}>
                <div className="table-cell table-cell-hour24High">
                    {marketStats && marketStats.volumeLowest ?
                        <FormattedValue value={marketStats.volumeLowest} maximumFractionDigits={volumeArray[1].get("precision")} /> : "-"}
                </div>
                <div className="table-cell table-cell-hour24Low">
                    {marketStats && marketStats.volumeHighest ?
                        <FormattedValue value={marketStats.volumeHighest} maximumFractionDigits={volumeArray[1].get("precision")} /> : "-"}
                </div>
                <div className={cnames("table-cell table-cell-hour24Volume")}>
                    {marketStats[volumeArray[0]] ? utils.format_volume(marketStats[volumeArray[0]], volumeArray[1].get("precision")) : "-"}
                </div>
            </MediaQuery>
        </div>
    );
};

//export default BindToChainState(MarketsRow);
export default MarketsRow;
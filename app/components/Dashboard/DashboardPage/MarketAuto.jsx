import React from "react";
import LoadingIndicator from "Components/LoadingIndicator";
import SettingsStore from "../../../stores/SettingsStore";

export  default class MarketAuto extends React.Component {


    componentDidMount() {
        SettingsStore.addChartsMarkets();
    }


    render(){
        return(
            <div className={"chart-market-item add-chart"}>
                <div className="spinner">
                    <div className="bounce1" style={{"backgroundColor": "#ccc"}} />
                    <div className="bounce2" style={{"backgroundColor": "#ccc"}} />
                    <div className="bounce3" style={{"backgroundColor": "#ccc"}} />
                </div>
            </div>
        );
    }
}

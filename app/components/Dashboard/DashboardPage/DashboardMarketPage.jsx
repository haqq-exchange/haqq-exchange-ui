import React from "react";
import loadable from "loadable-components";

const DashboardTabs = loadable(() => import("./DashboardTabs"));
const ChartMarket = loadable(() => import("./ChartMarket"));


// import { observer } from "mobx-react";
// import globalState from "App/mobx/globalState";

// @observer
export default class DashboardMarketPage extends React.Component {
    render() {

        return (
            <div className="grid-block dashboard-page" style={{"flexDirection": "column"}}>

                <ChartMarket {...this.props} />

                <DashboardTabs {...this.props} />
            </div>
        );
    }
}
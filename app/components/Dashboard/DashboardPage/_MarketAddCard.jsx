import React from "react";
import {set} from "lodash";
import {ChainStore} from "deexjs";
import SettingsActions from "actions/SettingsActions";
import SettingsStore from "stores/SettingsStore";
import ClickOutside from "react-click-outside";
import {getAlias} from "config/alias";
import ImageLoad from "Utility/ImageLoad";

export  default class _MarketAddCard extends React.Component {
    constructor(props){
        super(props);
        this.state={
            typeShow: null,
            filter: null,
            quote: null,
            base: null
        };
    }

    _handleFindMarket = (event) => {
        this.setState({
            filter: event.target.value.toUpperCase()
        });
    };

    _selectedAsset = (asset) => {
        let {typeShow} = this.state;
        let chainAsset = ChainStore.getAsset(asset);

        if( chainAsset ) {
            this.setState({
                [typeShow]: asset,
                typeShow: null,
                error: null,
                filter: null
            }, this._addChart );
        } else {
            this.setState({
                error: "Asset " + asset.toUpperCase() + " not found"
            });
        }


    };

    _addChart() {
        let {quote, base} = this.state;
        if( !quote || !base ) return;
        const {viewSettings,  chartPosition} = this.props;
        let viewMarketsCharts = viewSettings.get("viewMarketsCharts") || {};
        set(viewMarketsCharts, chartPosition, {
            "quote": quote,
            "base": base,
            "autoChart": false,
            "positionChart": chartPosition
        });

        SettingsActions.changeViewSetting({
            viewMarketsCharts: viewMarketsCharts
        });
        SettingsStore.addChartsMarkets();

    }
    _autoChart() {
        const {viewSettings,  chartPosition} = this.props;
        let viewMarketsCharts = viewSettings.get("viewMarketsCharts") || {};
        set(viewMarketsCharts, chartPosition, {
            "positionChart": chartPosition,
            "autoChart": true
        });

        SettingsActions.changeViewSetting({
            viewMarketsCharts: viewMarketsCharts
        });
        SettingsStore.addChartsMarkets();
    }

    renderDropDownAsset = () => {
        let {filter, typeShow} = this.state;
        let typeAsset = typeShow === "quote" ? "base" : "quote";
        let selectedAsset = this.state[typeAsset];
        let defaultMarkets = SettingsStore.getState().defaultMarkets,
            uniqueQuote = {};

        defaultMarkets
            .filter(item => {
                if( filter ){
                    return item.quote.indexOf(filter) !== -1 || getAlias(item.quote).indexOf(filter) !== -1;
                }
                return true;
            })
            .map(item => {
                /* кроме текущих */
                if( selectedAsset && item.quote !== selectedAsset ){
                    uniqueQuote[item.quote] = true;
                } else if( item.quote !== selectedAsset ) {
                    uniqueQuote[item.quote] = true;
                }
                return item.base;
            });

        return (
            <div className={"chart-market-add-tab"}>
                <ClickOutside className={"chart-market-app-tab-outside"} onClickOutside={()=>this.setState({ typeShow: null})}>
                    <div className="chart-market-add-tab-wrap">
                        <input type="text" className={"chart-market-add-tab-input"} onChange={(event)=>this._handleFindMarket(event)}/>
                        <div className={"chart-market-add-tab-drop-down"}>
                            {Object.keys(uniqueQuote)
                                .sort()
                                .map(quote=>{
                                    return(
                                        <div key={quote} onClick={()=>{this._selectedAsset(quote);}} className={"chart-market-add-tab-quote"}>
                                            <ImageLoad customStyle={{
                                                maxWidth: 25,
                                                marginRight: 5
                                            }} imageName={`${quote.toLowerCase()}.png`} />
                                            {getAlias(quote)}
                                        </div>
                                    );
                                })}
                        </div>
                    </div>
                </ClickOutside>
            </div>
        );
    };

    render(){
        let { chart } = this.props;
        let {typeShow, quote, base, error } = this.state;

        return(
            <div className={"chart-market-item add-chart"}>
                {error ? <div className={"chart-market-error"}>{error}</div> : null}
                <div className="chart-market-drop-down">
                    <div className="chart-market-drop-down-wrap">
                        <a onClick={()=>this.setState({ typeShow: "quote"})}>
                            {getAlias(quote) || "Quote"}
                        </a>
                        {typeShow === "quote" && this.renderDropDownAsset()}
                    </div>
                    /
                    <div className="chart-market-drop-down-wrap">
                        <a onClick={()=>this.setState({ typeShow: "base" })}>
                            {getAlias(base ) || "Base"}
                        </a>
                        {typeShow === "base" && this.renderDropDownAsset()}
                    </div>
                </div>
                {!chart.autoChart ? <span className="chart-market-auto" onClick={()=>this._autoChart()}>
                    auto
                </span>: null}
            </div>
        );
    }
}

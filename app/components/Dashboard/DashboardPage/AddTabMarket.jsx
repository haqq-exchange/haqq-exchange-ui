import React from "react";
import SettingsStore from "stores/SettingsStore";
import ClickOutside from "react-click-outside";
import SettingsActions from "actions/SettingsActions";
import DefaultModal from "components/Modal/DefaultModal";
import ImageLoad from "Utility/ImageLoad";
import Translate from "react-translate-component";
import cname from "classnames";
import MediaQuery from "react-responsive";
import ResponseOption from "config/response";
import {getAlias} from "config/alias";
import marketUtils from "../../../lib/common/market_utils";
import GatewayStore from "../../../stores/GatewayStore";
import topMarkets from "../../../config/markets";
import {availableGateways} from "common/gateways";

export default class AddTabMarket extends React.PureComponent {
    constructor(props){
        super(props);
        this.state = {
            isShow: false,
            allMarkets: {},
            filter: "",
        };
        this._closeDropDown = this._closeDropDown.bind(this);
        this._handleFindMarket = this._handleFindMarket.bind(this);
    }

    componentDidMount() {
        this.getAllMarkets();
    }


    _handleFindMarket = (event) => {
        this.setState({
            filter: event.target.value.toUpperCase()
        });
    };

    _closeDropDown = (callback) => {
        this.setState({isShow: false}, () => {
            if( typeof callback === "function" ) callback();
        });
    };

    renderModal = () => {
        const {isShow} = this.state;
        return (
            <DefaultModal
                id={"addTab"}
                ref={(ref) => this.TabModal = ref}
                isOpen={isShow}
                className="add-tab-modal"
                onRequestClose={this._closeDropDown}>
                <div className={"add-tab-modal-wrap"}>
                    {this._renderTabWrap()}
                    <button className={"add-tab-modal-close"} onClick={this._closeDropDown}>+</button>
                </div>
            </DefaultModal>
        );
    };

    renderDropdown = () => {
        return (
            <div className={"add-tab"}>
                <ClickOutside className={"add-tab-outside"} onClickOutside={this._closeDropDown}>
                    {this._renderTabWrap()}
                    <button className={"add-tab-close"} onClick={this._closeDropDown}>+</button>
                </ClickOutside>
            </div>
        );
    };

    getAllMarkets = () => {
        let {allMarkets} = this.state;
        let {backedCoins, preferredBases} = this.props;

        var uniqueArray = function(arrArg) {
            return arrArg.filter(function(elem, pos,arr) {
                return arr.indexOf(elem) === pos;
            });
        };
        Object.keys(availableGateways).map(gateway => {
            let itemGateways = availableGateways[gateway ];
            if( itemGateways.isEnabled ) {
                try {
                    const backedCoinItem = backedCoins.get(gateway, []);
                    console.log("backedCoinItem", backedCoinItem);
                    if( backedCoinItem.hasOwnProperty("length") ) {
                        let markets = uniqueArray(backedCoinItem
                            .map(coins => coins.symbol)
                            .filter(a=>a)
                        ).sort();
                        //console.log("markets", markets);
                        markets.map(market=>{
                            marketUtils.addMarkets(allMarkets, market, preferredBases);
                        });
                        this.setState({
                            allMarkets
                        });
                    }
                } catch (err) {
                    console.log("err", err);
                }
            }
        });
    };


    _renderTabWrap = () => {
        let {marketsTab, preferredBases, defaultMarkets} = this.props;
        let {filter, allMarkets} = this.state;
        const MarketsTab = marketsTab.size ? marketsTab : preferredBases;
        let uniqueQuote = {};
        Object.values({...allMarkets,...defaultMarkets.toJS()}).map(item => {
            /* кроме текущих */
            if( !MarketsTab.has(item.quote) || !MarketsTab.has(item.base)  ){
                /* если есть фильтр */
                if( filter){
                    if(
                        item.quote.indexOf(filter) !== -1 ||
                        getAlias(item.quote).indexOf(filter) !== -1 ||
                        item.base.indexOf(filter) !== -1 ||
                        getAlias(item.base).indexOf(filter) !== -1
                    )
                        uniqueQuote[item.quote] = true;
                    uniqueQuote[item.base] = true;
                } else {
                    uniqueQuote[item.quote] = true;
                    uniqueQuote[item.base] = true;
                }
            }
            return item.base;
        });
        console.log("MarketsTab", MarketsTab.toJS() );
        console.log("allMarkets", allMarkets );
        console.log("defaultMarkets", defaultMarkets.toJS());
        console.log("uniqueQuote", uniqueQuote);
        return (
            <div className="add-tab-wrap">
                <input type="text" className={"add-tab-input"} onChange={this._handleFindMarket.bind(this)}/>
                <div className={"add-tab-drop-down"}>
                    {Object.keys(uniqueQuote)
                        .sort()
                        .map(quote=>{
                            return(
                                <div key={quote} onClick={()=>{
                                    SettingsActions.addMarketTab(quote);
                                    this._closeDropDown(()=>{
                                        SettingsActions.changeViewSetting({
                                            dashboardTab: MarketsTab.size + 1 /* в deex */
                                        });
                                        this.props.afterAdd();
                                    });

                                }} className={"add-tab-quote"}>
                                    <ImageLoad
                                        customStyle={{
                                            maxWidth: 25,
                                            marginRight: 5
                                        }}
                                        imageName={`${quote.toLowerCase()}.png`} />
                                    {getAlias(quote)}
                                </div>
                            );
                        })}
                </div>
            </div>
        );
    };

    render(){
        const {isShow} = this.state;
        if( isShow ){
            return (
                <MediaQuery {...ResponseOption.mobile}>
                    {(matches) => {
                        if (matches) {
                            return this.renderModal();
                        } else {
                            return this.renderDropdown();
                        }
                    }}
                </MediaQuery>
            );
        } else {
            return this.renderPlus();
        }
    }

    renderPlus = () => {
        return (
            <li className={"tab-item tab-item-add"}>
                <span className={"tab-link tab-new-link"} onClick={()=>{this.setState({isShow: true});}}>+</span>
            </li>
        );
    };
}

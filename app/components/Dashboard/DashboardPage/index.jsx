import React from "react";
import {connect} from "alt-react";

// import ProductPage from "../../ProductPage";
// import MarketCard from "../MarketCard";
import AccountStore from "stores/AccountStore";
import SettingsStore from "stores/SettingsStore";
// import {Link} from "react-router-dom";
// import Translate from "react-translate-component";
// import ActionSheet from "react-foundation-apps/src/action-sheet";
// import TranslateWithLinks from "Utility/TranslateWithLinks";

// import LoadingIndicator from "../LoadingIndicator";
// import LoginSelector from "../LoginSelector";
// import MarketsCardStore from "stores/MarketsCardStore";

// import {Tabs, Tab} from "Utility/Tabs";
// import {StarredMarkets, FeaturedMarkets} from "../Markets";
// import {getPossibleGatewayPrefixes} from "common/gateways";
// import MarketsStore from "../../stores/MarketsStore";

import loadable from "loadable-components";
import "./dashboard.scss";
import AltContainer from "alt-container";
import GatewayStore from "../../../stores/GatewayStore";

// const AddTabMarket = loadable(() => import("./AddTabMarket"));
// const DeleteTabMarket = loadable(() => import("./DeleteTabMarket"));

// const ChartMarket = loadable(() => import("./ChartMarket"));
const DashboardMarketPage = loadable(() => import("./DashboardMarketPage"));
const DashboardHelloPage = loadable(() => import("./DashboardHelloPage"));


class DashboardPage extends React.Component {
    render() {

        // console.log("DashboardMarketPage", this.props);

        /*if (!passwordAccount) {
            return <DashboardHelloPage {...this.props} />;
        } else {
        }*/
        return <DashboardMarketPage {...this.props} />;

    }
}


class DashboardPageContainer extends React.Component {
    render() {
        let {
            myActiveAccounts,
            myHiddenAccounts,
            passwordAccount,
            accountsLoaded,
            wallet_name,
            currentAccount,
            refsLoaded
        } = AccountStore.getState();

        // console.log("SettingsStore.getState().chartsObjectList", SettingsStore.getState().chartsObjectList);

        return (
            <AltContainer
                stores={[AccountStore, SettingsStore]}
                inject={{
                    myActiveAccounts,
                    myHiddenAccounts,
                    passwordAccount,
                    wallet_name,
                    backedCoins: GatewayStore.getState().backedCoins,
                    defaultMarkets: SettingsStore.getState().defaultMarkets,
                    settings: () => {
                        return SettingsStore.getState().settings;
                    },
                    currentAccount: () => {
                        return currentAccount || passwordAccount;
                    },
                    accountsReady: () => {
                        return accountsLoaded && refsLoaded;
                    },
                    preferredBases: () => {
                        return SettingsStore.getState().preferredBases;
                    },
                    marketsTab: () => {
                        return SettingsStore.getState().marketsTab;
                    },

                    chartsMarkets: () => {
                        return SettingsStore.getState().chartsMarkets;
                    },
                    viewSettings: () => {
                        return SettingsStore.getState().viewSettings;
                    }
                }}
            >
                <DashboardPage {...this.props} />
            </AltContainer>
        );
    }
}

export default DashboardPageContainer;

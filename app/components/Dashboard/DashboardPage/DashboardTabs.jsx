import React from "react";
import {Tab, Tabs} from "Components/Utility/Tabs";
import {FeaturedMarkets, StarredMarkets} from "Components/Dashboard/Markets";
import {getPossibleGatewayPrefixes} from "common/gateways";
import loadable from "loadable-components";
import {getAlias} from "config/alias";
import ImageLoad from "Utility/ImageLoad";
import Icon from "Components/Icon/Icon";

const AddTabMarket = loadable(() => import("./AddTabMarket"));
const PairExchange = loadable(() => import("./PairExchange"));
const DeleteTabMarket = loadable(() => import("./DeleteTabMarket"));

export default class DashboardTabs extends React.Component {

    constructor(props){    // Optional, declare a class field
        super(props);
        this.childTabs=null;
    }

    afterAdd=()=>{
        // console.log("this.refTabs", this.childTabs, this);
        if( this.childTabs ) {
            let offsetWidth = this.childTabs.offsetWidth;
            this.childTabs.scroll(offsetWidth, 0);
        }
    };

    childRefTabs = (ref) => {
        this.childTabs = ref;
    };

    render() {
        let {
            marketsTab,
            preferredBases
        } = this.props;

        let tabClassName = {
            item: "tab-item",
            link: "tab-link",
            tabTitle: "tab-title",
            subText: "tab-subtext",
        };

        const notDeleteAsset = __GBLTN_CHAIN__ ? ["GBLTEST"] : (__GBL_CHAIN__ ? ["GBL"] : ["HAQQ"]);
        const MarketsTab = marketsTab?.size ? marketsTab : preferredBases;

        if(!MarketsTab) return null;

        return (
            <Tabs
                defaultActiveTab={1}
                segmented={false}
                collapsed={false}
                refProp={this.childRefTabs}
                setting="dashboardTab"
                className="account-tabs"
                tabsClass="dashboard-tabs"
            >
                <Tab title="" tabClassName={Object.assign({}, tabClassName, {item: "tab-item tab-item-star"})}>
                    <StarredMarkets />
                </Tab>
                <Tab title={
                    <span className={"tab-title-infinity"}>
                        <Icon name={"infinity"} size={"1_5x"} />
                        <small>Any</small>
                    </span>
                } tabClassName={Object.assign({}, tabClassName, {item: "tab-item tab-item-pair"})}>
                    <PairExchange {...this.props} />
                </Tab>

                {MarketsTab.map(q => {
                    let classNameLast;
                    let title = (
                        <span>
                            <ImageLoad customStyle={{
                                maxWidth: 30,
                                marginRight: 5
                            }} imageName={`${q.toLowerCase()}.png`} />
                            &nbsp;
                            {getAlias(q)}
                            { notDeleteAsset.indexOf(q) ? <DeleteTabMarket name={q} /> : null}
                        </span>
                    );
                    if(MarketsTab.size < 7  && MarketsTab.last() === q) {
                        classNameLast = tabClassName.item + "-last";
                    }

                    return (
                        <Tab key={q} title={title} className={classNameLast} tabClassName={tabClassName}>
                            <FeaturedMarkets quotes={[q]} />
                        </Tab>
                    );
                })}
                {MarketsTab.size < 7 &&  <AddTabMarket
                    afterAdd={this.afterAdd}
                    {...this.props} />}
            </Tabs>
        );
    }
}

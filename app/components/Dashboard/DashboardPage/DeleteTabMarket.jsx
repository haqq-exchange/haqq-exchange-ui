import React from "react";
import SettingsActions from "actions/SettingsActions";

export default class DeleteTabMarket extends React.PureComponent {
    _deleteTabs = (name) => {
        SettingsActions.changeViewSetting({
            dashboardTab: 1 /* в deex */
        });
        setTimeout(function() {
            SettingsActions.deleteMarketTab(name);
        }, 350);
    };

    render() {
        const {name} = this.props;
        return (<del onClick={this._deleteTabs.bind(this, name)}><i/></del>);
    }
}
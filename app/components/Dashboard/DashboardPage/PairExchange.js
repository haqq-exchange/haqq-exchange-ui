import React from "react";
import ChainTypes from "Utility/ChainTypes";
import {Link} from "react-router-dom";
import MarketsCardActions from "../../../actions/MarketsCardActions";
import MarketsActions from "../../../actions/MarketsActions";
import BindToChainState from "Utility/BindToChainState";
import {connect} from "alt-react";
import MarketsStore from "../../../stores/MarketsStore";
import MarketsCardStore from "../../../stores/MarketsCardStore";
import SettingsStore from "../../../stores/SettingsStore";
import {diff} from "deep-object-diff";
import {ChainStore} from "deexjs";
import utils from "../../../lib/common/utils";
import cNames from "classnames";
import SettingsActions from "../../../actions/SettingsActions";
import Translate from "react-translate-component";
import {getAlias} from "../../../config/alias";
import MediaQuery from "react-responsive";
import GatewayStore from "../../../stores/GatewayStore";

// import {Column, Table} from "react-virtualized";
import "react-virtualized/styles.css"; // only needs to be imported once

class PairExchange extends React.PureComponent {
    constructor(props){
        super(props);
        this.state = {
            pairRows: [],
            filter: "",
        };

        this.update = this.update.bind(this);
    }


    componentDidMount() {
        ChainStore.subscribe(this.update);
    }


    componentDidUpdate(prevProps) {
        let diffProps = diff(prevProps, this.props);

        if (Object.keys(diffProps).length > 0) {
            ChainStore.unsubscribe(this.update);
            ChainStore.subscribe(this.update);
        }
    }

    componentWillUnmount() {
        ChainStore.unsubscribe(this.update);
    }

    async update() {
        const pairRows = await this.getPairRow();
        this.setState({pairRows}, this.setTop24Volume);
    }

    _sortTableChange = ( sortA, sortB, direction) => {
        let floatB = typeof sortB === "string" ? parseFloat(sortB) : sortB,
            floatA = typeof sortA === "string" ? parseFloat(sortA) : sortA;

        if( direction ) {
            return floatA > floatB ? 1 : floatB > floatA ? -1 : 0 ;
        } else {
            return floatA > floatB ? -1 : floatB > floatA ? 1 : 0 ;
        }
    };

    getPairRow = async () => {
        const {
            pairMarkets,
            allMarketStats,
            userMarkets
        } = this.props;

        let allPairMarket = Object.keys(userMarkets.toJS() || {}).concat(pairMarkets)
        // console.log("pairMarkets", pairMarkets);
        // console.log("allPairMarket", allPairMarket);
        // console.log("allMarketStats", allMarketStats.toJS());
        // console.log("defaultMarkets", defaultMarkets.toArray());
        return await allPairMarket.map(pair=>{
            let [base, quote] = pair.split("_");
            let marketStats;
            if(allMarketStats.has(pair)) {
                marketStats = allMarketStats.get(pair);
            }
            return {
                marketStats,
                quote,
                base,
                isMarket: userMarkets.has(pair)
            };
        }).sort((a, b) => {
            let sortDirection = false;
            let sortBy = "volumeQuote";
            return this._sortTableChange(a.marketStats?.[sortBy], b.marketStats?.[sortBy], sortDirection)
        }).filter(a=>a);
    };

    setTop24Volume = () => {

        const {allMarketStats, backedCoins, viewMarketsCharts} = this.props;
        if( !backedCoins.size || viewMarketsCharts ) return false;
        let topHour24Volume = [];
        let coinsName = backedCoins.get("TDEX").map(coins=>coins.symbol);
        let pairsMarkets = Object.keys(allMarketStats.toJS());
        pairsMarkets.map(pairs => {
            let [base, quote] = pairs.split("_");
            let marketStats = allMarketStats.get(pairs, {});
            let validAsset = coinsName.includes(quote) || coinsName.includes(base);
            if( validAsset && marketStats.volumeQuote ) {
                topHour24Volume.push({
                    quote,
                    base,
                    volume: marketStats.volumeQuote
                });
            }
        });
        if( topHour24Volume.length ) {
            SettingsActions.setTopHour24Volume(topHour24Volume).then(result=>{
                if( result ) {
                    SettingsActions.changeViewSetting(result);
                    SettingsActions.changeChartsObjectList(result.viewMarketsCharts)
                        .then(()=>SettingsStore.addChartsMarkets());
                    // SettingsStore.addChartsMarkets();
                }
            });
        }
    };

    render(){
        const { starredMarkets } = this.props;
        const { pairRows } = this.state;

        return (
            <>
                <PairHeader />
                {pairRows.map((pair)=>{
                    if(!pair) return  null;
                    return <PairItemBind
                        key={`pair-item-bind-${pair.quote}-${pair.base}`}
                        starredMarkets={starredMarkets}
                        {...pair} />;
                })}
            </>
        )
    }
}

class PairItem extends React.Component {
    static propTypes = {
        quote: ChainTypes.ChainAsset.isRequired,
        base: ChainTypes.ChainAsset.isRequired
    };

    constructor(props) {
        super(props);
        this.statsInterval = null;
    }

    componentDidMount() {
        // this._subscribeToMarket();
        this._setInterval();
    }

    componentDidUpdate(prevProps) {
        if (this._getChangeProps(prevProps)) {
            this._clearInterval();
            this._setInterval();
        }
    }

    _getChangeProps(props){
        return !!Object.keys(diff(props, this.props)).length;
    }

    componentWillUnmount() {
        this._clearInterval();
    }

    _setInterval() {
        let { quote, base } = this.props;
        if( !base || !quote ) return null;

        this.statsInterval  = MarketsActions.getMarketStatsInterval(
            35 * 1000,
            base, quote
        );
    }

    _clearInterval() {
        if (this.statsInterval) this.statsInterval();
    }

    _toggleFavoriteMarket(quote, base) {
        let marketID = `${quote}_${base}`;
        if (!this.props.starredMarkets.has(marketID) ) {
            SettingsActions.addStarMarket(quote, base);
        } else {
            SettingsActions.removeStarMarket(quote, base);
        }
    }

    render() {
        const {quote, base, marketStats, starredMarkets, isMarket} = this.props;
        console.log("!!!markets", this.props)
        if( !quote || !base ) return null;
        // console.log("marketStats", marketStats, quote.get("symbol"), base.get("symbol"))
        let cnChanges = {
            "positive": marketStats?.change > 0,
            "negative": marketStats?.change < 0
        };
        return (
            <div className={"table-body"} style={{
                "alignItems": "center",
                "padding": "0 10px 0 0"
            }}>
                <a
                    onClick={()=>this._toggleFavoriteMarket(quote.get("symbol"), base.get("symbol"))}
                    className={cNames("table-cell table-cell-star", {
                        "is-active": starredMarkets.has([quote.get("symbol"), base.get("symbol")].join("_"))
                    })}>
                    <i/>
                </a>
                <div className={"table-cell table-cell-asset"} style={{
                    width: "20%",
                    textAlign: "center",
                    padding: "0 10px"
                }}>
                    <Link to={`/market/${[base.get("symbol"), quote.get("symbol")].join("_")}`} style={{
                        color: isMarket ? "gold" : "",
                    }}>
                        {getAlias(base.get("symbol"))}/{getAlias(quote.get("symbol"))}
                    </Link>

                </div>
                <span className={cNames("table-cell")} style={{
                    width: "30%",
                    padding: "0 10px",
                    overflow: "hidden"
                }}>
                    {marketStats && marketStats.price ?
                        utils.price_text( marketStats.price.toReal(), base, quote )
                        : "0.00"} &nbsp; {getAlias(quote.get("symbol"))}
                </span>
                <MediaQuery minDeviceWidth={480}>
                    <span className={cNames("table-cell", cnChanges)} style={{
                        width: "25%",
                        padding: "0 10px"
                    }}>
                        {marketStats && marketStats.change ? marketStats.change + "%" : "-"}
                    </span>
                    <span className={"table-cell"} style={{
                        width: "10%",
                        padding: "0 10px"
                    }}>
                        {marketStats && marketStats.volumeQuote ? utils.format_volume(marketStats.volumeQuote, quote.get("precision"), false) : "0"}
                    </span>
                    <span className={"table-cell"} style={{
                        width: "15%",
                        padding: "0 10px"
                    }}>

                        {marketStats && marketStats.volumeBase ? utils.format_volume(marketStats.volumeBase, base.get("precision"), false) : "0"}
                    </span>
                </MediaQuery>
            </div>
        );
    }
}

export default connect(
    PairExchange,
    {
        listenTo() {
            return [MarketsCardStore, SettingsStore, MarketsStore, GatewayStore];
        },
        getProps() {
            return {
                allMarketStats: MarketsStore.getState().allMarketStats,
                starredMarkets: SettingsStore.getState().starredMarkets,
                backedCoins: GatewayStore.getState().backedCoins,
                defaultMarkets : SettingsStore.getState().defaultAllMarkets,

                userMarkets: SettingsStore.getState().userMarkets,

                pairMarkets: SettingsStore.getState().viewSettings.get("pair_markets"),
                viewMarketsCharts: SettingsStore.getState().viewSettings.get("viewMarketsCharts"),
                marketData: MarketsCardStore.getState().marketData,
                totals: MarketsCardStore.getState().totals,
                marketLimitOrders: MarketsCardStore.getState().marketLimitOrders,
            };
        }
    }
);
const PairItemBind = BindToChainState(PairItem);


const PairHeader = () => {
    return (
        <div className={"table-heads"}>
            <div className={"table-cell table-cell-star"}><i/></div>
            <div className={"table-cell table-cell-asset"} style={{
                width: "20%",
                padding: "0 10px"
            }}>
                <div className="stat-text">
                    <Translate content={"account.trade"} />
                </div>
            </div>
            <div className={"table-cell table-cell-price"} style={{
                width: "30%",
                padding: "0 10px"
            }}>
                <Translate
                    component="div"
                    className="stat-text"
                    content="exchange.latest"
                />
            </div>
            <MediaQuery minDeviceWidth={480}>
                <div className={"table-cell table-cell-price"} style={{
                    width: "25%",
                    padding: "0 10px"
                }}>
                    <Translate
                        component="div"
                        className="stat-text"
                        content="account.hour_24"
                    />
                </div>
                <div className={"table-cell table-cell-price"} style={{
                    width: "25%",
                    textAlign: "left",
                    padding: "0 10px 0 30px"
                }}>
                    <Translate
                        component="div"
                        className="stat-text"
                        content="exchange.volume_24"
                    />
                </div>
            </MediaQuery>
        </div>
    )
};

import React from "react";
import loadable from "loadable-components";
import ResponseOption from "config/response";
import MediaQuery from "react-responsive";
import {Carousel} from "antd";
import {connect} from "alt-react";
import SettingsStore from "../../../stores/SettingsStore";

const MarketCard = loadable(() => import("Components/Dashboard/MarketCard"));
const MarketAddCard = loadable(() => import("./MarketAddCard"));
const MarketAuto = loadable(() => import("./MarketAuto"));


class ChartMarket extends React.Component {

    /*componentDidUpdate(prevProps) {
        if( prevProps.chartsObjectList !== this.props.chartsObjectList ) {
            console.log("chartsMarketsList", prevProps.chartsObjectList , this.props.chartsObjectList);
            console.log("chartsMarketsList ===", prevProps.chartsObjectList === this.props.chartsObjectList);
        }
    }*/

    getChildrenContent = () => {
        const {chartsMarketsList, chartsObjectList} = this.props;

        //console.log("chartsMarketsList !!!", chartsMarketsList.toJS() );
        // let chartsEach = chartsMarketsList.toArray();
        let chartsEach = chartsObjectList;

        return (
            <div className={"chart-market"}>
                {Object.keys(chartsEach).map((chartsKey, index)=>{
                    // console.log("chartsKey, index", chartsKey, index);
                    // console.log(chartsMarketsList.get(chartsKey));
                    let charts = chartsMarketsList.get(chartsKey).toJS();
                    return (
                        <div key={"chart-market-row-" + index} className={"chart-market-row"}>
                            {Object.keys(charts).map((chartKey, indexChart)=>{
                                let chartPosition = [chartsKey, chartKey];
                                let chart = chartsMarketsList.getIn(chartPosition);
                                if( chart.get("quote") && chart.get("base") ) {
                                    let marketId = [chart.get("quote"), chart.get("base")].join("_");
                                    //console.log(chart.toJS());
                                    return <MarketCard key={marketId} {...chart.toJS()} />;
                                } else if ( chart.has("component") ){
                                    const ShowComponent = chart.get("component");
                                    return <ShowComponent key={`market-component-${indexChart}`} {...this.props} />;
                                } else if( chart.get("autoChart")  ) {
                                    return <MarketAddCard
                                        key={`market-add-cart-${indexChart}`}
                                        chartPosition={chartPosition}
                                        hasLoaded
                                        chart={chart.toJS()} {...this.props} />;
                                } else {
                                    // console.log(chart.toJS());
                                    // console.log(this.props);
                                    return <MarketAddCard
                                        key={`market-add-cart-${indexChart}`}
                                        chartPosition={chartPosition}
                                        chart={chart.toJS()} {...this.props} />;
                                }
                            })}
                        </div>
                    );
                })}
            </div>
        );
    };

    onChange = (a,b) => {
        console.log("onChange", a,b);
    };

    getCharts = () =>{
        const {chartsObjectList} = this.props;

        let chartArray = [];

        Object.values(chartsObjectList).map(values=>{
            chartArray = chartArray.concat(Object.values(values));
        });
        return chartArray;
    };

    getChildrenMobileContent = (countSlide) => {
        if (__DEEX_CHAIN__ || __SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__) {
            const {chartsMarketsList} = this.props;
            let chartsEach = chartsMarketsList.toArray();
            return (
                <Carousel
                    className={"market-carusel"}
                    slidesToShow={countSlide}
                    slidesToScroll={1}
                    dots={false}
                    infinite={false}
                    arrows={true}
                    variableWidth={false}
                    adaptiveHeight={false}
                    centerPadding={10}

                    afterChange={this.onChange}>
                    {chartsEach.map((charts, index)=>{
                        let chartsMap = charts.toArray();
                        return (
                            chartsMap.map((chart, indexChart)=>{
                                if( chart.get("quote") && chart.get("base") ) {
                                    let marketId = [chart.get("quote"), chart.get("base")].join("_");
                                    return <MarketCard key={marketId} {...chart.toJS()} />;
                                } else if ( chart.has("component") ){
                                    const ShowComponent = chart.get("component");
                                    return <ShowComponent key={`market-component-${indexChart}`} {...this.props} />;
                                } else {
                                    return <MarketAddCard
                                        key={`market-add-cart-${indexChart}`}
                                        chartPosition={chart.get("positionChart").toJS()}
                                        chart={chart.toJS()}{...this.props} />;
                                }
                            })
                        );
                    })}
                </Carousel>
            )
        } else {
            const {chartsMarketsList, chartsObjectList} = this.props;

            // console.log("chartsMarketsList", chartsMarketsList );
            // let chartsEach = chartsMarketsList.toArray();
            let chartsEach = chartsObjectList;

            const charts = this.getCharts();

            return (
                <Carousel adaptiveHeight={false} afterChange={this.onChange}>
                    {charts.map((chartKey, indexChart)=>{
                        let chartPosition = chartKey.positionChart;
                        let chart = chartsMarketsList.getIn(chartPosition);
                        if( chart && chart.get("quote") && chart.get("base") ) {
                            let marketId = [chart.get("quote"), chart.get("base")].join("_");
                            return <MarketCard key={marketId} {...chart.toJS()} />;
                        } else if ( chart.has("component") ){
                            const ShowComponent = chart.get("component");
                            return <ShowComponent key={`market-component-${indexChart}`} {...this.props} />;
                        } else if( chart.get("autoChart")  ) {
                            return <MarketAddCard
                                key={`market-add-cart-${indexChart}`}
                                chartPosition={chartPosition}
                                hasLoaded
                                chart={chart.toJS()} {...this.props} />;
                        } else {
                            return <MarketAddCard
                                key={`market-add-cart-${indexChart}`}
                                chartPosition={chartPosition}
                                chart={chart.toJS()} {...this.props} />;
                        }
                    })}
                </Carousel>
            );
        }
    };

    render(){
        return (
            <MediaQuery {...ResponseOption.mobile}>
                {(matches) => {
                    if (matches) {
                        return this.getChildrenMobileContent(1);
                    } else {
                        return this.getChildrenMobileContent(6);
                    }
                }}
            </MediaQuery>
        );
    }

    render_(){
        return this.getChildrenMobileContent();
    }
}



export default connect(
    ChartMarket,
    {
        listenTo() {
            return [SettingsStore];
        },
        getProps() {
            //console.log("SettingsStore chartsObjectList", SettingsStore.getState().chartsObjectList);

            return {
                chartsMarketsList: SettingsStore.getState().chartsMarketsList,
                chartsObjectList: SettingsStore.getState().chartsObjectList,
            };
        }
    }
);

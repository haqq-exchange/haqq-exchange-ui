import React from "react";
import loadable from "loadable-components";
import Translate from "react-translate-component";
import {Link} from "react-router-dom";
import InfoModal from "components/Account/CreateAccount/InfoModal";
const DashboardTabs = loadable(() => import("./DashboardTabs"));

export default class DashboardHelloPage extends React.Component {
    constructor(){
        super();
        this.state = {
            isee: false,
            showInfoModal: false
        };
    }

    _infoModal = ( isReg ) => {
        if( isReg ) {
            this.props.history.push("/create-account");
        }
        this.setState({showInfoModal: false});
    };

    render() {
        const {showInfoModal}=this.state;
        return (
            <div className="grid-block dashboard-page" style={{"flexDirection": "column"}}>

                <div className="dashboard-hello">
                    <div className="dashboard-hello-title">
                        <Translate content="public.deex_provides" />
                    </div>
                    <div className="dashboard-hello-text">
                        <Translate content="public.deex_p1" />
                    </div>
                    <div className="dashboard-hello-buttons">
                        <button type={"button"} onClick={()=>this.setState({showInfoModal: true})} className={"btn btn-green"}>
                            <Translate content="public.join" />
                        </button>
                        <Link to={"/market/DEEX_BTS"} className={"btn btn-gray"}>
                            <Translate content="public.view_demo" />
                        </Link>
                    </div>
                </div>

                <DashboardTabs  {...this.props} />

                <InfoModal
                    resolve={this._infoModal}
                    isOpenModal={showInfoModal} />
            </div>
        );
    }
}
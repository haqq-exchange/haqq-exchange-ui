import React from "react";
import PropTypes from "prop-types";
import ChainTypes from "../Utility/ChainTypes";
import BindToChainState from "../Utility/BindToChainState";
import AssetName from "../Utility/AssetName";
import FormattedValue from "../Utility/FormattedValue";
import { set } from "lodash";
import cnames from "classnames";
import SettingsActions from "actions/SettingsActions";
import MarketsCardActions from "actions/MarketsCardActions";
import MarketsActions from "actions/MarketsActions";

import MarketsCardStore from "stores/MarketsCardStore";
import MarketsStore from "stores/MarketsStore";
import SettingsStore from "stores/SettingsStore";
import { connect } from "alt-react";
import utils from "common/utils";
import Translate from "react-translate-component";
import loadable from "loadable-components";
import {getAlias} from "../../config/alias";
import "./DashboardPage/dashboard.scss";

const DepthHighChart = loadable(() => import("./MarketCard/DepthHighChart"));

class MarketCard extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    static propTypes = {
        quote: ChainTypes.ChainAsset.isRequired,
        base: ChainTypes.ChainAsset.isRequired,
        invert: PropTypes.bool
    };

    static defaultProps = {
        invert: true
    };


    constructor() {
        super();

        this.statsInterval = null;

        this.state = {
            imgError: false
        };
    }


    componentDidMount() {
        this._subscribeToMarket();
        this._setInterval();

    }

    componentWillUnmount() {
        this._unSubscribeToMarket();
        this._clearInterval();
    }

    _unSubscribeToMarket() {
        let { quote, base } = this.props;
        if (base && quote && quote.get("id") && base.get("id")) {
            MarketsCardActions.unSubscribeCardMarket(
                quote.get("id"),
                base.get("id")
            );
        }
    }

    _subscribeToMarket(props = this.props) {
        let { quote, base } = props;
        if(!__GBL_CHAIN__) {
        if (base && quote && quote.get("id") && base.get("id")) {
            MarketsCardActions.subscribeCardMarket.defer(
                base,
                quote
            );
        } else null;
        }
    }

    _setInterval(nextProps = null) {
        let { quote, base } = nextProps || this.props;
        if( !base || !quote ) return null;
        if(!__GBL_CHAIN__) {
        this.statsInterval  = MarketsActions.getMarketStatsInterval(
            35 * 1000,
            base, quote
        );
        }
    }

    _clearInterval() {
        if (this.statsInterval) this.statsInterval();
    }

    goToMarket(e) {
        e.preventDefault();

        this.context.router.history.push(
            `/market/${this.props.quote.get("symbol")}_${this.props.base.get("symbol")}`
        );
    }

    getAttrAssets(attr, invert) {
        const { base, quote } = this.props;
        if( !base || !quote ) return null;
        let assetArray        = invert ? [quote.get(attr), base.get(attr)] : [base.get(attr), quote.get(attr)];
        return assetArray.join("_");
    }

    render() {
        let { base, quote, deleteChart, allMarketStats, marketData, marketLimitOrders, totals, children } = this.props;
        if( !base || !quote ) return <MarketCardEmpty />;
        //if (isLowVolume || hide) return null;

        // console.log("MarketCard.jsx this.props", this.props, marketData.toJS());

        const marketCardId    = this.getAttrAssets("id", true);
        const assetStats      = allMarketStats.get(this.getAttrAssets("symbol"));
        const assetStatsInvert= allMarketStats.get(this.getAttrAssets("symbol", true));
        const assetTotal      = totals[marketCardId];
        let assetMarketData   = {};

        if (marketData.get(marketCardId) && marketData.get(marketCardId).size) {
            assetMarketData = marketData.get(marketCardId).toJS();
        }


        let cnChanges = {
            "positive": assetStatsInvert && assetStatsInvert.change > 0,
            "negative": assetStatsInvert && assetStatsInvert.change < 0
        };

        // console.log("assetTotal", assetTotal);
        return (
            <div className="chart-market-item">
                {deleteChart && <DeletedChart {...this.props} />}
                <div className="chart-market-wrap" onClick={(event)=>this.goToMarket(event)}>
                    <div className="chart-market-depth">
                        <DepthHighChart
                            marketReady={true}
                            base={base}
                            quote={quote}
                            chartHeight={80}
                            theme={this.props.settings.get("themes")}
                            feedPrice={null}
                            order={marketLimitOrders.get(marketCardId)}
                            {...assetMarketData}
                        />

                    </div>
                    <div className="chart-market-header">
                        <div className="chart-market-name">
                            <AssetName dataPlace="top" name={quote.get("symbol")}/>
                            <AssetName dataPlace="top" name={base.get("symbol")}/> 
                        </div>
                    </div>
                    <div className="chart-market-value">
                        {/*<div>
                            <Translate content="dashboard.chanage_24_volume"/>
                            <span>
                                {assetStatsInvert && assetStatsInvert.volumeQuote
                                    ? utils.format_volume(assetStatsInvert.volumeQuote, base.get("precision"))
                                    : "0"}
                                &nbsp; {getAlias(quote.get("symbol"))}
                            </span>
                        </div>*/}
                        <span>

                            {assetStats && assetStats.price
                                ? <FormattedValue
                                    value={assetStats.price.toReal(true)}
                                    minimumFractionDigits={Math.max(quote.get("precision"), 0)}
                                    maximumFractionDigits={Math.max(quote.get("precision"), 2)}
                                />
                                : "0.00"}

                        </span>
                    </div>
                    <div className="chart-market-value">
                        <span className={cnames("chart-market-change", cnChanges)}>
                            {assetStatsInvert ? assetStatsInvert.change : "0.00"}%
                        </span>
                    </div>

                    {/*<div className="chart-market-footer">
                        <span className={"chart-market-bid"}>
                            {assetTotal ?<FormattedValue
                                value={assetTotal.bid}
                                minimumFractionDigits={2}
                                maximumFractionDigits={2}
                            /> : null}
                        </span>
                        <span className={"chart-market-ask"}>
                            {assetTotal ?<FormattedValue
                                value={assetTotal.ask}
                                minimumFractionDigits={2}
                                maximumFractionDigits={2}
                            /> : null}
                        </span>

                        {assetTotal ?<FormattedValue
                                value={assetTotal.bid}
                                minimumFractionDigits={Math.max(quote.get("precision"), 0)}
                                maximumFractionDigits={Math.max(quote.get("precision"), 2)}
                            /> : null}

                    </div>*/}
                </div>
                {children ? children : null}
            </div>
        );
    }
}

const MarketCardBind = BindToChainState(MarketCard);

class MarketCardWrapper extends React.Component {
    render() {
        return <MarketCardBind {...this.props} />;
    }
}

export default connect(
    MarketCardWrapper,
    {
        listenTo() {
            return [MarketsStore, MarketsCardStore, SettingsStore];
        },
        getProps() {
            return {
                marketData: MarketsCardStore.getState().marketData,
                allMarketStats: MarketsStore.getState().allMarketStats,
                totals: MarketsCardStore.getState().totals,
                marketLimitOrders: MarketsCardStore.getState().marketLimitOrders,
                //chartsMarketsPostion: SettingsStore.getState().chartsMarketsPostion,
                chartsPostion: SettingsStore.getState().chartsPostion,
                viewMarketsMix: SettingsStore.getState().viewMarketsMix,
                settings: SettingsStore.getState().settings,
                viewSettings: SettingsStore.getState().viewSettings,
            };
        }
    }
);

class DeletedChart extends React.PureComponent {
    constructor(props) {
        super(props);
        this._deletedChart = this._deletedChart.bind(this);
    }

    _deletedChart() {
        const { viewSettings, positionChart, viewMarketsMix , autoChart} = this.props;

        //console.log("this.props", this.props);
        //debugger;

        let viewMarketsCharts = viewSettings.get("viewMarketsCharts") || viewMarketsMix;
        set(viewMarketsCharts, positionChart, {
            positionChart,
            autoChart
        });

        SettingsActions.changeViewSetting({
            viewMarketsCharts: viewMarketsCharts,
        });
        SettingsStore.addChartsMarkets();
    }

    render() {
        return (
            <del className={"chart-market-del"} onClick={()=>this._deletedChart()}>
                <i/>
            </del>
        );
    }
}

const MarketCardEmpty = () => {
    return (
        <div className="chart-market-item empty" />
    );
};

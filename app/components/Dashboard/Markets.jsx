import React from "react";
import {Apis} from "deexjs-ws";
import {connect} from "alt-react";

import {diff} from "deep-object-diff";
import utils from "common/utils";
import SettingsStore from "stores/SettingsStore";
// import MarketsActions from "actions/MarketsActions";
import MarketsStore from "stores/MarketsStore";
import MarketsTable from "./MarketsTable";
import GatewayStore from "../../stores/GatewayStore";
import {availableGateways} from "../../lib/common/gateways";
import marketUtils from "../../lib/common/market_utils";

// import { ChainStore } from 'deexjs';

class StarredMarketsClass extends React.Component {
    render() {
        return (
            <MarketsTable
                markets={this.props.starredMarkets}
                forceDirection={true}
                quotes={[]}
                isFavorite
            />
        );
    }
}

const StarredMarkets = connect(
    StarredMarketsClass,
    {
        listenTo() {
            return [SettingsStore];
        },
        getProps() {
            return {
                starredMarkets: SettingsStore.getState().starredMarkets
            };
        }
    }
);

class FeaturedMarketsClass extends React.Component {
    constructor(props) {
        super(props);

        let chainID = Apis.instance().chain_id;
        if (chainID) chainID = chainID.substr(0, 8);
        this.state = {
            chainID,
            markets: []
        };

        this._getMarkets = this._getMarkets.bind(this);
        this.update = this.update.bind(this);
    }

    componentDidUpdate(prevProps) {
        let diffMarkets = diff(prevProps.markets, this.props.markets);

        // console.log("prevProps", prevProps, this.props);
        if (!utils.are_equal_shallow(prevProps.quotes, this.props.quotes)
            || diffMarkets.size
        ) {
            console.log("FeaturedMarketsClass UPDATE");
            this.update();
        }
    }

    componentDidMount() {
        //console.log('componentDidMount');
        this.update();
    }

    componentWillUnmount() {
        //console.log('componentWillUnmount');
    }

    shouldComponentUpdate(nextProps, nextState) {
        let diffState = diff(nextState, this.state);
        let diffMarkets = diff(nextProps.markets, this.props.markets);
        let diffQuotes = diff(nextProps.quotes, this.props.quotes);

        return Object.keys(diffQuotes).length
            || Object.keys(diffMarkets).length
            || Object.keys(diffState).length;
    }

    _getMarkets(state = this.state, props = this.props) {
        const {chainID} = state;
        console.log("chainID", chainID);

        if (chainID === "c6930e40" || chainID === "8d79be06" || chainID === "8835e222") {
            return props.markets;
        } else {
            // assume testnet
            return [["TEST", "PEG.FAKEUSD"], ["TEST", "BTWTY"]];
        }
    }

    async update(props = this.props) {

        // console.log("props >>> ", props, props.markets.toJS());
        let markets = (__DEEX_CHAIN__ || __SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__)
            ? this._getMarkets(this.state, props)
            : await this._getMarkets(this.state, props);

        markets = await Object.values(markets).filter(market => {
            return props.quotes[0] === market.base;
        }).filter(a => a);

        if (!markets) {
            for (var i = 1; i < props.quotes.length; i++) {
                markets.forEach(m => {
                    let obj = {
                        quote: m.quote,
                        base: props.quotes[i]
                    };
                    let marketKey = `${obj.quote}_${obj.base}`;
                    if (obj.quote !== obj.base && !markets.has(marketKey)) {
                        markets = markets.set(marketKey, obj);
                    }
                });
            }
        }

        this.setState({markets});
    }


    render() {
        let {markets} = this.state;
        let {quotes} = this.props;
        return (
            <MarketsTable
                markets={markets}
                showFlip={false}
                isFavorite={false}
                quotes={quotes}
            />
        );
    }
}

const FeaturedMarkets = connect(
    FeaturedMarketsClass,
    {
        listenTo() {
            return [MarketsStore, SettingsStore];
        },
        getProps() {
            let userMarkets = SettingsStore.getState().userMarkets;
            let defaultMarkets = SettingsStore.getState().defaultAllMarkets;

            if (userMarkets.size) {
                userMarkets.forEach((market, key) => {
                    if (!defaultMarkets.has(key))
                        defaultMarkets = defaultMarkets.set(key, market);
                });
            }

            let allMarkets = defaultMarkets.toJS();

            return {
                markets: allMarkets
            };
        }
    }
);

class TopMarkets extends React.Component {
    render() {
        return <MarketsTable markets={[]}/>;
    }
}

export {StarredMarkets, FeaturedMarkets, TopMarkets};

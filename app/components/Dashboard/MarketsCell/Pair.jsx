import React from "react";
import ChainTypes from "Utility/ChainTypes";
import MarketsActions from "actions/MarketsActions";
import {Link} from "react-router-dom";
import {getAlias} from "config/alias";

class MarketsCellPair extends React.Component {
    static propTypes = {
        quote: ChainTypes.ChainAsset.isRequired,
        base: ChainTypes.ChainAsset.isRequired
    };

    constructor() {
        super();

        this.statsInterval = null;
        this.state = {
            imgError: false
        };
    }

    /*shouldComponentUpdate(np, ns) {
        return (
            //utils.check_market_stats(np.marketStats, this.props.marketStats) ||
            np.base.get("id") !== this.props.base.get("id") ||
            np.quote.get("id") !== this.props.quote.get("id")
        );
    }*/

    componentDidMount() {
        //this._setInterval();
    }

    componentWillUnmount() {
        this._clearInterval();
    }

    componentDidUpdate() {
        /*if (
            prevProps.base.get("id") !== this.props.base.get("id") ||
            prevProps.quote.get("id") !== this.props.quote.get("id")
        ) {
            //this._clearInterval();
            //this._setInterval();
        }*/
    }

    _setInterval(nextProps = null) {
        let {quote_asset, base_asset } = nextProps || this.props;
        this.statsChecked = new Date();
        this.statsInterval = MarketsActions.getMarketStatsInterval(
            35 * 1000,
            quote_asset,
            base_asset
        );
    }

    _clearInterval() {
        if (this.statsInterval) this.statsInterval();
    }

    render() {
        let { quote_asset, base_asset, quotes, quote , isFavorite } = this.props;
        let marketLink = [base_asset.get("symbol"), quote_asset.get("symbol")],
            pairTitle = isFavorite ?  marketLink.map(market=>getAlias(market)).join("/")
                : quotes[0] === quote ? base_asset.get("symbol")
                    : quote_asset.get("symbol");


        return (
            <Link to={`/market/${marketLink.join("_")}`}>
                {getAlias(pairTitle)}
            </Link>
        );
    }
}

//export default BindToChainState(MarketsCellPair);
export default MarketsCellPair;

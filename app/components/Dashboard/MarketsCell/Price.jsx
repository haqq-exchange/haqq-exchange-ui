import React from "react";
import ChainTypes from "Utility/ChainTypes";
import MarketsActions from "actions/MarketsActions";
import utils from "common/utils";
import { diff } from "deep-object-diff";

class MarketsCellPrice extends React.Component {
    static propTypes = {
        quote: ChainTypes.ChainAsset.isRequired,
        base: ChainTypes.ChainAsset.isRequired
    };

    constructor(props) {
        super(props);

        this._getChangeProps = this._getChangeProps.bind(this);
    }

    shouldComponentUpdate(np) {
        return this._getChangeProps(np);
    }

    componentDidMount() {
        setTimeout(()=>this._setInterval(), 0);
    }

    componentWillUnmount() {
        this._clearInterval();
    }

    componentDidUpdate(prevProps) {
        if (this._getChangeProps(prevProps)) {
            this._clearInterval();
            setTimeout(()=>this._setInterval(), 0);
        }
    }

    _getChangeProps(props){
        return !!Object.keys(diff(props, this.props)).length;
    }

    _setInterval(nextProps = null) {
        let {quote_asset, base_asset } = nextProps || this.props;
        this.statsInterval = MarketsActions.getMarketStatsInterval(
            35 * 1000,
            base_asset, quote_asset
        );
    }

    _clearInterval() {
        if (this.statsInterval) this.statsInterval();
    }



    render() {
        let { marketStats, quote_asset, base_asset  } = this.props;
        //console.log('Price.jsx this.props', this.props);
        return (
            <span>
                {marketStats && marketStats.price ?
                    utils.price_text( marketStats.price.toReal(), base_asset, quote_asset )
                    : "0.00"}
            </span>
        );
    }
}

//export default BindToChainState(MarketsCellPrice);
export default MarketsCellPrice;

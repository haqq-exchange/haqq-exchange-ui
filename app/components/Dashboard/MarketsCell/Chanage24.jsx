
import React from "react";
import ChainTypes from "Utility/ChainTypes";
// import AssetName from "Utility/AssetName";
// import BindToChainState from "Utility/BindToChainState";
// import MarketsActions from "actions/MarketsActions";
// import SettingsActions from "actions/SettingsActions";
// import {Link} from "react-router-dom";
// import utils from "common/utils";
// import cnames from "classnames";
import { diff } from "deep-object-diff";

class MarketsCell24Chanage extends React.Component {
    static propTypes = {
        quote: ChainTypes.ChainAsset.isRequired,
        base: ChainTypes.ChainAsset.isRequired
    };

    constructor(props) {
        super(props);

        this._getChangeProps = this._getChangeProps.bind(this);
    }

    shouldComponentUpdate(np) {
        return this._getChangeProps(np);
    }

    _getChangeProps(props){
        return !!Object.keys(diff(props, this.props)).length;
    }


    render() {
        let { marketStats} = this.props;
        return (
            <span>
                <b>{marketStats ? marketStats.change : "0.00"}%</b>
            </span>
        );
    }
}

export default MarketsCell24Chanage;
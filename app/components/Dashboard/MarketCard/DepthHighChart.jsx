import React from "react";
import ReactHighstock from "react-highcharts";
import utils from "common/utils";
import colors from "assets/colors";

class DepthHighChart extends React.Component {
    shouldComponentUpdate(nextProps) {
        let settleCheck = isNaN(nextProps.feedPrice)
            ? false
            : nextProps.feedPrice !== this.props.feedPrice;
        return (
            // didOrdersChange(nextProps.orders, this.props.orders) ||
            // didOrdersChange(nextProps.call_orders, this.props.call_orders) ||
            settleCheck ||
            // nextProps.feedPrice !== this.props.feedPrice ||
            nextProps.flatBids !== this.props.flatBids
            // nextProps.leftOrderBook !== this.props.leftOrderBook ||
            // nextProps.LCP !== this.props.LCP ||
            // nextProps.showCallLimit !== this.props.showCallLimit ||
            // nextProps.hasPrediction !== this.props.hasPrediction ||
            // nextProps.feedPrice !== this.props.feedPrice ||
            // nextProps.marketReady !== this.props.marketReady
        );
    }

    componentDidMount() {
        this.reflowChart(500);
    }

    reflowChart(timeout) {
        setTimeout(() => {
            if (this.depthChart) {
                this.depthChart.chart.reflow();
            }
        }, timeout);
    }

    _getThemeColors(props = this.props) {
        return colors[props.theme];
    }

    findNormalizationPower(power, array) {
        while (array[array.length - 1][0] * power < 1) {
            power *= 10;
        }
        return power;
    }

    normalizeValues(array, power) {
        if (array && array.length) {
            array.forEach(entry => {
                entry[0] *= power;
            });
        }
    }

    render() {
        let {
            flatBids,
            flatAsks,
            flatCalls,
            flatSettles,
            totalBids,
            totalAsks,
            base,
            quote,
            feedPrice
        } = this.props;

        const {
            primaryText,
            callColor,
            settleColor,
            settleFillColor,
            bidColor,
            bidFillColor,
            askColor,
            askFillColor,
            axisLineColor
        } = this._getThemeColors();

        let {name: baseSymbol, prefix: basePrefix} = utils.replaceName(base);
        let {name: quoteSymbol, prefix: quotePrefix} = utils.replaceName(quote);
        baseSymbol = (basePrefix || "") + baseSymbol;
        quoteSymbol = (quotePrefix || "") + quoteSymbol;

        /*
		* Because Highstock does not deal with X axis values below 1, we need to
		* normalize the values and use the normalizing factor when displaying the
		* values on the chart
		*/
        let power = 1;
        if (flatBids.length) {
            power = this.findNormalizationPower(power, flatBids);
        } else if (flatAsks.length) {
            power = this.findNormalizationPower(power, flatAsks);
        } else if (flatCalls && flatCalls.length) {
            power = this.findNormalizationPower(power, flatCalls);
        } else if (flatSettles && flatSettles.length) {
            power = this.findNormalizationPower(power, flatSettles);
        }

        // Add one more factor of 10 to make sure it's enough
        power *= 10;

        if (power !== 1) {
            this.normalizeValues(flatBids, power);
            this.normalizeValues(flatAsks, power);
            this.normalizeValues(flatCalls, power);
            this.normalizeValues(flatSettles, power);
        }

        let config = {
            chart: {
                type: "area",
                backgroundColor: "rgba(255, 0, 0, 0)",
                spacing: [0, 0, 5, 0],
                height: this.props.chartHeight || 50
            },
            title: {
                text: null
            },
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            rangeSelector: {
                enabled: false
            },
            navigator: {
                enabled: false
            },
            scrollbar: {
                enabled: false
            },
            dataGrouping: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            series: [],
            yAxis: {
                visible: false,
                crosshair: false,
            },
            xAxis: {
                crosshair: false,
                visible: false,
            },
            plotOptions: {
                visible: false,
                enabled: false,
                width: 0,
                area: {
                    animation: false,
                    marker: {
                        enabled: false
                    },
                    enableMouseTracking: false,
                },
                series: {
                    //fillOpacity: 0.3,
                }
            }
        };

        // Center the charts between bids and asks
        if (flatBids.length > 0 && flatAsks.length > 0) {
            let middleValue =
                (flatAsks[0][0] + flatBids[flatBids.length - 1][0]) / 2;

            config.xAxis.min = middleValue * 0.4;
            config.xAxis.max = middleValue * 1.6;
            if (config.xAxis.max < flatAsks[0][0]) {
                config.xAxis.max = flatAsks[0][0] * 1.5;
            }
            if (config.xAxis.min > flatBids[flatBids.length - 1][0]) {
                config.xAxis.min = flatBids[flatBids.length - 1][0] * 0.5;
            }
            let yMax = 0;
            flatBids.forEach(b => {
                if (b[0] >= config.xAxis.min) {
                    yMax = Math.max(b[1], yMax);
                }
            });
            flatAsks.forEach(a => {
                if (a[0] <= config.xAxis.max) {
                    yMax = Math.max(a[1], yMax);
                }
            });
            config.yAxis.max = yMax * 1.15;


        } else if (flatBids.length && !flatAsks.length) {
            config.xAxis.min = flatBids[flatBids.length - 1][0] * 0.4;
            config.xAxis.max = flatBids[flatBids.length - 1][0] * 1.6;
        } else if (flatAsks.length && !flatBids.length) {
            config.xAxis.min = 0;
            config.xAxis.max = flatAsks[0][0] * 2;
        }

        // console.log('flatBids', flatBids);
        // console.log('flatAsks', flatAsks);

        // Push asks and bids
        if (flatBids.length) {
            config.series.push({
                step: "right",
                name: `Bid ${quoteSymbol}`,
                data: flatBids,
                color: {
                    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                    stops: [
                        [0, bidColor],
                        [1, bidFillColor]
                    ]
                }

            });
        }

        // console.log('bidFillColor', bidFillColor, bidColor);
        // console.log('askFillColor', askFillColor, askColor);

        if (flatAsks.length) {
            config.series.push({
                step: "left",
                name: `Ask ${quoteSymbol}`,
                data: flatAsks,
                color: {
                    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                    stops: [
                        [0, askColor],
                        [1, askFillColor]
                    ]
                }
            });
        }

        // Add onClick event listener if defined
        if (this.props.onClick) {
            config.chart.events = {
                click: this.props.onClick.bind(this, power)
            };
        }

        try {
            return (<div className="market-cart-chart">
                {flatBids || flatAsks ? (
                    <ReactHighstock ref={(c) => {
                        this.depthChart = c;
                    }} config={config}/>
                ) : null}
            </div>);
        } catch (e) {
            console.log("e", e);
        }
    }

}

DepthHighChart.defaultProps = {
    flatBids: [],
    flatAsks: [],
    orders: {},
    noText: false,
    noFrame: true
};

DepthHighChart.propTypes = {
    // flat_bids: PropTypes.array.isRequired,
    // flat_asks: PropTypes.array.isRequired,
    // orders: PropTypes.object.isRequired
};

export default DepthHighChart;

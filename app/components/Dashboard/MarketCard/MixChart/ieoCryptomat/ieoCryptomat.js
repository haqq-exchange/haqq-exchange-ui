import React from "react";
import {Link} from "react-router-dom";
import cn from "classnames";
import Translate from "react-translate-component";
import "./ieoCryptomat.scss";

export default class IeoCryptomat extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showBackground: [],
        };
    }


    componentDidMount() {
        this.showBackground(0);
    }

    showBackground = (currentStep) => {
        let {showBackground} = this.state;
        const _this = this;
        if( currentStep < 3 ) {
            const nextStep = currentStep +1;
            showBackground.push("bg" + currentStep);
            _this.setState({showBackground}, () => {
                setTimeout(()=>{
                    _this.showBackground(nextStep);
                }, 1000);
            });
        }
    };


    render() {
        const {showBackground} = this.state;
        return (
            <div className={"chart-market-item"}>
                <Link to={"/ieo/cryptomat"} className={"ico-cryptomat"}>
                    <div className="ico-cryptomat-wrap ">
                        <span className={cn("ico-cryptomat-bg ico-cryptomat-bg2", {active: showBackground.indexOf("bg2") !== -1 })} />
                        <span className={cn("ico-cryptomat-bg ico-cryptomat-bg1", {active: showBackground.indexOf("bg1") !== -1 })}  />
                        <span className={cn("ico-cryptomat-bg ico-cryptomat-bg0", {active: showBackground.indexOf("bg0") !== -1 })}  />
                        <div className="ico-cryptomat-time">
                            <DownDate />
                        </div>
                    </div>
                </Link>
            </div>
        );
    }
}

class DownDate extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            downDate: {
                days: 0,
                hours: 0,
                minutes: 0,
                seconds: 0
            }
        };
    }


    componentDidMount() {
        this.countDownDate();
    }

    componentWillUnmount(){
        clearInterval(this.downDate);
    }

    countDownDate = () => {
        const _this = this;

        let downDate = {};
        // Set the date we're counting down to
        var countDownDate = new Date("Jul 11, 2019 23:59:00").getTime();

        // Update the count down every 1 second
        this.downDate = setInterval(function () {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            downDate = {
                days: days.toString().padStart(2, "0"),
                hours: hours.toString().padStart(2, "0"),
                minutes: minutes.toString().padStart(2, "0"),
                seconds: seconds.toString().padStart(2, "0"),
            };

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(_this.downDate);
                downDate = {
                    distance
                };
            }
            _this.setState({downDate});
        }, 1000);
    };
    render() {
        const {downDate} = this.state;
        //console.log("downDate", downDate);
        return (
            <div className={"ico-cryptomat-down-date"}>
                {downDate.days ? 
                    <div>
                        <span>{downDate.days}</span>
                        <Translate component={"small"} content={"ieo.dateTime.days"}/>
                    </div>
                    : null}
                {downDate.days > 0 ?
                    <div>
                        <span>{downDate.hours}</span>
                        <Translate component={"small"} content={"ieo.dateTime.hours"}/>
                    </div> : null}
                <div>
                    <span>{downDate.minutes}</span>
                    <Translate component={"small"} content={"ieo.dateTime.minutes"}/>
                </div>
                <div>
                    <span>{downDate.seconds}</span>
                    <Translate component={"small"} content={"ieo.dateTime.seconds"}/>
                </div>
            </div>
        );
    }
}


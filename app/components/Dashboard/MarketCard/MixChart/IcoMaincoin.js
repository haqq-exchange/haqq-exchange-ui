import React from "react";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";

export default class HolidayListing extends React.PureComponent {
    render(){
        const {settings} = this.props;
        let imageName = require("./maincoinlightTheme.png");
        if( settings.get("themes") === "darkTheme" ) {
            imageName = require("./maincoindarkTheme.png");
        }

        return(
            <Link to={"/market/maincoin_deex"} className={"chart-market-item ico-market-hld"}>
                <div className="chart-market-wrap-image">
                    <img style={{
                        maxWidth: "100%"
                    }} src={imageName} alt=""/>
                    <Translate content={"markets.token_sell"} />
                </div>
            </Link>
        );
    }
}
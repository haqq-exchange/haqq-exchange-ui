import React from "react";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";

export default class HolidayListing extends React.PureComponent {
    render(){
        const {settings} = this.props;
        let imageName = require("./SlicelightTheme.png");
        if( settings.get("themes") === "lightTheme" ) {
            imageName = require("./SlicedarkTheme.png");
        }
        return(
            <Link to={"/market/CRYPTONOMICA_DEEX"} className={"chart-market-item ico-market-hld"}>
                <div className="chart-market-wrap-image">
                    <img style={{
                        maxWidth: "100%"
                    }} src={imageName} alt=""/>
                    <Translate content={"markets.token_sell"} />
                </div>
            </Link>
        );
    }
}
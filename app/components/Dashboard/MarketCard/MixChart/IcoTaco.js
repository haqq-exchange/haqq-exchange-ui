import React from "react";
import {Link} from "react-router-dom";
import "./IcoMaincoin.scss";
import ImageLoad from "Utility/ImageLoad";

export default class IcoTaco extends React.PureComponent {
    render(){
        return(
            <Link to={"/ieo/teco"} className={"chart-market-item ico-market teco"}>
                <div className="chart-market-wrap ">
                    <div className="chart-market-header ico-market-header">
                        <span className={"ico-market-info-text"}>
                            <span>IEO: Finished</span>
                        </span>
                    </div>
                    <div className="chart-market-body">
                        <div className="ico-market-row">
                            <div className="ico-market-name">price:</div>
                            <div className="ico-market-value price">
                                0.28 $
                            </div>
                        </div>
                        <div className="ico-market-row">
                            <div className="ico-market-name">
                                supply:
                            </div>
                            <div className="ico-market-value">
                                20 000 000
                            </div>
                        </div>
                        <div className="ico-market-row">
                            <div className="ico-market-name">
                                end date:
                            </div>
                            <div className="ico-market-value">
                                01.06.2019, 00:00
                            </div>
                        </div>
                    </div>
                </div>
            </Link>
        );
    }
}
import React from "react";
import {Checkbox, Form} from "antd";
import counterpart from "counterpart";
import classNames from "classnames";
import Translate from "react-translate-component";

class AccountCheckboxUnderstand extends React.Component {

    validateChecked = (rule, value, callback) => {
        console.log("rule, value", rule, value);
        if( typeof value !== "undefined" && !value){
            callback(counterpart.translate("form.message.required"));
        }
        callback();
    };
    onChangeData = (event) => {
        const {validateFields} = this.props.form;
        validateFields(["understand"], (err, values) => {
            if(this.props.onChange) {
                this.props.onChange(event.target.checked);
            }
        });
    };

    render() {
        const {getFieldDecorator, getFieldError, isFieldTouched} = this.props.form;
        let {className} = this.props;
        if( !className ) className = "account-field";

        let hasError = getFieldError("understand") && isFieldTouched("understand");
        console.log("hasError", hasError);

        let class_name = classNames(className, {
            ["error"]: hasError
        });

        return (
            <div className={class_name}>
                <Form.Item {...this.props.getShowError("understand")}>
                    {getFieldDecorator("understand", {
                        valuePropName: "understand",
                        rules: [
                            {
                                required: true,
                                message: counterpart.translate("form.message.required"),
                            },

                            {
                                validator: this.validateChecked,
                            },
                        ],
                    })(
                        <Checkbox defaultChecked={false} onChange={(event)=>this.onChangeData(event)}>
                            <Translate content={["form", "message", "chbox_understand"]} />
                        </Checkbox>,
                    )}
                </Form.Item>
            </div>
        );
    }
}

// const AccountEmailCreate = Form.create()(AccountEmail);

export default AccountCheckboxUnderstand;

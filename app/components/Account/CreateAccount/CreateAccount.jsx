import React from "react";
import {connect} from "alt-react";
import classNames from "classnames";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import AccountNameInput from "../../Forms/AccountNameInput";
import PasswordInput from "../../Forms/PasswordInput";
import WalletDb from "stores/WalletDb";
import notify from "actions/NotificationActions";
import {Link, withRouter} from "react-router-dom";
import AccountSelect from "../../Forms/AccountSelect";
import WalletUnlockActions from "actions/WalletUnlockActions";
import TransactionConfirmStore from "stores/TransactionConfirmStore";
import LoadingIndicator from "../../LoadingIndicator";
import WalletActions from "actions/WalletActions";
import Translate from "react-translate-component";
import {ChainStore, FetchChain} from "deexjs";
import {BackupCreate} from "../../Wallet/Backup";
import utils from "common/utils";
import SettingsActions from "actions/SettingsActions";
import counterpart from "counterpart";
import WalletManagerStore from "stores/WalletManagerStore";

import "./createaccount.scss";
import {ymSend} from "lib/common/metrika";

class CreateAccount extends React.Component {
    constructor() {
        super();
        this.state = {
            validAccountName: false,
            accountName: "",
            validPassword: false,
            registrar_account: null,
            loading: false,
            hide_refcode: true,
            show_identicon: false,
            step: 1
        };
        this.onFinishConfirm = this.onFinishConfirm.bind(this);

        this.accountNameInput = null;
        console.log("CreateAccount");
    }

    componentDidMount() {
        SettingsActions.changeSetting({
            setting: "passwordLogin",
            value: "loginWallet"
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !utils.are_equal_shallow(nextState, this.state)
            || !utils.are_equal_shallow(nextProps, this.props);
    }

    isValid() {
        const {binWallet} = this.props;
        let firstAccount = AccountStore.getMyAccounts().length === 0;
        let valid = this.state.validAccountName;
        if (binWallet && !WalletDb.getWallet()) {
            valid = valid && this.state.validPassword;
        }
        if (!firstAccount) {
            valid = valid && this.state.registrar_account;
        }
        console.log("valid", valid, this.state);
        return valid;
    }

    onAccountNameChange(e) {
        const state = {};
        if (e.value) {
            state.accountName = e.value;
            state.validAccountName = e.valid;
        }
        if (!this.state.show_identicon) state.show_identicon = true;
        this.setState(state);
    }

    onPasswordChange(e) {
        this.setState({validPassword: e.valid});
    }

    onFinishConfirm(confirm_store_state) {
        if (
            confirm_store_state.included &&
            confirm_store_state.broadcasted_transaction
        ) {
            TransactionConfirmStore.unlisten(this.onFinishConfirm);
            TransactionConfirmStore.reset();

            FetchChain("getAccount", this.state.accountName, undefined, {
                [this.state.accountName]: true
            }).then(() => {
                console.log("onFinishConfirm");
                this.props.history.push(
                    "/wallet/backup/create?newAccount=true"
                );
            });
        }
    }

    createAccount(name) {
        let refcode = this.refcode ? this.refcode.value() : null;

        let referralAccount = AccountStore.getState().referralAccount;

        WalletUnlockActions.unlock()
            .then(() => {
                this.setState({loading: true});

                AccountActions.createAccount(
                    name,
                    this.state.registrar_account,
                    referralAccount || null,
                    0,
                    refcode
                )
                    .then(() => {
                        // User registering his own account
                        if (this.state.registrar_account) {
                            FetchChain("getAccount", name, undefined, {
                                [name]: true
                            }).then(() => {
                                this.setState({
                                    step: 2,
                                    loading: false
                                });
                            });
                            TransactionConfirmStore.listen(
                                this.onFinishConfirm
                            );
                        } else {
                            // Account registered by the faucet
                            FetchChain("getAccount", name, undefined, {
                                [name]: true
                            }).then(() => {
                                this.setState({
                                    step: 2,
                                    loading: false
                                });
                            });
                        }


                    })
                    .catch(error => {
                        console.log(
                            "ERROR AccountActions.createAccount",
                            error
                        );
                        let error_msg =
                            error.base &&
                            error.base.length &&
                            error.base.length > 0
                                ? error.base[0]
                                : "unknown error";
                        if (error.remote_ip) error_msg = error.remote_ip[0];
                        notify.addNotification({
                            message: `Failed to create account: ${name} - ${error_msg}`,
                            level: "error",
                            autoDismiss: 10
                        });
                        this.setState({loading: false});
                    });
            })
            .catch(() => {
                console.log('Catch');
            });
    }

    createWallet(password, wallet_name = "default") {
        return WalletActions.setWallet(
            wallet_name, //wallet name
            password
        )
            .then(() => {
                console.log(
                    "Congratulations, your wallet was successfully created."
                );
            })
            .catch(err => {
                console.log("CreateWallet failed:", err);
                notify.addNotification({
                    message: `Failed to create wallet: ${err}`,
                    level: "error",
                    autoDismiss: 10
                });
            });
    }

    onSubmit(e) {
        e.preventDefault();
        if (!this.isValid()) return;
        let {binWallet, current_wallet} = this.props;
        let hasWallet = current_wallet && binWallet;
        let account_name = this.accountNameInput.getValue();

        ymSend("reachGoal", "ONSUB_REG" , {
            type_reg: "WALLET"
        });
        if (hasWallet) {
            this.createAccount(account_name);
        } else {
            let password = this.password.value();
            this.createWallet(password, account_name).then(() =>
                this.createAccount(account_name)
            );
        }
    }

    onRegistrarAccountChange(registrar_account) {
        this.setState({registrar_account});
    }

    // showRefcodeInput(e) {
    //     e.preventDefault();
    //     this.setState({hide_refcode: false});
    // }

    _renderAccountCreateForm() {
        let {registrar_account, validPassword} = this.state;
        let {binWallet, current_wallet} = this.props;

        let my_accounts = AccountStore.getMyAccounts();
        (console.log("ОНО САМОЕ", my_accounts))
        let firstAccount = my_accounts.length === 0;
        let hasWallet = current_wallet && binWallet;
        let valid = this.isValid();
        let isLTM = false;
        let registrar = registrar_account
            ? ChainStore.getAccount(registrar_account)
            : null;
        if (registrar) {
            if (registrar.get("lifetime_referrer") == registrar.get("id")) {
                isLTM = true;
            }
        }

        let isDisabled = !valid || !validPassword;

        let buttonClass = classNames("btn btn-green", {
            disabled: isDisabled || (registrar_account && !isLTM)
        });

        console.log("valid validPassword", !validPassword, "valid", !valid, "isDisabled",  isDisabled, buttonClass);
        console.log("hasWallet", hasWallet);

        return (
            <form
                style={{maxWidth: "40rem"}}
                onSubmit={this.onSubmit.bind(this)}
                noValidate
            >
                <div className={"account-creation-title"}>
                    {firstAccount ? (
                        <Translate content="wallet.create_w_a" />
                    ) : (
                        <Translate content="wallet.create_a" />
                    )}
                </div>
                <AccountNameInput
                    ref={ref => {
                        if (ref) {
                            this.accountNameInput = ref.nameInput;
                        }
                    }}
                    cheapNameOnly={!!firstAccount}
                    onChange={(event)=>this.onAccountNameChange(event)}
                    accountShouldNotExist={true}
                    placeholder={counterpart.translate("wallet.account_public")}
                    noLabel
                />


                {/* Only ask for password if a wallet already exists */}
                {hasWallet ? null : (
                    <PasswordInput
                        ref={(c)=>{this.password = c;}}
                        confirmation={true}
                        onChange={this.onPasswordChange.bind(this)}
                        noLabel
                        checkStrength
                    />
                )}

                {/* If this is not the first account, show dropdown for fee payment account */}
                {firstAccount ? null : (
                    <div className="full-width-content form-group no-overflow">
                        <label>
                            <Translate content="account.pay_from" />
                        </label>
                        <AccountSelect
                            account_names={my_accounts}
                            onChange={this.onRegistrarAccountChange.bind(this)}
                        />
                        {registrar_account && !isLTM ? (
                            <div
                                style={{textAlign: "left"}}
                                className="facolor-error"
                            >
                                <Translate content="wallet.must_be_ltm" />
                            </div>
                        ) : null}
                    </div>
                )}

                {/* Submit button */}
                <div className={"public-buttons"}>
                    {this.state.loading ? (
                        <LoadingIndicator type="three-bounce" />
                    ) : (
                        <button disabled={isDisabled} className={buttonClass}>
                            <Translate content="account.create_account" />
                        </button>
                    )}
                </div>

                {/* Backup restore option */}
                <div style={{paddingTop: 30}}>
                    <Link className={"public-link"} style={{fontSize: 14}} to="/wallet/backup/restore">
                        <Translate content="wallet.restore" />
                    </Link>
                </div>

                {/* Skip to step 3 */}
                {!binWallet || firstAccount ? null : (
                    <div style={{paddingTop: 20}}>
                        <label>
                            <a
                                onClick={() => {
                                    this.setState({step: 3});
                                }}
                            >
                                <Translate content="wallet.go_get_started" />
                            </a>
                        </label>
                    </div>
                )}
            </form>
        );
    }

    _renderAccountCreateText() {
        let {binWallet, current_wallet} = this.props;
        let hasWallet = current_wallet && binWallet;
        let my_accounts = AccountStore.getMyAccounts();
        let firstAccount = my_accounts.length === 0;

        return (
            <div className="account-creation-info">
                <Translate component={"h4"} content="wallet.wallet_browser" />

                {!binWallet ? (
                    <Translate
                        style={{textAlign: "left"}}
                        component="p"
                        content={__SCROOGE_CHAIN__ ? "wallet.has_wallet_scrooge" : "wallet.has_wallet"} />
                ) : null}

                <Translate
                    style={{textAlign: "left"}}
                    component="p"
                    content="wallet.create_account_text"
                />

                {firstAccount ? (
                    <Translate
                        style={{textAlign: "left"}}
                        component="p"
                        content="wallet.first_account_paid"
                    />
                ) : (
                    <Translate
                        style={{textAlign: "left"}}
                        component="p"
                        content="wallet.not_first_account"
                    />
                )}

            </div>
        );
    }

    _renderBackup() {
        return (
            <div className="backup-submit">
                <p>
                    <Translate unsafe content="wallet.wallet_crucial" />
                </p>
                <div className="divider" />
                <BackupCreate noText downloadCb={this._onBackupDownload} />
            </div>
        );
    }

    _onBackupDownload = () => {
        this.setState({
            step: 3
        });
    };

    _renderBackupText() {
        return (
            <div>
                <p
                    style={{
                        fontWeight: "normal",
                        fontFamily: "Roboto-Medium, arial, sans-serif",
                        fontStyle: "normal"
                    }}
                >
                    <Translate content="footer.backup" />
                </p>
                <p>
                    <Translate content="wallet.wallet_move" unsafe />
                </p>
                <p className="txtlabel warning">
                    <Translate unsafe content="wallet.wallet_lose_warning" />
                </p>
            </div>
        );
    }

    _renderGetStarted() {
        return (
            <div>
                <table className="table">
                    <tbody>
                        <tr>
                            <td>
                                <Translate content="wallet.tips_dashboard" />:
                            </td>
                            <td>
                                <Link to="/">
                                    <Translate content="header.dashboard" />
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_account" />:
                            </td>
                            <td>
                                <Link
                                    to={`/account/${
                                        this.state.accountName
                                    }/overview`}
                                >
                                    <Translate content="wallet.link_account" />
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_deposit" />:
                            </td>
                            <td>
                                <Link to="/deposit-withdraw">
                                    <Translate content="wallet.link_deposit" />
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_transfer" />:
                            </td>
                            <td>
                                <Link to="/transfer">
                                    <Translate content="wallet.link_transfer" />
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_settings" />:
                            </td>
                            <td>
                                <Link to="/settings">
                                    <Translate content="header.settings" />
                                </Link>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }

    _renderGetStartedText() {
        return (
            <div>
                <p
                    style={{
                        fontWeight: "normal",
                        fontFamily: "Roboto-Medium, arial, sans-serif",
                        fontStyle: "normal"
                    }}
                >
                    <Translate content="wallet.congrat" />
                </p>

                <p>
                    <Translate content="wallet.tips_explore" />
                </p>

                <p>
                    <Translate content="wallet.tips_header" />
                </p>

                <p className="txtlabel warning">
                    <Translate content="wallet.tips_login" />
                </p>
            </div>
        );
    }

    render() {
        let {step} = this.state;

        console.log("step", step)

        return (
            <div className="sub-content">
                <div style={{maxWidth: "95vw"}}>

                    {step !== 1 ? <Translate component="p" content={"wallet.step_" + step} /> : null}

                    {step === 1
                        ? this._renderAccountCreateForm()
                        : step === 2
                            ? this._renderBackup()
                            : this._renderGetStarted()}
                </div>

                <div style={{maxWidth: "95vw", paddingTop: "2rem"}}>
                    {step === 1
                        ? this._renderAccountCreateText()
                        : step === 2
                            ? this._renderBackupText()
                            : this._renderGetStartedText()}
                </div>
                <button className="btn btn-gray" style={{width: 200}} onClick={()=>this.props.history.goBack()}>
                    <Translate content="wallet.back" />
                </button>
            </div>
        );
    }
}

const CreateAccountRouter = withRouter(CreateAccount);

export default connect(CreateAccountRouter, {
    listenTo() {
        return [WalletManagerStore];
    },
    getProps() {
        console.log("WalletManagerStore.getState()", WalletManagerStore.getState());
        return {
            binWallet: WalletManagerStore.getState().bin_wallet,
            current_wallet: WalletManagerStore.getState().current_wallet,
        };
    }
});

import React, {useState} from "react";
import {connect} from "alt-react";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";
import {FetchChain, key} from "deexjs";
import PropTypes from "prop-types";
import {Form} from "antd";
import translator from "counterpart";

import AccountActions from "actions/AccountActions";
import WalletDb from "stores/WalletDb";
import notify from "actions/NotificationActions";
import SettingsActions from "actions/SettingsActions";
import WalletActions from "actions/WalletActions";
import AccountStore from "stores/AccountStore";
import TransactionConfirmStore from "stores/TransactionConfirmStore";

import LoadingIndicator from "Components/LoadingIndicator";
import AccountEmail from "Components/Account/CreateAccount/AccountEmail";
import AccountName from "Components/Account/CreateAccount/AccountName";
import AccountPasswordGroups from "Components/Account/CreateAccount/AccountGroupPassword";
import AccountCheckboxUnderstand from "Components/Account/CreateAccount/AccountCheckboxUnderstand";
import RefUtils from "common/ref_utils";
import {ymSend} from "common/metrika";
import {getRequestAddress} from "api/apiConfig";


import TranslateWithLinks from "Utility/TranslateWithLinks";

import "./createaccount.scss";
import SettingsStore from "stores/SettingsStore";
import Recaptcha from "Components/Account/CreateAccount/Recaptcha";
import ReferralId from "./ReferralId";

class CreateAccountPassword extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor() {
        super();
        this.state = {
            accountName: "",
            registrar_account: null,
            loading: false,
            step: 1,
            generatedPassword: ""
        };
        //this.refutils = new RefUtils();
        this.onFinishConfirm = this.onFinishConfirm.bind(this);

        this.accountNameInput = null;
        this.refutils = new RefUtils({
            url: getRequestAddress("api")
        });
    }

    componentDidMount() {

        console.log("componentDidMount!!!!", )
        //ReactTooltip.rebuild();
        SettingsActions.changeSetting({
            setting: "passwordLogin",
            value: "login"
        });
        this.generatedPassword();
    }

    /*shouldComponentUpdate(nextProps, nextState) {
        return !utils.are_equal_shallow(nextState, this.state)
            || !utils.are_equal_shallow(nextProps, this.props);
    }*/


    onFinishConfirm(confirm_store_state) {
        if (
            confirm_store_state.included &&
            confirm_store_state.broadcasted_transaction
        ) {
            TransactionConfirmStore.unlisten(this.onFinishConfirm);
            TransactionConfirmStore.reset();

            FetchChain("getAccount", this.state.accountName, undefined, {
                [this.state.accountName]: true
            }).then(() => {
                this.props.router.push("/wallet/backup/create?newAccount=true");
            });
        }
    }

    generatedPassword() {
        this.setState({
            generatedPassword: ("P" + key.get_random_key().toWif()).substr(
                0,
                45
            )
        });
    }

    _unlockAccount(name, password) {
        // FIXME: зависание страницы окончания регистрации происходит при вызове этого action
        // Система считает что во время работы одного dispatch вызывается другой
        // После того как этот блок кода закомментирован, аккаунт всё равно разлочен (судя по иконке замка и тому,
        // что клиент себя ведёт так же как и при нормальном логине пользователя)

        // SettingsActions.changeSetting({
        //     setting: "passwordLogin",
        //     value: true
        // });

        WalletActions.setWallet(name, password)
            .then(()=>WalletDb.isLocked())
            .then(()=>{
                return WalletDb.validatePassword(password, true, name);
            }).then(()=>{
                AccountActions.setPasswordAccount(name);
            });
    }

    saveAccountData = (data) => {
        window.console.log("saveAccountData data", data);
        this.setState(data, () => {
            this.createAccount(data.accountName, data.generatedPassword, data);
            ymSend("reachGoal", "ONSUB_REG" , {
                type_reg: "PASSWORD"
            });
        });
    };

    regFailed = (data, cause) => {
        this.refutils.postTemplate("/verify/reg_failed", {
            code: data.code,
            account: data.accountName,
            system_id: refutils.getSystemId(),
            // system: __GBL_CHAIN__ || __GBLTN_CHAIN__ ? "gbl" : (__SCROOGE_CHAIN__ ? "scrooge" : (__DEEX_CHAIN__ ? "deex" : "bts")),
            cause
        }, "");
    };

    checkEmailAccount = (data) => {
        window.console.log("checkEmailAccount data", data);
        return new Promise((resolve, reject)=>{
            if(!data.code || !data.accountName) {
                reject({
                    error: "Not found code or name"
                });
            } else {

                this.refutils.postTemplate("/verify/check_code", {
                    code: data.code,
                    account: data.accountName,
                    system_id: this.refutils.getSystemId(),
                }, "").then(res => {

                    if( res.status === 200 ) {
                        resolve();
                    } else {
                        reject(res.content);
                    }
                });
            }
        });
    };

    createAccount(name, password, data) {
        let refcode = this.refs.refcode ? this.refs.refcode.value() : null;
        let referralAccount = AccountStore.getState().referralAccount || data.refId;
        this.setState({loading: true});

        //debugger;
        console.log("data", data);

        AccountActions.createAccountWithPassword(
            name,
            password,
            this.state.registrar_account,
            referralAccount || this.state.registrar_account,
            0,
            refcode,
            {
                code: data.code,
                email: data.email
            }
        )
            .then(result => {
                console.log("CreateAccauntPassword, still works", result);

                // Account registered by the faucet
                FetchChain("getAccount", name).then(() => {
                    this.setState({
                        step: 2
                    });
                    this._unlockAccount(name, password);
                }).catch(()=>{
                    notify.addNotification({
                        message: "Failed check account",
                        level: "error",
                        autoDismiss: 10
                    });
                });
                // User registering his own account
                if (this.state.registrar_account) {
                    TransactionConfirmStore.listen(this.onFinishConfirm);
                }
            })
            .catch(error => {
                console.log("ERROR AccountActions.createAccount", error);
                const errorrCodeToCode = {
                    "0": "ERROR_UNKNOWN_ERROR",
                    "5": "ERROR_NOT_ALL_REQUIREMRNT_FIELDS",
                    "6": "ERROR_INCORRECT_LOGIN",
                    "7": "ERROR_DISABLED_SERVICE_PREFIX_FROM_LOGIN",
                    "8": "ERROR_ACCOUNT_EXISTS_IN_BLOCKCHAIN",
                    "20": "ERROR_NO_MONEY",
                    "10": "ERROR_ROBOT_DETECTED",
                    "11": "ERROR_ALL_REGISTRATION_ATTEMPTS_EXCEDED",
                    "12": "ERROR_INCORRECT_REFERRAL",
                    "13": "ERROR_REFERRAL_EXPIRED",
                    "14": "ERROR_REFERRAL_ACCOUNT_EXISTS",
                };

                let error_msg = error?.remote_ip?.[0] || error?.base?.[0] || error?.errorCode || "unknown error";

                if( error_msg === 10 ) {
                    window.location.reload();
                }

                if ( errorrCodeToCode[error_msg] ) {
                    error_msg = translator(["form","errors", errorrCodeToCode[error_msg]]);
                } else {
                    error_msg = `Failed to create account: ${name} - ${error_msg}`;
                }

                notify.addNotification({
                    message: `Failed to create account: ${name} - ${error_msg}`,
                    level: "error",
                    autoDismiss: 10
                });
                this.setState({loading: false});
                this.regFailed(data, error_msg);
                AccountStore.removeCurrentAccount();

            });
    }

    wrapChangeState = data => this.setState(data);


    render() {
        let {step, loading, resendEmail} = this.state;

        if( step === 1 ) {
            return <WrappedRegistrationForm
                loading={loading}
                locale={this.props.locale}
                email_code={this.props.email_code}
                referralState={this.props.referralState}
                resendEmail={resendEmail}
                wrapChangeState={this.wrapChangeState}
                createAccount={this.saveAccountData} />;
        } else {
            return <ShowBackap {...this.state} />;
        }
    }
}

const ShowBackap = props => {
    const [hasShow, setShowPassword] = useState(true);

    return (
        <div className="backup-submit">
            <Translate component={"p"} content={"wallet.step_2"}/>
            <Translate unsafe component={"p"} content="wallet.password_crucial"/>

            <div>
                {!hasShow ? (
                    <div
                        onClick={() => setShowPassword(true)}
                        className="button"
                    >
                        <Translate content="wallet.password_show"/>
                    </div>
                ) : (
                    <div>
                        <div>
                            <h5>
                                <Translate content="wallet.account_public"/>:
                            </h5>

                            <input type="text" readOnly={true} defaultValue={props.accountName} style={{
                                border: "none",
                                textAlign: "center",
                                cursor: "text"
                            }}/>
                        </div>
                        <div>
                            <h5>
                                <Translate content="settings.password"/>:
                            </h5>

                            <input type="text" readOnly={true} defaultValue={props.generatedPassword} style={{
                                border: "none",
                                textAlign: "center",
                                cursor: "text"
                            }}/>
                        </div>
                    </div>
                )}
            </div>
            <div className="divider"/>
            <p className="txtlabel warning">
                <Translate unsafe content="wallet.password_lose_warning"/>
            </p>

            <div >
                 {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?
                 <Link to={`/account/${props.accountName}`} className={"btn btn-green"}>
                    <Translate content="wallet.ok_done"/>
                </Link>:
                <Link to={"/"} className={"btn btn-green"}>
                    <Translate content="wallet.ok_done"/>
                </Link>}
            </div>
        </div>
    );
};

class CreateAccount extends React.Component{

    state = {
        showRecaptcha: false,
        cap: "",
        sendConfirmEmail: false,
        refId: null,
        sendConfirmReferral: false
    };

    componentDidMount() {
        const defRefVal = this.props.referralState?.query;
        defRefVal && this.props.form.setFieldsValue({referral: defRefVal});
        this.props.form.validateFields();
    }

    handleSubmit = (event) => {
        event.preventDefault();

        this.props.form.validateFields((err) => {
            if (err) {
                return false;
            }
            this.props.wrapChangeState({resendEmail: false});
            this.props.createAccount(this.state);
        });
    };
    setAccountData = (name, value) => {
        window.console.log("setAccountData name, value", name, value);
        if( typeof name === "string" && typeof value !== "object" ) {
            this.setState({
                [name]: value
            });
        } else {
            this.setState(value);
        }
        console.log("setAccountData", name, value);
    };

    hasErrors = (fieldsError) => {
        return Object.keys(fieldsError).some(field => fieldsError[field]);
    };

    getShowError = (name) => {
        const {isFieldTouched, getFieldError}= this.props.form;
        const hasValueError = name === "referral" && this.state.sendConfirmReferral ? getFieldError(name) : isFieldTouched(name) && getFieldError(name);
        // const hasValueError = isFieldTouched(name) && getFieldError(name);
        return {
            validateStatus: hasValueError ? "error" : "",
            help: hasValueError || ""
        };
    };

    setConfirmEmail = () => {
        const {getFieldValue} = this.props.form;
        this.setState({
            sendConfirmEmail: {
                [getFieldValue("email")]: true
            }
        });
    }

    setRefId = () => {
        const {getFieldValue} = this.props.form;
        this.setState({
            refId: getFieldValue("referral")
        });
    }

    setConfirmReferral = () => {
        this.setState({sendConfirmReferral: true});
    }

    resetConfirmEmail = () => {
        this.setState({
            sendConfirmEmail: false,
        });
    }

    render() {
        const {showRecaptcha} = this.state;
        const {loading} = this.props;
        const {getFieldsError} = this.props.form;

        return (
            <div className={"create-account-inner"}>
                <Form onSubmit={this.handleSubmit}>

                    <Translate content={"form.all_required"} className={"create-account-sub-title"} />

                    <AccountName
                        getShowError={this.getShowError}
                        onChange={(data)=>this.setAccountData("accountName", data)} {...this.props} />

                    <ReferralId
                        getShowError={this.getShowError}
                        onChange={(data)=>this.setAccountData("referral", data)} {...this.props} 
                        sendConfirmEmail={this.state.sendConfirmEmail} />

                    <AccountEmail
                        getShowError={this.getShowError}
                        onChange={(data)=>this.setAccountData("email", data)} {...this.props}
                        sendConfirmEmail={this.state.sendConfirmEmail} sendConfirmReferral={this.state.sendConfirmReferral}
                        setConfirmEmail={this.setConfirmEmail} setConfirmReferral={this.setConfirmReferral}
                        setRefId={this.setRefId} resetConfirmEmail={this.resetConfirmEmail} />

                    <AccountPasswordGroups
                        getShowError={this.getShowError}
                        hasReadOnly={false}
                        onChange={(data)=>this.setAccountData("generatedPassword", data)} {...this.props} />

                    <AccountCheckboxUnderstand
                        getShowError={this.getShowError}
                        onChange={(data)=>this.setAccountData("undestand", data)} {...this.props} />

                    <Form.Item className={"create-account-btn"}>
                        {loading ?
                            <LoadingIndicator type="three-bounce"/> :
                            <Translate
                                className={"btn btn-green"}
                                disabled={this.hasErrors(getFieldsError())}
                                component={"button"} type={"submit"} content="account.create_account"/>
                        }
                    </Form.Item>
                </Form>

                <div className="additional-account-options">
                    <TranslateWithLinks
                        string="account.optional.formatter"
                        keys={[
                            {
                                type: "link",
                                className: "public-link",
                                value: "/wallet/backup/restore",
                                translation:
                                    "account.optional.restore_link",
                                dataIntro: translator.translate(
                                    "walkthrough.restore_account"
                                ),
                                arg: "restore_link"
                            },
                            // {
                            //     type: "link",
                            //     className: "public-link",
                            //     value: "/create-account/wallet",
                            //     translation:
                            //         "account.optional.restore_form",
                            //     dataIntro: translator.translate(
                            //         "walkthrough.create_local_wallet"
                            //     ),
                            //     arg: "restore_form"
                            // }
                        ]}
                    />
                </div>
            </div>
        );
    }

}

const WrappedRegistrationForm = Form.create({ name: "register" })(CreateAccount);

export default connect(
    CreateAccountPassword,
    {
        listenTo() {
            return [SettingsStore];
        },
        getProps() {
            console.log("SettingsStore.getState()", SettingsStore.getState().viewSettings)
            return {
                locale: SettingsStore.getState().settings.get("locale"),
                email_code: SettingsStore.getState().viewSettings.get("email_code")
            };
        }
    }
);

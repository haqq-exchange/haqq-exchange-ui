import React from "react";
import {Form, Input } from "antd";
import {getRequestAddress} from "api/apiConfig";
import Translate from "react-translate-component";
import counterpart from "counterpart";
import cln from "classnames";
import cookies from "cookies-js";
import RefUtils from "../../../lib/common/ref_utils";
// import {debounce} from "lodash-es";

class AccountEmail extends React.Component {

    constructor(props) {
        super(props);
        //this.onChangeEmail      = debounce(this.onChangeEmail.bind(this), 250);
        this.state = {
            // sendConfirmEmail: false,
            checkConfirmEmail: false
        };
    }


    componentDidUpdate(prevProps) {
        console.log("prevProps", prevProps.resendEmail , this.props.resendEmail);
        if( prevProps.resendEmail !== this.props.resendEmail && this.props.resendEmail) {
            this.resetStateConfirm();
        }
    }

    _sendConfirmEmail = () => {
        const {locale} = this.props;
        const {getFieldValue, setFields} = this.props.form;

        this.props.setConfirmReferral();
        if (!getFieldValue("referral")) {
            setFields({
                referral: {
                    errors: [counterpart.translate("form.message.required")],
                }
            });
            return;
        }

        let hasDebug = cookies.get("config_c6930e40");

        const refutils = new RefUtils({
            url: getRequestAddress("api")
        });
        
        let sendConfirm = {
            email: getFieldValue("email"),
            lang: locale,
            link: refutils.defaultUrl,
            referral_id: getFieldValue("referral"),
            system_id: refutils.getSystemId()
            // system_id: __GBL_CHAIN__ || __GBLTN_CHAIN__ ? "gbl" : (__SCROOGE_CHAIN__ ? "scrooge" : "haqq")
        };

        if(hasDebug || __DEV__) {
            sendConfirm.debug = true;
        }

        refutils.postTemplate("/verify/initiate", sendConfirm, "")
            .then(res => {
                console.log('res', res);
                if( [200, 421].includes(res.status) ) {
                    // this.setState({
                    //     sendConfirmEmail: {
                    //         [sendConfirm.email]: true
                    //     }
                    // });
                    this.props.setRefId();
                    this.props.setConfirmEmail();
                    setFields({referral: {value: getFieldValue("referral"), errors: undefined}});
                } else {
                    setFields({
                        referral: {
                            value: getFieldValue("referral"),
                            errors: [counterpart.translate("form.message.not_referral_id")],
                        }
                    });
                }
                
            });
    };


    resetStateConfirm = () => {
        this.props.resetConfirmEmail();
        this.setState({
            // sendConfirmEmail: false,
            checkConfirmEmail: false
        })
    };

    onChangeEmailCode = (event) => {
        console.log("event", event.target.value);
        const {validateFields} = this.props.form;
        validateFields(["email", "code"], (errors, values) => {
            window.console.log("errors, values", errors, values);
            if(!errors) {
                window.console.log("this.props.onChange", this.props.onChange, values );
                if(this.props.onChange) {
                    this.props.onChange(values);
                    //this.setState({checkConfirmEmail: true});
                }
            }
        });
    };

    onPasteFields = (event, name) => {
        const value = event.clipboardData.getData("Text");
        /*this.props.form.setFieldsValue({
            [name]: value
        });*/
    };

    onChangeEmail = (event, type) => {
        console.log("event", event.target.value, type);
        const {validateFields} = this.props.form;
        validateFields(["email"], (err, values) => {

            const _email = !err ? values.email : false;
            window.console.log("this.props.onChange", err, values, _email);

            if(this.props.onChange) {
                this.props.onChange(_email || "");
            }
        });
    };


    render() {
        let {sendConfirmEmail} = this.props;
        let {checkConfirmEmail} = this.state;
        const {getFieldDecorator, getFieldError, isFieldTouched, getFieldValue} = this.props.form;
        let {className, notConfirm} = this.props;
        if( !className ) className = "account-field";

        let hasError = getFieldError("email");
        let emailError = isFieldTouched("email") && hasError;

        let class_name = cln(className, {
            ["error"]: emailError
        });

        let hasShowConfirmEmail = sendConfirmEmail && !checkConfirmEmail;

        return (
            <div className={class_name}>
                <Form.Item  label={counterpart.translate("form.label.email")} {...this.props.getShowError("email")}>
                    {getFieldDecorator("email", {
                        validateTrigger: ["onBlur", "onChange"],
                        normalize: value => value && value.trim(),
                        rules: [
                            {
                                type: "email",
                                message: counterpart.translate("form.message.type_email"),
                            },
                            {
                                required: true,
                                message: counterpart.translate("form.message.required"),
                            }
                        ],
                    })(notConfirm ?
                        <Input
                            autoComplete={"new-email-fields"}
                            autoFill={"off"}
                            onChange={(event) => this.onChangeEmail(event)}
                            onBlur={(event) => this.onChangeEmail(event)}
                        /> :
                        <Input
                            readOnly={checkConfirmEmail}
                            onChange={this.onChangeEmail}
                            onBlur={this.onChangeEmail}
                            className="input-email"
                            addonAfter={<button
                                type={"button"}
                                onClick={(event) => this._sendConfirmEmail(event)}
                                className={"btn btn-green"}
                                disabled={hasError || sendConfirmEmail?.[getFieldValue("email")]}><Translate content={"global.confirm"}/></button>}
                        />)}
                </Form.Item>
                {!notConfirm ? <ConfirmEmail
                    hasShowConfirmEmail={hasShowConfirmEmail}
                    checkConfirmEmail={this.onChangeEmailCode}
                    onPasteFields={this.onPasteFields}
                    {...this.props} /> : null}
            </div>
        );
    }
}

const ConfirmEmail = props => {
    const {getFieldDecorator} = props.form;

    console.log("props", props);

    const checkValidate = (rule, values, callback) => {
        try {
            if( values.length === props.email_code ) {
                callback();
            } else {
                callback("error values length");
            }
        } catch (e) {
            callback();
        }
    };

    return (
        <Form.Item label={counterpart.translate("form.label.confirm_email")} className={cln({
            "hide": !props.hasShowConfirmEmail
        })} >
            {getFieldDecorator("code", {
                validateTrigger: ["onBlur", "onChange"],
                rules: [{
                    validator: checkValidate
                },{
                    required: true,
                    message: counterpart.translate("form.message.required"),
                }]
            })(<Input
                autoComplete="dex-code"
                onChange={(event)=>props.checkConfirmEmail(event)}
                onBlur={(event)=>props.checkConfirmEmail(event)}
            />)}
        </Form.Item>
    );
};


// const AccountEmailCreate = Form.create()(AccountEmail);

export default AccountEmail;

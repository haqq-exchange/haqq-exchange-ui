import React from "react";
import PropTypes from "prop-types";
import Translate from "react-translate-component";
import DefaultModal from "components/Modal/DefaultModal";
import "./infoModal.scss";


import ls from "common/localStorage";
import cname from "classnames";
import configConst from "config/const";
let accountStorage = new ls(configConst.STORAGE_KEY);


class InfoModal extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    static defaultProps = {
        modalId: "info_modal",
        registrationModels: {
            "password": {
                "title": "public.InfoRegistration.models.password.title",
                "benefit": [
                    "public.InfoRegistration.models.password.benefit0",
                    "public.InfoRegistration.models.password.benefit1"
                ]
            },
            /*"wallet": {
                "title": "public.InfoRegistration.models.wallet.title",
                "benefit": [
                    "public.InfoRegistration.models.wallet.benefit0",
                    "public.InfoRegistration.models.wallet.benefit1",
                    "public.InfoRegistration.models.wallet.benefit2"
                ]
            },*/
            "password-2fa": {
                "title": "public.InfoRegistration.models.password-2fa.title",
                "benefit": [
                    "public.InfoRegistration.models.password-2fa.benefit0",
                    "public.InfoRegistration.models.password-2fa.benefit1"
                ]
            }
        },
        scroogeModels: {
            "password": {
                "title": "public.InfoRegistration.models.password.title",
                "benefit": [
                    "public.InfoRegistration.models.password.benefit0",
                    "public.InfoRegistration.models.password.benefit2"
                ]
            }
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            iSee: false
        };
    }

    closeModal = ( isReg ) => {
        const {resolve} = this.props;
        if (resolve) resolve(this.state.iSee && isReg);

    };


    render() {
        const {iSee} = this.state;
        const {modalId, isOpenModal, registrationModels, scroogeModels} = this.props;
        const models = __SCROOGE_CHAIN__ ? scroogeModels : registrationModels;

        return (
            <DefaultModal
                id={modalId}
                ref={(ref) => this.InfoModal = ref}
                isOpen={isOpenModal}
                className="InfoModal"
                onAfterOpen={this.afterOpenModal}
                onRequestClose={this.closeModal}>
                <div className="InfoModal-wrap">
                    <div className="InfoModal-title">
                        {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?
                        <Translate unsafe content={"public.InfoRegistration.title_gbl"}/>:
                        <Translate unsafe content={__SCROOGE_CHAIN__ ? "public.InfoRegistration.title_scrooge" : "public.InfoRegistration.title_haqq"}/>}
                    </div>
                    <div className="InfoModal-desc">
                        <Translate unsafe content={"public.InfoRegistration.desc"}/>
                    </div>
                    <div className="InfoModal-info">
                        {Object.keys(models).map(slug=> {
                            const model = models[slug];
                            return (
                                <div key={"InfoModal-item-" + slug} onClick={()=>this.setState({slug})} className={cname("InfoModal-item")}>
                                    <Translate
                                        className={cname("InfoModal-item-title", slug)}
                                        component={"span"}
                                        content={model.title} />
                                    <ul className={"InfoModal-item-ul"}>
                                        {model.benefit.map((benefit, index)=><li key={"InfoModal-item-li-"+index} className={"InfoModal-item-li"}><Translate content={benefit} /></li>)}
                                    </ul>
                                </div>
                            );
                        })}
                    </div>
                    <div className="InfoModal-btn">
                        <label className={"InfoModal-btn-label"}>
                            <input type="checkbox" onChange={()=>this.setState({iSee: true})}/>
                            <Translate content={"public.InfoRegistration.see"}/>
                        </label>

                        <div className="InfoModal-btn-group">
                            <button type={"button"} disabled={!iSee} onClick={()=>this.closeModal(true)} className={"btn btn-green"}>
                                <Translate content={"public.InfoRegistration.registration"}/>
                            </button>
                            <button type={"button"} className={"btn btn-gray"}  onClick={()=>this.closeModal(false)}>
                                <Translate content={"public.InfoRegistration.other"}/>
                            </button>
                        </div>

                    </div>
                </div>
            </DefaultModal>
        );
    }
}


export default InfoModal;

import React from "react";
import {connect} from "alt-react";
import classNames from "classnames";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import AccountNameInput from "../../Forms/AccountNameInput";
import WalletDb from "stores/WalletDb";
import notify from "actions/NotificationActions";
import {Link} from "react-router-dom";
// import AccountSelect from "../../Forms/AccountSelect";
import TransactionConfirmStore from "stores/TransactionConfirmStore";
import LoadingIndicator from "../../LoadingIndicator";
import AccountEmail from "Components/Account/CreateAccount/AccountEmail";

import Translate from "react-translate-component";
import counterpart from "counterpart";
const translator = require("counterpart");

import {ChainStore, FetchChain, key} from "deexjs";
// import ReactTooltip from "react-tooltip";
import utils from "common/utils";
import SettingsActions from "actions/SettingsActions";
// import WalletUnlockActions from "actions/WalletUnlockActions";
import Icon from "../../Icon/Icon";
import CopyButton from "../../Utility/CopyButton";
import PropTypes from "prop-types";

import TranslateWithLinks from "../../Utility/TranslateWithLinks";


import "./createaccount.scss";
import RefUtils from "common/ref_utils";
import WalletActions from "../../../actions/WalletActions";
import {ymSend} from "lib/common/metrika";
import {getRequestAddress} from "../../../api/apiConfig";

class CreateAccountPassword extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor() {
        super();
        this.state = {
            validAccountName: false,
            accountName: "",
            validPassword: false,
            registrar_account: null,
            loading: false,
            hide_refcode: true,
            show_identicon: false,
            step: 1,
            showPass: false,
            generatedPassword: ("P" + key.get_random_key().toWif()).substr(
                0,
                45
            ),
            confirm_password: "",
            chbox_understand: false
        };
        this.refutils = new RefUtils();
        this.onFinishConfirm = this.onFinishConfirm.bind(this);

        this.accountNameInput = null;
    }

    componentDidMount() {
        //ReactTooltip.rebuild();
        SettingsActions.changeSetting({
            setting: "passwordLogin",
            value: "login"
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !utils.are_equal_shallow(nextState, this.state)
            || !utils.are_equal_shallow(nextProps, this.props);
    }

    isValid() {
        const {chbox_understand, validAccountName, validPassword, email} = this.state;
        let firstAccount = AccountStore.getMyAccounts().length === 0;
        let valid = validAccountName;
        if (!WalletDb.getWallet()) {
            valid = valid && validPassword;
        }
        if (!firstAccount) {
            valid = valid && this.state.registrar_account;
        }
        return valid && chbox_understand && email;
    }

    onAccountNameChange(e) {
        const state = {};
        if (e.valid !== undefined) state.validAccountName = e.valid;
        if (e.value !== undefined) state.accountName = e.value;
        if (!this.state.show_identicon) state.show_identicon = true;
        this.setState(state);
    }

    onFinishConfirm(confirm_store_state) {
        if (
            confirm_store_state.included &&
            confirm_store_state.broadcasted_transaction
        ) {
            TransactionConfirmStore.unlisten(this.onFinishConfirm);
            TransactionConfirmStore.reset();

            FetchChain("getAccount", this.state.accountName, undefined, {
                [this.state.accountName]: true
            }).then(() => {
                this.props.router.push("/wallet/backup/create?newAccount=true");
            });
        }
    }

    _unlockAccount(name, password) {
        // FIXME: зависание страницы окончания регистрации происходит при вызове этого action
        // Система считает что во время работы одного dispatch вызывается другой
        // После того как этот блок кода закомментирован, аккаунт всё равно разлочен (судя по иконке замка и тому,
        // что клиент себя ведёт так же как и при нормальном логине пользователя)

        // SettingsActions.changeSetting({
        //     setting: "passwordLogin",
        //     value: true
        // });

        WalletActions.setWallet(name, password)
            .then(()=>WalletDb.isLocked())
            .then(()=>{
                return WalletDb.validatePassword(password, true, name);
            }).then(()=>{
                AccountActions.setPasswordAccount(name);
            });
    }

    _sendSaveEmail = (account) => {
        const {email} = this.state;
        const refutils = new RefUtils({
            url: getRequestAddress("api")
        });

        refutils.postTemplate("/verify/store_email", {
            email,
            account,
            link: ""
        }, "").then(res => {
            console.log('res', res);
        });

    };

    createAccount(name, password) {
        let refcode = this.refs.refcode ? this.refs.refcode.value() : null;
        let referralAccount = AccountStore.getState().referralAccount;
        this.setState({loading: true});

        console.log("CreateAccountPassword starts create new account");
        AccountActions.createAccountWithPassword(
            name,
            password,
            this.state.registrar_account,
            referralAccount || this.state.registrar_account,
            0,
            refcode
        )
            .then(() => {
                console.log("CreateAccauntPassword, still works");
                // User registering his own account
                if (this.state.registrar_account) {
                    FetchChain("getAccount", name, undefined, {
                        [name]: true
                    }).then(() => {
                        this.setState({
                            step: 2,
                            loading: false
                        });
                        this._unlockAccount(name, password);
                    });
                    TransactionConfirmStore.listen(this.onFinishConfirm);
                } else {
                    // Account registered by the faucet
                    FetchChain("getAccount", name, undefined, {
                        [name]: true
                    }).then(() => {
                        this.setState({
                            step: 2
                        });
                        this._unlockAccount(name, password);
                    });
                }
                this._sendSaveEmail(name);
            })
            .catch(error => {
                console.log("ERROR AccountActions.createAccount", error);
                let error_msg =
                    error.base && error.base.length && error.base.length > 0
                        ? error.base[0]
                        : "unknown error";
                if (error.remote_ip) error_msg = error.remote_ip[0];
                notify.addNotification({
                    message: `Failed to create account: ${name} - ${error_msg}`,
                    level: "error",
                    autoDismiss: 10
                });
                this.setState({loading: false});
            });
    }

    onSubmit(e) {
        e.preventDefault();
        if (!this.isValid()) return;
        let account_name = this.accountNameInput.getValue();
        // if (WalletDb.getWallet()) {
        //     this.createAccount(account_name);
        // } else {
        let password = this.state.generatedPassword;
        ymSend("reachGoal", "ONSUB_REG" , {
            type_reg: "PASSWORD"
        });
        this.createAccount(account_name, password);
    }

    setAccountEmail(email) {
        this.setState({email});
    }

    // showRefcodeInput(e) {
    //     e.preventDefault();
    //     this.setState({hide_refcode: false});
    // }

    _onInput(value, e) {
        this.setState({
            [value]:
                value === "confirm_password"
                    ? e.target.value
                    : !this.state[value],
            validPassword:
                value === "confirm_password"
                    ? e.target.value === this.state.generatedPassword
                    : this.state.validPassword
        });
    }

    _renderAccountCreateForm() {
        let {registrar_account, confirm_password, generatedPassword} = this.state;

        let my_accounts = AccountStore.getMyAccounts();
        let firstAccount = my_accounts.length === 0;
        let valid = this.isValid();
        let isLTM = false;
        let registrar = registrar_account
            ? ChainStore.getAccount(registrar_account)
            : null;
        if (registrar) {
            if (registrar.get("lifetime_referrer") === registrar.get("id")) {
                isLTM = true;
            }
        }

        let hasConfirmPass = confirm_password && confirm_password === generatedPassword;

        let buttonClass = classNames("btn btn-green", {
            disabled: !valid || (registrar_account && !isLTM)
        });

        let confirmClass = classNames("confirm-password", {
            error: !hasConfirmPass
        });

        return (
            <div className={"create-account-inner"}>
                <form
                    className={"create-account-inner-form"}
                    onSubmit={this.onSubmit.bind(this)}
                    noValidate
                >
                    <Translate content={"form.all_required"} className={"create-account-sub-title"} />


                    <AccountNameInput
                        ref={ref => {
                            if (ref) {
                                this.accountNameInput = ref.nameInput;
                            }
                        }}
                        cheapNameOnly={!!firstAccount}
                        onChange={this.onAccountNameChange.bind(this)}
                        accountShouldNotExist={true}
                        placeholder={counterpart.translate(
                            "wallet.account_public"
                        )}
                        noLabel
                    />

                    <AccountEmail onChange={(data)=>this.setAccountEmail(data)} />

                    <section className="form-group">
                        <label className="left-label">
                            <Translate content="wallet.generated"/>
                            &nbsp;&nbsp;
                            <span
                                className="tooltip"
                                data-html={true}
                                data-tip={counterpart.translate(
                                    "tooltip.generate"
                                )}
                            >
                                <Icon
                                    name="question-circle"
                                    title="icons.question_circle"
                                />
                            </span>
                        </label>
                        <div className={"input-button-password-wrap"}>
                            <span className="inline-label">
                                <input
                                    readOnly
                                    value={this.state.generatedPassword}
                                    type="text"
                                    className="input-button-password"
                                />
                                <CopyButton
                                    text={this.state.generatedPassword}
                                    tip="tooltip.copy_password"
                                    dataPlace="top"
                                />
                            </span>
                        </div>
                    </section>

                    <section className={confirmClass}>
                        <label className="left-label">
                            <Translate content="wallet.confirm_password"/>
                        </label>
                        <input
                            type="password"
                            name="password"
                            id="password"
                            className="input-button-password"
                            value={this.state.confirm_password}
                            onChange={this._onInput.bind(
                                this,
                                "confirm_password"
                            )}
                        />
                        {!hasConfirmPass ? (
                            <div className="confirm-password-error">
                                <Translate content="wallet.confirm_error"/>
                            </div>
                        ) : null}
                    </section>

                    <br/>

                    <div className="confirm-checks">
                        <label>
                            <input
                                type="checkbox"
                                onChange={this._onInput.bind(
                                    this,
                                    "chbox_understand"
                                )}
                                defaultChecked={this.state.chbox_understand}
                            />
                            <Translate content={["form", "message", "chbox_understand"]} />
                        </label>
                    </div>


                    {/* Submit button */}
                    <div className={"public-buttons"}>
                        {this.state.loading
                            ? (
                                <LoadingIndicator type="three-bounce"/>
                            ) : (
                                <button className={buttonClass}>
                                    <Translate content="account.create_account"/>
                                </button>
                            )
                        }
                    </div>
                </form>

                <div className="additional-account-options">
                    <TranslateWithLinks
                        string="account.optional.formatter"
                        keys={[
                            {
                                type: "link",
                                className: "public-link",
                                value: "/wallet/backup/restore",
                                translation:
                                    "account.optional.restore_link",
                                dataIntro: translator.translate(
                                    "walkthrough.restore_account"
                                ),
                                arg: "restore_link"
                            },
                            {
                                type: "link",
                                className: "public-link",
                                value: "/create-account/wallet",
                                translation:
                                    "account.optional.restore_form",
                                dataIntro: translator.translate(
                                    "walkthrough.create_local_wallet"
                                ),
                                arg: "restore_form"
                            }
                        ]}
                    />
                </div>
            </div>
        );
    }

    _renderAccountCreateText__1() {
        let my_accounts = AccountStore.getMyAccounts();
        let firstAccount = my_accounts.length === 0;

        return (
            <div>
                <h4
                    style={{
                        fontWeight: "normal",
                        fontFamily: "Roboto-Medium, arial, sans-serif",
                        fontStyle: "normal",
                        paddingBottom: 15
                    }}
                >
                    <Translate content="wallet.wallet_password"/>
                </h4>

                <Translate
                    style={{textAlign: "left"}}
                    unsafe
                    component="p"
                    content="wallet.create_account_password_text"
                />

                <Translate
                    style={{textAlign: "left"}}
                    component="p"
                    content="wallet.create_account_text"
                />

                {firstAccount ? null : (
                    <Translate
                        style={{textAlign: "left"}}
                        component="p"
                        content="wallet.not_first_account"
                    />
                )}
            </div>
        );
    }

    _renderBackup() {
        return (
            <div className="backup-submit">
                <Translate component={"p"} content={"wallet.step_2"}/>
                <Translate unsafe component={"p"} content="wallet.password_crucial"/>

                <div>
                    {!this.state.showPass ? (
                        <div
                            onClick={() => {
                                this.setState({showPass: true});
                            }}
                            className="button"
                        >
                            <Translate content="wallet.password_show"/>
                        </div>
                    ) : (
                        <div>
                            <h5>
                                <Translate content="settings.password"/>:
                            </h5>

                            <input type="text" readOnly={true} defaultValue={this.state.generatedPassword} style={{
                                border: "none",
                                textAlign: "center"
                            }}/>
                        </div>
                    )}
                </div>
                <div className="divider"/>
                <p className="txtlabel warning">
                    <Translate unsafe content="wallet.password_lose_warning"/>
                </p>

                <div
                    style={{width: "100%"}}
                    onClick={() => {
                        this.props.history.push("/");
                    }}
                    className="button"
                >
                    <Translate content="wallet.ok_done"/>
                </div>
            </div>
        );
    }

    _renderGetStarted() {
        return (
            <div>
                {this._renderGetStartedText()}
                <table className="table">
                    <tbody>
                        <tr>
                            <td>
                                <Translate content="wallet.tips_dashboard"/>:
                            </td>
                            <td>
                                <Link to="/">
                                    <Translate content="header.dashboard"/>
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_account"/>:
                            </td>
                            <td>
                                <Link to={`/account/${this.state.accountName}/overview`}>
                                    <Translate content="wallet.link_account"/>
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_deposit"/>:
                            </td>
                            <td>
                                <Link to="/deposit-withdraw">
                                    <Translate content="wallet.link_deposit"/>
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_transfer"/>:
                            </td>
                            <td>
                                <Link to="/transfer">
                                    <Translate content="wallet.link_transfer"/>
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_settings"/>:
                            </td>
                            <td>
                                <Link to="/settings">
                                    <Translate content="header.settings"/>
                                </Link>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }

    _renderGetStartedText() {
        return (
            <div>
                <Translate component={"p"} content="wallet.congrat"/>
                <Translate component={"p"} content="wallet.tips_explore_pass"/>
                <Translate component={"p"} content="wallet.tips_header"/>
                <Translate component={"p"} content="wallet.tips_login"/>
            </div>
        );
    }

    render() {
        let {step} = this.state;
        // let my_accounts = AccountStore.getMyAccounts();
        // let firstAccount = my_accounts.length === 0;
        console.log("step", step);
        switch (step) {
            case 1:
                return this._renderAccountCreateForm();
            case 2:
                return this._renderBackup();
            default:
                this._renderGetStarted();
        }
        /*return (
            <div className="sub-content">
                {}
            </div>
        );*/
    }
}

export default connect(
    CreateAccountPassword,
    {
        listenTo() {
            return [AccountStore];
        },
        getProps() {
            return {};
        }
    }
);

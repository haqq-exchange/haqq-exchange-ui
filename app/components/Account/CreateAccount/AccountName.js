import React from "react";
import {Form, Input } from "antd";

// import "antd/lib/input/style/index.css";
import counterpart from "counterpart";
import classNames from "classnames";
import AccountApi from "../../../api/accountApi";
import {ChainValidation} from "deexjs";

class AccountName extends React.Component {

    static defaultProps = {
        "accountPrefix": null
    };

    getAccountName = name => {
        const {accountPrefix} = this.props;

        return accountPrefix ? [accountPrefix, name].join("-") : name;
    };

    onChangeName = () => {
        const {form} = this.props;
        const {validateFields} = form;

        validateFields(["account"], (err, values) => {
            const _account = !err ? values.account : false;
            if(this.props.onChange && _account) {
                this.props.onChange(this.getAccountName(_account));
            }
        });
    };

    checkAccountName = (value) => {
        return new Promise(resolve => {
            const _name = this.getAccountName(value);
            AccountApi.lookupAccounts(_name, 30)
                .then(result => {
                    resolve(result.find(a => a[0] === _name));
                });
        });
    };

    normalizeValue = (value, prevValue, allValues) => {
        /* добавление тире в слове */
        /*if( value && value.length > 1 ) {
            let currentValue = value.replace(/-/g, "");
            let centerValue = Math.round(currentValue.length/2);
            let startValue = currentValue.slice(0, centerValue);
            let endValue = currentValue.slice(centerValue, currentValue.length);
            value = [startValue, endValue].join("-");
        }*/

        return value ? value.toLowerCase() : "";
    };

    validateAccountName = (rule, value, callback) => {
        //const {form} = this.props;
        const _name = this.getAccountName(value);
        if ( value ) {
            // console.log("ChainValidation _name", _name, value )
            // console.log("ChainValidation.is_account_name", ChainValidation.is_account_name(_name))
            // console.log("ChainValidation.is_account_name_error", ChainValidation.is_account_name_error(_name))
            // console.log("ChainValidation.is_cheap_name", ChainValidation.is_cheap_name(_name))
            // console.log("ChainValidation.is_empty_user_input", ChainValidation.is_empty_user_input(_name))
            // console.log("ChainValidation.is_valid_symbol_error", ChainValidation.is_valid_symbol_error(_name))
            // console.log("ChainValidation.AccountActions", AccountActions.accountSearch(_name))
            if( !ChainValidation.is_account_name(_name) ){
                callback(counterpart.translate("form.message.not_account_name"));
            } else {
                this.checkAccountName(value).then(result => {
                    if( result && result.includes(value) ) {
                        callback(counterpart.translate("account.name_input.name_is_taken"));
                    } else {
                        callback();
                    }
                });
            }

        } else {
            callback();
        }
    };


    render() {
        const {getFieldDecorator, getFieldError, isFieldTouched} = this.props.form;
        let {className, accountPrefix} = this.props;
        if( !className ) className = "account-field account-name";

        let hasError = getFieldError("account") && isFieldTouched("account");

        let class_name = classNames(className, {
            ["error"]: hasError
        });

        return (
            <div className={class_name}>
                <Form.Item label={counterpart.translate("wallet.account_public")} {...this.props.getShowError("account")}>
                    {getFieldDecorator("account", {
                        validateTrigger: ["onChange", "onBlur"],
                        normalize: this.normalizeValue,
                        rules: [
                            {
                                required: true,
                                message: counterpart.translate("form.message.required"),
                            },
                            {
                                validator: this.validateAccountName,
                            }
                        ],
                    })( accountPrefix ?  <Input
                        autoFocus={true}
                        addonBefore={accountPrefix + "-"}
                        onChange={(event) => this.onChangeName(event)}
                        onBlur={(event) => this.onChangeName(event)}
                    /> : <Input
                        autoFocus={true}
                        onChange={(event) => this.onChangeName(event)}
                        onBlur={(event) => this.onChangeName(event)}
                    />  )}
                </Form.Item>
            </div>
        );
    }
}

// const AccountNameCreate = Form.create()(AccountName);

export default AccountName;

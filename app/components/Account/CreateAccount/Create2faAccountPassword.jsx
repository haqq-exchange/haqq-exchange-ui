import React from "react";
import {connect} from "alt-react";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";
import translator from "counterpart";
import {FetchChain, key} from "deexjs";
import PropTypes from "prop-types";
import QRCode from "qrcode.react";
import {saveAs} from "file-saver/FileSaver";
import {Form} from "antd";

import SettingsActions from "actions/SettingsActions";
import AccountActions from "actions/AccountActions";
import notify from "actions/NotificationActions";
import WalletActions from "actions/WalletActions";

import AccountStore from "stores/AccountStore";
import WalletDb from "stores/WalletDb";

import LoadingIndicator from "Components/LoadingIndicator";

import Recaptcha from "Components/Account/CreateAccount/Recaptcha";
import AccountEmail from "Components/Account/CreateAccount/AccountEmail";
import AccountName from "Components/Account/CreateAccount/AccountName";
import AccountPasswordGroups from "Components/Account/CreateAccount/AccountGroupPassword";
import AccountCheckboxUnderstand from "Components/Account/CreateAccount/AccountCheckboxUnderstand";

import TranslateWithLinks from "../../Utility/TranslateWithLinks";


import Crypto from "common/Crypto";

import ls from "common/localStorage";
import {ymSend} from "common/metrika";
import RefUtils from "common/ref_utils";
import {getRequestAddress} from "api/apiConfig";

import "./createaccount.scss";
import SettingsStore from "../../../stores/SettingsStore";
import configConst from "config/const";
import ReferralId from "./ReferralId";

let accountStorage = new ls(configConst.STORAGE_KEY);


class CreateAccountPassword extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor(props) {
        super(props);
        this.state = {
            validAccountName: false,
            accountName: "",
            validPassword: false,
            registrar_account: null,
            loading: false,
            hide_refcode: true,
            show_identicon: false,
            step: 1,
            showPass: false,
            generatedPassword: ("P" + key.get_random_key().toWif()).substr(0, 45)
        };

        this.isValidCreate = this.isValidCreate.bind(this);
        this.changeStep = this.changeStep.bind(this);

        this.accountNameInput = null;
        this.refutils = new RefUtils({
            url: getRequestAddress("api")
        });


    }

    componentDidMount() {
        if (!WalletDb.getWallet()) {

            SettingsActions.changeSetting({
                setting: "passwordLogin",
                value: "login2fa"
            });

            AccountActions.saveDataAccount.defer({
                generatedPassword: ("P" + key.get_random_key().toWif()).substr(
                    0,
                    45
                ),
                confirmPassword: "",
                chbox_understand: false
            });
        }
    }

    isValidCreate() {
        let firstAccount = AccountStore.getMyAccounts().length === 0;
        let valid = this.state.validAccountName;
        if (!WalletDb.getWallet()) {
            valid = valid && this.state.validPassword;
        }
        if (!firstAccount) {
            valid = valid && this.state.registrar_account;
        }
        return valid && this.state.chbox_understand;
    }


    changeStep(step) {
        // console.log("changeStep", this, step);
        this.setState({step});
    }


    saveAccountData = (data) => {
        const {generatedPassword} = this.state;
        console.log("data this.state", this.state);
        console.log("data", data);
        let encryptBf = Crypto.encryptBF(generatedPassword, data.userPassword);

        if (encryptBf) {
            accountStorage.set("qrCode", encryptBf);
            data.encryptPassword = encryptBf;
            this.setState(data, ()=>{
                this.createAccount(data.accountName, generatedPassword, data);
                ymSend("reachGoal", "ONSUB_REG" , {
                    type_reg: "2FA"
                });
            });
        }
    };

    regFailed = (data, cause) => {
        this.refutils.postTemplate("/verify/reg_failed", {
            code: data.code,
            account: data.accountName,
            system_id: this.refutils.getSystemId(),
            // system_id: __GBL_CHAIN__ || __GBLTN_CHAIN__ ? "gbl" : (__SCROOGE_CHAIN__ ? "scrooge" : (__DEEX_CHAIN__ ? "deex" : "bts")),
            cause
        }, "");
    };


    checkEmailAccount = (data) => {
        return new Promise((resolve, reject)=>{
            if(!data.code || !data.accountName) {
                reject({
                    error: "Not found code or name"
                });
            } else {
                this.refutils.postTemplate("/verify/check_code", {
                    code: data.code,
                    account: data.accountName,
                    system_id: this.refutils.getSystemId(),
                }, "").then(res => {

                    if( res.status === 200 ) {
                        resolve();
                    } else {
                        reject(res.content);
                    }
                });
            }
        });
    };

    createAccount(name, password, data) {
        let referralAccount = AccountStore.getState().referralAccount || data.refId;
        this.setState({loading: true});
        window.console.log("name, password", name, password);
        console.log("data", data);
        // console.log("CreateAccountPassword starts create new account", AccountActions);
        AccountActions.createAccountWithPassword(
            name,
            password,
            null,
            referralAccount,
            0,
            null,
            {
                code: data.code,
                email: data.email
            }
        ).then(() => {
            FetchChain("getAccount", name, undefined, {
                [name]: true
            }).then(result => {
                window.console.log("result", result);
                this._unlockAccount(name, password);
                //this._sendSaveEmail(name);
                this.setState({
                    step: 2
                });
            }).catch(() => {
                //this.createAccount(name, password);

                notify.addNotification({
                    message: "Failed fetch get account",
                    level: "error",
                    autoDismiss: 10
                });
            });
        })
            .catch(error => {
                console.log("ERROR AccountActions.createAccount", error);
                let error_msg =
                    error.base && error.base.length && error.base.length > 0
                        ? error.base[0]
                        : "unknown error";
                if (error.remote_ip) error_msg = error.remote_ip[0];
                notify.addNotification({
                    message: `Failed to create account: ${name} - ${error_msg}`,
                    level: "error",
                    autoDismiss: 10
                });
                this.setState({loading: false});
                this.regFailed(data, error_msg);
                AccountStore.removeCurrentAccount();
            });
    }

    _unlockAccount(name, password) {
        window.console.log("WalletActions.setWallet", name, password);
        WalletActions.setWallet(name, password)
            .then(() => WalletDb.isLocked())
            .then(() => {
                window.console.log("WalletDb.validatePassword");
                return WalletDb.validatePassword(password, true, name);
            }).then(() => {
                window.console.log("AccountActions.setPasswordAccount");
                AccountActions.setPasswordAccount(name);
            });
    }

    wrapChangeState = data => this.setState(data);

    saveQrCode(event){
        event.preventDefault();
        const {accountName} = this.props;
        var canvas = document.getElementById("qrcode");
        let date = new Date();
        let dateFormat = [date.getDate(),date.getMonth(),date.getFullYear()].join("");
        canvas.toBlob(function(blob) {
            saveAs(blob, [accountName, dateFormat, "png"].join("."));
        });
    }


    render() {
        let {step, loading, resendEmail} = this.state;

        return (
            <div className="sub-content">
                {step === 1 && (
                    <WrappedRegistrationForm
                        loading={loading}
                        locale={this.props.locale}
                        email_code={this.props.email_code}
                        referralState={this.props.referralState}
                        resendEmail={resendEmail}
                        wrapChangeState={this.wrapChangeState}
                        createAccount={this.saveAccountData}
                    />
                )}
                {step === 2 && <AccountBackup {...this.state} />}
            </div>
        );
    }
}


class AccountBackup extends React.PureComponent {
    constructor(props){
        super(props);

        this.state = {
            showPass: false
        };
        this.setShowPassword = this.setShowPassword.bind(this);
        this.saveQrCode = this.saveQrCode.bind(this);
    }

    setShowPassword(event){
        event.preventDefault();
        this.setState({showPass: true});
    }
    saveQrCode(event){
        event.preventDefault();
        const {accountName} = this.props;
        var canvas = document.getElementById("qrcode");
        let date = new Date();
        let dateFormat = [date.getDate(),date.getMonth(),date.getFullYear()].join("");
        canvas.toBlob(function(blob) {
            saveAs(blob, [accountName, dateFormat, "png"].join("."));
        });
    }

    render() {
        const {showPass} = this.state;
        const {encryptPassword} = this.props;
        // console.log("AccountBackup mySaveDataAccount", mySaveDataAccount)
        return (
            <div className="backup-submit">
                <p>
                    <Translate unsafe content="wallet.password_crucial_qr" />
                </p>

                <div className={"backup-qrcode"}>
                    <QRCode
                        id={"qrcode"}
                        size={230}
                        includeMargin={true}
                        style={{
                            "border": "2px solid #ffffff"
                        }}
                        value={encryptPassword}/>
                    <div className={"backup-qrcode-link"}>
                        <a
                            href={""}
                            onClick={this.saveQrCode}
                            className="btn btn-green"
                        >
                            <Translate content="wallet.save_qrcode" />
                        </a>
                    </div>
                </div>

                <div className="divider" />
                <div>
                    {!showPass ? (
                        <a
                            href={""}
                            onClick={this.setShowPassword}
                            className="button"
                        >
                            <Translate content="wallet.password_show" />
                        </a>
                    ) : (
                        <ShowMyPassword {...this.props} />
                    )}
                </div>
                <br/>
                <div className="warning" style={{color: "#dd2233"}}>
                    <Translate unsafe content="wallet.password_lose_warning_qr" />
                </div>

                <Link
                    style={{width: "100%", marginTop: "20px", display: "inline-block"}}
                    to={"/"}
                    className={"public-link"}
                >
                    <Translate unsafe content="wallet.ok_done_qr" />
                </Link>
            </div>
        );
    }
}


const ShowMyPassword = (props) => {
    return (
        <div className={"show-info"}>
            <div className={"show-info-line"}>
                <h5>
                    <Translate content="wallet.link_account" />:
                </h5>
                <p
                    style={{
                        fontWeight: "normal",
                        fontFamily:
                            "Roboto-Medium, arial, sans-serif",
                        fontStyle: "normal",
                        textAlign: "center"
                    }}
                >
                    {props.accountName}
                </p>
            </div>
            <div className={"show-info-line"}>
                <h5>
                    <Translate content="settings.password" />:
                </h5>
                <p
                    style={{
                        fontWeight: "normal",
                        fontFamily:
                            "Roboto-Medium, arial, sans-serif",
                        fontStyle: "normal",
                        textAlign: "center"
                    }}
                >
                    {props.userPassword}
                </p>
            </div>
        </div>
    );
};

class CreateAccount extends React.Component{

    state = {
        showRecaptcha: false,
        cap: "",
        sendConfirmEmail: false,
        refId: null,
        sendConfirmReferral: false,
    };

    componentDidMount() {
        const defRef = this.props.referralState?.query
        defRef && this.props.form.setFieldsValue({referral: defRef});
        this.props.form.validateFields();
    }

    handleSubmit = (event) => {
        event.preventDefault();

        this.props.form.validateFields((err) => {
            if (err) {
                return false;
            }
            this.props.wrapChangeState({resendEmail: false});
            this.props.createAccount(this.state);
        });
    };

    setAccountData = (name, value) => {
        if( typeof name === "string" && typeof value !== "object" ) {
            this.setState({
                [name]: value
            });
        } else {
            this.setState(value);
        }
        console.log("setAccountData", name, value);
    };

    hasErrors = (fieldsError) => {
        return Object.keys(fieldsError).some(field => fieldsError[field]);
    };

    getShowError = (name) => {
        const {isFieldTouched, getFieldError}= this.props.form;
        const hasValueError = name === "referral" && this.state.sendConfirmReferral ? getFieldError(name) : isFieldTouched(name) && getFieldError(name);
        // const hasValueError = isFieldTouched(name) && getFieldError(name);
        return {
            validateStatus: hasValueError ? "error" : "",
            help: hasValueError || ""
        };
    };

    setConfirmEmail = () => {
        const {getFieldValue} = this.props.form;
        this.setState({
            sendConfirmEmail: {
                [getFieldValue("email")]: true
            }
        });
    }
    setRefId = () => {
        const {getFieldValue} = this.props.form;
        this.setState({
            refId: getFieldValue("referral")
        });
    }


    setConfirmReferral = () => {
        this.setState({sendConfirmReferral: true});
    }

    resetConfirmEmail = () => {
        this.setState({
            sendConfirmEmail: false,
        });
    }

    render() {
        const {loading} = this.props;
        const {getFieldsError} = this.props.form;

        return (
            <div className={"create-account-inner"}>
                <Form onSubmit={this.handleSubmit}>

                    <Translate content={"form.all_required"} className={"create-account-sub-title"} />

                    <AccountName
                        getShowError={this.getShowError}
                        onChange={(data)=>this.setAccountData("accountName", data)} {...this.props} />

                    <ReferralId
                        getShowError={this.getShowError}
                        onChange={(data)=>this.setAccountData("referral", data)} {...this.props} 
                        sendConfirmEmail={this.state.sendConfirmEmail} />

                    <AccountEmail
                        getShowError={this.getShowError}
                        onChange={(data)=>this.setAccountData("email", data)} {...this.props}
                        sendConfirmEmail={this.state.sendConfirmEmail} sendConfirmReferral={this.state.sendConfirmReferral}
                        setConfirmEmail={this.setConfirmEmail} setConfirmReferral={this.setConfirmReferral}
                        setRefId={this.setRefId} resetConfirmEmail={this.resetConfirmEmail} />

                    <AccountPasswordGroups
                        generatedPassword={false}
                        hasReadOnly={false}
                        getShowError={this.getShowError}
                        onChange={(data)=>this.setAccountData("userPassword", data)} {...this.props} />

                    <AccountCheckboxUnderstand
                        getShowError={this.getShowError}
                        onChange={(data)=>this.setAccountData("undestand", data)} {...this.props} />

                    <Form.Item className={"create-account-btn"}>
                        {loading ?
                            <LoadingIndicator type="three-bounce"/> :
                            <Translate
                                disabled={this.hasErrors(getFieldsError())}
                                className={"btn btn-green"} component={"button"} type={"submit"} content="account.create_account"/>
                        }
                    </Form.Item>


                </Form>

                <div className="additional-account-options">
                    <TranslateWithLinks
                        string="account.optional.formatter"
                        keys={[
                            {
                                type: "link",
                                className: "public-link",
                                value: "/wallet/backup/restore",
                                translation:
                                    "account.optional.restore_link",
                                dataIntro: translator.translate(
                                    "walkthrough.restore_account"
                                ),
                                arg: "restore_link"
                            },
                            {
                                type: "link",
                                className: "public-link",
                                value: "/create-account/wallet",
                                translation:
                                    "account.optional.restore_form",
                                dataIntro: translator.translate(
                                    "walkthrough.create_local_wallet"
                                ),
                                arg: "restore_form"
                            }
                        ]}
                    />
                </div>
            </div>
        );
    }

}

const WrappedRegistrationForm = Form.create({ name: "register" })(CreateAccount);

export default connect(
    CreateAccountPassword,
    {
        listenTo() {
            return [AccountStore, SettingsStore];
        },
        getProps() {
            return {
                mySaveDataAccount: AccountStore.getState().mySaveDataAccount,
                locale: SettingsStore.getState().settings.get("locale"),
                email_code: SettingsStore.getState().viewSettings.get("email_code")
            };
        }
    }
);

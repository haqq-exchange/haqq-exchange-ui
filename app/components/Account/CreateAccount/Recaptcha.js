import React, { Component } from "react";
import { ReCaptcha , loadReCaptcha} from "react-recaptcha-v3";

class Recaptcha extends Component {

    static defaultProps = {
        captchaData: {
            sitekey: "6Lee6bgUAAAAAFfJrv2qtb93ggOg9GfQxLjUOYNz",
            action: "register_deex_website"
        }
    };

    componentDidMount() {
        this.reloadCaptcha();
        //setInterval(this.reloadCaptcha, 30 * 1000);
    }

    reloadCaptcha = () => {
        const {captchaData} = this.props;
        loadReCaptcha(captchaData.sitekey);
    };

    verifyCallback = (recaptchaToken) => {
        // Here you will get the final recaptchaToken!!!
        console.log("recaptchaToken", recaptchaToken, "<= your recaptcha token");
        if(this.props.onChange) {
            this.props.onChange(recaptchaToken);
        }
    };

    render() {
        const {captchaData} = this.props;
        return (
            <ReCaptcha verifyCallback={this.verifyCallback} {...captchaData}/>
        );
    }
}

export default Recaptcha;

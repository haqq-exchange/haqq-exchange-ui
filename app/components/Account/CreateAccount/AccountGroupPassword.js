import React from "react";
import {Form, Input } from "antd";
import Translate from "react-translate-component";
import Icon from "Components/Icon/Icon";
// import "antd/lib/input/style/index.css";
import counterpart from "counterpart";
import classNames from "classnames";
import CopyButton from "Utility/CopyButton";
import {key} from "deexjs";

export default class AccountPasswordGroups extends React.Component {

    static defaultProps = {
        hasReadOnly: true,
        //generatedPassword:  ("P" + key.get_random_key().toWif()).substr(0, 45)
    };

    constructor(props) {
        super(props);

        this.state = {
            confirmDirty: false,
            generatedPassword: ""
        };

        // this.generatedPassword();

    }


    componentDidMount() {
        //this.generatedPassword();
    }


    generatedPassword() {
        let generatedPassword= ("P" + key.get_random_key().toWif()).substr(
            0,
            45
        );
        this.props.form.setFieldsValue({
            "password": generatedPassword
        });
        this.setState({generatedPassword});
    }

    compareToFirstPassword = (rule, value, callback) => {
        const {form} = this.props;
        if (value && value !== form.getFieldValue("password")) {
            callback(counterpart.translate("wallet.confirm_error"));
        } else {
            if( this.props.onChange ) {
                this.props.onChange(value);
            }
            callback();
        }
    };

    validateToNextPassword = (rule, value, callback) => {
        const {form} = this.props;
        const {generatedPassword} = this.state;
        console.log("value", value);

        if( generatedPassword !== value ) {
            this.setState({
                generatedPassword: value
            });
        }


        if (value && this.state.confirmDirty) {
            form.validateFields(["confirm"], { force: true });
        }
        if( this.props.onChange ) {
            this.props.onChange(value);
        }
        callback();
    };


    render() {
        const {generatedPassword} = this.state;
        const {hasReadOnly} = this.props;
        const {getFieldDecorator, getFieldsError, isFieldsTouched} = this.props.form;
        let {className} = this.props;
        if( !className ) className = "account-field password";

        let hasError = getFieldsError(["password", "confirm"]) && isFieldsTouched(["password", "confirm"]);

        console.log("hasError", hasError);

        console.log("generatedPassword", generatedPassword);

        let class_name = classNames(className, {
            ["error"]: hasError && (hasError.password || hasError.confirm)
        });


        return (
            <div className={class_name}>
                <Form.Item label={counterpart.translate("wallet.enter_password")} {...this.props.getShowError("password")} >
                    {getFieldDecorator("password", {
                        //ini: generatedPassword ,
                        validateTrigger: "onBlur",
                        rules: [
                            {
                                required: true,
                                message: counterpart.translate("form.message.required"),
                            },
                            {
                                min: 12,
                                message: counterpart.translate("form.message.minLength", {
                                    minLength: 12
                                })
                            },{
                                validator: this.validateToNextPassword,
                            },
                        ],
                    })(generatedPassword ?
                        <Input.Password readOnly={hasReadOnly} addonAfter={<CopyButton
                            text={generatedPassword}
                            className={"ant-input-clipboard"}
                            tip="tooltip.copy_password"
                            dataPlace="top" />} /> :
                        <Input.Password  /> )}

                </Form.Item>
                <div onClick={()=>this.generatedPassword()} className={"generate-pass"}>
                    <Icon name={"security"} />
                    <Translate content={"form.label.generate_pass"} className={""}  />
                </div>
                <Form.Item label={counterpart.translate("wallet.confirm_password")} {...this.props.getShowError("confirm")}>
                    {getFieldDecorator("confirm", {
                        validateTrigger: ["onChange", "onBlur"],
                        rules: [
                            {
                                required: true,
                                message: counterpart.translate("form.message.required"),
                            },
                            {
                                min: 12,
                                message: counterpart.translate("form.message.minLength", {
                                    minLength: 12
                                }),
                            }, {
                                validator: this.compareToFirstPassword,
                            },
                        ],
                    })(<Input.Password />)}
                </Form.Item>
            </div>
        );
    }
}

// const AccountGroupPasswordCreate = Form.create()(AccountGroupPassword);

// export default AccountGroupPassword;

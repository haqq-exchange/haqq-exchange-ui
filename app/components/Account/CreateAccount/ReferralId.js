import React from "react";
import {Form, Input} from "antd";
import counterpart from "counterpart";
import classNames from "classnames";

class ReferralId extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {getFieldDecorator, getFieldError, getFieldValue, isFieldTouched } = this.props.form;
        let {className, sendConfirmEmail} = this.props;
        if( !className ) className = "account-field";

        const hasError = getFieldError("referral") && isFieldTouched("referral");

        const class_name = classNames(className, {
            ["error"]: hasError || this.props.getShowError("referral").validateStatus
        });

        return (
            <div className={class_name}>
                <Form.Item label={counterpart.translate("wallet.referral_id")} {...this.props.getShowError("referral")}>
                    {getFieldDecorator("referral", {
                        validateTrigger: ["onBlur", "onChange"],
                        normalize: value => value && value.trim(),
                        rules: [
                            {
                                required: true,
                                message: counterpart.translate("form.message.required"),
                            }
                        ],
                    })(  
                        <Input disabled={sendConfirmEmail?.[getFieldValue("email")]} />
                    )}
                </Form.Item>
            </div>
        );
    }
}

export default ReferralId;

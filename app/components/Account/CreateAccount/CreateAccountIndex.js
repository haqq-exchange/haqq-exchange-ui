import React from "react";
import {connect} from "alt-react";
import cname from "classnames";
import AccountStore from "stores/AccountStore";
import {Link, withRouter} from "react-router-dom";
import Translate from "react-translate-component";
import { Carousel } from "antd";
import counterpart from "counterpart";
import MediaQuery from "react-responsive";
import ResponseOption from "config/response";
import {ymSend} from "lib/common/metrika";
import "./createaccount.scss";

class CreateAccountIndex extends React.Component {

    static defaultProps = {
        models: {
            "password": {
                "title": "public.InfoRegistration.models.password.title",
                "benefit": [
                    "public.InfoRegistration.models.password.benefit0",
                    "public.InfoRegistration.models.password.benefit1"
                ]
            },
            /* "wallet": {
                "title": "public.InfoRegistration.models.wallet.title",
                "benefit": [
                    "public.InfoRegistration.models.wallet.benefit0",
                    "public.InfoRegistration.models.wallet.benefit1",
                    "public.InfoRegistration.models.wallet.benefit2"
                ]
            },*/
            "password-2fa": {
                "title": "public.InfoRegistration.models.password-2fa.title",
                "benefit": [
                    "public.InfoRegistration.models.password-2fa.benefit0",
                    "public.InfoRegistration.models.password-2fa.benefit1"
                ]
            }
        },
        scroogeModels: {
            "password": {
                "title": "public.InfoRegistration.models.password.title",
                "benefit": [
                    "public.InfoRegistration.models.password.benefit0",
                    "public.InfoRegistration.models.password.benefit2"
                ]
            }
        }
    };

    constructor() {
        super();
        this.state = {
            slug: null
        };
    }

    goRegister = (slug) => {
        const search = this.props.history.location.search;
        const query =  search.startsWith("?ref=") ? search.slice(5) : "";
        ymSend("reachGoal", "CLICK_REG" , {});
        let url = ["/create-account", slug].join("/");
        this.props.history.push({pathname: url, state: {query}});
    };


    onChange = (a, b) => {
        const {models} = this.props;
        console.log(a, b, Object.keys(models) );
        this.setState({
            slug: Object.keys(models)[a]
        });
    };

    selectedAccountDesktop = (keys) => {
        const {models, scroogeModels} = this.props;
        const {slug: slugState} = this.state;
        let registrationModels = __SCROOGE_CHAIN__ ? scroogeModels : models;
        return (
            Object.keys(registrationModels).map(slug=> {
                const model = registrationModels[slug];
                return (
                    <div key={"create-account-item-" + keys + "-" + slug} onClick={()=>this.goRegister(slug)} className={cname("create-account-item", {active: slug === slugState })}>
                        <Translate className={cname("model-item-title", slug)} component={"span"}  content={model.title} />
                        <ul className={"model-item-ul"}>
                            {model.benefit.map((benefit, index)=><li key={"model-item-li-"+index} className={"model-item-li"}><Translate content={benefit} /></li>)}
                        </ul>
                    </div>
                );
            })
        );
    };
    selectedAccountMobile = () => {
        return (
            <Carousel adaptiveHeight={false} afterChange={this.onChange}>
                {this.selectedAccountDesktop("mobile")}
            </Carousel>
        );
    };


    render() {
        const {slug: slugState} = this.state;
        
        return (
            <div className="create-account">
                <div className="create-account-title">
                    <Translate content={"public.register"} />
                </div>
                <div className={"create-account-desc"}>
                    {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?
                    <Translate unsafe component="div" content={"public.deex_p3_gbl"} />:
                    <Translate unsafe component="div" content={__SCROOGE_CHAIN__ ? "public.deex_p3_scrooge" : "public.deex_p3_haqq"} />}
                </div>
                <div className={__SCROOGE_CHAIN__ ? "create-account-wrap-scrooge" : "create-account-wrap"}>
                    <MediaQuery {...ResponseOption.mobile}>
                        {(matches) => {
                            if (matches) {
                                return this.selectedAccountMobile();
                            } else {
                                return this.selectedAccountDesktop("Desktop");
                            }
                        }}
                    </MediaQuery>
                </div>

            </div>
        );
    }
}

const CreateAccountRouter = withRouter(CreateAccountIndex);

export default connect(CreateAccountRouter, {
    listenTo() {
        return [AccountStore];
    },
    getProps() {
        return {};
    }
});

import React from "react";
import Translate from "react-translate-component";
// import AccountPermissionsWitness from "./AccountPermissionsWitness";
import {JoinWitnessesBind} from "Components/Account/AccountVoting/JoinWitnessesModal";

import { Tabs } from "antd";
import "./AccountPermission.scss";
const { TabPane } = Tabs;

class AccountPermissions extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            WitnessById: false
        };
    }

    componentDidMount() {
    }

    componentDidUpdate(){

    }

    /* getWitness = () => {
        const {account} = this.props;
        if (account) {
            let isLifeStatus = this.getMemberStatus(account) === "lifetime";
            if( isLifeStatus ) {
                this.getWitnessAccount(account);
            }
        }
    }


    getMemberStatus = account => {
        return ChainStore.getAccountMemberStatus(
            account
        );
    }

    getWitnessAccount = async account => {
        console.log('account.get("id"): ', account.get("id"));
        let WitnessById = await ChainStore.getWitnessById(account.get("id"));
        this.setState({WitnessById});

    } */
    onChangeTabs = (active) => {
        this.setState({tab: active});
    }

    render() {
        const {tab, WitnessById} = this.state;

        console.log("AccountPermissions", this.props);

        return (
            <div className="grid-content app-tables no-padding account-permission" ref="appTables">
                <div className="content-block small-12">
                    <div className="tabs-container generic-bordered-box" style={{margin: "0 20px"}}>
                        <Tabs
                            type="card"
                            onChange={this.onChangeTabs}
                            activeKey={"witness"}
                            destroyInactiveTabPane={true}
                            segmented={false}
                            setting="explorer-tabs"
                            tabBarExtraContent={<></>}
                            tabsStyle={{width: "100%", padding: "10px 20px"}}
                        >
                            <TabPane key={"witness"} tab={<Translate content={"account.perm.witness"}/>} >
                                <JoinWitnessesBind
                                    wallet_locked={this.props.wallet_locked}
                                    isMyAccount={this.props.isMyAccount}
                                    account={this.props.account} />
                            </TabPane>
                        </Tabs>
                    </div>
                </div>
            </div>
        );
    }
}

export default AccountPermissions;

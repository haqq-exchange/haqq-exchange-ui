import React from "react";
import PropTypes from "prop-types";
import ZfApi from "react-foundation-apps/src/utils/foundation-api";
import AltContainer from "alt-container";
import WalletDb from "stores/WalletDb";
import WalletUnlockStore from "stores/WalletUnlockStore";
import WalletManagerStore from "stores/WalletManagerStore";
import BackupStore from "stores/BackupStore";
import AccountStore from "stores/AccountStore";
import Crypto from "lib/common/Crypto";
import classnames from "classnames";
import WalletUnlockActions from "actions/WalletUnlockActions";
import WalletActions from "actions/WalletActions";
import AccountActions from "actions/AccountActions";
import utils from "common/utils";
import AccountName from "components/Public/AccountName";
import counterpart from "counterpart";
import {DisableChromeAutocomplete} from "components/Wallet/WalletUnlockModalLib";
import Translate from "react-translate-component";
import Field from "components/Public/Field";
import QrReader from "react-qr-reader";
// import QrReader from "react-qr-scanner";
import ls from "common/localStorage";
import RefUtils from "common/ref_utils";
import SettingsActions from "../../../actions/SettingsActions";
import {ymSend} from "../../../lib/common/metrika";
import {getRequestAddress} from "../../../api/apiConfig";
import configConst from "config/const";
let accountStorage = new ls(configConst.STORAGE_KEY);

class ErrorMessageShow extends React.Component {
    render() {
        return (
            <Translate component={"div"} className={"login-error"} content={this.props.errorMessage} />
        );
    }
}
class Login2fa extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor(props) {
        super(props);
        this.state = this.initialState(props);
        this.handleScan = this.handleScan.bind(this);
        this.refutils = new RefUtils({
            url: getRequestAddress("referral")
        });

        console.log("Login2fa constructor");
    }

    initialState = (props = this.props) => {
        const {passwordAccount, currentWallet} = props;
        SettingsActions.changeSetting({
            setting: "passwordLogin",
            value: "login2fa"
        });
        return {
            passwordError: null,
            accountName: passwordAccount,
            walletSelected: !!currentWallet,
            customError: null,
            isOpen: false,
            restoringBackup: false,
            stopAskingForBackup: false,
            delay: 500,
            qrCode: false,
            legacyMode: false,
            showScanner: false,
            showLoadImage: false,
        };
    };

    UNSAFE_componentWillReceiveProps(np) {
        const {walletSelected, restoringBackup, accountName} = this.state;
        const {
            currentWallet: newCurrentWallet,
            passwordAccount: newPasswordAccount
        } = np;

        const newState = {};
        if (newPasswordAccount && !accountName)
            newState.accountName = newPasswordAccount;
        if (walletSelected && !restoringBackup && !newCurrentWallet)
            newState.walletSelected = false;
        if (this.props.passwordLogin !== np.passwordLogin) {
            newState.passwordError = false;
            newState.customError = null;
        }

        this.setState(newState);
    }

    shouldComponentUpdate(np, ns) {
        return (
            !utils.are_equal_shallow(np, this.props) ||
            !utils.are_equal_shallow(ns, this.state)
        );
    }

    componentDidMount() {
        const {modalId} = this.props;

        if( modalId ) {
            ZfApi.subscribe(modalId, (name) => {
                if (name !== modalId) return;
            });
        }
    }

    componentDidUpdate() {
        const {resolve, modalId, isLocked} = this.props;

        if (resolve)
            if (isLocked) {
                ZfApi.publish(modalId, "open");
            } else {
                resolve();
            }
        else ZfApi.publish(this.props.modalId, "close");
    }

    onImageLoad(data){
        console.log("onImageLoad", data);
    }
    handleScan(data){

        console.log("data", data)

        if(data){
            this.setState({
                qrCode: data,
                showLoadImage: false,
                showScanner: false
            });

            accountStorage.set("qrCode", data);
            this.handleLogin();
        }
    }
    handleError(){
        this.setState({
            showScanner: false,
            legacyMode: true,
            showLoadImage: true,
        });
    }

    handleLogin = (event) => {
        if(event) event.preventDefault();
        const {qrCode, password_input, account_name} = this.state;

        // new Error("handleLogin");

        if( qrCode && account_name && password_input) {
            /* старая версия кодировки, уже не регаем но возможно кто то прийдет */
            const decrypted = Crypto.decrypt(qrCode, password_input);
            const decryptBF = Crypto.decryptBF(qrCode, password_input);

            let decryptPassword = decrypted || decryptBF;
            if( decryptPassword) {
                this.validate(decryptPassword, account_name);
            } else {
                this.setState({
                    passwordError: true,
                    customError: "wallet.qrCode_incorrect"
                });
            }
        }
    };

    validate = (password, account) => {
        //debugger;
        const {resolve} = this.props;
        const wallet = WalletDb.getWallet();

        const promiseLogin = (result) => {
            console.log("promiseLogin result", result);
            return new Promise((resolve, reject) => {
                if( result.success ) {
                    AccountActions.setPasswordAccount(account);
                    //AccountActions.setCurrentAccount(account);
                    WalletUnlockActions.change();
                    WalletUnlockActions.cancel();
                    ymSend("reachGoal", "ENTER");
                    this.refutils.getUserReferral(
                        account,
                        AccountStore.getState().referralAccount
                    );

                    resolve();
                } else {
                    reject();
                }
            });
        };

        //debugger;
        if( !wallet || wallet.public_name !== account ) {
            WalletDb.setWallet(null);
            WalletActions.setWallet(account, password)
                .then(()=>WalletDb.isLocked())
                .then(()=>{
                    return WalletDb.validatePassword(password, true, account);
                })
                .then(promiseLogin)
                .catch(()=>{
                    this.setState({
                        passwordError: true,
                        loaded: false
                    });
                });
        } else {
            WalletDb.validatePassword(password, true, account)
                .then(promiseLogin)
                .catch(()=>{
                    this.setState({
                        passwordError: true,
                        loaded: false
                    });
                });
        }

        if (resolve) {
            resolve();
        }

    };

    render() {

        const {
            showScanner,
            showLoadImage,
            qrCode,
            passwordError,
            customError,
            accountName
        } = this.state;


        return (
            <div className="password-form">
                <form onSubmit={this.handleLogin} className="full-width">
                    {passwordError ? <ErrorMessageShow errorMessage={customError ? customError : "wallet.pass_incorrect"}  />: null}
                    <DisableChromeAutocomplete />
                    <AccountName
                        label="account.name"
                        ref={(ref)=>this.ref_account_input=ref}
                        accountName={accountName}
                        account={accountName}
                        onChange={(name)=>{
                            this.setState({account_name: name}, this.handleLogin);
                        }}
                        onAccountChanged={() => {}}
                        size={60}
                        hideImage
                        placeholder=" "
                        useHR
                        labelClass="login-label"
                        reserveErrorSpace
                        onEnter={()=>{}}
                    />
                    <Field
                        type="password"
                        ref={(ref)=>this.ref_password_input=ref}
                        inputRef="ref_password_input"
                        name="password"
                        id="password"
                        placeholder={counterpart.translate(
                            "settings.password"
                        )}
                        onChange={(event)=>{
                            this.setState({
                                password_input: event.target.value
                            }, this.handleLogin );
                        }}
                    />
                    {!showScanner && !showLoadImage &&
                        (<div className={"public-buttons"} style={{marginTop: 20}}>
                            {!qrCode && (<button
                                type="button"
                                onClick={()=>{
                                    this.setState({
                                        showScanner: true
                                    });
                                }}
                                className={
                                    classnames("btn btn-green btn-qr-code", {
                                        "button-active": !!qrCode
                                    })
                                }>
                                <Translate content={"public.show_scanner_qrcode"}/>
                            </button>)}

                            {qrCode && (<div>
                                <Translate style={{textAlign: "center", margin: "10px 0"}} component={"div"} content={"public.loaded_scanner_qrcode"}/>
                                <Translate
                                    component={"button"}
                                    className={classnames("btn btn-green btn-qr-code")}
                                    onClick={()=>{
                                        this.setState({
                                            showScanner: false,
                                            qrCode: false,
                                            showLoadImage: false,
                                            legacyMode: false,
                                        });
                                    }}
                                    content={"public.loading_new_qrcode"}/>
                            </div>)}

                        </div>)
                    }
                    {showScanner ?
                        <div className={"public-qr-code"}>
                            <QrReader
                                style={{
                                    width: "260px",
                                    height: "260px",
                                    margin: "0 auto"
                                }}
                                ref={(ref)=>this.refReader=ref}
                                delay={this.state.delay}
                                legacyMode={this.state.legacyMode}

                                onImageLoad={this.onImageLoad}
                                onError={()=>this.handleError()}
                                onScan={this.handleScan}
                            />
                        </div> : null}

                    {showLoadImage && this.state.legacyMode ? <div className={"public-buttons"} style={{marginTop: 20}}>

                        <QrReader
                            onImageLoad={this.onImageLoad}
                            onScan={this.handleScan}
                            onError={()=>this.handleError()}
                            ref={(ref)=>this.refReader=ref}
                            maxImageSize={10000}
                            legacyMode={this.state.legacyMode}
                            delay={this.state.delay}
                        />


                        {this.refReader && (<button
                            type={"button"}
                            className={classnames("btn btn-red btn-qr-code")}
                            onClick={()=>this.refReader.openImageDialog()}>
                            <Translate content={"public.load_scanner_qrcode"}/>
                        </button>)}
                    </div> : null}

                </form>
            </div>
        );
    }
}

class LoginOldContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[
                    WalletUnlockStore,
                    AccountStore,
                    WalletManagerStore,
                    WalletDb,
                    BackupStore
                ]}
                inject={{
                    currentWallet: () =>
                        WalletManagerStore.getState().current_wallet,
                    walletNames: () =>
                        WalletManagerStore.getState().wallet_names,
                    dbWallet: () => WalletDb.getWallet(),
                    isLocked: () => WalletDb.isLocked(),
                    backup: () => BackupStore.getState(),
                    resolve: () => WalletUnlockStore.getState().resolve,
                    reject: () => WalletUnlockStore.getState().reject,
                    locked: () => WalletUnlockStore.getState().locked,
                    passwordLogin: () =>
                        WalletUnlockStore.getState().passwordLogin,
                    passwordAccount: () =>
                        AccountStore.getState().passwordAccount || ""
                }}
            >
                <Login2fa {...this.props} />
            </AltContainer>
        );
    }
}
export default LoginOldContainer;

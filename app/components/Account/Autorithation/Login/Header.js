import React from "react";
import Translate from "react-translate-component";
import {Link} from "react-router-dom";
const translator = require("counterpart");
//import * as RoutesLink from "RoutesLink";
//import PublicHeader from "components/Layout/PublicHeader";

export default class LoginHeader extends React.Component {
    static defaultProps = {
        contentStatic: {
            "login": "password",
            "login-2fa": "password-2fa",
            "login-lw": "wallet",
        }
    };

    render(){
        const {contentStatic, params} = this.props;
        return(
            <div className={"login-header"}>
                {/*<PublicHeader />*/}

                <div className="login-header-wrap">
                    {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?
                    <Translate
                        className="login-header-title"
                        content={"public.welcome_to_deex_gbl"}
                        component="h2"
                    />:
                    <Translate
                        className="login-header-title"
                        content={__SCROOGE_CHAIN__ ? "public.welcome_to_deex_scrooge" : "public.welcome_to_haqq"}
                        component="h2"
                    />}

                    <p className="login-header-desc">
                        {translator.translate("public.please_login")}
                        &nbsp;
                        <Link className="public-link" to={"/create-account"}>
                            <Translate content="public.create_a_new_account" />
                        </Link>
                    </p>
                </div>
            </div>
        );
    }
}
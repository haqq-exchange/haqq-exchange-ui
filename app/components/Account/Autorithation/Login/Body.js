import React from "react";
import PropTypes from "prop-types";
import LoginHeader from "./Header";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";
import TranslateWithLinks from "Utility/TranslateWithLinks";
import * as RoutesLink from "RoutesLink";
import AccountStore from "stores/AccountStore";

class FooterLogin extends React.PureComponent{
    render(){
        return(
            <div className="public-gray-p-small">
                {__SCROOGE_CHAIN__ ? null :
                <>
                    <Link className={"public-link"} to="/authorization/password-2fa">
                        <Translate content="public.log_in_using_2fa" />
                    </Link>
                    &nbsp;
                    <Translate content="public.or" />
                </>}
                &nbsp;
                <Link className={"public-link"} to="/authorization/wallet">
                    <Translate content="public.login_using_local_wallet" />
                </Link>
            </div>
        );
    }
}
class FooterLogin2fa extends React.PureComponent {
    render(){

        return (
            <div className="public-gray-p-small">
                <Link className={"public-link"}  to="/authorization/password">
                    <Translate content="public.log_in_using_account" />
                </Link>
                &nbsp;
                <Translate content="public.or" />
                &nbsp;
                <Link className={"public-link"} to="/authorization/wallet">
                    <Translate content="public.login_using_local_wallet" />
                </Link>
            </div>
        );
    }
}
class FooterLoginLw extends React.PureComponent {
    render(){
        return(
            <div className="public-gray-p-small">
                <Link className={"public-link"}  to="/authorization/password">
                    <Translate content="public.log_in_using_account" />
                </Link>
                &nbsp;
                <Translate content="public.or" />
                &nbsp;
                <Link className={"public-link"}  to="/create-account/password">
                    <Translate content="public.create_an_account" />
                </Link>
            </div>
        );
    }
}
export default class LoginBody extends React.PureComponent {

    static propTypes = {
        route: PropTypes.object
    };

    static defaultProps = {
        contentStatic: {
            "password": {
                children: RoutesLink.LoginOldContainer,
                footer: FooterLogin
            },
            "password-2fa": {
                children: RoutesLink.Login2fa,
                footer: FooterLogin2fa
            },
            "wallet": {
                children: RoutesLink.LoginLW,
                footer: FooterLoginLw
            }
        }
    };

    render(){
        const {contentStatic, match} = this.props;
        
        const ComponentFooter = contentStatic[match.params.type].footer;
        let { passwordAccount } = AccountStore.getState();
        //debugger;

        if( passwordAccount ) {
            this.props.history.push("/");
        }

        return(
            <div className={"login-body"} >
                <LoginHeader {...match} />
                {/*<ComponentChildren {...this.props} />*/}
                {this.props.children}
                <ComponentFooter />
            </div>
        );
    }
}


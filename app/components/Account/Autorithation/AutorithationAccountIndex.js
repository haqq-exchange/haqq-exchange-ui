import React from "react";
import {connect} from "alt-react";
import cname from "classnames";
import AccountStore from "stores/AccountStore";
import {Link, withRouter} from "react-router-dom";
import Translate from "react-translate-component";
import "./autorithation.scss";

class AutorithationAccountIndex extends React.Component {

    static defaultProps = {
        btsModels: {
            "password": {
                "title": "public.InfoRegistration.models.password.title",
            },
            "wallet": {
                "title": "public.InfoRegistration.models.wallet.title",
            },
            "password-2fa": {
                "title": "public.InfoRegistration.models.password-2fa.title",
            },
        },
        models: {
            "password": {
                "title": "public.InfoRegistration.models.password.title",
            },
            "password-2fa": {
                "title": "public.InfoRegistration.models.password-2fa.title",
            },
        },
        scroogeModels: {
            "password": {
                "title": "public.InfoRegistration.models.password.title",
            }
        }
    };

    constructor() {
        super();
        this.state = {
            slug: null
        };
    }

    goRegister = () => {
        let url = ["/create-account", this.state.slug].join("/");
        this.props.history.push(url);
    };


    render() {
        const {models, wallet_name, passwordAccount, history, btsModels, scroogeModels} = this.props;
        const authModels = __SCROOGE_CHAIN__ ? scroogeModels : (__DEEX_CHAIN__  || __GBL_CHAIN__  || __GBLTN_CHAIN__ ? models : btsModels);
        // debugger;
        if( passwordAccount) {
            history.push("/");
        }
        return (
            <div className="authorization-account">
                <div className="authorization-account-title">
                    <Translate content={"public.login"} />
                </div>
                <div className={"authorization-account-desc"}>
                    <Translate unsafe component="div" content={"public.deex_p4"} />
                </div>
                <div className="authorization-account-wrap" style={{justifyContent: "space-evenly"}}>
                    {Object.keys(authModels).map(slug=> {
                        const model = authModels[slug];
                        return (
                            <Link key={"authorization-account-item-" + slug} to={["/authorization", slug].join("/")} className={cname("authorization-account-item")}>
                                <Translate
                                    className={cname("model-item-title", slug)}
                                    component={"span"}
                                    content={model.title} />
                            </Link>
                        );
                    })}
                </div>
            </div>
        );
    }
}

const AutorithationAccount = withRouter(AutorithationAccountIndex);

export default connect(AutorithationAccount, {
    listenTo() {
        return [AccountStore];
    },
    getProps() {
        return {
            currentAccount:
                AccountStore.getState().currentAccount ||
                AccountStore.getState().passwordAccount,
            wallet_name: AccountStore.getState().wallet_name
        };
    }
});

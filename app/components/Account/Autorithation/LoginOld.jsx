import React from "react";
import PropTypes from "prop-types";
import {Redirect} from "react-router-dom";
// import ZfApi from "react-foundation-apps/src/utils/foundation-api";
import LoadingIndicator from "Components/LoadingIndicator";
// import ZfApi from "react-foundation-apps/src/utils/foundation-api";
// import PasswordInput from "components/Forms/PasswordInput";
// import notify from "actions/NotificationActions";
import AltContainer from "alt-container";
import WalletDb from "stores/WalletDb";
import WalletUnlockStore from "stores/WalletUnlockStore";
import WalletManagerStore from "stores/WalletManagerStore";
import SettingsActions from "actions/SettingsActions";
import BackupStore from "stores/BackupStore";
import AccountStore from "stores/AccountStore";
import WalletUnlockActions from "actions/WalletUnlockActions";
import WalletActions from "actions/WalletActions";
// import BackupActions, {restore, backup} from "actions/BackupActions";
import AccountActions from "actions/AccountActions";
// import {Apis} from "deexjs-ws";
import utils from "common/utils";
import AccountName from "components/Public/AccountName";
// import {PrivateKey} from "deexjs";
// import {saveAs} from "file-saver";
// import LoginTypeSelector from "components/Wallet/LoginTypeSelector";
import counterpart from "counterpart";
import {DisableChromeAutocomplete} from "components/Wallet/WalletUnlockModalLib";
// import {backupName} from "common/backupUtils";
// import PublicHeader from "components/Layout/PublicHeader";
// import {Link} from "react-router-dom";
import Translate from "react-translate-component";
// import TranslateWithLinks from "components/Utility/TranslateWithLinks";
// import Form from "components/Public/Form";
import Field from "components/Public/Field";
// import LoginHeader from "./Login/Header";
// import BackupFile from "./BackupFile";
// import BackupFileChoose from "./BackupFileChoose";
import "./autorithation.scss";
import RefUtils from "common/ref_utils";
import {ymSend} from "../../../lib/common/metrika";
import {getRequestAddress} from "../../../api/apiConfig";

class LoginOld extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.public_name = "pNlz5L1VCuImnbDQV";
        this.refutils = new RefUtils({
            url: getRequestAddress("referral")
        });
        this.state = this.initialState(props);

        console.log("LoginOld constructor");
    }

    initialState = (props = this.props) => {
        const {passwordAccount, currentWallet} = props;
        SettingsActions.changeSetting({
            setting: "passwordLogin",
            value: "login"
        });
        return {
            passwordError: null,
            accountName: passwordAccount,
            walletSelected: !!currentWallet,
            customError: null,
            isOpen: false,
            loaded: false,
            restoringBackup: false,
            stopAskingForBackup: false
        };
    };

    componentDidCatch(error, info) {
        console.log("error, info", error, info);
    }

    UNSAFE_componentWillReceiveProps(np) {
        const {walletSelected, restoringBackup, accountName} = this.state;
        const {
            currentWallet: newCurrentWallet,
            passwordAccount: newPasswordAccount
        } = np;

        const newState = {};
        if (newPasswordAccount && !accountName)
            newState.accountName = newPasswordAccount;
        if (walletSelected && !restoringBackup && !newCurrentWallet)
            newState.walletSelected = false;
        if (this.props.passwordLogin !== np.passwordLogin) {
            newState.passwordError = false;
            newState.customError = null;
        }

        this.setState(newState);
    }

    shouldComponentUpdate(np, ns) {
        return (
            !utils.are_equal_shallow(np, this.props) ||
            !utils.are_equal_shallow(ns, this.state)
        );
    }

    componentWillUnmount() {
        console.log("LoginOld componentWillUnmount");
    }

    componentDidMount() {
        const {modalId, passwordLogin} = this.props;

        console.log("LoginOld componentDidMount", modalId, passwordLogin);
        if( modalId ) {

            if (passwordLogin === "login") {
                const {accountName} = this.state;
                if (accountName && this.ref_password_input) {
                    //password_input.focus();
                } else if (
                    this.ref_account_input &&
                    typeof this.ref_account_input.focus === "function"
                ) {
                    this.ref_account_input.focus();
                }
            }
        }
    }


    validate = (password, account) => {
        //debugger;
        const {resolve} = this.props;
        const wallet = WalletDb.getWallet();

        const promiseLogin = (result) => {
            console.warn("VALIDATEPASSWORD RESULT", result);

            return new Promise((res, reject)=>{
                if( result.success ) {
                    console.log("AccountActions.setPasswordAccount")
                    AccountActions.setPasswordAccount(account);
                    //AccountActions.setCurrentAccount(account);
                    WalletUnlockActions.change();
                    WalletUnlockActions.cancel();
                    ymSend("reachGoal", "ENTER");
                    // this.refutils.getUserReferral(
                    //     account,
                    //     AccountStore.getState().referralAccount
                    // );
                    __GBL_CHAIN__ || __GBLTN_CHAIN__ ? this.props.history.push(`/account/${this.props.wallet_name}`) : null;
                    res();
                } else {
                    reject();
                }
            });
        };

        // debugger;
        console.warn("wallet.public_name !== account ", wallet , account )
        if( !wallet || wallet.public_name !== account ) {
            WalletDb.setWallet(null);
            WalletActions.setWallet(account, password)
                .then(()=>WalletDb.isLocked())
                .then(()=>{
                    return WalletDb.validatePassword(password, true, account);
                })
                .then(promiseLogin)
                .catch(()=>{
                    this.setState({
                        passwordError: true,
                        loaded: false
                    });
                });
        } else {
            WalletDb.validatePassword(password, true, account)
                .then(promiseLogin)
                .catch(()=>{
                    this.setState({
                        passwordError: true,
                        loaded: false
                    });
                });
        }

        if (resolve) {
            resolve();
        }

    };

    passwordInput = () =>
        this.ref_password_input.refs.ref_password_input ||
        this.ref_password_input;

    handleLogin = e => {
        if (e) e.preventDefault();
        // debugger;
        const _this = this;
        const {accountName} = this.state;
        const wallet = WalletDb.getWallet();
        console.log("wallet", wallet)
        const loginValidate = () => {
            _this.setState({
                passwordError: null
            }, () => {
                //debugger;
                const password_input = _this.passwordInput();
                const password = password_input.value;
                console.log("password, accountName", password, accountName);
                _this.validate(password, accountName);
                console.log("ready");
            });
        };
        //debugger;
        _this.setState({
            loaded: true,
        }, loginValidate);

    };

    handleAccountNameChange = accountName =>
        this.setState({accountName, error: null});

    shouldShowBackupWarning = () =>
        !this.props.passwordLogin &&
        this.state.walletSelected &&
        !this.state.restoringBackup &&
        !(!!this.props.dbWallet && !!this.props.dbWallet.backup_date);

    shouldUseBackupLogin = () =>
        this.shouldShowBackupWarning() && !this.state.stopAskingForBackup;

    render() {

        const {
            walletSelected,
            passwordError,
            customError,
            accountName,
            stopAskingForBackup,
            loaded
        } = this.state;

        const errorMessage = passwordError
            ? counterpart.translate("wallet.pass_incorrect")
            : customError;

            console.log("PROPS", this.props)

        return (
            // U N L O C K
            <div className="password-form">

                <form onSubmit={this.handleLogin} className="full-width">
                    <DisableChromeAutocomplete />
                    <AccountName
                        allowUppercase={true}
                        label="account.name"
                        ref={(ref)=>this.ref_account_input=ref}
                        accountName={accountName}
                        account={accountName}
                        onChange={this.handleAccountNameChange}
                        onAccountChanged={() => {}}
                        size={60}
                        hideImage
                        placeholder=" "
                        useHR
                        labelClass="login-label"
                        reserveErrorSpace
                        onEnter={this.handleLogin}
                    />
                    <Field
                        type="password"
                        ref={(ref)=>this.ref_password_input=ref}
                        inputRef="ref_password_input"
                        name="password"
                        id="password"
                        error={errorMessage}
                        placeholder={counterpart.translate(
                            "settings.password"
                        )}
                        onEnter={this.handleLogin}
                    />


                    <div className="public-buttons" style={{marginTop: 20}}>
                        <button
                            type="button"
                            disabled={loaded}
                            onClick={this.handleLogin}
                            className="btn btn-green"
                        >

                            {loaded ?
                                <LoadingIndicator type={"three-bounce"} />:
                                <Translate
                                    content={
                                        this.shouldUseBackupLogin()
                                            ? "wallet.backup_login"
                                            : "header.unlock_short"
                                    }
                                />
                            }
                        </button>



                    </div>
                </form>
            </div>
        );
    }
}

class LoginOldContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[
                    WalletUnlockStore,
                    AccountStore,
                    WalletManagerStore,
                    WalletDb,
                    BackupStore
                ]}
                inject={{
                    currentWallet: () =>
                        WalletManagerStore.getState().current_wallet,
                    walletNames: () =>
                        WalletManagerStore.getState().wallet_names,
                    dbWallet: () => WalletDb.getWallet(),
                    isLocked: () => WalletDb.isLocked(),
                    backup: () => BackupStore.getState(),
                    resolve: () => WalletUnlockStore.getState().resolve,
                    reject: () => WalletUnlockStore.getState().reject,
                    locked: () => WalletUnlockStore.getState().locked,
                    passwordLogin: () =>
                        WalletUnlockStore.getState().passwordLogin,
                    passwordAccount: () =>
                        AccountStore.getState().passwordAccount || ""
                }}
            >
                <LoginOld {...this.props} />
            </AltContainer>
        );
    }
}
export default LoginOldContainer;

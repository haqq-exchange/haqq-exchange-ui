import React from "react";
import {connect} from "alt-react";
import AccountStore from "stores/AccountStore";
import WalletUnlockStore from "stores/WalletUnlockStore";
//import Translate from "react-translate-component";
//import TranslateWithLinks from "./Utility/TranslateWithLinks";
//import {isIncognito} from "feature_detect";
//var logo = require("assets/logo-ico-blue.png");
//import SettingsActions from "actions/SettingsActions";
//import WalletUnlockActions from "actions/WalletUnlockActions";
//import ActionSheet from "react-foundation-apps/src/action-sheet";
//import SettingsStore from "stores/SettingsStore";
//import IntlActions from "actions/IntlActions";
import {Redirect, Route, Switch} from "react-router-dom";
import loadable from "loadable-components";

//export const LoginSelector = loadable(() => import("components/LoginSelector"));
export const LoginBody = loadable(() => import("./Login/Body"));
export const LoginOldContainer = loadable(() => import("./LoginOld"));
export const Login2fa = loadable(() => import("./Login2fa"));
export const LoginLW = loadable(() => import("./LoginLW"));

import "./autorithation.scss";

class Authorization extends React.Component {
    constructor(props) {
        super(props);

        console.log("Authorization constructor");
    }

    componentDidUpdate() {
    }



    render() {
        const {wallet_name, currentAccount, history, isCanceled } = this.props;

        //debugger;

        console.warn("wallet_name, currentAccount, history, isCanceled", wallet_name, currentAccount, isCanceled)

        if( isCanceled ) {
            //history.go("/");
            return (
                <Redirect from={"/authorization/"} to={"/"}/>
            );
        }
        
        //console.log("wallet_name, currentAccount, history", wallet_name, currentAccount, history, isCanceled);
        
        return (
            <div className="autorithation-page">
                <LoginBody {...this.props}>
                    <Switch>
                        <Route
                            path={"/authorization/password-2fa"}
                            exact
                            render={() => <Login2fa {...this.props} />}/>


                        <Route
                            path={"/authorization/wallet"}
                            exact
                            render={() => <LoginLW {...this.props} passwordLogin={false} />}
                        />
                        <Route
                            path={"/authorization/password"}
                            exact
                            render={() => <LoginOldContainer {...this.props} />}
                        />
                    </Switch>
                </LoginBody>
            </div>
        );
    }
}

export default connect(
    Authorization,
    {
        listenTo() {
            return [AccountStore, WalletUnlockStore];
        },
        getProps() {
            console.log("WalletUnlockStore.getState()", WalletUnlockStore.getState())
            return {
                currentAccount:
                    AccountStore.getState().currentAccount ||
                    AccountStore.getState().passwordAccount,
                wallet_name: AccountStore.getState().wallet_name,
                isCanceled: WalletUnlockStore.getState().isCanceled,
            };
        }
    }
);

import React from "react";
import PropTypes from "prop-types";
import ZfApi from "react-foundation-apps/src/utils/foundation-api";
import AltContainer from "alt-container";
import WalletDb from "stores/WalletDb";
import WalletUnlockStore from "stores/WalletUnlockStore";
import WalletManagerStore from "stores/WalletManagerStore";
import SettingsActions from "actions/SettingsActions";
import BackupStore from "stores/BackupStore";
import AccountStore from "stores/AccountStore";
import WalletUnlockActions from "actions/WalletUnlockActions";
import WalletActions from "actions/WalletActions";
import BackupActions, {restore, backup} from "actions/BackupActions";
import AccountActions from "actions/AccountActions";
import utils from "common/utils";
import {PrivateKey} from "deexjs";
import {saveAs} from "file-saver";
// import LoginTypeSelector from "components/Wallet/LoginTypeSelector";
import counterpart from "counterpart";
import {
    WalletSelector,
    BackupWarning
} from "components/Wallet/WalletUnlockModalLib";
import {backupName} from "common/backupUtils";
// import PublicHeader from "components/Layout/PublicHeader";
// import {Link} from "react-router-dom";
import Translate from "react-translate-component";
// import TranslateWithLinks from "components/Utility/TranslateWithLinks";
// import Form from "components/Public/Form";
import Field from "components/Public/Field";
import BackupFile from "components/Public/BackupFile";
import BackupFileChoose from "components/Public/BackupFileChoose";
// import LoginHeader from "./Login/Header";
import RefUtils from "common/ref_utils";
import {ymSend} from "../../../lib/common/metrika";
import {getRequestAddress} from "../../../api/apiConfig";

class LoginLW extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor(props) {
        super(props);
        this.state = this.initialState(props);
        this.refutils = new RefUtils({
            url: getRequestAddress("referral")
        });
    }

    initialState = (props = this.props) => {
        const {passwordAccount, binWallet} = props;
        SettingsActions.changeSetting({
            setting: "passwordLogin",
            value: "loginWallet"
        });
        console.log("currentWallet", binWallet);

        return {
            passwordError: null,
            accountName: passwordAccount,
            walletSelected: !!binWallet,
            customError: null,
            isOpen: false,
            restoringBackup: false,
            stopAskingForBackup: false
        };

    };

    UNSAFE_componentWillReceiveProps(np) {
        const {walletSelected, restoringBackup, accountName} = this.state;
        const {
            currentWallet: newCurrentWallet,
            passwordAccount: newPasswordAccount
        } = np;

        const newState = {};
        if (newPasswordAccount && !accountName)
            newState.accountName = newPasswordAccount;
        if (walletSelected && !restoringBackup && !newCurrentWallet)
            newState.walletSelected = false;
        if (this.props.passwordLogin !== np.passwordLogin) {
            newState.passwordError = false;
            newState.customError = null;
        }

        this.setState(newState);
    }

    shouldComponentUpdate(np, ns) {
        return (
            !utils.are_equal_shallow(np, this.props) ||
            !utils.are_equal_shallow(ns, this.state)
        );
    }

    componentDidMount() {
        //const {passwordAccount, binWallet} = props;
        const {currentWallet, binWallet} = this.props;
        SettingsActions.changeSetting({
            setting: "passwordLogin",
            value: "loginWallet"
        });

        console.log("componentDidMount currentWallet !== binWallet", currentWallet ,  binWallet);

        if( currentWallet !== binWallet ) {
            WalletActions.setWallet(binWallet);
        }

        /*ZfApi.subscribe(modalId, (name, msg) => {
            const {isOpen} = this.state;

            if (name !== modalId) return;
        });*/

    }

    componentDidUpdate() {
        const {resolve, modalId, isLocked} = this.props;

        if (resolve)
            if (isLocked) {
                ZfApi.publish(modalId, "open");
            } else {
                resolve();
            }
        else ZfApi.publish(this.props.modalId, "close");
    }

    validate = (password, account) => {
        const {resolve} = this.props;
        const {stopAskingForBackup} = this.state;

        WalletDb.validatePassword(
            password || "",
            true //unlock
        ).then(({wallet, success})=>{
            console.log("result , success", success, wallet, WalletDb.isLocked() );
            console.log("result , success this.props", this.props);
            if (WalletDb.isLocked()) {
                this.setState({passwordError: true});
            } else {
                let currentAccount = account || wallet.public_name;
                //debugger;
                //AccountActions.setPasswordAccount(wallet.public_name);
                WalletUnlockActions.change();
                if (stopAskingForBackup) {
                    WalletActions.setBackupDate();
                } else if (this.shouldUseBackupLogin()) {
                    // this.backup();
                }
                if (resolve) {
                    resolve();
                }

                this.refutils.getUserReferral(
                    currentAccount,
                    AccountStore.getState().referralAccount
                );
                AccountActions.setPasswordAccount(currentAccount);
                WalletUnlockActions.change();
                WalletUnlockActions.cancel();
                ymSend("reachGoal", "ENTER");
            }
        });

    };

    passwordInput = () =>
        this.ref_password_input.refs.ref_password_input ||
        this.ref_password_input;

    restoreBackup = (password, callback) => {
        const {backup} = this.props;
        const privateKey = PrivateKey.fromSeed(password || "");
        console.log("backup", backup);
        const walletName = backup.name.split(".")[0];
        console.log("walletName", walletName)
        restore(privateKey.toWif(), backup.contents, walletName, password)
            .then(({wallet_name, wallet_object}) => {
                console.log("wallet_name, wallet_object", wallet_name, wallet_object)
                return WalletActions.setWallet(walletName)
                    .then(() => {
                        BackupActions.reset();
                        callback(wallet_name, wallet_object);
                    })
                    .catch(e => this.setState({customError: e.message}));
            })
            .catch(e => {
                const message = typeof e === "string" ? e : e.message;
                const invalidBackupPassword =
                    message === "invalid_decryption_key";
                this.setState({
                    customError: invalidBackupPassword ? null : message,
                    passwordError: invalidBackupPassword
                });
            });
    };

    handleLogin = e => {
        const _this = this;
        if (e) e.preventDefault();
        // debugger;
        const {backup} = this.props;
        const {walletSelected, accountName} = this.state;

        // console.log('LoginOld walletSelected, accountName', walletSelected, accountName, this.props, AccountStore.getState());
        console.log('LoginOld passwordLogin, backup', backup, walletSelected, accountName);

        if (!walletSelected) {
            this.setState({
                customError: counterpart.translate(
                    "wallet.ask_to_select_wallet"
                )
            });
        } else {
            this.setState({passwordError: null}, () => {
                const password_input = this.passwordInput();
                console.log('LoginOld password_input', password_input, backup.name )
                const password = password_input.value;
                console.log('LoginOld password', password);
                if (backup.name) {
                    // console.log('LoginOld accountName', accountName);
                    this.restoreBackup(password, (wallet_name, wallet_object) => {
                        console.log("LoginOld wallet_name, wallet_object", wallet_name, wallet_object)
                        _this.validate(password, wallet_object.linked_accounts[0].name);
                    });
                } else {
                    this.validate(password, accountName);
                }
            });
        }
    };

    closeRedirect = path => {
        WalletUnlockActions.cancel();
        this.props.history.push(path);
    };

    handleCreateWallet = () => this.closeRedirect("/create-account/wallet");

    handleRestoreOther = () => this.closeRedirect("/settings/restore");

    loadBackup = e => {
        const fullPath = e.target.value;
        const file = e.target.files[0];

        this.setState({restoringBackup: true}, () => {
            const startIndex =
                fullPath.indexOf("\\") >= 0
                    ? fullPath.lastIndexOf("\\")
                    : fullPath.lastIndexOf("/");
            let filename = fullPath.substring(startIndex);
            if (filename.indexOf("\\") === 0 || filename.indexOf("/") === 0) {
                filename = filename.substring(1);
            }
            console.log("file", file);
            console.log("file", file.name);
            BackupActions.incommingWebFile(file);
            this.setState({
                walletSelected: true
            });
        });
    };

    handleSelectedWalletChange = e => {
        const {value} = e.target;
        const selectionType = value.split(".")[0];
        const walletName = value.substring(value.indexOf(".") + 1);

        console.log("walletName", walletName);

        BackupActions.reset();
        if (selectionType === "upload")
            this.setState({
                restoringBackup: true,
                customError: null
            });
        else
            WalletActions.setWallet(walletName).then(() =>
                this.setState({
                    walletSelected: true,
                    customError: null,
                    restoringBackup: false
                })
            );
    };

    backup = () =>
        backup(this.props.dbWallet.password_pubkey).then(contents => {
            const {binWallet} = this.props;
            const name = backupName(binWallet);
            BackupActions.incommingBuffer({name, contents});

            const {backup} = this.props;
            let blob = new Blob([backup.contents], {
                type: "application/octet-stream; charset=us-ascii"
            });
            if (blob.size !== backup.size)
                throw new Error("Invalid backup to download conversion");
            saveAs(blob, name);
            WalletActions.setBackupDate();
            BackupActions.reset();
        });

    handleAskForBackupChange = e =>
        this.setState({stopAskingForBackup: e.target.checked});

    handleUseOtherWallet = () => {
        this.setState({
            walletSelected: false,
            restoringBackup: false,
            passwordError: null,
            customError: null
        });
    };

    handleAccountNameChange = accountName =>
        this.setState({accountName, error: null});

    shouldShowBackupWarning = () =>
        !this.props.passwordLogin &&
        this.state.walletSelected &&
        !this.state.restoringBackup &&
        !(!!this.props.dbWallet && !!this.props.dbWallet.backup_date);

    shouldUseBackupLogin = () =>
        this.shouldShowBackupWarning() && !this.state.stopAskingForBackup;

    render() {
        let {
            myActiveAccounts,
            myHiddenAccounts,
            passwordAccount
        } = AccountStore.getState();

        // console.log('myActiveAccounts', myActiveAccounts);
        // console.log('passwordAccount', passwordAccount);

        let accountCount =
            myActiveAccounts.size +
            myHiddenAccounts.size +
            (passwordAccount ? 1 : 0);

        if (accountCount) {
           // this.props.history.push("/");
        }

        const {
            backup,
            binWallet,
            walletNames
        } = this.props;
        const {
            walletSelected,
            restoringBackup,
            passwordError,
            customError,
            stopAskingForBackup
        } = this.state;

        const noWalletNames = !(walletNames.size > 0);
        const walletDisplayName = backup.name || binWallet;
        const walletDisplaySize = backup.size || 0;

        const errorMessage = passwordError
            ? counterpart.translate("wallet.pass_incorrect")
            : customError;

        console.log(walletSelected,
            restoringBackup,
            passwordError,
            customError,
            stopAskingForBackup);
        console.log("binWallet", binWallet);



        return (
            // U N L O C K
            <div className="password-form">
                <form onSubmit={this.handleLogin} className="full-width">
                    <div className={
                            "key-file-selector " +
                                (restoringBackup && !walletSelected
                                    ? "restoring"
                                    : "")
                        } >
                        {walletSelected ? (
                            <BackupFileChoose
                                name={walletDisplayName}
                                size={walletDisplaySize}
                                onUseOtherWallet={
                                    this.handleUseOtherWallet
                                }
                            />
                        ) : (
                            restoringBackup || noWalletNames || !binWallet
                                ? (
                                    <BackupFile
                                        onFileChosen={this.loadBackup}
                                        onRestoreOther={this.handleRestoreOther}
                                    />
                                ) : (
                                    <WalletSelector
                                        onFileChosen={
                                            this.loadBackup
                                        }
                                        restoringBackup={
                                            restoringBackup
                                        }
                                        walletNames={
                                            [binWallet]
                                        }
                                        onWalletChange={
                                            this
                                                .handleSelectedWalletChange
                                        }
                                    />
                                )
                        )}
                    </div>

                    <Field
                        type="password"
                        ref={(ref)=>this.ref_password_input=ref}
                        inputRef="ref_password_input"
                        name="password"
                        id="password"
                        error={errorMessage}
                        placeholder={counterpart.translate(
                            "settings.password"
                        )}
                    />
                    {this.shouldShowBackupWarning() && (
                        <BackupWarning
                            onChange={this.handleAskForBackupChange}
                            checked={stopAskingForBackup}
                        />
                    )}
                    <div className="public-buttons" style={{marginTop: 20}}>
                        <button
                            type="button"
                            onClick={this.handleLogin}
                            className="btn btn-green">
                            <Translate
                                content={
                                    this.shouldUseBackupLogin()
                                        ? "wallet.backup_login"
                                        : "header.unlock_short"
                                }
                            />
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}

class LoginLWContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[
                    WalletUnlockStore,
                    AccountStore,
                    WalletManagerStore,
                    WalletDb,
                    BackupStore
                ]}
                inject={{
                    currentWallet: () => WalletManagerStore.getState().current_wallet,
                    walletNames: () => WalletManagerStore.getState().wallet_names,
                    binWallet: () => WalletManagerStore.getState().bin_wallet,
                    dbWallet: () => WalletDb.getWallet(),
                    isLocked: () => WalletDb.isLocked(),
                    backup: () => BackupStore.getState(),
                    resolve: () => WalletUnlockStore.getState().resolve,
                    reject: () => WalletUnlockStore.getState().reject,
                    locked: () => WalletUnlockStore.getState().locked,
                    passwordAccount: () => AccountStore.getState().passwordAccount || ""
                }}
            >
                <LoginLW {...this.props} />
            </AltContainer>
        );
    }
}
export default LoginLWContainer;

import React from "react";
import Translate from "react-translate-component";
import ChainTypes from "../Utility/ChainTypes";
import BindToChainState from "../Utility/BindToChainState";
import PropTypes from "prop-types";

class AccountPermissionsWitness extends React.Component {
    static propTypes = {
        account: ChainTypes.ChainAccount.isRequired,
        label: PropTypes.string.isRequired, // a translation key for the label,
        witness: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
    }

    render() {
        const {witness} = this.props;

        return (
            <div>
                <table className="table">
                    <thead>
                        <tr>
                            <th >
                                <Translate content="account.perm.acct_or_key" style={{fontSize: "16px"}}/>
                            </th>
                        </tr>
                        <tr>
                            <th style={{border: "none"}}>
                                <span style={{fontSize: "16px"}}>{witness.get("signing_key")}</span>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        );
    }
}

export default BindToChainState(AccountPermissionsWitness);


import React from "react";
import utils from "common/utils";
import PriceText from "Components/Utility/PriceText";
import counterpart from "counterpart";
import Icon from "Components/Icon/Icon";
import Translate from "react-translate-component";
import AssetName from "Components/Utility/AssetName";
import FormattedPrice from "Components/Utility/FormattedPrice";
import {MarketPrice} from "Components/Utility/MarketPrice";
import {EquivalentValueComponent} from "Components/Utility/EquivalentValueComponent";
import {Link} from "react-router-dom";

export default class OrderItemRow extends React.Component {
    shouldComponentUpdate(nextProps) {
        return (
            nextProps.order.for_sale !== this.props.order.for_sale ||
            nextProps.order.id !== this.props.order.id ||
            nextProps.quote !== this.props.quote ||
            nextProps.base !== this.props.base ||
            nextProps.order.market_base !== this.props.order.market_base
        );
    }

    render() {
        let {
            base,
            quote,
            order,
            isMyAccount,
            settings
        } = this.props;
        const isBid = order.isBid();
        const isCall = order.isCall();


        // let preferredUnit = settings ? settings.get("unit") : "1.3.0";
        // let quoteColor = !isBid ? "value negative" : "value positive";
        // let baseColor = isBid ? "value negative" : "value positive";

        console.log('order', this.props );

        let formatBuy = utils.format_number(
            order[isBid ? "amountToReceive" : "amountForSale"]().getAmount({real: true}),
            base.get("precision"),
            false
        );

        let formatSell = utils.format_number(
            order[isBid ? "amountForSale" : "amountToReceive"]().getAmount({real: true}),
            quote.get("precision"),
            false
        );

        let timeOptions = {
            year: "numeric", month: "numeric", day: "numeric",
            hour: "numeric", minute: "numeric", second: "numeric",
            hour12: false
        };
        let expiration = new Intl
            .DateTimeFormat(settings.get("locale"), timeOptions)
            .format(new Date(order.expiration));

        let cancel = counterpart.translate("account.perm.cancel");

        return (
            <div key={order.id} className={"account-orders-tbody"}>
                <div className={"account-orders-tbody-tr"}>
                    {__SCROOGE_CHAIN__ ? null :
                    <div className={"account-orders-tbody-td flip"} onClick={()=>this.props.onFlip()}>
                        <Icon name={"shuffle"} />
                    </div>}
                    <div className={"account-orders-tbody-td sell"} >
                        {formatSell}{" "}<AssetName noTip name={base.get("symbol")}/>
                    </div>
                    <div className={"account-orders-tbody-td buy"} >
                        {formatBuy}{" "}<AssetName noTip name={quote.get("symbol")}/>
                    </div>
                    <div className={"account-orders-tbody-td price"} >
                        <FormattedPrice
                            base_amount={order.sellPrice().base.amount}
                            base_asset={order.sellPrice().base.asset_id}
                            quote_amount={order.sellPrice().quote.amount}
                            quote_asset={order.sellPrice().quote.asset_id}
                            force_direction={base.get("symbol")}
                            hide_symbols1
                            noPopOver
                            noInvert
                        />
                    </div>
                    <div className={"account-orders-tbody-td course"} >
                        <MarketPrice
                            base={base.get("id")}
                            quote={quote.get("id")}
                            force_direction={base.get("symbol")}
                            hide_symbols
                            hide_asset
                        />
                    </div>
                    <div className={"account-orders-tbody-td exchange"} >
                        <Link to={`/market/${quote.get("symbol")}_${base.get("symbol")}`}>
                            <Icon name="trade" className="icon-14px" />
                        </Link>
                    </div>
                    <div className={"account-orders-tbody-td expiration"} >
                        {expiration}
                    </div>
                    {isMyAccount ? <div className={"account-orders-tbody-td close"} >
                        <span onClick={()=>this.props.onCancel(order.id)}>{cancel} <i/></span>
                    </div> : null}
                </div>
            </div>
        );
    }
}

OrderItemRow.defaultProps = {
    showSymbols: false
};

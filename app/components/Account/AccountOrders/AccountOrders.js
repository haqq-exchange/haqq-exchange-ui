import React from "react";
import counterpart from "counterpart";
import MarketsActions from "actions/MarketsActions";
import {ChainStore, FetchChain} from "deexjs";
import {LimitOrder} from "common/MarketClasses";
import {connect} from "alt-react";
import SettingsStore from "stores/SettingsStore";
import MarketsStore from "stores/MarketsStore";
import SettingsActions from "actions/SettingsActions";
import marketUtils from "common/market_utils";
import Translate from "react-translate-component";
import BindToChainState from "Components/Utility/BindToChainState";
import AssetWrapper from "Components/Utility/AssetWrapper";
import OrderItemRow from "./OrderItemRow";

import "./AccountOrders.scss";
import WalletDb from "../../../stores/WalletDb";
import {Redirect} from "react-router-dom";

class AccountOrders extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            marketOrders: {},
            selectedOrders: [],
            filterValue: ""
        };
    }

    componentDidMount() {
        const _this = this;
        this._getMarketOrders();

        let cancelHeader = document.getElementById("cancelAllOrders");

        if (cancelHeader) {
            cancelHeader.addEventListener(
                "click",
                function() {
                    let orders = _this._getFilteredOrders.call(this);
                    orders = orders.toJS ? orders.toJS() : orders;

                    this.setState({selectedOrders: orders});

                    let checkboxes = document.querySelectorAll(".orderCancel");

                    checkboxes.forEach(item => {
                        if (!item.checked) item.checked = true;
                    });
                }.bind(this)
            );
        }
    }

    componentDidUpdate(nextProps){
        //console.log("prevProps", prevProps, this.props);
        let prevPropsOrders = nextProps.account.get("orders");
        let orders = this.props.account.get("orders");
        // console.log("componentDidUpdate prevPropsOrders", prevPropsOrders);
        // console.log("componentDidUpdate orders", orders);
        // console.log("componentDidUpdate is true", !nextProps.marketDirections.equals(this.props.marketDirections) || orders.size !== prevPropsOrders.size);
        if(!nextProps.marketDirections.equals(this.props.marketDirections) || orders.size !== prevPropsOrders.size){
            this._getMarketOrders();
        }
    }
    
    _getMarketOrders = () => {
        let {account, asset, marketDirections} = this.props;
        let cancel = counterpart.translate("account.perm.cancel");
        let markets = {};
        let orders = account.get("orders");

        let marketOrders = {};
        console.log("orders _getMarketOrders", !orders.size)
        if (!orders.size) {
            this.setState({marketOrders: {} });
            return null;
        }


        orders.forEach(orderID => {
            let order = ChainStore.getObject(orderID).toJS();
            FetchChain("getAsset", [order.sell_price.base.asset_id, order.sell_price.quote.asset_id])
                .then(result=>{
                    console.log("result", result);
                    let base = result.get(0);
                    let quote = result.get(1);

                    if( base.get("id") === asset.get("id") || quote.get("id") === asset.get("id") ) {

                        let assets = {
                            [base.get("id")]: {precision: base.get("precision")},
                            [quote.get("id")]: {precision: quote.get("precision")}
                        };
                        // let baseID = parseInt(order.sell_price.base.asset_id.split(".")[2], 10);
                        // let quoteID = parseInt(order.sell_price.quote.asset_id.split(".")[2], 10);

                        // let marketName = quoteID > baseID ? `${quote.get("symbol")}_${base.get("symbol")}` : `${base.get("symbol")}_${quote.get("symbol")}`;
                        const {marketName} = marketUtils.getMarketName(base, quote);
                        const direction = marketDirections.get(marketName);

                        if (!markets[marketName]) {
                            if (direction) {
                                markets[marketName] = {
                                    base: {
                                        id: base.get("id"),
                                        symbol: base.get("symbol"),
                                        precision: base.get("precision")
                                    },
                                    quote: {
                                        id: quote.get("id"),
                                        symbol: quote.get("symbol"),
                                        precision: quote.get("precision")
                                    }
                                };
                            } else {
                                markets[marketName] = {
                                    base: {
                                        id: quote.get("id"),
                                        symbol: quote.get("symbol"),
                                        precision: quote.get("precision")
                                    },
                                    quote: {
                                        id: base.get("id"),
                                        symbol: base.get("symbol"),
                                        precision: base.get("precision")
                                    }
                                };
                            }
                        }
                        let limitOrder = new LimitOrder(
                            order,
                            assets,
                            markets[marketName].quote.id
                        );

                        let marketBase = ChainStore.getAsset(
                            markets[marketName].base.id
                        );
                        let marketQuote = ChainStore.getAsset(
                            markets[marketName].quote.id
                        );

                        if (!marketOrders[marketName]) {
                            marketOrders[marketName] = [];
                        }

                        marketOrders[marketName].push(
                            <OrderItemRow
                                order={limitOrder}
                                base={marketBase}
                                quote={marketQuote}
                                key={order.id}
                                isMyAccount={this.props.isMyAccount}
                                settings={this.props.settings}
                                onFlip={()=>this.onFlip(marketName)}
                                onCancel={()=>this._cancelLimitOrders(order.id)}
                            />
                        );
                    }

                    return marketOrders;
                }).then(marketOrders=>this.setState({marketOrders}));

        });
    };

    _getFilteredOrders() {
        let {asset, account} = this.props;
        let {filterValue} = this.state;

        let orders = account.get("orders") || [];

        let findAsset = asset && asset.get("symbol") || filterValue;

        return orders.filter(item => {
            let order = ChainStore.getObject(item).toJS();
            let base = ChainStore.getAsset(order.sell_price.base.asset_id);
            let quote = ChainStore.getAsset(order.sell_price.quote.asset_id);

            let baseSymbol = base.get("symbol").toLowerCase();
            let quoteSymbol = quote.get("symbol").toLowerCase();

            return (
                baseSymbol.indexOf(findAsset) > -1 ||
                quoteSymbol.indexOf(findAsset) > -1
            );
        });
    }

    _cancelLimitOrder(orderID, e) {
        e.preventDefault();

        MarketsActions.cancelLimitOrder(
            this.props.account.get("id"),
            orderID, // order id to cancel
            false // Don't show normal confirms
        ).catch(err => {
            console.log("cancel order error:", err);
        });
    }

    _cancelLimitOrders(orderId) {
        MarketsActions
            .cancelLimitOrder(this.props.account.get("id"), orderId)
            .then(() => {
                this.resetSelected();
            })
            .catch(err => {
                console.log("cancel orders error:", err);
            });
    }

    onFlip(marketId) {
        let setting = {};
        console.log("onFlip");
        setting[marketId] = !this.props.marketDirections.get(marketId);
        SettingsActions.changeMarketDirection(setting);
    }

    onCheckCancel(orderId, evt) {
        let {selectedOrders} = this.state;
        let checked = evt.target.checked;

        if (checked) {
            this.setState({selectedOrders: selectedOrders.concat([orderId])});
        } else {
            let index = selectedOrders.indexOf(orderId);

            if (index > -1) {
                this.setState({
                    selectedOrders: selectedOrders
                        .slice(0, index)
                        .concat(selectedOrders.slice(index + 1))
                });
            }
        }
    }

    setFilterValue(evt) {
        this.setState({filterValue: evt.target.value.toLowerCase()});
    }

    resetSelected() {
        this.setState({selectedOrders: []});

        let checkboxes = document.querySelectorAll(".orderCancel");

        checkboxes.forEach(item => {
            if (item.checked) item.checked = false;
        });
    }

    cancelSelected() {
        this._cancelLimitOrders.call(this);
    }



    render() {
        let {account, account_name, asset} = this.props;
        let {marketOrders} = this.state;
        
        console.log("this.orders", account.get("orders"), marketOrders, !account.get("orders").size && Object.keys(marketOrders).length === 0);

        if (!account.get("orders").size && Object.keys(marketOrders).length === 0) {
            return (
                <Redirect from={`/account/${account_name}/orders/${asset.get("symbol")}`} to={`/account/${account_name}`}/>
            );
        }


        let tables = [];

        for (let market in marketOrders) {
            if( marketOrders.hasOwnProperty(market) ) {
                if (marketOrders[market].length) {
                    tables.push(marketOrders[market]);
                }
            }
        }


        return(
            <div className={"account-orders"}>
                <Translate content={"account.open_orders"} className="account-orders-title" component={"div"} />
                <div className="account-orders-table">
                    <TableHeader
                        settings={this.props.settings}
                        isMyAccount={this.props.isMyAccount}
                    />
                    {tables}
                </div>
            </div>
        );
    }
}

class TableHeader extends React.PureComponent {

    styleAlign = (type) => {
        return {
            textAlign: type
        };
    };

    render(){
        const {isMyAccount} = this.props;
        return (
            <div className={"account-orders-thead"}>
                <div className={"account-orders-thead-tr"}>
                    {__SCROOGE_CHAIN__ ? null :
                    <div className={"account-orders-thead-th flip"} >
                    </div>}
                    <div className={"account-orders-thead-th sell"} >
                        <Translate content="orders.sell" />
                    </div>
                    <div className={"account-orders-thead-th buy"} >
                        <Translate content="orders.buy" />
                    </div>
                    <div className={"account-orders-thead-th price"} >
                        <Translate content="orders.price" />
                    </div>
                    <div className={"account-orders-thead-th course"} >
                        <Translate content="orders.course" />
                    </div>
                    <div className={"account-orders-thead-th exchange"}>
                        <Translate content="orders.exchange" />
                    </div>
                    <div className={"account-orders-thead-th expiration"}>
                        <Translate content="orders.expiration" />
                    </div>
                    {isMyAccount ? (<div className={"account-orders-thead-th close"} />) : null}
                </div>
            </div>
        );
    }
}



AccountOrders = connect(AccountOrders, {
    listenTo() {
        return [SettingsStore, MarketsStore];
    },
    getProps() {
        return {
            marketDirections: SettingsStore.getState().marketDirections,
            marketLimitOrders: () => {
                return MarketsStore.getState().marketLimitOrders;
            }
        };
    }
});
AccountOrders = BindToChainState(AccountOrders);
AccountOrders = AssetWrapper(AccountOrders, {
    propNames: ["asset", "core"],
    defaultProps: {
        core: "1.3.0"
    },
    withDynamic: true
});
class AccountOrdersWrapper extends React.Component {
    render() {
        let asset = this.props.match.params.asset;
        return <AccountOrders asset={asset} {...this.props} />;
    }
}

export default AccountOrdersWrapper;

//export default AccountOrders;

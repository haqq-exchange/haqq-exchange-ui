import React from "react";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";
import AccountActions from "actions/AccountActions";
import counterpart from "counterpart";
import { CopyToClipboard } from "react-copy-to-clipboard";
import {Icon as AntIcon, Button, Input, Form} from "antd";
import Icon from "../../Icon/Icon";
import {PublicKey, ChainStore, FetchChain, PrivateKey, key} from "deexjs";
import utils from "common/utils";
import ChainTypes from "components/Utility/ChainTypes";
import AltContainer from "alt-container";
import DefaultModal from "components/Modal/DefaultModal";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";
import BindToChainState from "components/Utility/BindToChainState";
import TransactionConfirmStore from "App/stores/TransactionConfirmStore";
import PrivateKeyStore from "stores/PrivateKeyStore";
import "../AccountVoting.scss";


class JoinWitnesses extends React.Component {

    static propTypes = {
        account: ChainTypes.ChainAccount.isRequired,
    };

    static defaultProps = {
        closeModal: () => {}
    };

    constructor(props) {
        super(props);

        this.getWitnessIdInterval = null;
        this.state = this.getInitialState(props);
        this.onTrxIncluded = this.onTrxIncluded.bind(this);
        this.setIsCopy = this.setIsCopy.bind(this);
    }

    getInitialState(props) {
        return {
            witnessAccount: props.account,
            signingKey: "",
            privateKey: "",
            url: "",
            isShowKeys: false,
            isRegenerateKeys: false,
        };
    }

    componentDidMount() {
        this.initGetWitnessAccount();
    }

    componentWillUnmount() {
        clearInterval(this.getWitnessIdInterval);
    }

    shouldComponentUpdate(np, ns) {
        return (
            this.props.wallet_locked !== np.wallet_locked ||
            this.props.account !== np.account ||
            this.props.account.get("accountStatus") !== np.account.get("accountStatus") ||
            this.props.account.get("lifetime_referrer_name") !== np.account.get("lifetime_referrer_name") ||
            this.props.visible !== np.visible ||
            this.state.url !== ns.url ||
            this.state.copied !== ns.copied ||
            this.state.copied1 !== ns.copied1 ||
            this.state.copied2 !== ns.copied2 ||
            this.state.WitnessById !== ns.WitnessById ||
            this.state.privateKey !== ns.privateKey ||
            this.state.signingKey !== ns.signingKey ||
            this.state.isShowKeys !== ns.isShowKeys ||
            this.state.isRegenerateKeys !== ns.isRegenerateKeys ||
            this.state.witnessAccount.get("id") !== ns.witnessAccount.get("id")
        );
    }

    componentDidUpdate(prevProps){
        const isNewAccount = this.props.account.get("id") !== prevProps.account.get("id");
        const isNewLTMRef = this.props.account.get("lifetime_referrer_name") !== prevProps.account.get("lifetime_referrer_name");
        const isNewStatus = this.props.account.accountStatus !== prevProps.account.accountStatus;
        if( isNewAccount || isNewLTMRef || isNewStatus ) {
            this.initGetWitnessAccount();
        }
    }

    onAddWitness() {
        const {witnessAccount, signingKey, url, WitnessById} = this.state;
        this.setState({
            isRegenerateKeys: false,
            isShowKeys: false,
        });
        if (witnessAccount && signingKey) {
            TransactionConfirmStore.listen(this.onTrxIncluded);
            if (!WitnessById) {
                AccountActions.createWitness({
                    url,
                    account: witnessAccount,
                    signingKey
                });
            } else {
                AccountActions.updateWitness({
                    url,
                    account: witnessAccount,
                    witness: WitnessById,
                    signingKey
                });
            }
        }
        this.props.closeModal();
    }

    onTrxIncluded(confirm_store_state) {
        window.console.log("onTrxIncluded", confirm_store_state);
        if (
            confirm_store_state.included &&
            confirm_store_state.broadcasted_transaction ||
            confirm_store_state.closed
        ) {
            TransactionConfirmStore.unlisten(this.onTrxIncluded);
            TransactionConfirmStore.reset();
            window.console.log("onTrxIncluded getWitnessAccount");
            this.startGetWitnessId();
        }
    }

    isValidPubKey = value => {
        return !PublicKey.fromPublicKeyString(value);
    };

    createNewKeys = () => {
        const {account} = this.props;
        const key_auths = account.getIn(["active", "key_auths"]);
        const privKey = key_auths.map(a => a.get(0)).get(0);
        const randomNumber = Math.floor(Math.random() * Math.floor(1000000));
        const KEYS = PrivateKeyStore._generateWitnessKeys(randomNumber.toString(), privKey);
            this.setState({
                privateKey: KEYS.privateKey,
                signingKey: KEYS.publicKey,
                isRegenerateKeys: true,
            });
    };

    createKeys = () => {
        const {account} = this.props;
        const key_auths = account.getIn(["active", "key_auths"]);
        const privKey = key_auths.map(a => a.get(0)).get(0);
        const KEYS = PrivateKeyStore._generateWitnessKeys(this.props.account, privKey);
            this.setState({
                privateKey: KEYS.privateKey,
                signingKey: KEYS.publicKey,
            });
    };

    startGetWitnessId(){
        this.getWitnessIdInterval = setInterval(this.initGetWitnessAccount.bind(this), 5000);
    }

    initGetWitnessAccount(){
        this.getWitnessAccount()
            .then(this.setDefaultValue);
    }

    getMemberStatus = account => {
        return ChainStore.getAccountMemberStatus(account);
    };

    getWitnessAccount = () => {
        const {account, witnessAccount} = this.state;
        return new Promise(async (resolve)=>{
            let currentAccc = witnessAccount || account;
            let WitnessById = await FetchChain("fetchWitnessByAccount", currentAccc.get("id"));
            if( WitnessById && this.getWitnessIdInterval ) {
                clearInterval(this.getWitnessIdInterval);
            }
            this.setState({
                WitnessById
            }, resolve);
        });
    };

    setDefaultValue = () => {
        const {WitnessById} = this.state;
        const {account} = this.props;
        if (WitnessById) {
            const key_auths = account.getIn(["active", "key_auths"]);
            const privKey = key_auths.map(a => a.get(0)).get(0);
            const KEYS = PrivateKeyStore._generateWitnessKeys(this.props.account, privKey);
            this.setState({
                privateKey: KEYS.privateKey,
                signingKey: WitnessById.get("signing_key"),
                url: utils.sanitize(WitnessById.get("url").toLowerCase())
            });
        } else {
            this.createKeys();
        }
    };

    setIsCopy = name => {
        const _this= this;
        console.log("name", name);
        this.setState({
            [name]: true
        }, () => {
            window.setTimeout( function () {
                _this.setState( {
                    [name]: false
                } );
            } , 3000 );
        });
    };

    hidePrivateKey = () => {
        this.setState({
            isShowKeys: false,
            isRegenerateKeys: false,
        })
    };

    render() {
        const {account, wallet_locked, isMyAccount} = this.props;
        const {
            witnessAccount,
            signingKey,
            copied,
            copied1,
            copied2,
            WitnessById,
            privateKey,
            isShowKeys,
            isRegenerateKeys
        } = this.state;
        const accountName = witnessAccount || account;
        const isWitness = WitnessById && WitnessById.get("id");
        let member_status = this.getMemberStatus(accountName);
        const isLifeMember = member_status === "lifetime";
        let wgetLink;
        if( isWitness && !wallet_locked && isMyAccount) {
            `wget -qO - https://deex.exchange/tools/start_witness.sh | bash -s - '${WitnessById.get("id")}' '${WitnessById.get("signing_key")}' 'YOUR PRIVATE KEY'`;
        }

        console.log("account", account.toJS())
        console.log("witnessAccount", witnessAccount.toJS())

        // для тестирования ключей
        // console.log('ОЖИДАЕТ', WitnessById.get("signing_key"))
        //  let deexjs = require("deexjs");
        //  let wif = privateKey; // сюда пишем приватный ключ
        //  console.log("Public key TEST:", deexjs.PrivateKey.fromWif(wif).toPublicKey().toString(), "\n");

        return (
            <Form className="full-width" layout="vertical">
           {isWitness ?
               <Form.Item label={<Translate content="modal.witness.witness_account_id"  />} style={{marginTop: 10}}>
                   <Input
                       value={WitnessById.get("id")}
                       addonAfter={
                           <CopyToClipboard
                               text={WitnessById.get("id")}
                               onCopy={() => this.setIsCopy("copied1")} >
                               <AntIcon
                                   style={{outline: "none"}}
                                   className={copied1 ? "check" : "copy"}
                                   type={copied1 ? "check" : "copy"}/>
                           </CopyToClipboard>
                       }
                   />
               </Form.Item> : null }

           {isWitness ?
               <Form.Item label={<Translate content="modal.witness.witness_bash"  />} style={{marginTop: 10}}>
                   <Input
                       value={wgetLink}
                       addonAfter={
                           <CopyToClipboard
                               text={wgetLink}
                               onCopy={() => this.setIsCopy("copied2")} >
                               <AntIcon
                                   style={{outline: "none"}}
                                   className={copied2 ? "check" : "copy"}
                                   type={copied2 ? "check" : "copy"}/>
                           </CopyToClipboard>
                       }
                   />
               </Form.Item> : null }
           <br/>
           {!isLifeMember ?
               <p>
                   <Translate content="modal.witness.be_committee"  component="span" />
                   <Translate
                       component={Link}
                       className="btn btn-red"
                       style={{margin: "0 10px"}}
                       to={`/account/${accountName && accountName.get("name")}/member-stats`}
                       content={"modal.committee.create_committee"} />
               </p> : null }

           <Translate content={__SCROOGE_CHAIN__ ? "modal.witness.text_scrooge" : "modal.witness.text"} unsafe component="p" />
           <Form.Item>

                {/* GENERATE KEY for LTM*/}
                {isLifeMember && !isWitness ?
                    <> 
                        {isMyAccount ?
                        <Button 
                            className={`${isShowKeys ? 'visually-hidden' : ''}`}
                            onClick={() => this.setState({isShowKeys: true})}
                            style={{display: "flex", cursor: "pointer", color: "#C3CCCB", border: "1px solid #C3CCCB"}}
                        >
                        Generate keys
                            <Icon
                                style={{marginLeft: "2px"}}
                                name="key" title="icons.key" size="1x" 
                                />
                        </Button>
                        : null}

                        <div className={`${isShowKeys ? '' : 'visually-hidden'}`}>
                            <div>    
                                {this.isValidPubKey(signingKey) ? (
                                    <label
                                        className="right-label"
                                        style={{
                                            marginTop: "-22px",
                                            position: "static"
                                        }}
                                    >
                                        <Translate content="modal.witness.invalid_key" />
                                    </label>
                                ) : null}
                                <Translate content="modal.witness.public_signing_key"  component="p"
                                style={{marginBottom: "10px", color: "#FFF"}} />             
                                <Input
                                    addonBefore={
                                    <div 
                                        onClick={() => this.createNewKeys()}
                                        style={{display: "flex", cursor: "pointer", color: "#C3CCCB"}}> 
                                    Regenerate keys
                                        <Icon
                                            style={{marginLeft: "2px"}}
                                            name="key" title="icons.key" size="1x" 
                                            />
                                    </div>
                                    }
                                    addonAfter={
                                        <CopyToClipboard
                                            text={signingKey}
                                            onCopy={() => this.setIsCopy("copied")} >
                                            <AntIcon
                                                style={{outline: 'none'}}
                                                className={copied ? "check" : "copy"}
                                                type={copied ? "check" : "copy"}/>
                                        </CopyToClipboard>
                                    }
                                    value={signingKey}
                                    placeholder={counterpart.translate(
                                        "modal.witness.enter_public_signing_key"
                                    )}
                                />
                            </div>
                            <div style={{marginBottom: "16px"}}>
                                <Translate content="modal.witness.private_key"  component="p"
                                style={{color: "#FFF", marginTop: "20px", marginBottom: "0"}} />
                                <Translate content="modal.witness.private_key_crucial"  component="p"
                                style={{fontSize: "16px", color: "rgba(231, 77, 80, 1)", fontWeight: "bold", marginTop: "0", marginBottom: "10px"}} />     
                                <Input 
                                    value={privateKey}
                                    addonAfter={
                                        <CopyToClipboard
                                            text={privateKey}
                                            onCopy={() => this.setIsCopy("copied")} >
                                            <AntIcon
                                                style={{outline: 'none'}}
                                                className={copied ? "check" : "copy"}
                                                type={copied ? "check" : "copy"}/>
                                        </CopyToClipboard>
                                    }
                                />
                            </div>
                            <div className="modal-footer" style={{marginTop: "16px"}}>
                                <Button
                                    key="submit"
                                    type="primary"
                                    disabled={
                                        this.isValidPubKey(signingKey) ||
                                        !isLifeMember
                                    }
                                    onClick={this.onAddWitness.bind(this)}
                                >
                                    {WitnessById
                                        ? counterpart.translate(
                                                "modal.witness.confirm_update"
                                            )
                                        : counterpart.translate("modal.witness.confirm")}
                                </Button>
                                <Button
                                    key="cancel"
                                    style={{marginLeft: "8px"}}
                                    onClick={() => {
                                        this.hidePrivateKey();
                                    }}
                                >
                                    {counterpart.translate("modal.cancel")}
                                </Button>
                            </div>
                        </div>
                    </>
                : null} 

                {/* REGENERATE KEYS for witness */}
                {isWitness ?  
                    <>      
                        {this.isValidPubKey(signingKey) ? (
                            <label
                                className="right-label"
                                style={{
                                    marginTop: "-22px",
                                    position: "static"
                                }}
                            >
                                <Translate content="modal.witness.invalid_key" />
                            </label>
                        ) : null}
                        <Translate content="modal.witness.public_signing_key"  component="p"
                        style={{color: "#FFF"}} />             
                        <Input
                            addonBefore={
                                <div 
                                    onClick={() => this.createNewKeys()}
                                    style={{display: "flex", cursor: "pointer", color: "#C3CCCB"}}> 
                                Regenerate keys
                                    <Icon
                                        style={{marginLeft: "2px"}}
                                        name="key" title="icons.key" size="1x" 
                                        />
                                </div>
                            }
                            addonAfter={
                                <CopyToClipboard
                                    text={signingKey}
                                    onCopy={() => this.setIsCopy("copied")} >
                                    <AntIcon
                                        style={{outline: 'none'}}
                                        className={copied ? "check" : "copy"}
                                        type={copied ? "check" : "copy"}/>
                                </CopyToClipboard>
                            }
                            value={signingKey}
                            placeholder={counterpart.translate(
                                "modal.witness.enter_public_signing_key"
                            )}
                        />
                        <div className={`${isRegenerateKeys ? '' : 'visually-hidden'}`}>
                            <Translate content="modal.witness.private_key"  component="p"
                            style={{color: "#FFF", marginTop: "20px", marginBottom: "0"}} />  
                           <Translate content="modal.witness.private_key_crucial"  component="p"
                                style={{fontSize: "16px", color: "rgba(231, 77, 80, 1)", fontWeight: "bold", marginTop: "0", marginBottom: "10px"}} />       
                            <Input 
                            value={privateKey}
                                addonAfter={
                                    <CopyToClipboard
                                        text={privateKey}
                                        onCopy={() => this.setIsCopy("copied")} >
                                        <AntIcon
                                            style={{outline: 'none'}}
                                            className={copied ? "check" : "copy"}
                                            type={copied ? "check" : "copy"}/>
                                    </CopyToClipboard>
                            }
                            />
                            {isMyAccount ?
                                <div className="modal-footer " style={{marginTop: "16px"}}>
                                    <Button
                                        key="submit"
                                        type="primary"
                                        disabled={
                                            this.isValidPubKey(signingKey) ||
                                            !isLifeMember
                                        }
                                        onClick={this.onAddWitness.bind(this)}
                                    >
                                        {WitnessById
                                            ? counterpart.translate(
                                                    "modal.witness.confirm_update"
                                                )
                                            : counterpart.translate("modal.witness.confirm")}
                                    </Button>
                                    <Button
                                        key="cancel"
                                        style={{marginLeft: "8px"}}
                                        onClick={() => {
                                            this.hidePrivateKey();
                                        }}
                                    >
                                        {counterpart.translate("modal.cancel")}
                                    </Button>
                                </div>
                            : null}
                        </div>
                    </>
                : null}

           </Form.Item>
       </Form>
        );
    }
}


const JoinWitnessesBind = BindToChainState(JoinWitnesses, {
    autosubscribe: true
});

export {
    JoinWitnessesBind
};

class JoinWitnessesModal extends DefaultBaseModal {
    constructor(props) {
        super(props);
        this.state = {};
    }

    afterOpenModal = () => {};

    afterCloseModal = () => {};

    closeModal = () => {
        const {resolve, modalId} = this.props;
        ModalActions.hide(modalId).then(resolve);
    };

    render() {
        const {modalId, modalIndex, account, wallet_locked, isMyAccount} = this.props;
        const {isOpenModal} = this.state;
        return (
            <DefaultModal
                id={modalId}
                modalId={modalId}
                ref={ref => (this.RefUnlockModal = ref)}
                isOpen={isOpenModal}
                className="UnlockModal commitee-modal"
                onAfterOpen={this.afterOpenModal}
                shouldCloseOnOverlayClick={false}
                shouldCloseOnEsc={false}
                customStyle={{zIndex: modalIndex[modalId]}}
                onRequestClose={this.closeModal}
            >
                <div className="modal fadeIn ng-enter is-active ng-enter-active">
                    <div className="public-login-modal">
                        <div className="modal-header">
                            <a
                                onClick={this.closeModal}
                                className="close-button"
                            >
                                ×
                            </a>
                        </div>
                        <div className="modal-content">
                            <JoinWitnesses
                                closeModal={this.closeModal}
                                wallet_locked={wallet_locked}
                                isMyAccount={isMyAccount}
                                account={account} />
                        </div>
                    </div>
                </div>
            </DefaultModal>
        );
    }
}

JoinWitnessesModal.defaultProps = {
    modalId: "join_witnesses_modal"
};

export default class JoinWitnessesModalContainer extends React.Component {
    render() {
        console.log(">>>> ", this.props)
        return (
            <AltContainer
                stores={[ModalStore, TransactionConfirmStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    modalIndex: () => ModalStore.getState().modalIndex,
                    resolve: () => ModalStore.getState().resolve
                }}
            >
                <JoinWitnessesModal {...this.props} />
            </AltContainer>
        );
    }
}


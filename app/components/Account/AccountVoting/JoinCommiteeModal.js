import React from "react";
import PropTypes from "prop-types";
import counterpart from "counterpart";
import Translate from "react-translate-component";
import AccountActions from "actions/AccountActions";
import DefaultModal from "components/Modal/DefaultModal";
import AccountSelector from "components/Account/AccountSelector";
import {Form, Input } from "antd";

import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";

import sanitize from "xss";
// import "antd/lib/form/style/index.css";
// import "antd/lib/input/style/index.css";

class JoinCommiteeModal extends DefaultBaseModal {


    constructor(props) {
        super(props);
        this.state = {
        };
    }


    afterOpenModal = () => {};

    afterCloseModal = () => {};

    closeModal = () => {
        const {resolve, modalId} = this.props;
        ModalActions.hide(modalId)
            .then(resolve);
    };


    nextProcess = () => {

    };



    onAddComittee() {
        const {committeeAccount, url} = this.state;

        if (committeeAccount && url) {
            AccountActions.createCommittee({account: committeeAccount, url});
        }
        this.props.hideModal();
    }

    onChangeCommittee(account) {
        this.setState({
            committeeAccount: account
        });
    }


    onUrlChanged(e) {
        this.setState({
            url: sanitize(e.target.value.toLowerCase(), {
                whiteList: [], // empty, means filter out all tags
                stripIgnoreTag: true // filter out all HTML not in the whilelist
            })
        });
    }

    render() {
        const {isOpenModal} = this.state;
        const {modalId, modalIndex } = this.props;
        const {url, committeeAccount} = this.state;

        return (
            <DefaultModal
                id={modalId}
                modalId={modalId}
                ref={(ref) => this.RefUnlockModal = ref}
                isOpen={isOpenModal}
                className="UnlockModal commitee-modal"
                onAfterOpen={this.afterOpenModal}
                shouldCloseOnOverlayClick={false}
                shouldCloseOnEsc={false}
                customStyle={{zIndex: modalIndex[modalId]}}
                onRequestClose={this.closeModal}>
                <div className="modal fadeIn ng-enter is-active ng-enter-active">
                    <div className="public-login-modal">
                        <div className="modal-header">
                            <a onClick={this.closeModal} className="close-button">×</a>
                        </div>
                        <div className="modal-content">
                            <Form className="full-width" layout="vertical">
                                <AccountSelector
                                    label="modal.committee.from"
                                    accountName={
                                        (committeeAccount && committeeAccount.get("name")) || ""
                                    }
                                    account={committeeAccount || null}
                                    onAccountChanged={this.onChangeCommittee.bind(this)}
                                    size={35}
                                    typeahead={true}
                                    allowUppercase={true}
                                />
                                <Translate
                                    content="modal.committee.text"
                                    unsafe
                                    component="p"
                                />
                                <Form.Item
                                    label={counterpart.translate("modal.committee.url")}
                                >
                                    <Input
                                        value={url}
                                        onChange={this.onUrlChanged.bind(this)}
                                        placeholder={counterpart.translate(
                                            "modal.committee.web_example"
                                        )}
                                    />
                                </Form.Item>
                            </Form>
                        </div>
                        <div className="modal-footer">
                            <button
                                key="submit"
                                type="primary"
                                className={"btn btn-blue"}
                                onClick={()=>this.onAddComittee()}
                            >
                                {counterpart.translate("modal.committee.confirm")}
                            </button>
                        </div>
                    </div>
                </div>
            </DefaultModal>
        );
    }
}

JoinCommiteeModal.defaultProps = {
    modalId: "join_commitee_modal"
};

export default class JoinCommiteeModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    modalIndex: () => ModalStore.getState().modalIndex,
                    resolve: () => ModalStore.getState().resolve
                }}
            >
                <JoinCommiteeModal {...this.props} />
            </AltContainer>
        );
    }
}

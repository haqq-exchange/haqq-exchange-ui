import React from "react";
import {Link} from "react-router-dom";
import counterpart from "counterpart";
import Translate from "react-translate-component";

const anonIcon = require("assets/anonymous-mask-icon.png");
const anonActiveIcon = require("assets/anonymous-mask-active-icon.png");


export default class RegisterReferral extends React.Component  {

    _showJoinButton = () => {
        return(
            <div className={"referrals__field-error"}>
                <input
                    name="referer" placeholder="referral code"
                    value={this.props.refId} onChange={()=>this.props.onInputChangeHandler()}
                    type="hidden" />
                <button className={"btn btn-red"} onClick={()=>this.props.joinReferral()}>
                    <Translate content={"referral.join_program"} />
                </button>
            </div>
        );
    };

    _showJoinError = () => {
        console.log("this.props", this.props);
        const {error, code, account} = this.props;

        let codeText = __SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__ ? null : error;
        if( code === 420 ) {
            codeText = counterpart.translate(["referral", error.split(" ").join("_").toLowerCase()].join("."));
        }
        return(
            <>
                <div className={"referrals__field"}>
                    {codeText}
                </div>
                <div style={{textAlign: "center", marginTop: 10}}>
                    <Link className={"btn btn-green"} to={["","voting"].join("/")} >
                        <Translate content={"accountref.voiting"}/>
                    </Link>
                </div>
            </>
        );
    };

    render() {
        const {error, code} = this.props;
        console.log([200, 418].indexOf(code) !== -1, code);
        return (
            <div className={"referrals"}>
                <div className={"referrals-block"}>
                    <div className={"referrals__info referrals__info--about"}>
                        <Translate component={"h2"} className={"referrals__title"} content={"referral.title_program"}/>
                        {__GBL_CHAIN__ || __GBLTN_CHAIN__ &&
                        <Translate component={"div"} className={"send-transfer-subtitle"}  content={"transfer.header_subheader_gbl"}/>
                        // : <Translate unsafe component={"div"} className={"referrals__description"} content={__SCROOGE_CHAIN__ ? "referral.pre_reg_scrooge" : "referral.pre_reg"}/>
                        }
                        <div className="referrals__tree-info">
                            <div className="referrals__tree-item active">
                                <Translate className={"left"} content={"referral.referal0"}/>
                                <div className="anons">
                                    <img src={anonActiveIcon} alt="Logo" />
                                </div>
                                <Translate className={"right"} content={"referral.referal0_info"}/>
                            </div>
                            <div className="referrals__tree-item">
                                <Translate className={"left"} content={"referral.referal1"}/>
                                <div className="anons">
                                    <img src={anonIcon} alt="Logo" />
                                    <img src={anonIcon} alt="Logo" />
                                </div>
                                <Translate className={"right"} content={"referral.referal1_info"}/>
                            </div>
                            <div className="referrals__tree-item">
                                <Translate className={"left"} content={"referral.referal2"}/>
                                <div className="anons">
                                    <img src={anonIcon} alt="Logo" />
                                    <img src={anonIcon} alt="Logo" />
                                    <img src={anonIcon} alt="Logo" />
                                </div>
                                <Translate className={"right"} content={"referral.referal2_info"}/>
                            </div>
                            <div className="referrals__tree-item">
                                <Translate className={"left"} content={"referral.referal3"}/>
                                <div className="anons">
                                    <img src={anonIcon} alt="Logo" />
                                    <img src={anonIcon} alt="Logo" />
                                    <img src={anonIcon} alt="Logo" />
                                    <img src={anonIcon} alt="Logo" />
                                </div>
                                <Translate className={"right"} content={"referral.referal3_info"}/>
                            </div>
                        </div>
                        {error && [200, 418].indexOf(code) === -1
                            ? this._showJoinError()
                            : this._showJoinButton()}

                    </div>
                </div>
            </div>
        );
    }
}
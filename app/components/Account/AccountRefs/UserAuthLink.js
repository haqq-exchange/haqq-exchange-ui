import React from "react";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";
import "./AccountRefs.scss";
export default class UserAuthLink extends React.Component {

    render(){
        const {toggleLock} = this.props;
        return(
            <div className={"referrals"}>
                <div className={"referrals-block"}>
                    <div className={"referrals-block-wrap"}>
                        <Translate content={"referral.unlock_info"}/>
                        {toggleLock ? <button type={"button"} className={"btn btn-red btn-auth"} onClick={(event)=>this.props.toggleLock(event)}>
                            <Translate content={"header.unlock_short"}/>
                        </button> : <Link to={"/authorization"} className={"btn btn-red btn-auth 111"} ><Translate content={"header.unlock_short"}/></Link> }

                    </div>
                </div>
            </div>
        );
    }
}
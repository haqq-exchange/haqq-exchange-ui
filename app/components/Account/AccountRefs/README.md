# Partner Program Backend API


***Register***

  Registers the user into the partner program

* **URL:**   /register

* **Method:** `POST`

* **URL Params:** None

* **Data Params:**

  *   ```json
      {
        data: {"account":"user01","referrer":"nIrIJTpV"},
        sign: IEGlzsdJ3UpHb7...Rk=
      }
      ```

* **Request Headers:** `Content-Type: application/x-www-form-urlencoded`

* **Success Response:**

  **Code:** 200 <br />
  **Content:** 

  ```json
  {}
  ```

* **Error Responses:**

| Code          | Content           |
| ------------- |:-------------|
| 500      | `{ error : "Internal server error" }`|
| 400      | `{ error : "Bad request data" }`      |
| 401 | `{ error : "Wrong signature" }`     |    
| 403 | `{ error : "Account has not been registered by DEEX" }`|
| 404 | `{ error : "Account does not exist" }` |
| 418 | `{ error : "Account does not participate in the partnership program" }`|
| 422 | `{ error : "Account is register" }`|
  


***Join***

  Joins the user into the partner program and creaters his referral link.
  The user should vote for the deex-witness to qualify for that/

* **URL:** /join

* **Method:** `POST`

* **URL Params:** None

* **Data Params**

  * `data: {"account":"user01"}`
  * `sign: IEGlzsdJ3UpHb7...Rk=`

* **Request Headers**

  * `Content-Type: application/x-www-form-urlencoded`

* **Success Response:**

  **Code:** 200 <br />
  **Content:** 

  ```json
  {"refId": "78YZESQ4", "refLink": "?ref=78YZESQ4"}
  ```

* **Error Responses:**

| Code          | Content           |
| ------------- |:-------------|
| 500      | `{ error : "Internal server error" }`|
| 400      | `{ error : "Bad request data" }`      |
| 401 | `{ error : "Wrong signature" }`     |    
| 403 | `{ error : "Account has not been registered by DEEX" }`|
| 404 | `{ error : "Account does not exist" }` |
| 418 | `{ error : "Account does not participate in the partnership program" }`|
| 420 | `{ error : "Wrong voting account" }`|


***Get referral info***

  Returns user's referral link, id and total number of his direct referrals

- **URL**

  /get_ref_info

- **Method:**

  `POST`

- **URL Params**

  None

- **Data Params**

  * `data: {"account":"user01"}`
  * `sign: IEGlzsdJ3UpHb7...Rk=`

* **Request Headers**

  * `Content-Type: application/x-www-form-urlencoded`

- **Success Response:**

  **Code:** 200 <br />
  **Content:** 

  ```json
  {
    "refId": "OcmhXRR9",
    "refLink": "?ref=OcmhXRR9",
    "referrals": 3,
    "amounts": [
      {
        "currency": "DEEX",
        "amount": 308.48
      },
      {
        "currency": "BTS",
        "amount": 42.13
      }
    ],
    "our_voter": true
  }
  ```
  or if the user has not joined the program yet
  ```json
  {"referrals": 0}
  ```

- **Error Responses:**

| Code          | Content           |
| ------------- |:-------------|
| 500      | `{ error : "Internal server error" }`|
| 400      | `{ error : "Bad request data" }`      |
| 401 | `{ error : "Wrong signature" }`     |    
| 403 | `{ error : "Account has not been registered by DEEX" }`|
| 404 | `{ error : "Account does not exist" }` |
| 418 | `{ error : "Account does not participate in the partnership program" }`|

***Update referral info***

  Sets a referral's alias and , id and adds him to the favorites

- **URL**

  /update_referral_info

- **Method:**

  `POST`

- **URL Params**

  None

- **Data Params**

  * `data: {"account":"user01","referral":"ref_user_02","alias":"My greatest referral","favorite":"1"}`
  * `sign: IEGlzsdJ3UpHb7...Rk=`

  **Notes**
  1. *alias* could be empty
  2. *favorite* could be "0" or "1"
  3. Both parameters are optional.

* **Request Headers**

  * `Content-Type: application/x-www-form-urlencoded`

- **Success Response:**

  **Code:** 200 <br />
  **Content:** 

  ```json
  {"message": "Data successfully updated."}
  ```

- **Error Responses:**

| Code          | Content           |
| ------------- |:-------------|
| 500      | `{ error : "Internal server error" }`|
| 400      | `{ error : "Bad request data" }`      |
| 401 | `{ error : "Wrong signature" }`     |    
| 403 | `{ error : "Account has not been registered by DEEX" }`|
| 404 | `{ error : "Account does not exist" }` |
| 418 | `{ error : "Account does not participate in the partnership program" }`|


***Get referral tree branch***

  Returns user's referral tree branch

- **URL**

  /get_ref_tree

- **Method:**

  `POST`

- **URL Params**

  None

- **Data Params**

  * `data: {"account":"user01","refId":"nIrIJTpV","page-size":"10","page":"1","sort_by":"referrals desc","filter":"ser", "favorites":true}}`
  * `sign: IEGlzsdJ3UpHb7...Rk=`

  **Notes**
  1. *sort-by* modes:
  	* name
  	* name desc
  	* referrals
  	* referrals desc
  2. first *page* index - 1
  3. maximum *page-size* - 50
  4. filter checks referrals account names and aliases 

* **Request Headers**

  * `Content-Type: application/x-www-form-urlencoded`

- **Success Response:**

  **Code:** 200 <br />
  **Content:** 

  ```json
  [
    {
        "referrals": 2,
        "name": "user-42",
        "alias": null,
        "favorite": false,
        "total_earned": [
            {
                "currency": "DEEX",
                "amount": 13.24
            },
            {
                "currency": "BTS",
                "amount": 42.16
            }
        ]
    },
    {
        "referrals": 0,
        "name": "user-45",
        "alias": null,
        "favorite": false,
        "total_earned": []
    },
    {
        "referrals": 0,
        "name": "user-43",
        "alias": null,
        "favorite": false,
        "total_earned": [
            {
                "currency": "DEEX.LTC",
                "amount": 4567.0009
            }
        ]
    }
  ]
  ```

- **Error Responses:**

 
| Code          | Content           |
| ------------- |:-------------|
| 500      | `{ error : "Internal server error" }`|
| 400      | `{ error : "Bad request data" }`      |
| 401 | `{ error : "Wrong signature" }`     |    
| 403 | `{ error : "Account has not been registered by DEEX" }`|
| 404 | `{ error : "Account does not exist" }` |
| 418 | `{ error : "Account does not participate in the partnership program" }`|


***Initiate Withdrawal***

  Initiates withdrawal in DEEX

* **URL**

  /initiate_withdrawal

* **Method:**

  `POST`

* **URL Params**

   None

* **Data Params**

  * `data: {"account":"user01","amounts":[{amount:"42.06","currency":"DEEX"},{amount:"12.07","currency":"BTS"}]}`
  * `sign: IEGlzsdJ3UpHb7...Rk=`

* **Request Headers**

  * `Content-Type: application/x-www-form-urlencoded`

* **Success Response:**

  **Code:** 200 <br />
  **Content:** 

  ```json
  {}
  ```

* **Error Responses:**
 
| Code          | Content           |
| ------------- |:-------------|
| 500      | `{ error : "Internal server error" }`|
| 400      | `{ error : "Bad request data" }`      |
| 401 | `{ error : "Wrong signature" }`     |    
| 403 | `{ error : "Account has not been registered by DEEX" }`|
| 404 | `{ error : "Account does not exist" }` |
| 418 | `{ error : "Account does not participate in the partnership program" }`|
| 420 | `{ error : "Wrong voting account" }`|
| 460 | `{'error': 'Not enough amount'}`     |


***List Withdrawals***

  Returns all withdrawals and their current states

* **URL**

  /list_withdrawals

* **Method:**

  `POST`

* **URL Params**

   None

* **Data Params**

  * `data: {"account":"user01"}`
  * `sign: IEGlzsdJ3UpHb7...Rk=`

* **Request Headers**

  * `Content-Type: application/x-www-form-urlencoded`

* **Success Response:**

  **Code:** 200 <br />
  **Content:** 

  ```json
  [
    {
        "eventDT": "2018-11-12 17:10:08",
        "amounts": [
            {
                "currency": "DEEX",
                "amount": 42.08,
                "state": {
                    "name": "Succeeded",
                    "complete": true,
                    "failed": false
                }
            }
        ],
        "state": {
            "name": "Succeeded",
            "complete": true,
            "failed": false
        }
    },
    {
        "eventDT": "2018-11-12 17:19:54",
        "amounts": [
            {
                "currency": "DEEX",
                "amount": 22.08,
                "state": {
                    "name": "Failed",
                    "complete": true,
                    "failed": false
                }
            },
            {
                "currency": "BTS",
                "amount": 12.09,
                "state": {
                    "name": "InProcess",
                    "complete": false,
                    "failed": false
                }
            }
        ],
        "state": {
            "name": "In process",
            "complete": false,
            "failed": false
        }
    }  ]
  ```

* **Error Responses:**
 
| Code          | Content           |
| ------------- |:-------------|
| 500      | `{ error : "Internal server error" }`|
| 400      | `{ error : "Bad request data" }`      |
| 401 | `{ error : "Wrong signature" }`     |    
| 403 | `{ error : "Account has not been registered by DEEX" }`|
| 404 | `{ error : "Account does not exist" }` |
| 418 | `{ error : "Account does not participate in the partnership program" }`|
| 420 | `{ error : "Wrong voting account" }`|
| 460 | `{'error': 'Not enough amount'}`     |


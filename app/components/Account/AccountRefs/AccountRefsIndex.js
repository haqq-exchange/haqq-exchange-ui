import React from "react";
import {Route, Switch} from "react-router-dom";
import Loadable from "react-loadable";
import LoadingIndicator from "Components/LoadingIndicator";
import WalletDb from "stores/WalletDb";
import {ChainStore, Signature} from "deexjs";
import {connect} from "alt-react";
import AccountStore from "stores/AccountStore";
import UserAuthLink from "./UserAuthLink";

import "./AccountRefs.scss";
import ChainTypes from "Components/Utility/ChainTypes";
import BindToChainState from "Components/Utility/BindToChainState";

const HistoryFindings = Loadable({
    loader: () => import(/* webpackChunkName: "AccountRefs/HistoryFindings" */ "./HistoryFindings"),
    loading: LoadingIndicator
});

const AccountRefsAsync = Loadable({
    loader: () => import(/* webpackChunkName: "AccountRefs/AccountRefs" */ "./AccountRefs"),
    loading: LoadingIndicator
});

class AccountRefs extends React.Component {

    static propTypes = {
        account: ChainTypes.ChainAccount.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            cache: {}
        };
    }

    componentDidMount() {

    }




    setCache = (state) => {
        let newState = Object.assign({}, this.state.cache, state);
        console.log("newState", newState);
        this.setState({
            cache: newState
        });
    };


    getUserSign = (body) => {
        return new Promise((resolve, reject) => {
            // console.log("WalletDb.isLocked()", !WalletDb.isLocked());
            if (!WalletDb.isLocked()) {
                let memo_from_public,
                    memo_from_privkey,
                    active_from_public,
                    chain_account;
                const bodyForSign = JSON.stringify(body);
                chain_account = ChainStore.getAccount(this.props.account_name, false);
                // console.log("chain_account", chain_account);
                memo_from_public = chain_account.getIn(["options", "memo_key"]);
                active_from_public = chain_account.getIn(["active", "key_auths"]).map(a => a.get(0));
                memo_from_privkey = WalletDb.getPrivateKey(memo_from_public);
                if(!memo_from_privkey && active_from_public) {
                    memo_from_privkey = WalletDb.getPrivateKey(active_from_public.get(0));
                }

                if (memo_from_privkey) {
                    let sign = Signature.signBuffer(bodyForSign, memo_from_privkey);
                    sign = sign.toBuffer().toString("base64");
                    return resolve(sign);
                } else {
                    return reject("no memo_from_privkey");
                }
            }
            return reject("");
        });
    };

    render() {
        let {account_name} = this.props;


        let accOnProps = {
            getUserSign: this.getUserSign,
            setCache: this.setCache,
            ...this.state,
            ...this.props
        };

        console.log("accOnProps", accOnProps);

        if( !account_name ) return <UserAuthLink {...accOnProps} />;

        return (
            <Switch>
                <Route
                    path={"/account/referrals"}
                    exact
                    render={() => (<AccountRefsAsync {...accOnProps} />)}
                />
                <Route
                    path={"/account/referrals/history"}
                    exact
                    render={() => (<HistoryFindings {...accOnProps} />)}
                />
            </Switch>);
    }
}

const AccountRefsBind = BindToChainState(AccountRefs, {
    show_loader: true
});


class AccountRefsWrapper extends React.Component {
    render() {
        let account_name = this.props.account_name;

        return <AccountRefsBind {...this.props} account={account_name} />;
    }
}


export default connect(
    AccountRefsWrapper,
    {
        listenTo() {
            return [AccountStore];
        },
        getProps() {

            let { passwordAccount } = AccountStore.getState();
            return {
                account_name: passwordAccount
            };
        }
    }
);




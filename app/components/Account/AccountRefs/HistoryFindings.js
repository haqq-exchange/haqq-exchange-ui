import React from "react";
import RefUtils from "common/ref_utils";
import Immutable from "immutable";
import Translate from "react-translate-component";
import {Redirect} from "react-router-dom";
import WalletDb from "../../../stores/WalletDb";

export default class HistoryFindings extends React.Component {
    constructor(props) {
        super(props);

        this.state = this.getState();
        this.refutils = new RefUtils();

    }

    componentDidMount(){
        this.getListWithdraw();
    }

    getState = () => {

        let defaultStatus = {
            withdrawals: [],
        };

        return Object.assign({}, defaultStatus, this.props.cache);
    };

    getListWithdraw = () => {
        /* list_withdrawals */
        console.time("getListWithdraw");
        const _this = this;
        const {account_name} = this.props;
        return new Promise(resolve=>{
            this.props.getUserSign({account: account_name, system_id: _this.refutils.getSystemId()}).then(sign => {
                console.log("sign", sign, this, _this);
                _this.refutils.postTemplate("/list_withdrawals", {account: account_name, system_id: _this.refutils.getSystemId()}, sign)
                    .then((res)=>{

                        let saveState = {
                            withdrawals: res.content
                        };
                        this.setState(saveState);
                        this.props.setCache(saveState);
                        console.timeEnd("getListWithdraw");
                    });
            });
            resolve();
        });
    };

    render() {
        const {withdrawals} = this.state;
        const {settings, account,  account_name , isMyAccount} = this.props;
        // console.log("account", account, isMyAccount, this.props);
        if ( WalletDb.isLocked() ) {
            return (
                <Redirect from={`/account/${account_name}/referrals/history`} to={`/account/${account_name}/referrals`}/>
            );
        }
        // console.log("HistoryFindings.js", this.props, withdrawals);
        return (<div className={"referrals"}>
            <div className={"referrals-block"}>
                <div className="referrals__list">
                    <Translate component={"h2"} className={"referrals__title"} content={"referral.title_history"}/>

                    <div className={"referrals-findings-header"}>
                        <Translate className={"referrals-findings-header-date"} content={"referral.table_head_date"}/>
                        <Translate className={"referrals-findings-header-total"} content={"referral.table_head_total"}/>
                        <Translate className={"referrals-findings-header-status"} content={"referral.table_head_status"}/>
                    </div>
                    <div className={"referrals-findings-body"} >
                        {withdrawals.length ? withdrawals.map((withdraw, index)=>{
                            let timeOptions = {
                                year: "numeric", month: "numeric", day: "numeric",
                                hour: "numeric", minute: "numeric", second: "numeric",
                                hour12: false
                            };
                            let expiration = new Intl
                                .DateTimeFormat(settings.get("locale"), timeOptions)
                                .format(new Date(withdraw.eventDT));
                            return withdraw.amounts.map((item, key)=>{
                                return (
                                    <div key={"withdraw-"+ index + key} className={"referrals-findings-row"}>
                                        <span>{expiration}</span>
                                        <span>{item.amount} {item.currency}</span>
                                        <span>{item.state.name}</span>
                                    </div>
                                );
                            });
                        }) : null }
                    </div>
                </div>
            </div>
        </div>);
    }
}















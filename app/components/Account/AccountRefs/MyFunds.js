import React from "react";
import Immutable from "immutable";
import Translate from "react-translate-component";
import {Link} from "react-router-dom";
import {Checkbox} from "antd";
import {diff} from "deep-object-diff";

export default class MyFunds extends React.Component {

    constructor(props){
        super(props);
        this.state={
            allChecked: false,
            checkedAsset: Immutable.Map()
        };
    }

    componentDidUpdate(props){
        let diffAmounts =  diff(props.amounts, this.props.amounts);
        if( Object.keys(diffAmounts).length > 0 ){
            this.setState({
                checkedAsset: Immutable.Map()
            });
        }
    }


    allChecked=(data, isChecked)=>{
        let checkedAsset = {};
        data.forEach(item=>{
            if( isChecked && item.amount > 0 && item.withdrawal_allowed) {
                checkedAsset[item.currency] = item;
            }
        });
        this.setState({
            allChecked: isChecked,
            checkedAsset: Immutable.Map(Immutable.fromJS(checkedAsset))
        });
    };
    assetChecked=(item)=>{
        let { checkedAsset } = this.state;
        if( checkedAsset.has(item.currency)  ) {
            checkedAsset = checkedAsset.delete(item.currency);
        } else {
            checkedAsset = checkedAsset.set(item.currency, item);
        }
        this.setState({
            checkedAsset,
            allChecked: false
        });
    };


    render(){
        const { account_name, amounts }= this.props;
        const { checkedAsset, allChecked }= this.state;

        if( !amounts.length ) {
            return null;
        }
        
        return(
            <div className={"referrals__funds"}>
                <Translate component={"h2"} className={"referrals__title"} content={"referral.title_fy_funds"}/>

                <div className="referrals__funds-wrap">
                    <div className="referrals__funds-header">
                        <div className="referrals__funds-header-chbox">
                            <Checkbox prefixCls={"funds-checkbox"} checked={allChecked} onChange={(event)=>this.allChecked(amounts, event.target.checked)} />
                        </div>
                        <div className="referrals__funds-header-asset">
                            <Translate content={"referral.currency"}/>
                        </div>
                        <div className="referrals__funds-header-withdraw">
                            <Translate content={"referral.amount"}/>
                        </div>
                    </div>
                    <div className="referrals__funds-body">
                        {amounts.map(data=>{
                            let isChecked = data.amount !== 0 && checkedAsset.has(data.currency);
                            let isDisabled = data.amount === 0 || !data.withdrawal_allowed;
                            return (
                                <div key={"referrals__funds-"+data.currency} className={"referrals__funds-row"}>
                                    <span className={"referrals__funds-row-chbox"}>
                                        <Checkbox
                                            prefixCls={"funds-checkbox"}
                                            checked={isChecked}
                                            disabled={isDisabled}
                                            onChange={(event)=>this.assetChecked(data, event.target.checked)} />
                                    </span>
                                    <span className={"referrals__funds-row-asset"}>{data.currency}</span>
                                    <span className={"referrals__funds-row-withdraw"}>{data.amount}</span>
                                </div>
                            );
                        })}
                    </div>
                    <div className="referrals__funds-history">
                        <Link to={`/account/${account_name}/referrals/history`}>
                            <Translate content={"referral.history_findings"}/>
                        </Link>
                    </div>
                    <div className="referrals__funds-button">
                        <button type={"button"} className={"btn btn-green"} disabled={!checkedAsset.size} onClick={()=>this.props.withdraw(checkedAsset.toArray())}>
                            <Translate content={"referral.withdraw"}/>
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

import Translate from "react-translate-component";
import counterpart from "counterpart";
import React from "react";
import notify from "actions/NotificationActions";

export default class UserInfo extends React.Component {
    render(){
        const {refLink, refId} = this.props;
        const windowRefLink = [window.location.origin, refLink].join("/");
        return (
            <div className={"referrals__info"}>
                <Translate component={"h2"} className={"referrals__title"} content={"referral.title_program"}/>
                <Translate unsafe component={"p"} className={"referrals__description"} content={"referral.sub_title_program"}/>
                <ReferralField value={windowRefLink} title={counterpart.translate("referral.ref_link")}/>
                <ReferralField value={refId} title={counterpart.translate("referral.ref_id")}/>
            </div>
        );
    }
}

class ReferralField extends React.Component {
    copyToClipboard = e => {
        const {title} = this.props;
        this.input.select();
        document.execCommand("copy");
        e.target.focus();

        notify.addNotification({
            message: `${title} ${counterpart.translate("referral.copied")}`,
            level: "success",
            autoDismiss: 1
        });
    };

    render () {
        const {value, title} = this.props;
        return (
            <div className={"referrals__field-info"}>
                <span className={"referrals__description"}>{title}</span>
                <div className={"referrals__field"}>
                    <input type="text" value={value} readOnly={true} ref={(input) => this.input = input}/>
                    <button onClick={this.copyToClipboard} className="btn-copy" />
                </div>
            </div>
        );
    }
}
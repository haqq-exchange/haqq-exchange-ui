import React from "react";
import counterpart from "counterpart";

export default class ReferralsSearch extends React.Component {

    render(){
        return(
            <div className={"referrals-search"}>
                <div className={"referrals-search-wrap"}>
                    <input
                        type="text" onChange={(event)=>this.props.searchAccount(event.target.value)}
                        placeholder={counterpart.translate("referral.search")}/>
                </div>
            </div>
        );
    }
}
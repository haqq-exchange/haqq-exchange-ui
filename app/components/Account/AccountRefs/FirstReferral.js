import React from "react";
import LoadingIndicator from "Components/LoadingIndicator";
import MyFunds from "Components/Account/AccountRefs/MyFunds";
import Translate from "react-translate-component";
import ReferralsSearch from "Components/Account/AccountRefs/ReferralsSearch";
import ReferralsList from "Components/Account/AccountRefs/ReferralsList";
import {Pagination} from "antd";
import cname from "classnames";
// import counterpart from "counterpart";
import UserInfo from "./UserInfo";
import Immutable from "immutable";

export default class FirstReferral extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            isShowActive: false,
            account: null,
        };
    }

    toggleShowActive = (isShowActive, account) => {
        this.setState({isShowActive, account});
    };

    render(){
        const {refLink, refId, currentReferral, referralsCount, isLoaded,
            levelAccount, referalsList, changePagination, userSendData} = this.props;
        const {isShowActive} = this.state;
        const totalReferral = referralsCount.get(currentReferral).get("referrals");
        console.log("userSendData", this.props );
        let userData = userSendData.get(currentReferral);

        if( !userData ) {
            userData = Immutable.fromJS({
                "page-size": 10,
                "page": 1,
                "sort_by":"referrals desc"
            });
        }


        return(
            <div className={"referrals"}>
                <div className={"referrals-block"}>
                    {isLoaded ? <LoadingIndicator loadingText={"Connecting to Referral Api"}/>: null}
                    <div className="referrals-block-info">
                        <UserInfo refLink={refLink} refId={refId}/>
                        <MyFunds {...this.props} />
                    </div>
                    <div className={"referrals__list"}>
                        <Translate component={"h2"} className={"referrals__title"} content={"referral.title_leads"}/>
                        <div className="referrals__top">
                            {levelAccount.size > 0 ? <ReferralsBreadcrumbs accounts={levelAccount} onItemClick={this.props.onBreadCrumbsClick}/> : null}
                            {referalsList.length > 0 ? <ReferralsSearch {...this.props}/> : null}
                        </div>
                        {!isShowActive && <ReferralsList
                            referrals={referalsList}
                            levelAccount={levelAccount}
                            changeUserTree={this.props.changeUserTree}
                            toggleFavorite={this.props.toggleFavorite}
                            getUserTree={this.props.getUserTree}/>}

                        <Pagination
                            current={userData.get("page")}
                            pageSize={userData.get("page-size")}
                            defaultCurrent={1}
                            /*showSizeChanger*/
                            hideOnSinglePage={totalReferral<=10}
                            pageSizeOptions={["10","20","30","40","50"]}
                            onChange={changePagination}
                            total={totalReferral} />
                        {/*{isShowActive && account && <RecentTransactions
                            accountsList={Immutable.fromJS([account])}
                            compactView={false}
                            showMore={true}
                            fullHeight={true}
                            limit={15}
                            showFilters={true}
                            dashboard
                        />}*/}
                    </div>
                </div>
            </div>
        );
    }
}

const ReferralsBreadcrumbs = ({accounts, onItemClick}) => (
    <div className={"referrals-breadcrumbs"}>
        <ul className={"breadcrumbs-list"}>
            <li className={"breadcrumbs-item first"}>
                <span onClick={() => onItemClick()} className={"breadcrumbs-link"}>Leads </span>
            </li>

            {accounts.map((account, index) => {
                const isLast = index === accounts.size -1;
                return (
                    <li key={"breadcrumbs-item-" + index} className={cname("breadcrumbs-item")}>
                        <span onClick={() => isLast ? null : onItemClick(account)} className={cname("breadcrumbs-link", {"last": isLast})}>
                            {account}
                        </span>
                    </li>
                );
            })}
        </ul>
    </div>
);


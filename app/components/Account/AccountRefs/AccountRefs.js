import React from "react";
import {Signature, ChainStore} from "deexjs";
import WalletDb from "stores/WalletDb";
import RefUtils from "common/ref_utils";

import WalletUnlockActions from "actions/WalletUnlockActions";
import AccountActions from "actions/AccountActions";
import Immutable from "immutable";
import LoadingIndicator from "Components/LoadingIndicator";
import {debounce, trim} from "lodash";
import AccountStore from "stores/AccountStore";
import UserAuthLink from  "./UserAuthLink";
import RegisterReferral from  "./RegisterReferral";
import FirstReferral from  "./FirstReferral";
import {getRequestAddress} from "api/apiConfig";
import ModalActions from "../../../actions/ModalActions";


export default class AccountRefs extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.getState();

        this.refutils = new RefUtils({
            url: getRequestAddress("referral")
        });
        this.searchAccount = debounce(this.searchAccount, 500);

    }

    componentDidMount(){
        console.log("componentDidMount", this.props);
        this.getRefInfo();
    }

    getState = () => {
        const {account_name} = this.props;
        let defaultStatus = {
            status: "loading",
            error: null,
            referrer: "",
            refId: "",
            refLink: "",
            amounts: [],
            referalsList: [],
            referralsCount: Immutable.Map(),
            levelAccount: Immutable.List(),
            userSendData: Immutable.Map(),
            currentReferral: null,
            isLoaded: false,
        };

        defaultStatus.userSendData = defaultStatus.userSendData.set(account_name, Immutable.fromJS({
            "page-size": 10,
            "page": 1,
            "sort_by":"referrals desc"
        }));

        return Object.assign({}, defaultStatus, this.props.cache);
    };

    workWithFirstReferrers = ({content, status}) => {
        let {referralsCount} = this.state;
        if (status === 200) {
            Object.values(content).map(item=>{
                //let itemName = item.alias || item.name;
                referralsCount = referralsCount.set(item.name, Immutable.fromJS({
                    "referrals": item.referrals
                }));
            });
            const saveState = {
                status: "first referrals",
                referalsList: content,
                isLoaded: false,
                referralsCount
            };
            this.setState(saveState);
            this.props.setCache(saveState);
        }
    };

    workWithRefInfo = ({content, status}) => {
        if (status === 200) {
            const {refId, refLink, referrals, amounts} = content;
            const {account_name} = this.props;
            let {referralsCount} = this.state;
            if (refId) {
                referralsCount = referralsCount.set(account_name, Immutable.fromJS({
                    "referrals": referrals,
                }));
                let saveState = {
                    status: "show user info",
                    code: status,
                    error: null,
                    currentReferral: account_name,
                    refId, refLink,
                    amounts,
                    referralsCount
                };
                this.setState(saveState, this.getUserTree);
                this.props.setCache(saveState);
            } else {
                this.setStatusError({error: "Wrong voting account"}, status);
            }
        } else if( status === 418 ) {
            this.sendRegister()
                .then(this.sendJoin)
                .catch(this.workWithRegister);
        } else {
            this.setStatusError(content, status);
        }
    };

    getRefInfo = () => {
        const {account_name} = this.props;
        console.log("account_name", account_name)
        this.props.getUserSign({account: account_name, system_id: this.refutils.getSystemId()}).then(sign => {
            this.refutils.postTemplate("/get_ref_info", {account: account_name, system_id: this.refutils.getSystemId()}, sign)
                .then(this.workWithRefInfo);
        }).catch(this.catchUserSign);
    };

    getRequestData = (account_name, sendData) =>{
        let {userSendData} = this.state;
        return new Promise(resolve=>{
            if( !userSendData.has(account_name) ) {
                userSendData = userSendData.set(account_name, Immutable.fromJS({
                    "page-size": 10,
                    "page": 1,
                    "sort_by":"referrals desc"
                }));
            }
            userSendData = userSendData.set(account_name,
                userSendData.get(account_name).mergeDeep(Immutable.fromJS(sendData || {})));
            this.setState({userSendData}, ()=>{
                resolve(userSendData.get(account_name).toJS());
            });
            this.props.setCache({userSendData});
        });
    };

    getUserTree = ( root_account, sendData, isLevelAccount = true ) => {
        const {account_name} = this.props;
        let {levelAccount} = this.state;
        
        let sendName = account_name;
        if( root_account  ) {
            sendName = root_account.name || root_account;
        }
        let sendBody = {
            "account": account_name,
            "root_account": sendName,
            system_id: this.refutils.getSystemId(),
        };
        //const requestData = this.getRequestData(sendBody.root_account, sendData);
        this.getRequestData(sendBody.root_account, sendData).then(requestData=>{
            
            console.log("requestData", requestData, this.state)

            sendBody = Object.assign({}, requestData, sendBody);

            if( root_account && isLevelAccount ) {
                levelAccount = levelAccount.push(root_account.alias || root_account.name || root_account);
                let saveState = {
                    levelAccount,
                    currentReferral: sendBody.root_account,
                    isLoaded: true
                };
                this.setState(saveState);
                this.props.setCache(saveState);
            } else {
                this.setState({ currentReferral: sendBody.root_account, isLoaded: true });
                this.props.setCache({ currentReferral: sendBody.root_account, isLoaded: true });
            }
            this.props.getUserSign(sendBody).then(sign => {
                this.refutils
                    .postTemplate("/get_ref_tree", sendBody, sign)
                    .then(this.workWithFirstReferrers)
                    .then(()=>{
                        this.setState({
                            isLoaded: false
                        });
                    });
            }).catch(this.catchUserSign);
        });
    };

    /*getUserSign = (body) => {
        console.log("getUserSign", body )
        return new Promise((resolve, reject) => {
            console.log("WalletDb.isLocked()", WalletDb.isLocked(), body );
            if (!WalletDb.isLocked()) {
                //debugger;

                const chain_account = ChainStore.getAccount(this.props.account_name, false);
                console.log("chain_account", chain_account.toJS());
                const memo_from_public = chain_account.getIn(["options", "memo_key"]);
                console.log("memo_from_public", memo_from_public);
                const memo_from_privkey = WalletDb.getPrivateKey(memo_from_public);
                console.log("memo_from_privkey", memo_from_privkey);
                const bodyForSign = JSON.stringify(body);
                console.log("bodyForSign", bodyForSign);

                if (memo_from_privkey) {
                    let sign = Signature.signBuffer(bodyForSign, memo_from_privkey);
                    sign = sign.toBuffer().toString("base64");
                    return resolve(sign);
                }
            }
            return reject("");
        });
    };*/

    catchUserSign = (error) => {
        const {account_name} = this.props;
        console.log("user should be logged in", error, account_name);
        this.setState({
            status: "user_not_auth"
        });
    };

    onBreadCrumbsClick = account => {
        let {levelAccount} = this.state;
        levelAccount = levelAccount.pop();
        this.props.setCache({levelAccount});
        this.setState({levelAccount, isLoaded: true}, () => {
            this.getUserTree(account, false , false);
        });
    };

    onInputChangeHandler = (e) => {
        this.setState({refId: e.target.value});
        this.props.setCache({refId: e.target.value});
    };

    joinReferral = () => {
        const {error, code} = this.state;
        if( error.length && code === 418 ) {
            this.sendRegister()
                .then(this.sendJoin);
        } else {
            this.sendJoin();
        }
    };

    sendJoin = () => {
        const {account_name} = this.props;
        const sendBody = {
            account: account_name,
            root_account: "",
            system_id: this.refutils.getSystemId()
        };
        this.props.getUserSign(sendBody).then(sign => {
            this.refutils
                .postTemplate("/join", sendBody, sign)
                .then(this.workWithRegister);
        }).catch(this.catchUserSign);
    };

    // post запрос который должен отправляться при регистрации в реферальной программе
    sendRegister = () => {
        const {account_name} = this.props;
        let referralAccount = AccountStore.getState().referralAccount;
        const sendBody = {
            account: account_name,
            referrer: referralAccount || "",
            system_id: this.refutils.getSystemId()
        };
        return new Promise((resolve, reject) => {
            this.props.getUserSign(sendBody).then(sign => {
                this.refutils
                    .postTemplate("/register", sendBody, sign)
                    .then((result) => {
                        const {status} = result;
                        if ([403].indexOf(status) === -1) {
                            resolve(result);
                        } else {
                            reject(result);
                        }
                    });
            }).catch(reject);
        });
    };

    workWithRegister = (data) => {
        const {status, content} = data;
        if (status === 200) {
            const {refId, refLink} = content;
            const {account_name} = this.props;
            let {referralsCount} = this.state;
            referralsCount = referralsCount.set(account_name, Immutable.fromJS({
                "referrals": 0,
            }));
            let saveState = {
                status: "first referrals",
                code: status,
                refId, refLink,
                referralsCount,
                currentReferral: account_name,
            };
            this.setState(saveState);
            this.props.setCache(saveState);
        } else {
            this.setStatusError(content, status);
        }
    };

    setStatusError = (content, code, status = "register") => {
        let statusError = Object.assign({}, {
            status,
            code
        }, content);
        this.setState(statusError);
    };


    changePagination = (page, pageSize) => {
        const {account_name} = this.props;
        this.getUserTree(account_name, {page, pageSize}, false );
    };

    changeUserTree = (data) => {
        const {account_name} = this.props;
        this.getUserTree(account_name, data, false );
    };

    toggleFavorite = (item) => {
        const _this = this;
        const {currentReferral} = this.state;
        const {account_name} = this.props;
        const sendBody = {
            account: account_name,
            referral: item.name,
            favorite: !item.favorite,
            system_id: refutils.getSystemId()
        };
        this.setState({isLoaded: true}, ()=>{
            this.props.getUserSign(sendBody).then(sign => {
                this.refutils.postTemplate("/update_referral_info", sendBody, sign)
                    .then(()=>{
                        _this.getUserTree(currentReferral, false , false);
                    });
            }).catch(this.catchUserSign);
        });
    };

    searchAccount=(name)=>{
        const {userSendData, currentReferral} = this.state;
        const {account_name} = this.props;
        const userSendDataFilter = userSendData.getIn([account_name, "filter"]);
        let filter = name.length >= 3 ? name : "";
        if( userSendDataFilter !== filter ) {
            this.getUserTree(currentReferral, {
                filter: decodeURIComponent(trim(filter))
            } , false);
        }
    };

    withdraw = (data) => {
        const {account_name} = this.props;
        const sendBody = {
            account: account_name,
            amounts: data,
        };

        this.setState({isLoaded: true}, ()=>{
            this.props.getUserSign(sendBody).then(sign => {
                console.log("sign", sign);
                this.refutils
                    .postTemplate("/initiate_withdrawal", sendBody, sign)
                    .then(()=>this.getRefInfo());

            });
        });
    };

    toggleLock = (event) => {
        event.preventDefault();
        event.stopPropagation();
        if (WalletDb.isLocked()) {
            ModalActions.show("unlock_wallet_modal_public").then(() => {
                AccountActions.tryToSetCurrentAccount();
                this.getRefInfo();
            });
        }
    };

    render() {
        console.log("AccountRefs.js", this.props);
        const {status} = this.state;
        console.log("PROPS", this.props)
        if (status === "user_not_auth") {
            return (<UserAuthLink toggleLock={this.toggleLock} />);
        } else if (status === "loading") {
            return (<LoadingIndicator loadingText={"Connecting to Referral Api"}/>);
        } else if (status === "first referrals" || status === "show user info") {
            return <FirstReferral
                onBreadCrumbsClick={this.onBreadCrumbsClick}
                getUserTree={this.getUserTree}
                changeUserTree={this.changeUserTree}
                changePagination={this.changePagination}
                toggleFavorite={this.toggleFavorite}
                withdraw={this.withdraw}
                searchAccount={this.searchAccount}
                {...this.state} {...this.props} />;
        }
        if (this.state.status === "register") {
            return (
                <RegisterReferral
                    onInputChangeHandler={this.onInputChangeHandler}
                    joinReferral={this.joinReferral}
                    account={this.props.account}
                    {...this.state}
                />
            );

        }
        return (<h1>No state: {this.state.status}</h1>);
    }
}
























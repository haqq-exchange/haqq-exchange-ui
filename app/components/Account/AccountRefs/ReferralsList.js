import React from "react";
import LoadingIndicator from "Components/LoadingIndicator";
import Translate from "react-translate-component";
import cname from "classnames";
import Popover from "react-popover";
import {FetchChain} from "deexjs";
import {Asset} from "common/MarketClasses";
import {List} from "immutable";
//import utils from "common/utils";
import ValueStore from "Components/Utility/ValueStore";
import {diff} from "deep-object-diff";
const anon = require("assets/anonymous-mask-bg.png");

export default class ReferralsList extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            favorites: false,
            isPopoverOpen: {},
            totalAssetValue: {}
        };
    }

    componentDidMount(){
        this.getTotalValue();
    }
    componentDidUpdate(prevProps){
        let diffReferrals = diff(prevProps.referrals, this.props.referrals);
        if(Object.keys(diffReferrals).length) {
            this.getTotalValue();
        }
    }

    togglePopover(index) {
        let { isPopoverOpen }= this.state;
        isPopoverOpen[index] = !isPopoverOpen[index];
        this.setState({isPopoverOpen});
    }

    closePopover(index) {
        let { isPopoverOpen }= this.state;
        isPopoverOpen[index] = false;
        this.setState({isPopoverOpen});
    }
    getIsOpenPopOver(item) {
        const { isPopoverOpen }= this.state;
        if( item.total_earned.length  ) {
            return isPopoverOpen[item.name];
        } else {
            return false;
        }
    }

    getTotalValue = () => {
        const {referrals} = this.props;
        let totalAssetValue = {};
        referrals.map(referral => {
            if( referral.total_earned.length ) {
                let assetObject = {};
                totalAssetValue[referral.name] = {
                    balances: [],
                    fromAssets: List(),
                };
                referral.total_earned.map(total=>{assetObject[total.currency] = total.amount});
                FetchChain("getAsset", Object.keys(assetObject)).then(assets => {
                    assets.map(assetItem => {
                        let asset = new Asset({
                            real: assetObject[assetItem.get("symbol")],
                            asset_id: assetItem.get("id"),
                            precision: assetItem.get("precision")
                        });
                        totalAssetValue[referral.name].fromAssets = totalAssetValue[referral.name].fromAssets.push(assetItem.get("id"));
                        totalAssetValue[referral.name].balances.push({
                            asset_id: assetItem.get("id"),
                            amount: parseInt(asset.getAmount(), 10)
                        });
                    });

                    this.setState({totalAssetValue});
                });

            }
        });
    };

    setFavorite = (favorites) => {
        this.setState({
            favorites
        }, () => this.props.changeUserTree({ favorites }) );
    };

    render () {
        const {referrals, levelAccount, getUserTree, toggleFavorite} = this.props;
        const {totalAssetValue, favorites} = this.state;
        if (!referrals.length && !favorites) {
            if( levelAccount.size ) {
                return (<LoadingIndicator loadingText={"Loading referrals"}/>);
            } else {
                return (
                    <div className="no-refs-block">
                        <img src={anon} alt="Logo" />
                        <Translate component={"h3"} content={"referral.no_referal"}/>
                    </div>);
            }
        }

        return(
            <React.Fragment>
                <div className={"referrals-table-header"}>
                    <span className={cname("referrals-table-header-star", {"active": favorites})} onClick={()=>this.setFavorite(!favorites)} />
                    <Translate className={"referrals-table-header-name"} content={"referral.table_head_name"}/>
                    <Translate className={"referrals-table-header-count"} content={"referral.table_head_count"}/>
                    <Translate className={"referrals-table-header-total"} content={"referral.table_head_total"}/>
                    <span className={"referrals-table-header-money"} />
                </div>
                <div className={"referrals-table-body"}>
                    {referrals.map((item, index) => {
                        const isLast = levelAccount.size === 2;
                        const isGetUserTree = item.referrals && !isLast;
                        const popover_body = (
                            <div className={"Popover-body-row"}>
                                {item.total_earned.map(earn=>{
                                    return (<div key={"Popover-body-item-"+earn.amount} className={"Popover-body-item"}>
                                        <span>{earn.currency}</span>
                                        <i style={{width: "100%"}} />
                                        <span>{earn.amount}</span>
                                    </div>);
                                })}
                            </div>
                        );
                        let dataStoreValue =  totalAssetValue[item.name] || {};
                        let itemName =  item.alias || item.name;

                        return (
                            <div key={"referrals-table-item-"+index} className={"referrals-table-item"} >
                                <span className={cname("referrals-table-item-star", {active: item.favorite})} onClick={()=>{ toggleFavorite(item);} } />
                                {/*<span className={"referrals-table-info"} onClick={()=>{ toggleShowActive(true, item.name);} }>i</span>*/}
                                <span className={"referrals-table-item-name"} onClick={()=>{ isGetUserTree ? getUserTree(item) : null;} }>
                                    {itemName}
                                </span>
                                <span className={"referrals-table-item-count"}>{!isLast && item.referrals}</span>
                                <span className={"referrals-table-item-total"}>
                                    {totalAssetValue[item.name] ? <ValueStore
                                        noTip={true}
                                        hide_asset={false}
                                        inHeader={false}
                                        balances={dataStoreValue.balances}
                                        fromAssets={dataStoreValue.fromAssets}
                                    /> : "0,00" }
                                </span>
                                <Popover
                                    appendTarget={document.getElementById("content-wrapper")}
                                    className={"referrals-table-popover"}
                                    place={"below"}
                                    preferPlace={"below"}
                                    isOpen={this.getIsOpenPopOver(item)}
                                    onOuterAction={()=>this.closePopover(item.name)}
                                    body={popover_body} >
                                    <span
                                        className={cname("referrals-table-item-money", {active: item.total_earned.length  })}
                                        onClick={()=>this.togglePopover(item.name)}
                                    />
                                </Popover>
                            </div>
                        );
                    })}
                </div>
            </React.Fragment>
        );
    }
}
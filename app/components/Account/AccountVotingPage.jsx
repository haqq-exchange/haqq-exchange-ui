import React from "react";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import SettingsStore from "stores/SettingsStore";
import WalletUnlockStore from "stores/WalletUnlockStore";
import GatewayStore from "stores/GatewayStore";
import ChainTypes from "../Utility/ChainTypes";
import BindToChainState from "../Utility/BindToChainState";
import {connect} from "alt-react";
import accountUtils from "common/account_utils";
import {List} from "immutable";
import {Route, Switch, Redirect} from "react-router-dom";
import Page404 from "../Page404/Page404";
import Loadable from "react-loadable";
import LoadingIndicator from "Components/LoadingIndicator";
import UserAuthLink from "Components/Account/AccountRefs/UserAuthLink";
// import Translate from "react-translate-component";
//import RoutesLink from "RoutesLink";
//import AccountAssets from "./AccountAssets";
import {AccountAssetCreate} from "./AccountAssetCreate";
import AccountAssetUpdate from "./AccountAssetUpdate";
import AccountMembership from "./AccountMembership";
import AccountVesting from "./AccountVesting";
import AccountPermissions from "./AccountPermissions";
import AccountSignedMessages from "./AccountSignedMessages";
import ModalStore from "../../stores/ModalStore";
//import AccountOrders from "Components/Account/AccountOrders";
// import AccountWhitelist from "./AccountWhitelist";
// import AccountVoting from "./AccountVoting";
// import AccountOverview from "./AccountOverview";



const AccountVoting = Loadable({
    loader: () => import(/* webpackChunkName: "AccountVoting" */ "./AccountVoting"),
    loading: LoadingIndicator
});


class AccountPage extends React.Component {
    static propTypes = {
        account: ChainTypes.ChainAccount.isRequired
    };

    static defaultProps = {
        account: "props.match.params.account_name"
    };

    componentDidMount() {
        if (this.props.account) {
            AccountActions.setCurrentAccount.defer(
                this.props.account.get("name")
            );

            // Fetch possible fee assets here to avoid async issues later (will resolve assets)
            accountUtils.getPossibleFees(this.props.account, "transfer");
        }
    }

    UNSAFE_componentWillReceiveProps(np) {
        if (np.account) {
            const npName = np.account.get("name");
            const currentName =
                this.props.account && this.props.account.get("name");

            if (!this.props.account || npName !== currentName) {
                // Update the current account in order to access the header right menu options
                AccountActions.setCurrentAccount.defer(npName);
                // Fetch possible fee assets here to avoid async issues later (will resolve assets)
                accountUtils.getPossibleFees(np.account, "transfer");
            }
        }
    }


    render() {
        let {
            account_name,
            wallet_locked,
            account
        } = this.props;

        if (!account) {
            return <UserAuthLink />;
        }
        let isMyAccount = AccountStore.isMyAccount(account) && !wallet_locked;

        let passOnProps = {
            account,
            isMyAccount,
            contained: true,
            globalObject: "2.0.0",
            balances: account.get("balances", List()).toList(),
            orders: account.get("orders", List()).toList(),
            proxy: account.getIn(["options", "voting_account"]),
            history: this.props.history,
            ...this.props
        };



        //console.log("AccountPage.jsx render", passOnProps);

        return (
            <div className="grid-block page-layout account-voting-page">
                <AccountVoting {...passOnProps} />
            </div>
        );
    }
}
const AccountPageBind = BindToChainState(AccountPage, {
    show_loader: true
});


class AccountPageStoreWrapper extends React.Component {
    render() {
        const {account_name} = this.props;
        return <AccountPageBind {...this.props} account={account_name} />;
    }
}

export default connect(
    AccountPageStoreWrapper,
    {
        listenTo() {
            return [
                AccountStore,
                SettingsStore,
                WalletUnlockStore,
                ModalStore,
                GatewayStore
            ];
        },
        getProps() {

            let { passwordAccount } = AccountStore.getState();
            // console.log("AccountStore.getState()", AccountStore.getState())
            return {
                account_name: passwordAccount,
                myActiveAccounts: AccountStore.getState().myActiveAccounts,
                searchAccounts: AccountStore.getState().searchAccounts,
                private_accounts: AccountStore.getState().privateAccounts,
                private_contacts: AccountStore.getState().privateContacts,
                settings: SettingsStore.getState().settings,
                viewSsettings: SettingsStore.getState().viewSettings,
                hiddenAssets: SettingsStore.getState().hiddenAssets,
                favoriteAssets: SettingsStore.getState().favoriteAssets,
                wallet_locked: WalletUnlockStore.getState().locked,
                modals: ModalStore.getState().modals,
                viewSettings: SettingsStore.getState().viewSettings,
                backedCoins: GatewayStore.getState().backedCoins,
                bridgeCoins: GatewayStore.getState().bridgeCoins,
                gatewayDown: GatewayStore.getState().down
            };
        }
    }
);


import React from "react";
import Proposals from "components/Account/Proposals";
import "./AccountPermission.scss";

export default class AccountProposals extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="proposals-wrap">
                <Proposals className="dashboard-table"
                            account={this.props.account}
                            hideFishingProposals={true}
                />
            </div>
        );
    }
}

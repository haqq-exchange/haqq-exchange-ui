import React from "react";
import BalanceComponent from "../../Utility/BalanceComponent";
import {BalanceValueComponent} from "../../Utility/EquivalentValueComponent";

import assetUtils from "common/asset_utils";
// import counterpart from "counterpart";
import cname from "classnames";
import {Link} from "react-router-dom";
import LinkToAssetById from "../../Utility/LinkToAssetById";
import {getBackedCoin} from "common/gatewayUtils";
import {ChainStore} from "deexjs";
import {connect} from "alt-react";
import SettingsStore from "stores/SettingsStore";
import GatewayStore from "stores/GatewayStore";
import MarketsStore from "stores/MarketsStore";
import WalletUnlockStore from "stores/WalletUnlockStore";

import Icon from "../../Icon/Icon";
import utils from "common/utils";
import SettingsActions from "actions/SettingsActions";
import PaginatedList from "../../Utility/PaginatedList";
import MarketUtils from "common/market_utils";
import WalletDb from "../../../stores/WalletDb";
import WalletUnlockActions from "../../../actions/WalletUnlockActions";
import AccountStore from "../../../stores/AccountStore";
import {LimitOrder} from "../../../lib/common/MarketClasses";
import {diff} from "deep-object-diff";
import Translate from "react-translate-component";
import {getAlias} from "config/alias";
import ModalActions from "../../../actions/ModalActions";
import FormattedAsset from "Utility/FormattedAsset";
import AssetStore from "stores/AssetStore";
import { isConstructorDeclaration } from "typescript";

class AccountPortfolioList extends React.Component {
    constructor() {
        super();

        this.state = {
            isSettleModalVisible: false,
            isBorrowModalVisible: false,
            borrow: null,
            settleAsset: "1.3.0",
            depositAsset: null,
            withdrawAsset: null,
            bridgeAsset: null,
            allRefsAssigned: false,
            balanceRows: []
        };

        this.inOrders = {};
        this.qtyRefs = {};
        this.priceRefs = {};
        this.valueRefs = {};
        this.changeRefs = {};
        for (let key in this.sortFunctions) {
            this.sortFunctions[key] = this.sortFunctions[key].bind(this);
        }
        this._checkRefAssignments = this._checkRefAssignments.bind(this);

        this.showSettleModal = this.showSettleModal.bind(this);
        this.hideSettleModal = this.hideSettleModal.bind(this);

        this.showBorrowModal = this.showBorrowModal.bind(this);
        this.hideBorrowModal = this.hideBorrowModal.bind(this);

    }

    componentDidMount(){
        this.refCheckInterval = setInterval(this._checkRefAssignments);
        this._renderBalances(this.props.assetBalancesList);
    }
    componentDidUpdate(prevProps){
        let diffUpdate = diff(prevProps.assetBalancesList.toJS(), this.props.assetBalancesList.toJS());

        if( Object.keys(diffUpdate).length
            || !prevProps.hiddenAssets.equals(this.props.hiddenAssets)
            || !prevProps.favoriteAssets.equals(this.props.favoriteAssets)
            || prevProps.account_name !== this.props.account_name
            || prevProps.sortKey !== this.props.sortKey
            || prevProps.wallet_locked !== this.props.wallet_locked
            || prevProps.locked !== this.props.locked
            || prevProps.sortDirection !== this.props.sortDirection
            || prevProps.preferredUnit !== this.props.preferredUnit
            || prevProps.settings.get("locale") !== this.props.settings.get("locale")

        ) {
            this._renderBalances(this.props.assetBalancesList);
        }
    }

    componentWillUnmount() {
        clearInterval(this.refCheckInterval);
    }

    _checkRefAssignments() {
        /*
        * In order for sorting to work all refs must be assigned, so we check
        * this here and update the state to trigger a rerender
        */
        if (!this.state.allRefsAssigned) {
            let refKeys = ["qtyRefs", "priceRefs", "valueRefs", "changeRefs"];
            const allRefsAssigned = refKeys.reduce((a, b) => {
                if (a === undefined) return !!Object.keys(this[b]).length;
                return !!Object.keys(this[b]).length && a;
            }, undefined);
            if (allRefsAssigned) {
                clearInterval(this.refCheckInterval);
                this.setState({allRefsAssigned});
            }
        }
    }

    shouldComponentUpdate(np, ns) {

        return (
            !utils.are_equal_shallow(ns, this.state) ||
            !utils.are_equal_shallow(np.backedCoins, this.props.backedCoins) ||
            !utils.are_equal_shallow(np.balances, this.props.balances) ||
            !utils.are_equal_shallow(np.assetBalancesList, this.props.assetBalancesList) ||
            !utils.are_equal_shallow(
                np.optionalAssets,
                this.props.optionalAssets
            ) ||
            np.account_name !== this.props.account_name ||
            np.visible !== this.props.visible ||
            np.settings !== this.props.settings ||
            np.account.equals(this.props.account) ||
            np.hiddenAssets.equals(this.props.hiddenAssets) ||
            np.favoriteAssets.equals(this.props.favoriteAssets) ||
            np.sortDirection !== this.props.sortDirection ||
            np.sortKey !== this.props.sortKey ||
            np.isMyAccount !== this.props.isMyAccount ||
            np.preferredUnit !== this.props.preferredUnit ||
            np.allMarketStats.reduce((a, value, key) => {
                return (
                    utils.check_market_stats(
                        value,
                        this.props.allMarketStats.get(key)
                    ) || a
                );
            }, false)
        );
    }

    showSettleModal() {
        this.setState({
            isSettleModalVisible: true
        });
    }

    hideSettleModal() {
        this.setState({
            isSettleModalVisible: false
        });
    }

    showBorrowModal(quoteAsset, backingAsset, account) {
        this.setState({
            isBorrowModalVisible: true,
            borrow: {
                quoteAsset: quoteAsset,
                backingAsset: backingAsset,
                account: account
            }
        });
    }

    hideBorrowModal() {
        this.setState({
            borrow: null,
            isBorrowModalVisible: false
        });
    }

    sortFunctions = {
        qty: function(a, b, force) {

            /*if (this.qtyRefs[a.key] < this.qtyRefs[b.key])
                return this.props.sortDirection || force ? -1 : 1;

            if (this.qtyRefs[a.key] > this.qtyRefs[b.key])
                return this.props.sortDirection || force ? 1 : -1;*/

            let aValue = this.qtyRefs[a.key];
            let bValue = this.qtyRefs[b.key];
            if (aValue && bValue) {
                return this.props.sortDirection
                    ? aValue - bValue
                    : bValue - aValue;
            } else if (!aValue && bValue) {
                return 1;
            } else if (aValue && !bValue) {
                return -1;
            }
        },
        alphabetic: function(a, b, force) {
            if (a.key > b.key)
                return this.props.sortDirection || force ? 1 : -1;
            if (a.key < b.key)
                return this.props.sortDirection || force ? -1 : 1;
            return 0;
        },
        priceValue: function(a, b) {
            let aPrice = this.priceRefs[a.key];
            let bPrice = this.priceRefs[b.key];
            if (aPrice && bPrice) {
                return this.props.sortDirection
                    ? aPrice - bPrice
                    : bPrice - aPrice;
            } else if (aPrice === null && bPrice !== null) {
                return 1;
            } else if (aPrice !== null && bPrice === null) {
                return -1;
            } else {
                return this.sortFunctions.alphabetic(a, b, true);
            }
        },
        inOrders: function(a, b) {
            let aPrice = this.inOrders[a.key] || 0;
            let bPrice = this.inOrders[b.key] || 0;

            if (aPrice && bPrice && Number(aPrice) > 0 && Number(bPrice) > 0 ) {
                return this.props.sortDirection
                    ? aPrice - bPrice
                    : bPrice - aPrice;

            } else if (Number(aPrice) === 0 && Number(bPrice) !== 0) {
                return 1;
            } else if (Number(aPrice) !== 0 && Number(bPrice) === 0) {
                return -1;
            } else {
                return this.sortFunctions.alphabetic(a, b, true);
            }
        },
        totalValue: function(a, b) {
            let aValue = this.valueRefs[a.key];
            let bValue = this.valueRefs[b.key];
            if (aValue && bValue) {
                return this.props.sortDirection
                    ? aValue - bValue
                    : bValue - aValue;
            } else if (!aValue && bValue) {
                return 1;
            } else if (aValue && !bValue) {
                return -1;
            } else {
                return this.sortFunctions.alphabetic(a, b, true);
            }
        },
        changeValue: function(a, b) {
            let aValue = this.changeRefs[a.key];
            let bValue = this.changeRefs[b.key];

            if (aValue && bValue) {
                let aChange =
                    parseFloat(aValue) != "NaN" ? parseFloat(aValue) : aValue;
                let bChange =
                    parseFloat(bValue) != "NaN" ? parseFloat(bValue) : bValue;
                let direction =
                    typeof this.props.sortDirection !== "undefined"
                        ? this.props.sortDirection
                        : true;

                return direction ? aChange - bChange : bChange - aChange;
            }
        }
    };

    _triggerSend(asset) {
        this.setState({send_asset: asset}, () => {
            if (this.send_modal) this.send_modal.show();
        });
    }

    _showWindowSend = asset => {
        const _this = this;
        if (WalletDb.isLocked()) {
            WalletUnlockActions.unlock().then(() => {
                _this._onShowForm("transfer", asset);
            });
        } else {
            this._onShowForm("transfer", asset);
        }
    };



    _onShowForm(type, asset_id, dataForm) {
        const {account} = this.props;
        //const asset = ChainStore.getAsset(asset_id);
        //window.scrollTo(0, 0);
        this.props.onShowForm({
            form: type,
            from_name: account.get("name"),
            asset_id,
            asset: asset_id
        });
    }


    _getOrders(assetId) {
        const {account} = this.props;
        const accountOrders = account.get("orders");

        return accountOrders
            .toArray()
            .map(order => {
                let o = ChainStore.getObject(order);
                if (!o) return null;
                let sellBase = o.getIn(["sell_price", "base", "asset_id"]),
                    sellQuote = o.getIn(["sell_price", "quote", "asset_id"]);

                if ( sellBase === assetId ) {
                    const base = ChainStore.getAsset(sellBase);
                    const quote = ChainStore.getAsset(sellQuote);

                    if( quote && base ) {
                        const assets = {
                            [base.get("id")]: {precision: base.get("precision")},
                            [quote.get("id")]: {precision: quote.get("precision")}
                        };
                        let localLimitOption = new LimitOrder(o.toJS(), assets, quote.get("id"));
                        return utils.format_number(
                            localLimitOption.amountForSale().getAmount({real: true}),
                            quote.get("precision")
                        );
                    }
                }
            })
            .filter(a => !!a);

    }

    _renderBalances(balanceList) {
        const _this = this;
        const {
            account,
            account_name,
            coreSymbol,
            preferredUnit,
            settings,
            backedCoins,
            hiddenAssets,
            favoriteAssets,
            orders,
            assets
        } = this.props;

        //balance
        new Promise(resolve=>{
            let balances = [];
            // console.log("balanceList", balanceList)
            balanceList.map(asset=>{
                let assetId = asset.has("balance") ? asset.get("asset_type") : asset.get("id");
                const hasBalance = asset.has("balance") && asset.get("balance") > 0;
                const hasOnOrder = !!orders[asset.get("asset_type")];
                const inOrders = this._getOrders(assetId).map(inOrder=>{
                    return inOrder.replace(",", "");
                });
                const orderAmount = utils.format_number(inOrders.reduce((a, b) => Number(a) + Number(b), 0), asset.get("precision"), false);
                let ordersLink = inOrders.length ?
                    <Link to={["/account", account.get("name") ,"orders", asset.get("symbol")].join("/")}>
                        {orderAmount}
                    </Link> : "0";

                this.inOrders[asset.get("symbol")] = inOrders.length ? orderAmount.replace(",", "") : "0";

                /* let {market} = assetUtils.parseDescription(
                    asset.getIn(["options", "description"])
                );*/
                // const notCore = asset.get("id") !== "1.3.0";
                // const notCorePrefUnit = preferredUnit !== coreSymbol;
                // let preferredMarket = market ? market : preferredUnit;
                /*let directMarketLink = notCore
                    ? preferredMarket : notCorePrefUnit
                        ? preferredUnit : preferredUnit;
                */
                /*if( asset.get("symbol") === directMarketLink ) {
                }*/
                let directMarketLink = __GBLTN_CHAIN__ ? asset.get("symbol") === "GBLTEST" ? "USDT" : "GBLTEST" 
                : (__GBL_CHAIN__ ? asset.get("symbol") === "GBL" ? "USDT" : "GBL"
                : (__SCROOGE_CHAIN__ ? asset.get("symbol") === "SCROOGE" ? "USDT" : "SCROOGE"
                : asset.get("symbol") === "HAQQ" ? "USDT" : "HAQQ" ));

                const backedCoin = getBackedCoin(asset.get("symbol"), backedCoins);
                const canDeposit = backedCoin && backedCoin.depositAllowed;

                const canWithdraw = backedCoin && backedCoin.withdrawalAllowed;
                const canWithdrawDeexMigration = backedCoin && backedCoin.deexMigration;
                const canWithdrawDeexBalance = canWithdrawDeexMigration && hasBalance;
                const canWithdrawBalance = canWithdraw && hasBalance;

                const assetAmount = asset.get("balance");

                /* Sorting refs */
                let qtyRefValue = utils.get_asset_amount(
                    assetAmount,
                    asset
                );
                this.qtyRefs[getAlias(asset.get("symbol"))] = qtyRefValue;

                let preferredAsset = ChainStore.getAsset(preferredUnit);
                this.valueRefs[getAlias(asset.get("symbol"))] =
                    hasBalance && !!preferredAsset
                        ? MarketUtils.convertValue(
                            asset.get("balance"),
                            preferredAsset,
                            asset,
                            this.props.allMarketStats,
                            this.props.core_asset,
                            true
                        ) : 0;

                this.priceRefs[getAlias(asset.get("symbol"))] = !preferredAsset
                    ? 0
                    : MarketUtils.getFinalPrice(
                        this.props.core_asset,
                        asset,
                        preferredAsset,
                        this.props.allMarketStats,
                        true
                    ); 

                let marketId = asset.get("symbol") + "_" + preferredUnit;
                let currentMarketStats = this.props.allMarketStats.get(marketId);
                this.changeRefs[asset.get("symbol")] =
                    currentMarketStats && currentMarketStats.change ? currentMarketStats.change : 0;

                let isWithdraw, isMigration;
                if (__DEEX_CHAIN__ || __SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__) {
                    isWithdraw = canWithdraw && this.props.isMyAccount;
                    isMigration = canWithdrawDeexMigration && canWithdrawDeexBalance && this.props.isMyAccount;
                }

                // описание ассета
                let description = asset.get("options")._root.nodes[5].nodes[1].entry[1];
                let type = "-";
                let nftType;
                let nftPicture;
                if(description) {
                    type = JSON.parse(description).nft_object?.type;
                    nftType = JSON.parse(description).type;
                    nftPicture = 
                        JSON.parse(description).nft_object?.media_png ||
                        JSON.parse(description).nft_object?.media_gif ||
                        JSON.parse(description).nft_object?.media_jpeg || "";
                }
           
                balances.push(
                    <tr key={getAlias(asset.get("symbol"))} className={"table-row"}>
                        <td className={"table-item"} />
                        <td className={"table-item center"}>
                            <span
                                className={cname("table-item-star", { active: favoriteAssets.hasIn([account_name,assetId], false) })}
                                onClick={()=>SettingsActions.favoriteAsset(assetId, !favoriteAssets.hasIn([account_name,assetId]), account_name) }>
                            </span>
                        </td>
                        <td className={"table-item left asset-name"}>
                            <LinkToAssetById className={"table-item-link"} prefixLink={"/account-asset"} asset={assetId} showLogo />
                            {asset.get("symbol" === "DEEX") ?
                                <span style={{"fontSize": 12, "cursor": "pointer" }} onClick={()=>ModalActions.show("cryptoplat_io_buy")} >
                                    <Icon name="credit_card" title={"CARD buy"} style={{"marginRight": 5 }} />
                                    CARD buy
                                </span>:
                                null}
                            {/* ССЫЛКА НА КАРТИНКУ */}
                            {__SCROOGE_CHAIN__ ? null :
                                <>
                                   {(asset.get("symbol") === "BALLS") ? 
                                        <a 
                                            style={{color: "#8c979b", marginLeft: "20px", textDecoration: "underline", fontWeight: "bold"}} 
                                            target="_blank" 
                                            href={"https://static.haqq.exchange/gbl/images/ball.png"}>
                                            <Translate content={"nft.open_picture"}/>
                                        </a> : 
                                        <>
                                            {nftType === "nft" ?  
                                                <a 
                                                    style={{color: "#8c979b", marginLeft: "20px", textDecoration: "underline", fontWeight: "bold"}} 
                                                    target="_blank" 
                                                    href={nftPicture.length > 0 ? window.atob(nftPicture) : JSON.parse(description).image}>
                                                        <Translate content={"nft.open_picture"}/>
                                                    </a> : 
                                            null}
                                        </>}
                                       
                                </>}
                        </td>
                        {/* ТИП ТОКЕНА */}
                        {__SCROOGE_CHAIN__ ? null :
                        <td className={"table-item right"}>
                            <span>{type}</span>
                        </td>}
                        <td className={"table-item right"}>
                            {hasBalance || hasOnOrder ? (
                                <BalanceComponent balance={asset.get("balance_id")} hide_asset />
                            ) : "-"}

                        </td>
                        <td className={"table-item right"}>
                            {ordersLink}
                        </td>

                        <td className={"table-item right"}>
                            {hasBalance || hasOnOrder ? (
                                <BalanceValueComponent
                                    balance={asset.get("balance_id")}
                                    toAsset={preferredUnit}
                                    hide_asset
                                />
                            ) : 0}
                        </td>
                        <td className={"table-item"}>
                            <Link to={["/market", [asset.get("symbol"),directMarketLink].join("_")].join("/")}>
                                <Icon
                                    name="trade"
                                    title="icons.trade.trade"
                                    className="icon-14px"
                                />
                            </Link>
                        </td>

                        <td className={"table-item"}>
                            {hasBalance && this.props.isMyAccount ? (
                                <a onClick={()=>this._showWindowSend(assetId)}>
                                    <Icon
                                        name="send"
                                        title="icons.transfer"
                                    />
                                </a>
                            ) : "-" }
                        </td>
                        <td className={"table-item"}>

                            {canDeposit && this.props.isMyAccount ? (
                                <Icon
                                    style={{cursor: "pointer"}}
                                    name="deposit"
                                    title="icons.deposit.deposit"
                                    className="icon-14x"
                                    onClick={()=>this._onShowForm("deposit", asset.get("symbol"))}
                                />
                            ) : backedCoin && this.props.isMyAccount ? <ErrorIcon {...backedCoin}/> : "-"}
                        </td>
                        {__SCROOGE_CHAIN__ ?
                            <td className={"table-item"}>
                                {/*from deex20*/}
                                {__SCROOGE_CHAIN__ && (
                                    canWithdraw && this.props.isMyAccount ? (
                                        <Icon
                                            onClick={()=>canWithdrawBalance ? this._onShowForm("withdraw", asset.get("symbol")) : {} }
                                            name={canWithdrawBalance ? "withdraw" : "withdraw3"}
                                            title="icons.withdraw"
                                            className={cname("icon-14px", {})}
                                            style={{cursor: "pointer"}}
                                        />
                                    ) : backedCoin && !canWithdraw && this.props.isMyAccount ? <ErrorIcon {...backedCoin}/>  : "-"
                                )}

                                {/*from develop*/}
                                {!__SCROOGE_CHAIN__ && isMigration ? <Icon
                                    onClick={()=>this._onShowForm("deexWithdraw", asset.get("symbol") ) }
                                    name="withdraw"
                                    className="deex-withdraw"
                                    title="icons.withdraw"
                                    style={{
                                        cursor: "pointer",
                                        marginRight: isWithdraw ? 10 : 0
                                    }}
                                /> : null}
                                {!__SCROOGE_CHAIN__ && isWithdraw ? <Icon
                                    onClick={()=>canWithdrawBalance ? this._onShowForm("withdraw", asset.get("symbol")) : {} }
                                    name={canWithdrawBalance ? "withdraw": "withdraw3"}
                                    title="icons.withdraw"
                                    style={{
                                        cursor: canWithdrawBalance ? "pointer" : "default"
                                    }}
                                /> : null}
                                {!__SCROOGE_CHAIN__ && !isWithdraw && !isMigration ? <ErrorIcon {...backedCoin}/> : null}
                            </td> :
                            <td className={"table-item"}>
                                {/*from deex20*/}
                                {__DEEX_CHAIN__ && (
                                    canWithdraw && this.props.isMyAccount ? (
                                        <Icon
                                            onClick={()=>canWithdrawBalance ? this._onShowForm("withdraw", asset.get("symbol")) : {} }
                                            name={canWithdrawBalance ? "withdraw" : "withdraw3"}
                                            title="icons.withdraw"
                                            className={cname("icon-14px", {})}
                                            style={{cursor: "pointer"}}
                                        />
                                    ) : backedCoin && !canWithdraw && this.props.isMyAccount ? <ErrorIcon {...backedCoin}/>  : "-"
                                )}

                                {/*from develop*/}
                                {!__DEEX_CHAIN__ && isMigration ? <Icon
                                    onClick={()=>this._onShowForm("deexWithdraw", asset.get("symbol") ) }
                                    name="withdraw"
                                    className="deex-withdraw"
                                    title="icons.withdraw"
                                    style={{
                                        cursor: "pointer",
                                        marginRight: isWithdraw ? 10 : 0
                                    }}
                                /> : null}
                                {!__DEEX_CHAIN__ && isWithdraw ? <Icon
                                    onClick={()=>canWithdrawBalance ? this._onShowForm("withdraw", asset.get("symbol")) : {} }
                                    name={canWithdrawBalance ? "withdraw": "withdraw3"}
                                    title="icons.withdraw"
                                    style={{
                                        cursor: canWithdrawBalance ? "pointer" : "default"
                                    }}
                                /> : null}
                                {!__DEEX_CHAIN__ && !isWithdraw && !isMigration ? <ErrorIcon {...backedCoin}/> : null}
                            </td>
                        }
                        <td className={"table-item"}>
                            {this.props.isMyAccount ? (
                                <span
                                    className={cname("table-item-hidden-text", { active: hiddenAssets.hasIn([account_name,assetId], false) })}
                                    onClick={()=>SettingsActions.hideAsset(assetId, !hiddenAssets.hasIn([account_name,assetId]), account_name) }>
                                    {hiddenAssets.hasIn([account_name,assetId])
                                        ? <Translate content={"exchange.show"} />
                                        : <Translate content={"exchange.hide"} />
                                    }
                                </span> ) : null
                            }
                        </td>
                    </tr>
                );
            });
            resolve(balances);
        }).then(result=>{
            result.sort(_this.sortFunctions[_this.props.sortKey]);
            this.setState({
                balanceRows: result
            });
        });

    }

    render() {
        const {balanceRows} = this.state;

        return (
            <>
                <PaginatedList
                    style={{padding: 0}}
                    className="table-account"
                    rows={balanceRows}
                    header={this.props.header}
                    footer={this.props.footer}
                    withTransition={true}
                    pageSize={100}
                    label="utility.total_x_assets"
                    extraRow={this.props.extraRow}
                    leftPadding="1.5rem"
                >
                </PaginatedList>
            </>
        );
    }
}


const ErrorIcon = props => {
    const createLink = (data) => {
        if( data && data.href ) {
            window.open(data.href,"_blank");
        }
        return false;

    };
    let maintenanceMsg;
    if( props.maintenanceMsg ) {
        try {
            maintenanceMsg = JSON.parse(props.maintenanceMsg);
        } catch (e) {
            
        }
    }

    return (
        <Icon
            style={{cursor: maintenanceMsg ? "pointer" : "default" }}
            name="hammer"
            title={maintenanceMsg ? maintenanceMsg.title : "error.maintenance"}
            className={cname("icon-14x", { "icon-white1": maintenanceMsg && maintenanceMsg.href })}
            onClick={()=>createLink(maintenanceMsg)}
        />
    );
};

AccountPortfolioList = connect(
    AccountPortfolioList,
    {
        listenTo() {
            return [SettingsStore, GatewayStore, MarketsStore, AccountStore, WalletUnlockStore, AssetStore];
        },
        getProps() {
            return {
                settings: SettingsStore.getState().settings,
                favoriteAssets: SettingsStore.getState().favoriteAssets,
                viewSettings: SettingsStore.getState().viewSettings,
                backedCoins: GatewayStore.getState().backedCoins,
                bridgeCoins: GatewayStore.getState().bridgeCoins,
                gatewayDown: GatewayStore.getState().down,
                allMarketStats: MarketsStore.getState().allMarketStats,
                currentAccount: AccountStore.getState().currentAccount,
                locked: WalletUnlockStore.getState().locked,
                assets: AssetStore.getState().assets
            };
        }
    }
);

export default AccountPortfolioList;

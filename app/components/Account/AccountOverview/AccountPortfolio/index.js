import React from "react";
import {Checkbox} from "antd";
import classname from "classnames";
import WalletDb from "stores/WalletDb";
import loadable from "loadable-components";
import Translate from "react-translate-component";
import AssetName from "Components/Utility/AssetName";
import SettingsActions from "actions/SettingsActions";
import AccountPortfolioList from "../AccountPortfolioList";
import WalletUnlockActions from "actions/WalletUnlockActions";
import BindToChainState from "Components/Utility/BindToChainState";
import TotalBalanceValue from "components/Utility/TotalBalanceValue";

const SendTransfer = loadable(() => import("./SendTransfer"));
const DepositAddress = loadable(() => import("./DepositAddress"));
const SendWithdraw = loadable(() => import("./SendWithdraw"));
const SendDeexWithdraw = loadable(() => import("./SendDeexWithdraw"));

import "./AccountPortfolio.scss";
import counterpart from "counterpart";
//import {Asset} from "../../../../lib/common/MarketClasses";

const EmptyAside = () => {
    return (
        <div className={"account-portfolio-empty-aside"}>
            <Translate content={"account.info_block"} />
        </div>
    );
};

class AccountPortfolio extends React.Component {
    static defaultProps = {
        asideComponent: {
            "empty": EmptyAside,
            "transfer": SendTransfer,
            "withdraw": SendWithdraw,
            "deexWithdraw": SendDeexWithdraw,
            "deposit": DepositAddress
        }

    };

    constructor(props) {
        super(props);

        this.state = {
            send_asset: null,
            alwaysShowAssets: null,
            alwaysShowAsset: __SCROOGE_CHAIN__ ? ["SCROOGE"] : ["DEEX"],
            shownAssets: props.viewSettings.get("shownAssets", "active"),
            sortKey: props.viewSettings.get("portfolioSort", "totalValue"),
            sortDirection: props.viewSettings.get(
                "portfolioSortDirection",
                true
            ),
            rightAside: {}
        };

        this.qtyRefs = {};
        this.priceRefs = {};
        this.valueRefs = {};
        this.changeRefs = {};
        this.modalRef = {};


        this._showWindowSend = this._showWindowSend.bind(this);
        this._onSettleAsset = this._onSettleAsset.bind(this);
        this._handleFilterInput = this._handleFilterInput.bind(this);
        this._toggleSortOrder = this._toggleSortOrder.bind(this);


    }

    componentDidUpdate(prevProps){
        const { filters , hiddenAssets, favoriteAssets, account_name }=this.props;
        if( filters.filterHidden && (hiddenAssets.get(account_name) && !hiddenAssets.get(account_name).size) ) {
            this.props.onChangeVisibleAsset({
                name: "filterHidden",
                value: false
            });
        }
        if( filters.filterFavorite && (favoriteAssets.get(account_name) && !favoriteAssets.get(account_name).size) ) {
            this.props.onChangeVisibleAsset({
                name: "filterFavorite",
                value: false
            });
        }

        if( this.props.isMyAccount !== prevProps.isMyAccount) {
            this.onShowForm("empty");
        }
    }

    _handleFilterInput = event => {
        event.preventDefault();
        this.setState({
            filterValue: event.target.value
        });

        this.props.onChangeVisibleAsset({
            name: "filterValue",
            value: event.target.value
        });
    };

    _hideAsset = (asset, status) => {
        SettingsActions.hideAsset(asset, status);
    };


    _changeShownAssets = shownAssets => {
        if (this.state.shownAssets !== shownAssets) {
            this.setState({shownAssets});
            SettingsActions.changeViewSetting({shownAssets});
        }
    };


    _onSettleAsset = (id, e) => {
        e.preventDefault();
        this.setState({
            settleAsset: id
        });

        this.settlement_modal.show();
    };

    _toggleSortOrder = sortKey => {
        const {sortDirection} = this.state;

        SettingsActions.changeViewSetting({
            portfolioSort: sortKey,
            portfolioSortDirection: !sortDirection
        });
        this.setState({
            sortKey: sortKey,
            sortDirection: !sortDirection
        });
    };

    _triggerSend = asset => {
        this.setState({send_asset: asset}, () => {
            if (this.send_modal) this.send_modal.show();
        });
    };

    _showWindowSend = asset => {
        const _this = this;
        if (WalletDb.isLocked()) {
            WalletUnlockActions.unlock().then(() => {
                _this._triggerSend(asset);
            });
        } else {
            this._triggerSend(asset);
        }
    };

    onChangeCheckbox = (event) => {
        this.setState({
            [event.target.name]: event.target.checked
        });

        SettingsActions.changeViewSetting({
            [event.target.name]: event.target.checked
        });

        this.props.onChangeVisibleAsset({
            name: event.target.name,
            value: event.target.checked
        });

    };

    onShowForm = (payload) => {
        this.setState({
            rightAside: payload
        });
    };


    render() {
        const {settings, account_name, core_asset, hiddenAssets, favoriteAssets, assetBalancesList,
            filters, visibleBalancesList, asideComponent} = this.props;
        const {rightAside, filterValue, alwaysShowAsset} = this.state;
        const preferredUnit = settings.get("unit") || core_asset.get("symbol");
        const AsideComponent = asideComponent[rightAside.form || "empty"];
        // let showAssetPercent = settings.get("showAssetPercent", false);
        // let includedBalances, hiddenBalances;
        // let account_balances = account.get("balances");

        // console.log("assetBalancesList", assetBalancesList.toJS() , this.props );
        // console.log("AccountPortfolio.jsx render", preferredUnit);
        // console.log("TranslateWithLinks render", counterpart.translate("account.total"));

        console.log("settings", settings)

        const includedPortfolioBalance = (
            <tr key="portfolio" className="table-total-value">
                <td colSpan="5" style={{textAlign: "left"}}>

                    <Translate
                        content={"account.asset.total"}
                        asset={preferredUnit}
                    />
                </td>
                <td style={{textAlign: "center"}}>
                    <TotalBalanceValue noTip noPrefix balances={visibleBalancesList}/>
                </td>
                <td colSpan="4"/>
            </tr>
        );
        

        return (
            <div className={"account-portfolio"} >
                <div className="account-portfolio-content">
                    <div className="header-selector">
                        <div className="header-selector-search">
                            <input
                                type="text"
                                placeholder={counterpart.translate("markets.search")}
                                onChange={this._handleFilterInput}
                            />
                        </div>
                        <div className="header-selector-filter">
                            <Checkbox
                                name={"hideNull"} 
                                onChange={(event) => this.onChangeCheckbox(event)}
                                checked={filters.hideNull}
                                className={"wallet-checkbox"}>
                                <Translate content="account.hide_null"/>
                            </Checkbox>
                            {favoriteAssets.get(account_name) && favoriteAssets.get(account_name).size ? (
                                <Checkbox
                                    name={"filterFavorite"} onChange={(event) => this.onChangeCheckbox(event)}
                                    checked={filters.filterFavorite}>
                                    <Translate content="account.see_favorite"/>
                                </Checkbox>
                            ) : null}
                            {hiddenAssets.get(account_name) && hiddenAssets.get(account_name).size ? (
                                <Checkbox
                                    name={"filterHidden"} onChange={(event) => this.onChangeCheckbox(event)}
                                    checked={filters.filterHidden}>
                                    <Translate content="account.shown_hidden"/>
                                </Checkbox>
                            ) : null}
                        </div>
                    </div>


                    <AccountPortfolioList
                        balanceList={visibleBalancesList}
                        optionalAssets={
                            !filterValue ? alwaysShowAsset : null
                        }
                        visible={false}
                        preferredUnit={preferredUnit}
                        coreSymbol={this.props.core_asset.get("symbol")}
                        header={<TableHead
                            preferredUnit={preferredUnit}
                            sortKey={this.state.sortKey}
                            sortDirection={this.state.sortDirection}
                            toggleSortOrder={this._toggleSortOrder} {...this.props} />}
                        footer={includedPortfolioBalance}
                        onShowForm={this.onShowForm}
                        {...this.props}
                        {...this.state}
                    />
                </div>

                <div className={classname("account-portfolio-sticky" )}  >
                    <div className={classname("account-portfolio-form")} >
                        <AsideComponent
                            onShowForm={this.onShowForm}
                            {...Object.assign({},this.props, rightAside)} />
                    </div>
                </div>

            </div>
        );
    }
}

export default  BindToChainState(AccountPortfolio);

class TableHead extends React.Component {

    activeClassName = (typeName) => {
        const {sortKey, sortDirection} = this.props;
        return {
            "active": typeName === sortKey,
            "up": typeName === sortKey && sortDirection,
            "down": typeName === sortKey && !sortDirection,
        };
    };

    render() {
        const {preferredUnit, toggleSortOrder} = this.props;
            
        // console.log("TableHead alphabetic", this.activeClassName("alphabetic"))
        // console.log("TableHead qty", this.activeClassName("qty"))
        // console.log("TableHead inOrders", this.activeClassName("inOrders"))
        // console.log("TableHead totalValue", this.activeClassName("totalValue"))

        return (
            <tr>
                <th className={"table-head-item"} />
                <th className={"table-head-item center"}>
                    <i className={"table-head-item-star"}/>
                </th>
                <th
                    className={classname("table-head-item alphabetic left clickable", this.activeClassName("alphabetic"))}
                    onClick={toggleSortOrder.bind(this, "alphabetic")}
                >
                    <Translate
                        component="span"
                        content="account.asset.active"
                    />
                </th>
                {__SCROOGE_CHAIN__ ? null :
                <th
                    className={classname("table-head-item center clickable")}
                >
                   <Translate content="account.type_header"/>
                </th>}
                <th
                    onClick={toggleSortOrder.bind(this, "qty")}
                    className={classname("table-head-item center clickable", this.activeClassName("qty"))}
                >
                    <Translate content="account.qty"/>
                </th>
                <th onClick={toggleSortOrder.bind(this, "inOrders")}
                    className={classname("table-head-item center clickable", this.activeClassName("inOrders"))}>
                    <Translate content="account.order_header"/>
                </th>
                <th onClick={toggleSortOrder.bind(this, "totalValue")}
                    className={classname("table-head-item center clickable", this.activeClassName("totalValue"))}>
                    <AssetName name={preferredUnit} noTip noLink noPrefix/>&nbsp;
                    <Translate content="account.value_header"/>
                </th>

                <th className={"table-head-item"}>
                    <Translate content="account.trade"/>
                </th>
                <th className={"table-head-item"}>
                    <Translate content="header.payments"/>
                </th>

                <th className={"table-head-item"}>
                    <Translate content="modal.deposit.table_th"/>
                </th>
                <th className={"table-head-item"}>
                    <Translate content="modal.withdraw.table_th"/>
                </th>
                <th className={"table-head-item"} />

            </tr>

        );
    }
}

import React from "react";
import Translate from "react-translate-component";
import IntlStore from "stores/IntlStore";
import {Col, Form, Input, Row, Spin, Typography} from "antd";
import ChainTypes from "Components/Utility/ChainTypes";
import BindToChainState from "Components/Utility/BindToChainState";
import ModalActions from "actions/ModalActions";
import {getAlias} from "config/alias";
import BalanceComponent from "Components/Credit/Modals/PostDeposit";
import TransferWrapper from "Components/Utility/TransferWrapper";
import {ChainStore} from "bitsharesjs";
import FeeAssetSelector from "Components/Utility/FeeAssetSelector";
import debounce from "lodash-es/debounce";

import "./DepositAddress.scss"
import {Asset} from "../../../../lib/common/MarketClasses";
import {connect} from "alt-react";
import AccountStore from "../../../../stores/AccountStore";

const { Text } = Typography;

function toUTF8Array(str) {
    var utf8 = [];
    for (var i=0; i < str.length; i++) {
        var charcode = str.charCodeAt(i);
        if (charcode < 0x80) utf8.push(charcode);
        else if (charcode < 0x800) {
            utf8.push(0xc0 | (charcode >> 6),
                0x80 | (charcode & 0x3f));
        }
        else if (charcode < 0xd800 || charcode >= 0xe000) {
            utf8.push(0xe0 | (charcode >> 12),
                0x80 | ((charcode>>6) & 0x3f),
                0x80 | (charcode & 0x3f));
        }
        // surrogate pair
        else {
            i++;
            // UTF-16 encodes 0x10000-0x10FFFF by
            // subtracting 0x10000 and splitting the
            // 20 bits of 0x0-0xFFFFF into two halves
            charcode = 0x10000 + (((charcode & 0x3ff)<<10)
                | (str.charCodeAt(i) & 0x3ff));
            utf8.push(0xf0 | (charcode >>18),
                0x80 | ((charcode>>12) & 0x3f),
                0x80 | ((charcode>>6) & 0x3f),
                0x80 | (charcode & 0x3f));
        }
    }
    return utf8;
}

class PayForm extends React.Component {

    static defaultProps = {};

    state = {
        collateral: null,
        showFeeAsset: false,
        loading: false,
        loan: null,
    };

    constructor(props){
        super(props);

        this.setNewAsset = debounce(this.setNewAsset.bind(this), 300);
    }

    getShowError = (name) => {
        const {isFieldTouched, getFieldError}= this.props.form;
        const hasValueError = isFieldTouched(name) && getFieldError(name);
        return {
            validateStatus: hasValueError ? "error" : "",
            help: hasValueError || ""
        };
    };

    componentDidUpdate(prevProps) {
        if( prevProps.hasBalance !== this.props.hasBalance ){
            this.setState({
                loading: false
            });
        }
        if( prevProps.balanceError !== this.props.balanceError ){
            console.log(">>>>>>>>",  this.props );
            this.setNewAsset();

        }
    }

    componentDidMount() {
        // this.sendNameLookUp1("dim");
    }

    setNewAsset = () => {
        console.log("this.state.showFeeAsset >>>>>>", this.state.showFeeAsset);
        if(!this.state.showFeeAsset ) {
            // this.props.setAsset(this.props.asset.get("symbol"));
            /*this.props.setFeeAsset({
                asset: this.props.asset
            });*/
        }
    };

    sendNameLookUp1 = async (name) => {
        let requestData = {
            "jsonrpc": "2.0",
            "method": "get_account_by_name",
            "params": [name],
            "id": 1
        };
        try {
            //let toUTFArray = toUTF8Array(JSON.stringify(requestData));
            let data = await fetch("https://node2.private.deexnet.com/ws", {
                method: "POST",
                body: JSON.stringify(requestData)
            });
            console.log("data", data);
        } catch (e) {
            console.log(e);
        }
    };

    submitForm = (event) => {
        const { submitTransfer, callBack } = this.props;
        this.setState({loading: true});
        submitTransfer(event, true)
            .then(()=>{
                if( callBack ) callBack();
            })
            .catch(error=>{
                console.log("error", error)
                this.setState({loading: false});
            });
    };

    showModalDW = (asset, typeWindow) => {
        const { from_account , closeModal } = this.props;
        closeModal();
        let dataModal = {
            account: from_account.get("name"),
            asset: asset.get("id"),
            asset_id: asset.get("id"),
            onShowForm: () => {
                ModalActions.hide("transfer_asset_modal");
            }
        };

        ModalActions.show("transfer_asset_modal", {
            typeWindow,
            dataModal
        });
    };




    render() {
        const {form, balanceError} = this.props;
        const {getFieldDecorator} = form;

        console.log("this.props >>>> ", this.props);

        return (
            <Spin spinning={false}>
                <Form onSubmit={(event)=>this.submitForm(event)} noValidate >
                    <Row>
                        <Col>
                            <Form.Item label={<Translate content={"transfer.send_migrate"}/>} >
                                {getFieldDecorator("memo", {
                                    rules: [
                                        {
                                            required: true
                                        },
                                    ],
                                })(<Input onChange={event => this.props.setMemo("migration:" + event.target.value)}
                                />) }
                            </Form.Item>
                        </Col>
                    </Row>
                    {console.log("this.state.showFeeAsset", this.state.showFeeAsset)}
                    {balanceError || this.state.showFeeAsset ?
                        <FeeAssetSelector
                            label="transfer.fee"
                            account={this.props.from_account}
                            trxInfo={{
                                type: "transfer",
                                options: ["price_per_kbyte"],
                                data: {
                                    type: "memo",
                                    content: this.props.memo
                                }
                            }}
                            onChange={(result)=>{
                                this.setState({
                                    showFeeAsset: true
                                });
                                // this.props.setAsset(result.get("symbol"));
                                this.props.setFeeAsset({
                                    asset: result
                                });
                            }}
                        /> : null}

                    {balanceError ? <Row>
                        <Translate content="transfer.errors.noFunds"/>
                    </Row>: null}

                    <Row style={{
                        "textAlign": "center",
                        "marginTop": "30px",
                    }}>
                        <Translate disabled={balanceError} type={"submit"} content={"transfer.send"} className={"btn btn-green"} component={"button"}  />
                    </Row>
                </Form>
            </Spin>
        );
    }
}
const PayFormCreate = Form.create()(PayForm);
const PayFormTransfer = TransferWrapper(PayFormCreate, {});

class SendDeexWithdraw extends React.Component {
    static propTypes = {
        asset: ChainTypes.ChainAsset.isRequired
    };

    state = {
        requestLoad: false
    };


    render() {
        const {requestLoad} = this.state;
        const {account, backedCoins, asset} = this.props;
        const coin = backedCoins.get("TDEX")
            .filter(coins=>asset.get("symbol") === coins.symbol).pop()

        const balanceID = account.getIn(["balances", asset.get("id")]);
        let balanceObject = ChainStore.getObject(balanceID);

        let assetAmount = new Asset({
            amount: balanceObject.get("balance"),
            asset_id: asset.get("id"),
            precision: asset.get("precision")
        });


        const postDepositData = {
            amount: assetAmount.getAmount({real: 1}),
            asset: asset.get("id"),
            memo: null,
            to_account: coin?.deexMigration?.account,
            from_account: account.get("name"),
            callBack: () => {
                this.setState({
                    requestLoad: false
                });
            }
        };

        console.log("postDepositData", postDepositData);

        return (
            <Spin spinning={requestLoad}>
                <div className="deposit-address">
                    <div className="deposit-address-header">
                        <Translate
                            component={"div"}
                            className={"deposit-address-title"}
                            content="account.withdraw" with={{
                                symbol: getAlias(coin.symbol)
                            }} />
                    </div>
                    <br/>
                    <br/>
                    <div className="deposit-address-body">
                        <PayFormTransfer {...postDepositData} />
                    </div>
                </div>
            </Spin>
        );
    }
}

const SendDeexWithdrawBind = BindToChainState(SendDeexWithdraw);

export default connect(
    SendDeexWithdrawBind,
    {
        listenTo() {
            return [IntlStore];
        },
        getProps() {
            return {
            };
        }
    }
);
import React from "react";
import {Radio, Spin} from "antd";
import Translate from "react-translate-component";
import { FetchChain, ChainStore } from "deexjs";
import AmountSelector from "components/Utility/AmountSelector";
import AccountStore from "stores/AccountStore";
import TransactionConfirmStore from "stores/TransactionConfirmStore";
import {Asset} from "common/MarketClasses";
import { checkBalance, checkFeeStatusAsync } from "common/trxHelper";
import BalanceComponent from "components/Utility/BalanceComponent";
import AccountActions from "actions/AccountActions";
import utils from "common/utils";
import { connect } from "alt-react";
import classnames from "classnames";
import PropTypes from "prop-types";

import InputMask from "react-input-mask";

import BindToChainState from "Components/Utility/BindToChainState";
import {validateAddress, WithdrawAddresses} from "common/TDexMethods";

import "./SendWithdraw.scss";
import GatewayStore from "../../../../stores/GatewayStore";
import ChainTypes from "Components/Utility/ChainTypes";
// import {FormattedNumber} from "react-intl";
import {getAlias} from "../../../../config/alias";
import FormattedAsset from "Utility/FormattedAsset";
import ModalActions from "../../../../actions/ModalActions";
import CryptoJS from "crypto-js";
import {ymSend} from "../../../../lib/common/metrika";
import FeeAssetSelector from "Components/Utility/FeeAssetSelector";
import SwitchableWalletType from "./SwitchableWalletType";


class SendWithdraw extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    static defaultProps = {
        fee_asset_id: "1.3.0",
    };

    static propTypes = {
        account: ChainTypes.ChainAccount.isRequired,
        asset: ChainTypes.ChainAsset.isRequired
    };

    constructor(props) {
        super(props);
        this.state = this.getState();

        this.getSendError = this.getSendError.bind(this);
        this.onReceiveChanged = this.onReceiveChanged.bind(this);
        this.onAmountChanged = this.onAmountChanged.bind(this);
        this.onAccountBalance = this.onAccountBalance.bind(this);
        if (__DEEX_CHAIN__ || __SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__) {
            this.getMemoContent = this.getMemoContent.bind(this);
        }
    }

    componentDidMount(){
        this.getCoin();

    }

    componentDidUpdate(prevProps){
        if( prevProps.asset.get("id") !== this.props.asset.get("id") ) {
            this.setState(this.getState(), this.getCoin);
        }
    }

    getState = () => {



        // compress(JSON.stringify(other), 1, result=>{

        return {
            amount: 0,
            balance: 0,
            amountSend: new Asset({amount: 0}),
            amountSendValue: 0,
            amountSendError: null,
            amountReceive: new Asset({amount: 0}),
            amountReceiveValue: 0,
            fee_asset_id: this.props.fee_asset_id,
            feeAmount: new Asset({
                amount: 0,
                asset_id: this.props.fee_asset_id
            }),
            feeAmountValue: 0,

            gateFeeAssetAmount: new Asset({
                amount: 0,
            }),
            gateFeeAmountBalance: new Asset({
                amount: 0,
            }),
            feeAssetBalance: new Asset({
                amount: 0,
                asset_id: this.props.fee_asset_id
            }),
            commissionAsset: {},
            coin: {},
            memo: "",
            walletType: "",
            UNIQHASH: "",

            isPopoverOpen: false,
            requestLoad: false,

            withdraw_amount: 0,
            withdraw_address_check_in_progress: true,
            withdraw_address_is_valid: null,
            options_is_valid: false,
            withdraw_address_selected: WithdrawAddresses.getLast(
                this.props.output_wallet_type
            ),
            withdraw_address: WithdrawAddresses.getLast(
                this.props.output_wallet_type
            ),
        };
    };

    getUniqHash = () => {
        const {asset, account} = this.props;
        const uniqString = JSON.stringify({
            asset_id: asset.get("id"),
            account_id: account.get("id"),
            timestamp: new Date().getTime()
        });
        var passhash = CryptoJS.MD5(uniqString);

        this.setState({
            UNIQHASH: passhash.toString()
        });
    };

    getBalance = () => {
        let { account, asset }         = this.props;
        let balance = account.get("balances").toJS()[asset.get("id")];

        this.setState({
            balance: balance
        });
    };

    getCoin = () => {
        let { backedCoins, asset }         = this.props;
        let coin = backedCoins.get("TDEX")
            .filter(coins=>coins.symbol === asset.get("symbol")).shift();

        let switchableWalletType = coin?.switchableWalletType?.[coin.walletType];

        this.setState({
            coin: coin,
            issuer: switchableWalletType ? switchableWalletType?.issuer : coin.issuer,
            walletType: coin.walletType
        }, ()=>{
            this.getBalance();
            this.getCommission();
            this.getGateAssetFee();
            this.getUniqHash();
        });
    };
    getCommission = () => {
        const {coin} = this.state;
        let comissionValue = !parseFloat(coin.withdrawalPercentageFee)
            ? coin.withdrawalStaticFee : coin.withdrawalPercentageFee;
        if( comissionValue ) {
            let commissionAsset = {
                value: comissionValue,
                percent: !!parseFloat(coin.withdrawalPercentageFee)
            };
            this.setState({commissionAsset});
        }
    };

    getGateAssetFee = () => {
        const _this = this;
        const {account} = this.props;
        const {coin} = this.state;
        let account_balances = account.get("balances").toJS();
        if( coin.withdrawalAdditionalFeeAmount && coin.withdrawalAdditionalFeeAssetId ) {
            FetchChain("getAsset", coin.withdrawalAdditionalFeeAssetId).then(gateAsset => {
                let gateFeeAssetAmount = new Asset({
                    asset_id: gateAsset.get("id"),
                    precision: gateAsset.get("precision"),
                    amount: coin.withdrawalAdditionalFeeAmount
                });
                let gateFeeAmountBalance = new Asset({
                    asset_id: gateAsset.get("id"),
                    precision: gateAsset.get("precision"),
                    amount: 0
                });
                let balanceId = account_balances[gateAsset.get("id")];
                if( balanceId ) {
                    let balance_object = ChainStore.getObject(balanceId);
                    gateFeeAmountBalance.setAmount({
                        sats: this.getMaxAmount(balance_object.get("balance"))
                    });
                }


                _this.setState({
                    gateAsset,
                    gateFeeAssetAmount,
                    gateFeeAmountBalance
                });
            });
        }
    };


    getMaxAmount = (balance) => {
        const {coin} = this.state;
        let maxAsset, maxAmount;
        if( coin.withdrawMaxAmount > 0 ) {
            maxAsset = new Asset({
                amount: coin.withdrawMaxAmount,
                precision: coin.precision,
                asset_id: coin.issuerId
            });
            maxAmount = maxAsset.getAmount({real: false});
        }
        return maxAmount && balance > maxAmount ? maxAmount : balance;
    };
    getMinAmount = (balance) => {
        const {coin} = this.state;
        const minAsset = new Asset({
            amount: coin.withdrawMinAmount || 0,
            precision: coin.precision,
            asset_id: coin.issuerId
        });
        return balance.gte(minAsset) ? balance : minAsset;
    };

    onFeeChanged(asset) {
        const {fee_asset_id} = this.state;
        if(fee_asset_id !== asset.get("id")) {
            this.setState({
                fee_asset_id: asset.get("id"),
                error: null
            }, ()=>this._updateFee());
        }
    }

    /*
    * Set maximum amount
    * */
    onAccountBalance(balance_id, asset_id){
        const {asset} = this.props;
        let balance_object = ChainStore.getObject(balance_id);

        if (balance_object) {
            let amountSend = new Asset({
                asset_id: asset_id,
                precision: asset.get("precision"),
                amount: this.getMaxAmount(balance_object.get("balance")),
            });

            this.onAmountChanged({
                asset,
                amount: amountSend.getAmount({real: true})
            });

           /* let minAmount = this.getMinAmount(amountSend);

            let hasAmountError = amountSend.getAmount({real: true}) > 0 &&
                amountBalance.lt(amountSend) &&
                amountSend.lte(minAmount);
            */
            /*this.setState({
                amountSend,
                amountSendValue: amountSend.getAmount({real: true}),
            }, this.getToReceive );*/
        }
    }

    onReceiveChanged({amount, asset}) {
        
        const {amountSend, commissionAsset} = this.state;
        
        if( parseFloat(amount) ) {

            let amountSendValue = 0;
            let commisionAmount = new Asset({
                asset_id: asset.get("id"),
                precision: asset.get("precision"),
                amount: commissionAsset.value,
            });
            let receiveAmount = new Asset({
                asset_id: asset.get("id"),
                precision: asset.get("precision"),
                real: amount,
            });
            let cloneReceiveAmount = receiveAmount.clone();
            
            if( commissionAsset.percent ) {
                amountSendValue = cloneReceiveAmount.getAmount({real: false}) * 100 / (100 - commissionAsset.value);
                amountSend.setAmount({
                    sats: Math.round(amountSendValue)
                });
            } else {
                cloneReceiveAmount.plus(commisionAmount);
                amountSend.setAmount({
                    sats: cloneReceiveAmount.getAmount({real: 0})
                });
                //amountSendValue = cloneReceiveAmount.getAmount({real: 0});
            }
            
            
            this.setState({
                amountSend,
                amountSendValue: amountSend.getAmount({real: 1}),
                amountReceive: receiveAmount,
                amountReceiveRealValue: receiveAmount.getAmount({real: true}),
                amountReceiveValue: utils.format_asset(receiveAmount.getAmount({real: true}), asset, true, false )
            }, this.getSendError);
        } else {
            this.setState({
                amountReceiveRealValue: amount,
            }, this.getSendError);
        }
        
    }

    onAmountChanged({amount, asset}) {
        
        let setAmount = amount || 0;

        let amountSend = new Asset({
            asset_id: asset.get("id"),
            amount: setAmount,
            precision: asset.get("precision"),
        });

        amountSend.setAmount({real: setAmount});

       

        this.setState({
            amountSend,
            amountSendValue: setAmount
        }, ()=>{
            
            this.getToReceive(); 
        });

    }

    getToReceive(){
        const {amountSend, commissionAsset} = this.state;
        const {asset} = this.props;

        let commisionAmount = new Asset({
            asset_id: asset.get("id"),
            precision: asset.get("precision"),
            amount: commissionAsset.value,
        });
        let receiveAmount = amountSend.clone();
        if (receiveAmount) {
            if( commissionAsset.percent ) {
                let getPercent = receiveAmount.getAmount({real: false}) / 100 * commissionAsset.value;
                commisionAmount = new Asset({
                    asset_id: asset.get("id"),
                    precision: asset.get("precision"),
                    amount: Math.round(getPercent),
                });
            }

            if( receiveAmount.asset_id === commisionAmount.asset_id ){
                receiveAmount.minus(commisionAmount);
            }

            this.setState({
                amountReceive: receiveAmount,
                amountReceiveRealValue: receiveAmount.getAmount({real: true}),
                amountReceiveValue: utils.format_asset(receiveAmount.getAmount({real: true}), asset, true, false )
            }, this.getSendError);
        }
    }

    getSendError(){
        const {amountSend, amountSendValue, balance} = this.state;
        
        if( amountSend ) {
            let amountBalance = new Asset({
                asset_id: amountSend.id,
                amount: amountSendValue,
                precision: amountSend.precision
            });
            if( balance ) {
                let balance_object = ChainStore.getObject(balance);
                amountBalance.setAmount({
                    sats: this.getMaxAmount(balance_object.get("balance"))
                });
            }
            let minAmount = this.getMinAmount(amountSend);
    
    
            let amountSendError = null;
            if( !amountSend.hasAmount() || amountSend.gt(amountBalance)) {
                amountSendError = "modal.withdraw.cannot_cover";
            } else if ( minAmount.gt(amountSend) ) {
                amountSendError = "modal.withdraw.cannot_cover";
            }
    
            this.setState({amountSendError});
        }
    }

    _getGateFee = () => {
        let {coin, UNIQHASH, fee_asset_id} = this.state;
        let {account} = this.props;
        const memo = ["dex", UNIQHASH];
        return new Promise(resolve => {
            if( coin.withdrawalAdditionalFeeAmount && coin.withdrawalAdditionalFeeAssetId ) {

                checkFeeStatusAsync({
                    accountID: account.get("id"),
                    feeID: fee_asset_id,
                    options: ["price_per_kbyte"],
                    data: {
                        type: "memo",
                        content: memo.join(":")
                    }
                }).then(resolve);
            } else  {
                resolve({
                    fee: new Asset({
                        asset_id: fee_asset_id,
                        precision: 4,
                        amount: 0,
                    })
                });
            }
        });
    };

    getMemoContent(){
        let {coin, memo, withdraw_address, UNIQHASH} = this.state;

        let dataContent;
        if( coin.backingCoin ) {
            dataContent = [coin.backingCoin.toLowerCase(), withdraw_address, UNIQHASH];
            if( memo ) {
                dataContent.push(memo);
            }
        }

        return dataContent;
    }

    _updateFee() {
        let {account} = this.props;
        let {fee_asset_id, coin} = this.state;
        let dataContent = this.getMemoContent();

        console.log("dataContent", dataContent.join(":"));
        console.log("fee_asset_id", fee_asset_id );

        if (!account || !coin) return null;
        checkFeeStatusAsync({
            accountID: account.get("id"),
            feeID: fee_asset_id,
            options: ["price_per_kbyte"],
            data: {
                type: "memo",
                content: dataContent.join(":")
            }
        }).then(({fee, hasBalance, hasPoolBalance, feeBalance}) => {
            //if (this.unMounted) return;
            this._getGateFee().then(gateFee => {

                let gateFeeCheck = gateFee.fee.clone();

                gateFeeCheck.plus(fee);

                

                const hasCurrentBalance = feeBalance && feeBalance.get("balance") >= gateFeeCheck.getAmount();

                let feeAssetBalance = new Asset({
                    asset_id: fee_asset_id,
                    precision: gateFeeCheck.precision,
                    real: 0
                });

                if( feeBalance ) {
                    

                    feeAssetBalance.setAmount({
                        sats: feeBalance.get("balance")
                    });

                    //
                    /*feeAssetBalance.setAmount({
                        sats: feeAssetBalance.getAmount({real: 0})
                    });*/
                }

                this.setState({
                    feeAmount: gateFeeCheck,
                    hasBalance: hasCurrentBalance,
                    feeAssetBalance,
                    hasPoolBalance,
                    error: !hasCurrentBalance || !hasPoolBalance
                }, this._checkBalance);
            });
        });
    }

    _checkBalance() {
        const {feeAmount, withdraw_amount, balance} = this.state;
        const {asset} = this.props;
        if (!balance || !feeAmount) return;

        const hasBalance = checkBalance(
            withdraw_amount,
            asset,
            feeAmount,
            balance
        );
        if (hasBalance === null) return;
        this.setState({balanceError: !hasBalance});
        return hasBalance;
    }

    onDropDownList() {
        if (WithdrawAddresses.has(this.props.output_wallet_type)) {
            if (this.state.options_is_valid === false) {
                this.setState({options_is_valid: true});
                this.setState({withdraw_address_first: false});
            }

            if (this.state.options_is_valid === true) {
                this.setState({options_is_valid: false});
            }
        }
    }

    onWithdrawAddressChanged(e) {
        let new_withdraw_address = e.target.value.trim();

        new Promise((resolve)=> {
            //let withdraw_address = new_withdraw_address.replace(/\s|\(|\)/g, "")
            this.setState({
                withdraw_address: new_withdraw_address,
                withdraw_address_check_in_progress: true,
                withdraw_address_selected: new_withdraw_address,
                withdraw_address_is_valid: null
            }, resolve );
        }).then(()=>{
            this._updateFee();
            //this._validateAddress(new_withdraw_address);
        });


    }

    onSelectChanged(index) {
        let new_withdraw_address = WithdrawAddresses.get(
            this.props.output_wallet_type
        )[index];
        WithdrawAddresses.setLast({
            wallet: this.props.output_wallet_type,
            address: new_withdraw_address
        });

        this.setState({
            withdraw_address_selected: new_withdraw_address,
            options_is_valid: false,
            withdraw_address: new_withdraw_address,
            withdraw_address_check_in_progress: true,
            withdraw_address_is_valid: null
        }, ()=>{
            this._updateFee();
            this._validateAddress(new_withdraw_address);
        });

    }

    _validateAddress(new_withdraw_address, props = this.props) {
        const _this = this;
        const {withdraw_address, coin} = this.state;

        

        validateAddress({
            url: props.url,
            newAddress: new_withdraw_address,
            symbol: coin.symbol,
            walletType: this.state.walletType
        }).then(isValid => {
            // 
            if (withdraw_address === new_withdraw_address) {
                _this.setState({
                    requestLoad: false,
                    withdraw_address_check_in_progress: false,
                    withdraw_address_is_valid: isValid
                });
            } else {
                _this.setState({
                    requestLoad: false,
                });
            }
        }).catch(error => {
            console.log("error", error);
            _this.setState({
                requestLoad: false,
                error
            });
        });
    }

    onSubmit = () => {
        const _this = this;
        const {coin, amountSendValue, withdraw_address_check_in_progress,
            feeAmount, memo, withdraw_address, withdraw_address_is_valid, UNIQHASH} = this.state;
        const {asset, account} = this.props;
        // debugger;
        let  output_wallet_type = this.state.walletType;
        if (
            !withdraw_address_check_in_progress
            && (withdraw_address && withdraw_address.length)
            && amountSendValue !== null
        ) {
            if (!withdraw_address_is_valid) {
                
            } else if (parseFloat(amountSendValue) > 0) {
                if (!WithdrawAddresses.has(output_wallet_type)) {
                    let withdrawals = [];
                    withdrawals.push(withdraw_address);
                    WithdrawAddresses.set({
                        wallet: output_wallet_type,
                        addresses: withdrawals
                    });
                } else {
                    let withdrawals = WithdrawAddresses.get(
                        output_wallet_type
                    );
                    if (
                        withdrawals.indexOf(withdraw_address) === -1
                    ) {
                        withdrawals.push(withdraw_address);
                        WithdrawAddresses.set({
                            wallet: output_wallet_type,
                            addresses: withdrawals
                        });
                    }
                }
                WithdrawAddresses.setLast({
                    wallet: output_wallet_type,
                    address: withdraw_address
                });

                let sendAmount = new Asset({
                    asset_id: asset.get("id"),
                    precision: asset.get("precision"),
                    amount: amountSendValue,
                    real: amountSendValue
                });

                let sendMemo = [coin.backingCoin.toLowerCase(), withdraw_address];
                if( this.state.walletType === "ERC20" ) {
                    sendMemo.push(UNIQHASH);
                }
                if( memo ) {
                    sendMemo.push(new Buffer(memo, "utf-8"));
                }

                // 
                // debugger;
                console.log(
                    account.get("id"),
                    coin.issuer,
                    sendAmount.getAmount(),
                    asset.get("id"),
                    sendMemo.join(":"),
                    null,
                    feeAmount,
                    feeAmount ? feeAmount.asset_id : "1.3.2230"
                );

                AccountActions.transfer(
                    account.get("id"),
                    coin.issuer,
                    sendAmount.getAmount(),
                    asset.get("id"),
                    sendMemo.join(":"),
                    null,
                    feeAmount ? feeAmount.asset_id : "1.3.0"

                ).then(() => {
                    if( coin.withdrawalAdditionalFeeAmount && coin.withdrawalAdditionalFeeAssetId ) {
                        _this.sendGateFee(TransactionConfirmStore.getState().trx_id);
                    }
                    if( ["DEEX.ETH", "YANDEX", "QIWIRUBLE", "DEEX.BTC"].includes(coin.symbol) ) {
                        ymSend("reachGoal", [coin.symbol.toUpperCase(), "DEPO"].join("_"));
                    }
                    this.props.onShowForm({});
                });

            } else {
                this.setState({
                    empty_withdraw_value: true
                });
            }
        }
    };

    sendGateFee = (trx_id) => {
        if( !trx_id ) return null;

        const {coin, feeAmount, UNIQHASH} = this.state;
        const {account} = this.props;
        let  sendMemo = ["dex"];
        sendMemo.push(UNIQHASH);
        FetchChain("getAsset", coin.withdrawalAdditionalFeeAssetId).then(gateAsset => {

            let sendAmount = new Asset({
                asset_id: gateAsset.get("id"),
                precision: gateAsset.get("precision"),
                amount: coin.withdrawalAdditionalFeeAmount
            });

            AccountActions.transfer(
                account.get("id"),
                coin.issuer,
                sendAmount.getAmount(),
                gateAsset.get("id"),
                sendMemo.join(":"),
                null,
                feeAmount ? feeAmount.asset_id : "1.3.0",
                false
            );

        });
    };

    showDepositModal = (asset_id) => {
        const {account} = this.props;
        let dataModal = {
            account: account.get("name"),
            asset: asset_id,
            asset_id: asset_id
        };

        ModalActions.show("transfer_asset_modal", {
            typeWindow: "depositAddress",
            dataModal
        });
    };

    changeWalletType = event => {
        const { coin, withdraw_address } = this.state;
        let walletType = event.hasOwnProperty("target") ? event.target.value : event;
        let switchableWalletType = coin?.switchableWalletType;

        this.setState({
            requestLoad: true,
            walletType,
            coin: {
                ...coin,
                ...switchableWalletType[walletType],
                walletType,
            }
        },()=>{
            this._validateAddress(withdraw_address);
        });
    };

    render() {
        const _this = this;
        const {amountSendError, amountReceive, feeAmount, gateFeeAssetAmount, gateFeeAmountBalance,feeAssetBalance,
            amountSendValue, amountReceiveRealValue, coin, balance: feeBalance,
            options_is_valid, withdraw_address_selected, withdraw_address,
            withdraw_address_is_valid, withdraw_address_check_in_progress, requestLoad } = this.state;
        const {asset_id, account, asset} = this.props;
        let dataContent = this.getMemoContent();
        let balance = null;

        const visibleSymbol = getAlias(asset.get("symbol"));
        let account_balances = account.get("balances").toJS();
        let optionsAddress = null;
        let invalid_address_message = null;

        let current_asset_id = asset.get("id");
        let storedAddress = WithdrawAddresses.get(
            this.props.output_wallet_type
        );
        if (options_is_valid) {
            optionsAddress = (
                <div
                    className={
                        !storedAddress.length
                            ? "tdex-disabled-options"
                            : "tdex-options"
                    }
                >
                    {storedAddress.map(function(name, index) {
                        return (
                            <a key={index} onClick={()=>_this.onSelectChanged(index)} >
                                {name}
                            </a>
                        );
                    }, this)}
                </div>
            );
        }
        if (!withdraw_address_check_in_progress && (withdraw_address && withdraw_address.length) ) {
            if (!withdraw_address_is_valid) {
                invalid_address_message = (
                    <div className="send-withdraw-error">
                        <Translate content="gateway.valid_address" coin_type={getAlias(asset.get("symbol"))}/>
                    </div>
                );
            }
        }

        const helpBlockSend = (
            <div className="send-withdraw-help-link">
                {coin.withdrawMinAmount ? (
                    <Translate
                        onClick={()=>this.onAmountChanged({
                            amount: coin.withdrawMinAmount / utils.get_asset_precision(coin.precision),
                            asset
                        })}
                        content="gateway.tdex.min_amount"
                        minAmount={utils.format_asset(coin.withdrawMinAmount, asset, true, false )} symbol={getAlias(coin.symbol)} />
                ) : null }
                <Translate
                    content="modal.withdraw.maximum_amount"
                    onClick={()=>this.onAccountBalance(account_balances[current_asset_id], current_asset_id)} />
            </div>
        );
        let withdrawalStaticFee;

        let withdrawalPercentageFee = parseFloat(coin.withdrawalPercentageFee);
        if( coin.withdrawalStaticFee ) {
            withdrawalStaticFee = new Asset({
                asset_id: asset.get("id"),
                precision: coin.precision,
                amount: coin.withdrawalStaticFee
            });
        }

        console.log("coin", coin);
        console.log("requestLoad", requestLoad);

        let isDisableForm =
            !withdraw_address_is_valid ||
            !!amountSendError ||
            !amountSendValue ||
            !amountReceive.hasAmount() ||
            feeAmount && !feeAmount.hasAmount() ||
            feeAmount && feeAssetBalance.lte(feeAmount) ;

        if( coin.withdrawalAdditionalFeeAmount && coin.withdrawalAdditionalFeeAssetId && !isDisableForm ) {
            isDisableForm = gateFeeAssetAmount && !gateFeeAssetAmount.hasAmount() ||
                gateFeeAssetAmount && gateFeeAmountBalance.lte(gateFeeAssetAmount) ;
        }

        let switchableWalletType = Object.keys(coin?.switchableWalletType || {});

        return (
            <Spin spinning={requestLoad}>
                <div className="send-withdraw" >

                    <div className="send-withdraw-header">
                        <Translate unsafe component={"div"} className={"send-withdraw-title"} content={"account.withdraw"} symbol={visibleSymbol}/>

                        {coin.maintenanceMsg ?
                            <Translate component="div" className={"send-withdraw-msg"} content={["maintenance", coin.maintenanceMsg].join(".")}/> : null}

                            <div className="send-withdraw-sub-header">
                                <div>
                                    <Translate component="span" content="transfer.available"/>
                                    &nbsp;:&nbsp;
                                    <span className="set-cursor" onClick={()=>this.onAccountBalance(account_balances[current_asset_id], current_asset_id)}>
                                        {account_balances[current_asset_id] ? (
                                            <BalanceComponent
                                                balance={account_balances[current_asset_id]}
                                            />
                                        ) : (0)}
                                    </span>
                                </div>
                                <div>
                                <Translate component={"span"} content={"gateway.info.withdraw"}/>{" "}
                                {withdrawalStaticFee && withdrawalStaticFee.hasAmount() ?
                                    withdrawalStaticFee.getAmount({real: true}) + " "+ visibleSymbol :
                                    withdrawalPercentageFee ? withdrawalPercentageFee + " %" : "0" }
                                </div>
                            </div>
                    </div> 
                    {/* : null } */}

                    <div className="send-withdraw-row to">
                        {/* {(__DEEX_CHAIN__ || __SCROOGE_CHAIN__ || __GBL_CHAIN__) && (
                            <>
                                <Translate component={"label"} className="left-label" content="modal.withdraw.send_address"/>
                                <div className="inline-label input-wrapper">
                                    {visibleSymbol === "RUB" ?
                                        <InputMask
                                            type={"text"}
                                            mask="+7 (999) 999-99-99"
                                            alwaysShowMask={true}
                                            placeholder="+7 (___) ___-__-__"
                                            value={withdraw_address_selected}
                                            tabIndex="4"
                                            onChange={(event) => this.onWithdrawAddressChanged(event)}
                                            onBlur={() => this._validateAddress(withdraw_address_selected.replace(/\s|\(|\)|\-/g, ""))}
                                            autoComplete="off"
                                        /> : <input
                                            type="text"
                                            value={withdraw_address_selected}
                                            tabIndex="4"
                                            onChange={(event) => this.onWithdrawAddressChanged(event)}
                                            onBlur={() => this._validateAddress(withdraw_address_selected)}
                                            autoComplete="off"
                                        />}
                                </div>
                            </>
                        )} */}
                    <form action="" className="send-withdraw-form">
                        {/*{switchableWalletType?.length > 1 ? <div className="send-withdraw-row">
                            <div className="content-block">
                                <div className="amount-selector">
                                    <Radio.Group
                                        onChange={event=>this.changeWalletType(event.target.value)}
                                        value={this.state.walletType} >
                                        {switchableWalletType.map(item=>{
                                            return <div key={`radio-switch-${item}`}>
                                                <Radio value={item}>
                                                    <Translate content={`gateway.withdraw_switch.${[visibleSymbol.toUpperCase(), item.toUpperCase()].join("_")}`}  />
                                                </Radio>
                                            </div>
                                        })}
                                    </Radio.Group>
                                </div>
                            </div>
                        </div> : null }*/}

                        <SwitchableWalletType
                            type={"withdraw"}
                            prefixCln={"send-withdraw"}
                            prefixTrnsl={"gateway.withdraw_switch."}
                            coin={coin}
                            walletType={this.state?.walletType}
                            onChangeWalletType={event=>this.changeWalletType(event)}
                        />
                        <div className="send-withdraw-row to">
                            <Translate component={"label"} className="left-label" content="modal.withdraw.send_address"/>
                            <div className="inline-label input-wrapper">
                                { visibleSymbol === "RUB" ?
                                    <InputMask
                                        type={"text"}
                                        mask="+7 (999) 999-99-99"
                                        alwaysShowMask={true}
                                        /*placeholder="+7 (___) ___-__-__"*/
                                        value={withdraw_address_selected}
                                        tabIndex="4"
                                        onChange={(event)=>this.onWithdrawAddressChanged(event)}
                                        onBlur={()=>this._validateAddress(withdraw_address_selected.replace(/\s|\(|\)|\-/g, "")) }
                                        autoComplete="off"
                                    /> : <input
                                        type="text"
                                        value={withdraw_address_selected}
                                        tabIndex="4"
                                        onChange={(event)=>this.onWithdrawAddressChanged(event)}
                                        onBlur={()=>this._validateAddress(withdraw_address_selected)}
                                        autoComplete="off"
                                    /> }





                                {options_is_valid && (
                                    <span onClick={()=>this.onDropDownList(this)} >&#9660;</span>
                                )}
                            </div>
                            {options_is_valid && (
                                <div className="tdex-position-options">
                                    {optionsAddress}
                                </div>
                            )}
                            {invalid_address_message}
                        </div>
                        <div className="send-withdraw-row ">
                            <AmountSelector
                                label="modal.withdraw.amount_send"
                                amount={amountSendValue}
                                onChange={this.onAmountChanged}
                                asset={asset_id}
                                assets={[asset_id]}
                                display_balance={balance}
                                helpBlock={helpBlockSend}
                                errorBlock={amountSendError ? <Translate className={"send-withdraw-error"} content={amountSendError} /> : null}
                                tabIndex={0}
                            />


                            {/* onChange={this.onReceiveChanged} */}
                    </div>
                    <div className="send-withdraw-row amount1">
                        <AmountSelector
                            label="modal.withdraw.amount_receive"
                            amount={amountReceiveRealValue || 0}
                            onChange={(__DEEX_CHAIN__ || __SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__) ? this.onReceiveChanged : this.onAmountChanged}
                            asset={asset_id}
                            assets={[asset_id]}
                            /*display_balance={balance}*/
                            //helpBlock={helpBlockSend}
                            errorBlock={amountSendError ? <Translate className={"send-withdraw-error"} content={amountSendError} /> : null}
                            tabIndex={0}
                        />
                    </div>

                        {coin.withdrawMemoSupport ? <div className="send-withdraw-row" style={{marginBottom: 0}}>
                            <div className="gate_fee">
                                <div className="amount-selector">
                                    <Translate component="label" className="left-label" content="transfer.memo"/>
                                    <div className="inline-label input-wrapper">
                                        <input type="text" name={"memo"} onChange={(event)=>this.setState({memo: event.target.value}, ()=>this._updateFee())} />
                                    </div>
                                    <div className="send-withdraw-help">
                                    </div>
                                </div>

                            </div>
                        </div> : null }



                        {/*{feeAmount && feeAmount.hasAmount() ?  <div className="send-withdraw-row">
                            <div className="amount-selector">
                                <div className="send-withdraw-fee-title">
                                    <Translate content="transaction.feeTypes.fee" />:
                                    <FormattedAsset
                                        amount={feeAmount.getAmount({real: 0})}
                                        asset={feeAmount.asset_id}
                                    />
                                </div>
                            </div>
                            <div className="amount-selector">
                                <div className="send-withdraw-fee">
                                    <div>
                                        <Translate content="transfer.available" />:

                                        <FormattedAsset
                                            amount={feeAssetBalance.getAmount({real: 0})}
                                            asset={feeAssetBalance.asset_id}
                                        />
                                    </div>
                                    { feeAssetBalance.lte(feeAmount) ? <Translate className={"send-withdraw-link"} content={"wallet.tips_deposit"} onClick={()=>this.showDepositModal(feeAssetBalance.asset_id)} /> : null }
                                </div>

                            </div>
                        </div> : null}*/}

                        <FeeAssetSelector
                            label="transfer.fee"
                            account={account}
                            trxInfo={{
                                type: "transfer",
                                options: ["price_per_kbyte"],
                                data: {
                                    type: "memo",
                                    content: dataContent
                                }
                            }}
                            onChange={(result)=>this.onFeeChanged(result)}
                        />

                        {gateFeeAssetAmount && gateFeeAssetAmount.hasAmount() ?  <div className="send-withdraw-row" style={{marginTop: 10}}>
                            <div className="amount-selector">
                                <div className="send-withdraw-fee-title">
                                    <Translate content="transaction.feeTypes.fee" />:
                                    <FormattedAsset
                                        amount={gateFeeAssetAmount.getAmount({real: 0})}
                                        asset={gateFeeAssetAmount.asset_id}
                                    />
                                </div>
                            </div>
                            <div className="amount-selector">
                                <div className="send-withdraw-fee">
                                    <div>
                                        <Translate content="transfer.available" />:

                                        <FormattedAsset
                                            amount={gateFeeAmountBalance.getAmount({real: 0})}
                                            asset={gateFeeAmountBalance.asset_id}
                                        />
                                    </div>

                                    { gateFeeAmountBalance.lte(gateFeeAssetAmount) ? <Translate className={"send-withdraw-link"} content={"wallet.tips_deposit"} onClick={()=>this.showDepositModal(gateFeeAmountBalance.asset_id)} /> : null }
                                </div>
                            </div>
                        </div> : null}

                        <div className="send-withdraw-row btn-group">
                            <div className={"send-withdraw-row group-button"}>
                                <button
                                    className={classnames("btn btn-gray")}
                                    type="button"
                                    onClick={()=>this.props.onShowForm({})} >
                                    <Translate component="span" content="transfer.cancel"/>
                                </button>
                                <button
                                    className={classnames("btn btn-green", {disabled: isDisableForm})}
                                    disabled={isDisableForm}
                                    type="button"
                                    onClick={
                                        !isDisableForm ? ()=>this.onSubmit() : null
                                    } >
                                    <Translate component="span" content="account.withdraw"/>
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
                </div>
            </Spin>
        );
    }
}

const SendWithdrawBind = BindToChainState(SendWithdraw);

class SendWithdrawConnectWrapper extends React.Component {
    render() {
        return <SendWithdrawBind {...this.props} />;
    }
}

export default connect(
    SendWithdrawConnectWrapper,
    {
        listenTo() {
            return [AccountStore, GatewayStore];
        },
        getProps() {
            return {
                backedCoins: GatewayStore.getState().backedCoins,
                account: AccountStore.getState().passwordAccount || AccountStore.getState().currentAccount
            };
        }
    }
);


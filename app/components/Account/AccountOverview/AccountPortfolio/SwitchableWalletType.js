import React, {useEffect} from "react";
import {getAlias} from "config/alias";
import {Radio} from "antd";
import Translate from "react-translate-component";

const SwitchableWalletType = props => {

    if(!props.type) return null;

    // let switchableWalletType = Object.keys(props.coin?.switchableWalletType || {});
    let switchableWalletType = Object.keys(props.coin?.switchableWalletType || {})
        .sort()
        .map(walletTypes=>{
            const allowedName = {
                deposit: "depositAllowed",
                withdraw: "withdrawalAllowed"
            };
            let dataWalletTypes = props.coin?.switchableWalletType[walletTypes];
            if( dataWalletTypes && dataWalletTypes[allowedName[props.type]] ) {
                return walletTypes;
            }
        })
        .filter(a=>a);

    let visibleSymbol = getAlias(props.coin?.backingCoin);
    const prefixCln = props.prefixCln || "deposit-address";
    const prefixTrnsl = props.prefixTrnsl || "deposit.deposit_switch.";


    useEffect(()=>{
        if( switchableWalletType.length === 1 && !props.walletType ) {
            props.onChangeWalletType(switchableWalletType.shift());
        }
    });

    if(!switchableWalletType || switchableWalletType.length === 1) return null;

    console.log("props walletType", props);

    return (
        <div className={`${prefixCln}-row`}>
            <div className={`${prefixCln}-group`}>
                <Radio.Group
                    onChange={props.onChangeWalletType}
                    value={props.walletType} >
                    {switchableWalletType.map(item=>{
                        return (<div key={`radio-switch-${item}`}>
                            <Radio value={item}>
                                <Translate content={`${prefixTrnsl}${[visibleSymbol.toUpperCase(), item.toUpperCase()].join("_")}`}  />
                            </Radio>
                        </div>);
                    })}
                </Radio.Group>
            </div>
        </div>
    );
};

export default SwitchableWalletType;
import React, {useState} from "react";
import {connect} from "alt-react";
import URL from "url-parse";
import QRCode from "qrcode.react";
import Icon from "Components/Icon/Icon";
import notify from "actions/NotificationActions";
import Translate from "react-translate-component";
import counterpart from "counterpart";
import {getLoadedLocales} from "api/apiConfig";
import {requestDepositAddress, validateAddress} from "common/TDexMethods";
import {getAccountId} from "common/accountHelper";
import TDexDepositAddressCache from "common/TDexDepositAddressCache";
import "./DepositAddress.scss";
import AccountStore from "stores/AccountStore";
import GatewayStore from "stores/GatewayStore";
import IntlStore from "stores/IntlStore";
import BindToChainState from "Components/Utility/BindToChainState";
import ChainTypes from "Components/Utility/ChainTypes";
import LoadingIndicator from "Components/LoadingIndicator";
import {getAlias} from "config/alias";
import {ymSend} from "../../../../lib/common/metrika";
import FormattedAsset from "Utility/FormattedAsset";
import {Radio, Divider, Spin, Form, Input} from "antd";
import {cn} from "@bem-react/classname";
import {Asset} from "../../../../lib/common/MarketClasses";
import SwitchableWalletTypeDeex from "./DepositAddress/SwitchableWalletType";
import SwitchableWalletTypeBts from "./SwitchableWalletType";

const SwitchableWalletType = __DEEX_CHAIN__ || __SCROOGE_CHAIN__ || __GBL_CHAIN__  || __GBLTN_CHAIN__ ? SwitchableWalletTypeDeex : SwitchableWalletTypeBts;


class DepositAddress extends React.Component {

    static propTypes = {
        account: ChainTypes.ChainAccount.isRequired,
        asset: ChainTypes.ChainAsset.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            localesLoaded: false,
            ...DepositAddress.getDefaultState()
        };
        this.fetchController = new AbortController();
        this.deposit_address_cache = new TDexDepositAddressCache();
        this.toClipboard = this.toClipboard.bind(this);

    }


    componentDidMount() {
        this.setInitDepositAddress();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.asset_id !== this.props.asset_id) {
            this.fetchController.abort();
            this.fetchController = new AbortController();
            this.setInitDepositAddress();
        }
    }

    static getDefaultState = () => {
        return (__DEEX_CHAIN__ || __SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__) ? {
            coin: null,
            requestLoad: true,
            receive_address: "",
            receive_memo: "",
            walletType: "",
            receive: {},
        } : {
            requestLoad: true,
            receive_address: "",
            receive_memo: "",
            walletType: "",
            receive: {},
        }
    };

    setInitDepositAddress = () => {
        console.log("requestLoad", 0);
        this.setState(DepositAddress.getDefaultState(), ()=> {
            this.getCoin().then(()=>{
                const {coin} = this.state;
                if( !coin.requireAdditionalDepositButtonPress ) {
                    this._getDepositAddress();
                } else {
                    this.setState({
                        requestLoad: false
                    });
                }
            });
        });
    };

    _getDepositObject(state = this.state) {
        const { walletType } = state;
        const { account , backedCoins, asset } = this.props;
        const coin = backedCoins.get("TDEX").filter(coins => {
            return coins.symbol === asset.get("symbol");
        })[0];
        let depositData;
        try {
            if( coin ) {
                depositData = {
                    signal: this.fetchController.signal,
                    walletType,
                    inputCoinType: coin.backingCoin.toLowerCase(),
                    outputCoinType: coin.symbol.toLowerCase(),
                    account: account.get("name"),
                    user_id: account.get("id"),
                    service_id: 6,
                };
            }
        } catch (err) {
            console.log(new Error(err.message));
        }
        return depositData;
    }

    _getDepositAddress() {
        const {backedCoins} = this.props;
        if( backedCoins ) {
            let depositObject = this._getDepositObject();
            if( depositObject ) {
                console.log("requestLoad", 0);
                requestDepositAddress(depositObject)
                    .then(this.setDepositAddress);
            }
        }
    }

    setDepositAddress = result => {
        const {receive, coin} = this.state;
        if((__DEEX_CHAIN__ || __SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__) && !coin) return null;
        console.log("result", result);
        const {
            account,
            gateway,
            deposit_coin_type,
            receive_coin_type
        } = this.props;

        receive[coin.symbol] = result;

        if (result && result.address) {
            this.deposit_address_cache.cacheInputAddress(
                gateway,
                account.get("name"),
                deposit_coin_type,
                receive_coin_type,
                result.address,
                result.inputMemo
            );
        } else {
            console.warn(86, result);
        }
        console.log("requestLoad", 1, result);

        this.setState({
            receive,
            requestLoad: false
        });
    };

    getCoin = () => {
        let { backedCoins, asset }  = this.props;
        if( backedCoins ) {
            return new Promise(resolve=>{
                let coins = backedCoins.get("TDEX")
                    .filter(coins=>coins.symbol === asset.get("symbol"));

                this.setState({
                    coin: coins[0],
                    walletType: coins[0].walletType
                }, resolve);
            });
        }
    };

    toClipboard(clipboardText, copyType = "button", event) {
        const {coin} = this.state;
        if( event ) event.preventDefault();
        try {
            this.setState({clipboardText}, () => {
                var textArea = document.createElement("textarea");
                textArea.value = clipboardText;
                document.body.appendChild(textArea);
                textArea.select();

                if( ["DEEX.ETH", "YANDEX", "QIWIRUBLE", "DEEX.BTC"].includes(coin.symbol) ) {
                    ymSend("reachGoal", [coin.symbol.toUpperCase(), "DEPO"].join("_"));
                }

                try {
                    var successful = document.execCommand("copy");
                    var msg = successful ? "successful" : "unsuccessful";
                    console.log("Fallback: Copying text command was " + msg);
                } catch (err) {
                    console.error("Fallback: Oops, unable to copy", err);
                }

                document.body.removeChild(textArea);

                notify.addNotification({
                    message: `${copyType} copied`,
                    level: "success",
                    autoDismiss: 1
                });
            });
        } catch (err) {
            console.error(err);
        }
    }

    changeWalletType = walletType => {
        const { coin } = this.state;
        let switchableWalletType = coin?.switchableWalletType;

        this.setState({
            requestLoad: true,
            walletType,
            coin: {...coin, ...switchableWalletType[walletType]}
        }, this._getDepositAddress);
    };

    render() {
        const { asset_id } = this.props;
        const {receive, coin, walletType, requestLoad } = this.state;

        const receiveCurrent = receive?.[coin?.symbol];
        const visibleSymbol = backingCoin ? getAlias(backingCoin) : asset_id;
        let memoText = receiveCurrent?.inputMemo || "",
            backingCoin = coin && coin.backingCoin;

        let userDepositMemo = {
            "{user_name}" : this.props.account.get("name"),
            "{user_id}" : getAccountId(this.props.account),
            "{user_fullid}" : this.props.account.get("id"),
        };

        if ( coin && coin.depositMemoSupport ) {
            if(!memoText && walletType !== "ERC20") {
                Object.keys(userDepositMemo).map(key=>{
                    if( coin.depositMemoMask && coin.depositMemoMask.indexOf(key) !== -1 ) {
                        memoText = coin.depositMemoMask.replace(key, userDepositMemo[key]);
                    }
                });
            }
        }

        return (
            <Spin spinning={requestLoad}>
                <div className="deposit-address">
                    <div className="deposit-address-header">
                        <Translate
                            component={"div"}
                            className={"deposit-address-title"}
                            content="account.deposit" symbol={coin?.symbol ? coin.symbol : visibleSymbol} />
                        {receiveCurrent?.error ? <Translate
                            component={"div"}
                            className={"deposit-address-subtitle"}
                            content={["deposit","error_request"]}
                        /> :
                            <Translate
                                component={"div"}
                                className={"deposit-address-subtitle"}
                                content={["deposit","subtitle", visibleSymbol]}
                                fallback={counterpart.translate(memoText
                                    ? "deposit.subtitle.send_memo" :
                                    "account.deposit_send"
                                )}
                                /*fallback={depositAddressSubtitle}*/
                            /> }
                    </div>

                    {!receiveCurrent && coin?.requireAdditionalDepositButtonPress ?
                        <PreViewDepositAddressForm
                            coin={coin}
                            changeCoin={coinItem=>this.setState({
                                coin: Object.assign(coin, coinItem)
                            })}
                            toReset={this.setInitDepositAddress}
                            stateCallback={result=>this.setDepositAddress(result)}
                            getDepositObject={()=>this._getDepositObject()} />
                        : null}

                    {receiveCurrent ? <ViewDepositAddress
                        coin={coin}
                        memoText={memoText}
                        visibleSymbol={visibleSymbol}
                        walletType={walletType}
                        account={this.props.account}
                        toReset={this.setInitDepositAddress}
                        stateCallback={result=>this.changeWalletType(result)}
                        toClipboard={this.toClipboard}
                        receive={receiveCurrent}
                    /> : null}
                </div>
            </Spin>
        );
    }
}

const DepositAddressBind = BindToChainState(DepositAddress);
const PreViewDepositName = props => {
    const {getFieldDecorator} = props.form;
    const validateName = (rule, value, callback) => {
        validateAddress({
            newAddress: value,
            symbol: props.coin.symbol,
            walletType: props.walletType
        }).then(result=>{
            console.log("result", result);
            if ( result ) return callback();
            else return callback(counterpart.translate("deposit.error_wallet"));
        });
    };
    return (
        <Form.Item label={counterpart.translate("deposit.number_wallet")} >
            {getFieldDecorator(props.name, {
                rules: [
                    {
                        required: true,
                        message: "Please your number",
                    },
                    {
                        validator: validateName,
                    }
                ],
            })(<Input /> )}
        </Form.Item>
    );
};
const PreViewDepositEmail = props => {
    const {getFieldDecorator} = props.form;
    return (
        <Form.Item label={counterpart.translate("deposit.email")} >
            {getFieldDecorator(props.name, {
                rules: [
                    {
                        type: "email",
                        message: "The input is not valid E-mail!",
                    },
                    {
                        required: true,
                        message: "Please input your E-mail!",
                    },
                ],
            })(<Input autoComplete={"email"} />)}
        </Form.Item>
    );
};
const PreViewDepositAmount = props => {
    const {coin, asset_id, name} = props;
    const {getFieldDecorator} = props.form;
    const cln = cn("deposit-address");
    const getAsset = amount => {
        return new Asset({
            amount: amount || 0,
            asset_id,
            precision: coin.precision
        });
    };
    const validateAmount = (rule, value) => {
        const pfDMinAmount = getAsset(coin.depositMinAmount);
        const pfDMaxAmount = getAsset(coin.depositMaxAmount);
        const pfValue = getAsset();
        pfValue.setAmount({
            real: value
        });

        if( pfDMinAmount.hasAmount() && pfDMinAmount.gt(pfValue) ) {
            return Promise.reject(`Сумма должна быть больше ${pfDMinAmount.getAmount({real: 1})}`);
        } else if( pfDMaxAmount.hasAmount() && pfDMaxAmount.lt(pfValue) ) {
            return Promise.reject(`Сумма должна быть меньше ${pfDMaxAmount.getAmount({real: 1})}`);
        } else {
            return Promise.resolve();
        }
    };
    return (
        <Form.Item label={counterpart.translate("deposit.amount")} extra={<div className={cln("explain")}>
            {coin.depositMinAmount ? <MinimumAmount coin={coin} /> : null}
            {coin.depositMaxAmount ? <MaximumAmount coin={coin} /> : null}
        </div>} >
            {getFieldDecorator(name, {
                rules: [
                    {
                        required: true,
                        message: "Please your amount",
                    },
                    {
                        validator: validateAmount,
                    },
                ],
            })(<Input addonAfter={<span className={cln("addon-after")}>{getAlias(coin.backingCoin)}</span>} />)}
        </Form.Item>
    );
};
class PreViewDepositAddress extends React.Component {

    static defaultProps = {
        items: {
            email: PreViewDepositEmail,
            origin_account_name: PreViewDepositName,
            amount: PreViewDepositAmount,
        }
    };

    handleSubmit = event => {
        console.log("event", event);
        event.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                let depositObject = {
                    ...values,
                    ...this.props.getDepositObject(this.state)
                };
                requestDepositAddress(depositObject)
                    .then(this.props.stateCallback);
            }
        });
    };

    validateAmount = (rule, value, callback) => {
        callback();
    };

    changeWalletType = event => {
        let walletType;
        if( event.hasOwnProperty("target") ) walletType = event.target.value;
        else walletType = event;
        this.setState({
            walletType
        });
    };

    render() {
        const {coin, form, items} = this.props;
        const depositParamKeys = Object.keys(coin.requireAdditionalDepositParams);

        console.log("coin", coin);
        return (
            <>
                <Form onSubmit={this.handleSubmit}>

                    <SwitchableWalletType
                        type={"deposit"}
                        prefixCln={"deposit-address"}
                        prefixTrnsl={"deposit.deposit_switch."}
                        coin={coin}
                        walletType={this.state?.walletType}
                        onChangeWalletType={this.changeWalletType}
                    />
                    {depositParamKeys.map(coinKey=>{
                        const ItemView = items[coinKey];
                        if(!ItemView) return null;
                        return <ItemView
                            key={`item-view-${coinKey}`}
                            coin={coin}
                            walletType={this.state?.walletType}
                            name={coinKey}
                            {...this.props} />;
                    })}


                    <>
                        <Divider style={{height: 1}} />
                        <div className={"deposit-address-row group-button"} >
                            <button type={"reset"} className={"btn btn-gray"} onClick={()=>form.resetFields()}>
                                <Translate component="span" content="transfer.cancel"/>
                            </button>
                            <button
                                disabled={
                                    !(!Object.values(form.getFieldsError(depositParamKeys)).filter(errors => errors).length &&
                                        !Object.values(form.getFieldsValue(depositParamKeys)).filter(errors => !errors).length)
                                }
                                type={"submit"} className={"btn btn-red"}>
                                <Translate component="span" content="transfer.send"/>
                            </button>

                        </div>
                    </>
                </Form>
            </>
        );
    }
}
const PreViewDepositAddressForm = Form.create({name: "PreViewDeposit"})(PreViewDepositAddress);

const ViewDepositAddress = props => {
    const [walletType, setWalletType] = useState(props.walletType);
    const {receive, coin, memoText, visibleSymbol} = props;
    let depositFee, depositFeePercent, depositUrl, qrCodeString;

    const changeWalletType = event => {
        let curentWalletType = event.hasOwnProperty("target") ?
            event.target.value :
            event;

        setWalletType(curentWalletType);
        props.stateCallback(curentWalletType);
    };

    if( coin && ( parseFloat(coin.depositPercentageFee) || parseFloat(coin.depositStaticFee) ) ) {
        depositFee = parseFloat(coin.depositPercentageFee || coin.depositStaticFee);
        depositFeePercent = parseFloat(coin.depositPercentageFee);
    }
    if( receive.inputAddress )
        qrCodeString = receive.inputAddress;

    if( receive.depositUrl ) {
        depositUrl = URL(receive.depositUrl);
        qrCodeString = receive.depositUrl;
    }

    // if ( receive.error )
    //return "Error request";


    return (
        <div>
            <SwitchableWalletType
                type={"deposit"}
                prefixCln={"deposit-address"}
                prefixTrnsl={"deposit.deposit_switch."}
                coin={coin}
                walletType={walletType}
                onChangeWalletType={event => changeWalletType(event)}
            />
            {!receive.error && receive.inputAddress ? <div className={"deposit-address-row"} >
                <Translate component={"div"} className={"deposit-address-label"}  content="gateway.address"/>
                <div className="deposit-address-group">
                    <input type="text" className="deposit-address-input" value={receive.inputAddress} readOnly={true}/>
                    <button
                        type={"button"}
                        className="btn btn-gray"
                        onClick={(event)=>props.toClipboard(receive.inputAddress, "address", event)}
                    >
                        <Icon name="clippy" />
                    </button>
                </div>
            </div> : null}

            {!receive.error && receive.depositUrl ? <div className={"deposit-address-row"} >
                <Translate component={"div"} content="deposit.depositUrl"/>
                <br/>
                <a href={receive.depositUrl} target={"_blank"} rel={"noopener noreferrer"}>
                    {depositUrl.origin}
                </a>
            </div> : null }

            {!receive.error && coin.depositTtlMin ? <div className={"deposit-address-row"} >
                <Translate component={"div"} content="gateway.tdex.ttl_min" ttlMin={coin.depositTtlMin}/>
            </div> : null }

            {!receive.error && memoText ? <div className={"deposit-address-row"} >
                <Translate
                    component={"div"}
                    className={"deposit-address-label"}
                    content={["deposit","MemoContent", visibleSymbol]}
                    fallback={counterpart.translate("gateway.memo")}
                />
                <div className="deposit-address-group">
                    <input type="text" className="deposit-address-input" value={memoText} readOnly={true}/>
                    <button
                        type={"button"}
                        className="btn btn-gray"
                        onClick={(event) => (__DEEX_CHAIN__ || __SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__)
                        ? props.toClipboard(memoText, "memo", event)
                        : this.toClipboard(memoText, "memo", event)}
                    >
                        <Icon name="clippy" />
                    </button>
                </div>
            </div> : null }

            {!receive.error && coin.depositMinAmount || coin.depositMaxAmount ? <div className={"deposit-address-row line"} >
                {coin.depositMinAmount ? <div style={{fontSize:12, marginTop: 3}}>
                    <MinimumAmount coin={coin} />
                </div> : null}
                {coin.depositMaxAmount ?
                    <div style={{fontSize:12, marginTop: 3}}>
                        <MaximumAmount coin={coin} />
                    </div> : null}
            </div> : null }

            {!receive.error && depositFee ? <div className={"deposit-address-row line"} >
                <Translate
                    component={"div"}
                    content="gateway.tdex.fee_deposit"
                    style={{fontSize:12, marginTop: 3}}
                    fee={coin.depositPercentageFee ?
                        coin.depositPercentageFee :
                        <FormattedAsset
                            asset={coin.symbol}
                            amount={coin.depositStaticFee} />
                    }
                    symbol={depositFeePercent ? "%" : null }
                />
            </div> : null }

            {!receive.error && qrCodeString ?
                <div className="deposit-address-row" style={{textAlign: "center"}}>
                    <QRCode
                        size={200}
                        style={{border: "5px solid #ffffff"}}
                        value={qrCodeString}/>
                </div> : null}

            {!receive.error && coin.requireAdditionalDepositButtonPress ?
            <>
                <Divider style={{height: 1}} />
                <div className={"deposit-address-row group-button1"} style={{
                    textAlign: "center",
                    marginTop: 20
                }}>
                    <button className={"btn btn-gray"} onClick={()=>props.toReset()}>
                        <Translate component="span" content="transfer.cancel"/>
                    </button>
                </div>
            </> : null}

        </div>
    );
};


class DepositAddressConnectWrapper extends React.Component {

    state = {
        localesLoaded: false
    }

    componentDidMount() {
        this.getLoadedLocales();
    }

    getLoadedLocales = () => {
        let transIEO = counterpart.translate("deposit");
        let staticPath = undefined;
        // eslint-disable-next-line no-undef
        if (__DEV__) {
            staticPath = "http://localhost:8081";
        }
        if (__SCROOGE_CHAIN__) {
            staticPath = "https://static.scrooge.club";
        }

        if( typeof transIEO !== "object" ) {
            const localesName = (__DEEX_CHAIN__ || __SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__) ? "deposit.json?v2" : "deposit.json?v1";
            getLoadedLocales(localesName, undefined, staticPath)
                .then(() => this.setState({localesLoaded: true}));
        } else {
            this.setState({
                localesLoaded: true
            });
        }
    };

    render() {
        const {localesLoaded} = this.state;

        if( !localesLoaded ) return <LoadingIndicator />;
        return <DepositAddressBind {...this.props} />;
    }
}

export default connect(
    DepositAddressConnectWrapper,
    {
        listenTo() {
            return [AccountStore, GatewayStore, IntlStore];
        },
        getProps() {
            return {
                backedCoins: GatewayStore.getState().backedCoins,
                locale: IntlStore.getState().currentLocale,
                account: AccountStore.getState().passwordAccount || AccountStore.getState().currentAccount
            };
        }
    }
);

const MinimumAmount = props => {
    return (
        <span>
            <Translate content="gateway.tdex.min_deposit" />
            <FormattedAsset
                amount={props.coin.depositMinAmount}
                asset={props.coin.symbol}
            />
        </span>
    );
};
const MaximumAmount = props => {
    return (
        <span>
            <Translate content="gateway.tdex.max_deposit" />
            <FormattedAsset
                amount={props.coin.depositMaxAmount}
                asset={props.coin.symbol}
            />
        </span>
    );
};

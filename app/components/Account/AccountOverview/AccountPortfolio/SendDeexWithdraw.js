import React from "react";
import Translate from "react-translate-component";
import {ChainStore, FetchChain} from "bitsharesjs";
import AmountSelector from "components/Utility/AmountSelector";
import AccountStore from "stores/AccountStore";
import AccountSelector from "components/Account/AccountSelector";
import TransactionConfirmStore from "stores/TransactionConfirmStore";
import {Asset} from "common/MarketClasses";
import {debounce, isNaN} from "lodash-es";
import {checkBalance, checkFeeStatusAsync} from "common/trxHelper";
import BalanceComponent from "components/Utility/BalanceComponent";
import AccountActions from "actions/AccountActions";
import utils from "common/utils";
import counterpart from "counterpart";
import {connect} from "alt-react";
import classnames from "classnames";
import PropTypes from "prop-types";
import FeeAssetSelector from "Components/Utility/FeeAssetSelector";
import BindToChainState from "Components/Utility/BindToChainState";
import SettingsStore from "../../../../stores/SettingsStore";
import assetConfig from "config/asset";
import {getAlias} from "config/alias";

import "./SendTransfer.scss";

class SendTransfer extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    static defaultProps = {
        onShowForm: () => {}
    };

    constructor(props) {
        super(props);
        this.state     = SendTransfer.getInitialState();
        this.nestedRef = null;

        this.onTrxIncluded = this.onTrxIncluded.bind(this);

        this._checkBalanceBefore = debounce(this._checkBalanceBefore.bind(this), 350);
        this._updateFee      = debounce(this._updateFee.bind(this), 250);
        //this.onMemoChanged      = debounce(this.onMemoChanged.bind(this), 250);
        this._checkFeeStatus = this._checkFeeStatus.bind(this);
        this._checkBalance   = this._checkBalance.bind(this);
        this.toChanged       = this.toChanged.bind(this);
        this.onAmountChanged = this.onAmountChanged.bind(this);
        this.onFeeChanged    = this.onFeeChanged.bind(this);

    }

    static getInitialState() {

        let assetKey = SettingsStore.getState().starredKey;
        const DEEX_ID = assetConfig[assetKey].id;
        let asset     = ChainStore.getAsset(DEEX_ID);

        return {
            from_name: "",
            to_name: "",
            from_account: null,
            to_account: null,
            orig_account: null,
            amount: "",
            asset_id: null,
            asset: null,
            memo: "",
            error: null,
            knownScammer: null,
            propose: false,
            propose_account: "",
            feeAsset: asset,
            fee_asset_id: DEEX_ID,
            feeAmount: new Asset({ amount: 0 }),
            feeStatus: {},
            maxAmount: false,
            hidden: false
        };
    }

    componentDidMount(){
        this._initForm();
    }
    componentDidUpdate(){}

    onSubmit(e) {
        e.preventDefault();
        this.setState({ error: null });

        const {
            from_account,
            to_account,
            memo,
            asset,
            feeAsset,
            fee_asset_id}          = this.state;
        let { amount }   = this.state;
        const sendAmount = new Asset({
            real: amount,
            asset_id: asset.get("id"),
            precision: asset.get("precision")
        });

        this.setState({ hidden: true });

        AccountActions.transfer(
            from_account.get("id"),
            to_account.get("id"),
            sendAmount.getAmount(),
            asset.get("id"),
            "migration:" + memo,
            null,
            feeAsset ? feeAsset.asset_id : fee_asset_id
        )
            .then(() => {
                this.props.onShowForm({});
                TransactionConfirmStore.unlisten(this.onTrxIncluded);
                TransactionConfirmStore.listen(this.onTrxIncluded);
            })
            .catch(e => {
                let msg = e.message ? e.message.split("\n")[1] || e.message : null;
                
                this.setState({ error: msg });
            });

    }

    _initForm () {
        const _this = this;
        let { to_name, from_name , currentAccount, asset_id } = this.props;
        
        if (to_name && to_name !== from_name) {
            FetchChain("getAccount", to_name, undefined, {
                [to_name]: false
            }).then((_account) => {
                _this.setState({
                    to_name: _account.get("name"),
                    to_account: _account
                });
            });
        }

        if (from_name || currentAccount) {
            FetchChain("getAccount", from_name || currentAccount, undefined, {
                [from_name]: false
            }).then((_account) => {
                _this.setState({
                    from_name: _account.get("name"),
                    from_account: _account
                });
            });
        }

        if (!this.state.from_name) {
            this.setState({ from_name: currentAccount });
        }

        if (
            this.props.asset_id &&
            this.state.asset_id !== this.props.asset_id
        ) {

            FetchChain("getAsset", asset_id, undefined, {})
                .then(_asset => {
                    if (_asset) {
                        this.setState({
                            asset_id: asset_id,
                            asset: _asset
                        });
                    }
                });
        }
    }

    // shouldComponentUpdate(np, ns) {
    // if (ns.open && !this.state.open) this._checkFeeStatus(ns);
    // if (!ns.open && !this.state.open) return false;
    // return true;
    // }

    UNSAFE_componentWillReceiveProps(np) {
        if (
            np.currentAccount !== this.state.from_name &&
            np.currentAccount !== this.props.currentAccount ||
            np.asset_id !== this.props.asset_id
        ) {
            this.setState(
                {
                    from_name: np.from_name,
                    from_account: ChainStore.getAccount(np.from_name),
                    to_name: np.to_name ? np.to_name : "",
                    to_account: np.to_name ? ChainStore.getAccount(np.to_name) : null,
                    feeStatus: {},
                    fee_asset_id: this.state.fee_asset_id,
                    feeAmount: new Asset({ amount: 0 })
                },
                () => {
                    this._initForm();
                    this._updateFee();
                    this._checkFeeStatus();
                }
            );
        }
    }

    async _checkBalance() {
        await this._updateFee();
        this._checkBalanceBefore();
    }
    _checkBalanceBefore() {

        const { feeAmount, amount, from_account, asset } = this.state;
        if (!asset || !from_account) return;

        const balanceID    = from_account.getIn(["balances", asset.get("id")]);
        const feeBalanceID = from_account.getIn([
            "balances",
            feeAmount.asset_id
        ]);
        if (!asset || !from_account) return;
        if (!balanceID)
            return this.setState({
                balanceError: true,
                typeBalanceError: "balanceID"
            });
        let balanceObject    = ChainStore.getObject(balanceID);
        let feeBalanceObject = feeBalanceID ? ChainStore.getObject(feeBalanceID) : null;
        if (!feeBalanceObject)
            return this.setState({
                balanceError: true,
                typeBalanceError: "feeBalance"
            });
        if (!balanceObject || !feeAmount) return;
        if (!amount)
            return this.setState({
                balanceError: false
            });
        
        
        const hasBalance = checkBalance(
            amount,
            asset,
            feeAmount,
            balanceObject
        );

        if (hasBalance === null) return;
        this.setState({ balanceError: !hasBalance });
    }

    _checkFeeStatus(state = this.state) {
        let { from_account, memo } = state;
        if (!from_account) return;

        const assets  = Object.keys(from_account.get("balances").toJS()).sort(
            utils.sortID
        );
        let feeStatus = {};
        let p         = [];
        assets.forEach(a => {
            p.push(
                checkFeeStatusAsync({
                    accountID: from_account.get("id"),
                    feeID: a,
                    options: ["price_per_kbyte"],
                    data: {
                        type: "memo",
                        content: memo
                    }
                })
            );
        });
        Promise.all(p)
            .then(status => {
                assets.forEach((a, idx) => {
                    feeStatus[a] = status[idx];
                });
                if (!utils.are_equal_shallow(this.state.feeStatus, feeStatus)) {
                    this.setState({
                        feeStatus
                    });
                }
                this._checkBalance();
            })
            .catch(err => {
                
            });
    }

    _setTotal(asset_id, balance_id) {
        const _this = this;
        const { feeAmount } = this.state;
        
        //debugger;
        // let balanceObject   = ChainStore.getObject(balance_id);
        // let transferAsset   = ChainStore.getObject(asset_id);

        FetchChain("getObject", [asset_id,balance_id], undefined, {}).then(data=> {
            let balanceObject   = data.get(1);
            let transferAsset   = data.get(0);
            let balance;
            balance = new Asset({
                amount: balanceObject.get("balance"),
                asset_id: transferAsset.get("id"),
                precision: transferAsset.get("precision")
            });
            

            if (balanceObject) {
                
                if (feeAmount.asset_id === balance.asset_id) {
                    balance.minus(feeAmount);
                }
                _this.setState({
                    maxAmount: true,
                    amount: balance.getAmount({ real: true })
                },  _this._checkBalance);
            }
        });
    }

    _getAvailableAssets(state = this.state) {
        const { from_account, from_error, fee_asset_id } = state;
        let letDeexId                    = fee_asset_id,
            fee_asset_types              = [fee_asset_id || "1.3.2230"];
        /*if (!(from_account && from_account.get("balances") && !from_error)) {
            return { asset_types, fee_asset_types };
        }*/
        let account_balances = from_account.get("balances").toJS();
        let asset_types = Object.keys(account_balances).sort(utils.sortID);
        for (let key in account_balances) {
            if (account_balances.hasOwnProperty(key)) {
                let balanceObject = ChainStore.getObject(account_balances[key]);
                if (balanceObject && balanceObject.get("balance") === 0) {
                    asset_types.splice(asset_types.indexOf(key), 1);
                }
            }
        }
        
        return {
            asset_types,
            fee_asset_types
        };
    }

    _updateFee(state = this.state) {
        
        let { fee_asset_id, from_account, memo } = state;
        //const getAvailableAssets = this._getAvailableAssets(state);
        const { fee_asset_types } = this._getAvailableAssets(state);
        
        
        
        if (
            fee_asset_types.length === 1 &&
            fee_asset_types[0] !== fee_asset_id || !fee_asset_id
        ) {
            fee_asset_id = fee_asset_types[0];
        }
        if (!from_account) return null;
        //debugger;
        

        checkFeeStatusAsync({
            accountID: from_account.get("id"),
            feeID: fee_asset_id,
            options: ["price_per_kbyte"],
            data: {
                type: "memo",
                content: "migration:" + state.memo
            }
        }).then(({ fee, hasBalance, hasPoolBalance }) => {
            
            
            this.setState({
                feeAmount: fee,
                fee_asset_id: fee.asset_id,
                hasBalance,
                hasPoolBalance,
                error: !hasBalance || !hasPoolBalance
            });
            this.getValidDeexName(memo);
        });
    }

    setNestedRef(ref) {
        this.nestedRef = ref;
    }

    toChanged(to_name) {
        this.setState({ to_name, error: null });
    }

    onToAccountChanged(to_account) {
        this.setState({ to_account, error: null });
    }

    onAmountChanged({ amount, asset }) {
        if (!asset) {
            return;
        }
        this.setState(
            {
                amount,
                asset,
                asset_id: asset.get("id"),
                error: null,
                maxAmount: false
            },
            this._checkBalance
        );
    }

    onFeeChanged(asset) {
        
        this.setState(
            {
                feeAsset: asset,
                fee_asset_id: asset.get("id"),
                error: null
            },
            this._checkBalance
        );
    }

    getValidDeexName = (name, countReload = 0) => {
        const _this = this;
        let requestData = {
            "jsonrpc": "2.0",
            "method": "get_account_by_name",
            "params": [name],
            "id": 1
        };
        try {
            let data = fetch("https://node2p.deexnet.com/ws", {
                method: "POST",
                body: JSON.stringify(requestData)
            });
            data
                .then(result=>result.json())
                .then(data=>{
                    
                    _this.setState({ 
                        isValidName: !!data.result,
                        isLoadedValidName: true
                    });
                }).catch(e=>{
                    
                    if(countReload <= 5 ){
                        setTimeout(()=>_this.getValidDeexName(name, countReload++), 1000);
                    }
                });
            
        } catch (e) {
            console.error(e);
        }
    }

    onMemoChanged(e) {
        const targetValue = e.target.value;
        let { from_account, from_error, maxAmount , asset_id } = this.state;
        let assetKey = SettingsStore.getState().starredKey;
        let current_asset_id = assetConfig[assetKey].id;
        if (
            from_account &&
            from_account.get("balances") &&
            !from_error &&
            maxAmount &&
            asset_id === current_asset_id
        ) {
            let account_balances = from_account.get("balances").toJS();
            this._setTotal(
                current_asset_id,
                account_balances[current_asset_id]
            );
        }
        
        this.setState({ 
            isValidName: false,
            reloadValidName: false,
            isLoadedValidName: false,
            memo: targetValue
        }, ()=>{
            this._updateFee();
        });
        
    }

    onTrxIncluded(confirm_store_state) {
        if (
            confirm_store_state.included &&
            confirm_store_state.broadcasted_transaction
        ) {
            // this.setState(Transfer.getInitialState());
            TransactionConfirmStore.unlisten(this.onTrxIncluded);
            TransactionConfirmStore.reset();
        } else if (confirm_store_state.closed) {
            TransactionConfirmStore.unlisten(this.onTrxIncluded);
            TransactionConfirmStore.reset();
        }
    }

    render() {
        const {currentAccount, onShowForm} = this.props;
        let {
            propose,
            from_account,
            to_account,
            asset,
            asset_id,
            feeAmount,
            amount,
            to_name,
            memo,
            feeAsset,
            fee_asset_id,
            balanceError,
            hasPoolBalance,
            hasBalance,
            isValidName , isLoadedValidName} = this.state;

        // debugger;

        if(!asset) return null;

        let from_my_account = AccountStore.isMyAccount(from_account);
        let from_error = from_account && !from_my_account;
        let { asset_types, fee_asset_types } = this._getAvailableAssets();
        let balance = null;
        let balance_fee = null;

        // Estimate fee
        let fee = feeAmount.getAmount({ real: true });
        if (from_account && from_account.get("balances") ) {
            //debugger;
            let account_balances = from_account.get("balances").toJS();
            let _error           = balanceError ? "has-error" : "";
            if (asset_types.length === 1)
                asset = ChainStore.getAsset(asset_types[0]);

            if (asset_types.length > 0) {
                let current_asset_id = asset ? asset.get("id") : asset_types[0];
                let feeID            = feeAsset ? feeAsset.asset_id : fee_asset_id;

                balance = (
                    <span className={"send-transfer-balance"}>
                        <Translate component="span" content="transfer.available"/>
                        :{" "}
                        <span className={classnames("send-transfer-balance-all" , {"has-error": balanceError})} onClick={()=>this._setTotal(current_asset_id, account_balances[current_asset_id], fee, feeID)}>
                            <BalanceComponent noPrefix balance={account_balances[current_asset_id]} />
                        </span>
                    </span>
                );

                if (feeID === current_asset_id && balanceError) {
                    balance_fee = (
                        <span className={classnames("send-transfer-balance-fee" , {"has-error": balanceError})}>
                            <Translate content="transfer.errors.insufficient"/>
                        </span>
                    );
                }
            } else {
                balance     = (
                    <span className={classnames({"has-error": balanceError})}>
                        <Translate content="transfer.errors.noFunds"/>
                    </span>
                );
                balance_fee = (
                    <span className={classnames({"has-error": balanceError})}>
                        <Translate content="transfer.errors.noFunds"/>
                    </span>
                );
            }
        }

        const amountValue      = parseFloat(
            String.prototype.replace.call(amount, /,/g, "")
        );
        const isAmountValid    = amountValue && !isNaN(amountValue);
        const isSendNotValid   =
            !isValidName ||
            !from_account ||
            !to_account ||
            !isAmountValid ||
            !asset ||
            !hasBalance ||
            !hasPoolBalance ||
            from_error ||
            balanceError ||
            (!AccountStore.isMyAccount(from_account) && !propose);

        let tabIndex = this.props.tabIndex || 0; // Continue tabIndex on props count


        return (
            <div className="send-transfer">
                <div className="send-transfer-header">
                    <Translate
                        component={"div"}
                        className={"send-transfer-title"}
                        content="account.withdraw" with={{
                            symbol: asset ? getAlias(asset.get("symbol")) : "" }} />
                    <br/>
                    <Translate
                        component={"div"}
                        className={"send-transfer-sub-title"}
                        content="account.withdraw_deex" />
                    <br/>
                </div>
                <form noValidate>
                    <div className={"send-transfer-row to"} style={{display: "none"}}>
                        <AccountSelector
                            label="transfer.to"
                            accountName={to_name}
                            account={to_account}
                            onChange={this.toChanged}
                            onAccountChanged={this.onToAccountChanged.bind(
                                this
                            )}
                            size={60}
                            typeahead={true}
                            tabIndex={tabIndex++}
                            hideImage
                        />
                    </div>

                    <div className={"send-transfer-row amount"}>

                        <AmountSelector
                            label="transfer.amount"
                            amount={amount}
                            onChange={this.onAmountChanged}
                            asset={
                                asset_types.length > 0 && asset
                                    ? asset.get("id")
                                    : asset_id ? asset_id : asset_types[0]
                            }
                            assets={[asset.get("id")]}
                            display_balance={balance}
                            tabIndex={tabIndex++}
                        />
                    </div>
                    {/*  M E M O  */}
                    <div className={"send-transfer-row memo"}>
                        {memo && memo.length ? (
                            <label className="right-label">
                                {memo.length}
                            </label>
                        ) : null}
                        <Translate
                            className="left-label tooltip"
                            component="label"
                            content="transfer.send_migrate"
                            data-place="top"
                            data-tip={counterpart.translate(
                                "tooltip.memo_tip"
                            )}
                        />
                        <textarea
                            style={{ marginBottom: 0 }}
                            rows="1"
                            value={memo}
                            tabIndex={tabIndex++}
                            onBlur={event=>this.getValidDeexName(event.target.value)}
                            onChange={event=>this.onMemoChanged(event)}
                        />
                        
                        {!isValidName && isLoadedValidName ? <Translate component={"div"} className={"send-withdraw-error"} content={"form.message.not_account_name"} /> : null}
                        
                    </div>

                    <FeeAssetSelector
                        label="transfer.fee"
                        account={from_account}
                        trxInfo={{
                            type: "transfer",
                            options: ["price_per_kbyte"],
                            data: {
                                type: "memo",
                                content: "migration:" + memo
                            }
                        }}
                        onChange={(result)=>this.onFeeChanged(result)}
                        tabIndex={tabIndex++}
                    />

                    <div className={"send-transfer-row group-button"}>
                        <button
                            className={classnames("btn btn-gray")}
                            type="button"
                            value="Cancel"
                            onClick={()=>onShowForm({})} >
                            <Translate component="span" content="transfer.cancel"/>
                        </button>
                        <button
                            className={classnames("btn btn-green", {disabled: isSendNotValid})}
                            disabled={isSendNotValid}
                            type="submit"
                            onClick={
                                !isSendNotValid ? this.onSubmit.bind(this) : null
                            }
                            tabIndex={tabIndex++} >
                            <Translate component="span" content="transfer.send"/>
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}

const SendTransferBind = BindToChainState(SendTransfer);

class SendTransferConnectWrapper extends React.Component {
    render() {
        const {account, backedCoins, asset, asset_symbol} = this.props;
        const coin = backedCoins.get("TDEX")
            .filter(coins=>{
                return asset === coins.symbol || asset_symbol === coins.symbol;
            }).pop();
        const postDepositData = {
            to_name: coin?.deexMigration?.account,
        };
        
        
        return <SendTransferBind {...postDepositData} {...this.props} />;
    }
}

export default connect(
    SendTransferConnectWrapper,
    {
        listenTo() {
            return [AccountStore];
        },
        getProps() {
            return {
                currentAccount: AccountStore.getState().currentAccount
                    || AccountStore.getState().passwordAccount,
                passwordAccount: AccountStore.getState().passwordAccount
            };
        }
    }
);

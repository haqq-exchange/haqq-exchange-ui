import LinkToAssetById from "../../Utility/LinkToAssetById";
import EquivalentPrice from "../../Utility/EquivalentPrice";
import React from "react";

const AccountEmptyAsset = props => {
    return props.optionalAssets.map(asset => {
        return (
            <tr key={asset} style={{maxWidth: "100rem"}}>
                <td style={{textAlign: "left"}}>
                    <LinkToAssetById asset={asset} />
                </td>
                <td style={{textAlign: "right"}}>0</td>
                <td className="column-hide-small" style={{textAlign: "right"}}>
                    <EquivalentPrice
                        fromAsset={asset}
                        pulsate={{reverse: true, fill: "forwards"}}
                        hide_symbols
                    />
                </td>
                <td className="column-hide-small" style={{textAlign: "right"}}>
                    0.00%
                </td>
                <td className="column-hide-small" style={{textAlign: "right"}}>
                    -
                </td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
            </tr>
        );
    });
};

export default AccountEmptyAsset;

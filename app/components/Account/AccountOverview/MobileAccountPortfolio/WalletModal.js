import React from "react";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
import DefaultModal from "Components/Modal/DefaultModal";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";
import {Checkbox} from "antd";
import Translate from "react-translate-component";



class WalletModal extends DefaultBaseModal {

    closeModal = () => {
        const {modalId} = this.props;
        ModalActions.hide(modalId);
    };
    render () {
        const {isOpenModal} = this.state;
        const {modalId} = this.props;
        return (
            <DefaultModal
                id={modalId}
                isOpen={isOpenModal}
                className={modalId}
                onRequestClose={this.closeModal}
            >
                <div className="modal fadeIn is-active">
                    <div className="modal-header">
                        <a onClick={this.closeModal} className="close-button">×</a>
                    </div>
                    <div className="modal-content">
                        {this.props.children}
                    </div>
                </div>

            </DefaultModal>
        );
    }
}


class WalletModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    resolve: () => ModalStore.getState().resolve,
                }}
            >
                <WalletModal {...this.props} />
            </AltContainer>
        );
    }
}
export default WalletModalContainer;
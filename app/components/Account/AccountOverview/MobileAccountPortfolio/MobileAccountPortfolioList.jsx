import React from "react";
import BalanceComponent from "Utility/BalanceComponent";
import {BalanceValueComponent} from "Utility/EquivalentValueComponent";
import assetUtils from "common/asset_utils";
import cname from "classnames";
import AssetName from "Components/Utility/AssetName";
import {Link} from "react-router-dom";
import LinkToAssetById from "Utility/LinkToAssetById";
import {getBackedCoin} from "common/gatewayUtils";
import {ChainStore, FetchChain} from "deexjs";
import {connect} from "alt-react";
import SettingsStore from "stores/SettingsStore";
import GatewayStore from "stores/GatewayStore";
import MarketsStore from "stores/MarketsStore";
import Icon from "Components/Icon/Icon";
import PulseIcon from "Components/Icon/PulseIcon";
import utils from "common/utils";
import SettingsActions from "actions/SettingsActions";
import ZfApi from "react-foundation-apps/src/utils/foundation-api";
import PaginatedList from "Utility/PaginatedList";
import MarketUtils from "common/market_utils";
import WalletDb from "stores/WalletDb";
import WalletUnlockActions from "actions/WalletUnlockActions";
import AccountStore from "stores/AccountStore";
import {LimitOrder} from "common/MarketClasses";
import {diff} from "deep-object-diff";
import Translate from "react-translate-component";
import ModalActions from '../../../../actions/ModalActions';

class MobileAccountPortfolioList extends React.Component {
    constructor() {
        super();

        this.state = {
            isSettleModalVisible: false,
            isBorrowModalVisible: false,
            borrow: null,
            settleAsset: "1.3.0",
            depositAsset: null,
            withdrawAsset: null,
            bridgeAsset: null,
            allRefsAssigned: false,
            balanceRows: [],
            showBottom: {}
        };

        this.inOrders = {};
        this.qtyRefs = {};
        this.priceRefs = {};
        this.valueRefs = {};
        this.changeRefs = {};
        for (let key in this.sortFunctions) {
            this.sortFunctions[key] = this.sortFunctions[key].bind(this);
        }
        this._checkRefAssignments = this._checkRefAssignments.bind(this);

    }


    componentDidMount(){
        this.refCheckInterval = setInterval(this._checkRefAssignments);
        this._renderBalances(this.props.assetBalancesList);
    }

    componentDidUpdate(prevProps, prevState){
        let diffUpdate = diff(prevProps.assetBalancesList.toJS(), this.props.assetBalancesList.toJS());
        
        console.log("prevState", prevState)
        
        if( Object.keys(diffUpdate).length
            || !prevProps.hiddenAssets.equals(this.props.hiddenAssets)
            || !prevProps.favoriteAssets.equals(this.props.favoriteAssets)
            //|| !utils.are_equal_shallow(prevState.showBottom, this.state.showBottom)
            || prevProps.account_name !== this.props.account_name
            || prevProps.sortKey !== this.props.sortKey
            || prevProps.wallet_locked !== this.props.wallet_locked
            || prevProps.sortDirection !== this.props.sortDirection
            || prevProps.preferredUnit !== this.props.preferredUnit

        ) {
            this._renderBalances(this.props.assetBalancesList);
        }
    }

    componentWillUnmount() {
        clearInterval(this.refCheckInterval);
    }

    _checkRefAssignments() {
        /*
        * In order for sorting to work all refs must be assigned, so we check
        * this here and update the state to trigger a rerender
        */
        if (!this.state.allRefsAssigned) {
            let refKeys = ["qtyRefs", "priceRefs", "valueRefs", "changeRefs"];
            const allRefsAssigned = refKeys.reduce((a, b) => {
                if (a === undefined) return !!Object.keys(this[b]).length;
                return !!Object.keys(this[b]).length && a;
            }, undefined);
            if (allRefsAssigned) {
                clearInterval(this.refCheckInterval);
                this.setState({allRefsAssigned});
            }
        }
    }

    shouldComponentUpdate(np, ns) {

        
        return (
            !utils.are_equal_shallow(ns, this.state) ||
            !utils.are_equal_shallow(np.backedCoins, this.props.backedCoins) ||
            !utils.are_equal_shallow(np.balances, this.props.balances) ||
            !utils.are_equal_shallow(np.assetBalancesList, this.props.assetBalancesList) ||
            !utils.are_equal_shallow(
                np.optionalAssets,
                this.props.optionalAssets
            ) ||
            ns.showBottom !== this.state.showBottom ||
            np.account_name !== this.props.account_name ||
            np.visible !== this.props.visible ||
            np.settings !== this.props.settings ||
            np.account.equals(this.props.account) ||
            np.hiddenAssets.equals(this.props.hiddenAssets) ||
            np.favoriteAssets.equals(this.props.favoriteAssets) ||
            np.sortDirection !== this.props.sortDirection ||
            np.sortKey !== this.props.sortKey ||
            np.isMyAccount !== this.props.isMyAccount ||
            np.preferredUnit !== this.props.preferredUnit ||
            np.allMarketStats.reduce((a, value, key) => {
                return (
                    utils.check_market_stats(
                        value,
                        this.props.allMarketStats.get(key)
                    ) || a
                );
            }, false)
        );
    }

    sortFunctions = {
        qty: function(a, b, force) {
            if (Number(this.qtyRefs[a.key]) < Number(this.qtyRefs[b.key]))
                return this.props.sortDirection || force ? -1 : 1;

            if (Number(this.qtyRefs[a.key]) > Number(this.qtyRefs[b.key]))
                return this.props.sortDirection || force ? 1 : -1;
        },
        alphabetic: function(a, b, force) {
            // console.log("a, b, force", a, b, force);
            if (a.key > b.key)
                return this.props.sortDirection || force ? 1 : -1;
            if (a.key < b.key)
                return this.props.sortDirection || force ? -1 : 1;
            return 0;
        },
        priceValue: function(a, b) {
            // console.log("a, b, force", a, b);
            let aPrice = this.priceRefs[a.key];
            let bPrice = this.priceRefs[b.key];
            if (aPrice && bPrice) {
                return this.props.sortDirection
                    ? aPrice - bPrice
                    : bPrice - aPrice;
            } else if (aPrice === null && bPrice !== null) {
                return 1;
            } else if (aPrice !== null && bPrice === null) {
                return -1;
            } else {
                return this.sortFunctions.alphabetic(a, b, true);
            }
        },
        inOrders: function(a, b) {
            // console.log("a, b, force", a, b);
            let aPrice = this.inOrders[a.key];
            let bPrice = this.inOrders[b.key];
            // console.log("aPrice", aPrice, bPrice)
            if (aPrice && bPrice) {
                return this.props.sortDirection
                    ? aPrice - bPrice
                    : bPrice - aPrice;
            } else if (aPrice === null && bPrice !== null) {
                return 1;
            } else if (aPrice !== null && bPrice === null) {
                return -1;
            } else {
                return this.sortFunctions.alphabetic(a, b, true);
            }
        },
        totalValue: function(a, b) {
            let aValue = this.valueRefs[a.key];
            let bValue = this.valueRefs[b.key];
            if (aValue && bValue) {
                return this.props.sortDirection
                    ? aValue - bValue
                    : bValue - aValue;
            } else if (!aValue && bValue) {
                return 1;
            } else if (aValue && !bValue) {
                return -1;
            } else {
                return this.sortFunctions.alphabetic(a, b, true);
            }
        },
        changeValue: function(a, b) {
            let aValue = this.changeRefs[a.key];
            let bValue = this.changeRefs[b.key];

            if (aValue && bValue) {
                let aChange =
                    parseFloat(aValue) != "NaN" ? parseFloat(aValue) : aValue;
                let bChange =
                    parseFloat(bValue) != "NaN" ? parseFloat(bValue) : bValue;
                let direction =
                    typeof this.props.sortDirection !== "undefined"
                        ? this.props.sortDirection
                        : true;

                return direction ? aChange - bChange : bChange - aChange;
            }
        }
    };

    _showWindowSend = (name, assetId) => {
        const _this = this;
        console.log("name, assetId", name, assetId);
        if (WalletDb.isLocked()) {
            ModalActions.show("setting_wallet_modal").then(() => {
                _this._onShowForm(name, assetId);
                console.log("setting_wallet_modal showin");
            });
        } else {

            this._onShowForm(name, assetId);
        }
    };

    _onShowForm(type, asset_id) {


        const {account} = this.props;
        const asset = ChainStore.getAsset(asset_id);
        window.scrollTo(0, 0);
        this.props.onShowForm({
            from_name: account.get("name"),
            type,
            asset_id,
            asset
        });
    }

    _getOrders(assetId) {
        const {account} = this.props;
        const accountOrders = account.get("orders");

        return accountOrders
            .toArray()
            .map(order => {
                let o = ChainStore.getObject(order);
                if (!o) return null;
                let sellBase = o.getIn(["sell_price", "base", "asset_id"]),
                    sellQuote = o.getIn(["sell_price", "quote", "asset_id"]);

                if ( sellBase === assetId ) {
                    const base = ChainStore.getAsset(sellBase);
                    const quote = ChainStore.getAsset(sellQuote);
                    const assets = {
                        [base.get("id")]: {precision: base.get("precision")},
                        [quote.get("id")]: {precision: quote.get("precision")}
                    };
                    let localLimitOption = new LimitOrder(o.toJS(), assets, quote.get("id"));
                    return utils.format_number(
                        localLimitOption.amountForSale().getAmount({real: true}),
                        quote.get("precision")
                    );
                }
            })
            .filter(a => !!a);
        
    }
    
    changeVisibleBottom = (asset, isActive) => {
        let {showBottom} = this.state;
        this.setState({
            showBottom: Object.assign(showBottom, {
                [asset.get("symbol")]: isActive
            })
        }, ()=> {
            this._renderBalances(this.props.assetBalancesList);
        });
    };

    _renderBalances(balanceList) {
        const _this = this;
        const {
            isMyAccount, account,
            account_name,
            coreSymbol,
            preferredUnit, asideComponent,
            backedCoins,
            hiddenAssets,
            favoriteAssets,
            orders
        } = this.props;

        let {showBottom} = this.state;

        //console.log("MobileAccountPortfolioList balanceList", balanceList );
        
        // console.log("asideComponent", asideComponent);
        // console.log("showBottom", showBottom);


        new Promise(resolve=>{
            let balances = [];
            balanceList.map(asset=>{
                let assetId = asset.has("balance") ? asset.get("asset_type") : asset.get("id");
                // console.log("balance, index", assetId , asset.toJS() );
                //
                const hasBalance = asset.has("balance") && asset.get("balance");
                const hasOnOrder = !!orders[asset.get("asset_type")];
                const inOrders = this._getOrders(assetId);
                const orderAmount = utils.format_number(inOrders.reduce((a, b) => Number(a) + Number(b), 0), asset.get("precision"), false);
                let ordersLink = inOrders.length ? <Link to={["/account", account.get("name") ,"orders", asset.get("symbol")].join("/")}>{orderAmount}</Link>: "0";
                this.inOrders[asset.get("symbol")] = inOrders.length ? orderAmount : "0";

                let {market} = assetUtils.parseDescription(
                    asset.getIn(["options", "description"])
                );
                const notCore = asset.get("id") !== "1.3.0";
                const notCorePrefUnit = preferredUnit !== coreSymbol;
                let preferredMarket = market ? market : preferredUnit;
                let directMarketLink = notCore
                    ? preferredMarket : notCorePrefUnit
                        ? preferredUnit : preferredUnit;
                if( asset.get("symbol") === directMarketLink ) {
                    if(__GBL_CHAIN__) {
                        directMarketLink = directMarketLink === "GBL" ? "USDT" : "GBL" ;
                    } else if(__SCROOGE_CHAIN__) {
                        directMarketLink = directMarketLink === "SCROOGE" ? "USDT" : "SCROOGE" ;
                    } else {
                    directMarketLink = directMarketLink === "DEEX" ? "USDT" : "DEEX" ;
                    }
                }

                const backedCoin = getBackedCoin(asset.get("symbol"), backedCoins);
                const canDeposit = backedCoin && backedCoin.depositAllowed;
                const canWithdrawDeexMigration = backedCoin && backedCoin.deexMigration;
                const canWithdrawDeexBalance = canWithdrawDeexMigration && hasBalance;

                const canWithdraw = backedCoin
                    && backedCoin.withdrawalAllowed
                    && (hasBalance && asset.get("balance") !== 0);

                const assetAmount = asset.get("balance");

                /* Sorting refs */
                this.qtyRefs[asset.get("symbol")] = utils.get_asset_amount(
                    assetAmount,
                    asset
                );




                let preferredAsset = ChainStore.getAsset(preferredUnit);
                this.valueRefs[asset.get("symbol")] =
                    hasBalance && !!preferredAsset
                        ? MarketUtils.convertValue(
                            asset.get("balance"),
                            preferredAsset,
                            asset,
                            this.props.allMarketStats,
                            this.props.core_asset,
                            true
                        ) : null;

                this.priceRefs[asset.get("symbol")] = !preferredAsset
                    ? null
                    : MarketUtils.getFinalPrice(
                        this.props.core_asset,
                        asset,
                        preferredAsset,
                        this.props.allMarketStats,
                        true
                    );

                let marketId = asset.get("symbol") + "_" + preferredUnit;
                let currentMarketStats = this.props.allMarketStats.get(marketId);
                this.changeRefs[asset.get("symbol")] =
                    currentMarketStats && currentMarketStats.change ? currentMarketStats.change : 0;

                let isMigration = canWithdrawDeexMigration && canWithdrawDeexBalance && this.props.isMyAccount;
                //console.log("favoriteAssets.has(assetId, false)", favoriteAssets.toJS(), favoriteAssets, account_name, assetId, favoriteAssets.hasIn([account_name,assetId], false));
                
                // описание ассета
                let description = asset.get("options")._root.nodes[5].nodes[1].entry[1];
                let type = "-";
                let nftPicture;
                let nftType;
                if(description) {
                    type = JSON.parse(description).nft_object?.type;
                    nftType = JSON.parse(description).type;
                    nftPicture = 
                        JSON.parse(description).nft_object?.media_png ||
                        JSON.parse(description).nft_object?.media_gif ||
                        JSON.parse(description).nft_object?.media_jpeg || "";
                }

                balances.push(
                    <tr key={asset.get("symbol")} >
                        <td>
                            <div className={cname("table-row", {
                                "top-show": showBottom[asset.get("symbol")],
                                "top-hide": !showBottom[asset.get("symbol")]
                            })}>
                                <div className={cname("table-row-top")}>
                                    <div className={"table-item-top star"}>
                                        <span
                                            className={cname("table-item-star", { active: favoriteAssets.hasIn([account_name,assetId], false) })}
                                            onClick={()=>SettingsActions.favoriteAsset(assetId, !favoriteAssets.hasIn([account_name,assetId]), account_name) }>
                                        </span>
                                    </div>
                                    <div className={"table-item-top asset"}  style={{display: "flex", alignItems: "center"}}>
                                        <LinkToAssetById className={"table-item-link"} asset={assetId} showLogo />
                                        {__SCROOGE_CHAIN__ ? null :
                                        <>
                                            {(asset.get("symbol") === "BALLS") ? 
                                                <a style={{lineHeight: "0px"}} target="_blank" href={"https://static.haqq.exchange/gbl/images/ball.png"}>                                            
                                                    <Picture />
                                                </a> :
                                            <>
                                                {nftType === "nft" ?
                                                    <a style={{lineHeight: "0px"}} target="_blank" href={nftPicture.length > 0 ? window.atob(nftPicture) : JSON.parse(description).image}>                                            
                                                        <Picture />
                                                    </a> 
                                                : null} 
                                            </>}
                                        </>}
                                    </div>
                                    <div className={"table-item-top balance"}>
                                        {hasBalance || hasOnOrder ? (
                                            <BalanceComponent balance={asset.get("balance_id")} hide_asset />
                                        ) : null}
                                    </div>
                                    <div className={"table-item-top bottom"}>
                                        <span className={cname({
                                            "arrow-up": !showBottom[asset.get("symbol")],
                                            "arrow-down": showBottom[asset.get("symbol")]
                                        })} onClick={()=>_this.changeVisibleBottom(asset, !showBottom[asset.get("symbol")]) } />
                                    </div>
                                </div>
                                <div className={cname("table-row-bottom", {"hide": !showBottom[asset.get("symbol")]})}>
                                    <div className={"table-item-bottom-info"}>
                                        {__SCROOGE_CHAIN__ ? null :
                                        <div className={"table-item-bottom-group"}>
                                            <div className={"table-item-bottom-title"}>
                                                <Translate component={"span"} content="account.type_header"/>
                                            </div>
                                            {/* костыль для протатипа */}
                                            {(asset.get("symbol") === "MRK" || asset.get("symbol") === "MORTLCOMPAT" || asset.get("symbol") === "FIGHTKILLER"
                                            || asset.get("symbol") === "HABIBNURGOMEDOV" ||  asset.get("symbol") === "HASAMABDULAEV") ? 
                                            <span>nft</span> : null}
                                            {/* рабочая версия */}
                                            <span>{type}</span>
                                        </div>}
                                        <div className={"table-item-bottom-group"}>
                                            <div className={"table-item-bottom-title"}>
                                                <Translate component={"span"} content="account.order_header"/>
                                            </div>
                                            {ordersLink}
                                        </div>
                                        {hasBalance || hasOnOrder ? (
                                            <div className={"table-item-bottom-group"}>
                                                <div className={"table-item-bottom-title"}>
                                                    <Translate content="account.value_header"/>&nbsp;
                                                    <AssetName name={preferredUnit} noTip noLink/>
                                                </div>
                                                <BalanceValueComponent
                                                    balance={asset.get("balance_id")}
                                                    toAsset={preferredUnit}
                                                    hide_asset
                                                />
                                            </div>
                                        ) : null}

                                        {isMyAccount && (<div className={"table-item-bottom-close"}>
                                            <span
                                                className={cname("table-item-bottom-hidden-text", {
                                                    "hide-close": hiddenAssets.hasIn([account_name,assetId], false),
                                                    "show-close": !hiddenAssets.hasIn([account_name,assetId], false)
                                                })}
                                                onClick={()=>{
                                                    _this.changeVisibleBottom(asset, !showBottom[asset.get("symbol")]);
                                                    SettingsActions.hideAsset(assetId, !hiddenAssets.hasIn([account_name,assetId]), account_name);
                                                } } />
                                        </div>)}
                                    </div>
                                    <div className={"table-item-bottom-button"}>

                                        <div className={"table-item-bottom"}>
                                            <Link to={["/market", [asset.get("symbol"),directMarketLink].join("_")].join("/")}>
                                                <Icon
                                                    name="trade"
                                                    title="icons.trade.trade"
                                                    className="icon-12px"
                                                />
                                                <Translate content={"account.trade"} />
                                            </Link>
                                        </div>
                                        {hasBalance && isMyAccount && (
                                            <div className={"table-item-bottom"}>
                                                <a onClick={()=>this._showWindowSend(asideComponent.send, assetId)}>
                                                    <Icon
                                                        name="transfer"
                                                        title="icons.transfer"
                                                        className="icon-12px"
                                                    />
                                                    <Translate content={"account.send"} />
                                                </a>
                                            </div>
                                        )}
                                        {canDeposit && isMyAccount && (<div className={"table-item-bottom"}>
                                            <a onClick={()=>this._showWindowSend(asideComponent.deposit, assetId)}>
                                                <Icon
                                                    name="deposit"
                                                    title="icons.deposit.deposit"
                                                    className="icon-12px"
                                                />
                                                <Translate content={"account.deposit"} />
                                            </a>
                                        </div>)}

                                        {canWithdraw && isMyAccount && (<div className={"table-item-bottom"}>
                                            <a onClick={()=>this._showWindowSend(asideComponent.withdraw, assetId)}>
                                                <Icon
                                                    name="withdraw"
                                                    title="icons.withdraw"
                                                    className="icon-12px"
                                                />
                                                <Translate content={"account.withdraw"} />
                                            </a>
                                        </div>)}

                                        {isMigration ? <div className={"table-item-bottom"}><Icon
                                            onClick={()=>this._showWindowSend(asideComponent.deexWithdraw, asset.get("symbol") ) }
                                            name="withdraw"
                                            className="deex-withdraw"
                                            title="icons.withdraw"
                                            style={{cursor: "pointer"}}
                                        /></div> : null}
                                    </div>
                                </div>
                            </div>
                        </td>


                    </tr>
                );
            });
            resolve(balances);
        }).then(result=>{
            result.sort(_this.sortFunctions[_this.props.sortKey]);
            this.setState({
                balanceRows: result
            });
        });

    }


    render() {

        const {balanceRows} = this.state;

        //console.log("MobileAccountPortfolioList.jsx render");

        // console.log("this.inOrders", this.inOrders);
        // console.log("this.qtyRefs", this.qtyRefs);
        // console.log("this.priceRefs", this.priceRefs);
        // console.log("this.valueRefs", this.valueRefs);
        // console.log("this.changeRefs", this.changeRefs);

        return (
            <PaginatedList
                style={{padding: 0}}
                className="table-account"
                rows={balanceRows}
                header={this.props.header}
                footer={this.props.footer}
                withTransition={true}
                pageSize={100}
                label="utility.total_x_assets"
                extraRow={this.props.extraRow}
                leftPadding="1.5rem"
            >
            </PaginatedList>
        );
    }
}

const Picture = () => {
    return (
        <svg width="20px" height="20px" fill="#ffffff" version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
            viewBox="0 0 512 512" style={{enableBackground: "new 0 0 512 512", margin: "0 40px 0 0"}} >
            <g>
                <g>
                    <path d="M446.575,0H65.425C29.349,0,0,29.35,0,65.426v381.149C0,482.65,29.349,512,65.425,512h381.15
                        C482.651,512,512,482.65,512,446.574V65.426C512,29.35,482.651,0,446.575,0z M481.842,446.575
                        c0,19.447-15.821,35.267-35.267,35.267H65.425c-19.447,0-35.268-15.821-35.268-35.267v-55.007l99.255-84.451
                        c3.622-3.082,8.906-3.111,12.562-0.075l62.174,51.628c5.995,4.977,14.795,4.569,20.304-0.946L372.181,209.77
                        c2.67-2.675,5.783-2.935,7.408-2.852c1.62,0.083,4.695,0.661,7.078,3.596l95.176,117.19V446.575z M481.842,279.865l-71.766-88.366
                        c-7.117-8.764-17.666-14.122-28.942-14.701c-11.268-0.57-22.317,3.672-30.294,11.662L212.832,326.681l-51.59-42.839
                        c-14.959-12.422-36.563-12.293-51.373,0.308l-79.712,67.822V65.426c0-19.447,15.821-35.268,35.268-35.268h381.15
                        c19.447,0,35.267,15.821,35.267,35.268V279.865z"/>
                </g>
            </g>
            <g>
                <g>
                    <path d="M161.174,62.995c-40.095,0-72.713,32.62-72.713,72.713c0,40.094,32.619,72.713,72.713,72.713s72.713-32.619,72.713-72.713
                        S201.269,62.995,161.174,62.995z M161.174,178.264c-23.466,0-42.556-19.091-42.556-42.556c0-23.466,19.09-42.556,42.556-42.556
                        c23.466,0,42.556,19.091,42.556,42.556S184.64,178.264,161.174,178.264z"/>
                </g>
            </g>
        </svg>
    )
}

export default  connect(
    MobileAccountPortfolioList,
    {
        listenTo() {
            return [SettingsStore, GatewayStore, MarketsStore, AccountStore];
        },
        getProps() {
            return {
                settings: SettingsStore.getState().settings,
                favoriteAssets: SettingsStore.getState().favoriteAssets,
                viewSettings: SettingsStore.getState().viewSettings,
                backedCoins: GatewayStore.getState().backedCoins,
                bridgeCoins: GatewayStore.getState().bridgeCoins,
                gatewayDown: GatewayStore.getState().down,
                allMarketStats: MarketsStore.getState().allMarketStats,
                currentAccount: AccountStore.getState().currentAccount,
            };
        }
    }
);



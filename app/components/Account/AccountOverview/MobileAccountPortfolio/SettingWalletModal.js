import React from "react";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
import DefaultModal from "Components/Modal/DefaultModal";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";
import {Checkbox} from "antd";
import Translate from "react-translate-component";
import classname from "classnames";
import AssetName from "Components/Utility/AssetName";
import Icon from "Components/Icon/Icon";



class SettingWalletModal extends DefaultBaseModal {

    activeClassName = (typeName) => {
        const {sortKey, sortDirection} = this.props;
        return {
            "active": typeName === sortKey,
            "up": typeName === sortKey && sortDirection,
            "down": typeName === sortKey && !sortDirection,
        };
    };
    closeModal = () => {
        const {modalId} = this.props;
        ModalActions.hide(modalId);
    };
    render () {
        const {isOpenModal} = this.state;
        const {modalId, filters, favoriteAssets, toggleSortOrder, preferredUnit,
            account_name, hiddenAssets} = this.props;
        console.log("isOpenModal", modalId, isOpenModal);
        return (
            <DefaultModal
                id={modalId}
                isOpen={isOpenModal}
                className={modalId}
                onRequestClose={this.closeModal}
            >
                <div className="modal fadeIn is-active">
                    <div className="modal-header">
                        <a onClick={this.closeModal} className="close-button">×</a>
                    </div>
                    <div className="modal-content">
                        <div className="account-portfolio">

                            <div className="account-portfolio-wrap">
                                <div className="header-selector-filter">
                                    <Checkbox
                                        name={"hideNull"} onChange={this.props.onChangeCheckbox}
                                        checked={filters.hideNull}>
                                        <Translate content="account.hide_null"/>
                                    </Checkbox>
                                    {favoriteAssets.get(account_name) && favoriteAssets.get(account_name).size ? (
                                        <Checkbox
                                            name={"filterFavorite"} onChange={this.props.onChangeCheckbox}
                                            checked={filters.filterFavorite}>
                                            <Translate content="account.see_favorite"/>
                                        </Checkbox>
                                    ) : null}
                                    {hiddenAssets.get(account_name) && hiddenAssets.get(account_name).size ? (
                                        <Checkbox
                                            name={"filterHidden"} onChange={this.props.onChangeCheckbox}
                                            checked={filters.filterHidden}>
                                            <Translate content="account.shown_hidden"/>
                                        </Checkbox>
                                    ) : null}
                                </div>
                            </div>

                            <div className="account-portfolio-wrap">
                                <div className={"header-selector-filter"}>
                                    <Translate className={"header-selector-title"} component="div" content="account.sorting"/>

                                    <div
                                        className={classname("header-selector-item alphabetic clickable", this.activeClassName("alphabetic"))}
                                        onClick={toggleSortOrder.bind(this, "alphabetic")}
                                    >
                                        <Translate
                                            component="span"
                                            content="account.asset.active"
                                        />
                                    </div>
                                    <div
                                        onClick={toggleSortOrder.bind(this, "qty")}
                                        className={classname("header-selector-item clickable", this.activeClassName("qty"))}
                                    >
                                        <Translate content="account.qty"/>
                                    </div>
                                    <div onClick={toggleSortOrder.bind(this, "totalValue")}
                                         className={classname("header-selector-item clickable", this.activeClassName("totalValue"))}>
                                        <AssetName name={preferredUnit} noTip noLink/>&nbsp;
                                        <Translate content="account.value_header"/>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </DefaultModal>
        );
    }
}

SettingWalletModal.defaultProps = {
    modalId: "setting_wallet_modal"
};

class SettingWalletModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    resolve: () => ModalStore.getState().resolve,
                }}
            >
                <SettingWalletModal {...this.props} />
            </AltContainer>
        );
    }
}
export default SettingWalletModalContainer;
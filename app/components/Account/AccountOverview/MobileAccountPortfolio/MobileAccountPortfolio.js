import React from "react";
import {Checkbox} from "antd";
import classname from "classnames";
import WalletDb from "stores/WalletDb";
import loadable from "loadable-components";
import Translate from "react-translate-component";
import AssetName from "Components/Utility/AssetName";
import Icon from "Components/Icon/Icon";
import SettingsActions from "actions/SettingsActions";
import MobileAccountPortfolioList from "./MobileAccountPortfolioList";
import WalletUnlockActions from "actions/WalletUnlockActions";
import BindToChainState from "Components/Utility/BindToChainState";
import TotalBalanceValue from "components/Utility/TotalBalanceValue";
import TranslateWithLinks from "components/Utility/TranslateWithLinks";

import SettingWalletModal from "./SettingWalletModal";
import WalletModal from "./WalletModal";

const SendTransfer = loadable(() => import("../AccountPortfolio/SendTransfer"));
const DepositAddress = loadable(() => import("../AccountPortfolio/DepositAddress"));
const SendWithdraw = loadable(() => import("../AccountPortfolio/SendWithdraw"));
const SendDeexWithdraw = loadable(() => import("../AccountPortfolio/SendDeexWithdraw"));

import "../AccountPortfolio/AccountPortfolio.scss";
import counterpart from "counterpart";
import ModalActions from "actions/ModalActions";

const EmptyAside = () => {
    return (
        <div className={"account-portfolio-empty-aside"}>
            <Translate content={"account.info_block"} />
        </div>
    );
};

class AccountPortfolio extends React.Component {
    static defaultProps = {
        asideComponent: {
            "send": "send_transfer",
            "deposit": "deposit_address",
            "deexWithdraw": "deex_withdraw",
            "withdraw": "send_withdraw"
        }

    };

    constructor(props) {
        super(props);

        this.state = {
            send_asset: null,
            alwaysShowAssets: null,
            alwaysShowAsset: __SCROOGE_CHAIN__ ? ["SCROOGE"] : ["DEEX"],
            shownAssets: props.viewSettings.get("shownAssets", "active"),
            sortKey: props.viewSettings.get("portfolioSort", "totalValue"),
            sortDirection: props.viewSettings.get(
                "portfolioSortDirection",
                true
            ),
            rightAside: {}
        };

        this.qtyRefs = {};
        this.priceRefs = {};
        this.valueRefs = {};
        this.changeRefs = {};
        this.modalRef = {};


        this._showWindowSend = this._showWindowSend.bind(this);
        this._onSettleAsset = this._onSettleAsset.bind(this);
        this._handleFilterInput = this._handleFilterInput.bind(this);
        this._toggleSortOrder = this._toggleSortOrder.bind(this);


    }

    componentDidUpdate(prevProps){
        const { filters , hiddenAssets, favoriteAssets, account_name }=this.props;
        if( filters.filterHidden && (hiddenAssets.get(account_name) && !hiddenAssets.get(account_name).size) ) {
            this.props.onChangeVisibleAsset({
                name: "filterHidden",
                value: false
            });
        }
        if( filters.filterFavorite && (favoriteAssets.get(account_name) && !favoriteAssets.get(account_name).size) ) {
            this.props.onChangeVisibleAsset({
                name: "filterFavorite",
                value: false
            });
        }

        if( this.props.isMyAccount !== prevProps.isMyAccount) {
            this.onShowForm("empty");
        }
    }

    _handleFilterInput = event => {
        event.preventDefault();
        this.setState({
            filterValue: event.target.value
        });

        this.props.onChangeVisibleAsset({
            name: "filterValue",
            value: event.target.value
        });
    };

    _hideAsset = (asset, status) => {
        SettingsActions.hideAsset(asset, status);
    };


    _changeShownAssets = shownAssets => {
        if (this.state.shownAssets !== shownAssets) {
            this.setState({shownAssets});
            SettingsActions.changeViewSetting({shownAssets});
        }
    };


    _onSettleAsset = (id, e) => {
        e.preventDefault();
        this.setState({
            settleAsset: id
        });

        this.settlement_modal.show();
    };

    _toggleSortOrder = sortKey => {
        const {sortDirection} = this.state;

        SettingsActions.changeViewSetting({
            portfolioSort: sortKey,
            portfolioSortDirection: !sortDirection
        });
        this.setState({
            sortKey: sortKey,
            sortDirection: !sortDirection
        });
    };

    _triggerSend = asset => {
        this.setState({send_asset: asset}, () => {
            if (this.send_modal) this.send_modal.show();
        });
    };

    _showWindowSend = asset => {
        const _this = this;
        if (WalletDb.isLocked()) {
            WalletUnlockActions.unlock().then(() => {
                _this._triggerSend(asset);
            });
        } else {
            this._triggerSend(asset);
        }
    };

    onChangeCheckbox = (event) => {
        this.setState({
            [event.target.name]: event.target.checked
        });

        SettingsActions.changeViewSetting({
            [event.target.name]: event.target.checked
        });

        this.props.onChangeVisibleAsset({
            name: event.target.name,
            value: event.target.checked
        });

    };

    onHideForm = (payload) => {
        const {rightAside} = this.state;
        ModalActions.hide(rightAside.type).then(() => {
            console.log("setting_wallet_modal hide", payload);
            this.setState({
                rightAside: {}
            });
        });
    };

    onShowForm = (payload) => {

        if( payload && payload.type ) {
            this.setState({
                rightAside: payload
            }, () => {
                ModalActions.show(payload.type).then(() => {
                    console.log("setting_wallet_modal showin", payload);
                });
            });
        }
    };

    showSettingWallet = () => {
        ModalActions.show("setting_wallet_modal").then(() => {
            console.log("setting_wallet_modal showin");
        });
    };


    render() {
        const {settings, account_name, core_asset, favoriteAssets,
            filters, visibleBalancesList, asideComponent} = this.props;
        const {rightAside, filterValue, alwaysShowAsset} = this.state;
        const {asset} = rightAside;
        const preferredUnit = settings.get("unit") || core_asset.get("symbol");
        // let showAssetPercent = settings.get("showAssetPercent", false);
        // let includedBalances, hiddenBalances;
        // let account_balances = account.get("balances");

        let dataModal = {
            account: account_name,
            asset: core_asset.get("id"),
            asset_id: core_asset.get("id")
        };

        if( asset ) {
            dataModal = Object.assign(dataModal, {
                asset: asset.get("id"),
                asset_symbol: asset.get("symbol"),
                asset_id: asset.get("id")
            });
        }

        const includedPortfolioBalance = (
            <tr key="portfolio" className="table-total-value">
                <td colSpan="4" style={{textAlign: "left"}}>
                    <div>
                        <TranslateWithLinks
                            noLink
                            string="account.total"
                            keys={[{type: "asset", value: preferredUnit, arg: "asset"}]}
                        />
                    </div>
                    <div>
                        <TotalBalanceValue noTip  balances={visibleBalancesList}/>
                    </div>
                </td>
            </tr>
        );

        return (
            <div className={"account-portfolio"} >
                <div className="account-portfolio-content">
                    <div className="header-selector">
                        <div className="header-selector-search">
                            <input
                                type="text"
                                placeholder={counterpart.translate("markets.search")}
                                onChange={this._handleFilterInput}
                            />
                        </div>
                        <div onClick={this.showSettingWallet} className={classname("header-selector-setting")} >
                            <Icon name={"filter-setting"} />
                        </div>

                    </div>

                    <MobileAccountPortfolioList
                        balanceList={visibleBalancesList}
                        optionalAssets={
                            !filterValue ? alwaysShowAsset : null
                        }
                        visible={false}
                        preferredUnit={preferredUnit}
                        coreSymbol={this.props.core_asset.get("symbol")}
                        /*header={<TableHead
                            preferredUnit={preferredUnit}
                            sortKey={this.state.sortKey}
                            sortDirection={this.state.sortDirection}
                            toggleSortOrder={this._toggleSortOrder} {...this.props} />}*/
                        footer={includedPortfolioBalance}
                        onShowForm={this.onShowForm}
                        {...this.props}
                        {...this.state}
                    />

                    <SettingWalletModal
                        preferredUnit={preferredUnit}
                        toggleSortOrder={this._toggleSortOrder}
                        onChangeCheckbox={this.onChangeCheckbox}
                        sortKey={this.state.sortKey}
                        sortDirection={this.state.sortDirection}
                        {...this.props} />

                    <WalletModal modalId={asideComponent.send}>
                        <SendTransfer onShowForm={this.onHideForm} {...dataModal}/>
                    </WalletModal>
                    <WalletModal modalId={asideComponent.withdraw}>
                        <SendWithdraw onShowForm={this.onHideForm} {...dataModal}/>
                    </WalletModal>
                    <WalletModal modalId={asideComponent.deposit}>
                        <DepositAddress onShowForm={this.onHideForm} {...dataModal}/>
                    </WalletModal>
                    <WalletModal modalId={asideComponent.deexWithdraw}>
                        <SendDeexWithdraw
                            onShowForm={this.onHideForm}
                            {...dataModal} {...this.props} />
                    </WalletModal>
                </div>
            </div>
        );
    }
}

export default  BindToChainState(AccountPortfolio);

class TableHead extends React.Component {

    activeClassName = (typeName) => {
        const {sortKey, sortDirection} = this.props;
        return {
            "active": typeName === sortKey,
            "up": typeName === sortKey && sortDirection,
            "down": typeName === sortKey && !sortDirection,
        };
    };

    render() {
        const {preferredUnit, toggleSortOrder} = this.props;
            
        // console.log("TableHead alphabetic", this.activeClassName("alphabetic"))
        // console.log("TableHead qty", this.activeClassName("qty"))
        // console.log("TableHead inOrders", this.activeClassName("inOrders"))
        // console.log("TableHead totalValue", this.activeClassName("totalValue"))

        return (
            <tr>
                <th >
                    <div className={"table-account-th"}>
                        <div className={"table-head-item star"}>
                            <i className={"table-head-item-star"}/>
                        </div>

                        <div
                            className={classname("table-head-item alphabetic clickable", this.activeClassName("alphabetic"))}
                            onClick={toggleSortOrder.bind(this, "alphabetic")}
                        >
                            <Translate
                                component="span"
                                content="account.asset.active"
                            />
                        </div>
                        <div
                            onClick={toggleSortOrder.bind(this, "qty")}
                            className={classname("table-head-item clickable", this.activeClassName("qty"))}
                        >
                            <Translate content="account.qty"/>
                        </div>
                        <div onClick={toggleSortOrder.bind(this, "totalValue")}
                             className={classname("table-head-item clickable", this.activeClassName("totalValue"))}>
                            <AssetName name={preferredUnit} noTip noLink/>&nbsp;
                            <Translate content="account.value_header"/>
                        </div>

                        <div onClick={this.showSettingWallet} className={classname("table-head-item clickable")} >
                            <Icon name={"filter-setting"} />
                        </div>
                    </div>
                </th>

            </tr>

        );
    }
}


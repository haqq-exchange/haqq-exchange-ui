import React from "react";
import Immutable from "immutable";
import Translate from "react-translate-component";
import {checkFeeStatusAsync} from "common/trxHelper";
import {Asset} from "common/MarketClasses";
import BindToChainState from "Components/Utility/BindToChainState";
import ChainTypes from "Components/Utility/ChainTypes";
import Icon from "Components/Icon/Icon";
import ConfidentialWallet from "stores/ConfidentialWallet";
import {getAlias} from "config/alias";
import ImageLoad from "Utility/ImageLoad";


import utils from "../../../lib/common/utils";
import cn from "classnames";
import ClickOutside from "react-click-outside";
import {connect} from "alt-react";
import AccountStore from "stores/AccountStore";
import SettingsStore from "stores/SettingsStore";
import WalletUnlockStore from "stores/WalletUnlockStore";
import ModalStore from "stores/ModalStore";
import GatewayStore from "stores/GatewayStore";

import "./AccountNetworkFeesStyle.scss";

class AccountNetworkFees extends React.Component {

    static propTypes = {
        asset: ChainTypes.ChainAsset.isRequired,
        account: ChainTypes.ChainAccount.isRequired,
    };


    static defaultProps = {
        asset: "1.3.0"
    };

    constructor(){
        super();

        this.state = {
            transfer: new Asset({asset_id: "1.3.0", precision: 4}),
            tableHead: {
                operation: "account.networkfees.operation",
                type: "account.networkfees.type_fee",
                price: "account.networkfees.cost"
            },
            tableData: [
                {
                    operation: "account.networkfees.translation",
                    type: "account.networkfees.transaction_fee",
                    price: "0.0000"
                },{
                    operation: "account.networkfees.translation",
                    type: "account.networkfees.price_memo",
                    price: "0.0000"
                },
                {
                    operation: "account.networkfees.place_order",
                    type: "account.networkfees.transaction_fee",
                    price: "0.0000"
                },
                {
                    operation: "account.networkfees.cancel_order",
                    type: "account.networkfees.transaction_fee",
                    price: "0.0000"
                },
                {
                    operation: "account.networkfees.stelth_from_account",
                    type: "account.networkfees.transaction_fee",
                    price: "0.0000"
                },
                {
                    operation: "account.networkfees.stelth_from_blind",
                    type: "account.networkfees.transaction_fee",
                    price: "0.0000"
                }
            ]
        };
    }

    componentDidMount(){
        this.getTransfer();
        this.getTransferBuy();
        this.getTransferCancel();
        this.getTransferStelth();
    }


    getTransfer = () => {
        const {account, asset}= this.props;
        checkFeeStatusAsync({
            accountID: account.get("id"),
            feeID: asset.get("id"),
        }).then(result => {
            this.setPriceTableData(result.fee, 0).then(feeAsset=>{
                this.getTransferKbt(feeAsset.clone());
            });
        });
    };
    // limit_order_create
    getTransferKbt = (feeTransfer) => {
        const {account, asset}= this.props;
        const {tableData}= this.state;
        checkFeeStatusAsync({
            accountID: account.get("id"),
            feeID: asset.get("id"),
            options: ["price_per_kbyte"],
            data: {
                type: "memo",
                content: "1111111"
            }
        }).then(result => {
            let feeAsset = new Asset(result.fee);
            feeAsset.minus(feeTransfer);
            tableData[1].price = feeAsset.getAmount({real: true});
            this.setState({tableData});
        });
    };
    getTransferBuy = () => {
        const {account, asset}= this.props;
        checkFeeStatusAsync({
            accountID: account.get("id"),
            feeID: asset.get("id"),
            type: "limit_order_create"

        }).then(result => {
            this.setPriceTableData(result.fee, 2);
        });
    };
    getTransferCancel = () => {
        const {account, asset}= this.props;
        checkFeeStatusAsync({
            accountID: account.get("id"),
            feeID: asset.get("id"),
            type: "limit_order_cancel"

        }).then(result => {
            this.setPriceTableData(result.fee, 3);
        });
    };
    getTransferStelth = () => {
        const {asset}= this.props;
        ConfidentialWallet.getFeeFromBlind(asset.get("id")).then(result=>{
            this.setPriceTableData(result, 4);
        });
        ConfidentialWallet.getFeeFromAccount(asset.get("id")).then(result=>{
            this.setPriceTableData(result, 5);
        });
    };

    setPriceTableData = (fee , id) => {
        const {tableData}= this.state;
        return new Promise(resolve=>{
            let feeAsset = new Asset(fee);
            tableData[id].price = feeAsset.getAmount({real: true});
            this.setState({tableData});
            resolve(feeAsset);
        });
    };

    render() {
        const {account, account_name, isMyAccount} = this.props;
        const {tableHead, tableData} = this.state;
        const keysTableHead = Object.keys(tableHead);

        // console.log("tableData", tableData)
        //console.log("account_name", account_name, account.toJS());

        return (
            <div className="account-portfolio-wrap">
                <div className="account-portfolio-title">
                    {account_name ? <><Translate content={"explorer.account.title"}/>: &nbsp; {account_name} </>: null}
                    {isMyAccount && <Translate className={"account-mine"} content={"account.mine"} />}

                </div>

                <div className={"account-networkfees"}>
                    <div className="account-networkfees-wrap">
                        {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?
                        <Translate content={"account.network_fees_gbl"} component={"div"} className="account-networkfees-title" />:
                        <Translate content={__SCROOGE_CHAIN__ ? "account.network_fees_scrooge" : "account.network_fees"} component={"div"} className="account-networkfees-title" />}
                        {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?
                        <Translate content={"account.network_fees_title_gbl"} component={"div"} className="account-networkfees-sub-title" />:
                        <Translate content={__SCROOGE_CHAIN__ ? "account.network_fees_title_scrooge" : "account.network_fees_title"} component={"div"} className="account-networkfees-sub-title" />}
                        <div className={"dx-table-nf"}>
                            <div className={"dx-table-nf-head"}>
                                <div className={"dx-table-nf-row"}>
                                    {keysTableHead.map(index => {
                                        return (
                                            <div className={"dx-table-nf-head-th left"} key={`th-${index}`}>
                                                <Translate content={tableHead[index]} />
                                            </div>
                                        );
                                    })}
                                </div>
                            </div>
                            <div className={"dx-table-nf-body"}>
                                {tableData.map((data, index) => {
                                    return (
                                        <BodyData
                                            key={"body-data-" + index}
                                            data={data}
                                        />
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                </div>

                <GateWayFees {...this.props} />

                {/*<GateWayAction />*/}
            </div>
        );
    }
}



class BodyData extends React.Component {
    renderColSpan(data) {
        const typeLength = data.type.length;
        let tr = [];
        for (let i = 0; i < typeLength; i++) {
            tr.push(
                <div className={"dx-table-nf-row"} key={"network-fee-line-" + i}>
                    {!i && (
                        <div className={"dx-table-nf-body-td"} >
                            <Translate content={data.operation} />
                        </div>
                    )}
                    <div className={"dx-table-nf-body-td"} >
                        <Translate content={data.type[i]} />
                    </div>
                    <div className={"dx-table-nf-body-td"}>{data.price[i]} {__GBLTN_CHAIN__ ? "GBLTEST" : (__GBL_CHAIN__ ? "GBL" : (__SCROOGE_CHAIN__ ? "SCROOGE" : "DEEX"))}</div>
                </div>
            );
        }
        return tr;
    }

    renderLine(data) {
        return (
            <div className={"dx-table-nf-row"} key={"network-fee-line"}>
                <div className={"dx-table-nf-body-td"} >
                    <Translate content={data.operation} />
                </div>
                <div className={"dx-table-nf-body-td"} >
                    <Translate content={data.type} />
                </div>
                <div className={""}>{data.price} {__GBLTN_CHAIN__ ? "GBLTEST" : (__GBL_CHAIN__ ? "GBL" : (__SCROOGE_CHAIN__ ? "SCROOGE" : "DEEX"))}</div>
            </div>
        );
    }

    render() {
        const {data} = this.props;
        if (typeof data.type === "object") {
            return this.renderColSpan(data);
        } else {
            return this.renderLine(data);
        }
    }
}

class FilterCoins extends React.Component {
    state = {
        showDropDown: false,
        filterText: ""
    };

    selectedItem = (asset) => {
        console.log("selectedItem asset", asset);
        this.setState({
            showDropDown: false,
            filterText: asset ? getAlias(asset.symbol) : ""
        });
        this.props.callback(asset);
    };
    render() {
        const {showDropDown, filterText} = this.state;
        return (
            <ClickOutside className={"account-gatewayfees-filter-coins"} onClickOutside={()=>this.setState({showDropDown: false})}>
                <div className={"account-gatewayfees-filter-coins-wrap"}>


                    <Translate
                        component="input" type="text" className="account-gatewayfees-filter-coins-input"
                        value={this.state.filterText}
                        onChange={(event)=>this.setState({filterText: event.target.value})}
                        onBlur={()=>this.setState({showDropDown1: false})}
                        onFocus={()=>this.setState({showDropDown: true})}
                        attributes={{ placeholder: "transfer.deposit.currency_placeholder" }}
                    />
                    {filterText
                        ? <Icon name={"menu-close"} onClick={()=>this.selectedItem(false)} />
                        :  <Icon name={"unfold_more"} />}
                </div>

                {showDropDown ? <FilterCoinsDropDown
                    {...this.props}
                    callback={this.selectedItem}
                    filterText={filterText.toLowerCase()} /> : null }
            </ClickOutside>
        );
    }
}

class FilterCoinsDropDown extends React.Component {
    /*state = {
        showDropDown: false
    };*/
    render() {
        const {backedCoins, callback, filterText} = this.props;
        let showBackedCoins = backedCoins;
        if( filterText ) {
            showBackedCoins = backedCoins.map(coins => {

                if( coins.symbol.toLowerCase().indexOf(filterText) !== -1 ||
                    getAlias(coins.symbol).toLowerCase().indexOf(filterText) !== -1) {
                    return coins;
                }
            }).filter(a=>a);
        }

        console.log("showBackedCoins", showBackedCoins);
        // console.log("showBackedCoins .sort()", showBackedCoins.sort((a, b) => a.name !== b.name ? 1 : -1 ) );
        // console.log("showBackedCoins .sort()", showBackedCoins.sort((a, b) => a.name !== b.name ? -1 : 1 ) );

        return (
            <div className={"account-gatewayfees-drop-down"}>
                <ul className={"account-gatewayfees-drop-down-list"}>
                    {showBackedCoins.map(coins => {
                        if( !coins.depositAllowed && !coins.withdrawalAllowed ) {
                            return  null;
                        }
                        return (
                            <li key={"gatewayfees-" + coins.name} onClick={()=>callback(coins)} className={"account-gatewayfees-drop-down-item"}>
                                <ImageLoad
                                    customStyle={{
                                        maxWidth: 25,
                                        marginRight: 15
                                    }}
                                    imageName={`${coins.name.toLowerCase()}.png`} />
                                {getAlias(coins.symbol)}
                            </li>
                        );
                    })}
                </ul>
            </div>
        );
    }
}

const ContentInfo = (props) => {
    const {backedCoin, asset} = props;
    let withdrawalStaticFee,
        depositStaticFee,
        depositPercentageFee,
        withdrawalPercentageFee;
    if(!backedCoin) return null;
    let aliasBackingCoin = getAlias(backedCoin.name),
        withdrawalAllowed = backedCoin.withdrawalAllowed,
        depositAllowed = backedCoin.depositAllowed;


    //console.log("socialLinks", socialLinks)

    const getAsset = (amount, precision) => {
        return new Asset({
            asset_id: asset.get("id"),
            precision: precision || asset.get("precision"),
            amount: amount || 0
        });
    };


    let nullAsset = getAsset(0);
    depositPercentageFee = Number(backedCoin.depositPercentageFee);
    depositStaticFee = getAsset(backedCoin.depositStaticFee);

    let depositMaxAmount = utils.format_number(backedCoin.depositMaxAmount / utils.get_asset_precision(backedCoin.precision), backedCoin.precision, false)
    let depositMinAmount = utils.format_number(backedCoin.depositMinAmount / utils.get_asset_precision(backedCoin.precision), backedCoin.precision, false)

    withdrawalStaticFee = getAsset(backedCoin.withdrawalStaticFee, backedCoin.precision);
    withdrawalPercentageFee = Number(backedCoin.withdrawalPercentageFee);
    let withdrawMaxAmount = utils.format_number(backedCoin.withdrawMaxAmount / utils.get_asset_precision(backedCoin.precision), backedCoin.precision, false)
    let withdrawMinAmount = utils.format_number(backedCoin.withdrawMinAmount / utils.get_asset_precision(backedCoin.precision), backedCoin.precision, false)


    return (
        <div className={cn("account-gatewayfees-info")}>
            <div className={"account-gatewayfees-info-wrapper"} >
                <div className={"account-gatewayfees-info-lines"} >
                    {depositAllowed ? <div className={"account-gatewayfees-info-line"}>
                        <Translate component={"span"} content={"gateway.info.deposit"}/>{" "}
                        {!depositPercentageFee ? depositStaticFee.getAmount({real: true}) + " " + aliasBackingCoin : depositPercentageFee + " %"}
                    </div>: null }

                    {depositAllowed ? <div className={"account-gatewayfees-info-line"}>
                        <span >
                            <Translate component={"span"} content={"gateway.info.depositMin"}/>{": "}
                            {depositMinAmount} {aliasBackingCoin}
                        </span>
                        {depositMaxAmount > 0 ?
                            <span >{" / "}
                                <Translate component={"span"} content={"gateway.info.depositMax"}/>{": "}
                                {depositMaxAmount} {aliasBackingCoin}
                            </span>
                            : null}
                    </div> : null}
                </div>
                <div className={"account-gatewayfees-info-lines"}>

                    {withdrawalAllowed  ? <div className={"account-gatewayfees-info-line"}>
                        <Translate component={"span"} content={"gateway.info.withdraw"}/>{" "}
                        {!withdrawalPercentageFee ? withdrawalStaticFee.getAmount({real: true}) + " "+ aliasBackingCoin : withdrawalPercentageFee + " %"}
                    </div>: null }

                    {withdrawalAllowed ? <div className={"account-gatewayfees-info-line"}>
                        <span>
                            <Translate component={"span"} content={"gateway.info.withdrawMin"}/>{": "}
                            {withdrawMinAmount} {aliasBackingCoin}
                        </span>
                        {withdrawMaxAmount > 0 ? <span>{" / "}
                            <Translate component={"span"} content={"gateway.info.withdrawMax"}/>{": "}
                            {withdrawMaxAmount} {aliasBackingCoin}
                        </span>: null }
                    </div>: null }
                </div>
            </div>

        </div>
    );
};

class GateWayFees extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            selectedAsset: ""
        };
    }
    setFiltered = (asset) => {
        this.setState({selectedAsset: asset ? asset.name : ""});
    };

    render() {
        const {selectedAsset} = this.state;
        const {backedCoins} = this.props;
        console.log("backedCoins", backedCoins);
        let tdexBackedCoins = backedCoins.get("TDEX").sort((a,b)=>{
            let aName = getAlias(a.symbol);
            let bName = getAlias(b.symbol);
            if(aName < bName) { return -1; }
            if(aName > bName) { return 1; }
            return 0;
        });
        let showBackedCoins = tdexBackedCoins;
        if( selectedAsset ) {
            showBackedCoins = tdexBackedCoins.map(coins => {

                if( coins.name === selectedAsset) {
                    return coins;
                }
            }).filter(a=>a);
        }
        return (
            <div className={"account-gatewayfees"}>
                <div className="account-gatewayfees-wrap">
                    <Translate content={"gateway.fees"} component={"div"} className="account-gatewayfees-title" />
                    {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?
                    <Translate content={"account.network_gate_title_gbl"} component={"div"} className="account-networkfees-sub-title" />:
                    <Translate content={__SCROOGE_CHAIN__ ? "account.network_gate_title_scrooge" : "account.network_gate_title"} component={"div"} className="account-networkfees-sub-title" />}
                    <div className={"account-gatewayfees-table"}>
                        <div className={"account-gatewayfees-table-head"}>
                            <FilterCoins backedCoins={tdexBackedCoins} callback={this.setFiltered}  />
                        </div>
                        <div className={"account-gatewayfees-table-body"}>
                            {showBackedCoins.map(coins => {
                                if( !coins.depositAllowed && !coins.withdrawalAllowed ) {
                                    return  null;
                                }
                                const symbolAlias = getAlias(coins.symbol);
                                return (
                                    <div key={"table-body-"+coins.symbol} className={"account-gatewayfees-table-item"}>
                                        <div className={"account-gatewayfees-table-item-name"}>
                                            <ImageLoad
                                                customStyle={{
                                                    maxWidth: 25,
                                                    marginRight: 5
                                                }}
                                                imageName={`${coins.name.toLowerCase()}.png`} />
                                            {symbolAlias}
                                        </div>


                                        <ContentInfo
                                            {...this.props}
                                            backedCoin={coins}  />
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

class GateWayAction extends React.Component {

    static defaultProps = {
        actions: [
            {
                link: "",
                type: "sale",
                label: {
                    name: "Скидка",
                    icon: ""
                },
                title: "Возврат комиссий за 6 месяцев",
                desc: "Всем участникам IEO/ICO"
            },{
                link: "",
                type: "action",
                label: {
                    name: "Акции",
                    icon: ""
                },
                title: "Возврат комиссий за 6 месяцев",
                desc: "Всем участникам IEO/ICO"
            },{
                link: "",
                type: "cashback",
                label: {
                    name: "Кэшбек",
                    icon: ""
                },
                title: "Возврат комиссий за 6 месяцев",
                desc: "Всем участникам IEO/ICO"
            }
        ]
    };


    render() {
        const {actions} = this.props;
        return (
            <div className={"account-gateway-action"}>
                <div className="account-gateway-action-wrap">
                    {actions.map(action=>{
                        return (
                            <div key={"gateway-action-"+ action.type} className={cn("account-gateway-action-item", action.type)}>
                                <div className={cn("account-gateway-action-label", action.type)}>
                                    {action.label.name}
                                    {action.label.icon ? <i className={action.label.icon} /> : null}
                                </div>
                                <div className={"account-gateway-action-title"}>
                                    {action.title}
                                </div>
                                <div className={"account-gateway-action-desc"}>
                                    {action.desc}
                                </div>
                            </div>
                        );
                    })}

                </div>
            </div>
        );
    }
}

const AccountNetworkFeesBind = BindToChainState(AccountNetworkFees, {keep_updating: false});

class AccountPageStoreWrapper extends React.Component {
    render() {
        let {account_name} = this.props;

        return <AccountNetworkFeesBind
            {...this.props}
            account={account_name || "v-putin"}
        />;
    }
}


export default connect(
    AccountPageStoreWrapper,
    {
        listenTo() {
            return [
                AccountStore,
                SettingsStore,
                WalletUnlockStore,
                ModalStore,
                GatewayStore
            ];
        },
        getProps() {
            let { passwordAccount } = AccountStore.getState();
            let backedCoins = GatewayStore.getState().backedCoins;
            // console.log("AccountStore.getState()", AccountStore.getState())
            return {
                account_name: passwordAccount,
                settings: SettingsStore.getState().settings,
                viewSsettings: SettingsStore.getState().viewSettings,
                backedCoins: Immutable.fromJS(backedCoins),
                bridgeCoins: GatewayStore.getState().bridgeCoins,
                gatewayDown: GatewayStore.getState().down
            };
        }
    }
);

import React from "react";
import AccountOrders from "../AccountOrders";
import TranslateWithLinks from "../../Utility/TranslateWithLinks";
import TotalBalanceValue from "../../Utility/TotalBalanceValue";
import Immutable from "immutable";

export default class AccountOrdersTab extends React.Component {
    render() {
        const {settings, core_asset, orders, isMyAccount} = this.props;
        const preferredUnit = settings.get("unit") || core_asset.get("symbol");
        const totalValueText = (
            <TranslateWithLinks
                noLink
                string="account.total"
                keys={[{type: "asset", value: preferredUnit, arg: "asset"}]}
            />
        );
        let ordersValue = (
            <TotalBalanceValue
                noTip
                balances={Immutable.List()}
                openOrders={orders}
                hide_asset
            />
        );
        return (
            <AccountOrders {...this.props}>
                <tr className="total-value">
                    <td colSpan="5" style={{textAlign: "right"}}>
                        {totalValueText}
                    </td>
                    <td colSpan="4" style={{textAlign: "left"}}>
                        {ordersValue}
                    </td>
                    {isMyAccount && <td />}
                </tr>
            </AccountOrders>
        );
    }
}

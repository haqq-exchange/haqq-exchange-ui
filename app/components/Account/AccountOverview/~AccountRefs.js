import React from "react";
import cname from "classnames";
import Translate from "react-translate-component";
import {Signature, ChainStore} from "deexjs";
import WalletDb from "stores/WalletDb";
import RefUtils from "../../../lib/common/ref_utils";
import "../AccountRefs/AccountRefs.scss";
import WalletUnlockActions from "actions/WalletUnlockActions";
import AccountActions from "actions/AccountActions";
import Immutable from "immutable";
import LoadingIndicator from "Components/LoadingIndicator";
import counterpart from "counterpart";
import { Checkbox, Pagination } from "antd";
import Popover from "react-popover";
import {debounce} from "lodash";

export default class AccountRefs extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            status: "loading",
            error: null,
            referrer: "",
            refId: "",
            refLink: "",
            referalsList: [],
            referralsCount: Immutable.Map(),
            levelAccount: Immutable.List(),
            userSendData: Immutable.Map(),
            currentReferral: null,
            isLoaded: false,
        };

        this.refutils = new RefUtils();
        this.searchAccount = debounce(this.searchAccount, 500);

        this.getRefInfo();
    }

    componentDidMount() {

    }

    workWithFirstReferrers = ({content, status}) => {
        let {referralsCount} = this.state;
        if (status === 200) {
            Object.values(content).map(item=>{
                referralsCount = referralsCount.set(item.name, Immutable.fromJS({
                    "referrals": item.referrals
                }));
            });
            this.setState({
                status: "first referrals",
                referalsList: content,
                isLoaded: false,
                referralsCount
            });
        }
    };

    workWithRefInfo = ({content, status}) => {
        if (status === 200) {
            const {refId, refLink, referrals, amounts} = content;
            const {account_name} = this.props;
            let {referralsCount} = this.state;
            if (refId) {
                referralsCount = referralsCount.set(account_name, Immutable.fromJS({
                    "referrals": referrals,
                }));
                this.setState({
                    status: "show user info",
                    code: status,
                    error: null,
                    currentReferral: account_name,
                    refId, refLink, amounts,
                    referralsCount
                }, this.getUserTree);
            } else {
                this.setStatusError({error: "Wrong voting account"}, status);
            }
        } else if( status === 418 ) {
            this.sendRegister().then(this.sendJoin);
        } else {
            this.setStatusError(content, status);
        }
    };

    getRefInfo = () => {
        const {account_name} = this.props;
        this.getUserSign({account: account_name}).then(sign => {
            this.refutils.postTemplate("/get_ref_info", {account: account_name}, sign)
                .then(this.workWithRefInfo);
        }).catch(this.catchUserSign);
    };


    getRequestData = (account_name, sendData) =>{
        let {userSendData} = this.state;
        if( !userSendData.has(account_name) ) {
            userSendData = userSendData.set(account_name, Immutable.fromJS({
                "page-size":"10",
                "page":"1",
                "sort_by":"referrals desc"
            }));
        }

        if( sendData ) {
            userSendData = userSendData.set(account_name,
                userSendData.get(account_name).mergeDeep(Immutable.fromJS(sendData)));
            this.setState({userSendData});
        }
        return userSendData.get(account_name).toJS();
    };


    getUserTree = ( root_account, sendData, isLevelAccount = true ) => {
        const {account_name} = this.props;
        let {levelAccount} = this.state;
        let sendBody = {
            "account": account_name,
            "root_account": root_account || account_name,
        };
        const requestData = this.getRequestData(root_account || account_name, sendData);
        sendBody = Object.assign({}, requestData, sendBody);

        if( root_account && isLevelAccount ) {
            levelAccount = levelAccount.push(root_account);
            this.setState({
                levelAccount,
                currentReferral: root_account,
                isLoaded: true
            });
        } else {
            this.setState({ currentReferral: account_name, isLoaded: true });
        }
        this.getUserSign(sendBody).then(sign => {
            this.refutils
                .postTemplate("/get_ref_tree", sendBody, sign)
                .then(this.workWithFirstReferrers)
                .catch(()=>{
                    this.setState({
                        isLoaded: false
                    });
                });
        }).catch(this.catchUserSign);
    };

    getUserSign = (body) => {
        return new Promise((resolve, reject) => {
            if (!WalletDb.isLocked()) {
                const chain_account = ChainStore.getAccount(this.props.account_name, false);
                const memo_from_public = chain_account.getIn(["options", "memo_key"]);
                const memo_from_privkey = WalletDb.getPrivateKey(memo_from_public);
                const bodyForSign = JSON.stringify(body);

                if (memo_from_privkey) {
                    let sign = Signature.signBuffer(bodyForSign, memo_from_privkey);
                    sign = sign.toBuffer().toString("base64");
                    return resolve(sign);
                }
            }
            return reject("");
        });
    };

    catchUserSign = (error) => {
        console.log("user should be logged in", error);
        this.setState({status: "user_not_auth"});
    };

    onBreadCrumbsClick = account => {
        let {levelAccount} = this.state;
        levelAccount = levelAccount.pop();
        this.setState({levelAccount, isLoaded: true}, () => {
            this.getUserTree(account,false , false);
        });
    };

    onInputChangeHandler = (e) => {
        this.setState({refId: e.target.value});
    };

    joinReferral = () => {
        const {error, code} = this.state;
        if( error.length && code === 418 ) {
            this.sendRegister()
                .then(this.sendJoin);
        } else {
            this.sendJoin();
        }
    };

    sendJoin = () => {
        const {account_name} = this.props;
        const sendBody = {
            account: account_name,
            root_account: ""
        };
        this.getUserSign(sendBody).then(sign => {
            this.refutils
                .postTemplate("/join", sendBody, sign)
                .then(this.workWithRegister);
        }).catch(this.catchUserSign);
    };

    sendRegister = () => {
        const {account_name} = this.props;
        const sendBody = {
            account: account_name,
            referrer: ""
        };
        return new Promise((resolve, reject) => {
            this.getUserSign(sendBody).then(sign => {
                this.refutils
                    .postTemplate("/register", sendBody, sign)
                    .then(resolve);
            }).catch(reject);
        });
    };

    workWithRegister = (data) => {
        const {status, content} = data;
        if (status === 200) {
            const {refId, refLink} = content;
            const {account_name} = this.props;
            let {referralsCount} = this.state;
            referralsCount = referralsCount.set(account_name, Immutable.fromJS({
                "referrals": 0,
            }));
            this.setState({status: "first referrals",
                code: status,
                refId, refLink,
                referralsCount,
                currentReferral: account_name,
            });
        } else {
            this.setStatusError(content, status);
        }
    };

    setStatusError = (content, code, status = "register") => {
        let statusError = Object.assign({}, {
            status,
            code
        }, content);
        this.setState(statusError);
    };

    toggleLock = (event) => {
        event.preventDefault();
        event.stopPropagation();
        if (WalletDb.isLocked()) {
            WalletUnlockActions.unlock().then(() => {
                AccountActions.tryToSetCurrentAccount();
                this.getRefInfo();
            });
        }
    };

    changeUserTree = (data) => {
        const {account_name} = this.props;
        this.getUserTree(account_name, data, false );
    };
    changePagination = (page, pageSize) => {
        this.changeUserTree({page, pageSize});
    };

    toggleFavorite = (item) => {
        const _this = this;
        const {account_name} = this.props;
        const sendBody = {
            account: account_name,
            referral: item.name,
            favorite: !item.favorite,
        };
        this.setState({isLoaded: true}, ()=>{
            this.getUserSign(sendBody).then(sign => {
                this.refutils.postTemplate("/update_referral_info", sendBody, sign)
                    .then(()=>{
                        _this.getUserTree(account_name, false , false);
                    });
            }).catch(this.catchUserSign);
        });
    };

    searchAccount=(name)=>{
        if( name.length >= 3 ) {
            this.getUserTree(name, false , false);
        }
    };

    withdraw = (data) => {
        const _this = this;
        const {account_name} = this.props;
        const sendBody = {
            account: account_name,
            withdraw: data,
        };

        this.setState({isLoaded: true}, ()=>{
            this.getUserSign(sendBody).then(sign => {
                console.log("sign", sign);
                this.refutils
                    .postTemplate("/initiate_withdrawal", sendBody, sign)
                    .then((res)=>{
                        console.log("res", res);
                        this.setState({isLoaded: false});
                    });

            });
        });
    };

    render() {
        const {status} = this.state;

        if (status === "user_not_auth") {
            return (<UserAuthLink toggleLock={this.toggleLock} />);
        } else if (status === "loading") {
            return (<LoadingIndicator loadingText={"Connecting to Referral Api"}/>);
        } else if (status === "show user info") {
            return (
                <div className={"referrals"}>
                    <div className={"referrals-block"}>
                        <div className="referrals-block-info">
                            <UserInfo {...this.state} />
                        </div>
                    </div>
                </div>
            );
        } else if (status === "first referrals") {
            return <FirstReferral
                onBreadCrumbsClick={this.onBreadCrumbsClick}
                getUserTree={this.getUserTree}
                changePagination={this.changePagination}
                changeUserTree={this.changeUserTree}
                toggleFavorite={this.toggleFavorite}
                withdraw={this.withdraw}
                searchAccount={this.searchAccount}
                {...this.state} {...this.props} />;
        }
        if (this.state.status === "register") {
            return (
                <RegisterReferral
                    onInputChangeHandler={this.onInputChangeHandler}
                    joinReferral={this.joinReferral}
                    {...this.state}
                />
            );

        }
        return (<h1>No state: {this.state.status}</h1>);
    }
}

const anonIcon = require("assets/anonymous-mask-icon.png");
const anonActiveIcon = require("assets/anonymous-mask-active-icon.png");
const anon = require("assets/anonymous-mask-bg.png");

class FirstReferral extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            isShowActive: false,
            account: null,
        };
    }

    toggleShowActive = (isShowActive, account) => {
        this.setState({isShowActive, account});
    };

    render(){
        const {refLink, refId, currentReferral, referralsCount, isLoaded,
            levelAccount, referalsList, changePagination, amounts} = this.props;
        const {isShowActive} = this.state;
        const totalReferral = referralsCount.get(currentReferral).get("referrals");
        console.log("this.props", this.props)
        return(
            <div className={"referrals"}>
                <div className={"referrals-block"}>
                    {isLoaded ? <LoadingIndicator loadingText={"Connecting to Referral Api"}/>: null}
                    <div className="referrals-block-info">
                        <UserInfo refLink={refLink} refId={refId}/>
                        <MyFunds {...this.props} />
                    </div>
                    <div className={"referrals__list"}>
                        <h2 className={"referrals__title"}>Referral leads</h2>
                        <div className="referrals__top">
                            {levelAccount.size > 0 ? <ReferralsBreadcrumbs accounts={levelAccount} onItemClick={this.props.onBreadCrumbsClick}/> : null}
                            <ReferralsSearch {...this.props}/>
                        </div>
                        {!isShowActive && <ReferralsList
                            referrals={referalsList}
                            levelAccount={levelAccount}
                            changeUserTree={this.changeUserTree}
                            toggleShowActive={this.toggleShowActive}
                            toggleFavorite={this.props.toggleFavorite}
                            getUserTree={this.props.getUserTree}/>}

                        <Pagination
                            defaultCurrent={1}
                            /*showSizeChanger*/
                            hideOnSinglePage={totalReferral<=10}
                            pageSizeOptions={["10","20","30","40","50"]}
                            onChange={changePagination}
                            total={totalReferral} />
                        {/*{isShowActive && account && <RecentTransactions
                            accountsList={Immutable.fromJS([account])}
                            compactView={false}
                            showMore={true}
                            fullHeight={true}
                            limit={15}
                            showFilters={true}
                            dashboard
                        />}*/}
                    </div>
                </div>
            </div>
        );
    }
}

class RegisterReferral extends React.Component  {

    _showJoinButton = () => {
        return(
            <div className={"referrals__field-error"}>
                <input
                    name="referer" placeholder="referral code"
                    value={this.props.refId} onChange={()=>this.props.onInputChangeHandler()}
                    type="hidden" />
                <button className={"btn btn-red"} onClick={()=>this.props.joinReferral()}>Join</button>
            </div>
        );
    };

    _showJoinError = () => {
        const {error, code} = this.props;
        let codeText = error;
        if( code === 420 ) {
            codeText = counterpart.translate(["accountref", error.split(" ").join("_").toLowerCase()].join("."));
        }
        return(
            <div className={"referrals__field"}>
                {codeText}
            </div>
        );
    };

    render() {
        const {error, code} = this.props;
        console.log([200, 418].indexOf(code) !== -1, code);
        return (
            <div className={"referrals"}>
                <div className={"referrals-block"}>
                    <div className={"referrals__info referrals__info--about"}>
                        <h2 className={"referrals__title"}> Referral program </h2>
                        <p className={"referrals__description"}>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint.</p>
                        <div className="referrals__tree-info">
                            <div className="referrals__tree-item active">
                                <span className="left">The owner</span>
                                <div className="anons">
                                    <img src={anonActiveIcon} alt="Logo" />
                                </div>
                                <span className="right">The three levels of referral program</span>
                            </div>
                            <div className="referrals__tree-item">
                                <span className="left">The 1st level referrals</span>
                                <div className="anons">
                                    <img src={anonIcon} alt="Logo" />
                                    <img src={anonIcon} alt="Logo" />
                                </div>
                                <span className="right">10%</span>
                            </div>
                            <div className="referrals__tree-item">
                                <span className="left">The 2nd level referrals</span>
                                <div className="anons">
                                    <img src={anonIcon} alt="Logo" />
                                    <img src={anonIcon} alt="Logo" />
                                    <img src={anonIcon} alt="Logo" />
                                </div>
                                <span className="right">5%</span>
                            </div>
                            <div className="referrals__tree-item">
                                <span className="left">The 3rd level referrals</span>
                                <div className="anons">
                                    <img src={anonIcon} alt="Logo" />
                                    <img src={anonIcon} alt="Logo" />
                                    <img src={anonIcon} alt="Logo" />
                                    <img src={anonIcon} alt="Logo" />
                                </div>
                                <span className="right">5%</span>
                            </div>
                        </div>
                        {error && [200, 418].indexOf(code) === -1
                            ? this._showJoinError()
                            : this._showJoinButton()}

                    </div>
                </div>
            </div>
        );
    }
}

const UserInfo = ({refLink, refId}) => {
    const windowRefLink = [window.location.origin, refLink].join("/");
    return (
        <div className={"referrals__info"}>
            <h2 className={"referrals__title"}> Referal programm </h2>
            <p className={"referrals__description"}>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint.</p>
            <ReferralField value={windowRefLink} title={"Referral link"}/>
            <ReferralField value={refId} title={"ID"}/>
        </div>
    );
};

class ReferralField extends React.Component {
    copyToClipboard = e => {
        this.input.select();
        document.execCommand("copy");
        e.target.focus();
    };

    render () {
        const {value, title} = this.props;
        return (
            <div>
                <span className={"referrals__description"}>{title}</span>
                <div className={"referrals__field"}>
                    <input type="text" value={value} readOnly={true} ref={(input) => this.input = input}/>
                    <button onClick={this.copyToClipboard} className="btn-copy"></button>
                </div>
            </div>
        );
    }
}

class ReferralsList extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            isPopoverOpen: {}
        };
    }
    togglePopover(index) {
        let { isPopoverOpen }= this.state;
        isPopoverOpen[index] = !isPopoverOpen[index];
        this.setState({isPopoverOpen});
    }

    closePopover(index) {
        let { isPopoverOpen }= this.state;
        isPopoverOpen[index] = false;
        this.setState({isPopoverOpen});
    }
    getIsOpenPopOver(item) {
        const { isPopoverOpen }= this.state;
        if( item.total_earned.length  ) {
            return isPopoverOpen[item.name];
        } else {
            return false;
        }
    }

    render () {
        const {referrals, levelAccount, getUserTree, toggleFavorite} = this.props;
        if (!referrals.length) {
            if( levelAccount.size ) {
                return (<LoadingIndicator loadingText={"Loading referrals"}/>);
            } else {
                return (
                    <div className="no-refs-block">
                        <img src={anon} alt="Logo" />
                        <h3> You don't have referrals yet</h3>
                    </div>);
            }
        }


        return(
            <React.Fragment>
                <div className={"referrals-table-header"}>
                    <span className={"referrals-table-header-star"} />
                    <span className={"referrals-table-header-name"}>Referral</span>
                    <span className={"referrals-table-header-count"}>Количество рефералов</span>
                    <span className={"referrals-table-header-total"}>Income {/*(Total: -)*/}</span>
                    <span className={"referrals-table-header-money"} />
                </div>
                <div>
                    {referrals.map((item, index) => {
                        const isLast = levelAccount.size === 2;
                        const isGetUserTree = item.referrals && !isLast;
                        const popover_body = (
                            <div className={"Popover-body-row"}>
                                {item.total_earned.map(earn=>{
                                    return (<div key={"Popover-body-item-"+earn.amount} className={"Popover-body-item"}>
                                        <span>{earn.currency}</span>
                                        <i style={{width: "100%"}} />
                                        <span>{earn.amount}</span>
                                    </div>);
                                })}
                            </div>
                        );
                        return (
                            <div key={"referrals-table-item-"+index} className={"referrals-table-item"} >
                                <span className={cname("referrals-table-item-star", {active: item.favorite})} onClick={()=>{ toggleFavorite(item);} } />
                                {/*<span className={"referrals-table-info"} onClick={()=>{ toggleShowActive(true, item.name);} }>i</span>*/}
                                <span className={"referrals-table-item-name"} onClick={()=>{ isGetUserTree ? getUserTree(item.name) : null;} }>
                                    {item.alias || item.name}
                                </span>
                                <span className={"referrals-table-item-count"}>{!isLast && item.referrals}</span>
                                <span className={"referrals-table-item-total"}>{!isLast && item.referrals}</span>
                                <Popover
                                    appendTarget={document.getElementById("content-wrapper")}
                                    className={"referrals-table-popover"}
                                    place={"below"}
                                    preferPlace={"below"}
                                    isOpen={this.getIsOpenPopOver(item)}
                                    onOuterAction={()=>this.closePopover(item.name)}
                                    body={popover_body} >
                                    <span className={cname("referrals-table-item-money", {active: item.total_earned.length  })}
                                        onClick={()=>this.togglePopover(item.name)}
                                    />
                                </Popover>
                            </div>
                        );
                    })}
                </div>
            </React.Fragment>
        );
    }
}

const ReferralsBreadcrumbs = ({accounts, onItemClick}) => (
    <div className={"referrals-breadcrumbs"}>
        <ul className={"breadcrumbs-list"}>
            <li className={"breadcrumbs-item first"}><span onClick={() => onItemClick()} className={"breadcrumbs-link"}>Leads </span></li>

            {accounts.map((account, index) => {
                const isLast = index === accounts.size -1;
                return (
                    <li key={"breadcrumbs-item-" + index} className={cname("breadcrumbs-item")}>
                        <span onClick={() => isLast ? null : onItemClick(account)} className={cname("breadcrumbs-link", {"last": isLast})}>
                            {account}
                        </span>
                    </li>
                );
            })}
        </ul>
    </div>
);

class UserAuthLink extends React.Component {
    render(){
        const {toggleLock} = this.props;
        return(
            <div className={"referrals"}>
                <div className={"referrals-block"}>
                    <button type={"button"} className={"btn btn-red btn-auth"} onClick={(event)=>toggleLock(event)}>
                        <Translate content={"header.unlock_short"}/>
                    </button>
                </div>
            </div>
        );
    }
}

class MyFunds extends React.Component {

    constructor(){
        super();
        this.state={
            allChecked: false,
            checkedAsset: Immutable.Map()
        };
    }

    allChecked=(data, isChecked)=>{
        let checkedAsset = {};
        data.forEach(item=>{
            if( isChecked ) {
                checkedAsset[item.currency] = item;
            }
        });
        this.setState({
            allChecked: isChecked,
            checkedAsset: Immutable.Map(Immutable.fromJS(checkedAsset))
        });
    };
    assetChecked=(item)=>{
        let { checkedAsset } = this.state;
        if( checkedAsset.has(item.currency)  ) {
            checkedAsset = checkedAsset.delete(item.currency);
        } else {
            checkedAsset = checkedAsset.set(item.currency, item);
        }
        this.setState({
            checkedAsset,
            allChecked: false
        });
    };


    render(){
        const { amounts }= this.props;
        const { checkedAsset, allChecked }= this.state;

        if( !amounts.length ) {
            return null;
        }

        return(
            <div className={"referrals__funds"}>
                <h2 className={"referrals__title"}>Мои средства</h2>
                <div className="referrals__funds-wrap">
                    <div className="referrals__funds-header">
                        <div className="referrals__funds-header-chbox">
                            <Checkbox prefixCls={"funds-checkbox"} checked={allChecked} onChange={(event)=>this.allChecked(amounts, event.target.checked)} />
                        </div>
                        <div className="referrals__funds-header-asset">
                            <span>Валюта</span>
                        </div>
                        <div className="referrals__funds-header-withdraw">
                            <span>К выводу</span>
                        </div>
                    </div>
                    <div className="referrals__funds-body">
                        {amounts.map(data=>{
                            return (
                                <div key={"referrals__funds-"+data.currency} className={"referrals__funds-row"}>
                                    <span className={"referrals__funds-row-chbox"}>
                                        <Checkbox prefixCls={"funds-checkbox"} checked={checkedAsset.has(data.currency)}  onChange={(event)=>this.assetChecked(data, event.target.checked)} />
                                    </span>
                                    <span className={"referrals__funds-row-asset"}>{data.currency}</span>
                                    <span className={"referrals__funds-row-withdraw"}>{data.amount}</span>
                                </div>
                            );
                        })}
                    </div>
                    <div className="referrals__funds-history">
                        <a href="">История выводов</a>
                    </div>
                    <div className="referrals__funds-button">
                        <button type={"button"} className={"btn btn-green"} disabled={!checkedAsset.size} onClick={()=>this.props.withdraw(checkedAsset.toArray())}>
                            Вывести
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

class ReferralsSearch extends React.Component {

    render(){
        return(
            <div className={"referrals-search"}>
                <div className={"referrals-search-wrap"}>
                    <input type="text" onChange={(event)=>this.props.searchAccount(event.target.value)} placeholder={"Search"}/>
                </div>
            </div>
        );
    }
}
import React from "react";
import AccountSelector from "./AccountSelector";
import Translate from "react-translate-component";
import Icon from "../Icon/Icon";
import {ChainStore} from "deexjs";
import ChainTypes from "../Utility/ChainTypes";
import FormattedAsset from "../Utility/FormattedAsset";
import BindToChainState from "../Utility/BindToChainState";
import LinkToAccountById from "../Utility/LinkToAccountById";
import cn from "classnames";
import PropTypes from "prop-types";
import ResponseOption from "../../config/response";
import MediaQuery from "react-responsive";

function getWitnessOrCommittee(type, acct) {
    let url = "",
        votes = 0,
        account;
    if (type === "witness") {
        account = ChainStore.getWitnessById(acct.get("id"));
    } else if (type === "committee") {
        account = ChainStore.getCommitteeMemberById(acct.get("id"));
    }

    url = account ? account.get("url") : url;
    votes = account ? account.get("total_votes") : votes;
    return {
        url,
        votes,
        id: account.get("id")
    };
}

class AccountItemRow extends React.Component {
    static propTypes = {
        account: PropTypes.object.isRequired,
        onAction: PropTypes.func.isRequired
    };

    shouldComponentUpdate(nextProps) {
        return (
            nextProps.account !== this.props.account ||
            nextProps.action !== this.props.action ||
            nextProps.isActive !== this.props.isActive ||
            nextProps.idx !== this.props.idx ||
            nextProps.proxy !== this.props.proxy
        );
    }

    onAction(item_id) {
        this.props.onAction(item_id);
    }

    render() {
        let {account, type, action, isActive} = this.props;
        let item_id = account.get("id");

        let {url, votes} = getWitnessOrCommittee(type, account);

        let link =
            url && url.length > 0 && url.indexOf("http") === -1
                ? "http://" + url
                : url;
        const isSupported = action === "remove";

        return (
            <MediaQuery {...ResponseOption.mobile}>
                {(matches) => {
                    if (matches) {
                        return <div className={cn("account-voting-item-row", {
                            supported: isSupported,
                            unsupported: !isSupported
                        })} >
                            <div className={"account-voting-item-row-left"}>
                                <div className={"account-voting-item-row-top"}>
                                    <span>{this.props.idx + 1}</span>
                                    <LinkToAccountById account={account.get("id")} />
                                </div>
                                <div className={"account-voting-item-row-bottom"}>
                                    <div>
                                        <Translate component={"b"} content="account.votes.votes" />
                                        <FormattedAsset
                                            amount={votes}
                                            asset="1.3.0"
                                            decimalOffset={5}
                                            hide_asset
                                        />
                                    </div>
                                    <div>
                                        <Translate component={"b"} content="account.votes.status.title" />
                                        <Translate content={`account.votes.${isActive ? "active_short" : "inactive"}`}/>
                                    </div>
                                    <div>
                                        <Translate component={"b"} content="account.votes.supported" />
                                        <Translate
                                            content={`settings.${isSupported ? "yes" : "no"}`}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className={"account-voting-item-row-right"}>
                                {link && link.indexOf(".") !== -1 ? (
                                    <a
                                        href={link}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                    >
                                        <Icon name="share" title="icons.share" />
                                    </a>
                                ) : null}

                                <span
                                    className={this.props.proxy ? "" : "clickable"}
                                    onClick={this.props.proxy ? () => {} : this.onAction.bind(this, item_id)} >
                                    {!this.props.proxy ? (
                                        <Icon
                                            name={
                                                isSupported
                                                    ? "checkmark-circle"
                                                    : "minus-circle"
                                            }
                                            title={
                                                isSupported
                                                    ? "icons.checkmark_circle.yes"
                                                    : "icons.minus_circle.no"
                                            }
                                        />
                                    ) : (
                                        <Icon name="locked" title="icons.locked.action" />
                                    )}
                                </span>
                            </div>
                        </div>;
                    } else {
                        return (
                            <tr className={isSupported ? "supported" : "unsupported"}>
                                <td >{this.props.idx + 1}</td>
                                <td >
                                    <LinkToAccountById account={account.get("id")} />
                                </td>
                                <td>
                                    {link && link.indexOf(".") !== -1 ? (
                                        <a
                                            href={link}
                                            target="_blank"
                                            rel="noopener noreferrer"
                                        >
                                            <Icon name="share" title="icons.share" />
                                        </a>
                                    ) : null}


                                </td>
                                <td>
                                    <FormattedAsset
                                        amount={votes}
                                        asset="1.3.0"
                                        decimalOffset={5}
                                        hide_asset
                                    />
                                </td>
                                <td>
                                    <Translate content={`account.votes.${isActive ? "active_short" : "inactive"}`}/>
                                </td>
                                <td>
                                    <Translate
                                        content={`settings.${isSupported ? "yes" : "no"}`}
                                    />
                                </td>
                                <td
                                    className={this.props.proxy ? "" : "clickable"}
                                    onClick={
                                        this.props.proxy
                                            ? () => {}
                                            : this.onAction.bind(this, item_id)
                                    }
                                >
                                    {!this.props.proxy ? (
                                        <Icon
                                            name={
                                                isSupported
                                                    ? "checkmark-circle"
                                                    : "minus-circle"
                                            }
                                            title={
                                                isSupported
                                                    ? "icons.checkmark_circle.yes"
                                                    : "icons.minus_circle.no"
                                            }
                                        />
                                    ) : (
                                        <Icon name="locked" title="icons.locked.action" />
                                    )}
                                </td>
                            </tr>
                        );
                    }
                }}
            </MediaQuery>
        );
    }
}

class VotingAccountsList extends React.Component {
    static propTypes = {
        items: ChainTypes.ChainObjectsList,
        onAddItem: PropTypes.func.isRequired,
        onRemoveItem: PropTypes.func.isRequired,
        validateAccount: PropTypes.func,
        label: PropTypes.string.isRequired, // a translation key for the label,
        placeholder: PropTypes.string, // the placeholder text to be displayed when there is no user_input
        tabIndex: PropTypes.number, // tabindex property to be passed to input tag
        action: PropTypes.string,
        withSelector: PropTypes.bool
    };

    static defaultProps = {
        action: "remove",
        withSelector: true,
        autosubscribe: false
    };

    constructor(props) {
        super(props);
        this.state = {
            selected_item: null,
            item_name_input: "",
            error: null
        };
        this.onItemChange = this.onItemChange.bind(this);
        this.onItemAccountChange = this.onItemAccountChange.bind(this);
        this.onAddItem = this.onAddItem.bind(this);
    }

    onItemChange(item_name_input) {
        this.setState({item_name_input});
    }

    onItemAccountChange(selected_item) {
        this.setState({selected_item, error: null});
        if (selected_item && this.props.validateAccount) {
            let res = this.props.validateAccount(selected_item);
            if (res === null) return;
            if (typeof res === "string") this.setState({error: res});
            else res.then(error => this.setState({error: error}));
        }
    }

    onAddItem(item) {
        if (!item) return;
        let next_state = {
            selected_item: null,
            item_name_input: "",
            error: null
        };
        this.setState(next_state);
        this.props.onAddItem(item.get("id"));
    }

    render() {
        if (!this.props.items) return null;

        let item_rows = this.props.items
            .filter(i => {
                if (!i) return false;
                //if (this.state.item_name_input) return i.get("name").indexOf(this.state.item_name_input) !== -1;
                return true;
            })
            .sort((a, b) => {
                let {votes: a_votes} = getWitnessOrCommittee(
                    this.props.type,
                    a
                );
                let {votes: b_votes} = getWitnessOrCommittee(
                    this.props.type,
                    b
                );
                if (a_votes !== b_votes) {
                    return parseInt(b_votes, 10) - parseInt(a_votes, 10);
                } else if (a.get("name") > b.get("name")) {
                    return 1;
                } else if (a.get("name") < b.get("name")) {
                    return -1;
                } else {
                    return 0;
                }
            })
            .map((i, idx) => {
                let action =
                    this.props.supported &&
                    this.props.supported.includes(i.get("id"))
                        ? "remove"
                        : "add";
                let isActive = this.props.active.includes(
                    getWitnessOrCommittee(this.props.type, i).id
                );
                return (
                    <AccountItemRow
                        idx={idx}
                        key={i.get("name")}
                        account={i}
                        type={this.props.type}
                        onAction={
                            action === "add"
                                ? this.props.onAddItem
                                : this.props.onRemoveItem
                        }
                        isSelected={this.props.items.indexOf(i) !== -1}
                        action={action}
                        isActive={isActive}
                        proxy={this.props.proxy}
                    />
                );
            });


        return (
            <div className={"account-voting-list-wrap"}>

                {this.props.title && item_rows.length ? (
                    <h4>{this.props.title}</h4>
                ) : null}
                {item_rows.length ? (
                    <MediaQuery {...ResponseOption.mobile}>
                        {(matches) => {
                            if (matches) {
                                return <TableModal item_rows={item_rows}/>;
                            } else {
                                return <TableDesktop item_rows={item_rows}/>;
                            }
                        }}
                    </MediaQuery>
                ) : null}

            </div>
        );
    }
}

const TableModal = (props) => {
    return (
        <>{props.item_rows}</>
    );
};
const TableDesktop = (props) => {
    return (
        <table className="account-voting-list">
            <thead>
                <tr>
                    <th>#</th>
                    <Translate component={"th"} content="account.votes.name" />
                    <Translate component={"th"} content="account.votes.about" />
                    <Translate component={"th"} content="account.votes.votes" />
                    <Translate component={"th"} content="account.votes.status.title" />
                    <Translate component={"th"} content="account.votes.supported" />
                    <Translate component={"th"} content="account.votes.toggle" />
                </tr>
            </thead>
            <tbody>{props.item_rows}</tbody>
        </table>
    );
};

export default BindToChainState(VotingAccountsList);

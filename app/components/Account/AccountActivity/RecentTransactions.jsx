import React from "react";
import Translate from "react-translate-component";
import {saveAs} from "file-saver";
import Operation from "components/Blockchain/Operation";
import ChainTypes from "components/Utility/ChainTypes";
import BindToChainState from "components/Utility/BindToChainState";
import utils from "common/utils";
import {ChainTypes as grapheneChainTypes} from "deexjs";
import TransitionWrapper from "components/Utility/TransitionWrapper";
import ps from "perfect-scrollbar";
import counterpart from "counterpart";
import Icon from "components/Icon/Icon";
import cnames from "classnames";
import PropTypes from "prop-types";

import {Apis} from "deexjs-ws";

const {operations} = grapheneChainTypes;
const alignLeft = {textAlign: "left"};

function compareOps(b, a) {
    if (a.block_num === b.block_num) {
        return a.virtual_op - b.virtual_op;
    } else {
        return a.block_num - b.block_num;
    }
}

function textContent(n) {
    return n ? `"${n.textContent.replace(/[\s\t\r\n]/gi, " ")}"` : "";
}

class RecentTransactions extends React.Component {
    static propTypes = {
        accountsList: ChainTypes.ChainAccountsList.isRequired,
        compactView: PropTypes.bool,
        limit: PropTypes.number,
        maxHeight: PropTypes.number,
        fullHeight: PropTypes.bool,
        showFilters: PropTypes.bool
    };

    static defaultProps = {
        limit: 25,
        maxHeight: 500,
        fullHeight: false,
        showFilters: false
    };

    constructor(props) {
        super();
        this.state = {
            limit: props.limit || 20,
            csvExport: false,
            headerHeight: 85,
            filter: "all"
        };
    }

    componentDidMount() {
        if (!this.props.fullHeight) {
            let t = this.refTransactions;
            ps.initialize(t);

            this._setHeaderHeight();
        }
        this.getFilters();
        this.getHistory();
    }

    _setHeaderHeight() {
        let height = this.refHeader.offsetHeight;

        if (height !== this.state.headerHeight) {
            this.setState({
                headerHeight: height
            });
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (
            !utils.are_equal_shallow(
                this.props.accountsList,
                nextProps.accountsList
            )
        )
            return true;
        if (this.props.maxHeight !== nextProps.maxHeight) return true;
        if (this.state.headerHeight !== nextState.headerHeight) return true;
        if (this.state.filter !== nextState.filter) return true;
        if (this.props.customFilter) {
            if (
                !utils.are_equal_shallow(
                    this.props.customFilter.fields,
                    nextProps.customFilter.fields
                ) ||
                !utils.are_equal_shallow(
                    this.props.customFilter.values,
                    nextProps.customFilter.values
                )
            ) {
                return true;
            }
        }

        if (this.props.maxHeight !== nextProps.maxHeight) return true;
        if (
            nextState.limit !== this.state.limit ||
            nextState.csvExport !== this.state.csvExport
        )
            return true;
        for (let key = 0; key < nextProps.accountsList.length; ++key) {
            let npa = nextProps.accountsList[key];
            let nsa = this.props.accountsList[key];
            if (npa && nsa && npa.get("history") !== nsa.get("history"))
                return true;
        }
        return false;
    }

    componentDidUpdate() {
        if (this.state.csvExport) {
            /* Bad practice */
            /*this.setState({
                csvExport: false
            });*/
            const csv_export_container = document.getElementById(
                "csv_export_container"
            );
            const nodes = csv_export_container.childNodes;
            let csv = "";
            for (const n of nodes) {
                //console.log("-- RecentTransactions._downloadCSV -->", n);
                const cn = n.childNodes;
                if (csv !== "") csv += "\n";
                csv += [
                    textContent(cn[0]),
                    textContent(cn[1]),
                    textContent(cn[2]),
                    textContent(cn[3])
                ].join(",");
            }
            var blob = new Blob([csv], {type: "text/csv;charset=utf-8"});
            var today = new Date();
            if(__SCROOGE_CHAIN__) {
            saveAs(
                blob,
                "scroogeist-" +
                today.getFullYear() +
                "-" +
                ("0" + (today.getMonth() + 1)).slice(-2) +
                "-" +
                ("0" + today.getDate()).slice(-2) +
                "-" +
                ("0" + today.getHours()).slice(-2) +
                ("0" + today.getMinutes()).slice(-2) +
                ".csv"
            )} else if(__GBL_CHAIN__ || __GBLTN_CHAIN__) {
            saveAs(
                blob,
                "gblhist-" +
                today.getFullYear() +
                "-" +
                ("0" + (today.getMonth() + 1)).slice(-2) +
                "-" +
                ("0" + today.getDate()).slice(-2) +
                "-" +
                ("0" + today.getHours()).slice(-2) +
                ("0" + today.getMinutes()).slice(-2) +
                ".csv"
            )} else {
                saveAs(
                    blob,
                    "btshist-" +
                    today.getFullYear() +
                    "-" +
                    ("0" + (today.getMonth() + 1)).slice(-2) +
                    "-" +
                    ("0" + today.getDate()).slice(-2) +
                    "-" +
                    ("0" + today.getHours()).slice(-2) +
                    ("0" + today.getMinutes()).slice(-2) +
                    ".csv"
                );
            }
        }

        if (!this.props.fullHeight) {
            let t = this.refTransactions;
            ps.update(t);

            this._setHeaderHeight();
        }
    }

    _onIncreaseLimit() {
        this.setState({
            limit: this.state.limit + 30
        });
    }

    getHistory() {
        const {account} = this.props;
        var db = Apis.instance().history_api();
        // console.log("this.props", this.props, account);
        /*ChainStore.fetchRecentHistory(account.get("id")).then(result => {
            console.log("account fetchRecentHistory", result.toJS());
        });*/
        let limit = 1000;
        let account_id = account.get("id");
        let start = "1.11.0";
        let most_recent = "1.11.0";
        var history = account.get("history");
        if (history && history.size) most_recent = history.last().get("id");

        console.log("account_id, most_recent, limit, start", account_id, most_recent, limit, most_recent);

        db
            .exec("get_account_history", [account_id, most_recent, limit, start])
            .then(result => {
                console.log("account fetchRecentHistory", result , "result.toJS()");
            });
        // fetchRecentHistory
    }

    _getHistory(accountsList, filterOp, customFilter) {
        let history = [];
        let seen_ops = new Set();
        for (let account of accountsList) {
            if (account) {
                let h = account.get("history");
                if (h)
                    history = history.concat(
                        h
                            .toJS()
                            .filter(
                                op =>
                                    !seen_ops.has(op.id) && seen_ops.add(op.id)
                            )
                    );
            }
        }
        if (filterOp) {
            history = history.filter(a => {
                return a.op[0] === operations[filterOp];
            });
        }

        if (customFilter) {
            history = history.filter(a => {
                let finalValue = customFilter.fields.reduce((final, filter) => {
                    switch (filter) {
                        case "asset_id":
                            return (
                                final &&
                                a.op[1]["amount"][filter] ===
                                customFilter.values[filter]
                            );
                        default:
                            return (
                                final &&
                                a.op[1][filter] === customFilter.values[filter]
                            );
                    }
                }, true);
                return finalValue;
            });
        }
        return history;
    }

    _downloadCSV() {
        this.setState({csvExport: true});
    }

    getFilters = () => {
        let {search} = window.location;
        if( search ) {
            let [name , value] = search.slice(1).split("=");
            if( name && name === "filter" ) {
                this.setState({
                    filter: value
                });
            }
        }

    };

    _onChangeFilter(value) {
        console.log("value", value);
        this.setState({
            filter: value
        });
    }

    render() {
        let {
            accountsList,
            compactView,
            filter,
            customFilter,
            style,
            maxHeight
        } = this.props;
        let {limit, headerHeight} = this.state;
        let current_account_id =
            accountsList.length === 1 && accountsList[0]
                ? accountsList[0].get("id")
                : null;
        let history = this._getHistory(
            accountsList,
            this.props.showFilters && this.state.filter !== "all"
                ? this.state.filter
                : filter,
            customFilter
        ).sort(compareOps);
        let historyCount = history.length;

        let options = null;
        if (this.props.showFilters) {
            options = [
                "all",
                "transfer",
                "limit_order_create",
                "limit_order_cancel",
                "fill_order",
                "account_create",
                "account_update",
                // "asset_create",
                //"witness_withdraw_pay",
                //"vesting_balance_withdraw"
            ].map(type => {
                return (
                    <option value={type} key={type}>
                        {counterpart.translate("transaction.trxTypes." + type)}
                    </option>
                );
            });
        }

        let display_history = history.length
            ? history.slice(0, limit).map(o => {
                //console.log("o", o);
                return (
                    <Operation
                        includeOperationId={true}
                        operationId={o.id}
                        style={alignLeft}
                        key={o.id}
                        op={o.op}
                        result={o.result}
                        block={o.block_num}
                        current={current_account_id}
                        hideFee
                        inverted={false}
                        hideOpLabel={compactView}
                        fullDate={true}
                        prefixCls={"dx-table-"}
                    />
                );
            })
            : [
                <div className={cnames("dx-table-body-row no_recent", {"compact": compactView})} key="no_recent">
                    <div className={"dx-table-body-td"}>
                        <Translate content="operation.no_recent"/>
                    </div>
                </div>
            ];
        display_history.push(
            <div className={"dx-table-row"} key="total_value">
                <div className={"dx-table-body-td"} />
                <div className={"dx-table-body-td right"}>

                </div>
                <div className={"dx-table-body-td"} >
                    &nbsp;
                    {(this.props.showMore && historyCount > limit) || (20 && limit < historyCount) ? (
                        <a onClick={this._onIncreaseLimit.bind(this)}>
                            <Icon
                                name="chevron-down"
                                title="icons.chevron_down.transactions"
                                className="icon-14px"
                            />
                        </a>
                    ) : null}
                </div>
            </div>
        );

        console.log("display_history", display_history);

        return (
            <div className="recent-transactions">
                <div className="recent-transactions-box">
                    {this.props.dashboard ? null : (
                        <div ref={(ref)=>this.refHeader=ref}>
                            <div className="block-content-header">
                                <span>
                                    {this.props.title ? (
                                        this.props.title
                                    ) : (
                                        <Translate content="account.recent"/>
                                    )}
                                </span>
                            </div>
                        </div>
                    )}
                    {this.props.showFilters ? (<div className="recent-transactions-select">
                        <select
                            data-place="left"
                            data-tip={counterpart.translate(
                                "tooltip.filter_ops"
                            )}
                            className=""
                            defaultValue={this.state.filter}
                            onChange={(event)=>this._onChangeFilter(event.target.value)}
                        >
                            {options}
                        </select>

                        {historyCount > 0 ? (
                            <Icon
                                onClick={()=>this._downloadCSV()}
                                data-tip={counterpart.translate(
                                    "transaction.csv_tip"
                                )}
                                data-place="bottom"
                                name="xls"
                                title="icons.excel"

                            />
                        ) : null}

                    </div>) : null}
                    <div
                        className="box-content grid-block no-margin"
                        style={
                            !this.props.fullHeight
                                ? {
                                    maxHeight: maxHeight - headerHeight
                                }
                                : null
                        }
                        ref={(ref)=>this.refTransactions=ref}
                    >
                        <div
                            className={cnames("dx-table", {
                                "compact": compactView,
                                "dx-table-dashboard": this.props.dashboard
                            })} >
                            <div className={"dx-table-head"}>
                                <div className={"dx-table-row"}>
                                    <div className={"dx-table-head-th left"}>
                                        <Translate content="account.transactions.id"/>
                                    </div>
                                    <div className={"dx-table-head-th left"}>
                                        <Translate content="account.transactions.type"/>
                                    </div>
                                    <div className={"dx-table-head-th left"}>
                                        <Translate content="account.transactions.info"/>
                                    </div>
                                    <div className={"dx-table-head-th left"}>
                                        <Translate content="account.transactions.time"/>
                                    </div>
                                </div>
                            </div>
                            <TransitionWrapper component="div" className={"dx-table-body"} transitionName="newrow">
                                {display_history}
                            </TransitionWrapper>
                        </div>
                    </div>
                    {historyCount > 0 &&
                    this.state.csvExport && (
                        <div
                            id="csv_export_container"
                            style={{display: "none"}}
                        >
                            <div>
                                <div>DATE</div>
                                <div>OPERATION</div>
                                <div>MEMO</div>
                                <div>AMOUNT</div>
                            </div>
                            {history.map(o => {
                                return (
                                    <Operation
                                        key={o.id}
                                        op={o.op}
                                        result={o.result}
                                        block={o.block_num}
                                        inverted={false}
                                        csvExportMode
                                    />
                                );
                            })}
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

RecentTransactions = BindToChainState(RecentTransactions);

class TransactionWrapper extends React.Component {
    static propTypes = {
        asset: ChainTypes.ChainAsset.isRequired,
        to: ChainTypes.ChainAccount.isRequired,
        fromAccount: ChainTypes.ChainAccount.isRequired
    };

    static defaultProps = {
        asset: "1.3.0"
    };

    render() {
        return (
            <span className="wrapper">{this.props.children(this.props)}</span>
        );
    }
}

TransactionWrapper = BindToChainState(TransactionWrapper);

export {RecentTransactions, TransactionWrapper};

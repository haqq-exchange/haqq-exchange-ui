import React from "react";
import Immutable from "immutable";
import {RecentTransactions} from "./RecentTransactions";
import BalanceWrapper from "../BalanceWrapper";
import AssetWrapper from "components/Utility/AssetWrapper";
import Translate from "react-translate-component";
import "./AccountActivity.scss";

class AccountActivity extends React.Component {
    static defaultProps = {};

    constructor(props) {
        super(props);
        this.state = {

        };
    }


    shouldComponentUpdate(nextProps) {
        return (
            nextProps.account !== this.props.account
        );
    }

    render() {
        let {account} = this.props;

        return (
            <div className="account-activity" ref={(ref)=>this.appTables=ref}>
                <Translate content={"account.history"} component={"div"} className="account-activity-title" />
                <div className="account-activity-wrap">
                    <RecentTransactions
                        accountsList={Immutable.fromJS([
                            account.get("id")
                        ])}
                        account={account}
                        compactView={false}
                        showMore={true}
                        fullHeight={true}
                        limit={15}
                        showFilters={true}
                        dashboard
                    />
                </div>
            </div>
        );
    }
}

AccountActivity = AssetWrapper(AccountActivity, {propNames: ["core_asset"]});

export default class AccountActivityWrapper extends React.Component {
    render() {
        return <BalanceWrapper {...this.props} wrap={AccountActivity} />;
    }
}

import React from "react";
import {Link} from "react-router-dom";
import Immutable from "immutable";
//import { ChainStore } from "deexjs";
import {FetchChain} from "deexjs";
import utils from "common/utils";
import { checkMarginStatus } from "common/accountHelper";
import BalanceWrapper from "./BalanceWrapper";
import AssetWrapper from "../Utility/AssetWrapper";
import AccountPortfolio from "./AccountOverview/AccountPortfolio";

import Icon from "Components/Icon/Icon";
import MobileAccountPortfolio from "./AccountOverview/MobileAccountPortfolio/MobileAccountPortfolio";
import {diff} from "deep-object-diff";
import Translate from "react-translate-component";

// import PubKey from "../Utility/PubKey";
import MediaQuery from "react-responsive";
import ResponseOption from "config/response";

// import ConfidentialWallet from "stores/ConfidentialWallet";
// import ModalActions from "../../actions/ModalActions";
import TotalBalanceValue from "components/Utility/TotalBalanceValue";

class AccountOverview extends React.Component {
    static defaultProps = {
        alwaysShowAssets: __GBL_CHAIN__ ? ["GBL"] : ["BTS", "DEEX"]
    };

    constructor(props) {
        super();
        this.state = {
            settleAsset: "1.3.0",
            shownAssets: props.viewSettings.get("shownAssets", "active"),
            depositAsset: null,
            withdrawAsset: null,
            bridgeAsset: null,
            accountBalances: Immutable.Map(),
            backedCoinsAssets: Immutable.Map(),
            assetBalancesList: Immutable.Map(),
            filters: {
                filterFavorite: props.viewSettings.get("filterFavorite", false),
                filterHidden: props.viewSettings.get("filterHidden", false),
                filterValue: null,
                hideNull: props.viewSettings.get("hideNull", false),
            }

        };
    }


    componentDidMount() {
        this._checkMarginStatus();
        this._getBalances();
    }

    componentDidUpdate(prevProps){
        // console.log("AccountOverview componentDidUpdate", prevProps)
        let diffHiddenAssets = diff(prevProps.hiddenAssets.toJS(), this.props.hiddenAssets.toJS());
        let diffBalancesAssets = diff(prevProps.balances, this.props.balances);
        let difffavoriteAssets = diff(prevProps.favoriteAssets.toJS(), this.props.favoriteAssets.toJS());
        // console.log("diffBalancesAssets", diffBalancesAssets );
        if( Object.keys(diffHiddenAssets).length
            || Object.keys(difffavoriteAssets).length
            || Object.keys(diffBalancesAssets).length
            || prevProps.account_name !== this.props.account_name
        ) {

            // console.log("difffavoriteAssets update" );
            this._getBalances();
        }
    }

    _checkMarginStatus(props = this.props) {
        // console.log("props.account", props.account);
        checkMarginStatus(props.account).then(status => {
            let globalMarginStatus = null;
            for (let asset in status) {
                globalMarginStatus =
                    status[asset].statusClass || globalMarginStatus;
            }
            this.setState({ globalMarginStatus });
        });
    }

    UNSAFE_componentWillReceiveProps(np) {
        if (np.account !== this.props.account) {
            this._checkMarginStatus(np);
            this.priceRefs  = {};
            this.valueRefs  = {};
            this.changeRefs = {};
            setTimeout(this.forceUpdate.bind(this), 500);
        }
    }

    shouldComponentUpdate(nextProps, nextState) {

        // console.log("AccountOverview.jsx shouldComponentUpdate nextProps", nextProps);
        // console.log("AccountOverview.jsx shouldComponentUpdate nextProps.balances", nextProps.balances);
        // console.log("AccountOverview.jsx shouldComponentUpdate this.props", this.props);
        // console.log("AccountOverview.jsx shouldComponentUpdate nextProps.wallet_locked", nextProps.wallet_locked, this.props.wallet_locked );

        return (
            !utils.are_equal_shallow(
                nextProps.backedCoins,
                this.props.backedCoins
            ) ||
            !utils.are_equal_shallow(nextProps.balances, this.props.balances) ||

            nextProps.private_contacts !== this.props.private_contacts ||
            nextProps.private_accounts !== this.props.private_accounts ||

            nextProps.account !== this.props.account ||
            nextProps.settings !== this.props.settings ||
            nextProps.hiddenAssets !== this.props.hiddenAssets ||
            nextProps.favoriteAssets !== this.props.favoriteAssets ||
            nextProps.wallet_locked !== this.props.wallet_locked ||
            nextProps.isMyAccount !== this.props.isMyAccount ||
            !utils.are_equal_shallow(nextState, this.state) ||
            this.state.filters !== nextState.filters
        );
    }

    onChangeVisibleAsset = (payload) => {
        const { filters } = this.state;
        // console.log("onChangeVisibleAsset", payload, Object.assign({}, filters, { [payload.name]: payload.value }))
        this.setState({
            filters: Object.assign({}, filters, { [payload.name]: payload.value })
        }, this._getBalances );
    };

    _getBalances = () => {
        let { account, backedCoins }         = this.props;
        let { backedCoinsAssets }         = this.state;
        let account_balances = account.get("balances");

        return new Promise(function(resolve) {
            if( backedCoins.has("TDEX") ) {
                let coinsName = backedCoins.get("TDEX").map(coins=>coins.symbol);
                FetchChain("getAsset", coinsName).then(assets => {
                    //window.console.log("assets", assets);
                    //backedCoinsTdex[assets.get("id")] = assets.get("dynamic_asset_data_id");
                    resolve(assets);
                });
            }
        }).then(results=>{
            results.map(item=>{
                //window.console.log("item", item);
                if( item ) {
                    if(!account_balances.has(item.get("id")) ) {
                        account_balances = account_balances.set(item.get("id"), item.get("dynamic_asset_data_id"));
                    }
                    backedCoinsAssets = backedCoinsAssets.set(item.get("dynamic_asset_data_id"), item);
                }
            });
            // console.log("AccountOverview.jsx backedCoinsAssets", backedCoinsAssets);
            // console.log("AccountOverview.jsx account_balances", account_balances);
            this.setState({
                accountBalances: account_balances,
                backedCoinsAssets
            }, this._getVisibleList);
        });
    };

    _getVisibleList = () => {
        let { account, account_name, hiddenAssets, favoriteAssets }         = this.props;
        let { filters, accountBalances, backedCoinsAssets } = this.state;
        let { filterValue, filterFavorite, filterHidden, hideNull } = filters;

        let account_balances = account.get("balances");
        let accountOrders = account.get("orders");
        let visibleBalancesList  = Immutable.List();
        let assetBalancesList  = Immutable.Map();
        let balances  = accountBalances || account_balances;

        FetchChain("getObject", balances.toArray()).then(balanceObject => {
            //console.log("balanceObject", balanceObject);
            // console.log("balances balanceObject", balanceObject.toArray());
            let asset_type = balanceObject
                .map(object=>{
                    let objectId = object.has("balance") ? object.get("asset_type") : object.get("id");
                    object = object.mergeDeep({balance_id: balances.get(objectId)});
                    if( backedCoinsAssets.has(objectId) ) {
                        object = object.mergeDeep(backedCoinsAssets.get(objectId));
                    }
                    assetBalancesList = assetBalancesList.set(objectId, object);
                    return object.get("asset_type");
                })
                .filter(a=>a).toArray();

            FetchChain("getObject", accountOrders.toArray()).then(ordersObject => {
                FetchChain("getAsset", asset_type).then(assets => {
                    assets.map(asset=> {
                        let assetId = asset.get("asset_type") || asset.get("id");
                        asset = assetBalancesList.get(assetId).mergeDeep(asset);
                        assetBalancesList = assetBalancesList.set(assetId, asset);
                    });
                    return assets;
                }).then(()=>{
                    return assetBalancesList.map(item => {
                        if( hideNull ) {
                            let ordersFilter = ordersObject.filter(object => {
                                return object.getIn(["sell_price", "base", "asset_id"]) === item.get("id");
                            });
                            if( item.has("balance") && item.get("balance") || ordersFilter.size) {
                                return item;
                            }
                        } else {
                            return item;
                        }
                    }).filter(a=>a);
                }).then(result=>{
                    // console.log("result asset", result);
                    // console.log("filterHidden", filterHidden);
                    // console.log("filterFavorite >>>>", filterFavorite);

                    result.forEach((asset) => {

                        let assetId = asset.has("balance") ? asset.get("asset_type") : asset.get("id");

                        if (filterValue && asset) {
                            let filter             = filterValue ? String(filterValue).toLowerCase() : "";
                            let assetName          = asset.get("symbol").toLowerCase();

                            if( assetName.indexOf(filter) === -1 ) {
                                return null;
                            }
                        }
                        if (filterFavorite) {
                            //console.log("filterFavorite", assetId , " => " ,favoriteAssets.hasIn([account_name, assetId]))
                            if (favoriteAssets.hasIn([account_name, assetId])) {
                                if (filterHidden) {
                                    if (hiddenAssets.hasIn([account_name,assetId])) {
                                        visibleBalancesList = visibleBalancesList.push(asset);
                                    }
                                } else if (!hiddenAssets.hasIn([account_name, assetId])) {
                                    visibleBalancesList = visibleBalancesList.push(asset);
                                }
                            }
                        } else if (filterHidden) {
                            //console.log("AccountPortfolio hiddenAssets", hiddenAssets, hiddenAssets.getIn(account_name), assetId)
                            if (hiddenAssets.hasIn([account_name, assetId])) {
                                visibleBalancesList = visibleBalancesList.push(asset);
                            }
                        } else {
                            if (!hiddenAssets.hasIn([account_name,assetId])) {
                                visibleBalancesList = visibleBalancesList.push(asset);
                            }
                        }
                    });

                    //console.log("visibleBalancesList", visibleBalancesList.toJS())

                   this.setState({ assetBalancesList: visibleBalancesList });
                });
            });
            // console.clear()
            // console.log("accountOrders", accountOrders.toJS());
            // let o = ChainStore.getObject(accountOrders.toJS());
            // console.log("accountOrders o", o.toJS());
            // console.log("hideNull", hideNull);
            // console.log("filterFavorite", filterFavorite);
            // console.log("filterHidden", filterHidden);

            /*balanceObject.map(object=> {
                let asset = {};
                if (object.has("balance")) {
                    asset = ChainStore.getAsset(object.get("asset_type"));
                } else {
                    asset = backedCoinsAssets.get(object.get("id"));
                }

                console.log("asset", asset, object);
            });*/

            // console.log("asset_type", asset_type);
            // console.log("backedAssets", backedAssets.toJS());
            // console.log("balanceObject", balanceObject);
            // console.log("balanceObject 2.5.2627822", assetBalancesList);
        });

    };



    render() {
        let { account, account_name, isMyAccount } = this.props;
        let { filters, assetBalancesList } = this.state;

        //console.log("this.props >>>>>", this.props);

        if (!account) {
            return null;
        }
        //console.log("AccountOverview.jsx wallet_locked", this.props.wallet_locked);

        const portfolioProps = {
            filters: filters,
            assetBalancesList: assetBalancesList,
            visibleBalancesList: assetBalancesList.map(data=>data.get("balance_id")).filter(a=>a),
            onChangeVisibleAsset: this.onChangeVisibleAsset,
            ...this.props
        };

        return (
            <div className="account-portfolio-wrap">
                <div className="account-portfolio-title">
                    <Translate content={"explorer.account.title"}/>: &nbsp;
                    {account.get("name")} {isMyAccount && <Translate className={"account-mine"} content={"account.mine"} />}
                    {/* {__SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__ ? null : 
                    <MediaQuery {...ResponseOption.mobile}>
                        {(matches) => {
                            if (!matches) {
                                return (
                                    <Link style={{marginLeft: 20}} className={"btn btn-green"} to={"/stealth"}>
                                        Stealth <Icon name={"stealth2"} />
                                    </Link>
                                );
                            } else {
                                return null;
                            }
                        }}
                    </MediaQuery>}  */}
                    {__GBL_CHAIN__ || __GBLTN_CHAIN__ ? 
                    <div className={"account-portfolio-title-balance"} style={{display: "flex", flexDirection: "column"}}>
                        <TotalBalanceValue noTip noPrefix balances={assetBalancesList.map(data=>data.get("balance_id")).filter(a=>a)}/>
                        <Link to={`${account.get("name")}/create-asset`} className="btn btn-green" style={{margin: "14px 0"}}>
                            <Translate content={"header.create_asset"} />
                        </Link>
                    </div>:
                    <span className={"account-portfolio-title-balance"} >
                        <TotalBalanceValue noTip noPrefix balances={assetBalancesList.map(data=>data.get("balance_id")).filter(a=>a)}/>
                    </span>}
                </div>



                <MediaQuery {...ResponseOption.mobile}>
                    {(matches) => {
                        if (matches) {
                            return <MobileAccountPortfolio {...portfolioProps} />;
                        } else {
                            return <AccountPortfolio {...portfolioProps} />;
                        }
                    }}
                </MediaQuery>



            </div>
        );
    }
}

AccountOverview = AssetWrapper(AccountOverview, { propNames: ["core_asset"] });

export default class AccountOverviewWrapper extends React.Component {
    render() {
        return <BalanceWrapper {...this.props} wrap={AccountOverview}/>;
    }
}

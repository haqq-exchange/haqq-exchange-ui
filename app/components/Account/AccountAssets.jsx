import React from "react";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import {ChainStore, FetchChain} from "deexjs";
import Translate from "react-translate-component";
import {Modal} from "antd";
import AssetActions from "actions/AssetActions";
import AssetStore from "stores/AssetStore";
import AccountActions from "actions/AccountActions";
// import BaseModal from "../Modal/BaseModal";
import FormattedAsset from "../Utility/FormattedAsset";
import ZfApi from "react-foundation-apps/src/utils/foundation-api";
import notify from "actions/NotificationActions";
import utils from "common/utils";
import {debounce} from "lodash-es";
import LoadingIndicator from "../LoadingIndicator";
import IssueModal from "../Modal/IssueModal";
import ReserveAssetModal from "../Modal/ReserveAssetModal";
import {connect} from "alt-react";
import assetUtils from "common/asset_utils";
import {Map, List} from "immutable";
import AssetWrapper from "../Utility/AssetWrapper";
import {Tabs, Tab} from "../Utility/Tabs";
import {getUniqueArray} from "../../utils";

class AccountAssets extends React.Component {
    static defaultProps = {
        symbol: "",
        name: "",
        description: "",
        max_supply: 0,
        precision: 0
    };

    static propTypes = {
        symbol: PropTypes.string.isRequired,
        assets: PropTypes.object,
        // getDynamicObject: PropTypes.func,
        // assetsList: PropTypes.object,
        assetsList: PropTypes.array,
    };

    constructor(props) {
        super(props);

        this.state = {
            create: {
                symbol: "",
                name: "",
                description: "",
                max_supply: 1000000000000000,
                precision: 4
            },
            issue: {
                amount: 0,
                to: "",
                to_id: "",
                asset_id: "",
                symbol: ""
            },
            errors: {
                symbol: null
            },
            isValid: false,
            searchTerm: "",
            modals: {

            },
            assetsBalanceId: []
        };

        this._searchAccounts = debounce(this._searchAccounts, 150);
    }

    _checkAssets(assets, force) {
        if (this.props.account.get("assets").size) return;
        let lastAsset = assets
            .sort((a, b) => {
                if (a.symbol > b.symbol) {
                    return 1;
                } else if (a.symbol < b.symbol) {
                    return -1;
                } else {
                    return 0;
                }
            })
            .last();

        if (assets.size === 0 || force) {
            AssetActions.getAssetList.defer("A", 100);
            this.setState({assetsFetched: 100});
        } else if (assets.size >= this.state.assetsFetched) {
            AssetActions.getAssetList.defer(lastAsset.symbol, 100);
            this.setState({assetsFetched: this.state.assetsFetched + 99});
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        this._checkAssets(nextProps.assets);
    }

    componentDidMount() {
        this._checkAssets(this.props.assets, true);
         this.getBalance();
    }

    _onIssueInput(value, e) {
        let key = e.target.id;
        let {issue} = this.state;

        if (key === "to") {
            this._searchAccounts(e.target.value);
            issue.to = e.target.value;
            let account = this.props.searchAccounts.findEntry(name => {
                return name === e.target.value;
            });

            issue.to_id = account ? account[0] : null;
        } else {
            issue[value] = e.target.value;
        }

        this.setState({issue: issue});
    }

    _searchAccounts(searchTerm) {
        AccountActions.accountSearch(searchTerm);
    }

    _issueAsset(account_id, e) {
        e.preventDefault();
        ZfApi.publish("issue_asset", "close");
        let {issue} = this.state;
        let asset = this.props.assets.get(issue.asset_id);
        issue.amount *= utils.get_asset_precision(asset.precision);
        AssetActions.issueAsset(account_id, issue).then(result => {
            if (result) {
                notify.addNotification({
                    message: `Successfully issued ${utils.format_asset(
                        issue.amount,
                        this.props.assets.get(issue.asset_id)
                    )}`, //: ${this.state.wallet_public_name}
                    level: "success",
                    autoDismiss: 10
                });

                // Update the data for the asset
                ChainStore.getAsset(issue.asset_id);
            } else {
                notify.addNotification({
                    message: "Failed to issue asset", //: ${this.state.wallet_public_name}
                    level: "error",
                    autoDismiss: 10
                });
            }
        });
    }

    _reserveButtonClick(assetId, id) {
        this.setState({reserve: assetId});
        //ZfApi.publish("reserve_asset", "open");
        this.toggleModals(id);
    }

    _reserveAsset(account_id, e) {
        e.preventDefault();
        ZfApi.publish("reserve_asset", "close");
        let {issue} = this.state;
        let asset = this.props.assets.get(issue.asset_id);
        issue.amount *= utils.get_asset_precision(asset.precision);
        AssetActions.issueAsset(account_id, issue);
    }

    _issueButtonClick(asset_id, symbol, id) {
        //e.preventDefault();
        //const {id} = this.issue_asset.props;
        let {issue} = this.state;
        issue = Object.assign(issue, {
            asset_id, symbol
        });
        this.toggleModals(id);
        this.setState({issue});
        //ZfApi.publish("issue_asset", "open");
    }

    toggleModals = id => {
        console.log("closeModals", this, id);
        let {modals} = this.state;

        modals = Object.assign(modals, {
            [id]: !modals[id]
        });
        this.setState({
            modals
        });
    };

    _editButtonClick(symbol, account_name, e) {
        e.preventDefault();
        this.props.router.push(
            `/account/${account_name}/update-asset/${symbol}`
        );
    }

    _onAccountSelect(account_name) {
        let {issue} = this.state;
        issue.to = account_name;
        issue.to_id = this.props.account_name_to_id[account_name];
        this.setState({issue: issue});
    }

    getBalance = () => {
        const {account} = this.props;
        let balanceArr = [];
       
        const balances = account.get("balances");
        FetchChain("getObject", balances.toArray())
            .then(balanceObject => {
                balanceObject.map(object=>{
                    let objectId = object.has("balance") ? object.get("asset_type") : object.get("id");
                    object = object.mergeDeep({balance_id: balances.get(objectId)});
                    let balanceId = object.get("balance_id")
                    let assetId =  object.get("asset_type")
                    let totalBalance = object.get("balance");
        
                    balanceArr.push({
                        balanceId,
                        assetId,
                        totalBalance: totalBalance
                    })

                    this.setState({assetsBalanceId: getUniqueArray(balanceArr)});
                })  
            })
    }

    getAssetOrder = (assetIssuer) => {
        let sellBase;
        let assetsOnSale = [];
        const issuer = ChainStore.getAccount(assetIssuer); // нахожу владельца токена
        let accountOrders= issuer.get("orders"); // нахожу его ордера
        accountOrders.toArray().map(order => { // иду по ордерам
            let o = ChainStore.getObject(order) // нахожу ордер в сторе
            sellBase =  o.getIn(["sell_price", "base", "asset_id"]) //нахожу id ассета который продают
            assetsOnSale.push(sellBase) 
        });

        return assetsOnSale;
    }
 
    render() {
        const {modals, assetsBalanceId} = this.state;
        let {account, account_name, assets, assetsList} = this.props;

        let accountExists = true;
        if (!account) {
            return <LoadingIndicator type="circle" />;
        } else if (account.notFound) {
            accountExists = false;
        }
        if (!accountExists) {
            return (
                <div className="grid-block">
                    <h5>
                        <Translate
                            component="h5"
                            content="account.errors.not_found"
                            name={account_name}
                        />
                    </h5>
                </div>
            );
        }

        if (assetsList.length) {
            assets = assets.clear();
            assetsList.forEach(a => {
                if (a) assets = assets.set(a.get("id"), a.toJS());
            });
        }

        let myAssets = assets
            .filter(asset => {
                return asset.issuer === account.get("id");
            })
            .sort((a, b) => {
                return (
                    parseInt(a.id.substring(4, a.id.length), 10) -
                    parseInt(b.id.substring(4, b.id.length), 10)
                );
            })
            .map(asset => {
                let description = assetUtils.parseDescription(
                    asset.options.description
                );
                let desc = description.short_name
                    ? description.short_name
                    : description.main;

                if (desc.length > 100) {
                    desc = desc.substr(0, 100) + "...";
                }

                const dynamicObject = this.props.getDynamicObject(
                    asset.dynamic_asset_data_id
                );

                const assetType = JSON.parse(asset.options.description).type; // тип ассета
                const assetCurrentSupply = dynamicObject?.get("current_supply"); // кол-во выпущенных ассетов

                const precision = asset?.precision;
                const currentSupply = Number(parseInt(assetCurrentSupply,10)) / Math.pow(10, precision);
                const maxSupply = Number(parseInt(asset.options.max_supply, 10)) / Math.pow(10, precision);

                // получаю баланс ассета
                let assetBalance;
                assetsBalanceId.filter(item => {
                    if(item.assetId === asset.id) {
                        assetBalance = item;
                    }
                })

                const assetsWhithOrders = this.getAssetOrder(asset.issuer);
                let assetWithOrder = false;
                assetsWhithOrders.filter(a => {
                    if(a == asset.id) {
                        assetWithOrder = true;
                    }
                })

                return (
                    <tr key={asset.symbol} style={{borderBottom: "1px solid rgb(142 145 148)"}}>
                        <td style={{paddingTop: "20px", paddingBottom: "20px", backgroundColor: "#1f1f1f"}}>
                            <Link to={`/asset/${asset.symbol}`}>
                                {asset.symbol}
                            </Link>
                        </td>
                        <td style={{maxWidth: "250px", paddingTop: "20px", paddingBottom: "20px", backgroundColor: "#1f1f1f"}}>{desc}</td>
                        <td style={{paddingTop: "20px", paddingBottom: "20px", backgroundColor: "#1f1f1f"}}>
                            {dynamicObject ? (
                                <div>{currentSupply}</div>
                             ) : <div>-</div>} 
                        </td>
                        <td style={{paddingTop: "20px", paddingBottom: "20px", backgroundColor: "#1f1f1f"}}>
                            {maxSupply}
                        </td>
                        {/* если тип ассета nft */}
                        {(assetType == "nft") ? 
                            <> 
                                {(assetCurrentSupply > 0) ? // если ассет выпущен
                                    <td style={{paddingTop: "20px", paddingBottom: "20px", backgroundColor: "#1f1f1f", maxWidth: "140px"}}>
                                        {assetWithOrder ? // если есть ордер, предлагаем посмотреть торни
                                            <Link to={`/market/${asset.symbol}_DEEX`} className="ant-btn btn btn-red ant-btn-dashed" style={{minWidth: "100px"}}>
                                                <Translate content="exchange.bidding" />
                                            </Link> :
                                            <>
                                                {(!assetWithOrder && assetBalance?.totalBalance) ? // выпущен и есть ордера, предлагаем продать
                                                <Link to={`/market/${asset.symbol}_DEEX`} className="ant-btn btn btn-green ant-btn-dashed" style={{minWidth: "100px"}}>
                                                    <Translate content="exchange.sell" />
                                                </Link> : <Translate content="markets.sold" />} 
                                            </>
                                        }
                                    </td> :
                                    <td style={{paddingTop: "20px", paddingBottom: "20px", backgroundColor: "#1f1f1f", maxWidth: "140px"}}>
                                        <Translate content="markets.issured_asset" />
                                    </td>
                                }
                            </>
                         : <td style={{paddingTop: "20px", paddingBottom: "20px", backgroundColor: "#1f1f1f", maxWidth: "140px"}} />}

                        {(assetCurrentSupply === asset.options.max_supply) ? 
                            <>
                                <td style={{paddingTop: "20px", paddingBottom: "20px", backgroundColor: "#1f1f1f"}}>
                                    <Translate content="transaction.trxTypes.asset_issued" />
                                </td>
                                <td style={{paddingTop: "20px", paddingBottom: "20px", backgroundColor: "#1f1f1f"}} />
                                <td style={{paddingTop: "20px", paddingBottom: "20px", backgroundColor: "#1f1f1f"}} />
                                </>
                         :
                            <>
                                <td style={{paddingTop: "20px", paddingBottom: "20px", backgroundColor: "#1f1f1f"}}>
                                    {!asset.bitasset_data_id ? (
                                        <button
                                            onClick={()=>this._issueButtonClick(asset.id, asset.symbol, "issue_asset")}
                                            className="ant-btn btn btn-green ant-btn-dashed"
                                        >
                                            <Translate content="transaction.trxTypes.asset_issue" />
                                        </button>
                                    ) : null}
                                </td>

                                <td style={{paddingTop: "20px", paddingBottom: "20px", backgroundColor: "#1f1f1f"}}>
                                    {!asset.bitasset_data_id ? (
                                        <button
                                            onClick={()=>this._reserveButtonClick(asset.id, "reserve_asset")}
                                            className="ant-btn btn btn-red ant-btn-dashed"
                                        >
                                            <Translate content="transaction.trxTypes.asset_reserve" />
                                        </button>
                                    ) : null}
                                </td>

                                <td style={{paddingTop: "20px", paddingBottom: "20px", backgroundColor: "#1f1f1f"}}>
                                    <Link to={`/account/${account_name}/update-asset/${asset.symbol}`} className="ant-btn btn btn-green ant-btn-dashed">
                                        <Translate content="transaction.trxTypes.asset_update" />
                                    </Link>
                                </td>
                            </>
                        }
                        
                    </tr>
                );
            })
            .toArray();

        let modalProps = {
            footer: null
        };

        return (
            <div className="grid-content app-tables no-padding" >
                <div className="content-block small-12">
                    <div className="tabs-container generic-bordered-box">
                        <Tabs
                            segmented={false}
                            setting="issuedAssetsTab"
                            className="account-tabs"
                            tabsClass="account-overview bordered-header content-block"
                            contentClass="padding"
                        >
                            <Tab title="account.user_issued_assets.issued_assets">
                                {assetsList.length > 0 ?
                                <div className="content-block">
                                    <table className="table dashboard-table table-hover 6">
                                        <thead>
                                            <tr>
                                                <th style={{backgroundColor: "#3f3f3f"}}>
                                                    <Translate content="account.user_issued_assets.symbol" />
                                                </th>
                                                <th style={{maxWidth: "200px", backgroundColor: "#3f3f3f"}}>
                                                    <Translate content="account.user_issued_assets.description" />
                                                </th>
                                                <th style={{backgroundColor: "#3f3f3f"}}>
                                                    <Translate content="markets.issured"
                                                    />
                                                </th>
                                                <th style={{backgroundColor: "#3f3f3f"}}>
                                                    <Translate content="account.user_issued_assets.max_supply" />
                                                </th>
                                                <th
                                                    style={{
                                                        textAlign: "center",
                                                        backgroundColor: "#3f3f3f"
                                                    }}
                                                    colSpan="3"
                                                >
                                                    <Translate content="account.perm.action" />
                                                </th>
                                                <th style={{backgroundColor: "#3f3f3f"}}>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>{myAssets}</tbody>
                                    </table>
                                </div>
                                : <Translate content="account.user_issued_assets.no_assets" component="div" style={{margin: "0 0 20px 0"}} />}

                                <div className="content-block">
                                    {this.props.wallet_locked ? 
                                    <Translate content="account.login_for_create_asset" /> :
                                    <Link
                                        to={`/account/${account_name}/create-asset/`}
                                    >
                                        <button className="ant-btn btn btn-green ant-btn-dashed">
                                            <Translate content="header.create_asset" />
                                        </button>
                                    </Link>}
                                </div>
                            </Tab>
                        </Tabs>
                    </div>

                    <Modal
                        getContainer={()=>document.getElementById("content-wrapper")}
                        id="issue_asset"
                        ref={ref=>this.issue_asset=ref}
                        onOk={()=>this.toggleModals("issue_asset")}
                        onCancel={()=>this.toggleModals("issue_asset")}

                        visible={modals.issue_asset} {...modalProps}>
                        <br />
                        <div className="grid-block vertical">
                            <IssueModal
                                asset_to_issue={this.state.issue.asset_id}
                                onClose={() => this.toggleModals("issue_asset")}
                            />
                        </div>
                    </Modal>

                    <Modal
                        getContainer={()=>document.getElementById("content-wrapper")}
                        id="reserve_asset"
                        onOk={()=>this.toggleModals("reserve_asset")}
                        onCancel={()=>this.toggleModals("reserve_asset")}
                        ref={ref=>this.reserve_asset=ref} visible={modals.reserve_asset}>
                        <br />
                        <div className="grid-block vertical">
                            <ReserveAssetModal
                                assetId={this.state.reserve}
                                account={account}
                                onClose={() => this.toggleModals("reserve_asset")}
                            />
                        </div>
                    </Modal>
                </div>
            </div>
        );
    }
}

const AccountAssetsWrapper = AssetWrapper(AccountAssets, {
    propNames: ["assetsList"],
    asList: true,
    withDynamic: true
});


export default connect(
    AccountAssetsWrapper,
    {
        listenTo() {
            return [AssetStore];
        },
        getProps(props) {
            let assets = Map(),
                assetsList = List();

            if (props.account.get("assets", []).size) {
                props.account.get("assets", []).forEach(id => {
                    assetsList = assetsList.push(id);
                });
            } else {
                assets = AssetStore.getState().assets;
            }
            console.log("assets, assetsList", assets, assetsList);
            return {assets, assetsList};
        }
    }
);

import React, {createRef} from "react";
import Translate from "react-translate-component";
import classnames from "classnames";
import AssetActions from "actions/AssetActions";
import HelpContent from "../Utility/HelpContent";
import utils from "common/utils";
import {ChainStore, ChainValidation, Signature} from "deexjs";
import FormattedAsset from "../Utility/FormattedAsset";
import counterpart from "counterpart";
import ChainTypes from "../Utility/ChainTypes";
import BindToChainState from "../Utility/BindToChainState";
import AssetSelector from "../Utility/AssetSelector";
import big from "bignumber.js";
import cnames from "classnames";
import assetUtils from "common/asset_utils";
import {Tabs, Tab} from "../Utility/Tabs";
import AmountSelector from "../Utility/AmountSelector";
import assetConstants from "chain/asset_constants";
import {estimateFee} from "common/trxHelper";
import PropTypes from "prop-types";
import WalletDb from "stores/WalletDb";

let GRAPHENE_MAX_SHARE_SUPPLY = new big(
    assetConstants.GRAPHENE_MAX_SHARE_SUPPLY
);

class BitAssetOptions extends React.Component {
    static propTypes = {
        backingAsset: ChainTypes.ChainAsset.isRequired,
        isUpdate: PropTypes.bool
    };

    static defaultProps = {
        isUpdate: false
    };

    constructor(props) {
        super(props);
        this.state = {
            backingAsset: props.backingAsset.get("symbol"),
            error: null, 
        };
    }

    _onInputBackingAsset(asset) {
        this.setState({
            backingAsset: asset.toUpperCase(),
            error: null
        });
    }

    _onFoundBackingAsset(asset) {
        if (asset) {
            if (
                asset.get("id") === "1.3.0" ||
                (asset.get("bitasset_data_id") &&
                    !asset.getIn(["bitasset", "is_prediction_market"]))
            ) {
                if (
                    asset.get("precision") !==
                    parseInt(this.props.assetPrecision, 10)
                ) {
                    this.setState({
                        error: counterpart.translate(
                            "account.user_issued_assets.error_precision",
                            {asset: this.props.assetSymbol}
                        )
                    });
                } else {
                    this.props.onUpdate("short_backing_asset", asset.get("id"));
                }
            } else {
                this.setState({
                    error: counterpart.translate(
                        "account.user_issued_assets.error_invalid"
                    )
                });
            }
        }
    }

    render() {
        let {bitasset_opts} = this.props;
        let {error} = this.state;

        return (
            <div className="small-12 grid-content">
                <label>
                    <Translate content="account.user_issued_assets.feed_lifetime_sec" />
                    <input
                        type="number"
                        value={bitasset_opts.feed_lifetime_sec / 60}
                        onChange={this.props.onUpdate.bind(
                            this,
                            "feed_lifetime_sec"
                        )}
                    />
                </label>

                <label>
                    <Translate content="account.user_issued_assets.minimum_feeds" />
                    <input
                        type="number"
                        value={bitasset_opts.minimum_feeds}
                        onChange={this.props.onUpdate.bind(
                            this,
                            "minimum_feeds"
                        )}
                    />
                </label>

                <label>
                    <Translate content="account.user_issued_assets.force_settlement_delay_sec" />
                    <input
                        type="number"
                        value={bitasset_opts.force_settlement_delay_sec / 60}
                        onChange={this.props.onUpdate.bind(
                            this,
                            "force_settlement_delay_sec"
                        )}
                    />
                </label>

                <label>
                    <Translate content="account.user_issued_assets.force_settlement_offset_percent" />
                    <input
                        type="number"
                        value={
                            bitasset_opts.force_settlement_offset_percent /
                            assetConstants.GRAPHENE_1_PERCENT
                        }
                        onChange={this.props.onUpdate.bind(
                            this,
                            "force_settlement_offset_percent"
                        )}
                    />
                </label>

                <label>
                    <Translate content="account.user_issued_assets.maximum_force_settlement_volume" />
                    <input
                        type="number"
                        value={
                            bitasset_opts.maximum_force_settlement_volume /
                            assetConstants.GRAPHENE_1_PERCENT
                        }
                        onChange={this.props.onUpdate.bind(
                            this,
                            "maximum_force_settlement_volume"
                        )}
                    />
                </label>

                <div className="grid-block no-margin small-12">
                    <AssetSelector
                        label="account.user_issued_assets.backing"
                        onChange={this._onInputBackingAsset.bind(this)}
                        asset={this.state.backingAsset}
                        assetInput={this.state.backingAsset}
                        tabIndex={1}
                        style={{width: "100%", paddingRight: "10px"}}
                        onFound={this._onFoundBackingAsset.bind(this)}
                    />
                    {error ? (
                        <div className="content-block has-error">{error}</div>
                    ) : null}
                </div>
            </div>
        );
    }
}
BitAssetOptions = BindToChainState(BitAssetOptions);

class AccountAssetCreate extends React.Component {
    static propTypes = {
        core: ChainTypes.ChainAsset.isRequired,
        globalObject: ChainTypes.ChainObject.isRequired
    };

    static defaultProps = {
        globalObject: "2.0.0",
        core: "1.3.0"
    };

    constructor(props) {
        super(props);
        this.state = this.resetState(props);

        this.artistRef = createRef();
        this.narrativeRef = createRef();
        this.attestationRef = createRef();
        this.licenseRef = createRef();
        this.workTitleRef = createRef();
        this.mediaRef = createRef();
        this.mediaTypeRef = createRef();
    }

    resetState(props) {
        // let asset = props.asset.toJS();
        let isBitAsset = false;
        let precision = utils.get_asset_precision(4);
        let corePrecision = utils.get_asset_precision(
            props.core.get("precision")
        );

        let {flagBooleans, permissionBooleans} = this._getPermissions({
            isBitAsset
        });

        // let flags = assetUtils.getFlags(flagBooleans);
        // let permissions = assetUtils.getPermissions(permissionBooleans, isBitAsset);
        // console.log("all permissions:", permissionBooleans, permissions)

        let coreRateBaseAssetName = ChainStore.getAsset("1.3.0").get("symbol");

        return {
            update: {
                symbol: "",
                precision: 4,
                max_supply: 100000,
                max_market_fee: 0,
                market_fee_percent: 0,
                // description: {main: "", market: ""}
                description: {main: "", type: "uia"}
            },
            errors: {
                max_supply: null
            },
            nftSign: "",
            workType: "png",
            mediaWork: "",
            isValid: false,
            flagBooleans: flagBooleans,
            permissionBooleans: permissionBooleans,
            isBitAsset: isBitAsset,
            is_prediction_market: false,
            core_exchange_rate: {
                quote: {
                    asset_id: null,
                    amount: 1
                },
                base: {
                    asset_id: "1.3.0",
                    amount: 1
                }
            },
            bitasset_opts: {
                feed_lifetime_sec: 60 * 60 * 24,
                minimum_feeds: 7,
                force_settlement_delay_sec: 60 * 60 * 24,
                force_settlement_offset_percent:
                    1 * assetConstants.GRAPHENE_1_PERCENT,
                maximum_force_settlement_volume:
                    20 * assetConstants.GRAPHENE_1_PERCENT,
                short_backing_asset: "1.3.0"
            },
            marketInput: ""
        };
    }

    componentDidMount() {
        let account = ChainStore.getAccount(this.props.account_name);
        const key = account.get("options").get("memo_key");

        this.getUserSign({public_key: key})
            .then(sign => {
                console.log("sign", sign)
                this.setState({nftSign: sign});
            })
            .catch("sign error");
    }

    _getPermissions(state) {
        let flagBooleans = assetUtils.getFlagBooleans(0, state.isBitAsset);
        let permissionBooleans = assetUtils.getFlagBooleans(
            "all",
            state.isBitAsset
        );

        return {
            flagBooleans,
            permissionBooleans
        };
    }

    _createAsset(e) {
        e.preventDefault();
        let {
            update,
            flagBooleans,
            permissionBooleans,
            core_exchange_rate,
            isBitAsset,
            is_prediction_market,
            bitasset_opts
        } = this.state;

        let {account} = this.props;
       
        let flags = assetUtils.getFlags(flagBooleans, isBitAsset);
        let permissions = assetUtils.getPermissions(
            permissionBooleans,
            isBitAsset
        );

        if (this.state.marketInput !== update.description.market) {
            update.description.market = "";
        }

        if(this.state.update.precision === "") {
            update.precision = 0;
        }

        let description = JSON.stringify(update.description);
        console.log(
            account.get("id"),
            update,
            flags,
            permissions,
            core_exchange_rate,
            isBitAsset,
            is_prediction_market,
            bitasset_opts,
            description
        );
        AssetActions.createAsset(
            account.get("id"),
            update,
            flags,
            permissions,
            core_exchange_rate,
            isBitAsset,
            is_prediction_market,
            bitasset_opts,
            description
        ).then(result => {
            console.log(
                "... AssetActions.createAsset(account_id, update)",
                account.get("id"),
                update,
                flags,
                permissions,
                core_exchange_rate,
                isBitAsset,
                is_prediction_market,
                bitasset_opts,
                description
            );
        });
    }

    _hasChanged() {
        return !utils.are_equal_shallow(
            this.state,
            this.resetState(this.props)
        );
    }

    _reset(e) {
        e.preventDefault();

        this.setState(this.resetState(this.props));
    }

    _forcePositive(number) {
        return parseFloat(number) < 0 ? "0" : number;
    }

    _onUpdateDescription(value, e) {
        let {update} = this.state;
        let updateState = true;

        switch (value) {
            case "condition":
                if (e.target.value.length > 60) {
                    updateState = false;
                    return;
                }
                update.description[value] = e.target.value;
                break;

            case "short_name":
                if (e.target.value.length > 32) {
                    updateState = false;
                    return;
                }
                update.description[value] = e.target.value;
                break;

            case "market":
                update.description[value] = e;
                break;

            case "visible":
                update.description[value] = !update.description[value];
                break;

            default:
                update.description[value] = e.target.value;
                break;
        }

        if (updateState) {
            this.forceUpdate();
            this._validateEditFields(update);
        }
    }

    onChangeBitAssetOpts(value, e) {
        let {bitasset_opts} = this.state;

        switch (value) {
            case "force_settlement_offset_percent":
            case "maximum_force_settlement_volume":
                bitasset_opts[value] =
                    parseFloat(e.target.value) *
                    assetConstants.GRAPHENE_1_PERCENT;
                break;
            case "minimum_feeds":
                bitasset_opts[value] = parseInt(e.target.value, 10);
                break;
            case "feed_lifetime_sec":
            case "force_settlement_delay_sec":
                console.log(
                    e.target.value,
                    parseInt(parseFloat(e.target.value) * 60, 10)
                );
                bitasset_opts[value] = parseInt(
                    parseFloat(e.target.value) * 60,
                    10
                );
                break;

            case "short_backing_asset":
                bitasset_opts[value] = e;
                break;

            default:
                bitasset_opts[value] = e.target.value;
                break;
        }

        this.forceUpdate();
    }

    _onUpdateInput(value, e) {
        let {update, errors} = this.state;
        let updateState = true;
        let shouldRestoreCursor = false;
        let precision = utils.get_asset_precision(this.state.update.precision);
        const target = e.target;
        const caret = target.selectionStart;
        const inputValue = target.value;

        switch (value) {
            case "market_fee_percent":
                update[value] = this._forcePositive(target.value);
                break;

            case "max_market_fee":
                if (
                    new big(inputValue)
                        .times(precision)
                        .gt(GRAPHENE_MAX_SHARE_SUPPLY)
                ) {
                    errors.max_market_fee =
                        "The number you tried to enter is too large";
                    return this.setState({errors});
                }
                target.value = utils.limitByPrecision(
                    target.value,
                    this.state.update.precision
                );
                update[value] = target.value;
                break;

            case "precision":
                // Enforce positive number
                update[value] = this._forcePositive(target.value);
                break;

            case "max_supply":
                shouldRestoreCursor = true;

                const regexp_numeral = new RegExp(/[[:digit:]]/);

                // Ensure input is valid
                if (!regexp_numeral.test(target.value)) {
                    target.value = target.value.replace(/[^0-9.]/g, "");
                }

                // Catch initial decimal input
                if (target.value.charAt(0) == ".") {
                    target.value = "0.";
                }

                // Catch double decimal and remove if invalid
                if (
                    target.value.charAt(target.value.length) !=
                    target.value.search(".")
                ) {
                    target.value.substr(1);
                }

                target.value = utils.limitByPrecision(
                    target.value,
                    this.state.update.precision
                );
                update[value] = target.value;

                // if ((new big(target.value)).times(Math.pow(10, precision).gt(GRAPHENE_MAX_SHARE_SUPPLY)) {
                //     return this.setState({
                //         update,
                //         errors: {max_supply: "The number you tried to enter is too large"
                //     }});
                // }
                break;

            case "symbol":
                shouldRestoreCursor = true;
                // Enforce uppercase
                const symbol = target.value.toUpperCase();
                // Enforce characters
                let regexp = new RegExp("^[.A-Z]+$");
                if (symbol !== "" && !regexp.test(symbol)) {
                    break;
                }
                ChainStore.getAsset(symbol);
                update[value] = this._forcePositive(symbol);
                break;

            default:
                update[value] = target.value;
                break;
        }

        if (updateState) {
            this.setState({update: update}, () => {
                if (shouldRestoreCursor) {
                    const selectionStart =
                        caret - (inputValue.length - update[value].length);
                    target.setSelectionRange(selectionStart, selectionStart);
                }
            });
            this._validateEditFields(update);
        }
    }

    _validateEditFields(new_state) {
        let errors = {
            max_supply: null
        };

        errors.symbol = ChainValidation.is_valid_symbol_error(new_state.symbol);
        let existingAsset = ChainStore.getAsset(new_state.symbol);
        if (existingAsset) {
            errors.symbol = counterpart.translate(
                "account.user_issued_assets.exists"
            );
        }

        try {
            errors.max_supply =
                new_state.max_supply <= 0
                    ? counterpart.translate(
                          "account.user_issued_assets.max_positive"
                      )
                    : new big(new_state.max_supply)
                          .times(Math.pow(10, new_state.precision))
                          .gt(GRAPHENE_MAX_SHARE_SUPPLY)
                        ? counterpart.translate(
                              "account.user_issued_assets.too_large"
                          )
                        : null;
        } catch (err) {
            console.log("err:", err);
            errors.max_supply = counterpart.translate(
                "account.user_issued_assets.too_large"
            );
        }

        let validType = true;
        if (this.state.update.description.type === "nft") {
            if (
                this.mediaRef.current === null ||
                this.mediaRef.current.value < 1
            ) {
                validType = false;
            }
        } else {
            validType = true;
        }

        let isValid =
            !errors.symbol &&
            !errors.max_supply &&
            // !errors.invalid_bitasset &&
            validType;

        this.setState({isValid: isValid, errors: errors});
    }

    _onFlagChange(key) {
        let booleans = this.state.flagBooleans;
        booleans[key] = !booleans[key];
        this.setState({
            flagBooleans: booleans
        });
    }

    _onPermissionChange(key) {
        let booleans = this.state.permissionBooleans;
        booleans[key] = !booleans[key];
        this.setState({
            permissionBooleans: booleans
        });
    }

    _onInputCoreAsset(type, asset) {
        if (type === "quote") {
            this.setState({
                quoteAssetInput: asset
            });
        } else if (type === "base") {
            this.setState({
                baseAssetInput: asset
            });
        }
    }

    _onFoundCoreAsset(type, asset) {
        if (asset) {
            let core_rate = this.state.core_exchange_rate;
            core_rate[type].asset_id = asset.get("id");

            this.setState({
                core_exchange_rate: core_rate
            });

            this._validateEditFields({
                max_supply: this.state.max_supply,
                core_exchange_rate: core_rate
            });
        }
    }

    _onInputMarket(asset) {
        this.setState({
            marketInput: asset
        });
    }

    _onFoundMarketAsset(asset) {
        if (asset) {
            this._onUpdateDescription("market", asset.get("symbol"));
        }
    }

    _onCoreRateChange(type, e) {
        let amount, asset;
        if (type === "quote") {
            amount = utils.limitByPrecision(
                e.target.value,
                this.state.update.precision
            );
            asset = null;
        } else {
            if (!e || !("amount" in e)) {
                return;
            }
            amount =
                e.amount == ""
                    ? "0"
                    : utils.limitByPrecision(
                          e.amount.toString().replace(/,/g, ""),
                          this.props.core.get("precision")
                      );
            asset = e.asset.get("id");
        }

        let {core_exchange_rate} = this.state;
        core_exchange_rate[type] = {
            amount: amount,
            asset_id: asset
        };
        this.forceUpdate();
    }

    _onToggleBitAsset() {
        let {update} = this.state;
        this.state.isBitAsset = !this.state.isBitAsset;
        if (!this.state.isBitAsset) {
            this.state.is_prediction_market = false;
        }

        let {flagBooleans, permissionBooleans} = this._getPermissions(
            this.state
        );
        this.state.flagBooleans = flagBooleans;
        this.state.permissionBooleans = permissionBooleans;

        this.forceUpdate();
    }

    _onTogglePM() {
        this.state.is_prediction_market = !this.state.is_prediction_market;
        this.state.update.precision = this.props.core.get("precision");
        this.state.core_exchange_rate.base.asset_id = this.props.core.get("id");
        this.forceUpdate();
    }

    handleAssetTypeChange = event => {
        const {value} = event.target;
        const {nftSign} = this.state;
        let account = ChainStore.getAccount(this.props.account_name);

        if (value === "nft") {
            this.setState(prevState => ({
                ...prevState,
                update: {
                    ...prevState.update,
                    description: {
                        ...prevState.update.description,
                        type: value,
                        nft_object: {
                            encoding: "base64",
                            sig_pubkey_or_address: account
                                .get("options")
                                .get("memo_key"),
                            type: "NFT/ART/VISUAL"
                        },
                        nft_signature: nftSign
                    }
                }
            }));
        } else {
            this.setState(prevState => ({
                ...prevState,
                update: {
                    ...prevState.update,
                    description: {
                        ...prevState.update.description,
                        type: value,
                        nft_object: {
                            encoding: null,
                            sig_pubkey_or_address: account
                                .get("options")
                                .get("memo_key"),
                            type: null
                        },
                        nft_signature: null
                    }
                }
            }));
        }

        this._validateEditFields(this.state.update);
    };

    handleAssetImageChange = (event) => {
        const { value } = event.target;
   
        this.setState(
          (prevState) => ({
            ...prevState,
            update: {
              ...prevState.update,
              description: { ...prevState.update.description, image: value }
            }
          }),
          () => console.log(this.state)
        );

        this._validateEditFields(this.state.update);
    };

    handleNftObject(evt) {
        const {nftSign} = this.state;
        const artist = this.artistRef.current.value;
        const narrative = this.narrativeRef.current.value;
        const attestation = this.attestationRef.current.value;
        const license = this.licenseRef.current.value;
        const workTitle = this.workTitleRef.current.value;
        const media = btoa(this.mediaRef.current.value);
        let account = ChainStore.getAccount(this.props.account_name);
        let mediaType = this.mediaTypeRef.current.value;
        this.setState({workType: mediaType, mediaWork: this.mediaRef.current.value});

        let regexp = new RegExp("^[a-zA-Z0-9]+$");

        // if (media !== "" && !regexp.test(this.mediaRef.current.value)) {
        //     return;
        // } else 
        if (mediaType === "png") {
            this.setState(prevState => ({
                ...prevState,
                update: {
                    ...prevState.update,
                    description: {
                        ...prevState.update.description,
                        nft_object: {
                            artist: artist,
                            attestation: attestation,
                            encoding: "base64",
                            holder_license: license,
                            license: license,
                            media_png: media,
                            narrative: narrative,
                            sig_pubkey_or_address: account
                                .get("options")
                                .get("memo_key"),
                            title: workTitle,
                            type: "NFT/ART/VISUAL"
                        },
                        nft_signature: nftSign
                    }
                }
            }));
        } else if (mediaType === "jpeg") {
            this.setState(prevState => ({
                ...prevState,
                update: {
                    ...prevState.update,
                    description: {
                        ...prevState.update.description,
                        nft_object: {
                            artist: artist,
                            attestation: attestation,
                            encoding: "base64",
                            holder_license: license,
                            license: license,
                            media_jpeg: media,
                            narrative: narrative,
                            sig_pubkey_or_address: account
                                .get("options")
                                .get("memo_key"),
                            title: workTitle,
                            type: "NFT/ART/VISUAL"
                        },
                        nft_signature: nftSign
                    }
                }
            }));
        } else if (mediaType === "gif") {
            this.setState(prevState => ({
                ...prevState,
                update: {
                    ...prevState.update,
                    description: {
                        ...prevState.update.description,
                        nft_object: {
                            artist: artist,
                            attestation: attestation,
                            encoding: "base64",
                            holder_license: license,
                            license: license,
                            media_gif: media,
                            narrative: narrative,
                            sig_pubkey_or_address: account
                                .get("options")
                                .get("memo_key"),
                            title: workTitle,
                            type: "NFT/ART/VISUAL"
                        },
                        nft_signature: nftSign
                    }
                }
            }));
        }

        this._validateEditFields(this.state.update);
    }

    // получаю подпись
    getUserSign = body => {
        return new Promise((resolve, reject) => {
            const chain_account = ChainStore.getAccount(
                this.props.account_name,
                false
            );
            const memo_from_public = chain_account.getIn([
                "options",
                "memo_key"
            ]);
            const memo_from_privkey = WalletDb.getPrivateKey(memo_from_public);
            const bodyForSign = JSON.stringify(body);
            if (memo_from_privkey) {
                let sign = Signature.signBuffer(bodyForSign, memo_from_privkey);
                sign = sign.toBuffer().toString("base64");
                console.log("sign", sign);
                return resolve(sign);
            }
            console.log("sign wasn't to create");
            return reject("");
        });
    };

    render() {
        let {globalObject, core} = this.props;
        let {
            errors,
            isValid,
            update,
            flagBooleans,
            permissionBooleans,
            core_exchange_rate,
            is_prediction_market,
            isBitAsset,
            bitasset_opts
        } = this.state;

        const mediaTypes = ["png", "gif", "jpeg"];

        const AssetTypes = [
            {
                label: "uia",
                value: "uia",
            },{
                label: "nft",
                value: "nft",
            }
        ];

        // Estimate the asset creation fee from the symbol character length
        let symbolLength = update.symbol.length,
            createFee = "N/A";

        if (symbolLength === 3) {
            createFee = (
                <FormattedAsset
                    amount={estimateFee(
                        "asset_create",
                        ["symbol3"],
                        globalObject
                    )}
                    asset={"1.3.0"}
                />
            );
        } else if (symbolLength === 4) {
            createFee = (
                <FormattedAsset
                    amount={estimateFee(
                        "asset_create",
                        ["symbol4"],
                        globalObject
                    )}
                    asset={"1.3.0"}
                />
            );
        } else if (symbolLength > 4) {
            createFee = (
                <FormattedAsset
                    amount={estimateFee(
                        "asset_create",
                        ["long_symbol"],
                        globalObject
                    )}
                    asset={"1.3.0"}
                />
            );
        }

        // Loop over flags
        let flags = [];
        let getFlag = (key, onClick, isChecked) => {
            return (
                <table key={"table_" + key} className="table">
                    <tbody>
                        <tr>
                            <td style={{border: "none", width: "80%"}}>
                                <Translate
                                    content={`account.user_issued_assets.${key}`}
                                />:
                            </td>
                            <td style={{border: "none"}}>
                                <div
                                    className="switch"
                                    style={{marginBottom: "10px"}}
                                    onClick={onClick}
                                >
                                    <input
                                        type="checkbox"
                                        checked={isChecked}
                                    />
                                    <label />
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            );
        };
        for (let key in permissionBooleans) {
            if (permissionBooleans[key] && key !== "charge_market_fee") {
                flags.push(
                    getFlag(
                        key,
                        this._onFlagChange.bind(this, key),
                        flagBooleans[key]
                    )
                );
            }
        }

        flags.push(
            getFlag(
                "visible",
                this._onUpdateDescription.bind(this, "visible"),
                update.description.visible
                    ? false
                    : update.description.visible === false ? true : false
            )
        );

        // Loop over permissions
        let permissions = [];
        for (let key in permissionBooleans) {
            permissions.push(
                <table key={"table_" + key} className="table">
                    <tbody>
                        <tr>
                            <td style={{border: "none", width: "80%"}}>
                                <Translate
                                    content={`account.user_issued_assets.${key}`}
                                />:
                            </td>
                            <td style={{border: "none"}}>
                                <div
                                    className="switch"
                                    style={{marginBottom: "10px"}}
                                    onClick={this._onPermissionChange.bind(
                                        this,
                                        key
                                    )}
                                >
                                    <input
                                        type="checkbox"
                                        checked={permissionBooleans[key]}
                                        onChange={() => {}}
                                    />
                                    <label />
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            );
        }

        const confirmButtons = (
            <div>
                <button
                    className={"ant-btn btn btn-green ant-btn-dashed"}
                    disabled = {!isValid}
                    onClick={this._createAsset.bind(this)}
                >
                    <Translate content={"header.create_asset"} />
                </button>
                <button
                    className="ant-btn btn btn-red ant-btn-dashed"
                    style={{margin: "20px"}}
                    onClick={this._reset.bind(this)}
                    value={counterpart.translate("account.perm.reset")}
                >
                    <Translate content="account.perm.reset" />
                </button>
            </div>
        );

        return (
            <div className="grid-content app-tables no-padding" ref="appTables" style={{background: "#2f2f2f", color: "#ffffff"}}>
                <div className="content-block small-12">
                    <div className="tabs-container generic-bordered-box" style={{background: "#2f2f2f", color: "#ffffff"}}>
                        <div className="tabs-header">
                            <h3>
                                <Translate content={"token.title"} />
                            </h3>
                        </div>

                        <Tabs
                            setting="createAssetTab"
                            className="account-tabs"
                            style={{background: "#2f2f2f"}}
                            tabsClass="account-overview no-padding bordered-header content-block"
                            style={{background: "#3f3f3f"}}
                            contentClass="grid-block shrink small-vertical medium-horizontal padding"
                            segmented={false}
                            actionButtons={confirmButtons}
                        >
                            <Tab title="account.user_issued_assets.primary">
                                <div className="small-12 grid-content" style={{background: "#2f2f2f", color: "#ffffff"}}> 

                                   {/* символ */}
                                    <label style={{color: "#ffffff"}}>
                                        <div className="required_field">
                                            <span className="required"> * </span>
                                            <Translate content={__GBL_CHAIN__ || __GBLTN_CHAIN__ ? "token.name_token" : "account.user_issued_assets.symbol"} />
                                        </div>
                                        <input
                                            type="text"
                                            value={update.symbol}
                                            onChange={this._onUpdateInput.bind(
                                                this,
                                                "symbol"
                                            )}
                                            style={{marginBottom: "0"}}
                                        />
                                    </label>
                                    {errors.symbol ? 
                                        <p style={{padding: "0"}} className="required">
                                            <Translate content="token.required_field" />
                                        </p>
                                    : null}

                                    {/* тип */}
                                    <Translate
                                        style={{color: "#ffffff"}}
                                        component="label"
                                        content="token.type"
                                    />
                                    <select 
                                        className="selector" 
                                        onChange={this.handleAssetTypeChange}
                                        value={update.description.type}
                                    >
                                        {AssetTypes.map((type) => (
                                            <option value={type.value}>{type.label}</option>
                                        ))}
                                    </select>

                                    {/* максимальное кол-во активов */}
                                    <label style={{color: "#ffffff"}}>
                                        <div className="required_field">
                                            <span className="required"> * </span>
                                            <Translate content="account.user_issued_assets.max_supply" />{" "}
                                            {update.symbol ? (
                                                <span>({update.symbol})</span>
                                            ) : null}
                                        </div>
                                        <input
                                            type="text"
                                            value={update.max_supply}
                                            onChange={this._onUpdateInput.bind(
                                                this,
                                                "max_supply"
                                            )}
                                        />
                                    </label>
                                    {errors.max_supply ? (
                                        <p style={{color: "#ffffff"}} className="grid-content has-error">
                                            {errors.max_supply}
                                        </p>
                                    ) : null}
                                    
                                    {/* ссылка на картинку */}
                                    {/* {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?
                                    <>
                                        {(update.description.type == "nft") ? 
                                        <>  
                                            <div className="required_field">
                                                <span className="required"> * </span>
                                                <Translate
                                                    style={{color: "#ffffff"}}
                                                    component="label"
                                                    content="token.image"
                                                />
                                            </div>
                                            <label style={{color: "#ffffff"}}>
                                                <input
                                                    type="text"
                                                    rows="1"
                                                    onChange={this.handleAssetImageChange}
                                                    required="required"
                                                    style={{marginBottom: "0"}}
                                                />
                                            </label>
                                            {update.description.image.length < 3 ? 
                                                <p style={{padding: "0"}} className="required">
                                                    <Translate content="token.required_field" />
                                                </p> : null}
                                        </> : null}
                                    </> : null} */}

                                    {/* кол-во знаков после запятой */}
                                    {(update.description.type == "nft") ? 
                                    <label style={{display: "none"}}>
                                        <input
                                            value="0"
                                            onChange={this._onUpdateInput.bind(
                                                0,
                                                "precision"
                                            )}
                                        />
                                        <p>{(update.precision = 0)}</p>
                                    </label> :
                                    <>
                                        <label style={{color: "#ffffff"}}>
                                            <Translate content="account.user_issued_assets.decimals" />
                                            <input
                                            type="text"
                                            min="0"
                                            max="8"
                                            value={update.precision > 8 ? 8 : update.precision}
                                                onChange={this._onUpdateInput.bind(
                                                    this,
                                                    "precision"
                                            )}
                                            style={{marginBottom: 0}}
                                        />
                                        {update.precision > 8 ? 
                                                <p style={{padding: "0"}} className="required">
                                                    <Translate content="account.user_issued_assets.max_decimals" />
                                                </p> : null}
                                            {/* <input
                                                style={{border: "0.5px solid #515b5e"}}
                                                // style={{height: "2px", background: "#ffffff"}}
                                                min="0"
                                                max="8"
                                                step="1"
                                                type="range"
                                                value={update.precision}
                                                onChange={this._onUpdateInput.bind(
                                                    this,
                                                    "precision"
                                                )} */}
                                            {/* /> */}
                                        </label>
                                        {/* <p style={{color: "#ffffff"}}>{update.precision}</p> */}
                                        <div
                                            style={{marginBottom: 10, color: "#ffffff"}}
                                            className="txtlabel cancel"
                                        >
                                            <Translate content="account.user_issued_assets.precision_warning" />
                                        </div>
                                    </>}

                                    {/* smart coin */}
                                    {/* <table
                                        className="table"
                                        style={{width: "inherit", color: "#ffffff"}}
                                    >
                                        <tbody>
                                            <tr>
                                                <td style={{border: "none"}}>
                                                    <Translate
                                                        content={
                                                            "account.user_issued_assets.mpa"
                                                        }
                                                    />:
                                                </td>
                                                <td style={{border: "none"}}>
                                                    <div
                                                        className="switch"
                                                        style={{
                                                            marginBottom: "10px"
                                                        }}
                                                        onClick={this._onToggleBitAsset.bind(
                                                            this
                                                        )}
                                                    >
                                                        <input
                                                            type="checkbox"
                                                            checked={isBitAsset}
                                                        />
                                                        <label />
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table> */}

                                    {isBitAsset ? (
                                        <table
                                            className="table"
                                            style={{width: "inherit", color: "#ffffff"}}
                                        >
                                            <tbody>
                                                <tr>
                                                    <td
                                                        style={{border: "none"}}
                                                    >
                                                        <Translate
                                                            content={
                                                                "account.user_issued_assets.pm"
                                                            }
                                                        />:
                                                    </td>
                                                    <td
                                                        style={{border: "none"}}
                                                    >
                                                        <div
                                                            className="switch"
                                                            style={{
                                                                marginBottom:
                                                                    "10px"
                                                            }}
                                                            onClick={this._onTogglePM.bind(
                                                                this
                                                            )}
                                                        >
                                                            <input
                                                                type="checkbox"
                                                                checked={
                                                                    is_prediction_market
                                                                }
                                                            />
                                                            <label />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    ) : null}

                                    {/* CER */}
                                    <Translate
                                        style={{color: "#ffffff"}}
                                        component="h3"
                                        content="account.user_issued_assets.core_exchange_rate"
                                    />

                                    <label style={{color: "#ffffff"}}>
                                        <div className="grid-block no-margin">
                                            {errors.quote_asset ? (
                                                <p className="grid-content has-error">
                                                    {errors.quote_asset}
                                                </p>
                                            ) : null}
                                            {errors.base_asset ? (
                                                <p className="grid-content has-error">
                                                    {errors.base_asset}
                                                </p>
                                            ) : null}
                                            <div className="grid-block no-margin small-12 medium-6">
                                                <div
                                                    className="amount-selector"
                                                    style={{
                                                        width: "100%",
                                                        paddingRight: "10px"
                                                    }}
                                                >
                                                    <Translate
                                                        component="label"
                                                        content="account.user_issued_assets.quote"
                                                    />
                                                    <div className="inline-label">
                                                        <input
                                                            type="text"
                                                            placeholder="0.0"
                                                            onChange={this._onCoreRateChange.bind(
                                                                this,
                                                                "quote"
                                                            )}
                                                            value={
                                                                core_exchange_rate
                                                                    .quote
                                                                    .amount
                                                            }
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="grid-block no-margin small-12 medium-6">
                                                <AmountSelector
                                                    label="account.user_issued_assets.base"
                                                    amount={
                                                        core_exchange_rate.base
                                                            .amount
                                                    }
                                                    onChange={this._onCoreRateChange.bind(
                                                        this,
                                                        "base"
                                                    )}
                                                    asset={
                                                        core_exchange_rate.base
                                                            .asset_id
                                                    }
                                                    assets={[
                                                        core_exchange_rate.base
                                                            .asset_id
                                                    ]}
                                                    placeholder="0.0"
                                                    tabIndex={1}
                                                    style={{
                                                        width: "100%",
                                                        paddingLeft: "10px"
                                                    }}
                                                />
                                            </div>
                                        </div>
                                        <div>
                                            <h5>
                                                <Translate content="exchange.price" />
                                                <span>
                                                    :{" "}
                                                    {utils.format_number(
                                                        utils.get_asset_price(
                                                            core_exchange_rate
                                                                .quote.amount *
                                                                utils.get_asset_precision(
                                                                    update.precision
                                                                ),
                                                            {
                                                                precision:
                                                                    update.precision
                                                            },
                                                            core_exchange_rate
                                                                .base.amount *
                                                                utils.get_asset_precision(
                                                                    core
                                                                ),
                                                            core
                                                        ),
                                                        2 +
                                                            (parseInt(
                                                                update.precision,
                                                                10
                                                            ) || 8)
                                                    )}
                                                </span>
                                                <span>
                                                    {" "}
                                                    {update.symbol}/{core.get(
                                                        "symbol"
                                                    )}
                                                </span>
                                            </h5>
                                        </div>
                                    </label>
                                    <div style={{color: "rgb(106 213 197)", marginTop: "34px"}}>
                                        <Translate
                                            style={{color: "rgb(106 213 197)"}}
                                            content="account.user_issued_assets.cer_warning_1"
                                            component="label"
                                            className="has-error"
                                        />
                                        <Translate
                                            style={{fontSize: "14px", marginBottom: 0}}
                                            content="account.user_issued_assets.cer_warning_2"
                                            component="p"
                                        />
                                    </div>
                                    {
                                        <p style={{color: "rgb(106 213 197)"}}>
                                            <Translate style={{fontSize: "14px"}} content="account.user_issued_assets.approx_fee" />:{" "}
                                            {createFee}
                                        </p>
                                    }
                                </div>
                            </Tab>

                            <Tab title="account.user_issued_assets.description">
                                <div className="small-12 grid-content" style={{background: "#2f2f2f", color: "#ffffff"}}>
                                    <Translate
                                        style={{color: "#ffffff"}}
                                        component="label"
                                        content="account.user_issued_assets.description"
                                    />
                                    <label style={{color: "#ffffff"}}>
                                        <textarea
                                            style={{height: "7rem"}}
                                            rows="1"
                                            value={update.description.main}
                                            onChange={this._onUpdateDescription.bind(
                                                this,
                                                "main"
                                            )}
                                        />
                                    </label>

                                    {update.description.type === "nft" ? (
                                        // nft_object
                                        <div>
                                            <label>
                                                <Translate className="asset-title" content="account.asset.artist_name" />
                                                <input
                                                    type="text"
                                                    ref={this.artistRef}
                                                    onChange={() =>
                                                        this.handleNftObject()
                                                    }
                                                    value={update.description.nft_object.artist ?
                                                        update.description.nft_object.artist : ""
                                                    }
                                                />
                                            </label>

                                            <label>
                                                <Translate className="asset-title" content="account.asset.work_title" />
                                                <input
                                                    type="text"
                                                    ref={this.workTitleRef}
                                                    onChange={() =>
                                                        this.handleNftObject()
                                                    }
                                                    value={update.description.nft_object.title ?
                                                        update.description.nft_object.title : ""
                                                    }
                                                />
                                            </label>
                                            <label>
                                                <Translate className="asset-title" content="account.asset.file_type" />
                                                <select
                                                    className="selector"
                                                    ref={this.mediaTypeRef}
                                                    onChange={() =>
                                                        this.handleNftObject()
                                                    }
                                                    value={this.state.workType}
                                                >
                                                    {mediaTypes.map(type => (
                                                        <option
                                                            className="selector-item"
                                                            value={type}
                                                        >
                                                            {type}
                                                        </option>
                                                    ))}
                                                </select>
                                            </label>
                                            <label>
                                                <Translate className="asset-title" content="account.asset.path" />
                                                <Translate
                                                    className="asset-title"
                                                    style={{
                                                        textTransform:
                                                            "lowercase"
                                                    }}
                                                    content="account.asset.path_example"
                                                />
                                                <input
                                                    type="text"
                                                    style={{margin: 0}}
                                                    ref={this.mediaRef}
                                                    onChange={() =>
                                                        this.handleNftObject()
                                                    }
                                                    value={this.state.mediaWork.length > 0 ? this.state.mediaWork : ""}
                                                    
                                                />
                                                <Translate
                                                    className="has-error"
                                                    style={{
                                                        textTransform:
                                                            "lowercase"
                                                    }}
                                                    content="validation.required"
                                                    component="p"
                                                />
                                            </label>
                                            <label>
                                                <Translate className="asset-title" content="account.asset.narrative" />
                                                <Translate
                                                    className="asset-title"
                                                    style={{
                                                        textTransform:
                                                            "lowercase"
                                                    }}
                                                    content="account.asset.narrative_note"
                                                />
                                                <textarea
                                                    style={{height: "7rem"}}
                                                    rows="1"
                                                    ref={this.narrativeRef}
                                                    onChange={() =>
                                                        this.handleNftObject()
                                                    }
                                                    value={update.description.nft_object.narrative ?
                                                        update.description.nft_object.narrative : ""
                                                    }
                                                />
                                            </label>
                                            <label>
                                                <Translate className="asset-title" content="account.asset.attestation" />
                                                <Translate
                                                    className="asset-title"
                                                    style={{
                                                        textTransform:
                                                            "lowercase"
                                                    }}
                                                    content="account.asset.attestation_note"
                                                />
                                                <textarea
                                                    style={{height: "7rem"}}
                                                    rows="1"
                                                    ref={this.attestationRef}
                                                    onChange={() =>
                                                        this.handleNftObject()
                                                    }
                                                    value={update.description.nft_object.attestation ?
                                                        update.description.nft_object.attestation : ""
                                                    }
                                                />
                                            </label>

                                            <label>
                                                <Translate className="asset-title" content="account.asset.license" />
                                                <Translate
                                                    className="asset-title"
                                                    style={{
                                                        textTransform:
                                                            "lowercase"
                                                    }}
                                                    content="account.asset.license_example"
                                                />
                                                <Translate className="asset-title" content="account.asset.license_note" />
                                                <input
                                                    type="text"
                                                    ref={this.licenseRef}
                                                    onChange={() =>
                                                        this.handleNftObject()
                                                    }
                                                    value={update.description.nft_object.license ?
                                                        update.description.nft_object.license : ""
                                                    }
                                                />
                                            </label>
                                        </div>
                                    ) : null}

                                    <Translate
                                        style={{color: "#ffffff"}}
                                        component="label"
                                        content="account.user_issued_assets.short"
                                    />
                                    <label style={{color: "#ffffff"}}>
                                        <input
                                            type="text"
                                            rows="1"
                                            value={
                                                update.description.short_name
                                            }
                                            onChange={this._onUpdateDescription.bind(
                                                this,
                                                "short_name"
                                            )}
                                        />
                                    </label>

                                    <Translate
                                        style={{color: "#ffffff"}}
                                        component="label"
                                        content="account.user_issued_assets.market"
                                    />
                                    <AssetSelector
                                        style={{color: "#ffffff"}}
                                        label="account.user_issued_assets.name"
                                        onChange={this._onInputMarket.bind(
                                            this
                                        )}
                                        asset={this.state.marketInput}
                                        assetInput={this.state.marketInput}
                                        style={{
                                            width: "100%",
                                            paddingRight: "10px"
                                        }}
                                        onFound={this._onFoundMarketAsset.bind(
                                            this
                                        )}
                                    />

                                    {is_prediction_market ? (
                                        <div style={{color: "#ffffff"}}>
                                            <Translate
                                                component="h3"
                                                content="account.user_issued_assets.condition"
                                            />
                                            <label>
                                                <input
                                                    type="text"
                                                    rows="1"
                                                    value={
                                                        update.description
                                                            .condition
                                                    }
                                                    onChange={this._onUpdateDescription.bind(
                                                        this,
                                                        "condition"
                                                    )}
                                                />
                                            </label>

                                            <Translate
                                                component="h3"
                                                content="account.user_issued_assets.expiry"
                                            />
                                            <label>
                                                <input
                                                    type="date"
                                                    value={
                                                        update.description
                                                            .expiry
                                                    }
                                                    onChange={this._onUpdateDescription.bind(
                                                        this,
                                                        "expiry"
                                                    )}
                                                />
                                            </label>
                                        </div>
                                    ) : null}
                                </div>
                            </Tab>

                            {isBitAsset ? (
                                <Tab title="account.user_issued_assets.bitasset_opts" style={{background: "#2f2f2f", color: "#ffffff"}}>
                                    <BitAssetOptions
                                        bitasset_opts={bitasset_opts}
                                        onUpdate={this.onChangeBitAssetOpts.bind(
                                            this
                                        )}
                                        backingAsset={
                                            bitasset_opts.short_backing_asset
                                        }
                                        assetPrecision={update.precision}
                                        assetSymbol={update.symbol}
                                    />
                                </Tab>
                            ) : null}

                            <Tab title="account.permissions" style={{background: "#2f2f2f", color: "#ffffff"}}>
                                <div className="small-12 grid-content">
                                    <div style={{maxWidth: 800}}>
                                        <HelpContent
                                            path={
                                                "components/AccountAssetCreate"
                                            }
                                            section="permissions"
                                        />
                                    </div>
                                    {permissions}
                                </div>
                            </Tab>

                            <Tab title="account.user_issued_assets.flags" style={{background: "#2f2f2f", color: "#ffffff"}}>
                                <div className="small-12 grid-content">
                                    <div style={{maxWidth: 800}}>
                                        <HelpContent
                                            path={
                                                "components/AccountAssetCreate"
                                            }
                                            section="flags"
                                        />
                                    </div>
                                    {permissionBooleans["charge_market_fee"] ? (
                                        <div>
                                            <Translate
                                                component="h3"
                                                content="account.user_issued_assets.market_fee"
                                            />
                                            <table className="table">
                                                <tbody>
                                                    <tr>
                                                        <td
                                                            style={{
                                                                border: "none",
                                                                width: "80%"
                                                            }}
                                                        >
                                                            <Translate content="account.user_issued_assets.charge_market_fee" />:
                                                        </td>
                                                        <td
                                                            style={{
                                                                border: "none"
                                                            }}
                                                        >
                                                            <div
                                                                className="switch"
                                                                style={{
                                                                    marginBottom:
                                                                        "10px"
                                                                }}
                                                                onClick={this._onFlagChange.bind(
                                                                    this,
                                                                    "charge_market_fee"
                                                                )}
                                                            >
                                                                <input
                                                                    type="checkbox"
                                                                    checked={
                                                                        flagBooleans.charge_market_fee
                                                                    }
                                                                />
                                                                <label />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div
                                                className={cnames({
                                                    disabled: !flagBooleans.charge_market_fee
                                                })}
                                            >
                                                <label>
                                                    <Translate content="account.user_issued_assets.market_fee" />{" "}
                                                    (%)
                                                    <input
                                                        type="number"
                                                        value={
                                                            update.market_fee_percent
                                                        }
                                                        onChange={this._onUpdateInput.bind(
                                                            this,
                                                            "market_fee_percent"
                                                        )}
                                                    />
                                                </label>

                                                <label>
                                                    <Translate content="account.user_issued_assets.max_market_fee" />{" "}
                                                    ({update.symbol})
                                                    <input
                                                        type="number"
                                                        value={
                                                            update.max_market_fee
                                                        }
                                                        onChange={this._onUpdateInput.bind(
                                                            this,
                                                            "max_market_fee"
                                                        )}
                                                    />
                                                </label>
                                                {errors.max_market_fee ? (
                                                    <p className="grid-content has-error">
                                                        {errors.max_market_fee}
                                                    </p>
                                                ) : null}
                                            </div>
                                        </div>
                                    ) : null}

                                    <h3>
                                        <Translate content="account.user_issued_assets.flags" />
                                    </h3>
                                    {flags}
                                </div>
                            </Tab>
                        </Tabs>
                    </div>
                </div>
            </div>
        );
    }
}

AccountAssetCreate = BindToChainState(AccountAssetCreate);

export {AccountAssetCreate, BitAssetOptions};

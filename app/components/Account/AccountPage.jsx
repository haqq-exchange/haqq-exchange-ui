import React from "react";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import SettingsStore from "stores/SettingsStore";
import WalletUnlockStore from "stores/WalletUnlockStore";
import GatewayStore from "stores/GatewayStore";
import ChainTypes from "../Utility/ChainTypes";
import BindToChainState from "../Utility/BindToChainState";
import {connect} from "alt-react";
import accountUtils from "common/account_utils";
import {List} from "immutable";
import {Route, Switch, Redirect} from "react-router-dom";
import Page404 from "../Page404/Page404";
import Loadable from "react-loadable";
import LoadingIndicator from "Components/LoadingIndicator";
// import Translate from "react-translate-component";
//import RoutesLink from "RoutesLink";
//import AccountAssets from "./AccountAssets";
import {AccountAssetCreate} from "./AccountAssetCreate";
import AccountAssetUpdate from "./AccountAssetUpdate";
import AccountMembership from "./AccountMembership";
import AccountVesting from "./AccountVesting";
import AccountPermissions from "./AccountPermissions";
import AccountSignedMessages from "./AccountSignedMessages";
import AccountProposals from "./AccountProposals";
import ModalStore from "../../stores/ModalStore";
//import AccountOrders from "Components/Account/AccountOrders";
// import AccountWhitelist from "./AccountWhitelist";
// import AccountVoting from "./AccountVoting";
// import AccountOverview from "./AccountOverview";



const AccountOverview = Loadable({
    loader: () => import(/* webpackChunkName: "AccountOverview" */ "./AccountOverview"),
    loading: LoadingIndicator
});
const AccountAssets = Loadable({
    loader: () => import(/* webpackChunkName: "AccountVoting" */ "./AccountAssets"),
    loading: LoadingIndicator
});
const AccountWhitelist = Loadable({
    loader: () => import(/* webpackChunkName: "AccountWhitelist" */ "./AccountWhitelist"),
    loading: LoadingIndicator
});
const AccountOrders = Loadable({
    loader: () => import(/* webpackChunkName: "AccountOrders" */ "./AccountOrders/AccountOrders"),
    loading: LoadingIndicator
});
const AccountActivity = Loadable({
    loader: () => import(/* webpackChunkName: "AccountActivity" */ "./AccountActivity/AccountActivity"),
    loading: LoadingIndicator
});
const AccountWitness = Loadable({
    loader: () => import(/* webpackChunkName: "AccountActivity" */ "./AccountWitness"),
    loading: LoadingIndicator
});


class AccountPage extends React.Component {
    static propTypes = {
        account: ChainTypes.ChainAccount.isRequired
    };

    static defaultProps = {
        account: "props.match.params.account_name"
    };

    componentDidMount() {
        if (this.props.account) {
            AccountActions.setCurrentAccount.defer(
                this.props.account.get("name")
            );

            // Fetch possible fee assets here to avoid async issues later (will resolve assets)
            accountUtils.getPossibleFees(this.props.account, "transfer");
        }
    }

    UNSAFE_componentWillReceiveProps(np) {
        if (np.account) {
            const npName = np.account.get("name");
            const currentName =
                this.props.account && this.props.account.get("name");

            if (!this.props.account || npName !== currentName) {
                // Update the current account in order to access the header right menu options
                AccountActions.setCurrentAccount.defer(npName);
                // Fetch possible fee assets here to avoid async issues later (will resolve assets)
                accountUtils.getPossibleFees(np.account, "transfer");
            }
        }
    }


    render() {
        let {
            account_name,
            wallet_locked,
            account
        } = this.props;

        if (!account) {
            return <Page404 />;
        }
        let isMyAccount = AccountStore.isMyAccount(account) && !wallet_locked;

        let passOnProps = {
            account,
            isMyAccount,
            contained: true,
            globalObject: "2.0.0",
            balances: account.get("balances", List()).toList(),
            orders: account.get("orders", List()).toList(),
            proxy: account.getIn(["options", "voting_account"]),
            history: this.props.history,
            ...this.props
        };

        //console.log("AccountPage.jsx render", passOnProps);

        return (
            <div className="grid-block account-page-layout">

                <Switch>

                    <Route
                        path={`/account/${account_name}`}
                        exact
                        render={() => <AccountOverview {...passOnProps} />}
                    />
                    <Route
                        path={`/account/${account_name}/overview`}
                        exact
                        render={() => <AccountOverview {...passOnProps} />}
                    />
                    <Redirect
                        from={`/account/${account_name}/overview`}
                        to={`/account/${account_name}`}
                    />
                    <Route
                        path={`/account/${account_name}/proposals`}
                        exact
                        render={() => <AccountProposals {...passOnProps} />}
                    />
                    <Route
                        path={`/account/${account_name}/assets`}
                        exact
                        render={() => (<AccountAssets {...passOnProps} />)}
                    />
                    <Route
                        path={`/account/${account_name}/create-asset`}
                        exact
                        render={() => (
                            <AccountAssetCreate {...passOnProps} />
                        )}
                    />
                    <Route
                        path={`/account/${account_name}/update-asset/:asset`}
                        exact
                        render={() => (
                            <AccountAssetUpdate {...passOnProps} />
                        )}
                    />
                    <Route
                        path={`/account/${account_name}/orders/:asset`}
                        exact
                        render={() => (
                            <AccountOrders {...passOnProps} />
                        )}
                    />
                    <Route
                        path={`/account/${account_name}/member-stats`}
                        exact
                        render={() => (
                            <AccountMembership {...passOnProps} />
                        )}
                    />
                    <Route
                        path={`/account/${account_name}/witness`}
                        exact
                        render={() => (
                            <AccountWitness {...passOnProps} />
                        )}
                    />
                    <Route
                        path={`/account/${account_name}/vesting`}
                        exact
                        render={() => (<AccountVesting {...passOnProps} />)}
                    />
                    <Route
                        path={`/account/${account_name}/permissions`}
                        exact
                        render={() => (
                            <AccountPermissions {...passOnProps} />
                        )}
                    />


                    <Route
                        path={`/account/${account_name}/whitelist`}
                        exact
                        render={() => (
                            <AccountWhitelist {...passOnProps} />
                        )}
                    />
                    <Route
                        path={`/account/${account_name}/signedmessages`}
                        exact
                        render={() => (
                            <AccountSignedMessages {...passOnProps} />
                        )}
                    />
                    <Route
                        path={`/account/${account_name}/activity`}
                        exact
                        render={() => (
                            <AccountActivity {...passOnProps} />
                        )}
                    />

                </Switch>
            </div>
        );
    }
}
const AccountPageBind = BindToChainState(AccountPage, {
    show_loader: true
});


class AccountPageStoreWrapper extends React.Component {
    render() {
        let account_name = this.props.match.params.account_name;

        return <AccountPageBind {...this.props} account_name={account_name} />;
    }
}

export default connect(
    AccountPageStoreWrapper,
    {
        listenTo() {
            return [
                AccountStore,
                SettingsStore,
                WalletUnlockStore,
                ModalStore,
                GatewayStore
            ];
        },
        getProps() {
            // console.log("AccountStore.getState()", AccountStore.getState())
            return {
                myActiveAccounts: AccountStore.getState().myActiveAccounts,
                searchAccounts: AccountStore.getState().searchAccounts,
                private_accounts: AccountStore.getState().privateAccounts,
                private_contacts: AccountStore.getState().privateContacts,
                settings: SettingsStore.getState().settings,
                hiddenAssets: SettingsStore.getState().hiddenAssets,
                favoriteAssets: SettingsStore.getState().favoriteAssets,
                wallet_locked: WalletUnlockStore.getState().locked,
                modals: ModalStore.getState().modals,
                viewSettings: SettingsStore.getState().viewSettings,
                backedCoins: GatewayStore.getState().backedCoins,
                bridgeCoins: GatewayStore.getState().bridgeCoins,
                gatewayDown: GatewayStore.getState().down
            };
        }
    }
);

import React from "react";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";
import {ChainStore} from "deexjs";
import ChainTypes from "../Utility/ChainTypes";
import BindToChainState from "../Utility/BindToChainState";
import Statistics from "./Statistics";
import AccountActions from "actions/AccountActions";
import TimeAgo from "../Utility/TimeAgo";
import HelpContent from "../Utility/HelpContent";
import accountUtils from "common/account_utils";
import {Tabs, Tab} from "../Utility/Tabs";
import {Row, Col, Button, Typography} from "antd";
import {cn} from "@bem-react/classname";
import "./AccountPermission.scss";
const {Paragraph} = Typography;

class FeeHelp extends React.Component {
    static propTypes = {
        dprops: ChainTypes.ChainObject.isRequired
    };
    static defaultProps = {
        dprops: "2.1.0"
    };

    render() { 
        let {dprops} = this.props;

        return (
            <HelpContent
                {...this.props}
                path="components/AccountMembership"
                section="fee-division"
                nextMaintenanceTime={{
                    time: dprops.get("next_maintenance_time")
                }}
            />
        );
    }
}
FeeHelp = BindToChainState(FeeHelp);

const cln = cn("member-ship");
class AccountMembership extends React.Component {
    static propTypes = {
        account: ChainTypes.ChainAccount.isRequired,
        gprops: ChainTypes.ChainObject.isRequired,
        core_asset: ChainTypes.ChainAsset.isRequired
    };
    static defaultProps = {
        gprops: "2.0.0",
        core_asset: "1.3.0"
    };

    upgradeAccount(id, lifetime) {
        //e.preventDefault();
        AccountActions.upgradeAccount(id, lifetime);
    }

    componentDidMount() {
        accountUtils.getFinalFeeAsset(this.props.account, "account_upgrade");
    }

    render() {
        let {gprops, core_asset} = this.props;

        let account = this.props.account.toJS();

        let ltr = ChainStore.getAccount(account.lifetime_referrer, false);
        if (ltr) account.lifetime_referrer_name = ltr.get("name");
        let ref = ChainStore.getAccount(account.referrer, false);
        if (ref) account.referrer_name = ref.get("name");
        let reg = ChainStore.getAccount(account.registrar, false);
        if (reg) account.registrar_name = reg.get("name");

        let account_name = account.name;

        let network_fee = account.network_fee_percentage / 100;
        let lifetime_fee = account.lifetime_referrer_fee_percentage / 100;
        let referrer_total_fee = 100 - network_fee - lifetime_fee;
        let referrer_fee =
            referrer_total_fee * account.referrer_rewards_percentage / 10000;
        let registrar_fee = 100 - referrer_fee - lifetime_fee - network_fee;

        let lifetime_cost =
            gprops.getIn([
                "parameters",
                "current_fees",
                "parameters",
                8,
                1,
                "membership_lifetime_fee"
            ]) *
            gprops.getIn(["parameters", "current_fees", "scale"]) /
            10000;

        let member_status = ChainStore.getAccountMemberStatus(
            this.props.account
        );
        let membership = "account.member." + member_status;
        let expiration = null;
        if (member_status === "annual")
            expiration = (
                <span className={cln("title", {"expires": true})}>
                    (<Translate content="account.member.expires" />{" "}
                    <TimeAgo time={account.membership_expiration_date} />)
                </span>
            );
        let expiration_date = account.membership_expiration_date;
        if (expiration_date === "1969-12-31T23:59:59")
            expiration_date = "Never";
        else if (expiration_date === "1970-01-01T00:00:00")
            expiration_date = "N/A";

        return (
            <div className={cln()}>
                <div className={cln("wrap")}>
                    <div className={cln("title")}>
                        <Translate content={"account.member.membership"} />{" "}
                        <Translate className={cln("title", {"sub": true})} content={membership} /> {expiration}
                    </div>
                    <Row>

                        {member_status !== "lifetime" ? <HeaderBasicMember
                            member_status={member_status} 
                            network_fee={network_fee} 
                            lifetime_cost={lifetime_cost}
                            core_asset={core_asset}
                            upgradeAccount={(id, active)=>this.upgradeAccount(id, active)}
                            account={account}
                        /> : null}

                    </Row>
                    <Row>
                        <Col xs={24} md={12}>
                            {member_status === "lifetime" ? <BodyLifeMember account={account} /> : null }
                            <div className={cln("block")}>
                                <Translate component="h4" content="account.member.fee_allocation" />
                                <table className="table key-value-table">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <Translate content="account.member.network_percentage" />
                                            </td>
                                            <td>
                                                {network_fee}%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <Translate content="account.member.lifetime_referrer" />{" "}
                                                &nbsp; (<Link
                                                    to={`account/${
                                                        account.lifetime_referrer_name
                                                    }/overview`}
                                                >
                                                    {
                                                        account.lifetime_referrer_name
                                                    }
                                                </Link>)
                                            </td>
                                            <td>
                                                {lifetime_fee}%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <Translate content="account.member.registrar" />{" "}
                                                &nbsp; (<Link
                                                    to={`account/${
                                                        account.registrar_name
                                                    }/overview`}
                                                >
                                                    {
                                                        account.registrar_name
                                                    }
                                                </Link>)
                                            </td>
                                            <td>
                                                {registrar_fee}%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <Translate content="account.member.referrer" />{" "}
                                                &nbsp; (<Link
                                                    to={`account/${
                                                        account.referrer_name
                                                    }/overview`}
                                                >
                                                    {
                                                        account.referrer_name
                                                    }
                                                </Link>)
                                            </td>
                                            <td>
                                                {referrer_fee}%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <Translate content="account.member.membership_expiration" />{" "}
                                            </td>
                                            <td>
                                                {
                                                    expiration_date
                                                }
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className={cln("block")}>
                                <Translate component={"h4"} content="account.member.fees_cashback" />
                                <table className="table key-value-table">
                                    <Statistics
                                        stat_object={
                                            account.statistics
                                        }
                                    />
                                </table>
                            </div>
                        </Col>
                        <Col xs={24} md={12}>
                            <div className={cln("block")}>
                                <FeeHelp
                                    account={account_name}
                                    networkFee={network_fee}
                                    referrerFee={referrer_fee}
                                    registrarFee={registrar_fee}
                                    lifetimeFee={lifetime_fee}
                                    referrerTotalFee={
                                        referrer_total_fee
                                    }
                                    maintenanceInterval={gprops.getIn(
                                        [
                                            "parameters",
                                            "maintenance_interval"
                                        ]
                                    )}
                                    vestingThreshold={{
                                        amount: gprops.getIn([
                                            "parameters",
                                            "cashback_vesting_threshold"
                                        ]),
                                        asset: core_asset
                                    }}
                                    vestingPeriod={
                                        gprops.getIn([
                                            "parameters",
                                            "cashback_vesting_period_seconds"
                                        ]) /
                                        60 /
                                        60 /
                                        24
                                    }
                                />
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}
export default  BindToChainState(AccountMembership);

//export default AccountMembership;

const BodyLifeMember = ({account}) => {
    const pathname = window.location.origin;
    return (
        <div className={cln("block")}>
            <Translate content={"h4"} content={"account.member.referral_link"} />
            <Translate content={"div"} content="account.member.referral_text" />:
            <Paragraph className={cln("copyable")} copyable>{`${pathname}?r=${account.name}`}</Paragraph>
        </div>
    )
}

const HeaderBasicMember = ({network_fee, lifetime_cost, core_asset, account, upgradeAccount}) => {
    return (
        <div className={cln("block")}>
            <HelpContent
                path="components/AccountMembership"
                section="lifetime"
                feesCashback={100 - network_fee}
                price={{
                    amount: lifetime_cost,
                    asset: core_asset
                }}
            />
            <br/>
            <Col className={"btn-group"}>
                <Button
                    className="btn btn-red"
                    onClick={()=>upgradeAccount(account.id,true)}
                >
                    <Translate content="account.member.upgrade_lifetime" />
                </Button>
                {/*  Not work  */}        
                {/* <Button
                    className="btn btn-red"
                    onClick={()=>upgradeAccount(account.id, false)}
                >
                    <Translate content="account.member.subscribe" />
                </Button> */}
            </Col>
        </div>
    );
};

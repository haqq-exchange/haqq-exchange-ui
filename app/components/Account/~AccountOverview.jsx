import React from "react";
import Immutable from "immutable";
import TotalBalanceValue from "../Utility/TotalBalanceValue";
import {RecentTransactions} from "./RecentTransactions";
import {ChainStore} from "deexjs";
import utils from "common/utils";
import DepositModal from "../Modal/DepositModal";
import SimpleDepositWithdraw from "../Dashboard/SimpleDepositWithdraw";
import SimpleDepositBlocktradesBridge from "../Dashboard/SimpleDepositBlocktradesBridge";
import {Tabs, Tab} from "../Utility/Tabs";
import {checkMarginStatus} from "common/accountHelper";
import BalanceWrapper from "./BalanceWrapper";
import WithdrawModal from "../Modal/WithdrawModalNew";
import AssetWrapper from "../Utility/AssetWrapper";
import AccountPortfolio from "./AccountOverview/AccountPortfolio";
import AccountOrdersTab from "./AccountOverview/AccountOrdersTab";
import AccountNetworkFees from "./AccountOverview/AccountNetworkFees";
import AccountRefs from "./AccountRefs";

class AccountOverview extends React.Component {
    static defaultProps = {
        alwaysShowAssets: __GBL_CHAIN__ ? ["GBL"] : ["BTS", "DEEX"]
    };
    constructor(props) {
        super();
        this.state = {
            settleAsset: "1.3.0",
            shownAssets: props.viewSettings.get("shownAssets", "active"),
            depositAsset: null,
            withdrawAsset: null,
            bridgeAsset: null
        };
    }

    UNSAFE_componentWillMount() {
        this._checkMarginStatus();
    }

    _checkMarginStatus(props = this.props) {
        checkMarginStatus(props.account).then(status => {
            let globalMarginStatus = null;
            for (let asset in status) {
                globalMarginStatus =
                    status[asset].statusClass || globalMarginStatus;
            }
            this.setState({globalMarginStatus});
        });
    }

    UNSAFE_componentWillReceiveProps(np) {
        if (np.account !== this.props.account) {
            this._checkMarginStatus(np);
            this.priceRefs = {};
            this.valueRefs = {};
            this.changeRefs = {};
            setTimeout(this.forceUpdate.bind(this), 500);
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (
            !utils.are_equal_shallow(
                nextProps.backedCoins,
                this.props.backedCoins
            ) ||
            !utils.are_equal_shallow(nextProps.balances, this.props.balances) ||
            nextProps.account !== this.props.account ||
            nextProps.settings !== this.props.settings ||
            nextProps.hiddenAssets !== this.props.hiddenAssets ||
            !utils.are_equal_shallow(nextState, this.state) ||
            this.state.filterValue !== nextState.filterValue
        );
    }

    render() {
        let {account, orders, hiddenAssets} = this.props;
        let {filterValue} = this.state;

        if (!account) {
            return null;
        }

        let account_balances = account.get("balances");

        let visibleBalancesList = Immutable.List(),
            hiddenBalancesList = Immutable.List();

        if (account_balances) {
            // Filter out balance objects that have 0 balance or are not included in open orders
            account_balances = account_balances.filter((a, index) => {
                let balanceObject = ChainStore.getObject(a);
                if (
                    balanceObject &&
                    (!balanceObject.get("balance") && !orders[index])
                ) {
                    return false;
                } else {
                    return true;
                }
            });

            // Separate balances into hidden and included
            account_balances.forEach((a, asset_type) => {
                const asset = ChainStore.getAsset(asset_type);

                let assetName = "";
                let filter = "";

                if (filterValue) {
                    filter = filterValue
                        ? String(filterValue).toLowerCase()
                        : "";
                    assetName = asset.get("symbol").toLowerCase();
                    let {isBitAsset} = utils.replaceName(asset);
                    if (isBitAsset) {
                        assetName = "bit" + assetName;
                    }
                }

                if (
                    hiddenAssets.includes(asset_type) &&
                    assetName.includes(filter)
                ) {
                    hiddenBalancesList = hiddenBalancesList.push(a);
                } else if (assetName.includes(filter)) {
                    visibleBalancesList = visibleBalancesList.push(a);
                }
            });
        }

        let portfolioActiveAssetsBalance = (
            <TotalBalanceValue
                noTip
                balances={visibleBalancesList}
                hide_asset
            />
        );
        let ordersValue = (
            <TotalBalanceValue
                noTip
                balances={Immutable.List()}
                openOrders={orders}
                hide_asset
            />
        );

        // Find the current Openledger coins
        // const currentDepositAsset = this.props.backedCoins.get("OPEN", []).find(c => {
        //     return c.symbol === this.state.depositAsset;
        // }) || {};
        const currentWithdrawAsset =
            this.props.backedCoins.get("OPEN", []).find(c => {
                return c.symbol === this.state.withdrawAsset;
            }) || {};
        const currentBridges =
            this.props.bridgeCoins.get(this.state.bridgeAsset) || null;

        // add unicode non-breaking space as subtext to Activity Tab to ensure that all titles are aligned
        // horizontally
        const hiddenSubText = "\u00a0";
        return (
            <div className="grid-content app-tables no-padding" ref="appTables">
                <div style={{padding: "20px"}}>
                    <h3>
                        {this.props.account_name +
                            (this.props.isMyAccount ? " (My account)" : "")}
                    </h3>
                </div>
                <div className="content-block small-12">
                    <div className="tabs-container generic-bordered-box">
                        <Tabs
                            defaultActiveTab={0}
                            segmented={false}
                            setting="overviewTab"
                            className="account-tabs"
                            tabsClass="account-overview no-padding bordered-header content-block">
                            <Tab
                                title="account.portfolio"
                                subText={portfolioActiveAssetsBalance}>
                                <AccountPortfolio
                                    visibleBalancesList={visibleBalancesList}
                                    hiddenBalancesList={hiddenBalancesList}
                                    {...this.props}
                                />
                            </Tab>
                            <Tab
                                title="account.open_orders"
                                subText={ordersValue}>
                                <AccountOrdersTab {...this.props} />
                            </Tab>
                            <Tab
                                title={"account.network_fees"}
                                subText={hiddenSubText}>
                                <AccountNetworkFees />
                            </Tab>
                            <Tab
                                title="account.activity"
                                subText={hiddenSubText}>
                                <RecentTransactions
                                    accountsList={Immutable.fromJS([
                                        account.get("id")
                                    ])}
                                    compactView={false}
                                    showMore={true}
                                    fullHeight={true}
                                    limit={15}
                                    showFilters={true}
                                    dashboard
                                />
                            </Tab>
                            {/*<Tab title={"account.referals"} subText={hiddenSubText}>
                                <AccountRefs {...this.props} />
                            </Tab>*/}
                        </Tabs>
                    </div>
                </div>

                {/* Withdraw Modal*/}
                <SimpleDepositWithdraw
                    ref="withdraw_modal"
                    action="withdraw"
                    fiatModal={this.state.fiatModal}
                    account={this.props.account.get("name")}
                    sender={this.props.account.get("id")}
                    asset={this.state.withdrawAsset}
                    modalId="simple_withdraw_modal"
                    balances={this.props.balances}
                    {...currentWithdrawAsset}
                    isDown={this.props.gatewayDown.get("OPEN")}
                />

                <WithdrawModal
                    ref="withdraw_modal_new"
                    modalId="withdraw_modal_new"
                    backedCoins={this.props.backedCoins}
                    initialSymbol={this.state.withdrawAsset}
                />

                {/* Deposit Modal */}
                <DepositModal
                    ref="deposit_modal_new"
                    modalId="deposit_modal_new"
                    asset={this.state.depositAsset}
                    account={this.props.account.get("name")}
                    backedCoins={this.props.backedCoins}
                />

                {/* Bridge modal */}
                <SimpleDepositBlocktradesBridge
                    ref="bridge_modal"
                    action="deposit"
                    account={this.props.account.get("name")}
                    sender={this.props.account.get("id")}
                    asset={this.state.bridgeAsset}
                    modalId="simple_bridge_modal"
                    balances={this.props.balances}
                    bridges={currentBridges}
                    isDown={this.props.gatewayDown.get("TRADE")}
                />
            </div>
        );
    }
}

AccountOverview = AssetWrapper(AccountOverview, {propNames: ["core_asset"]});

export default class AccountOverviewWrapper extends React.Component {
    render() {
        return <BalanceWrapper {...this.props} wrap={AccountOverview} />;
    }
}

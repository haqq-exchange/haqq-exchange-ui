// look for more icons here https://linearicons.com/free or here http://hawcons.com/preview/

import React from "react";
import PropTypes from "prop-types";
import counterpart from "counterpart";
import classNames from "classnames";
import "./icon.scss";

// const reqSvgs = require.context("./", true, /\.svg$/);
const reqSvgs = require.context("assets/static/icon", true, /\.svg$/);
const IconMap = reqSvgs.keys().reduce((images, path) => {
    let pathName = path.match(/\.\/([\w\D]+)(\.svg)/);
    images[pathName[1]] = reqSvgs(path);
    return images;
}, {});

class Icon extends React.Component {
    render() {
        let iconAttr = {
            style: this.props.style || {},
            dangerouslySetInnerHTML: {
                __html: IconMap[this.props.name]
            },
            onClick: this.props.onClick,
            className: classNames("icon", this.props.name, {
                ["icon-" + this.props.size]: !!this.props.size,
                [this.props.className]: !!this.props.className
            })
        };

        if (this.props.title != null) {
            let title = this.props.title;
            if (typeof title === "string" && title.indexOf(".") > 0) {
                title = counterpart.translate(title);
            }
            iconAttr["title"] = title;
        }

        return <span {...iconAttr} />;
    }
}

Icon.propTypes = {
    name: PropTypes.string.isRequired,
    title: PropTypes.string,
    size: PropTypes.oneOf(["1x", "1_5x", "2x", "3x", "4x", "5x", "10x"]),
    inverse: PropTypes.bool,
    className: PropTypes.string
};

Icon.defaultProps = {
    title: null
};

export default Icon;

import React from "react";
import Icon from "./Icon";


const reqSvgs = require.context("./", true, /\.svg$/);
const IconMap = reqSvgs.keys().reduce((images, path) => {
    let pathName = path.match(/\.\/([\w\D]+)(\.svg)/);
    images[pathName[1]] = reqSvgs(path);
    return images;
}, {});

export default class IconPage extends React.Component {
    render() {
        console.log("IconMap", IconMap);
        return (
            <div className={"icon-page"}>
                <h1>IconPage</h1>
                <ul>
                    {
                        Object.keys(IconMap).map(icon => {
                            return (<li key={icon}>
                                <div>{icon}</div>
                                <Icon name={icon} title={icon} style={{
                                    margin: 10,
                                }} />
                            </li>);
                        })
                    }
                </ul>
            </div>
        );
    }
}
import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import {ChainValidation} from "deexjs";
import Translate from "react-translate-component";
import counterpart from "counterpart";
import AltContainer from "alt-container";

class AccountNameInput extends React.Component {
    static propTypes = {
        id: PropTypes.string,
        placeholder: PropTypes.string,
        initial_value: PropTypes.string,
        onChange: PropTypes.func,
        onEnter: PropTypes.func,
        accountShouldExist: PropTypes.bool,
        accountShouldNotExist: PropTypes.bool,
        cheapNameOnly: PropTypes.bool,
        noLabel: PropTypes.bool,
        focus: PropTypes.bool,
        prefixSymbol: PropTypes.string
    };

    static defaultProps = {
        noLabel: false,
        cheapNameValidate: true
    };

    constructor(props) {
        super(props);
        this.state = {
            value: null,
            error: null,
            existing_account: false,
            account_name: props.prefixSymbol
        };

        this.handleChange = this.handleChange.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (
            nextState.value !== this.state.value ||
            nextState.error !== this.state.error ||
            nextState.account_name !== this.state.account_name ||
            nextState.existing_account !== this.state.existing_account ||
            nextProps.placeholder !== this.props.placeholder ||
            nextProps.searchAccounts !== this.props.searchAccounts
        );
    }

    componentDidUpdate() {
        const {value} = this.state;
        if (this.props.onChange)
            this.props.onChange({
                value: value,
                valid: !this.getError()
            });
    }

    getValue() {
        return this.state.value;
    }

    setValue(value) {
        this.setState({value});
    }

    clear() {
        this.setState({account_name: this.props.prefixSymbol, error: null, warning: null});
    }

    focus() {
        this.refInput.focus();
    }

    valid() {
        return !this.getError();
    }

    getError() {
        if (this.state.value === null) return null;
        let error = null;
        if (this.state.error) {
            error = this.state.error;
        } else if (
            this.props.accountShouldExist ||
            this.props.accountShouldNotExist
        ) {
            let account = this.props.searchAccounts.find(
                a => a === this.state.value
            );
            if (this.props.accountShouldNotExist && account) {
                error = counterpart.translate(
                    "account.name_input.name_is_taken"
                );
            }
            if (this.props.accountShouldExist && !account) {
                error = counterpart.translate("account.name_input.not_found");
            }
        }
        return error;
    }

    validateAccountName(value) {
        let warning = null;
        let error = null;

        if( this.props.cheapNameValidate ) {
            if( !value ) {
                error = "Please enter valid account name";
            } else {

                if (this.props.cheapNameOnly) {
                    if (!this.state.error && !ChainValidation.is_cheap_name(value))
                        error = counterpart.translate(
                            "account.name_input.name_is_taken"
                        );
                } else {
                    if (!this.state.error && !ChainValidation.is_cheap_name(value))
                        warning = counterpart.translate(
                            "account.name_input.name_is_taken"
                        );
                }
            }

        }
        console.log("warning", value , warning, this.props.cheapNameValidate);
        this.setState({
            value: value,
            error,
            warning
        });
        if (this.props.onChange)
            this.props.onChange({value: value, valid: !this.getError()});
        if (this.props.accountShouldExist || this.props.accountShouldNotExist)
            AccountActions.accountSearch(value);
    }

    handleChange(e) {
        e.preventDefault();
        e.stopPropagation();
        // Simplify the rules (prevent typing of invalid characters)
        var account_name = e.target.value.toLowerCase();
        //console.log("account_name", account_name);
        if (this.props.prefixSymbol) {
            if (account_name[0] !== this.props.prefixSymbol) return;
            if (account_name.length === 0 && this.state.account_name.length === 1 && this.state.account_name[0] === this.props.prefixSymbol) return;
        }
        account_name = account_name.match(/[~a-z0-9\.-]+/);
        account_name = account_name ? account_name[0] : null;
        console.log("account_name2", account_name);
        this.setState({account_name});
        this.validateAccountName(account_name);
    }

    onKeyDown(e) {
        //console.log("keyCode", e, e.keyCode);
        if (this.props.onEnter && e.keyCode === 13) this.props.onEnter(e);
    }

    render() {
        let {className} = this.props;
        let error = this.getError() || "";
        if( !className ) className = "account-name";
        let class_name = classNames("form-group", className, {
            ["error"]: error
        });
        let warning = this.state.warning;

        console.log("warning", warning);

        // let {noLabel} = this.props;


        return (
            <div className={class_name}>
                {/* {noLabel ? null : <label><Translate content="account.name" /></label>} */}
                <section className={className + "-input-wrap"}>
                    {this.props.placeholder && <label className={className + "-label left-label"}>
                        {this.props.placeholder}
                    </label>}
                    <input
                        name="username"
                        id="username1"
                        type="text"
                        ref={(ref)=>this.refInput=ref}
                        autoComplete="off"
                        placeholder={null}
                        className={className + "-input"}
                        onChange={this.handleChange}
                        onKeyDown={this.onKeyDown}
                        value={
                            this.state.account_name || this.props.initial_value
                        }
                    />
                </section>
                {error ? <div className={className + "-error"}>
                    {error}
                </div>: null}
                {warning && !error ? <div className={className + "-warning"}>
                    {warning}
                </div>: null}
            </div>
        );
    }
}

export default class StoreWrapper extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[AccountStore]}
                inject={{
                    searchAccounts: () => {
                        return AccountStore.getState().searchAccounts;
                    }
                }}
            >
                <AccountNameInput ref={(ref)=>this.nameInput=ref} {...this.props} />
            </AltContainer>
        );
    }
}

import React, {Component} from "react";
import {Link} from "react-router-dom";
import {connect} from "alt-react";
import WalletActions from "actions/WalletActions";
import BackupActions from "actions/BackupActions";
import WalletManagerStore from "stores/WalletManagerStore";
import Translate from "react-translate-component";
import cname from "classnames";
import counterpart from "counterpart";

const connectObject = {
    listenTo() {
        return [WalletManagerStore];
    },
    getProps() {
        return WalletManagerStore.getState();
    }
};

class ChangeActiveWallet extends Component {
    constructor() {
        super();
        this.state = {};
    }

    UNSAFE_componentWillMount() {
        let current_wallet = this.props.current_wallet;
        this.setState({current_wallet});
    }

    UNSAFE_componentWillReceiveProps(np) {
        if (np.current_wallet !== this.state.current_wallet) {
            this.setState({current_wallet: np.current_wallet});
        }
    }

    render() {
        let state = WalletManagerStore.getState();

        let options = [];
        state.wallet_names.forEach(wallet_name => {
            options.push(
                <option key={wallet_name} value={wallet_name}>
                    {wallet_name.toLowerCase()}
                </option>
            );
        });

        let is_dirty = this.state.current_wallet !== this.props.current_wallet;

        return (
            <div>
                <section className="block-list">
                    <header>
                        <Translate content="wallet.active_wallet" />:
                    </header>

                    <ul>
                        <li className="with-dropdown" style={{borderBottom: 0}}>
                            {state.wallet_names.count() <= 1 ? (
                                <div
                                    style={{
                                        paddingLeft: 10,
                                        lineHeight: "36px",
                                        borderRadius: "3px"
                                    }}
                                    className="settings-input"
                                >
                                    {this.state.current_wallet}
                                </div>
                            ) : (
                                <select
                                    className="settings-select"
                                    value={this.state.current_wallet}
                                    onChange={this.onChange.bind(this)}
                                >
                                    {options}
                                </select>
                            )}
                        </li>
                    </ul>
                </section>

                <Link to="wallet/create">
                    <div className="button outline">
                        <Translate content="wallet.new_wallet" />
                    </div>
                </Link>

                {is_dirty ? (
                    <div
                        className="button outline"
                        onClick={this.onConfirm.bind(this)}
                    >
                        <Translate
                            content="wallet.change"
                            name={this.state.current_wallet}
                        />
                    </div>
                ) : null}
            </div>
        );
    }

    onConfirm() {
        WalletActions.setWallet(this.state.current_wallet);
        BackupActions.reset();
        // if (window.electron) {
        //     window.location.hash = "";
        //     window.remote.getCurrentWindow().reload();
        // }
        // else window.location.href = "/";
    }

    onChange(event) {
        let current_wallet = event.target.value;
        this.setState({current_wallet});
    }
}
export default connect(ChangeActiveWallet, connectObject);

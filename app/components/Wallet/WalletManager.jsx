import React, {Component} from "react";
import {connect} from "alt-react";
import WalletManagerStore from "stores/WalletManagerStore";
import Translate from "react-translate-component";
import {Switch, Route} from "react-router-dom";
import {ExistingAccountOptions} from "./ExistingAccount";
import ImportKeys from "./ImportKeys";
import BalanceClaimActive from "./BalanceClaimActive";
import WalletChangePassword from "./WalletChangePassword";
import {WalletCreate} from "./WalletCreate";
import {BackupCreate, BackupRestore} from "./Backup";
import BackupBrainkey from "./BackupBrainkey";
import WalletOptions from "./WalletOptions";
import ChangeActiveWallet from "./ChangeActiveWallet";
import WalletDelete from "./WalletDelete";

const connectObject = {
    listenTo() {
        return [WalletManagerStore];
    },
    getProps() {
        return WalletManagerStore.getState();
    }
};

class WalletManager extends Component {

    render() {
        return (
            <div className="grid-block vertical">
                <div className="grid-container" style={{maxWidth: "40rem"}}>
                    <div className="content-block">

                        <div className="content-block">
                            <Switch>
                                <Route
                                    exact
                                    path="/wallet"
                                    component={WalletOptions}
                                />
                                <Route
                                    exact
                                    path="/wallet/change"
                                    component={ChangeActiveWallet}
                                />
                                <Route
                                    exact
                                    path="/wallet/change-password"
                                    component={WalletChangePassword}
                                />
                                <Route
                                    exact
                                    path="/wallet/import-keys"
                                    component={ImportKeys}
                                />
                                <Route
                                    exact
                                    path="/wallet/brainkey"
                                    component={ExistingAccountOptions}
                                />
                                <Route
                                    exact
                                    path="/wallet/create"
                                    component={WalletCreate}
                                />
                                <Route
                                    exact
                                    path="/wallet/delete"
                                    component={WalletDelete}
                                />
                                <Route
                                    exact
                                    path="/wallet/backup/restore"
                                    component={BackupRestore}
                                />
                                <Route
                                    exact
                                    path="/wallet/backup/create"
                                    component={BackupCreate}
                                />
                                <Route
                                    exact
                                    path="/wallet/backup/brainkey"
                                    component={BackupBrainkey}
                                />
                                <Route
                                    exact
                                    path="/wallet/balance-claims"
                                    component={BalanceClaimActive}
                                />
                            </Switch>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default connect(WalletManager, connectObject);
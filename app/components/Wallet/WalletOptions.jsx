import React, {Component} from "react";
import {Link} from "react-router-dom";
import {connect} from "alt-react";
import WalletActions from "actions/WalletActions";
import BackupActions from "actions/BackupActions";
import WalletManagerStore from "stores/WalletManagerStore";
import Translate from "react-translate-component";
import cname from "classnames";
import counterpart from "counterpart";

const connectObject = {
    listenTo() {
        return [WalletManagerStore];
    },
    getProps() {
        return WalletManagerStore.getState();
    }
};

class WalletOptions extends Component {
    render() {
        let has_wallet = !!this.props.current_wallet;
        let has_wallets = this.props.wallet_names.size > 1;
        let current_wallet = this.props.current_wallet
            ? this.props.current_wallet.toUpperCase()
            : "";
        return (
            <span>
                <div className="grid-block">
                    <div className="grid-content">
                        <div className="card">
                            <div className="card-content">
                                <label>
                                    <Translate content="wallet.active_wallet" />:
                                </label>
                                <div>{current_wallet}</div>
                                <br />
                                {has_wallets ? (
                                    <Link to="/wallet/change">
                                        <div className="button outline success">
                                            <Translate content="wallet.change_wallet" />
                                        </div>
                                    </Link>
                                ) : null}
                            </div>
                        </div>
                    </div>

                    <div className="grid-content">
                        <div className="card">
                            <div className="card-content">
                                <label>
                                    <Translate content="wallet.import_keys_tool" />
                                </label>
                                <div style={{visibility: "hidden"}}>Dummy</div>
                                <br />
                                {has_wallet ? (
                                    <Link to="/wallet/import-keys">
                                        <div className="button outline success">
                                            <Translate content="wallet.import_keys" />
                                        </div>
                                    </Link>
                                ) : null}
                            </div>
                        </div>
                    </div>

                    {has_wallet ? (
                        <div className="grid-content">
                            <div className="card">
                                <div className="card-content">
                                    <label>
                                        <Translate content="wallet.balance_claims" />
                                    </label>
                                    <div style={{visibility: "hidden"}}>
                                        Dummy
                                    </div>
                                    <br />
                                    <Link to="wallet/balance-claims">
                                        <div className="button outline success">
                                            <Translate content="wallet.balance_claim_lookup" />
                                        </div>
                                    </Link>
                                    {/*<BalanceClaimByAsset>
                            <br/>
                            <div className="button outline success">
                                <Translate content="wallet.balance_claims" /></div>
                        </BalanceClaimByAsset>
                        */}
                                </div>
                            </div>
                        </div>
                    ) : null}
                </div>

                {has_wallet ? (
                    <Link to="wallet/backup/create">
                        <div className="button outline success">
                            <Translate content="wallet.create_backup" />
                        </div>
                    </Link>
                ) : null}

                {has_wallet ? (
                    <Link to="wallet/backup/brainkey">
                        <div className="button outline success">
                            <Translate content="wallet.backup_brainkey" />
                        </div>
                    </Link>
                ) : null}

                <Link to="wallet/backup/restore">
                    <div className="button outline success">
                        <Translate content="wallet.restore_backup" />
                    </div>
                </Link>

                <br />

                {has_wallet ? <br /> : null}

                <Link to="wallet/create">
                    <div className="button outline success">
                        <Translate content="wallet.new_wallet" />
                    </div>
                </Link>

                {has_wallet ? (
                    <Link to="wallet/delete">
                        <div className="button outline success">
                            <Translate content="wallet.delete_wallet" />
                        </div>
                    </Link>
                ) : null}

                {has_wallet ? (
                    <Link to="wallet/change-password">
                        <div className="button outline success">
                            <Translate content="wallet.change_password" />
                        </div>
                    </Link>
                ) : null}
            </span>
        );
    }
}
export default connect(WalletOptions, connectObject);
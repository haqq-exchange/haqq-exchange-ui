import React, {Component} from "react";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import {FormattedDate} from "react-intl";
import {connect} from "alt-react";
import WalletActions from "actions/WalletActions";
import WalletManagerStore from "stores/WalletManagerStore";
import BackupStore from "stores/BackupStore";
import WalletDb from "stores/WalletDb";
import BackupActions, {
    backup,
    decryptWalletBackup
} from "actions/BackupActions";
import notify from "actions/NotificationActions";
import {saveAs} from "file-saver";
import cname from "classnames";
import Translate from "react-translate-component";
import {PrivateKey} from "deexjs";
import SettingsActions from "actions/SettingsActions";
import {backupName} from "common/backupUtils";
import ActionSheet from "react-foundation-apps/src/action-sheet";
import IntlActions from "actions/IntlActions";
import SettingsStore from "stores/SettingsStore";

import {Create, NameSizeModified, Download, Sha1} from "./Backup";

const connectObject = {
    listenTo() {
        return [WalletManagerStore, BackupStore];
    },
    getProps() {
        let wallet = WalletManagerStore.getState();
        let backup = BackupStore.getState();
        return {wallet, backup};
    }
};

//The default component is WalletManager.jsx
class BackupCreate extends Component {
    render() {
        return (
            <div style={{maxWidth: "40rem"}}>
                <Create
                    noText={this.props.noText}
                    newAccount={
                        this.props.location
                            ? this.props.location.query.newAccount
                            : null
                    }
                >
                    <NameSizeModified />
                    {this.props.noText ? null : <Sha1 />}
                    <Download downloadCb={this.props.downloadCb} />
                </Create>
            </div>
        );
    }
}
export default connect(
    BackupCreate,
    connectObject
);


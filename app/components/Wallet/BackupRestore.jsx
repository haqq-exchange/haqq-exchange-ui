import React, {Component} from "react";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import {FormattedDate} from "react-intl";
import {connect} from "alt-react";
import WalletActions from "actions/WalletActions";
import WalletManagerStore from "stores/WalletManagerStore";
import BackupStore from "stores/BackupStore";
import WalletDb from "stores/WalletDb";
import BackupActions, {
    backup,
    decryptWalletBackup
} from "actions/BackupActions";
import notify from "actions/NotificationActions";
import {saveAs} from "file-saver";
import cname from "classnames";
import Translate from "react-translate-component";
import {PrivateKey} from "deexjs";
import SettingsActions from "actions/SettingsActions";
import {backupName} from "common/backupUtils";
import ActionSheet from "react-foundation-apps/src/action-sheet";
import IntlActions from "actions/IntlActions";
import SettingsStore from "stores/SettingsStore";
import {Upload, NameSizeModified, NewWalletName, DecryptBackup, Restore} from "./Backup";

const FlagImage = ({flag, width = 30, height = "auto"}) => {
    return (
        <img
            height={height}
            width={width}
            src={require(`${__BASE_URL__}assets/language-dropdown/img/${flag.toUpperCase()}.png`)}
        />
    );
};

const connectObject = {
    listenTo() {
        return [WalletManagerStore, BackupStore];
    },
    getProps() {
        let wallet = WalletManagerStore.getState();
        let backup = BackupStore.getState();
        return {wallet, backup};
    }
};

class BackupRestore extends Component {
    constructor() {
        super();
        this.state = {
            newWalletName: null,
            locales: SettingsStore.getState().defaults.locale,
            currentLocale: SettingsStore.getState().settings.get("locale")
        };
    }

    UNSAFE_componentWillMount() {
        BackupActions.reset();
    }

    render() {
        let new_wallet = this.props.wallet.new_wallet;
        let has_new_wallet = this.props.wallet.wallet_names.has(new_wallet);
        let restored = has_new_wallet;

        const flagDropdown = (
            <ActionSheet>
                <ActionSheet.Button title="" style={{width: "30px"}}>
                    {/* <a
                        style={{padding: "1rem", border: "none"}}
                        className="button arrow-down"
                    > */}
                    <FlagImage flag={this.state.currentLocale} />
                    {/* </a> */}
                </ActionSheet.Button>
                <ActionSheet.Content>
                    <ul className="no-first-element-top-border">
                        {this.state.locales.map(locale => {
                            return (
                                <li key={locale}>
                                    <a
                                        href={"#"}
                                        onClick={e => {
                                            e.preventDefault();
                                            IntlActions.switchLocale(locale);
                                            this.setState({
                                                currentLocale: locale
                                            });
                                        }}
                                    >
                                        <div className="table-cell">
                                            <FlagImage
                                                width="20"
                                                height="20"
                                                flag={locale}
                                            />
                                        </div>
                                        <div
                                            className="table-cell"
                                            style={{paddingLeft: 10}}
                                        >
                                            <Translate
                                                content={"languages." + locale}
                                            />
                                        </div>
                                    </a>
                                </li>
                            );
                        })}
                    </ul>
                </ActionSheet.Content>
            </ActionSheet>
        );

        return (
            <div className="restore-backup">
                <header className="restore-backup__header">
                    <div
                        className="restore-backup__logo"
                        dangerouslySetInnerHTML={{
                            __html: require("assets/logo-ico.svg")
                        }}
                    />

                    <div className="restore-backup__lang">{flagDropdown}</div>
                </header>
                <Translate
                    component="h1"
                    className="restore-backup__title"
                    content="wallet.restore_backup"
                />
                <Translate
                    style={{textAlign: "left", maxWidth: "30rem"}}
                    className="restore-backup__text"
                    component="p"
                    content="wallet.import_backup_choose"
                />
                <div className="restore-backup__container">
                    {new FileReader().readAsBinaryString ? null : (
                        <p className="error">
                            Warning! You browser doesn't support some some file
                            operations required to restore backup, we recommend
                            you to use Chrome or Firefox browsers to restore
                            your backup.
                        </p>
                    )}

                    <Upload>
                        <NameSizeModified />
                    </Upload>

                    <DecryptBackup saveWalletObject={true}>
                        <NewWalletName>
                            <Restore />
                        </NewWalletName>
                    </DecryptBackup>

                    <div className="restore-backup__link">
                        <Link to="/">
                            <Translate content="wallet.restore-login" />
                        </Link>
                        <span> or </span>
                        <Link to="/create-account/password">
                            <Translate content="wallet.create-account" />
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default  connect(
    BackupRestore,
    connectObject
);

import React, {Component} from "react";
import {Link} from "react-router-dom";
import {connect} from "alt-react";
import WalletActions from "actions/WalletActions";
import BackupActions from "actions/BackupActions";
import WalletManagerStore from "stores/WalletManagerStore";
import Translate from "react-translate-component";
import cname from "classnames";
import counterpart from "counterpart";

const connectObject = {
    listenTo() {
        return [WalletManagerStore];
    },
    getProps() {
        return WalletManagerStore.getState();
    }
};

class WalletDelete extends Component {
    constructor() {
        super();
        this.state = {
            selected_wallet: null,
            confirm: 0
        };
    }

    _onCancel() {
        this.setState({
            confirm: 0,
            selected_wallet: null
        });
    }

    render() {
        if (this.state.confirm === 1) {
            return (
                <div style={{paddingTop: 20}}>
                    <h4>
                        <Translate content="wallet.delete_confirm_line1" />
                    </h4>
                    <Translate
                        component="p"
                        content="wallet.delete_confirm_line3"
                    />
                    <br />
                    <div
                        className="button outline"
                        onClick={this.onConfirm2.bind(this)}
                    >
                        <Translate
                            content="wallet.delete_confirm_line4"
                            name={this.state.selected_wallet}
                        />
                    </div>
                    <div
                        className="button outline"
                        onClick={this._onCancel.bind(this)}
                    >
                        <Translate content="wallet.cancel" />
                    </div>
                </div>
            );
        }

        // this.props.current_wallet
        let placeholder = (
            <option
                key="placeholder"
                value=""
                disabled={this.props.wallet_names.size > 1}
            />
        );
        // if (this.props.wallet_names.size > 1) {
        //     placeholder = <option value="" disabled>{placeholder}</option>;
        // }
        // else {
        //     //When disabled and list_size was 1, chrome was skipping the
        //     //placeholder and selecting the 1st item automatically (not shown)
        //     placeholder = <option value="">{placeholder}</option>;
        // }
        let options = [placeholder];
        options.push(
            <option key="select_option" value="">
                {counterpart.translate("settings.delete_select")}&hellip;
            </option>
        );
        this.props.wallet_names.forEach(wallet_name => {
            options.push(
                <option key={wallet_name} value={wallet_name}>
                    {wallet_name.toLowerCase()}
                </option>
            );
        });

        let is_dirty = !!this.state.selected_wallet;
        let locationHash = this.props.location.hash.split("#");
        
        console.log('delete-wallet', locationHash);

        return (
            <div style={{paddingTop: 20}} className={cname({
                "highlight": locationHash[1] === "delete-wallet"
            })} id={"delete-wallet"}>
                <section className="block-list">
                    <header>
                        <Translate content="wallet.delete_wallet" />
                    </header>
                    <ul>
                        <li className="with-dropdown" style={{borderBottom: 0}}>
                            <select
                                className="settings-select"
                                value={this.state.selected_wallet || ""}
                                style={{margin: "0 auto"}}
                                onChange={this.onChange.bind(this)}
                            >
                                {options}
                            </select>
                        </li>
                    </ul>
                </section>
                <div
                    className={cname("button outline", {disabled: !is_dirty})}
                    onClick={this.onConfirm.bind(this)}
                >
                    <Translate
                        content={
                            this.state.selected_wallet
                                ? "wallet.delete_wallet_name"
                                : "wallet.delete_wallet"
                        }
                        name={this.state.selected_wallet}
                    />
                </div>
            </div>
        );
    }

    onConfirm() {
        this.setState({confirm: 1});
    }

    onConfirm2() {
        WalletActions.deleteWallet(this.state.selected_wallet);
        this._onCancel();
        window.setTimeout(()=>{
            window.location.replace("/");
        }, 500);
    }

    onChange(event) {
        let selected_wallet = event.target.value;
        this.setState({selected_wallet});
    }
}
export default connect(WalletDelete, connectObject);

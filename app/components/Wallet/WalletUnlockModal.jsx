import React from "react";
import PropTypes from "prop-types";
import BaseModal from "../Modal/BaseModal";
import ZfApi from "react-foundation-apps/src/utils/foundation-api";
import PasswordInput from "../Forms/PasswordInput";
import notify from "actions/NotificationActions";
import AltContainer from "alt-container";
import WalletDb from "stores/WalletDb";
import WalletUnlockStore from "stores/WalletUnlockStore";
import WalletManagerStore from "stores/WalletManagerStore";
import BackupStore from "stores/BackupStore";
import AccountStore from "stores/AccountStore";
import WalletUnlockActions from "actions/WalletUnlockActions";
import WalletActions from "actions/WalletActions";
import BackupActions, {restore, backup} from "actions/BackupActions";
import AccountActions from "actions/AccountActions";
import {Apis} from "deexjs-ws";
import utils from "common/utils";
import AccountSelector from "../Account/AccountSelector";
import {PrivateKey} from "deexjs";
import {saveAs} from "file-saver";
import LoginTypeSelector from "../Wallet/LoginTypeSelector";
import counterpart from "counterpart";
import {
    WalletSelector,
    CreateLocalWalletLink,
    WalletDisplay,
    CustomPasswordInput,
    LoginButtons,
    BackupWarning,
    BackupFileSelector,
    DisableChromeAutocomplete,
    CustomError,
    KeyFileLabel
} from "../Wallet/WalletUnlockModalLib";
import {backupName} from "common/backupUtils";
import Form from "./Form";
import Field from "./Field";
import Translate from "react-translate-component";
import TranslateWithLinks from "../Utility/TranslateWithLinks";
import AccountName from "./AccountName";

import ls from "common/localStorage";
import configConst from "config/const";
let accountStorage = new ls(configConst.STORAGE_KEY);
import {Login2fa, LoginOldContainer} from "../../RoutesLink";

class WalletUnlockModal extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor(props) {
        super(props);
        this.state = {
            isLogin2fa: accountStorage.has("qrCode")
        };
    }

    UNSAFE_componentWillReceiveProps() {
        // console.log('UNSAFE_componentWillReceiveProps', this);

    }

    componentDidMount() {

    }

    componentDidUpdate() {
        const {resolve, modalId, isLocked} = this.props;

        if (resolve)
            if (isLocked) {
                ZfApi.publish(modalId, "open");
            } else {
                resolve();
            }
        else ZfApi.publish(this.props.modalId, "close");
    }


    render() {
        const { modalId , passwordLogin } = this.props;
        const { isLogin2fa } = this.state;

        // console.log("passwordLogin", passwordLogin)

        return (
            // U N L O C K
            <BaseModal
                id={modalId}
                ref={(ref)=>this.modal=ref}
                overlay={true}
                overlayClose={false}
                modalHeader="header.unlock_short"
                className="public-login-modal"
                noLoggo={true}
                noHeader={true}
            >
                {isLogin2fa ?
                    <Login2fa accountQrCode={accountStorage.get("qrCode")} /> :
                    <LoginOldContainer />
                }
            </BaseModal>
        );
    }
}

WalletUnlockModal.defaultProps = {
    modalId: "unlock_wallet_modals"
};

class WalletUnlockModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[
                    WalletUnlockStore,
                    AccountStore,
                    WalletManagerStore,
                    WalletDb,
                    BackupStore
                ]}
                inject={{
                    currentWallet: () =>
                        WalletManagerStore.getState().current_wallet,
                    walletNames: () =>
                        WalletManagerStore.getState().wallet_names,
                    dbWallet: () => WalletDb.getWallet(),
                    isLocked: () => WalletDb.isLocked(),
                    backup: () => BackupStore.getState(),
                    resolve: () => WalletUnlockStore.getState().resolve,
                    reject: () => WalletUnlockStore.getState().reject,
                    locked: () => WalletUnlockStore.getState().locked,
                    passwordLogin: () =>
                        WalletUnlockStore.getState().passwordLogin,
                    passwordAccount: () =>
                        AccountStore.getState().passwordAccount || ""
                }}
            >
                <WalletUnlockModal {...this.props} />
            </AltContainer>
        );
    }
}
export default WalletUnlockModalContainer;

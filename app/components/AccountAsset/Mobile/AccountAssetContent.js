import React from "react";
import Translate from "react-translate-component";
// import ValueStore from "Utility/ValueStore";
import MarketPrice from "Components/Dashboard/MarketsCell/Price";
import utils from "lib/common/utils";
import {getAlias} from "config/alias";
// import MediaQuery from "Components/Dashboard/MarketsRow";
import {Asset} from "lib/common/MarketClasses";
import ChangeDropDownCash from "Components/Layout/Header/ChangeDropDownCash.js";
import AssetWrapper from "Utility/AssetWrapper";
import FormattedAsset from "Utility/FormattedAsset";

class AccountAssetContent extends React.Component {

    render() {
        return (
            <div className={"account-asset-content"}>
                <ContentInfo {...this.props}  />
                <ContentTable {...this.props} />
            </div>
        );
    }
}

const ContentBody = (props) => {
    const {currentStats, base_asset, quote_asset} = props;
    return (
        <div className={"account-asset-content-mobile-body"}>

            <div className={"account-asset-info-line"}>
                <Translate component={"div"} className={"account-asset-content-mobile-title"} content="dashboard.price"/>
                {currentStats ? <MarketPrice
                    base={base_asset.get("symbol")}
                    quote={quote_asset.get("symbol")}
                    base_asset={base_asset}
                    quote_asset={quote_asset}
                    marketStats={currentStats} /> : "-"}

            </div>
            <div className={"account-asset-info-line"}>
                <Translate component={"div"} className={"account-asset-content-mobile-title"} content="dashboard.chanage_24"/>
                <span>{currentStats && currentStats.change ? currentStats.change + "%" : "-"}</span>
            </div>
            <div className={"account-asset-info-line"} >
                <Translate component={"div"} className={"account-asset-content-mobile-title"} content="dashboard.chanage_24_high"/>
                <span>{currentStats && currentStats.volumeLowest ? currentStats.volumeLowest : "-"}</span>
            </div>
            <div className={"account-asset-info-line"} >
                <Translate component={"div"} className={"account-asset-content-mobile-title"} content="dashboard.chanage_24_low"/>
                <span>{currentStats && currentStats.volumeHighest ? currentStats.volumeHighest : "-"}</span>
            </div>
            <div className={"account-asset-info-line"}>
                <Translate component={"div"} className={"account-asset-content-mobile-title"} content="dashboard.current_volume"/>
                <CurrentSupplyWraper component={"span"} backingAsset={quote_asset.get("symbol")} />
            </div>
            <div className={"account-asset-info-line"}>
                <Translate component={"div"} className={"account-asset-content-mobile-title"} content="dashboard.chanage_24_volume"/>
                <span>{currentStats && currentStats.volumeBase ? utils.format_volume(currentStats.volumeBase, quote_asset.get("precision")) : "-"}</span>
            </div>
        </div>
    );
};
const ContentFooter = (props) => {
    const {settings} = props;
    return (
        <div className={"account-asset-content-footer"}>
            <Translate content={"account.asset.display_currency"} />
            <span>{settings.get("unit")}</span>
            <ChangeDropDownCash prefixClassName={"account-asset-cash"} />
        </div>
    );
};

const ContentTable = (props) => {
    return (
        <div className={"account-asset-table"}>
            <ContentBody {...props}/>
            <ContentFooter {...props} />
        </div>
    );
};

const ContentInfo = (props) => {
    const {backedCoin, quote_asset} = props;
    let withdrawalStaticFee,
        depositStaticFee,
        depositPercentageFee,
        withdrawalPercentageFee;
    if(!backedCoin) return null;
    let withdrawalAllowed = backedCoin.withdrawalAllowed,
        depositAllowed = backedCoin.depositAllowed;

    const getAsset = (amount) => {
        return new Asset({
            asset_id: quote_asset.get("id"),
            precision: quote_asset.get("precision"),
            amount: amount || 0
        });
    };


    let nullAsset = getAsset(0);
        depositPercentageFee = Number(backedCoin.depositPercentageFee);
        depositStaticFee = getAsset(backedCoin.depositStaticFee);


        withdrawalStaticFee = getAsset(backedCoin.withdrawalStaticFee);
        withdrawalPercentageFee = Number(backedCoin.withdrawalPercentageFee);


    return (
        <div className={"account-asset-info"}>
            <div className={"account-asset-info-wrapper"}>
                {depositAllowed && backedCoin.depositMaxAmount ? <div className={"account-asset-info-line"}>
                    <Translate component={"div"} content={"gateway.info.depositMaxAmount"}/>
                    <FormattedAsset
                        amount={backedCoin.depositMaxAmount}
                        asset={backedCoin.backingCoin}
                    />
                </div> : null }

                {depositAllowed && backedCoin.depositMinAmount ? <div className={"account-asset-info-line"}>
                    <Translate component={"div"} content={"gateway.info.depositMinAmount"}/>
                    <FormattedAsset
                        amount={backedCoin.depositMinAmount}
                        asset={backedCoin.backingCoin}
                    />
                </div>: null }

                {depositAllowed && depositStaticFee.gt(nullAsset) ? <div className={"account-asset-info-line"}>
                    <Translate component={"div"} content={"gateway.info.depositPercentageFee"}/>
                    <FormattedAsset
                        amount={depositStaticFee.getAmount({real: false})}
                        asset={backedCoin.backingCoin}
                    />
                </div>: null }
                {depositAllowed && depositPercentageFee ? <div className={"account-asset-info-line"}>
                    <Translate component={"div"} content={"gateway.info.depositPercentageFee"}/>
                    {depositPercentageFee} %
                </div>: null }


                {withdrawalAllowed && backedCoin.withdrawMaxAmount ? <div className={"account-asset-info-line"}>
                    <Translate component={"div"} content={"gateway.info.withdrawMaxAmount"}/>
                    <FormattedAsset
                        amount={backedCoin.withdrawMaxAmount}
                        asset={backedCoin.backingCoin}
                    />
                </div>: null }

                {withdrawalAllowed && backedCoin.withdrawMinAmount ? <div className={"account-asset-info-line"}>
                    <Translate component={"div"} content={"gateway.info.withdrawMinAmount"}/>
                    <FormattedAsset
                        amount={backedCoin.withdrawMinAmount}
                        asset={backedCoin.backingCoin}
                    />
                </div>: null }
                {withdrawalAllowed && withdrawalStaticFee.gt(nullAsset) ? <div className={"account-asset-info-line"}>
                    <Translate component={"div"} content={"gateway.info.withdrawalPercentageFee"}/>
                    <FormattedAsset
                        amount={withdrawalStaticFee.getAmount({real: false})}
                        asset={backedCoin.backingCoin}
                    />
                </div>: null }

                {withdrawalAllowed && withdrawalPercentageFee ? <div className={"account-asset-info-line"}>
                    <Translate component={"div"} content={"gateway.info.withdrawalPercentageFee"}/>
                    {withdrawalPercentageFee} %
                </div>: null }
            </div>
            {/*<div className={"account-asset-info-wrapper"}>
                ContentInfo
            </div>*/}
        </div>
    );
};

class CurrentSupply extends React.Component {

    render() {
        const {backingAsset, component}= this.props;
        let dynamic = this.props.getDynamicObject(backingAsset.get("dynamic_asset_data_id"));
        if (!dynamic) return <div>-</div>;
        dynamic = dynamic.toJS();
        return (
            <FormattedAsset
                component={component || "div"}
                amount={dynamic.current_supply}
                asset={backingAsset.get("id")}
            />
        );
    }
}

const CurrentSupplyWraper = AssetWrapper(CurrentSupply, {
    propNames: ["backingAsset"],
    withDynamic: true
});

export default AccountAssetContent;

import React from "react";
import {Link} from "react-router-dom";
import cn from "classnames";
import Icon from "Components/Icon/Icon";
import ModalActions from "actions/ModalActions";
import Translate from "react-translate-component";
// import AssetName from "Utility/AssetName";
import LinkToAssetById from "Utility/LinkToAssetById";
// import AccountUtils from "app/lib/common/account_utils";
// import {Asset} from "lib/common/MarketClasses";
// import {FetchChain} from "deexjs";
import BalanceComponent from "Utility/BalanceComponent";
import WalletDb from "stores/WalletDb";

class AccountAssetHeader extends React.Component {
    render() {
        return (
            <div className={"account-asset-header"}>
                <AssetTitle {...this.props} />
                <AssetButton {...this.props} />
            </div>
        );
    }
}
class AssetTitle extends React.Component {


    render() {

        const {currentStats, currentAccount, quote_asset, account} = this.props;

        return (
            <div className={"account-asset-title"}>
                <div className={"account-asset-title-top"}>
                    {quote_asset && <LinkToAssetById className={"account-asset-title"} noLink asset={quote_asset.get("id")} showLogo />}
                    <Link to={["/account", currentAccount, "activity?filter=transfer"].join("/")} className={"account-asset-btn-link"} >
                        <Translate content="header.transfer" />
                    </Link>
                </div>
                <div className={"account-asset-title-bottom"}>
                    <div className={"account-asset-title-price"}>
                        {account && account.hasIn(["balances", quote_asset.get("id")])
                            ? <BalanceComponent balance={account.getIn(["balances", quote_asset.get("id")])} hide_asset />
                            : null}
                    </div>
                    <div className={cn("account-asset-title-change", {
                        "positive": currentStats && currentStats.change > 0,
                        "negative": currentStats && currentStats.change < 0,
                    })}>{currentStats && currentStats.change || 0}%</div>
                </div>
            </div>
        );
    }
}

const AssetButton = (props) => {
    const {quote_asset, currentAccount, backedCoin, account} = props;
    let dataModal = {
        account: currentAccount,
        asset: quote_asset.get("id"),
        asset_id: quote_asset.get("id"),
        onShowForm: () => {
            ModalActions.hide("transfer_asset_modal");
        }
    };

    let hasBalanceAsset = account && account.hasIn(["balances", quote_asset.get("id")]);

    const showWindow = (typeWindow) => {
        ModalActions.show("transfer_asset_modal", {
            typeWindow,
            dataModal
        });
    };

    const canDeposit = backedCoin && backedCoin.depositAllowed;

    const canWithdraw = backedCoin && backedCoin.withdrawalAllowed && account.hasIn(["balances", quote_asset.get("id")]);

    return (
        <div className={"account-asset-btn"}>

            <button onClick={()=> showWindow("sendTransfer")} className={"btn btn-green"} disabled={WalletDb.isLocked() || !hasBalanceAsset} >
                <Icon name="transfer" className="icon-14px"/>
                <Translate content="header.payments"/>
            </button>
            <button onClick={()=> showWindow("depositAddress")} className={"btn btn-green"} disabled={!canDeposit}>
                <Icon name="deposit2" className="icon-14px"/>
                <Translate content="modal.deposit.submit"/>
            </button>
            <button onClick={()=> showWindow("sendWithdraw")} className={"btn btn-green"} disabled={!canWithdraw}>
                <Icon name="withdraw2" className="icon-14px"/>
                <Translate content="modal.withdraw.submit"/>
            </button>
        </div>
    );
};


export default AccountAssetHeader;

import React from "react";
import {connect} from "alt-react";
import ChainTypes from "Utility/ChainTypes";
import BindToChainState from "Utility/BindToChainState";

import AccountAssetHeader from "./Desktop/AccountAssetHeader";
import AccountAssetContent from "./Desktop/AccountAssetContent";
import AccountAssetMarket from "./Desktop/AccountAssetMarket";
import DeexNews from "./Desktop/DeexNews";

import MobileAccountAssetHeader from "./Mobile/AccountAssetHeader";
import MobileAccountAssetContent from "./Mobile/AccountAssetContent";
import MobileAccountAssetMarket from "./Mobile/AccountAssetMarket";

import SettingsStore from "stores/SettingsStore";
import MarketsStore from "stores/MarketsStore";
import AccountStore from "stores/AccountStore";
import GatewayStore from "stores/GatewayStore";
import MarketsActions from "actions/MarketsActions";
//import TransferAssetModal from "Components/Modal/TransferAssetModal";
import Page404 from "../Page404/Page404";

import MediaQuery from "react-responsive";
import ResponseOption from "config/response";

import "./style.scss";
import {async_fetch} from "api/apiConfig";
import counterpart from "counterpart";

class AccountAsset extends React.Component {

    static propTypes = {
        account: ChainTypes.ChainAccount,
        quote_asset: ChainTypes.ChainAsset.isRequired,
        base_asset: ChainTypes.ChainAsset.isRequired
    };

    static defaultProps = {
        quote_asset: "props.match.params.symbol",
        base_asset: "1.3.0"
    };

    state = {
        socialLinks: {}
    };

    componentDidMount() {
        // this._setInterval();
        this.getLoadedDescriptionAsset();
        this.getLoadedLinks();
    }

    componentWillUnmount() {
        this._clearInterval();
    }

    _setInterval() {
        let { quote_asset, base_asset } = this.props;
        if( !base_asset || !quote_asset ) return null;
        // console.log("base_asset", base_asset)
        // console.log("actions.getMarketStatsInterval _setInterval");
        this.statsInterval  = MarketsActions.getMarketStatsInterval(
            35 * 1000,
            base_asset, quote_asset
        );
    }

    _clearInterval() {
        if (this.statsInterval) this.statsInterval();
    }

    getLoadedDescriptionAsset = () => {
        const {settings} = this.props;
        let accountasset = counterpart.translate("account.asset");
        if( !accountasset.DEEX ) {
            let response = __GBL_CHAIN__ || __GBLTN_CHAIN__ ?
                async_fetch("https://static.haqq.exchange/gbl/locales/account_asset.json")
                : (__SCROOGE_CHAIN__ ?
                 async_fetch("https://static.scrooge.club/locales//account_asset.json") : async_fetch("https://static.haqq.exchange/locales/account_asset.json"));
            // let response = async_fetch("/static/main_page.json");
            response.then(pageData => {
                Object.keys(pageData).map(local=>{
                    counterpart.registerTranslations(local, pageData[local]);
                });
                return pageData;
            }).then(()=>{
                counterpart.setLocale(settings.get("locale"));
            });
        }

    };

    getLoadedLinks = () => {
        const _this = this;
        const {quote_asset} = this.props;
        var myHeaders = new Headers();
        var myInit = {
            headers: myHeaders,
        };

        if( !quote_asset ) return null;

        if(!__SCROOGE_CHAIN__) {
        fetch("https://haqq.exchange/tools/get_social_links?asset=" + quote_asset.get("symbol"), myInit)
            .then(r=>r.json())
            .then((result) => {
                // _this.setState({
                //     socialLinks: result.data
                // });
            }).catch(()=>{

            });
        }
    };

    render() {
        const {quote_asset, base_asset} = this.props;
        const {socialLinks} = this.state;
        // console.log("AccountAsset marketData", currentStats);
        // console.log("AccountAsset socialLinks", socialLinks, this.state);
        if( !base_asset || !quote_asset ) return <Page404 />;
        return (
            <div className={"account-asset"}>
                <MediaQuery {...ResponseOption.mobile}>
                    {(matches) => {
                        if (matches) {
                            return (
                                <>
                                    <MobileAccountAssetHeader {...this.props} />
                                    <MobileAccountAssetContent {...this.props} socialLinks={socialLinks} /> {/* socialLinks={socialLinks} */}
                                    <MobileAccountAssetMarket {...this.props} />
                                    {__GBL_CHAIN__ || __SCROOGE_CHAIN__ || __GBLTN_CHAIN__ ? null :
                                    <DeexNews {...this.props} />}
                                </>
                            );
                        } else {
                            return (
                                <>
                                    <AccountAssetHeader {...this.props} />
                                    <AccountAssetContent {...this.props} socialLinks={socialLinks} />
                                    <AccountAssetMarket {...this.props} />
                                    {__GBL_CHAIN__ || __SCROOGE_CHAIN__ || __GBLTN_CHAIN__ ? null :
                                    <DeexNews {...this.props} />}
                                </>
                            );
                        }
                    }}
                </MediaQuery>

                {/*<TransferAssetModal />*/}
            </div>
        );
    }
}

const AccountAssetBind = BindToChainState(AccountAsset, {});

class AccountPageStoreWrapper extends React.Component {
    render() {
        let {settings, unit, match, allMarketStats} = this.props;
        let symbol = match.params.symbol;

        let preferredUnit = settings.get("unit");

        if(!__GBL_CHAIN__ && !__GBLTN_CHAIN__) {
            if( preferredUnit === symbol ) {
                preferredUnit = unit.find(a => a !== symbol);
            }
        }

        const backedCoin = GatewayStore.getBackedCoin("TDEX", symbol);
        console.log("this.props", this.props);
        console.log("this.props preferredUnit", preferredUnit);

        return <AccountAssetBind
            {...this.props}
            backedCoin={backedCoin}
            currentStats={allMarketStats.get([symbol, preferredUnit].join("_"))}
            base_asset={preferredUnit} />;
    }
}



export default connect(AccountPageStoreWrapper, {
    listenTo() {
        return [SettingsStore, MarketsStore];
    },
    getProps() {
        return {
            allMarketStats: MarketsStore.getState().allMarketStats,
            currentAccount: AccountStore.getState().passwordAccount || AccountStore.getState().currentAccount,
            account: AccountStore.getState().passwordAccount || AccountStore.getState().currentAccount,
            settings: SettingsStore.getState().settings,
            current_supply: SettingsStore.getState().viewSettings.get("cs"),
            assetMarketTab: SettingsStore.getState().assetMarketTab,
            unit: SettingsStore.getState().defaults.unit,
        };
    }
});

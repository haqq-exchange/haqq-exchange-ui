import React from "react";
import Translate from "react-translate-component";
import cn from "classnames";
import MarketPrice from "Components/Dashboard/MarketsCell/Price";
import Icon from "Components/Icon/Icon";
import utils from "lib/common/utils";
import {getAlias} from "config/alias";
// import MediaQuery from "Components/Dashboard/MarketsRow";
import {Asset} from "lib/common/MarketClasses";
import ChangeDropDownCash from "Components/Layout/Header/ChangeDropDownCash";
import AssetWrapper from "Utility/AssetWrapper";
import FormattedAsset from "Utility/FormattedAsset";

class AccountAssetContent extends React.Component {

    render() {
        return (
            <div className={"account-asset-content"}>
                <div className={"account-asset-wrapper"}>
                    <ContentTable {...this.props} />
                    <ContentInfo {...this.props}  />
                </div>
            </div>
        );
    }
}

const ContentHeader = () => {
    return (
        <div className={"account-asset-content-header"}>
            <Translate content="dashboard.price"/>
            <Translate content="dashboard.chanage_24"/>
            <Translate content="dashboard.chanage_24_low"/>
            <Translate content="dashboard.chanage_24_high"/>
            <Translate content="dashboard.current_volume"/>
            <Translate content="dashboard.chanage_24_volume"/>
        </div>
    );
};

const ContentBody = (props) => {
    const {currentStats, base_asset, quote_asset} = props;

    return (
        <div className={"account-asset-content-body"}>

            <div>
                {currentStats ? <MarketPrice
                    base={base_asset.get("symbol")}
                    quote={quote_asset.get("symbol")}
                    base_asset={base_asset}
                    quote_asset={quote_asset}
                    marketStats={currentStats} /> : "-"}

            </div>
            <div>
                {currentStats && currentStats.change ? currentStats.change + "%" : "-"}
            </div>
            <div >
                {currentStats && currentStats.volumeLowest ? currentStats.volumeLowest : "-"}
            </div>
            <div >
                {currentStats && currentStats.volumeHighest ? currentStats.volumeHighest : "-"}
            </div>
            <CurrentSupplyWraper
                current_supply={props.current_supply}
                backingAsset={quote_asset.get("symbol")} />
            <div>
                {currentStats && currentStats.volumeBase ? utils.format_volume(currentStats.volumeBase, quote_asset.get("precision")) : "-"}
            </div>
        </div>
    );
};
const ContentFooter = (props) => {
    const {settings} = props;
    return (
        <div className={"account-asset-content-footer"}>
            <Translate content={"account.asset.display_currency"} />
            <span>{settings.get("unit")}</span>
            <ChangeDropDownCash prefixClassName={"account-asset-cash"} />
        </div>
    );
};

const ContentTable = (props) => {
    return (
        <div className={"account-asset-table"}>
            <ContentHeader />
            <ContentBody {...props}/>
            <ContentFooter {...props} />
        </div>
    );
};

const ContentInfo = (props) => {
    const {backedCoin, quote_asset, socialLinks} = props;
    let withdrawalStaticFee,
        depositStaticFee,
        depositPercentageFee,
        withdrawalPercentageFee;
    if(!backedCoin) return null;
    let withdrawalAllowed = backedCoin.withdrawalAllowed,
        depositAllowed = backedCoin.depositAllowed;


    const getAsset = (amount) => {
        return new Asset({
            asset_id: quote_asset.get("id"),
            precision: quote_asset.get("precision"),
            amount: amount || 0
        });
    };

    //console.log("socialLinks", socialLinks)


    let nullAsset = getAsset(0);
        depositPercentageFee = Number(backedCoin.depositPercentageFee);
        depositStaticFee = getAsset(backedCoin.depositStaticFee);

        withdrawalStaticFee = getAsset(backedCoin.withdrawalStaticFee);
        withdrawalPercentageFee = Number(backedCoin.withdrawalPercentageFee);

    const createSocialLink = (key, name, link) => {
        let hasLink = link.toString().indexOf("http") !== -1;
        let nameToIcon = {
            "website": "link-right",
            "explorer": "explore",
            "explorer2": "explore",
            "social": "social",
            "anonce": "notification",
            "chats": "chat",
            "docs": "dock",
            "desc": "description",
        };
        if( hasLink ) {
            return (<a key={key} target={"_blank"} href={link}>
                {/*<i className={name} />*/}
                <Icon name={nameToIcon[name] || "link-right"} />
            </a>);
        }
    };

    return (
        <div className={cn("account-asset-info", {"hide": !depositAllowed && !withdrawalAllowed })}>
            <div className={"account-asset-info-wrapper"}>
                {depositAllowed && backedCoin.depositMaxAmount ? <div className={"account-asset-info-line"}>
                    <Translate component={"div"} content={"gateway.info.depositMaxAmount"}/>

                    <FormattedAsset
                        amount={backedCoin.depositMaxAmount}
                        asset={backedCoin.symbol}
                    />
                </div> : null }
                {depositAllowed && backedCoin.depositMinAmount ? <div className={"account-asset-info-line"}>
                    <Translate component={"div"} content={"gateway.info.depositMinAmount"}/>
                    <FormattedAsset
                        amount={backedCoin.depositMinAmount}
                        asset={backedCoin.symbol}
                    />
                </div>: null }
                {depositAllowed && depositStaticFee.gt(nullAsset) ? <div className={"account-asset-info-line"}>
                    <Translate component={"div"} content={"gateway.info.depositPercentageFee"}/>
                    <FormattedAsset
                        amount={depositStaticFee.getAmount({real: false})}
                        asset={backedCoin.symbol}
                    />
                </div>: null }
                {depositAllowed && depositPercentageFee ? <div className={"account-asset-info-line"}>
                    <Translate component={"div"} content={"gateway.info.depositPercentageFee"}/>
                    {depositPercentageFee} %
                </div>: null }
                {withdrawalAllowed && backedCoin.withdrawMaxAmount ? <div className={"account-asset-info-line"}>
                    <Translate component={"div"} content={"gateway.info.withdrawMaxAmount"}/>
                    <FormattedAsset
                        amount={backedCoin.withdrawMaxAmount}
                        asset={backedCoin.symbol}
                    />
                </div>: null }
                {withdrawalAllowed && backedCoin.withdrawMinAmount ? <div className={"account-asset-info-line"}>
                    <Translate component={"div"} content={"gateway.info.withdrawMinAmount"}/>
                    <FormattedAsset
                        amount={backedCoin.withdrawMinAmount}
                        asset={backedCoin.symbol}
                    />
                </div>: null }
                {withdrawalAllowed && withdrawalStaticFee.gt(nullAsset) ? <div className={"account-asset-info-line"}>
                    <Translate component={"div"} content={"gateway.info.withdrawalPercentageFee"}/>
                    <FormattedAsset
                        amount={withdrawalStaticFee.getAmount({real: false})}
                        asset={backedCoin.symbol}
                    />
                </div>: null }
                {withdrawalAllowed && withdrawalPercentageFee ? <div className={"account-asset-info-line"}>
                    <Translate component={"div"} content={"gateway.info.withdrawalPercentageFee"}/>
                    {withdrawalPercentageFee} %
                </div>: null }
            </div>
            {Object.keys(socialLinks).length ? <div className={"account-asset-info-wrapper social-links"}>
                {Object.keys(socialLinks).map(links => {
                    if( socialLinks[links] ){
                        if( typeof socialLinks[links] === "object" ) {
                            return socialLinks[links].map((link, index) => {
                                return createSocialLink(links + index, links, link);
                            });
                        } else {
                            return  createSocialLink(links, links, socialLinks[links]);
                        }
                    }
                }) }
            </div> : null}
        </div>
    );
};

class CurrentSupply extends React.Component {

    render() {
        const {backingAsset, current_supply}= this.props;
        let dynamic = this.props.getDynamicObject(backingAsset.get("dynamic_asset_data_id"));
        if (!dynamic) return <div>-</div>;
        dynamic = dynamic.toJS();
        if( dynamic.id === "2.3.0" ) {
            dynamic.current_supply = current_supply;
        }
        return (
            <FormattedAsset
                component={"div"}
                amount={dynamic.current_supply}
                asset={backingAsset.get("id")}
            />
        );
    }

}

const CurrentSupplyWraper = AssetWrapper(CurrentSupply, {
    propNames: ["backingAsset"],
    withDynamic: true
});

export default AccountAssetContent;

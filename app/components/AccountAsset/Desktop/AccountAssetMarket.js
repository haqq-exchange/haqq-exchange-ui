import React, {useState} from "react";
import {Link} from "react-router-dom";
import cn from "classnames";
import Translate from "react-translate-component";
import SettingsActions from "actions/SettingsActions";
import ClickOutside from "react-click-outside";
import SettingsStore from "stores/SettingsStore";
import ModalActions from "actions/ModalActions";
import ImageLoad from "Utility/ImageLoad";
import {getAlias} from "config/alias";

class AccountAssetMarket extends React.Component {

    constructor(){
        super();
        this.state = {
            hasShowMarketTabs: false
        };
    }

    componentDidMount(){
        const {assetMarketTab, unit, quote_asset} = this.props;
        let symbol = quote_asset.get("symbol");
        if(!assetMarketTab.has(symbol)) {
            /*let unitFilter = unit.filter(data => data !== quote_asset.get("symbol"));
            console.log("unitFilter", unitFilter);
            unitFilter.splice(0,3);
            console.log("unitFilter", unitFilter);*/
            console.log("unit", unit);
            this.changeAssetMarket(unit, true);
        }
    }


    componentDidUpdate(prevProps) {
        const {quote_asset, unit} = this.props;
        // const {quote_asset} = prevProps;
        if( prevProps.quote_asset.get("id") !== quote_asset.get("id") ) {
            this.changeAssetMarket(unit.filter(data => data !== quote_asset.get("symbol")));
        }
    }


    changeAssetMarket = (unit, didMount ) => {
        let unitArray = unit.toJS ? unit.toJS() : unit;
        const {quote_asset} = this.props;
        unitArray = unitArray.filter(data => data !== quote_asset.get("symbol") );
        if( didMount ) {
            unitArray = unitArray.splice(0,4);
        }
        SettingsActions.changeAssetMarketTab(quote_asset.get("symbol"), unitArray  );
    };

    setShowMarketTabs = (hasShowMarketTabs) => {
        this.setState({hasShowMarketTabs});
    };

    render() {
        const {hasShowMarketTabs} = this.state;
        const {assetMarketTab, base_asset, quote_asset, allMarketStats} = this.props;
        const symbol = quote_asset.get("symbol");
        const marketTabs = assetMarketTab.get(quote_asset.get("symbol"));
        //const [hasShowMarketTabs, setShowMarketTabs] = useState(false);
        if( !marketTabs ) return null;
        // console.log("assetMarketTab", assetMarketTab);
        // console.log("assetMarketTab quote_asset", quote_asset);
        // console.log("assetMarketTab allMarketStats", hasShowMarketTabs);
        return (
            <div className={cn("account-asset-market-tab", { showMarketTabs: hasShowMarketTabs })}>
                {__SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__ ? null :
                <div className={"account-asset-market-tab-title"}>
                    <Translate
                        content={"account.asset.title_market"}
                        component={"div"}
                    />

                    <Translate
                        content={__SCROOGE_CHAIN__ ? "account.asset.deex_fiat_scrooge" : "account.asset.deex_fiat"}
                        component={"span"}
                        /*href={"https://cryptoplat.io/deex"}
                        target={"_blank"}*/
                        onClick={()=>ModalActions.show("cryptoplat_io_buy")}
                        className={"btn btn-blue"}
                    />
                    <Translate
                        content={__SCROOGE_CHAIN__ ? "account.asset.deex_kran_scrooge" : "account.asset.deex_kran"}
                        component={"a"}
                        href={"https://deex4free.com"}
                        target={"_blank"}
                        className={"btn btn-blue"}
                    />
                </div>}
                <div className={"account-asset-market-tab-wrapper"}>
                    {marketTabs.map(asset => {
                        return <MarketTabs
                            key={`market-tabs-${asset}`}
                            asset={asset}
                            marketTabs={marketTabs}
                            stats={allMarketStats.get([symbol, asset].join("_"))}
                            quote_asset={quote_asset}
                            changeAssetMarket={this.changeAssetMarket}
                        />;
                    })}
                    {!hasShowMarketTabs && <AddMarketTabs setShowMarketTabs={this.setShowMarketTabs} />}
                    {hasShowMarketTabs && <ShowMarketTabs
                        marketTabs={marketTabs}
                        changeAssetMarket={this.changeAssetMarket}
                        setShowMarketTabs={this.setShowMarketTabs} />}
                </div>

            </div>
        );
    }
}

const ShowMarketTabs = (props) => {
    let [filter, setFilter] = useState("");
    let {marketTabs} = props;
    let allMarkets = SettingsStore.getState().defaultMarkets,
        uniqueQuote = {};
    allMarkets.map(item => {
        if( props.marketTabs.indexOf(item.quote) === -1 ) {
            if( filter ) {
                if( item.quote.toLowerCase().indexOf(filter) !== -1 ){
                    uniqueQuote[item.quote] = true;
                }
            } else {
                uniqueQuote[item.quote] = true;
            }
        }
        return item.base;
    });

    const addMarketTabs = add => {
        let marketTabsJs = marketTabs.toJS();
        marketTabsJs.push(add);
        props.changeAssetMarket(marketTabsJs);
        props.setShowMarketTabs(false);
    };

    // let defaultMarkets = SettingsStore.getState().defaultMarkets;
    // console.log("defaultMarkets", defaultMarkets.toJS());
    return (
        <ClickOutside className={"account-asset-market-tab-show"} onClickOutside={() => props.setShowMarketTabs(false)}>
            <span className={"account-asset-market-tab-close"} onClick={() => props.setShowMarketTabs(false)}><i /></span>
            <input
                type="text" className={"account-asset-market-tab-input"}
                onChange={(event) => setFilter(event.target.value.toLowerCase())}/>
            <div className={"account-asset-market-tab-drop-down"}>
                {Object.keys(uniqueQuote).sort().map((quote, index) => {
                    return <div key={`market-tab-show-${quote + index}`} onClick={()=>addMarketTabs(quote)}>
                        <ImageLoad customStyle={{
                            maxWidth: 25,
                            marginRight: 5
                        }} imageName={`${quote.toLowerCase()}.png`} />
                        {getAlias(quote)}
                    </div>;
                })}
            </div>
        </ClickOutside>);
};

const AddMarketTabs = (props) => {
    return (<div className={"account-asset-market-tab-add"}>
        <span onClick={()=>props.setShowMarketTabs(true)}>+</span>
    </div>);
};

const MarketTabs = ({asset, quote_asset, stats, marketTabs, changeAssetMarket}) => {
    const qb = [quote_asset.get("symbol"), asset];

    const removeAssetMarket = asset_name => {
        let indexAsset = marketTabs.indexOf(asset_name);
        marketTabs = marketTabs.delete(indexAsset);
        changeAssetMarket(marketTabs);
    };

    let styleChange = cn("stats-change", {
        "positive": stats && stats.change > 0,
        "negative": stats && stats.change < 0,
    });

    return (
        <div className={"account-asset-market-tab-item"}>
            <del className={"account-asset-market-tab-close"} onClick={() => removeAssetMarket(asset)}><i /></del>
            <Link to={["/market", qb.join("_")].join("/")}>
                {qb.map(asset=>getAlias(asset)).join("/")} <span className={styleChange}>{stats && stats.change || "0.00"}%</span>
            </Link>
        </div>
    );
};


export default AccountAssetMarket;
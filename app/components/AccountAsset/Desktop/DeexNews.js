import React from "react";
import Translate from "react-translate-component";
import Immutable from "immutable";
import WPAPI from "wpapi";
import {FormattedDate} from "react-intl";
import utils from "lib/common/utils";

class DeexNews extends React.Component {

    static defaultProps = {
        timeDuration: 30,
        tags: {
            en: 14,
            ru: 15,
            tr: 16,
        }
    };

    constructor(props){
        super(props);

        this.state = {
            showPosts: false,
            lastUpdate: null,
            posts_media: Immutable.Map(),
            postList: Immutable.Map()
        };
    }


    componentDidMount() {
        this.createWpUrl()
            .then(this._getTags)
            .then(this._getLoadedPosts);
    }


    componentDidUpdate(prevProps) {
        //console.log("prevProps.settings, this.props.settings", prevProps.settings, this.props.settings);
        if( !utils.are_equal_shallow(prevProps.settings, this.props.settings) ) {
            this.createWpUrl()
                .then(this._getTags)
                .then(this._getLoadedPosts);
        }
    }


    shouldComponentUpdate(prevProps, prevState){
        return !utils.are_equal_shallow(prevProps.settings, this.props.settings) ||
            !utils.are_equal_shallow(prevState.postList , this.state.postList) ||
            !utils.are_equal_shallow(prevState.posts_media , this.state.posts_media);
    }

    createWpUrl = () => {
        const locale = this.props.settings.get("locale");
        return new Promise(resolve=>{
            let endPoint = ["https://deex.blog", "wp-json"];

            if(locale!== "ru") {
                endPoint.splice(1, 0, locale);
            }
            this.WP = new WPAPI({
                endpoint: endPoint.join("/")
            });
            resolve();
        });
    };

    _getTags = () => {
        const {quote_asset, settings} = this.props;
        return new Promise((resolve, reject)=>{

            this.WP.tags().slug([quote_asset && quote_asset.get("symbol").toLowerCase() || "deex", settings.get("locale")].join("_")).then(result=>{
                if( result ) {
                    resolve(result.pop());
                } else {
                    reject();
                }
            });
        });
    };

    _getLoadedPosts = (tags) => {
        const _this = this;
        const locale = this.props.settings.get("locale");
        let { postList } = this.state;
        return new Promise(resolve=>{
            this.WP
                .posts()
                .tags(tags && tags.id || "")
                .perPage(3)
                .then(result => {
                    postList.clear();
                    postList = postList.set(locale, Immutable.fromJS(result));

                    _this.setState({
                        postList,
                        lastUpdate: new Date()
                    }, () => _this._getLoadedFeatureMedia(result));
                    resolve(result);
                });
        });
    };

    _getLoadedFeatureMedia = (result) => {
        const _this = this;
        let { posts_media } = this.state;
        return result.map(data => {
            if( data.featured_media ) {
                this.WP
                    .media()
                    .id(data.featured_media)
                    .then(media => {
                        posts_media = posts_media.set(data.featured_media, Immutable.fromJS(media));
                        _this.setState({posts_media});
                    });
            }
        });
    };

    render() {
        let { settings } = this.props;
        const locale = settings.get("locale");
        let { postList , posts_media} = this.state;

        if( !postList.has(locale) ) return <LoadedNews />;

        return (
            <div className={"account-asset-news"}>
                {/*<Translate />*/}
                <Translate content={"account.asset.title_news"} className={"account-asset-news-title"} component={"div"}  />
                <div className="account-asset-wrapper">
                    <div className="account-asset-posts">
                        {postList.get(locale).map(post=>{
                            let fragmentPost = post.getIn(["content", "rendered"]).split("<!--more-->");
                            /* получаем медиа контент */
                            let featured_media = posts_media && posts_media.get(post.get("featured_media"));
                            /* ссылка */
                            const postLink = (link, children, props = {}) => {
                                return (<a href={link} target={"_blank"} {...props} >{children}</a>);
                            };
                            return (
                                <div key={`news-post-${post.get("id")}`} className="account-asset-posts-item" >
                                    {featured_media
                                        ? postLink(post.get("link"), (<img src={featured_media.getIn(["media_details", "sizes", "thumbnail", "source_url"])} alt=""/>), {
                                            className: "account-asset-posts-image"
                                        })
                                        : (<span className={"account-asset-posts-empty-image"} />) }

                                    <div  className="account-asset-posts-content"  >
                                        <div className="account-asset-posts-title" >
                                            {postLink(post.get("link"), <span dangerouslySetInnerHTML={{__html: post.getIn(["title", "rendered"]) }} /> )}
                                        </div>
                                        <div className="account-asset-posts-date" >
                                            <FormattedDate value={post.getIn(["date"])} />
                                        </div>

                                        <div className="account-asset-posts-body" dangerouslySetInnerHTML={{__html: fragmentPost.shift()}} />

                                        {postLink(post.get("link"), <Translate content={"account.asset.more_detailed"}  /> , {
                                            className: "btn btn-gray"
                                        } )}
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                </div>
            </div>
        );
    }
}

const LoadedNews = () => {
    return (
        <div className={"account-asset-news"}>
            {/*<Translate />*/}
            <Translate content={"account.asset.title_news"} className={"account-asset-news-title"} component={"div"}  />
            <div className="account-asset-wrapper">
                <div className="account-asset-posts-empty">
                    {[1,2,3].map(post=>{
                        return (
                            <div key={`news-post-${post}`} className="account-asset-posts-empty-item" >
                                <span className={"account-asset-posts-empty-image"} />

                                <div  className="account-asset-posts-empty-content"  >
                                    <div className="account-asset-posts-empty-title" />
                                    <div className="account-asset-posts-empty-date" />
                                    <div className="account-asset-posts-empty-body" />
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        </div>
    );
};



export default DeexNews;
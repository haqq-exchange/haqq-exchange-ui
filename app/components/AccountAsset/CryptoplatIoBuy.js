import React from "react";
// import PropTypes from "prop-types";
// import Translate from "react-translate-component";
import DefaultModal from "components/Modal/DefaultModal";
import { debounce, isNaN } from "lodash-es";
import {Form, Input } from "antd";
import { FetchChain } from "deexjs";

// import {getRequestAddress} from "api/apiConfig";
// import RefUtils from "common/ref_utils";

// import ls from "common/localStorage";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";

// import "antd/lib/form/style/index.css";
// import "antd/lib/input/style/index.css";

class CryptoplatIoBuy extends DefaultBaseModal {


    constructor(props) {
        super(props);
        this.state = {
        };
    }


    afterOpenModal = () => {};

    afterCloseModal = () => {};

    closeModal = () => {
        const {resolve, modalId} = this.props;
        ModalActions.hide(modalId)
            .then(resolve);
    };


    nextProcess = () => {

    };

    render() {
        const {isOpenModal} = this.state;
        const {modalId, modalIndex } = this.props;


        return (
            <DefaultModal
                id={modalId}
                modalId={modalId}
                ref={(ref) => this.RefUnlockModal = ref}
                isOpen={isOpenModal}
                className="UnlockModal cryptoplatio-modal"
                onAfterOpen={this.afterOpenModal}
                shouldCloseOnOverlayClick={false}
                shouldCloseOnEsc={false}
                customStyle={{zIndex: modalIndex[modalId]}}
                onRequestClose={this.closeModal}>
                <div className="modal fadeIn ng-enter is-active ng-enter-active">
                    <div className="public-login-modal">
                        <div className="modal-header">
                            <a onClick={this.closeModal} className="close-button">×</a>
                        </div>
                        <div className="modal-content">
                            <CryptoplatIoFormCreate account={this.props.account}/>
                        </div>
                    </div>
                </div>
            </DefaultModal>
        );
    }
}

class CryptoplatIoForm extends React.Component {

    static defaultProps = {
        tokenBalance: 2768,
        fixedCommission: 65,
        fiatPricePerOneToken: 0.927
    };

    constructor(props) {
        super(props);
        this.state = {
            succesShow: false,
            totalSum: 0,
            paramsForms: {}
        };

        this.validateAccount = debounce(this.validateAccount, 300);
    }

    componentDidMount() {
        this.getDefaultData();
        this.props.form.validateFields();
    }


    getDefaultData = () => {
        const _this = this;
        if(!__SCROOGE_CHAIN__) {
        fetch("https://cryptoplat.io/params", {})
            .then(result=>result.json())
            .then(result=>{
                _this.setState({paramsForms: result});
            });
        }
        
    };

    hasErrors = (fieldsError) => {
        return Object.keys(fieldsError).some(field => fieldsError[field]);
    };


    validateAccount = (rule, value, callback) => {
        FetchChain("getAccount", value).then(result => {
            if(!result){
                callback("Нет такого аккаунта");
            } else {
                callback();
            }
        });
    };

    validateAmount = (rule, value, callback) => {
        const {paramsForms} = this.state;
        const {DEEXAmountRemaining, fiatPricePerOneToken, fixedCommission} = paramsForms;
        let totalSum = 0;
        let hasTotalSum = true;
        if(isNaN(Number(value))) {
            callback("Это не число");
            hasTotalSum = false;
        } else if(Number(value) > DEEXAmountRemaining) {
            callback("Сумма должна быть не больше " + DEEXAmountRemaining);
            hasTotalSum = false;
        }

        totalSum = (fiatPricePerOneToken * value + fixedCommission).toFixed(2);

        this.setState({
            totalSum: hasTotalSum ? totalSum : 0
        });

        //form.validateFields(["amount"], { force: Number(value) });
        callback();
    };
    submitForm = (event) => {
        const _this = this;
        event.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.setState({
                    formPayOut: values,
                }, () =>{
                    new Promise(resolve=>{
                        resolve(_this.fromPayout.submit());
                    }).then(()=>{
                        _this.setState({
                            formPayOut: false,
                            succesShow: true
                        });
                    });
                })
            }
        });
    };

    getShowError = (name) => {
        const {isFieldTouched, getFieldError}= this.props.form;
        const hasValueError = isFieldTouched(name) && getFieldError(name);
        return {
            validateStatus: hasValueError ? "error" : "",
            help: hasValueError || ""
        };
    };

    render() {

        console.log("CryptoplatIoForm", this.props, this.state );

        const {totalSum, formPayOut, succesShow}= this.state;
        const {account, form}= this.props;
        const {getFieldDecorator, getFieldsError, getFieldValue}= form;

        let timeOptions = {
            year: "numeric", month: "numeric", day: "numeric",
            hour: "numeric", minute: "numeric", second: "numeric",
            hour12: false
        };
        let expiration = new Intl
            .DateTimeFormat("ru", timeOptions)
            .format(new Date());

        return (
            <div className={"cryptoplatio"}>
                {!succesShow ? <div className="cryptoplatio-wrap">
                    <h4 style={{textAlign: "center"}}>Внимание! Оплата ведется только по картам российских банков.</h4>
                    <Form onSubmit={this.submitForm} noValidate >
                        <Form.Item>
                            {getFieldDecorator("orderCreationTime", {
                                initialValue: expiration
                            })(<Input type={"hidden"} />)}
                        </Form.Item>
                        <Form.Item label="Электронный адрес" {...this.getShowError("email")}>
                            {getFieldDecorator("email", {
                                rules: [
                                    {
                                        type: "email",
                                        message: "The input is not valid E-mail!",
                                    },
                                    {
                                        required: true,
                                        message: "Please input your E-mail!",
                                    },
                                ],
                            })(<Input autoComplete={"email"} />)}
                        </Form.Item>
                        <Form.Item label="Телефон" {...this.getShowError("phone")}>
                            {getFieldDecorator("phone", {
                                rules: [
                                    {
                                        required: true,
                                        message: "Please your phone!",
                                    }
                                ],
                            })(<Input addonBefore={<span className={"cryptoplatio-form-group-addon-before"}>+7</span>} autoComplete={"phone"} />)}
                        </Form.Item>


                        <Form.Item label="Имя deex-аккуанта" {...this.getShowError("account")}>
                            {getFieldDecorator("account", {
                                initialValue: account && account.get("name") ,
                                rules: [
                                    {
                                        required: true,
                                        message: "Please your account name!",
                                    },
                                    {
                                        validator: this.validateAccount,
                                    },
                                ],
                            })(<Input autoComplete={"off"} />)}
                        </Form.Item>
                        <Form.Item label="Я хочу приобрести" {...this.getShowError("fiatAmtExpected")}>
                            {getFieldDecorator("fiatAmtExpected", {
                                rules: [
                                    {
                                        required: true,
                                        message: "Please your amount",
                                    },
                                    {
                                        validator: this.validateAmount,
                                    },
                                ],
                            })(<Input addonAfter={<span className={"cryptoplatio-form-group-addon-after"}>{__GBLTN_CHAIN__ ? "GBLTEST" : (__GBL_CHAIN__ ? "GBL" : (__SCROOGE_CHAIN__ ? "SCROOGE" : "DEEX"))}</span>} />)}
                        </Form.Item>


                        <div className={"cryptoplatio-form-group"}>
                            <div>Итого: {getFieldValue("fiatAmtExpected") ? totalSum : 0} р </div>
                            <div className={"cryptoplatio-form-group-btn"}>
                                <button type={"submit"} disabled={this.hasErrors(getFieldsError())} className={"btn btn-red"}>Купить</button>
                            </div>
                        </div>
                    </Form>

                    {formPayOut ? <form target="_blank" method={"post"} ref={ref=>this.fromPayout = ref} action="https://cryptoplat.io/viaExchange">
                        <input type="hidden" name={"email"} value={formPayOut.email}/>
                        <input type="hidden" name={"username"} value={formPayOut.account}/>
                        <input type="hidden" name={"phone"} value={["+7", formPayOut.phone].join("")}/>
                        <input type="hidden" name={"fiatCurrencyName"} value={"RUR"}/>
                        <input type="hidden" name={"tokenAmt"} value={formPayOut.fiatAmtExpected || 0}/>
                        <input type="hidden" name={"tokenName"} value={__GBL_CHAIN__ ? "GBL" : "DEEX"}/>

                    </form> : null}
                </div> : <SuccesShow />}
            </div>
        );
    }
}

const SuccesShow = () => {
    return (
        <div className="cryptoplatio-wrap">
            <div className="cryptoplatio-succes">
                Спасибо за покупку DEEX
            </div>
        </div>
    );
}

const CryptoplatIoFormCreate = Form.create()(CryptoplatIoForm);

CryptoplatIoBuy.defaultProps = {
    modalId: "cryptoplat_io_buy"
};

export default class CryptoplatIoBuyContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    modalIndex: () => ModalStore.getState().modalIndex,
                    resolve: () => ModalStore.getState().resolve
                }}
            >
                <CryptoplatIoBuy {...this.props} />
            </AltContainer>
        );
    }
}

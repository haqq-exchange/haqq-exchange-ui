import React from "react";
import PropTypes from "prop-types";
import cname from "classnames";

class LoadingIndicator extends React.Component {
    static propTypes = {
        type: PropTypes.string,
        loadingText: PropTypes.string
    };

    static defaultProps = {
        type: null,
        customStyle: {},
        loadingText: null
    };

    constructor(props) {
        super(props);
        this.state = {progress: 0};
    }

    render() {
        const {customStyle} = this.props;

        switch (this.props.type) {
            case "loaded-node":
                return (
                    <div className="content__body" style={customStyle}>
                        <span className="loaded-node maps1"></span>
                        <span className="loaded-node maps2"></span>
                        <span className="loaded-node maps3"></span>
                        <span className="loaded-node maps4"></span>
                        <span className="loaded-node maps5"></span>
                        <span className="loaded-node maps6"></span>
                        <span className="loaded-node maps7"></span>
                        <span className="loaded-node maps8"></span>
                        <span className="loaded-node maps9"></span>
                        <span className="loaded-node maps10"></span>
                        <span className="loaded-node maps11"></span>
                        <span className="loaded-node maps12"></span>
                    </div>
                );
            case "three-bounce":
                return (
                    <div className="three-bounce" style={customStyle}>
                        <div className="bounce1" />
                        <div className="bounce2" />
                        <div className="bounce3" />
                    </div>
                );
            case "circle":
                return (
                    <div className="circle-wrapper" style={customStyle}>
                        <div className="circle1 circle" />
                        <div className="circle2 circle" />
                        <div className="circle3 circle" />
                        <div className="circle4 circle" />
                        <div className="circle5 circle" />
                        <div className="circle6 circle" />
                        <div className="circle7 circle" />
                        <div className="circle8 circle" />
                        <div className="circle9 circle" />
                        <div className="circle10 circle" />
                        <div className="circle11 circle" />
                        <div className="circle12 circle" />
                    </div>
                );
            case "circle-small":
                return (
                    <div
                        className="circle-wrapper"
                        style={Object.assign({height: "15px", minHeight: "15px"}, customStyle)}
                    >
                        <div className="circle1 circle" />
                        <div className="circle2 circle" />
                        <div className="circle3 circle" />
                        <div className="circle4 circle" />
                        <div className="circle5 circle" />
                        <div className="circle6 circle" />
                        <div className="circle7 circle" />
                        <div className="circle8 circle" />
                        <div className="circle9 circle" />
                        <div className="circle10 circle" />
                        <div className="circle11 circle" />
                        <div className="circle12 circle" />
                    </div>
                );
            default:

                return (
                    <div className={cname("loading-overlay", {
                        "with-progress": this.progress > 0
                    })} style={customStyle} >
                        <div className="loading-panel">
                            {this.props.loadingText && (
                                <div
                                    className="text-center"
                                    style={{paddingTop: "10px"}}
                                >
                                    {this.props.loadingText}
                                </div>
                            )}
                            <div className="spinner">
                                <div className="bounce1" />
                                <div className="bounce2" />
                                <div className="bounce3" />
                            </div>
                            <div className="progress-indicator">
                                <span>{this.state.progress}</span>
                            </div>
                        </div>
                    </div>
                );
        }
    }
}

export default LoadingIndicator;

import React from "react";
import {connect} from "alt-react";
import AccountStore from "stores/AccountStore";
//import Translate from "react-translate-component";
//import TranslateWithLinks from "./Utility/TranslateWithLinks";
//import {isIncognito} from "feature_detect";
//var logo = require("assets/logo-ico-blue.png");
//import SettingsActions from "actions/SettingsActions";
//import WalletUnlockActions from "actions/WalletUnlockActions";
//import ActionSheet from "react-foundation-apps/src/action-sheet";
//import SettingsStore from "stores/SettingsStore";
//import IntlActions from "actions/IntlActions";
import {Route, Switch} from "react-router-dom";
import loadable from "loadable-components";

export const LoginBody = loadable(() => import("components/Public/Login/Body"));
export const LoginSelector = loadable(() => import("components/LoginSelector"));
export const LoginOldContainer = loadable(() => import("components/Account/Autorithation/LoginOld"));
export const Login2fa = loadable(() => import("components/Account/Autorithation/Login2fa"));
export const LoginLW = loadable(() => import("components/Account/Autorithation/LoginLW"));

class Autorithation extends React.Component {
    constructor(props) {
        super(props);

    }

    componentDidUpdate() {
    }

    render() {
        return (
            <div className="grid-block align-center">
                <LoginBody {...this.props}>
                    <Switch>
                        <Route path={"/authorization/login-2fa"} exact
                            render={() => <Login2fa {...this.props} />}
                        />

                        <Route
                            path={"/authorization/login"}
                            exact
                            render={() => <LoginOldContainer  {...this.props} />}
                        />
                        <Route
                            path={"/authorization/login-lw"}
                            exact
                            render={() => <LoginLW  {...this.props} />}
                        />
                    </Switch>
                </LoginBody>
            </div>
        );
    }
}

export default connect(
    Autorithation,
    {
        listenTo() {
            return [AccountStore];
        },
        getProps() {
            return {
                currentAccount:
                    AccountStore.getState().currentAccount ||
                    AccountStore.getState().passwordAccount
            };
        }
    }
);

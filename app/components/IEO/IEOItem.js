import React from "react";
import WPAPI from "wpapi";
import cn from "classnames";
import Immutable from "immutable";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";
import Icon from "Components/Icon/Icon";
import IEoTransfer from "./IEoTransfer";
import HelloIeoModal from "./HelloIeoModal";
import {getAlias} from "../../config/alias";
import utils from "../../lib/common/utils";
import ChainTypes from "Utility/ChainTypes";
import BindToChainState from "Utility/BindToChainState";
import {FetchChain} from "deexjs";
import {Asset} from "../../lib/common/MarketClasses";
import ImageLoad from "Utility/ImageLoad";
import ModalActions from "../../actions/ModalActions";

export default class IEOItem extends React.Component {

    static defaultProps = {
        tagsLocales: {
            en: 14,
            ru: 15,
            tr: 16,
        }
    };

    constructor(){
        super();

        this.state = {
            eioItem: null,
            showPosts: false,
            posts_media: {},
            info: null,
            postList: null
        };

        this.WP = new WPAPI({
            endpoint: "https://deex.blog/wp-json"
        });

    }

    componentDidMount() {
        this._getLoadedInfoProject();
        this._initNews();
        this._showInfoModal();
    }

    componentDidUpdate(prevProps) {
        if( prevProps.ieoList !== this.props.ieoList || prevProps.settings.get("locale")!== this.props.settings.get("locale")) {
            this._initNews();
        }
    }

    _showInfoModal = () => {
        const {currentAccount} = this.props;
        if(!currentAccount) {
            ModalActions.show("hello_ieo_modal", {}).then(()=>{});
        }
    };

    _initNews = () => {
        this.getEioItem()
            .then(this.createWpUrl)
            .then(this._getTags)
            .then(this._getLoadedPosts)
            .then(this._getLoadedFeatureMedia)
            .then(postList=>{
                this.setState({postList});
            }).catch(error=>{
                console.log("error", error);
            });
    };

    createWpUrl = () => {
        const locale = this.props.settings.get("locale");
        return new Promise(resolve=>{
            let endPoint = ["https://deex.blog", "wp-json"];
            if(locale!== "ru") {
                endPoint.splice(1, 0, locale);
            }

            this.WP = new WPAPI({
                endpoint: endPoint.join("/")
            });
            resolve();
        });
    };

    getEioItem = () => {
        const _this = this;
        return new Promise(resolve => {
            const {ieoList, token_name} = this.props;
            let eioItem = ieoList.find(eio => eio.token_name.toLowerCase() === token_name.toLowerCase());
            _this.setState({eioItem: eioItem}, resolve);
        });
    };

    _getTags = () => {
        const {token_name, settings} = this.props;
        return new Promise((resolve, reject)=>{
            this.WP.tags().slug([token_name, settings.get("locale")].join("_")).then(result=>{
                if( result ) {
                    resolve(result.pop());
                } else {
                    reject();
                }
            });
        });
    };

    _getLoadedPosts = (tags) => {
        const {token_name}= this.props;
        return new Promise((resolve, reject)=>{
            try {
                this.WP
                    .posts()
                    .tags(tags.id)
                    .tag(token_name)
                    .then(result => {
                        resolve(result);
                    });

            } catch (e) {
                reject();
            }
        });
    };

    _getLoadedFeatureMedia = (result) => {
        return new Promise(resolve=>{
            result.map(data => {
                if( data.featured_media ) {
                    this.WP
                        .media()
                        .id(data.featured_media)
                        .then(media => {
                            this.setState({
                                posts_media: Object.assign(this.state.posts_media , {[data.featured_media]: media})
                            });
                        });
                    return data;
                }
            });
            resolve(result);
        });
    };

    _getLoadedInfoProject = () => {
        const {presaleApi, token_name} = this.props;

        var myHeaders = new Headers();
        var myInit = {
            headers: myHeaders,
            mode: "cors"
        };

        fetch([presaleApi, "info?name=" + token_name].join("/"), myInit)
            .then(r=>r.json())
            .then((result) => {
                try {
                    const {currencies} = result.data.info;
                    result.data.currencies = currencies;
                    result.data.info.currencies = currencies.map(currenci => currenci.currency_code);
                    console.log(result.data);
                    this.setState({
                        data: result.data
                    });
                } catch (e) {}
            });

    };


    render() {
        const {currentAccount} = this.props;
        const {postList, posts_media, eioItem, data} = this.state;
        // console.log("this.state", this.state)
        console.log("currentAccount", currentAccount);
        const hasIeoFinishDate = eioItem && new Date(eioItem.ieo_finish_date) < new Date();
        return (
            <div className={"ieo-item"}>
                <TopItem {...this.props} eioItem={eioItem} />
                <div className="ieo-item-wrap">
                    <div className="ieo-item-content">
                        <AboutProject
                            {...this.props}
                            eioItem={eioItem}
                            settings={data ? data.settings : null}
                            info={data ? data.info : null} />
                        <NewsProject {...this.props} postList={postList} posts_media={posts_media} />
                    </div>
                    <div className="ieo-item-aside">
                        {!hasIeoFinishDate ? <EioChatOrTransfer  {...this.props} {...this.state}  /> : null}
                        <EioInfo eioItem={eioItem} {...this.state} settings={this.props.settings} />
                        <EioMessages {...this.state} {...this.props} />
                    </div>
                </div>
                <HelloIeoModal />
            </div>
        );
    }
}

const TopItem = props => {
    return (
        <div className={cn("ieo-item-top")}>
            <Link to={"/ieo"} className={"ieo-item-top-link"}>
                <Icon name={"arrow-left"} />
                <Translate content={"ieo.back_project"}/>
            </Link>
            {/*{props.eioItem && props.eioItem.url ? <Translate content={"ieo.private_cabinet"} className={"btn btn-blue"} component={"a"} href={ props.eioItem.url.indexOf("http") === -1 ? "http://" + props.eioItem.url : props.eioItem.url } target={"_blank"} /> : null}*/}
        </div>
    );
};

const EioInfo = props => {
    if( !props.data ) return null;
    const {settings} = props;
    const dataInfo = Immutable.fromJS(props.data.info);
    let remomveKeys = ["token_precision"];
    return (
        <div className={cn("ieo-item-info")}>
            <div className={cn("ieo-item-info-title")}>IEO</div>
            {Object.keys(props.data.info).map(typesName=>{
                if( remomveKeys.indexOf(typesName) === -1 ) {
                    try {

                        let hasArray = ["string", "number"].indexOf(typeof dataInfo.get(typesName)) !== -1;
                        let hasBool = ["boolean"].indexOf(typeof dataInfo.get(typesName)) !== -1;
                        let hasLink = dataInfo.hasIn([typesName, "href"]);
                        let hrefLink;
                        if(  hasLink ) {
                            let pathToHref = [typesName, "href", settings.get("locale")];
                            let pathToHrefAlt = [typesName, "href", "en"];
                            if ( dataInfo.hasIn(pathToHref) || dataInfo.hasIn(pathToHrefAlt) ) {
                                hrefLink = dataInfo.getIn(pathToHref, dataInfo.getIn(pathToHrefAlt));
                            } else {
                                hrefLink = dataInfo.getIn([typesName,"href"]);
                            }
                        }
                        return (
                            <div key={typesName} className={cn("ieo-item-info-row")}>

                                <Translate content={`ieo.info.${typesName}`}/>
                                {hasBool
                                    ? <Translate content={`ieo.info.${dataInfo.get(typesName).toString()}`}/>
                                    : (<span>{
                                        hasLink ? (<a target={"_blank"} href={hrefLink}>{dataInfo.getIn([typesName,"title"])}</a>)  :
                                            hasArray ? dataInfo.get(typesName).toString() :
                                                dataInfo.get(typesName)
                                                    .sort((a,b)=>getAlias(a) > getAlias(b) ? 0 : -1)
                                                    .map((item, index)=>{
                                                        return (
                                                            <span key={"item" + index}>{getAlias(item)}</span>
                                                        );
                                                    }) }</span>)}

                            </div>
                        );
                    } catch (e) {console.log("e", e);}
                }
            })}
        </div>
    );
};

const EioMessages = props => {
    if( !props.data || !props.eioItem ) return null;
    return (
        <div className={cn("ieo-item-links")}>
            <Translate content={"ieo.links_title"} component={"div"} name={props.eioItem.name} className={"ieo-item-links-title"}/>
            <div className={"ieo-item-links-wrap"}>
                {Object.keys(props.data.links).map((name, index)=>{
                    return (<a key={`links-${name}-${index}`} href={props.data.links[name]}>
                        <Icon name={name}/>
                    </a>);
                })}
            </div>
        </div>
    );
};

const EioChatOrTransfer = props => {
    if(!props.data) return null;
    const {currencies, info} = props.data;
    let assets_avalible = currencies.map(currenci=>currenci.currency_code);
    console.log("props.eioItem", props.eioItem)
    return (
        <IEoTransfer
            to_name={info.deposit_account}
            currencies={currencies}
            info={props.data.info}
            assets_avalible={assets_avalible || []}
            {...props.eioItem}
        />
    );
};

const NewsProject = props => {
    const {postList, posts_media} = props;

    if( !postList ) return null;

    return (
        <div className={cn("ieo-item-news")}>

            <Translate content={"ieo.news_title"} component={"div"} className={"ieo-item-news-title"}/>
            {postList.map(post=>{
                /* обрезаем */
                let fragmentPost = post.content.rendered.split("<!--more-->");
                /* получаем медиа контент */
                let featured_media = posts_media && posts_media[post.featured_media];
                /* ссылка */
                const postLink = (link, children) => {
                    return (<a href={link} target={"_blank"} className={"ieo-item-news-post-title"}>{children}</a>);
                };
                return (
                    <div key={post.id} className={"ieo-item-news-post"}>
                        {postLink(post.link, post.title.rendered)}
                        {featured_media && postLink(post.link, (<img src={featured_media.source_url} alt=""/>) )}
                        <div className={"ieo-item-news-post-text"} dangerouslySetInnerHTML={{__html: fragmentPost.shift()}} />
                    </div>
                );
            })}
        </div>
    );
};

const AboutProject = props => {
    return (
        <div className={cn("ieo-item-about", props.token_name)}>
            <div className={"ieo-item-about-bg"} style={{backgroundImage: "url("+ `https://static.haqq.exchange/images/ieo/${props.token_name}_about.png` +")"}} />
            {/*<SoftHardCap {...props} />*/}
            <Translate content={"ieo.about"} component={"div"} className={"ieo-item-about-title"}/>
            {props.token_name ? <Translate unsafe content={["ieo", props.token_name, "about"].join(".")} component={"div"} className={"ieo-item-about-text"}/> : null }
            {props.settings && props.settings.show_collection_progress ? <CollectedAssetBindWrapper {...props} /> : null}
        </div>
    );
};

const SoftHardCap = props => {
    const {info} = props;
    if( !info ) return  null;
    const onePercent = info.hard_cap / 100;
    const percentSoftCap = info.soft_cap / onePercent;
    // console.log("info", info);
    // console.log("onePercent", onePercent);
    // console.log("percentSoftCap", percentSoftCap);
    return (
        <div className={"ieo-progress"}>
            <div className={"ieo-progress-wrap"}>
                <div className={"ieo-progress-full"}>
                    0% <span /> 100%
                </div>
                <div className={"ieo-progress-collected"}>
                    <span className={"soft-cap"} style={{width: "15%"}}><span>15.0%</span></span>
                    <span className={"hard-cap active"} style={{width: "56%"}} ><span>55.0%</span></span>
                </div>
                <div className={"ieo-progress-softcap"}>
                    <span>softcap</span>
                </div>
            </div>
        </div>
    );
};

class CollectedAsset extends React.Component {
    static propTypes = {
        account: ChainTypes.ChainAccount.isRequired
    };

    constructor(props) {
        super(props);

        this.state= {
            backedCoinsAssets: Immutable.Map()
        };
    }

    componentDidMount() {
        this._getBalances();
    }


    _getBalances = () => {
        let { account, info }         = this.props;
        console.log("this.props", this.props);
        let { backedCoinsAssets }         = this.state;
        let account_balances;
        try {
            console.log("account", account);
            if(typeof account === "object") {
                account_balances = account.get("balances");
            }
            return new Promise(function(resolve) {
                //let coinsName = backedCoins.filter(a=> !a.deleted).map(coins=>coins.symbol);

                FetchChain("getAsset", info.currencies).then(assets => {
                    resolve(assets);
                });
            }).then(results=>{
                console.log("results", results);
                results.map(item=>{
                    if( item ) {
                        if(account_balances && account_balances.has(item.get("id")) ) {
                            backedCoinsAssets = backedCoinsAssets.set(item.get("id"), account_balances.get(item.get("id")));
                        } else {
                            backedCoinsAssets = backedCoinsAssets.set(item.get("id"), null);
                        }
                    }
                });
                return backedCoinsAssets;
            }).then(balances => {
                let balancesData = balances.toJS();
                Promise.all([
                    FetchChain("getAsset", Object.keys(balancesData)),
                    FetchChain("getObject", balances.toArray().filter(a=>a) )
                ]).then(result => {
                    let [assets, balanceObject] = result;
                    let assetBalance = {};
                    assets.map(asset => {
                        assetBalance[asset.get("id")] = asset;
                    });
                    balanceObject.map(data => {
                        if( data.has("balance") ) {
                            assetBalance[data.get("asset_type")] = data.mergeDeep(assetBalance[data.get("asset_type")]);
                        }
                    });
                    this.setState({
                        assetBalance
                    });
                });
            });
        } catch (e) {

        }
    };

    render () {
        const  {assetBalance} = this.state;
        if( !assetBalance ) return  null;
        return (
            <div className={"collected-asset"}>
                <Translate content={"ieo.collected"} component={"div"} className={"ieo-item-about-title"}/>
                <div className={"collected-asset-row"}>
                    {Object.values(assetBalance).sort().map(asset => {
                        let assetData = new Asset({
                            asset_id: asset.get("id"),
                            precision: asset.get("precision"),
                            amount: asset.has("balance") ? asset.get("balance") : 0,
                        });
                        return (
                            <div className="collected-asset-item" key={"collected-asset-" + asset.get("id")} >
                                <div className={"collected-asset-item-wrap"}>
                                    <ImageLoad
                                        customStyle={{maxWidth: 21, marginRight: 10, verticalAlign: "sub"}}
                                        imageName={`${asset.get("symbol").toLowerCase()}.png`}/>
                                    <div>{getAlias(asset.get("symbol"))}</div>
                                </div>
                                <span>{assetData.getAmount({real: true})}</span>
                            </div>
                        );
                    })}
                </div>
            </div>
        );
    }
}

const CollectedAssetBind = BindToChainState(CollectedAsset, {
    show_loader: true
});

class CollectedAssetBindWrapper extends React.Component {
    render() {
        if(!this.props.info) return  null;
        let account_name = this.props.info.deposit_account;

        return <CollectedAssetBind {...this.props} account={account_name} />;
    }
}

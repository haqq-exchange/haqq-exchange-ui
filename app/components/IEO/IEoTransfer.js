import React from "react";
// import ZfApi from "react-foundation-apps/src/utils/foundation-api";
import Translate from "react-translate-component";
import { FetchChain, ChainStore } from "deexjs";
import AmountSelector from "components/Utility/AmountSelector";
import AccountStore from "stores/AccountStore";
// import AccountSelector from "components/Account/AccountSelector";
import TransactionConfirmStore from "stores/TransactionConfirmStore";
import { Asset } from "common/MarketClasses";
import { debounce, isNaN } from "lodash-es";
import { checkBalance, checkFeeStatusAsync, shouldPayFeeWithAssetAsync } from "common/trxHelper";
import BalanceComponent from "components/Utility/BalanceComponent";
import AccountActions from "actions/AccountActions";
import utils from "common/utils";
import counterpart from "counterpart";
import { connect } from "alt-react";
import classnames from "classnames";
import PropTypes from "prop-types";
import BindToChainState from "Components/Utility/BindToChainState";
import SettingsStore from "stores/SettingsStore";
import assetConfig from "config/asset";
import {getAlias, getAssetsAlias} from "config/alias";
import WrapperAuth from "Utility/WrapperAuth";
import log from "loglevel";


class IEoTransfer extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    static defaultProps = {
        from_name: "",
        to_name: "",
        assets_avalible: [],
    };

    constructor(props) {
        super(props);
        this.state     = IEoTransfer.getInitialState(props);
        this.nestedRef = null;

        this.onTrxIncluded = this.onTrxIncluded.bind(this);

        this._updateFee      = debounce(this._updateFee.bind(this), 250);
        this._checkFeeStatus = this._checkFeeStatus.bind(this);
        this._checkBalance   = this._checkBalance.bind(this);
        this.toChanged       = this.toChanged.bind(this);
        this.onAmountChanged = this.onAmountChanged.bind(this);
        this.onFeeChanged    = this.onFeeChanged.bind(this);


    }

    static getInitialState(props) {

        let assetKey = SettingsStore.getState().starredKey;
        const DEEX_ID = assetConfig[assetKey].id;
        let asset     = ChainStore.getAsset(DEEX_ID);


        return {
            from_name: props.from_name,
            to_name: props.to_name,
            from_account: null,
            to_account: null,
            orig_account: null,
            amount: "",
            asset_id: null,
            asset: null,
            assets_avalible: props.assets_avalible,
            memo: "",
            error: null,
            knownScammer: null,
            amountChanged: false,
            propose: false,
            propose_account: "",
            feeAsset: asset,
            fee_asset_id: DEEX_ID,
            feeAmount: new Asset({
                amount: 0,
                asset_id: DEEX_ID
            }),
            feeStatus: {},
            maxAmount: false,
            hidden: false,
        };
    }

    componentDidMount(){
        this._initForm();
    }

    componentDidUpdate(){}

    onSubmit(e) {
        e.preventDefault();
        this.setState({ error: null });
        const {
            from_account,
            to_account,
            memo,
            asset,
            feeAsset,
            fee_asset_id} = this.state;
        let { amount }   = this.state;

        console.log("ОНОООО", to_account)


        const sendAmount = new Asset({
            real: amount,
            asset_id: asset.get("id"),
            precision: asset.get("precision")
        });

        this.setState({ hidden: true });

        AccountActions.transfer(
            from_account.get("id"),
            to_account.get("id"),
            sendAmount.getAmount(),
            asset.get("id"),
            memo ? new Buffer(memo, "utf-8") : memo,
            null,
            feeAsset ? feeAsset.get("id") : fee_asset_id
        )
            .then(() => {
                this.props.onShowForm({});
                TransactionConfirmStore.unlisten(this.onTrxIncluded);
                TransactionConfirmStore.listen(this.onTrxIncluded);
            })
            .catch(e => {
                let msg = e.message ? e.message.split("\n")[1] || e.message : null;
                console.log("error: ", e, msg);
                this.setState({ error: msg });
            });

    }

    _initForm () {
        const _this = this;
        let { to_name, from_name , currentAccount, asset_id , assets_avalible, nft, deposit_account} = this.props;

        if (to_name && to_name !== from_name) {
            if(nft) {
                console.log("deposit_account", deposit_account)
                FetchChain("getAccount", deposit_account)
                    .then((_account) => {
                        if( _account ) {
                            _this.setState({
                                to_name: _account.get("name"),
                                to_account: _account
                            });
                        }
                    });
            } else {FetchChain("getAccount", to_name, undefined, {
                [to_name]: false
                }).then((_account) => {
                    if( _account ) {
                        _this.setState({
                            to_name: _account.get("name"),
                            to_account: _account
                        });
                    }
                });
            }
        }

        if (from_name || currentAccount) {
            FetchChain("getAccount", from_name || currentAccount, undefined, {
                [from_name]: false
            }).then((_account) => {
                _this.setState({
                    from_name: _account.get("name"),
                    from_account: _account
                });
            });
        }

        if (!this.state.from_name) {
            this.setState({ from_name: currentAccount });
        }

        if( assets_avalible.length ) {
            FetchChain("getAsset", assets_avalible).then(result => {
                try {
                    let chain_assets_avalible = {};
                    result.map(asset => {
                        if( asset ) {
                            chain_assets_avalible[asset.get("id")] = asset;
                        }
                    });

                    console.log("chain_assets_avalible", chain_assets_avalible )

                    this.setState({chain_assets_avalible});
                } catch (err) {

                }
            });
        }

        if (
            this.props.asset_id &&
            this.state.asset_id !== this.props.asset_id
        ) {

            FetchChain("getAsset", asset_id, undefined, {})
                .then(_asset => {
                    if (_asset) {
                        this.setState({
                            asset_id: asset_id,
                            asset: _asset
                        });
                    }
                });
        }
    }

    // shouldComponentUpdate(np, ns) {
    // if (ns.open && !this.state.open) this._checkFeeStatus(ns);
    // if (!ns.open && !this.state.open) return false;
    // return true;
    // }

    UNSAFE_componentWillReceiveProps(np) {
        if (
            np.currentAccount !== this.state.from_name &&
            np.currentAccount !== this.props.currentAccount ||
            np.asset_id !== this.props.asset_id
        ) {
            this.setState(
                {
                    from_name: np.from_name,
                    from_account: ChainStore.getAccount(np.from_name),
                    to_name: np.to_name ? np.to_name : "",
                    to_account: np.to_name ? ChainStore.getAccount(np.to_name) : null,
                    feeStatus: {},
                    fee_asset_id: this.state.fee_asset_id,
                    feeAmount: new Asset({ amount: 0 })
                },
                () => {
                    this._initForm();
                    this._updateFee();
                    this._checkFeeStatus();
                }
            );
        }
    }

    _checkBalance(hasMemo = true) {
        const { feeAmount, amount, from_account, asset } = this.state;
        if (!asset || !from_account) return;

        if(hasMemo) this._setCurrencyMemo();
        /*if( !amountChanged && amount ) {
        } else {
            this.onMemoChanged(memo);
        }*/
        // this._updateFee();

        const balanceID    = from_account.getIn(["balances", asset.get("id")]);
        const feeBalanceID = from_account.getIn([
            "balances",
            feeAmount.asset_id
        ]);
        if (!asset || !from_account) return;
        if (!balanceID)
            return this.setState({
                balanceError: true,
                typeBalanceError: "balanceID"
            });
        let balanceObject    = ChainStore.getObject(balanceID);
        let feeBalanceObject = feeBalanceID ? ChainStore.getObject(feeBalanceID) : null;
        if (!feeBalanceObject)
            return this.setState({
                balanceError: true,
                typeBalanceError: "feeBalance"
            });
        if (!balanceObject || !feeAmount) return;
        if (!amount)
            return this.setState({
                balanceError: false
            });
        const hasBalance = checkBalance(
            amount,
            asset,
            feeAmount,
            balanceObject
        );

        if (hasBalance === null) return;
        this.setState({ balanceError: !hasBalance });
    }

    _setCurrencyMemo = () => {
        const { amount, asset } = this.state;
        const { currencies } = this.props;

        let exchange_rate = currencies
            .map(curr=>curr.currency_code === asset.get("symbol") && curr.exchange_rate.toString())
            .filter(a => a)
            .shift();


        if( Number(exchange_rate) && amount ) {

            let amounMeno = amount / Number(exchange_rate);

            if( amounMeno ) {
                try {
                    let memoAsset = new Asset({
                        asset_id: 0,
                        amount: amounMeno,
                        real: amounMeno,
                        precision: 5
                    });


                    this.setState({
                        memo: memoAsset.getAmount({real: true}).toString()
                    }, this._updateFee);
                } catch (e) {
                }
            }



        } else if (!amount) {
            // if(this.props.nft) {
            //     this.setState({
            //         memo: "1"
            //     }, this._updateFee);
            // }
            // else {
                this.setState({
                    memo: ""
                }, this._updateFee);
            // }
        }
    };

    _checkFeeStatus(state = this.state) {
        let { from_account } = state;
        if (!from_account) return;

        const assets  = Object.keys(from_account.get("balances").toJS()).sort(
            utils.sortID
        );
        let feeStatus = {};
        let p         = [];
        assets.forEach(a => {
            p.push(
                checkFeeStatusAsync({
                    accountID: from_account.get("id"),
                    feeID: a,
                    options: ["price_per_kbyte"],
                    data: {
                        type: "memo",
                        content: this.state.memo
                    }
                })
            );
        });
        Promise.all(p)
            .then(status => {
                assets.forEach((a, idx) => {
                    feeStatus[a] = status[idx];
                });
                if (!utils.are_equal_shallow(this.state.feeStatus, feeStatus)) {
                    this.setState({
                        feeStatus
                    });
                }
                this._checkBalance();
            })
            .catch(err => {
                console.error(err);
            });
    }

    _setTotal(asset_id, balance_id) {
        const _this = this;
        const { feeAmount } = this.state;
        //debugger;
        // let balanceObject   = ChainStore.getObject(balance_id);
        // let transferAsset   = ChainStore.getObject(asset_id);

        FetchChain("getObject", [asset_id,balance_id], undefined, {}).then(data=> {
            let balanceObject   = data.get(1);
            let transferAsset   = data.get(0);
            let balance;
            balance = new Asset({
                amount: balanceObject.get("balance"),
                asset_id: transferAsset.get("id"),
                precision: transferAsset.get("precision")
            });

            if (balanceObject) {
                if (feeAmount.asset_id === balance.asset_id) {
                    balance.minus(feeAmount);
                }
                _this.setState({
                    maxAmount: true,
                    amount: balance.getAmount({ real: true })
                },  _this._checkBalance);
            }
        });
    }

    _getAvailableAssets(state = this.state) {
        const { from_account, from_error, fee_asset_id, chain_assets_avalible } = state;
        let asset_types                    = [],
            letDeexId                    = fee_asset_id,
            fee_asset_types              = [letDeexId];
        if (!(from_account && from_account.get("balances") && !from_error)) {
            return { asset_types, fee_asset_types };
        }
        //let account_balances = state.from_account.get("balances").toJS();
        asset_types = Object.keys(chain_assets_avalible || []);
        // asset_types          = Object.keys(account_balances).sort(utils.sortID);
        //asset_types          = array_chain_assets_avalible;

        // debugger;

        /*for (let key in account_balances) {
            if (account_balances.hasOwnProperty(key)) {
                let balanceObject = ChainStore.getObject(account_balances[key]);
                if (balanceObject && balanceObject.get("balance") === 0 ) {
                    asset_types.splice(asset_types.indexOf(key), 1);
                }
            }

            if( array_chain_assets_avalible.indexOf(key) === -1
                && asset_types.indexOf(key) !== -1
            ) {
                asset_types.splice(asset_types.indexOf(key), 1);
            }
        }*/


        return {
            asset_types,
            fee_asset_types
        };
    }

    _updateFee(state = this.state) {
        console.log("_UPDATEFEE !!!!");
        let { fee_asset_id, from_account, asset_id } = state;
        const { fee_asset_types } = this._getAvailableAssets(state);
        if (
            fee_asset_types.length === 1 &&
            fee_asset_types[0] !== fee_asset_id
        ) {
            fee_asset_id = fee_asset_types[0];
        }
        if (!from_account) return null;
        //debugger;

        checkFeeStatusAsync({
            accountID: from_account.get("id"),
            feeID: fee_asset_id,
            options: ["price_per_kbyte"],
            data: {
                type: "memo",
                content: state.memo
            }
        }).then(({ fee, hasBalance, hasPoolBalance }) => {
            shouldPayFeeWithAssetAsync(from_account, fee).then(
                should =>
                    should ? this.setState(
                        {
                            fee_asset_id: asset_id,
                            hasPoolBalance,
                            hasBalance
                        },
                        this._updateFee
                    ) : this.setState({
                        feeAmount: fee,
                        fee_asset_id: fee.asset_id,
                        hasBalance,
                        hasPoolBalance,
                        error: !hasBalance || !hasPoolBalance
                    })
            );
        });
    }

    setNestedRef(ref) {
        this.nestedRef = ref;
    }

    toChanged(to_name) {
        this.setState({ to_name, error: null });
    }


    onAmountChanged({ amount, asset }) {
        if (!asset) {
            return;
        }
        console.log("amount, asset", amount, asset);
        this.setState({
            amount,
            asset,
            asset_id: asset.get("id"),
            error: null,
            maxAmount: false,
            amountChanged: true,
        }, this._checkBalance);
    }

    onFeeChanged({asset}) {
        this.setState({
            feeAsset: asset,
            fee_asset_id: asset.get("id"),
            error: null
        }, this._updateFee);
    }

    onMemoChanged(value) {
        const { asset } = this.state;
        const { currencies , info, nft } = this.props;

        let exchange_rate = currencies
            .map(curr=>curr.currency_code === asset.get("symbol") && curr.exchange_rate)
            .filter(a => a)
            .shift();

        if( exchange_rate ) {
            try {
                let amountAsset = new Asset({
                    asset_id: asset.get("id"),
                    amount: Number(value) * exchange_rate,
                    real: Number(value) * exchange_rate,
                    precision: asset.get("precision")
                });

                let getAmount = amountAsset.getAmount({real: true});
                // console.log("value", value, info , amountAsset);
                // console.log("value", amountAsset.getAmount({real: true}));
                if(nft) {
                    this.setState({
                        memo: "1",
                        amount: getAmount || 0
                    }, () => {
                        //this._updateFee();
                        this._checkBalance(false);
                    });
                } else {
                    this.setState({
                        memo: value,
                        amount: getAmount || 0
                    }, () => {
                        //this._updateFee();
                        this._checkBalance(false);
                    });
                }

            } catch (e) {
                console.log("e", e);
            }
        }
    }

    onTrxIncluded(confirm_store_state) {
        if (
            confirm_store_state.included &&
            confirm_store_state.broadcasted_transaction
        ) {
            // this.setState(Transfer.getInitialState());
            TransactionConfirmStore.unlisten(this.onTrxIncluded);
            TransactionConfirmStore.reset();
        } else if (confirm_store_state.closed) {
            TransactionConfirmStore.unlisten(this.onTrxIncluded);
            TransactionConfirmStore.reset();
        }
    }

    render() {
        const {passwordAccount, token_name, info} = this.props;

        if( !passwordAccount ) return null;

        let {
            propose,
            from_account,
            to_account,
            asset,
            asset_id,
            feeAmount,
            amount,
            memo,
            feeAsset,
            fee_asset_id,
            balanceError,
            hasPoolBalance,
            hasBalance} = this.state;

        // debugger;

        let { asset_types, fee_asset_types } = this._getAvailableAssets();
        let balance = null;
        let balance_fee = null;

        // console.log("this.props >>>>>", this.props);
        // console.log("this.state >>>>>", this.state);
        // Estimate fee
        let fee = feeAmount.getAmount({ real: true });
        if (from_account && from_account.get("balances") ) {
            //debugger;
            let account_balances = from_account.get("balances").toJS();
            let _error           = balanceError ? "has-error" : "";
            if (asset_types.length === 1)
                asset = ChainStore.getAsset(asset_types[0]);
                let current_asset_id = asset ? asset.get("id") : asset_types[0];
                let feeID            = feeAsset ? feeAsset.get("id") : fee_asset_id;
            if (asset_types.length > 0 && account_balances[current_asset_id] ) {

                balance = (
                    <span className={"send-transfer-balance"}>
                        <Translate component="span" content="transfer.available"/>
                        :{" "}
                        <span className={classnames("send-transfer-balance-all" , {"has-error": balanceError})}
                              onClick={()=>this._setTotal(current_asset_id, account_balances[current_asset_id], fee, feeID)}>
                            <BalanceComponent noPrefix balance={account_balances[current_asset_id]} />
                        </span>
                    </span>
                );



                if (feeID === current_asset_id && balanceError) {
                    balance = (
                        <span className={classnames("send-transfer-balance-fee" , {"has-error": balanceError})}>
                            <Translate content="transfer.errors.insufficient"/>
                        </span>
                    );
                }
            } else {
                balance     = (
                    <span className={classnames({"has-error": balanceError})}>
                        <Translate content="transfer.errors.noFunds"/>
                    </span>
                );
                balance_fee = (
                    <span className={classnames({"has-error": balanceError})}>
                        <Translate content="transfer.errors.noFunds"/>
                    </span>
                );
            }
        }

        const amountValue      = parseFloat(
            String.prototype.replace.call(amount, /,/g, "")
        );
        let hasMinimum = Number(memo) >= info.min_tokens_purchase;
        const isAmountValid    = amountValue && !isNaN(amountValue);
        const isSendNotValid   =
            !hasMinimum ||
            !from_account ||
            // !to_account ||
            !isAmountValid ||
            !asset ||
            !hasBalance ||
            !hasPoolBalance ||
            balanceError /*||
            !AccountStore.isMyAccount(from_account)*/;

        let tabIndex = this.props.tabIndex || 0; // Continue tabIndex on props count

        /*console.log("isSendNotValid", isSendNotValid);
        if( !from_account )
            console.log("isSendNotValid from_account", from_account);
        if( !to_account )
            console.log("isSendNotValid to_account", to_account);
        if( !isAmountValid )
            console.log("isSendNotValid isAmountValid", isAmountValid);
        if( !asset )
            console.log("isSendNotValid asset", asset);
        if( !hasBalance )
            console.log("isSendNotValid hasBalance", hasBalance);
        if( !hasPoolBalance )
            console.log("isSendNotValid hasPoolBalance", hasPoolBalance);
        if( !hasMinimum )
            console.log("isSendNotValid hasMinimum", hasMinimum);
        if( balanceError )
            console.log("isSendNotValid balanceError", balanceError);
        console.log("isSendNotValid token_name", token_name);
        console.log("isSendNotValid memo", memo);
        */
        // console.log(this.props);

        return (
            <div className="send-transfer">
                {token_name && (<div className="send-transfer-header">
                    {this.props.nft ?
                    <>
                    <div className={"send-transfer-sub-title"}>Выберите валюту, в которой Вы будете приобретать токены {token_name.toUpperCase()}</div>
                    <br/>
                    </> 
                    :
                    <>
                    <Translate unsafe component={"div"} className={"send-transfer-sub-title"} content={"ieo.transfer_title"} token_name={token_name.toUpperCase()} />
                    <br/>
                    </>}
                </div>)}
                <form noValidate>
                    {/* T O */}
                    <div className={"send-transfer-row amount"}>
                        {/*  A M O U N T  */}
                        {asset_types.length ? <AmountSelector
                            label="transfer.amount"
                            amount={amount}
                            onChange={this.onAmountChanged}
                            asset={
                                asset_types.length > 0 && asset
                                    ? asset.get("id")
                                    : asset_id ? asset_id : asset_types[0]
                            }
                            assets={asset_types}
                            display_balance={balance}
                            tabIndex={tabIndex++}
                        /> : null }
                    </div>
                    {/*  M E M O  */}
                    <div className={"send-transfer-row memo"}>
                    {this.props.nft ?
                        <label
                            className="left-label tooltip"
                            

                            token_name={token_name}
                            data-place="top"
                            data-tip={counterpart.translate(
                                "tooltip.memo_tip"
                            )}
                        >Количество {token_name}s</label>
                        :
                        <Translate
                            className="left-label tooltip"
                            component="label"
                            content="ieo.memo_count"

                            token_name={token_name.toUpperCase()}
                            data-place="top"
                            data-tip={counterpart.translate(
                                "tooltip.memo_tip"
                            )}
                        /> }
                        <textarea
                            style={{ marginBottom: 0 }}
                            rows="1"
                            value={memo}
                            tabIndex={tabIndex++}
                            onChange={(e)=>this.onMemoChanged(e.target.value)}
                        />
                        {this.props.nft ?
                        <div className="send-transfer-memo-info" style={{marginTop: 5}}>
                            Количество:
                            <span>{info.min_tokens_purchase} {token_name.toUpperCase()}</span>
                        </div> :
                        <div className="send-transfer-memo-info" style={{marginTop: 5}}>
                            <Translate content={"ieo.info.min_tokens_purchase"}/>:
                            <span>{info.min_tokens_purchase} {token_name.toUpperCase()}</span>
                        </div>}
                    </div>

                    <div className={"send-transfer-row fee"}>
                        <div className="no-margin no-padding">
                            {/*  F E E  */}
                            <div id="txFeeSelector" className="small-12">
                                <AmountSelector
                                    label="transfer.fee"
                                    disabled={true}
                                    amount={fee}
                                    onChange={this.onFeeChanged}
                                    asset={fee_asset_types[0]}
                                    assets={getAssetsAlias(fee_asset_types)}
                                    display_balance={
                                        balance_fee
                                    }
                                    tabIndex={tabIndex++}
                                    error={
                                        !this.state.hasPoolBalance ? "transfer.errors.insufficient" : null
                                    }
                                    scroll_length={2}
                                />
                            </div>
                            {!hasBalance && (
                                <div
                                    className="has-error"
                                    style={{ marginTop: 5 }}
                                >
                                    <Translate content="transfer.errors.noFeeBalance"/>
                                </div>
                            )}
                        </div>
                    </div>

                    <div className={"send-transfer-row group-button"}>
                        {/*<button
                            className={classnames("btn btn-green", {disabled: isSendNotValid})}
                            disabled={isSendNotValid}
                            type="submit"
                            onClick={
                                !isSendNotValid ? this.onSubmit.bind(this) : null
                            }
                            tabIndex={tabIndex++} >
                            <Translate component="span" content="ieo.transfer_send"/>
                        </button>*/}

                        <WrapperAuth
                            component={"button"}
                            type="button"
                            className={classnames("btn btn-green", {disabled: isSendNotValid})}
                            disabled={isSendNotValid}
                            onClick={(event)=>{
                                this.onSubmit(event)
                            }}
                        >
                            {this.props.nft ? <span>Инвестировать</span> : <Translate component="span" content="ieo.transfer_send"/>}
                        </WrapperAuth>
                    </div>
                </form>
            </div>
        );
    }
}

const IEoTransferBind = BindToChainState(IEoTransfer);

class IEoTransferConnectWrapper extends React.Component {
    render() {
        return <IEoTransferBind {...this.props} />;
    }
}

export default connect(
    IEoTransferConnectWrapper,
    {
        listenTo() {
            return [AccountStore];
        },
        getProps() {
            return {
                currentAccount: AccountStore.getState().currentAccount
                    || AccountStore.getState().passwordAccount,
                passwordAccount: AccountStore.getState().passwordAccount
            };
        }
    }
);

import React from "react";
import {Link} from "react-router-dom";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
import DefaultModal from "Components/Modal/DefaultModal";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";
// import {Checkbox} from "antd";
import Translate from "react-translate-component";
// import classname from "classnames";
// import AssetName from "Components/Utility/AssetName";
// import Icon from "Components/Icon/Icon";
import ImageLoad from "Utility/ImageLoad";


class HelloIeoModal extends DefaultBaseModal {


    closeModal = () => {
        const {reject, modalId} = this.props;
        ModalActions.hide(modalId)
            .then(reject);
    };

    confirmModal = () => {
        const {resolve, modalId} = this.props;
        ModalActions.hide(modalId)
            .then(resolve);
    };


    render () {
        const {isOpenModal} = this.state;
        const {modalId, modalIndex, data} = this.props;

        let content = data && data.content;

        let dataInfo = {
            penalty_percent: content ? [content.penalty, "%"].join("") : null,
            penalty_sum: content ? content.price / 100 * content.penalty : null,
            interest_percent: content ? [content.interest, "%"].join("") : null,
            interest_sum: content ? content.price / 100 * content.interest : null,
            price: content ? content.price : null
        };

        //window.console.log({zIndex: modalIndex[modalId]});

        return (
            <DefaultModal
                id={modalId}
                isOpen={isOpenModal}
                onAfterOpen={this.getCurrentPrice}
                className={["CenterModal",modalId].join(" ")}
                onRequestClose={this.closeModal}
                customStyle={{zIndex: modalIndex[modalId]}}
            >
                <div className="modal fadeIn is-active" >
                    <div className="modal-header">
                        <span onClick={this.closeModal} className="close-button">×</span>
                    </div>
                    <div className="modal-content">

                        <Translate content={"ieo.hello_modal.title"} className={"ieo-modal-title"} component={"div"}  />

                        <div className={"ieo-modal-image"}>
                            <ImageLoad
                                customStyle={{
                                    maxWidth: 149
                                }}
                                staticPath={"images/ieo"}
                                imageName={"smile.gif"} />
                        </div>

                        <Translate content={"ieo.hello_modal.info"} className={"ieo-modal-info"} component={"div"}  />
                        <Translate unsafe content={"ieo.hello_modal.why_buy"} className={"ieo-modal-why-buy"} component={"div"}  />

                        <div className={"ieo-modal-buttons"}>
                            <Link to={"/create-account"} className={"btn btn-blue"}>
                                <Translate content="public.register" />
                            </Link>
                            <Link to={"/authorization"} className={"btn btn-green"}>
                                <Translate content="public.login" />
                            </Link>
                        </div>


                    </div>
                </div>

            </DefaultModal>
        );
    }
}

HelloIeoModal.defaultProps = {
    modalId: "hello_ieo_modal"
};

export default class HelloIeoModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    data: () => ModalStore.getState().data["hello_ieo_modal"],
                    modalIndex: () => ModalStore.getState().modalIndex,
                    resolve: () => ModalStore.getState().resolve,
                }}
            >
                <HelloIeoModal {...this.props} />
            </AltContainer>
        );
    }
}

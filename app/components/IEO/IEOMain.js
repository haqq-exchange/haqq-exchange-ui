import React from "react";
import cn from "classnames";
import Immutable from "immutable";
import Translate from "react-translate-component";
import PropTypes from "prop-types";
import {sortByOrder} from "lodash";
// import counterpart from "counterpart";
// import {async_fetch} from "../../api/apiConfig";

export default class IEOMain extends React.Component {

    static contextTypes = {
        location: PropTypes.object,
        router: PropTypes.object
    };

    componentDidMount() {
    }

    render() {
        return (
            <div className={"ieo-main"}>
                <IEOHeader />
                <IEOContent {...this.props}  />
            </div>
        );
    }
}

const IEOContent = (props) => {
    const {ieoList, settings, history} = props;
    let labelSwitch = Immutable.fromJS({
        "0": "waiting",
        "1": "active",
        "2": "stop",
        "3": "coming_soon",
    });

    console.log("ieoList", ieoList)

    const eioPage = (data) => {
        history.push(["/ieo", data.token_name].join("/"));
        /*if( labelSwitch.get(data.active) === "active" ) {
        } else {
            window.open(data.url, "_blank");
        }*/
    };

    const getExpiration = ieo_finish_date => {
        try {
            let timeOptions = {
                year: "numeric", month: "numeric", day: "numeric",
                hour: "numeric", minute: "numeric",
                hour12: false
            };
            return new Intl
                .DateTimeFormat(settings.get("locale"), timeOptions)
                .format(new Date(ieo_finish_date));
        } catch (e) {

        }
    };

    const getStatus = data => {
        const hasIeoFinishDate = data.ieo_finish_date ? new Date(data.ieo_finish_date) >= new Date() : false;
        const hasIeoStartDate = data.ieo_start_date ? new Date(data.ieo_start_date) >= new Date() : false;

        let hasActive = hasIeoFinishDate && !hasIeoStartDate;
        let hasStop = !!data.ieo_finish_date && !hasIeoFinishDate ;
        let hasCS = !hasIeoFinishDate && !hasIeoStartDate && !data.ieo_finish_date && !data.ieo_start_date;
        let hasWaiting = false;

        data.status = {
            hasIeoFinishDate,
            hasIeoStartDate,
            hasActive,
            hasStop,
            hasCS,
            hasWaiting
        };
        return data;
    };

    if( !ieoList ) return null;
    let ieoListStatus = ieoList.map(ieo => getStatus(ieo));
    ieoListStatus = sortByOrder(ieoListStatus, [ "status.hasStop", "status.hasActive", "status.hasCS"]);
    // console.clear();
    // console.log("ieoList", ieoList);
    // console.log("ieoListStatus", ieoListStatus);

    return (
        <div className={"ieo-content"}>
            {ieoListStatus.map(data => {
                let expiration = getExpiration(data.ieo_finish_date);
                const labelActive = data.status.hasActive ? "1" : data.status.hasStop ? "2" : data.status.hasCS ? "3" : "0" ;
                return (
                    <div key={data.token_name} className={cn("ieo-content-item", data.token_name.toLowerCase(), {
                        "not-started": data.status.hasCS
                    })} onClick={()=>data.status.hasActive ? eioPage(data) : {}} >
                        <div className={"ieo-content-item-bg"} style={{backgroundImage: "url("+ `https://static.haqq.exchange/images/ieo/${data.token_name.toLowerCase()}_preview.png` +")"}} />
                        <Translate content={["ieo.label", labelSwitch.get(labelActive)].join("_")} className={cn("ieo-content-item-label", labelSwitch.get(labelActive) )} />
                        <Translate content={"ieo.item_title"} component={"div"} className={cn("ieo-content-item-title")} type_ieo={data.type} token_name={data.name} core_token={ __GBL_CHAIN__ ? "GBL" : "DEEX"} />
                        <Translate content={["ieo",data.token_name.toLowerCase(), "about_short"].join(".")} component={"div"} className={cn("ieo-content-item-desc")}  />
                        <div className={"ieo-content-item-footer"}>
                            {data.asset_total_volume ? <div className={"ieo-content-item-row"}>
                                <Translate content={"ieo.volume_sale"}   />
                                <b>{data.asset_total_volume} {data.name.toUpperCase()}</b>
                            </div> : null}
                            {expiration ? <div className={"ieo-content-item-row"}>
                                <Translate content={"ieo.end_time_sales"}  />
                                <b>{expiration}</b>
                            </div>: null}
                        </div>
                    </div>
                );
            })}
        </div>
    );
};

const IEOHeader = () => {
    return (
        <div className={"ieo-header"}>
            <Translate content={"ieo.title"}  className={"ieo-header-title"} component={"div"}  />
            <Translate content={"ieo.description"} className={"ieo-header-desc"} component={"div"}  />
            <Translate
                content={"ieo.spend_link"} className={"ieo-header-link btn btn-green"}
                component={"a"} href={"https://forms.gle/swScsLMtMvMYpQrz6"} target={"_blank"}  />
            <Translate
                content={"ieo.spend_link_present"} className={"ieo-header-link btn btn-gray"}
                component={"a"} href={"https://forms.gle/9CuFuThf6qNYNFPf8"} target={"_blank"}  />

        </div>
    );
};
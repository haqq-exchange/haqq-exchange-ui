import React from "react";
import {Switch, Route} from "react-router-dom";
import Loadable from "react-loadable";
LoadingIndicator

import "./style.scss";
import {async_fetch, getRequestAddress, getLoadedLocales} from "api/apiConfig";
import {connect} from "alt-react";
import SettingsStore from "stores/SettingsStore";
import counterpart from "counterpart";
import AccountStore from "../../stores/AccountStore";
import { createModuleResolutionCache } from "typescript";

const IEOMain = Loadable({
    loader: () => import(/* webpackChunkName: "IEOMain" */ "./IEOMain"),
    loading: LoadingIndicator
});
const IEOItem = Loadable({
    loader: () => import(/* webpackChunkName: "IEOItem" */ "./IEOItem"),
    loading: LoadingIndicator
});
const Page404 = Loadable({
    loader: () => import(/* webpackChunkName: "IEOItem" */ "Components/Page404/Page404"),
    loading: LoadingIndicator
});

class IEOPage extends React.Component {

    static defaultProps = {
        "presaleApi": getRequestAddress("ieo")
    };

    constructor(){
        super();

        this.state = {
            localesLoaded: false,
            hasLoadedIeo: false,
            ieoList: []
        };
    }

    componentDidMount() {
        this.getApiList();
        this.getLoadedLocales();
    }

    getLoadedLocales = () => {
        let transIEO = counterpart.translate("ieo");
        if( typeof transIEO !== "object" ) {
            getLoadedLocales("ieo.json")
                .then(() => this.setState({localesLoaded: true}));
        } else {
            this.setState({
                localesLoaded: true
            });
        }
    };

    getApiList = () => {
        const _this = this;
        const {presaleApi} = this.props;

        var myHeaders = new Headers();
        var myInit = {
            headers: myHeaders,
            mode: "cors"
        };

        fetch([presaleApi, "list"].join("/"), myInit)
            .then(r=>r.json())
            .then((result) => {
                _this.setState({
                    ieoList:  result.ids,
                    hasLoadedIeo: true
                });
            });
    };



    render() {
        const {ieoList, hasLoadedIeo, localesLoaded} = this.state;
        const {name} = this.props.match.params;
        // window.console.log("this.props", this.props);
        // window.console.log("this.props name", name);
        // window.console.log("this.props ieoList", ieoList);

        const hasTokenName = ieoList.find(ieo => name && ieo.token_name.toLowerCase() === name.toLowerCase());
        if( !localesLoaded ) return  <LoadingIndicator />;
        return (
            <div className={"ieo"}>
                <Switch >
                    <Route
                        path={"/ieo"}
                        exact
                        render={() => <IEOMain {...this.props} ieoList={ieoList}  />}
                    />
                    {hasTokenName ? <Route
                        path={`/ieo/${name}`}
                        exact
                        render={() => <IEOItem {...this.props} token_name={name.toLowerCase()} ieoList={ieoList}  />}
                    /> : !hasLoadedIeo ? <LoadingIndicator /> : <Page404/>}
                </Switch>
            </div>
        );
    }
}

export default  connect(IEOPage, {
    listenTo() {
        return [SettingsStore];
    },
    getProps() {
        let {passwordAccount, currentAccount} = AccountStore.getState();
        return {
            settings: SettingsStore.getState().settings,
            currentAccount: currentAccount || passwordAccount,
        };
    }
});

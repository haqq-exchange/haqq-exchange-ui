import React from "react";
import {Link} from "react-router-dom";
import {connect} from "alt-react";
import SettingsStore from "stores/SettingsStore";
import Translate from "react-translate-component";

const light = require("assets/svg/anonim_black.svg");
const dark = require("assets/svg/anonim_white.svg");

class Page404 extends React.Component {
    static defaultProps = {
        subtitle: "page_not_found_subtitle",
        svgLogo: {
            "lightTheme": require("assets/svg/anonim_black.svg"),
            "darkTheme": require("assets/svg/anonim_white.svg"),
        }
    };
    render() {
        const {theme, svgLogo} = this.props;
        const logo = svgLogo[theme];

        return (
            <div className="page-404">
                <div className="page-404-container">
                    <div className="page-404-title">
                        oops!.. 404
                    </div>
                    <span className="page-404-logo"  dangerouslySetInnerHTML={{
                        __html: svgLogo[theme]
                    }}>

                    </span>
                    <div className="page-404-subtitle">
                        <Translate content={"page404." + this.props.subtitle} />
                    </div>
                    {__SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__ ? null :
                    <Link to={"/"} className="page-404-button-back">
                        <Translate content="page404.home"/>
                    </Link>}
                </div>
            </div>
        );
    }
}

export default (Page404 = connect(Page404, {
    listenTo() {
        return [SettingsStore];
    },
    getProps() {
        return {
            theme: SettingsStore.getState().settings.get("themes")
        };
    }
}));

import React from "react";
import {connect} from "alt-react";
import AccountStore from "stores/AccountStore";
// import {Link} from "react-router-dom";
// import Translate from "react-translate-component";
// import TranslateWithLinks from "./Utility/TranslateWithLinks";
import {isIncognito} from "feature_detect";
// import SettingsActions from "actions/SettingsActions";
// import WalletUnlockActions from "actions/WalletUnlockActions";
// import ActionSheet from "react-foundation-apps/src/action-sheet";
import SettingsStore from "stores/SettingsStore";
// import IntlActions from "actions/IntlActions";
import {Route} from "react-router-dom";
import {
    Create2faAccountPassword,
    CreateAccountPassword,
    CreateAccount} from "RoutesLink";



class LoginSelector extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            step: 1,
            locales: SettingsStore.getState().defaults.locale.filter(item => {
                return ["en", "tr", "ru", "zh"].includes(item);
            }),
            currentLocale: SettingsStore.getState().settings.get("locale")
        };
    }

    componentDidUpdate() {
        const myAccounts = AccountStore.getMyAccounts();

        // use ChildCount to make sure user is on /create-account page except /create-account/*
        // to prevent redirect when user just registered and need to make backup of wallet or password
        const childCount = React.Children.count(this.props.children);

        // do redirect to portfolio if user already logged in
        if (
            this.props.router &&
            Array.isArray(myAccounts) &&
            myAccounts.length !== 0 &&
            childCount === 0
        )
            this.props.router.push("/account/" + this.props.currentAccount);
    }

    componentDidMount() {
        isIncognito(incognito => {
            this.setState({incognito});
        });
    }

    render() {
        const referralState = this.props.history.location.state;

        return (
            <div className="grid-block align-center">
                <div className="grid-block shrink vertical account-creation-wrapper">
                    <div className="grid-content shrink text-center account-creation">
                        <Route path="/create-account/wallet" exact component={CreateAccount} />
                        <Route path="/create-account/password" exact render={() => (<CreateAccountPassword referralState={referralState}/>)} />
                        <Route path="/create-account/password-2fa" exact render={() => (<Create2faAccountPassword referralState={referralState}/>)} />
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(
    LoginSelector,
    {
        listenTo() {
            return [AccountStore];
        },
        getProps() {
            return {
                currentAccount:
                    AccountStore.getState().currentAccount ||
                    AccountStore.getState().passwordAccount
            };
        }
    }
);

import React from "react";
import SettingsStore from "stores/SettingsStore";
import Icon from "../Icon/Icon";
import FlagDropdown from "./Header/FlagDropdown";

class PublicHeader extends React.Component {
    render() {
        const {settings} = SettingsStore.getState();
        const logoName =
            settings.get("themes") !== "lightTheme" ? "logo-white" : "logo";

        const {logoPosition = "center"} = this.props;
        return (
            <div className={"public-header " + logoPosition}>
                {/* <img src={logo} className="public-logo" /> */}
                <Icon name={logoName} />
                <FlagDropdown />
            </div>
        );
    }
}



export default PublicHeader;

import React from "react";
import "./iFramePage.scss";

class IframePage extends React.PureComponent {
    static defaultProps = {
        linksFrame: {
            "/cran": "https://deex4free.com/?iframe",
            "/loto": "https://loto.deex.exchange/rounds/current/?iframe"
        }
    };

    addStyle = () => {
        const frame = this.refIframe;
        // console.log("frame.contentDocument", frame.contentDocument);
        //console.log("frame.contentDocument", frame.contentWindow);

    };

    render() {
        const {linksFrame} = this.props;
        const {path} = this.props.match;

        return <iframe src={linksFrame[path]} ref={ref=>this.refIframe=ref} onLoad={()=>this.addStyle()} style={{
            width: "100%",
            height: "100%",
            backgroundColor: "white"
        }} frameBorder="0" />;

    }
}

export default IframePage;
import React, {Component} from "react";
import PropTypes from "prop-types";
import AltContainer from "alt-container";
import BindToChainState from "../Utility/BindToChainState";
import ChainTypes from "../Utility/ChainTypes";
import CachedPropertyStore from "stores/CachedPropertyStore";
import BlockchainStore from "stores/BlockchainStore";
import WalletDb from "stores/WalletDb";
import SettingsStore from "stores/SettingsStore";
import "intro.js/introjs.css";
import ResponseOption from "../../config/response";
import MediaQuery from "react-responsive";
import Loadable from "react-loadable";
import LoadingIndicator from "Components/LoadingIndicator";

const FooterDesktop = Loadable({
    loader: () => import(/* webpackChunkName: "ExchangeContainer" */ "./Footer/desktop"),
    loading: LoadingIndicator
});
const FooterMobile = Loadable({
    loader: () => import(/* webpackChunkName: "ExchangeContainer" */ "./Footer/mobile"),
    loading: LoadingIndicator
});

class Footer extends React.Component {
    static propTypes = {
        dynGlobalObject: ChainTypes.ChainObject.isRequired,
        synced: PropTypes.bool.isRequired
    };

    static defaultProps = {
        dynGlobalObject: "2.1.0"
    };

    static contextTypes = {
        router: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.state = {
            showNodesPopup: false,
            minify: true
        };
    }

    componentDidMount() {
    }

    shouldComponentUpdate(nextProps, nextState) {
        let apiLatencies1 = Object.values(nextProps.apiLatencies);
        let apiLatencies2 = Object.values(this.props.apiLatencies);
        let difference = apiLatencies1.filter(x => apiLatencies2.indexOf(x) == -1);
        //console.log("shouldComponentUpdate Footer", nextProps, this.props, difference, apiLatencies1, apiLatencies2);
        //console.log('this.props', this.props, nextProps, diff(this.props, nextProps));
        return (
            nextProps.dynGlobalObject !== this.props.dynGlobalObject ||
            nextProps.backup_recommended !== this.props.backup_recommended ||
            nextProps.rpc_connection_status !== this.props.rpc_connection_status ||
            nextProps.synced !== this.props.synced ||
            nextProps.currentNode !== this.props.currentNode ||
            nextState.showNodesPopup !== this.state.showNodesPopup ||
            difference.length > 0
        );
    }


    getNodeIndexByURL(url) {
        let nodes = this.props.defaults.apiServer;

        let index = nodes.findIndex(node => node.url === url);
        if (index === -1) {
            return null;
        }
        return index;
    }


    getNode(node) {
        const {props} = this;

        return {
            name: node.location || "Unknown location",
            url: node.url,
            up: node.url in props.apiLatencies,
            ping: props.apiLatencies[node.url],
            hidden: !!node.hidden
        };
    }

    render() {
        // const { state } = this;
        // const {synced} = this.props;


        return (
            <div className="footer">
                <MediaQuery {...ResponseOption.mobile}>
                    {(matches) => {
                        if (matches) {
                            return <FooterMobile {...this.props} />;
                        } else {
                            return <FooterDesktop {...this.props} />;
                        }
                    }}
                </MediaQuery>

            </div>
        );
    }
}
Footer = BindToChainState(Footer, {keep_updating: true});

class AltFooter extends Component {
    render() {
        var wallet = WalletDb.getWallet();
        return (
            <AltContainer
                stores={[
                    CachedPropertyStore,
                    BlockchainStore,
                    WalletDb,
                    SettingsStore
                ]}
                inject={{
                    defaults: () => {
                        return SettingsStore.getState().defaults;
                    },
                    apiLatencies: SettingsStore.getState().apiLatencies.toJS(),
                    currentNode: () => {
                        return SettingsStore.getState().settings.get(
                            "apiServer"
                        );
                    },
                    theme: () => {
                        return SettingsStore.getState().settings.get(
                            "themes"
                        );
                    },
                    activeNode: () => {
                        return SettingsStore.getState().settings.get(
                            "activeNode"
                        );
                    },
                    backup_recommended: () =>
                        wallet &&
                        (!wallet.backup_date ||
                            CachedPropertyStore.get("backup_recommended")),
                    rpc_connection_status: () =>
                        BlockchainStore.getState().rpc_connection_status
                }}
            >
                <Footer {...this.props} />
            </AltContainer>
        );
    }
}

export default AltFooter;

import React from "react";
import ModalActions from "actions/ModalActions";
import Translate from "react-translate-component";
import {cn} from "@bem-react/classname";
import PropTypes from "prop-types";

import "./AuthLogin.scss"

const cln = cn("AuthLogin");


export default class AuthLoginPage extends React.Component {

    static defaultProps = {
        title: "admin-exchange.AuthLogin.title",
        info: "admin-exchange.AuthLogin.login_info",
    };

    showModalLogin = event => {
        event.preventDefault();

        ModalActions.show("unlock_wallet_modal_public").then(() => {
            if(this?.props?.setNodeStatus) this.props.setNodeStatus("loading");
        });
    };

    render(){
        const {title, info} = this.props;
        return(
            <div className={cln()}>
                <div className={cln("block")}>
                    <div className={cln("wrap")}>
                        <Translate className={cln("title")} component={"div"} content={title}/>
                        <Translate className={cln("unlock-info")} component={"div"} content={info}/>
                        <button type={"button"} onClick={(event)=>this.showModalLogin(event)} className={"btn btn-red btn-auth"} >
                            <Translate content={"header.unlock_short"}/>
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}


AuthLoginPage.propTypes = {
    title: PropTypes.string.isRequired,
    info: PropTypes.string.isRequired,
};

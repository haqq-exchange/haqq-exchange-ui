import React from "react";
import {NavLink, Link} from "react-router-dom";
import Translate from "react-translate-component";
import accountUtils from "../../../lib/common/account_utils";
import RefUtils from "../../../lib/common/ref_utils";
import {getRequestAddress} from "../../../api/apiConfig";
import SettingsActions from "../../../actions/SettingsActions";
import WalletMenuLinks from "./WalletMenuLinks";
import ResponseOption from "../../../config/response";
// import MobileAccountAssetHeader from "Components/AccountAsset/Mobile/AccountAssetHeader";
// import MobileAccountAssetContent from "Components/AccountAsset/Mobile/AccountAssetContent";
// import MobileAccountAssetMarket from "Components/AccountAsset/Mobile/AccountAssetMarket";
// import DeexNews from "Components/AccountAsset/Desktop/DeexNews";
// import AccountAssetHeader from "Components/AccountAsset/Desktop/AccountAssetHeader";
// import AccountAssetContent from "Components/AccountAsset/Desktop/AccountAssetContent";
// import AccountAssetMarket from "Components/AccountAsset/Desktop/AccountAssetMarket";
import MediaQuery from "react-responsive";
import ModalActions from "../../../actions/ModalActions";
import { block_header } from "deexjs/dist/serializer/src/operations";

export default class MenuNav extends React.Component {

    state = {
        isVisibleMenu: false
    };
    static defaultProps = {
        locked: true,
        getUserSign: accountUtils.getUserSign,
        fetchRequest: new RefUtils({
            url: getRequestAddress("nodes")
        }),
        adminFetchRequest: new RefUtils({
            url: getRequestAddress("adminExchange")
        })
    };

    componentDidMount() {
        const {settings} = this.props;
        this.setState({
            is_service_node: settings.get("is_service_node"),
            has_admin_exchange: settings.get("has_admin_exchange"),
        });
        if( ! this.props.locked ) {
            this.getMenuAccess();
            this.getAdminAccess();
        }
        // console.log("this.props.locked ", this.props.locked );
    }

    componentDidUpdate(prevProps) {
        if( this.props.locked !== prevProps.locked && !this.props.locked ||
            this.props.currentAccount !== prevProps.currentAccount && !prevProps.locked
        ) {
            this.getMenuAccess( true );
            this.getAdminAccess( true );
        }
    }

    _onShowForm = (typeWindow, event) => {
        event.preventDefault();
        this.props.clickedMenu();
        ModalActions.show("selected_deposit_withdraw", {
            typeWindow
        });
    };


    getMenuAccess = ( isReload ) => {
        if(!__SCROOGE_CHAIN__) {
        const {currentAccount, fetchRequest, settings} = this.props;

        if( !settings.has("is_service_node") || !settings.get("is_service_node") || isReload ) {
            
                this.props.getUserSign({account: currentAccount}).then(sign => {
                    fetchRequest
                        .postTemplate("/is_service_allowed", {
                            account: currentAccount
                        }, sign )
                        .then(({status})=>{
                            this.setState({
                                is_service_node: status === 200
                            });

                            SettingsActions.changeSetting({
                                setting: "is_service_node",
                                value: status === 200
                            });
                        }).catch(error=>{
                        console.log("is_service_node", error);
                    });
                }, () => {  });
            }
        }
    };

    getAdminAccess = ( isReload ) => {
        if(!__SCROOGE_CHAIN__) {
        const {currentAccount, adminFetchRequest, settings} = this.props;
        const has_admin_exchange = settings.getIn(["has_admin_exchange"]);

        console.log("settings", settings.toJS(), currentAccount);

        if(currentAccount && !has_admin_exchange?.[currentAccount] || isReload ) {
        
                this.props.getUserSign({account_name: currentAccount}).then(sign => {
                    adminFetchRequest
                        .postTemplate("/hasExchanger", {
                            account_name: currentAccount
                        }, sign )
                        .then(data=>{
                            const {content} = data;
                            const hasActive = content.data.valid;
                            console.log("data", data);
                            console.log("content", content);

                            this.setState({
                                has_admin_exchange: hasActive
                            });

                            SettingsActions.changeSetting({
                                setting: "has_admin_exchange",
                                value: {
                                    [currentAccount]: hasActive
                                }
                            });
                            if( hasActive ) {
                                SettingsActions.changeSetting({
                                    setting: "admin_exchange",
                                    value: content.data
                                });
                            }
                        }).catch(error=>{
                            console.log("is_service_node", error);
                            this.setState({
                                has_admin_exchange: false
                            });
                            SettingsActions.changeSetting({
                                setting: "admin_exchange",
                                value: null
                            });
                        });
                }, () => {  });
            }  
        }
    };


    render() {
        const {currentAccount, lastMarket, clickedMenu, settings} = this.props;
        const has_admin_exchange = settings.getIn(["has_admin_exchange"]);
        //console.log("has_admin_exchange", has_admin_exchange[currentAccount], currentAccount, settings.toJS() , settings.getIn(["has_admin_exchange"]), settings.getIn(["has_admin_exchange", currentAccount]) );
        return (
            <React.Fragment>
                <NavLink activeClassName="active" exact strict={true} to={__SCROOGE_CHAIN__ ? "/market/USDT_BTC" : "/market"} onClick={clickedMenu} className={"header-link"} >
                    {/*<Translate className="header-link-span" content="header.markets"/>*/}
                    {__GBL_CHAIN__ ?
                    <Translate className="header-link-span" content="markets.bidding"/> :
                    <Translate className="header-link-span" content="header.exchange"/>}
                </NavLink>
                {__GBL_CHAIN__ ? 
                <NavLink activeClassName="active" exact strict={true} to={"/market/GBL_SBERRUBLE"} className={"header-link"} >
                    <Translate className="header-link-span" content="header.exchange"/>
                </NavLink> : null }

                {/*<NavLink activeClassName="active" exact strict={true} to={"/credit"} onClick={clickedMenu} className={"header-link"} >
                    <Translate className="header-link-span" content="header.credit"/>
                </NavLink>*/}
                
                {__SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__ ? null :
                <NavLink activeClassName="active" exact strict={true} to={"/ieo"} onClick={clickedMenu} className={"header-link"} >
                    <Translate className="header-link-span" content="header.ieo"/>
                </NavLink>}
                {/*<NavLink activeClassName="active" to={["/market", lastMarket || "DEEX_BTS"].join("/")} onClick={clickedMenu} className={"header-link"} >
                    <Translate className="header-link-span" content="header.exchange"/>
                </NavLink>*/}
                {has_admin_exchange?.[currentAccount] ? <NavLink activeClassName="active" to={["/admin/exchange"].join("/")} onClick={clickedMenu} className={"header-link"} >
                    <Translate className="header-link-span" content="header.admin_exchange"/>
                </NavLink> : null}

                <MediaQuery {...ResponseOption.mobile}>
                    {(matches) => {
                        if (matches) {
                            return (
                                currentAccount ?
                                    <>
                                        <NavLink
                                            activeClassName="active" exact
                                            to={["", "account", currentAccount].join("/")} onClick={clickedMenu}
                                            className={"header-link"}>
                                            <Translate className="header-link-span" content="header.wallet"/>
                                        </NavLink>
                                       {/* <NavLink
                                            activeClassName="active" exact
                                            to={["", "credit", "my"].join("/")} onClick={clickedMenu}
                                            className={"header-link"}>
                                            <Translate className="header-link-span" content="header.credit"/>
                                        </NavLink> */}
                                        <Link to={"#"} className={"header-link"} onClick={(event) => this._onShowForm("deposit", event)}>
                                            <Translate className="header-link-span" content="account.deposit"/>
                                        </Link>
                                        <Link to={"#"} className={"header-link"} onClick={(event) => this._onShowForm("withdraw", event)}>
                                            <Translate className="header-link-span" content="account.withdraw"/>
                                        </Link>

                                    </> : null

                            );
                        } else {
                            return (
                                currentAccount ? <WalletMenuLinks className={"header-link"} currentAccount={currentAccount}>
                                    <NavLink activeClassName="active" exact to={["", "account", currentAccount].join("/")} onClick={clickedMenu} className={"header-link"}>
                                        <Translate className="header-link-span" content="header.wallet"/>
                                    </NavLink>
                                </WalletMenuLinks> : null
                            );
                        }
                    }}
                </MediaQuery>

                {__SCROOGE_CHAIN__ ? 
                <NavLink activeClassName="active" exact strict={true} to={"/api"} onClick={clickedMenu} className={"header-link"} >
                    API
                </NavLink> : null}

                {__SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__ ? null :
                <>
                    <Translate content={"header.blog"} className={"header-link"} component={"a"} href={"https://teletype.in/@halalcryptochat"} target={"_blank"}  />
                    <Translate content={"header.otc"} className={"header-link"} component={"a"} href={"https://teletype.in/@halalcryptochat/Tj-dOaf3BcA"} target={"_blank"}  />
                </>}
                {__GBL_CHAIN__ || __DEEX_CHAIN__ || __GBLTN_CHAIN__ ? 
                <NavLink activeClassName="active" exact strict={true} to={"/marketplace"} onClick={clickedMenu} className={"header-link"} >
                    Marketplace
                </NavLink>
                : null}

                {/*<NavLink activeClassName="active" exact to={["","account", currentAccount, "referrals"].join("/")} onClick={clickedMenu} className={"header-link"} >
                    <Translate className="header-link-span" content="header.referrals"/>
                </NavLink>
                {is_service_node ? <NavLink activeClassName="active" exact to={["","nodes"].join("/")} onClick={clickedMenu} className={"header-link"} >
                    <Translate className="header-link-span" content="node.title"/>
                </NavLink> : null}*/}
            </React.Fragment>
        );
    }
}

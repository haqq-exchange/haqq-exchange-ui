import React from "react";
import ClickOutside from "react-click-outside";
import cnames from "classnames";
import {Link} from "react-router-dom";
import Icon from "../../Icon/Icon";
import Translate from "react-translate-component";
import WalletDb from "stores/WalletDb";
import WalletUnlockActions from "actions/WalletUnlockActions";
import ModalActions from "actions/ModalActions";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import {getRequestAddress} from "api/apiConfig";
import RefUtils from "../../../lib/common/ref_utils";
import accountUtils from "common/account_utils";
import SettingsActions from "../../../actions/SettingsActions";

export default class AuthMenuLinks extends React.PureComponent {
    state = {
        isVisibleMenu: false,
        staticMenu: []
    };

    static defaultProps = {
        getUserSign: accountUtils.getUserSign,
        fetchRequest: new RefUtils({
            url: getRequestAddress("nodes")
        })
    };

    componentDidMount() {

    }


    toggleLock = (event) => {
        event.preventDefault();
        event.stopPropagation();
        if (WalletDb.isLocked()) {
            ModalActions.show("unlock_wallet_modal_public").then(() => {
                AccountActions.tryToSetCurrentAccount();
                /*if(this.props.history) {
                    let {pathname} = this.props.history.location;
                    this.props.history.push("/");
                    setTimeout(()=>{
                        this.props.history.push(pathname);
                    }, 300);
                }*/
            });
        } else {
            this._lockAccount();
        }
        this.onBodyClick(false);
    };

    _lockAccount = () => {
        const {history} = this.props;
        WalletUnlockActions.lock();
        AccountStore.removeCurrentAccount().then(() => {
            history.push("/");
        });
    };

    onBodyClick = (isVisibleMenu) => {
        this.setState({isVisibleMenu});
    };

    createUserMenu(){
        const query = this.props.location.search.startsWith("?ref=") ? this.props.location.search : "";
        let staticMenu = __SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__ ? 
        [
            [{
                "content": "public.login",
                "icon": "login",
                "attr": {
                    "to": __GBL_CHAIN__ || __GBLTN_CHAIN__ ? "/authorization/password" : ["","authorization"].join("/"),
                    "onClick": () => {
                        this.onBodyClick(false);
                    }
                }
            },{
                "content": "public.register",
                "icon": "add-user",
                "attr": {
                    "to": __GBL_CHAIN__ || __GBLTN_CHAIN__ ? "/create-account/password" : ["","create-account"].join("/"),
                    "onClick": () => {
                        this.onBodyClick(false);
                    }
                }
            }], [],[]
        ]:[
            [{
                "content": "public.login",
                "icon": "login",
                "attr": {
                    "to": ["","authorization"].join("/"),
                    "onClick": () => {
                        this.onBodyClick(false);
                    }
                }
            },{
                "content": "public.register",
                "icon": "add-user",
                "attr": {
                    "to": ["",`create-account${query}`].join("/"),
                    "onClick": () => {
                        this.onBodyClick(false);
                    }
                }
            },
            // {
            //     "content": __SCROOGE_CHAIN__ ? "account.network_fees_scrooge" : "account.network_fees",
            //     "icon": "network_fees",
            //     "attr": {
            //         "to": ["", "networkfees"].join("/"),
            //         "onClick": () => {
            //             this.onBodyClick(false);
            //         }
            //     }
            // },
            {
                "content": "account.voting",
                "icon": "voting",
                "attr": {
                    "to": ["","voting"].join("/"),
                    "onClick": ()=>{this.onBodyClick(false);}
                }
            }], [/*{
                "content": "node.title",
                "icon": "nodes",
                "attr": {
                    "to": "/nodes",
                    "onClick": () => {
                        this.onBodyClick(false);
                    }
                }
            }

                ,{
                "content": "header.referrals",
                "icon": "referrals",
                "attr": {
                    "to": ["", "account", "referrals"].join("/"),
                    "onClick": () => {
                        this.onBodyClick(false);
                    }
                }


            }*/],[]
        ];



        return (<div className={"user-menu-drop-down right"}>
            <div className={"user-menu-drop-down-block"}>
                {staticMenu.map((menu, index) => {
                    return (<div key={"user-menu-row-" + index} className={"user-menu-row"}>
                        {menu.map((item, keyItem)=> {
                            return (<div key={"user-menu-item-" + keyItem} className={"user-menu-item"}>
                                <Link className={"user-menu-link"} {...item.attr}>
                                    <Icon name={item.icon}/>
                                    <Translate content={item.content}/>
                                </Link>
                            </div>);
                        })}
                    </div>);
                })}
            </div>
        </div>);
    }
    render(){
        const {isVisibleMenu} = this.state;

        // console.log("locked",locked);

        return(
            <>
               {__SCROOGE_CHAIN__ ?
               <div className={"main-page-footer-btns scrooge-desctop-menu"}>
                    <Link to={"/authorization"} className={"main-page-footer-login"}>
                        <Translate content="public.login" className={"main-page-footer-login-text"} />
                    </Link>
                    <Link to={"/create-account"} className={"main-page-footer-signin"}>
                        <Translate content="public.register" className={"main-page-footer-signin-text"} />
                    </Link>
                </div>
                
                :
                <ClickOutside onClickOutside={()=>this.onBodyClick(false)} className={cnames("user-menu-outside", {"active": isVisibleMenu})} >
                    <a className={cnames("user-menu-account notAuth")} onClick={()=>this.onBodyClick(true)}>
                        <Icon className="no-auth" name={"account-no-auth"}/>
                    </a>
                    {isVisibleMenu && this.createUserMenu()}
                </ClickOutside>}
            </>
        );
    }
}
import React from "react";
import {Link, NavLink} from "react-router-dom";
//import cnames from "classnames";
import Translate from "react-translate-component";
//import DinamicMenu from "Components/Layout/Header/dynamicMenuItem";
import FlagDropdown from "Components/Layout/Header/FlagDropdown";
import MediaQuery from "react-responsive";
import ResponseOption from "../../../config/response";
import MenuNav from "Components/Layout/Header/MenuNav";
import Icon from "Components/Icon/Icon";
import cn from "classnames";
import AuthMenuLinks from "./AuthMenuLinks";
import LogoType from "Components/Layout/Header/LogoType";
import "./header.scss";
import "../../MainPage/MainPage.scss";

export default class HeaderAuth extends React.Component {

    state = {
        showNavMenu: false
    };

    render(){
        let { lastMarket, settings , passwordAccount, locked } = this.props;
        let { showNavMenu } = this.state;

        return(
            <div className="header-container">
                <div className="header-container-wrap">
                    <div className={"header-container-left"}>
                        {__GBL_CHAIN__ || __GBLTN_CHAIN__ ? 
                        <NavLink activeClassName="active" exact to={"/"} className={"header-logo logo"} style={{height: "60px", width: "206px"}} >
                            <div  style={{backgroundImage: "url("+ `https://static.haqq.exchange/gbl/images/logo-gbl.png` +")", height: "60px", width: "205px", margin: "0 auto", backgroundRepeat: "no-repeat"}} />
                        </NavLink>
                        : (__SCROOGE_CHAIN__ ? 
                        <NavLink activeClassName="active" exact to={"/"} className={"header-logo logo main-scrooge-logo"} >
                            <div className="scrooge-logo"/> 
                        </NavLink>
                        :
                        <NavLink activeClassName="active" exact to={"/"} className={"header-logo logo"} >
                            <span className="logo-logotype"/> 
                            {/* <LogoType /> */}
                        </NavLink>)}
                        <MediaQuery {...ResponseOption.notMobile}>
                            <MenuNav
                                locked={locked}
                                clickedMenu={()=>{}}
                                currentAccount={passwordAccount} settings={settings}  lastMarket={lastMarket} />
                        </MediaQuery>
                    </div>
                    <div className={__SCROOGE_CHAIN__ ? "header-container-right-scrooge" : "header-container-right"}>


                        {/*<MediaQuery minDeviceWidth={480}>
                            <NavLink activeClassName="active" to={"/create-account"} className={"header-link"} >
                                <Translate content="public.register" />
                            </NavLink>
                        </MediaQuery>
                        <NavLink activeClassName="active" to={"/authorization"} className={"header-link"} >
                            <Translate content="public.login" />
                        </NavLink>*/}

                        <AuthMenuLinks {...this.props}/>

                        <FlagDropdown />
                        <MediaQuery {...ResponseOption.mobile}>
                            <Icon className={"public-header-menu"} onClick={()=>this.setState({showNavMenu: !showNavMenu})} name={showNavMenu ? "menu-close" : "menu-open"} />
                        </MediaQuery>
                    </div>
                </div>
                <MediaQuery {...ResponseOption.mobile}>
                    <div className={cn("header-container-wrap menu-nav", {"hide": !showNavMenu})}>
                        <MenuNav
                            locked={locked}
                            clickedMenu={()=>this.setState({showNavMenu: !showNavMenu})}
                            currentAccount={passwordAccount} settings={settings} lastMarket={lastMarket} />
                    </div>
                </MediaQuery>
            </div>
        );
    }
}

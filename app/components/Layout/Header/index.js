import React from "react";
import PropTypes from "prop-types";
import {connect} from "alt-react";
import AccountStore from "stores/AccountStore";
import SettingsStore from "stores/SettingsStore";
import GatewayStore from "stores/GatewayStore";
import WalletUnlockStore from "stores/WalletUnlockStore";
import WalletManagerStore from "stores/WalletManagerStore";
import {Apis} from "deexjs-ws";


import loadable from "loadable-components";

const HeaderAuth = loadable(() => import("./HeaderAuth"));
const HeaderUser = loadable(() => import("./HeaderUser"));


class Header extends React.Component {
    static contextTypes = {
        location: PropTypes.object,
        router: PropTypes.object
    };

    render() {
        let {
            passwordAccount
        } = this.props;

        // console.log("passwordAccount", passwordAccount, this.props);

        if( !passwordAccount ){
            return <HeaderAuth {...this.props} />;
        } else {
            return <HeaderUser {...this.props} />;
        }
    }
}

export default connect(
    Header,
    {
        listenTo() {
            return [
                AccountStore,
                WalletUnlockStore,
                WalletManagerStore,
                SettingsStore,
                GatewayStore
            ];
        },
        getProps() {
            const chainID = Apis.instance().chain_id;
            return {
                backedCoins: GatewayStore.getState().backedCoins,
                linkedAccounts: AccountStore.getState().linkedAccounts,
                currentAccount:
                    AccountStore.getState().currentAccount ||
                    AccountStore.getState().passwordAccount,
                passwordAccount: AccountStore.getState().passwordAccount,
                myActiveAccounts: AccountStore.getState().myActiveAccounts,
                myHiddenAccounts: AccountStore.getState().myHiddenAccounts,
                wallet_name: AccountStore.getState().wallet_name,
                locked: WalletUnlockStore.getState().locked,
                current_wallet: WalletManagerStore.getState().current_wallet,
                lastMarket: SettingsStore.getState().viewSettings.get(
                    `lastMarket${chainID ? "_" + chainID.substr(0, 8) : ""}`
                ),
                starredAccounts: AccountStore.getState().starredAccounts,
                passwordLogin: SettingsStore.getState().settings.get(
                    "passwordLogin"
                ),
                currentLocale: SettingsStore.getState().settings.get("locale"),
                hiddenAssets: SettingsStore.getState().hiddenAssets,
                settings: SettingsStore.getState().settings,
                locales: SettingsStore.getState().defaults.locale,
                defaults: SettingsStore.getState().defaults["unit"]
            };
        }
    }
);


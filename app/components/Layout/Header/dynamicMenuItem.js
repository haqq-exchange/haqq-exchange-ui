import React from "react";
import Translate from "react-translate-component";

export default class DinamicMenu extends React.PureComponent {
    static defaultProps = {
        findAction: {
            transfer: {
                exclude: "transfer"
            },
            "deposit-withdraw": {
                exclude: "deposit-withdraw"
            },
            news: {
                exclude: "news"
            },
            help: {
                exclude: "help"
            },
            voting: {
                exclude: "/voting"
            },
            assets: {
                find: "explorer",
                exclude: "/assets"
            },
            signedmessages: {
                exclude: "/signedmessages"
            },
            "member-stats": {
                exclude: "/member-stats"
            },
            vesting: {
                exclude: "/vesting"
            },
            whitelist: {
                exclude: "/whitelist"
            },
            permissions: {
                exclude: "/permissions"
            }
        },
        menuItem: {
            transfer: {
                content: "header.payments",
                className: "column-hide-small",
                component: "span"
            },
            "deposit-withdraw": {
                content: "header.deposit-withdraw",
                className: "column-hide-small",
                component: "span"
            },
            news: {
                content: "news.news",
                className: "column-hide-small",
                component: "span"
            },
            help: {
                content: "header.help",
                className: "column-hide-small",
                component: "span"
            },
            voting: {
                content: "account.voting",
                className: "column-hide-small",
                component: "span"
            },
            assets: {
                content: "explorer.assets.title",
                className: "column-hide-small",
                component: "span"
            },
            explorer: {
                content: "explorer.assets.title",
                className: "column-hide-small",
                component: "span"
            },
            signedmessages: {
                content: "account.signedmessages.menuitem",
                className: "column-hide-small",
                component: "span"
            },
            "member-stats": {
                content: "account.member.stats",
                className: "column-hide-small",
                component: "span"
            },
            vesting: {
                content: "account.vesting.title",
                className: "column-hide-small",
                component: "span"
            },
            whitelist: {
                content: "account.whitelist.title",
                className: "column-hide-small",
                component: "span"
            },
            permissions: {
                content: "account.permissions",
                className: "column-hide-small",
                component: "span"
            }
        }
    };

    render() {
        const {active, findAction, menuItem} = this.props;
        let keyMenu;

        for (let prop in findAction) {
            if (findAction.hasOwnProperty(prop)) {
                let findItem = findAction[prop];
                if (!keyMenu && active.indexOf(findItem.exclude) !== -1) {
                    if (findItem.find && active.indexOf(findItem.find) === -1) {
                        keyMenu = prop;
                    } else if (!findItem.find) {
                        keyMenu = prop;
                    }
                }
            }
        }

        return keyMenu ? (
            <a className={"active"}>
                <Translate {...menuItem[keyMenu]} />
            </a>
        ) : null;
    }
}

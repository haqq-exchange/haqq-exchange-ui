import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { connect } from "alt-react";
import ActionSheet from "react-foundation-apps/src/action-sheet";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import SettingsStore from "stores/SettingsStore";
import SettingsActions from "actions/SettingsActions";
import ZfApi from "react-foundation-apps/src/utils/foundation-api";
import SendModal from "../../Modal/SendModal";
import DepositModal from "../../Modal/DepositModal";
import Icon from "../../Icon/Icon";
import Translate from "react-translate-component";
import counterpart from "counterpart";
import WalletDb from "stores/WalletDb";
import WalletUnlockActions from "actions/WalletUnlockActions";
import cnames from "classnames";
import TotalBalanceValue from "../../Utility/TotalBalanceValue";
import ReactTooltip from "react-tooltip";
import { Apis } from "deexjs-ws";
import notify from "actions/NotificationActions";
import AccountImage from "../../Account/AccountImage";
import DinamicMenu from "./dynamicMenuItem";
import ChangeThemes from "./ChangeThemes";
import ChangeDropDownCash from "./ChangeDropDownCash";
import ClickOutside from "react-click-outside";
import { ChainStore } from "deexjs";

// var logo = require("assets/logo-ico.png");

// const FlagImage = ({flag, width = 20, height = 20}) => {
//     return <img height={height} width={width} src={`${__BASE_URL__}language-dropdown/${flag.toUpperCase()}.png`} />;
// };

const SUBMENUS = {
    SETTINGS: "SETTINGS"
};

export default class HeaderUser extends React.Component {
    static contextTypes = {
        location: PropTypes.object,
        router: PropTypes.object
    };

    constructor(props, context) {
        super();
        console.log("context.router", context.router, props);
        this.state = {
            active: context.router.route.location.pathname,
            accountsListDropdownActive: false,
            dropdownCurrencyActive: false
        };

        this.unlisten                    = null;
        this._toggleAccountDropdownMenu  = this._toggleAccountDropdownMenu.bind(
            this
        );
        this._toggleDropdownMenu         = this._toggleDropdownMenu.bind(this);
        this._closeDropDownCurrencyMenu  = this._closeDropDownCurrencyMenu.bind(
            this
        );
        this._toggleDropdownCurrencyMenu = this._toggleDropdownCurrencyMenu.bind(
            this
        );
        this._closeDropdown              = this._closeDropdown.bind(this);
        this._closeDropdownSubmenu       = this._closeDropdownSubmenu.bind(this);
        this._toggleDropdownSubmenu      = this._toggleDropdownSubmenu.bind(this);
        this._closeMenuDropdown          = this._closeMenuDropdown.bind(this);
        this._closeAccountsListDropdown  = this._closeAccountsListDropdown.bind(
            this
        );
        this.onBodyClick                 = this.onBodyClick.bind(this);
        this._onNavigate                 = this._onNavigate.bind(this);
    }

    UNSAFE_componentWillMount() {
        this.unlisten = this.context.router.history.listen((newState, err) => {
            if (!err) {
                if (this.unlisten && this.state.active !== newState.pathname) {
                    this.setState({
                        active: newState.pathname
                    });
                }
            }
        });
    }

    componentDidMount() {
        setTimeout(() => {
            ReactTooltip.rebuild();
        }, 1250);

        /*document.body.addEventListener("click", this.onBodyClick, {
            capture: false,
            passive: true
        });*/
    }

    componentWillUnmount() {
        if (this.unlisten) {
            this.unlisten();
            this.unlisten = null;
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (
            nextProps.linkedAccounts !== this.props.linkedAccounts ||
            nextProps.currentAccount !== this.props.currentAccount ||
            nextProps.passwordLogin !== this.props.passwordLogin ||
            nextProps.locked !== this.props.locked ||
            nextProps.current_wallet !== this.props.current_wallet ||
            nextProps.lastMarket !== this.props.lastMarket ||
            nextProps.starredAccounts !== this.props.starredAccounts ||
            nextProps.currentLocale !== this.props.currentLocale ||
            nextState.active !== this.state.active ||
            nextState.hiddenAssets !== this.props.hiddenAssets ||
            nextState.dropdownActive !== this.state.dropdownActive ||
            nextState.dropdownSubmenuActive !==
            this.state.dropdownSubmenuActive ||
            nextState.accountsListDropdownActive !==
            this.state.accountsListDropdownActive ||
            nextProps.height !== this.props.height
        );
    }

    _showSend(e) {
        e.preventDefault();
        this.send_modal.show();
        this._closeDropdown();
    }

    _showDeposit(e) {
        e.preventDefault();
        this.deposit_modal_new.show();
        this._closeDropdown();
    }

    _triggerMenu(e) {
        e.preventDefault();
        ZfApi.publish("mobile-menu", "toggle");
    }

    _toggleLock(e) {
        e.preventDefault();
        e.stopPropagation();
        if (WalletDb.isLocked()) {
            WalletUnlockActions.unlock().then(() => {
                AccountActions.tryToSetCurrentAccount();
            });
        } else {
            this._lockAccount();
        }
        this._closeDropdown();
    }

    _lockAccount() {
        WalletUnlockActions.lock();
        AccountStore.removeCurrentAccount().then(() => {
            document.location.assign("/");
        });
    }

    _onNavigate(route, e) {
        e.preventDefault();

        // Set Accounts Tab as active tab
        if (route === "/accounts") {
            SettingsActions.changeViewSetting({
                dashboardEntry: "accounts"
            });
        }
        if (route !== this.state.active) {
            this.props.history.push(route);
            this.setState({
                active: route
            });
        }
        this._closeDropdown();
    }

    _closeAccountsListDropdown() {
        this.setState({
            accountsListDropdownActive: false
        });
    }

    _closeMenuDropdown() {
        this.setState({
            dropdownActive: false
        });
    }

    _closeDropdownSubmenu() {
        this.setState({
            dropdownSubmenuActive: false
        });
    }

    _closeDropdown() {
        this._closeMenuDropdown();
        this._closeAccountsListDropdown();
        this._closeDropdownSubmenu();
    }

    _onGoBack(e) {
        e.preventDefault();
        window.history.back();
    }

    _onGoForward(e) {
        e.preventDefault();
        window.history.forward();
    }

    _accountClickHandler(account_name, e) {
        e.preventDefault();
        ZfApi.publish("account_drop_down", "close");
        if (this.context.router.route.location.pathname.indexOf("/account/") !== -1) {
            let currentPath = this.context.router.route.location.pathname.split("/");
            currentPath[2]  = account_name;
            this.props.history.push(currentPath.join("/"));
        }
        if (account_name !== this.props.currentAccount) {
            AccountActions.setCurrentAccount.defer(account_name);
            notify.addNotification({
                message: counterpart.translate("header.account_notify", {
                    account: account_name
                }),
                level: "success",
                autoDismiss: 2,
                position: "br"
            });
            this._closeDropdown();
        }
        // this.onClickUser(account_name, e);
    }

    _toggleAccountDropdownMenu() {
        // prevent state toggling if user cannot have multiple accounts

        const hasLocalWallet = !!WalletDb.getWallet();

        if (!hasLocalWallet) return false;
    }

    _toggleDropdownSubmenu(item = this.state.dropdownSubmenuActiveItem, e) {
        if (e) e.stopPropagation();

        this.setState({
            dropdownSubmenuActive: !this.state.dropdownSubmenuActive,
            dropdownSubmenuActiveItem: item
        });
    }

    _toggleDropdownMenu() {
        this.setState({
            dropdownActive: !this.state.dropdownActive
        });
    }

    _closeDropDownCurrencyMenu() {
        if (this.state.dropdownCurrencyActive) {
            this.setState({
                dropdownCurrencyActive: false
            });
        }
    }

    _toggleDropdownCurrencyMenu() {
        this.setState({
            dropdownCurrencyActive: !this.state.dropdownCurrencyActive
        });
    }

    _onClickCurrency(item) {
        SettingsActions.changeSetting({
            setting: "unit",
            value: item
        });
    }

    onBodyClick() {
        let insideMenuDropdown    = document.querySelectorAll(
            ".menu-dropdown-wrapper.active"
        );
        let insideAccountDropdown = document.querySelectorAll(
            ".account-dropdown-wrapper.active"
        );

        if (insideAccountDropdown.length) {
            this._closeAccountsListDropdown();
        }

        if (insideMenuDropdown.length) {
            this._closeMenuDropdown();
            this._closeDropdownSubmenu();
        }
    }

    _onLinkAccount() {
        AccountActions.linkAccount(this.props.currentAccount);
    }

    _onUnLinkAccount() {
        AccountActions.unlinkAccount(this.props.currentAccount);
    }

    render() {
        const { settings } = SettingsStore.getState();
        let {
                  history,
                  locked,
                  currentAccount,
                  starredAccounts,
                  passwordLogin,
                  passwordAccount,
                  height
              }            = this.props;

        let active          = history.location.pathname;
        let tradingAccounts = AccountStore.getMyAccounts();
        let maxHeight       = Math.max(40, height - 67 - 36) + "px";

        let currentAccountShort = currentAccount
                                  ? currentAccount.slice(0, 8)
                                  : null;
        if (
            currentAccountShort &&
            currentAccountShort.length < currentAccount.length
        ) {
            currentAccountShort += "...";
        }

        const a = ChainStore.getAccount(currentAccount);
        const isMyAccount = !a ? false : AccountStore.isMyAccount(a) || (passwordLogin && currentAccount === passwordAccount);
        // const isContact = this.props.linkedAccounts.has(currentAccount);
        const enableDepositWithdraw = Apis.instance().chain_id.substr(0, 8) === "c6930e40" && isMyAccount;

        if (starredAccounts.size) {
            for (let i = tradingAccounts.length - 1 ; i >= 0 ; i--) {
                if (!starredAccounts.has(tradingAccounts[i])) {
                    tradingAccounts.splice(i, 1);
                }
            }
            starredAccounts.forEach(account => {
                if (tradingAccounts.indexOf(account.name) === -1) {
                    tradingAccounts.push(account.name);
                }
            });
        }

        let myAccounts     = AccountStore.getMyAccounts();
        let myAccountCount = myAccounts.length;

        let walletBalance =
                myAccounts.length && currentAccount ? (
                    <div className="total-value">
                        <TotalBalanceValue.AccountWrapper
                            hiddenAssets={this.props.hiddenAssets}
                            accounts={[this.props.currentAccount]}
                            noTip
                            style={{ minHeight: 15 }}
                        />
                    </div>
                ) : null;

        let tradeUrl = ["/market", this.props.lastMarket || "DEEX_BTC"].join("/");

        let dashboard = (
            <Link to={tradeUrl} className={cnames("logo")}>
                <span className="logo__logotype"/>
            </Link>
        );

        let createAccountLink =
                myAccountCount === 0 ? (
                    <ActionSheet.Button title="" setActiveState={() => {
                    }}>
                        <a
                            className="button create-account"
                            onClick={this._onNavigate.bind(this, "/create-account")}
                            style={{ padding: "1rem", border: "none" }}
                        >
                            <Icon className="icon-14px" name="user"/>{" "}
                            <Translate content="header.create_account"/>
                        </a>
                    </ActionSheet.Button>
                ) : null;

        // Account selector: Only active inside the exchange
        let account_display_name, accountsList;
        if (currentAccount) {
            account_display_name =
                currentAccount.length > 20
                ? `${currentAccount.slice(0, 20)}..`
                : currentAccount;
            if (tradingAccounts.indexOf(currentAccount) < 0 && isMyAccount) {
                tradingAccounts.push(currentAccount);
            }
            if (tradingAccounts.length >= 1) {
                accountsList = tradingAccounts
                    .sort()
                    .filter(name => name !== currentAccount)
                    .map(name => {
                        return (
                            <li
                                key={name}
                                className={cnames({
                                    active:
                                    active
                                        .replace("/account/", "")
                                        .indexOf(name) === 0
                                })}
                                onClick={this._accountClickHandler.bind(
                                    this,
                                    name
                                )}
                            >
                                <div
                                    style={{ paddingTop: 0 }}
                                    className="table-cell"
                                >
                                    <AccountImage
                                        style={{ position: "relative", top: 4 }}
                                        size={{ height: 20, width: 20 }}
                                        account={name}
                                    />
                                </div>
                                <div
                                    className="table-cell"
                                    style={{ paddingLeft: 10 }}
                                >
                                    <a
                                        className={
                                            "lower-case" +
                                            (name === account_display_name
                                             ? " current-account"
                                             : "")
                                        }
                                    >
                                        {name}
                                    </a>
                                </div>
                            </li>
                        );
                    });
            }
        }

        const submenus = {
            [SUBMENUS.SETTINGS]: (
                <ul
                    className="dropdown header-menu header-submenu"
                    style={{
                        left: 0,
                        top: 60,
                        maxHeight: !this.state.dropdownActive ? 0 : maxHeight,
                        overflowY: "auto"
                    }}
                >
                    <li
                        className="divider parent-item"
                        onClick={this._toggleDropdownSubmenu.bind(
                            this,
                            undefined
                        )}
                    >
                        <div className="table-cell">
                            <span className="parent-item-icon">&lt;</span>
                            <Translate
                                content="header.settings"
                                component="span"
                                className="parent-item-name"
                            />
                        </div>
                    </li>
                    <hr className="table-cell-hr"/>
                    <li
                        onClick={this._onNavigate.bind(
                            this,
                            "/settings/general"
                        )}
                    >
                        <Translate
                            content="settings.general"
                            component="div"
                            className="table-cell"
                        />
                    </li>
                    {!this.props.settings.get("passwordLogin") && (
                        <li
                            onClick={this._onNavigate.bind(
                                this,
                                "/settings/wallet"
                            )}
                        >
                            <Translate
                                content="settings.wallet"
                                component="div"
                                className="table-cell"
                            />
                        </li>
                    )}
                    <li
                        onClick={this._onNavigate.bind(
                            this,
                            "/settings/accounts"
                        )}
                    >
                        <Translate
                            content="settings.accounts"
                            component="div"
                            className="table-cell"
                        />
                    </li>

                    {!this.props.settings.get("passwordLogin") && [
                        <li
                            key={"settings.password"}
                            onClick={this._onNavigate.bind(
                                this,
                                "/settings/password"
                            )}
                        >
                            <Translate
                                content="settings.password"
                                component="div"
                                className="table-cell"
                            />
                        </li>,
                        <li
                            key={"settings.backup"}
                            onClick={this._onNavigate.bind(
                                this,
                                "/settings/backup"
                            )}
                        >
                            <Translate
                                content="settings.backup"
                                component="div"
                                className="table-cell"
                            />
                        </li>
                    ]}
                    <li
                        onClick={this._onNavigate.bind(
                            this,
                            "/settings/restore"
                        )}
                    >
                        <Translate
                            content="settings.restore"
                            component="div"
                            className="table-cell"
                        />
                    </li>
                    <li
                        onClick={this._onNavigate.bind(
                            this,
                            "/settings/access"
                        )}
                    >
                        <Translate
                            content="settings.access"
                            component="div"
                            className="table-cell"
                        />
                    </li>
                    <li
                        onClick={this._onNavigate.bind(
                            this,
                            "/settings/faucet_address"
                        )}
                    >
                        <Translate
                            content="settings.faucet_address"
                            component="div"
                            className="table-cell"
                        />
                    </li>
                    <li
                        onClick={this._onNavigate.bind(this, "/settings/reset")}
                    >
                        <Translate
                            content="settings.reset"
                            component="div"
                            className="table-cell"
                        />
                    </li>
                </ul>
            )
        };

        return (
            <div className="header-container" style={{ minHeight: "60px" }}>
                <div
                    className="header menu-group primary"
                    style={{ flexWrap: "nowrap", justifyContent: "none" }}>
                    <ul className="menu-bar">
                        <li>{dashboard}</li>
                        <li>
                            <Link
                                style={{ flexFlow: "row" }}
                                to={"/"}
                                className={cnames({ active: active === "/" })}>
                                <Translate
                                    className="column-hide-small"
                                    content="header.markets"
                                />
                            </Link>
                        </li>
                        {!currentAccount || !!createAccountLink ? null : (
                            <li>
                                <Link
                                    style={{ flexFlow: "row" }}
                                    to={`/account/${currentAccount}`}
                                    className={cnames({
                                        active:
                                        active.indexOf("account/") !== -1 &&
                                        active.indexOf("/account/") !==
                                        -1 &&
                                        active.indexOf("/assets") === -1 &&
                                        active.indexOf("/voting") === -1 &&
                                        active.indexOf(
                                            "/signedmessages"
                                        ) === -1 &&
                                        active.indexOf("/member-stats") ===
                                        -1 &&
                                        active.indexOf("/vesting") === -1 &&
                                        active.indexOf("/whitelist") ===
                                        -1 &&
                                        active.indexOf("/permissions") ===
                                        -1
                                    })}
                                >
                                    <Translate
                                        className="column-hide-small"
                                        content="header.dashboard"
                                    />
                                </Link>
                            </li>
                        )}
                        <li>
                            <Link to={tradeUrl} style={{ flexFlow: "row" }}
                                  className={cnames(
                                      {
                                          "column-hide-xxs": active.indexOf("/market") === 0,
                                          active: active.indexOf("/market") === 0
                                      }
                                  )}>
                                <Translate
                                    className="column-hide-small"
                                    component="span"
                                    content="header.exchange"
                                />
                            </Link>
                        </li>
                        <li>
                            <Link to={"/settings"} style={{ flexFlow: "row" }}
                                  className={cnames(
                                      {
                                          "column-hide-xxs": active.indexOf("/settings") === 0,
                                          active: active.indexOf("/settings") === 0
                                      }
                                  )}>
                                <Translate
                                    className="column-hide-small"
                                    component="span"
                                    content="header.settings"
                                />
                            </Link>
                        </li>
                        {/* Dynamic Menu Item */}
                        <li>
                            <DinamicMenu
                                {...this.props}
                                {...{ active: active }}
                            />
                        </li>
                    </ul>
                </div>
                <div className="account-wrapper">
                    {__SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__ ? null :
                    <ChangeThemes settings={settings}/>}

                    <ClickOutside className={"app-menu-outside"} onClickOutside={this.onBodyClick}>
                        <div className="app-menu">
                            <div
                                onClick={this._toggleDropdownMenu}
                                className={cnames(
                                    "menu-dropdown-wrapper dropdown-wrapper",
                                    { active: this.state.dropdownActive }
                                )}
                            >
                                <div className="menu-wrapper">
                                    <div>
                                        {this.props.currentAccount ==
                                         null ? null : (
                                                <span
                                                     onClick={this._toggleLock.bind(
                                                         this
                                                     )}
                                                     style={{ cursor: "pointer" }}
                                                 >
                                                <Icon
                                                    className="lock-unlock"
                                                    name={
                                                        locked
                                                        ? "locked"
                                                        : "unlocked"
                                                    }
                                                />
                                                </span>
                                         )}
                                    </div>
                                    {locked ? (
                                        <Translate
                                            className="text account-name"
                                            component="div"
                                            content="header.unlock_short"
                                        />
                                    ) : (
                                         <div className="text account-name">
                                             {currentAccountShort}
                                         </div>
                                     )}
                                </div>
                                <div className="menu-wrapper">
                                    {/*<div className="notice">2</div>*/}
                                    <div className="app-menu-arrow"/>
                                </div>

                                {(this.state.dropdownSubmenuActive &&
                                    submenus[
                                        this.state.dropdownSubmenuActiveItem
                                        ] &&
                                    submenus[
                                        this.state.dropdownSubmenuActiveItem
                                        ]) || (
                                    <ul
                                        className="dropdown header-menu"
                                        style={{
                                            left: 0,
                                            top: 60,
                                            maxHeight: !this.state
                                                .dropdownActive
                                                       ? 0
                                                       : maxHeight,
                                            overflowY: "auto"
                                        }}
                                    >
                                        <li
                                            className="divider"
                                            onClick={this._toggleLock.bind(
                                                this
                                            )}>
                                            <div className="table-cell">
                                                <Translate
                                                    content={`header.${
                                                        this.props.locked
                                                        ? "unlock_short"
                                                        : "lock_short"
                                                        }`}
                                                />
                                            </div>
                                        </li>

                                        {this.props.locked && (
                                            <li
                                                className="divider"
                                                onClick={this._lockAccount.bind(
                                                    this
                                                )}>
                                                <div className="table-cell">
                                                    <Translate content="header.lock_short"/>
                                                </div>
                                            </li>
                                        )}

                                        <hr className="table-cell-hr"/>

                                        <li
                                            className={cnames(
                                                {
                                                    active:
                                                    active.indexOf(
                                                        "/deposit-withdraw"
                                                    ) !== -1
                                                },
                                                {
                                                    disabled: !enableDepositWithdraw
                                                }
                                            )}
                                            onClick={
                                                !enableDepositWithdraw
                                                ? () => {}
                                                : e => {
// set withdraw/deposite
                                                    SettingsActions.changeViewSetting({ ["tdexAction"]: "deposit" });
                                                    this._onNavigate(
                                                        "/deposit-withdraw",
                                                        e
                                                    );
                                                }
                                            }
                                        >
                                            <div className="table-cell">
                                                <Translate content="gateway.deposit"/>
                                            </div>
                                        </li>
                                        <li
                                            className={cnames(
                                                "divider",
                                                {
                                                    active:
                                                    active.indexOf(
                                                        "/deposit-withdraw"
                                                    ) !== -1
                                                },
                                                {
                                                    disabled: !enableDepositWithdraw
                                                },
                                                "notice-wrapper"
                                            )}
                                            onClick={
                                                !enableDepositWithdraw
                                                ? () => {
                                                }
                                                : e => {
// set withdraw/deposite
                                                    SettingsActions.changeViewSetting(
                                                        {
                                                            ["tdexAction"]:
                                                                "withdraw"
                                                        }
                                                    );

                                                    this._onNavigate(
                                                        "/deposit-withdraw",
                                                        e
                                                    );
                                                }
                                            }
                                        >
                                            <div className="table-cell">
                                                <Translate content="modal.withdraw.submit"/>
                                            </div>
                                            {/*<div className="notice">1</div>*/}
                                        </li>
                                        <li
                                            className={cnames(
                                                {
                                                    active:
                                                    active.indexOf(
                                                        "/settings"
                                                    ) !== -1
                                                },
                                                "divider",
                                                "mobile-only",
                                                "has-submenu"
                                            )}
                                            onClick={this._toggleDropdownSubmenu.bind(
                                                this,
                                                SUBMENUS.SETTINGS
                                            )}
                                        >
                                            <div className="table-cell">
                                                <Translate content="header.settings"/>{" "}
                                            </div>
                                        </li>

                                        <hr className="table-cell-hr"/>

                                        <li
                                            className={cnames(
                                                {
                                                    active:
                                                    active.indexOf(
                                                        "/news"
                                                    ) !== -1
                                                },
                                                "notice-wrapper"
                                            )}
                                        >
                                            <a
                                                href="https://www.deex.blog/"
                                                target="_blank"
                                                rel="noreferrer"
                                                style={{ color: "#fff" }}
                                            >
                                                <div className="table-cell">
                                                    <Translate content="news.news"/>
                                                </div>
                                            </a>
                                            {/*<div className="notice">322</div>*/}
                                        </li>
                                        <li
                                            className={cnames(
                                                {
                                                    active:
                                                    active.indexOf(
                                                        "/help/introduction/bitshares"
                                                    ) !== -1
                                                },
                                                "divider"
                                            )}
                                        >
                                            <a
                                                href="https://deexhelp.zendesk.com"
                                                target="_blank"
                                                style={{ color: "#fff" }}
                                            >
                                                <div className="table-cell">
                                                    <Translate content="header.help"/>
                                                </div>
                                            </a>
                                        </li>

                                        <hr className="table-cell-hr"/>

                                        <li
                                            className={cnames({
                                                active:
                                                active.indexOf(
                                                    "/voting"
                                                ) !== -1
                                            })}
                                            onClick={this._onNavigate.bind(
                                                this,
                                                `/account/${currentAccount}/voting`
                                            )}
                                        >
                                            <div className="table-cell">
                                                <Translate content="account.voting"/>
                                            </div>
                                        </li>
                                        <li
                                            className={cnames({
                                                active:
                                                active.indexOf(
                                                    "/whitelist"
                                                ) !== -1
                                            })}
                                            onClick={this._onNavigate.bind(
                                                this,
                                                `/account/${currentAccount}/whitelist`
                                            )}
                                        >
                                            <div className="table-cell">
                                                <Translate content="account.whitelist.title"/>
                                            </div>
                                        </li>
                                        <li
                                            className={cnames("divider", {
                                                active:
                                                active.indexOf(
                                                    "/permissions"
                                                ) !== -1
                                            })}
                                            onClick={this._onNavigate.bind(
                                                this,
                                                `/account/${currentAccount}/permissions`
                                            )}
                                        >
                                            <div className="table-cell">
                                                <Translate content="account.permissions"/>
                                            </div>
                                        </li>

                                        <hr className="table-cell-hr"/>

                                        <li
                                            className={cnames({
                                                active:
                                                active.indexOf(
                                                    "/settings"
                                                ) !== -1
                                            })}
                                            onClick={this._onNavigate.bind(
                                                this,
                                                "/settings"
                                            )}
                                        >
                                            <div className="table-cell">
                                                <Translate content="header.settings"/>
                                            </div>
                                        </li>
                                    </ul>
                                )}
                            </div>
                        </div>
                    </ClickOutside>
                    {!locked && (
                        <CurrentCash
                            currentAccount={currentAccount}
                            walletBalance={walletBalance}
                        />
                    )}
                    {!locked && <ChangeDropDownCash {...this.props} />}
                </div>
                <SendModal
                    id="send_modal_header"
                    ref={(r) => {
                        this.send_modal = r;
                    }}
                    from_name={currentAccount}
                />

                <DepositModal
                    ref={(r) => {
                        this.deposit_modal_new = r;
                    }}
                    modalId="deposit_modal_new"
                    account={currentAccount}
                    backedCoins={this.props.backedCoins}
                />
            </div>
        );
    }
}

const CurrentCash = props => {
    return (
        <div className="account-cash" style={{ paddingRight: "5px" }}>
            <div className="table-cell">
                <Icon name="wallet"/>
            </div>
            <Link to={`/account/${props.currentAccount}`}>
                {props.walletBalance}
            </Link>
        </div>
    );
};



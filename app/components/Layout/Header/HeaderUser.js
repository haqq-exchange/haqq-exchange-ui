import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";
import {Link, NavLink} from "react-router-dom";
import AccountStore from "stores/AccountStore";
import SettingsStore from "stores/SettingsStore";
import Icon from "../../Icon/Icon";
import TotalBalanceValue from "../../Utility/TotalBalanceValue";
import ChangeThemes from "./ChangeThemes";
import LogoType from "./LogoType";
import FlagDropdown from "Components/Layout/Header/FlagDropdown";
import MenuNav from "./MenuNav";
import UserMenuLinks from "./UserMenuLinks";
import MediaQuery from "react-responsive";
import ResponseOption from "config/response";
import "./header.scss";
import "../../MainPage/MainPage.scss";

export default class HeaderUser extends React.Component {
    static contextTypes = {
        location: PropTypes.object,
        router: PropTypes.object
    };

    constructor(props, context) {
        super();
        this.state = {
            showNavMenu: false
            //  active: context.router.route.location.pathname,
            //  accountsListDropdownActive: false,
            //  dropdownCurrencyActive: false
        };

        this.unlisten = null;
        // this._toggleAccountDropdownMenu = this._toggleAccountDropdownMenu.bind(this);
        // this._toggleDropdownMenu = this._toggleDropdownMenu.bind(this);
        // this._closeDropDownCurrencyMenu = this._closeDropDownCurrencyMenu.bind(this);
        // this._toggleDropdownCurrencyMenu = this._toggleDropdownCurrencyMenu.bind(this);
        // this._closeDropdown = this._closeDropdown.bind(this);
        // this._closeDropdownSubmenu = this._closeDropdownSubmenu.bind(this);
        // this._toggleDropdownSubmenu = this._toggleDropdownSubmenu.bind(this);
        // this._closeMenuDropdown = this._closeMenuDropdown.bind(this);
        // this._closeAccountsListDropdown = this._closeAccountsListDropdown.bind(this);
        // this.onBodyClick = this.onBodyClick.bind(this);
    }

    UNSAFE_componentWillMount() {
        this.unlisten = this.context.router.history.listen((newState, err) => {
            if (!err) {
                if (this.unlisten && this.state.active !== newState.pathname) {
                    this.setState({
                        active: newState.pathname
                    });
                }
            }
        });
    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    shouldComponentUpdate(nextProps, nextState) {
        return (
            nextProps.linkedAccounts !== this.props.linkedAccounts ||
            nextProps.currentAccount !== this.props.currentAccount ||
            nextProps.passwordLogin !== this.props.passwordLogin ||
            nextProps.locked !== this.props.locked ||
            nextProps.current_wallet !== this.props.current_wallet ||
            nextProps.lastMarket !== this.props.lastMarket ||
            nextProps.starredAccounts !== this.props.starredAccounts ||
            nextProps.currentLocale !== this.props.currentLocale ||
            nextState.active !== this.state.active ||
            nextState.hiddenAssets !== this.props.hiddenAssets ||
            nextState.dropdownActive !== this.state.dropdownActive ||
            nextState.dropdownSubmenuActive !==
            this.state.dropdownSubmenuActive ||
            nextState.accountsListDropdownActive !==
            this.state.accountsListDropdownActive ||
            nextProps.height !== this.props.height
        );
    }







    render() {
        const {settings} = SettingsStore.getState();
        let { showNavMenu } = this.state;
        let { lastMarket, history, passwordAccount, locked } = this.props;


        // console.log("locked", locked );

        /*let marketLink = "/";
        if( history.location.pathname.indexOf("/market") === -1 ) {
            marketLink = ["/market", lastMarket || "DEEX_BTS"].join("/");
        }*/
        //console.log("history.location.pathname", history.location.pathname);

        let myAccounts = AccountStore.getMyAccounts();

        let walletBalance =
            myAccounts.length && passwordAccount ? (
                <div className="total-value">
                    <TotalBalanceValue.AccountWrapper
                        hiddenAssets={this.props.hiddenAssets}
                        accounts={[this.props.passwordAccount]}
                        noTip
                        style={{minHeight: 15}}
                    />
                </div>
            ) : null;

        return (
            <div className="header-container" >
                <div className="header-container-wrap">
                    <div className={"header-container-left"}>
                        {__GBL_CHAIN__ || __GBLTN_CHAIN__ ? 
                        <NavLink activeClassName="active" exact to={"/"} className={"header-logo logo"} style={{height: "60px", width: "206px"}} >
                             <div  style={{backgroundImage: "url("+ `https://static.haqq.exchange/gbl/images/logo-gbl.png` +")", height: "60px", width: "205px", margin: "0 auto", backgroundRepeat: "no-repeat"}} />
                        </NavLink>
                        : (__SCROOGE_CHAIN__ ? 
                        <NavLink activeClassName="active" exact to={"/"} className={"header-logo logo main-scrooge-logo"} >
                            <div className="scrooge-logo" /> 
                        </NavLink>
                        :
                        <NavLink activeClassName="active" exact to={"/"} className={"header-logo logo"} >
                            <div className="logo-logotype" /> 
                            {/* <LogoType /> */}
                        </NavLink>)}
                        <MediaQuery {...ResponseOption.notMobile}>
                            <MenuNav
                                locked={locked}
                                clickedMenu={()=>{}}
                                currentAccount={passwordAccount} settings={settings}  lastMarket={lastMarket} />
                        </MediaQuery>
                    </div>
                    <div className={__SCROOGE_CHAIN__ ? "header-container-right-scrooge" : "header-container-right"}>

                        <UserMenuLinks {...this.props}/>

                        <MediaQuery {...ResponseOption.notMobile}>
                        {__SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__ ? null : <ChangeThemes settings={settings}/>}
                        </MediaQuery>

                        <FlagDropdown />

                        {/*<MediaQuery {...ResponseOption.notMobile}>
                            {!locked && <CurrentCash passwordAccount={passwordAccount} walletBalance={walletBalance}/>}
                            {!locked && <ChangeDropDownCash walletBalance={walletBalance} {...this.props} />}
                        </MediaQuery>*/}
                        <MediaQuery {...ResponseOption.mobile}>
                            <Icon className={"public-header-menu"} onClick={()=>this.setState({showNavMenu: !showNavMenu})} name={showNavMenu ? "menu-close" : "menu-open"} />
                        </MediaQuery>

                    </div>
                </div>
                <MediaQuery {...ResponseOption.mobile}>
                    <div className={cn("header-container-wrap menu-nav", {"hide": !showNavMenu})}>
                        <MenuNav
                            locked={locked}
                            clickedMenu={()=>this.setState({showNavMenu: !showNavMenu})}
                            currentAccount={passwordAccount} settings={settings} lastMarket={lastMarket} />
                    </div>
                </MediaQuery>


            </div>
        );
    }
}

const CurrentCash = props => {
    return (
        <div className="account-cash" style={{paddingRight: "5px"}}>
            <div className="table-cell">
                <Icon name="wallet"/>
            </div>
            <Link to={`/account/${props.passwordAccount}`}>
                {props.walletBalance}
            </Link>
        </div>
    );
};







import React from "react";
import cn from "classnames";
import {Link} from "react-router-dom";
import Icon from "../../Icon/Icon";
import Translate from "react-translate-component";
import ModalActions from "actions/ModalActions";

export default class WalletMenuLinks extends React.PureComponent {
    state = {
        isVisibleMenu: false,
        staticMenu: []
    };

    static defaultProps = {
    };

    componentDidMount() {

    }

    _onShowForm = (typeWindow, event) => {
        event.preventDefault();
        ModalActions.show("selected_deposit_withdraw", {
            typeWindow,
        });
    };
    _onShowFormTransfer = (typeWindow, event) => {
        event.preventDefault();
        let dataModal = {
            onShowForm: () => {
                ModalActions.hide("transfer_asset_modal");
            }
        };
        ModalActions.show("transfer_asset_modal", {
            typeWindow,
            dataModal
        });
    };



    onBodyClick = (isVisibleMenu) => {
        this.setState({isVisibleMenu});
    };

    createUserMenu(){
        const {className, currentAccount} = this.props;
        const {isVisibleMenu} = this.state;

        let staticMenu = [
            [{
                "content": "account.balance",
                "icon": "wallet",
                "attr": {
                    "to": ["","account", currentAccount].join("/"),
                    "onClick": () => {
                        this.onBodyClick(false);
                    }
                }
            },
            // {
            //     "content": "header.credit",
            //     "icon": "network_fees",
            //     "attr": {
            //         "to": ["","credit", "my"].join("/"),
            //         "onClick": () => {
            //             this.onBodyClick(false);
            //         }
            //     }
            // },
            {
                "content": "header.payments",
                "icon": "send",
                "attr": {
                    "to": "",
                    "onClick": (event) => this._onShowFormTransfer("sendTransfer", event)
                }
            },{
                "content": "account.deposit",
                "icon": "deposit",
                "attr": {
                    "to": "",
                    "onClick": (event) => this._onShowForm("deposit", event)
                }
            },{
                "content": "account.withdraw",
                "icon": "withdraw",
                "attr": {
                    "to": "",
                    "onClick": (event) => this._onShowForm("withdraw", event)
                }
            },/*{
                "content": "account.asset.deex_fiat",
                "icon": "buy",
                "attr": {
                    "to": "",
                    "onClick": (event) => {
                        event.preventDefault();
                        ModalActions.show("cryptoplat_io_buy");
                    }
                }
            }*/],
            [
                {
                    "content": "account.member.membership",
                    "icon": "membership",
                    "attr": {
                        "to": ["", "account", currentAccount, "member-stats"].join("/"),
                        "onClick": () => {
                            this.onBodyClick(false);
                        }
                    }
                }, 
                // {
                //     "content": "account.member.witness",
                //     "icon": "membership",
                //     "attr": {
                //         "to": ["", "account", currentAccount, "witness"].join("/"),
                //         "onClick": () => {
                //             this.onBodyClick(false);
                //         }
                //     }
                // }, 
                // {
                //     "content": "account.asset.active",
                //     "icon": "assets-header",
                //     "attr": {
                //         "to": ["", "account", currentAccount, "assets"].join("/"),
                //         "onClick": () => {
                //             this.onBodyClick(false);
                //         }
                //     }
                // },
                {
                    "content": "account.permissions",
                    "icon": "permissions",
                    "attr": {
                        "to": ["", "account", currentAccount, "permissions"].join("/"),
                        "onClick": () => {
                            this.onBodyClick(false);
                        }
                    }
                }, 
                {
                    "content": "account.whitelist.title",
                    "icon": "whitelist",
                    "attr": {
                        "to": ["", "account", currentAccount, "whitelist"].join("/"),
                        "onClick": () => {
                            this.onBodyClick(false);
                        }
                    }
                }/*, {
                    "content": "header.create_asset",
                    "icon": "link",
                    "attr": {
                        "to": ["", "account", currentAccount, "create-asset"].join("/"),
                        "onClick": () => {
                            this.onBodyClick(false);
                        }
                    }
                }*/
            ]
        ];

        let staticScroogeMenu = [
            [{
                "content": "account.balance",
                "icon": "wallet",
                "attr": {
                    "to": ["","account", currentAccount].join("/"),
                    "onClick": () => {
                        this.onBodyClick(false);
                    }
                }
            },{
                "content": "header.payments",
                "icon": "send",
                "attr": {
                    "to": "",
                    "onClick": (event) => this._onShowFormTransfer("sendTransfer", event)
                }
            },{
                "content": "account.deposit",
                "icon": "deposit",
                "attr": {
                    "to": "",
                    "onClick": (event) => this._onShowForm("deposit", event)
                }
            },{
                "content": "account.withdraw",
                "icon": "withdraw",
                "attr": {
                    "to": "",
                    "onClick": (event) => this._onShowForm("withdraw", event)
                }
            },
        ],
            // [ 
            //     {
            //         "content": "account.permissions",
            //         "icon": "permissions",
            //         "attr": {
            //             "to": ["", "account", currentAccount, "permissions"].join("/"),
            //             "onClick": () => {
            //                 this.onBodyClick(false);
            //             }
            //         }
            //     }
            // ]
        ];

        let staticGblMenu = [
            [{
                "content": "account.balance",
                "icon": "wallet",
                "attr": {
                    "to": ["","account", currentAccount].join("/"),
                    "onClick": () => {
                        this.onBodyClick(false);
                    }
                }
            },{
                "content": "header.payments",
                "icon": "send",
                "attr": {
                    "to": "",
                    "onClick": (event) => this._onShowFormTransfer("sendTransfer", event)
                }
            },{
                "content": "account.deposit",
                "icon": "deposit",
                "attr": {
                    "to": "",
                    "onClick": (event) => this._onShowForm("deposit", event)
                }
            },{
                "content": "account.withdraw",
                "icon": "withdraw",
                "attr": {
                    "to": "",
                    "onClick": (event) => this._onShowForm("withdraw", event)
                }
            },
        ],
            [ 
                {
                    "content": "account.permissions",
                    "icon": "permissions",
                    "attr": {
                        "to": ["", "account", currentAccount, "permissions"].join("/"),
                        "onClick": () => {
                            this.onBodyClick(false);
                        }
                    }
                }, 
                {
                    "content": "account.whitelist.title",
                    "icon": "whitelist",
                    "attr": {
                        "to": ["", "account", currentAccount, "whitelist"].join("/"),
                        "onClick": () => {
                            this.onBodyClick(false);
                        }
                    }
                },
                // {
                //     "content": "account.asset.active",
                //     "icon": "assets-header",
                //     "attr": {
                //         "to": ["", "account", currentAccount, "assets"].join("/"),
                //         "onClick": () => {
                //             this.onBodyClick(false);
                //         }
                //     }
                // },
                {
                    "content": "header.create_asset",
                    "icon": "wallet",
                    "attr": {
                        "to": ["", "account", currentAccount, "create-asset"].join("/"),
                        "onClick": () => {
                            this.onBodyClick(false);
                        }
                    }
                },
                // {
                //     "content": "explorer.proposals.title",
                //     "icon": "wallet",
                //     "attr": {
                //         "to": ["", "account", currentAccount, "proposals"].join("/"),
                //         "onClick": () => {
                //             this.onBodyClick(false);
                //         }
                //     }
                // },
            ]
        ];

        const walletMenu = __GBL_CHAIN__ || __GBLTN_CHAIN__ ? staticGblMenu : (__SCROOGE_CHAIN__ ? staticScroogeMenu : staticMenu);

        return (<div className={cn([className, "drop-down-list"].join("-"), {"active": isVisibleMenu})}>
            {walletMenu.map((menu, index) => {
                return (<div key={"user-menu-row-" + index} className={[className, "drop-down-row"].join("-")}>
                    {menu.map((item, keyItem)=> {
                        return (<div key={"user-menu-item-" + keyItem} className={[className, "drop-down-item"].join("-")}>
                            <Link className={[className, "drop-down-link"].join("-")} {...item.attr}>
                                <Icon name={item.icon}/>
                                <Translate content={item.content}/>
                            </Link>
                        </div>);
                    })}
                </div>);
            })}
        </div>);
    }
    render(){
        const {children, className} = this.props;
        const {isVisibleMenu} = this.state;

        // console.log("locked",locked);

        return(
            <div className={cn([className, "drop-down-wrap"].join("-"), {"active": isVisibleMenu})} onMouseEnter={()=>this.setState({isVisibleMenu: true})} onMouseLeave={()=>this.setState({isVisibleMenu: false})} >
                {children}
                {isVisibleMenu && (<div className={cn([className, "drop-down-block"].join("-"))}  >
                    {this.createUserMenu()}
                </div>)}
            </div>
        );
    }
}

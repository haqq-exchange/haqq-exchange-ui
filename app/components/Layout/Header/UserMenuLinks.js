import React from "react";
import ClickOutside from "react-click-outside";
import cnames from "classnames";
import {Link} from "react-router-dom";
import Icon from "../../Icon/Icon";
import Translate from "react-translate-component";
import WalletDb from "stores/WalletDb";
import WalletUnlockActions from "actions/WalletUnlockActions";
import ModalActions from "actions/ModalActions";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import {getRequestAddress} from "api/apiConfig";
import RefUtils from "../../../lib/common/ref_utils";
import accountUtils from "common/account_utils";
import SettingsActions from "../../../actions/SettingsActions";

export default class UserMenuLinks extends React.PureComponent {
    state = {
        isVisibleMenu: false,
        staticMenu: []
    };

    static defaultProps = {
        getUserSign: accountUtils.getUserSign,
        fetchRequest: new RefUtils({
            url: getRequestAddress("nodes")
        })
    };

    componentDidMount() {

    }


    toggleLock = (event) => {
        event.preventDefault();
        event.stopPropagation();
        if (WalletDb.isLocked()) {
            ModalActions.show("unlock_wallet_modal_public").then(() => {
                AccountActions.tryToSetCurrentAccount();
                /*if(this.props.history) {
                    let {pathname} = this.props.history.location;
                    this.props.history.push("/");
                    setTimeout(()=>{
                        this.props.history.push(pathname);
                    }, 300);
                }*/
            });
        } else {
            this._lockAccount();
        }
        this.onBodyClick(false);
    };

    _lockAccount = () => {
        const {history} = this.props;
        WalletUnlockActions.lock();
        AccountStore.removeCurrentAccount().then(() => {
            history.push("/");
        });
    };

    onBodyClick = (isVisibleMenu) => {
        this.setState({isVisibleMenu});
    };

    createUserMenu(){
        const {currentAccount, locked, settings} = this.props;

        console.log("settings", settings.toJS(), this.props);

        let lockShort = {
            "content": "header.lock_short",
            "icon": "logout",
            "attr": {
                "to": "",
                "onClick": this._lockAccount
            }
        };

        let staticMenu = [
            [/*{
                "content": "header.credit",
                "icon": "network_fees",
                "attr": {
                    "to": ["","credit"].join("/"),
                    "onClick": () => {
                        this.onBodyClick(false);
                    }
                }
            },*/
                {
                    "content": "account.history",
                    "icon": "transaction_history",
                    "attr": {
                        "to": ["","account", currentAccount, "activity"].join("/"),
                        "onClick": () => {
                            this.onBodyClick(false);
                        }
                    }
                },
                // {
                //     "content": "account.network_fees",
                //     "icon": "network_fees",
                //     "attr": {
                //         "to": ["", "networkfees"].join("/"),
                //         "onClick": () => {
                //             this.onBodyClick(false);
                //         }
                //     }
                // },
                // {
                //     "content": "account.voting",
                //     "icon": "voting",
                //     "attr": {
                //         "to": ["", "voting"].join("/"),
                //         "onClick": ()=>{this.onBodyClick(false);}
                //     }
                // },
                {
                    "content": "invoice.title",
                    "icon": "wallet",
                    "attr": {
                        "to": "/invoice-create",
                        "onClick": ()=>{this.onBodyClick(false);}
                    }
                }], [
            //         {
            //     "content": "node.title",
            //     "icon": "nodes",
            //     "attr": {
            //         "to": "/nodes",
            //         "onClick": () => {
            //             this.onBodyClick(false);
            //         }
            //     }
            // },
            {
                "content": "header.referrals",
                "icon": "referrals",
                "attr": {
                    "to": ["", "account", "referrals"].join("/"),
                    "onClick": () => {
                        this.onBodyClick(false);
                    }
                }
            }/*,{
                "content": ["settings", settings.get("themes")].join("."),
                "icon": settings.get("themes") === "lightTheme" ? "sun" : "moon",
                "attr": {
                    "to": "",
                    "onClick": (event) => {
                        event.preventDefault();
                        const newThemes =
                            settings.get("themes") === "lightTheme"
                                ? "darkTheme"
                                : "lightTheme";
                        SettingsActions.changeSetting({
                            setting: "themes",
                            value: newThemes
                        });
                    }
                }
            }*/
            ],

            /*[
                {
                    "content": "header.help",
                    "icon": "question-circle",
                    "attr": {
                        "to": "",
                        "onClick": (event) => {
                            event.preventDefault();
                            window.open("https://deexhelp.zendesk.com", "_blank");
                            this.onBodyClick(false);
                        }
                    }
                },{
                    "content": "news.news",
                    "icon": "news",
                    "attr": {
                        "to": "",
                        "onClick": (event) => {
                            event.preventDefault();
                            window.open("https://www.deex.blog/", "_blank");
                            this.onBodyClick(false);
                        }
                    }
                }
            ],[
                ,{
                    "content": "account.whitelist.title",
                    "icon": "list",
                    "attr": {
                        "to": ["","account", currentAccount, "whitelist"].join("/"),
                        "onClick": ()=>{this.onBodyClick(false);}
                    }
                },{
                    "content": "account.permissions",
                    "icon": "warning",
                    "attr": {
                        "to": ["","account", currentAccount, "permissions"].join("/"),
                        "onClick": ()=>{this.onBodyClick(false);}
                    }
                }
            ],*/
            // [
            //     {
            //         "content": "header.explorer",
            //         "icon": "link",
            //         "attr": {
            //             "to": ["","explorer"].join("/"),
            //             "onClick": ()=>{this.onBodyClick(false);}
            //         }
            //     }
            // ],
            [
                {
                    "content": "header.settings",
                    "icon": "settings",
                    "attr": {
                        "to": "/settings",
                        "onClick": ()=>{this.onBodyClick(false);}
                    }
                }
            ],[]
        ];

        let staticScroogeMenu = [
            [
                {
                    "content": "account.history",
                    "icon": "transaction_history",
                    "attr": {
                        "to": ["","account", currentAccount, "activity"].join("/"),
                        "onClick": () => {
                            this.onBodyClick(false);
                        }
                    }
                },
                // {
                //     "content": "account.network_fees_scrooge",
                //     "icon": "network_fees",
                //     "attr": {
                //         "to": ["", "networkfees"].join("/"),
                //         "onClick": () => {
                //             this.onBodyClick(false);
                //         }
                //     }
                // },
                {
                "content": "header.referrals",
                "icon": "referrals",
                "attr": {
                    "to": ["", "account", "referrals"].join("/"),
                    "onClick": () => {
                        this.onBodyClick(false);
                    }
                }
            },{
                "content": "account.aml",
                "icon": "",
                "attr": {
                    "to": "/",
                    "onClick": () => {
                        this.onBodyClick(false);
                    }
                }
            },
            ,{
                "content": "account.partners",
                "icon": "",
                "attr": {
                    "to": "/",
                    "onClick": () => {
                        this.onBodyClick(false);
                    }
                }
            },
            ,{
                "content": "account.about_us",
                "icon": "",
                "attr": {
                    "to": "/",
                    "onClick": () => {
                        this.onBodyClick(false);
                    }
                }
            },,{
                "content": "account.contacts",
                "icon": "",
                "attr": {
                    "to": "/",
                    "onClick": () => {
                        this.onBodyClick(false);
                    }
                }
            }
            ],
            [
                {
                    "content": "header.settings",
                    "icon": "settings",
                    "attr": {
                        "to": "/settings",
                        "onClick": ()=>{this.onBodyClick(false);}
                    }
                }
            ],[]
        ];

        let staticGblMenu = [
            [
                {
                    "content": "account.history",
                    "icon": "transaction_history",
                    "attr": {
                        "to": ["","account", currentAccount, "activity"].join("/"),
                        "onClick": () => {
                            this.onBodyClick(false);
                        }
                    }
                },
                // {
                //     "content": "account.network_fees_gbl",
                //     "icon": "network_fees",
                //     "attr": {
                //         "to": ["", "networkfees"].join("/"),
                //         "onClick": () => {
                //             this.onBodyClick(false);
                //         }
                //     }
                // },
                {
                    "content": "header.create_asset",
                    "icon": "wallet",
                    "attr": {
                        "to": ["","account", currentAccount, "create-asset"].join("/"),
                        "onClick": ()=>{this.onBodyClick(false);}
                    }
                },{
                "content": "header.referrals",
                "icon": "referrals",
                "attr": {
                    "to": ["", "account", "referrals"].join("/"),
                    "onClick": () => {
                        this.onBodyClick(false);
                    }
                }
            }
            ],
            // [
            //     {
            //         "content": "header.explorer",
            //         "icon": "link",
            //         "attr": {
            //             "to": ["","explorer"].join("/"),
            //             "onClick": ()=>{this.onBodyClick(false);}
            //         }
            //     }
            // ],
            [
                {
                    "content": "header.settings",
                    "icon": "settings",
                    "attr": {
                        "to": "/settings",
                        "onClick": ()=>{this.onBodyClick(false);}
                    }
                }
            ],[]
        ];

        let headerLockAttr = {
            "content": !locked ? "header.lock_short" : "header.unlock_short",
            "icon": locked ? "login" : "logout",
            "attr": {
                "to": "",
                "onClick": this.toggleLock
            }
        };
        /*let headerChangeThemes = {
            "content": ["settings", settings.get("themes")].join("."),
            "icon": settings.get("themes") === "lightTheme" ? "sun" : "moon",
            "attr": {
                "to": "",
                "onClick": (event) => {
                    event.preventDefault();
                    const newThemes =
                        settings.get("themes") === "lightTheme"
                            ? "darkTheme"
                            : "lightTheme";
                    SettingsActions.changeSetting({
                        setting: "themes",
                        value: newThemes
                    });
                }
            }
        };
        let deleteWalletLink = {
            "to": "/settings/wallet#delete-wallet",
            "onClick": ()=>{this.onBodyClick(false);}
        };
        if( wallet_name ) {
            if( locked ) {
                lockShort.attr = deleteWalletLink;
            } else {
                headerLockAttr.attr = deleteWalletLink;
            }
        }*/

        const userMenu = __GBL_CHAIN__ || __GBLTN_CHAIN__ ? staticGblMenu : (__SCROOGE_CHAIN__ ? staticScroogeMenu : staticMenu);

        if( locked ) {
            userMenu[0].unshift(headerLockAttr);
            userMenu[userMenu.length-1].push(lockShort);
        } else {
            userMenu[userMenu.length-1].push(lockShort);
        }

        return (<div className={"user-menu-drop-down"}>
            <div className={"user-menu-drop-down-block"}>
                {userMenu.map((menu, index) => {
                    return (<div key={"user-menu-row-" + index} className={"user-menu-row"}>
                        {menu.map((item, keyItem)=> {
                            return (<div key={"user-menu-item-" + keyItem} className={"user-menu-item"}>
                                <Link className={"user-menu-link"} {...item.attr}>
                                    {__SCROOGE_CHAIN__ ? null :
                                    <Icon name={item.icon}/>}
                                    <Translate content={item.content}/>
                                </Link>
                            </div>);
                        })}
                    </div>);
                })}
            </div>
        </div>);
    }
    render(){
        const {currentAccount, locked} = this.props;
        const {isVisibleMenu} = this.state;

        // console.log("locked",locked);

        return(
            <ClickOutside onClickOutside={()=>this.onBodyClick(false)} className={cnames("user-menu-outside", {"active": isVisibleMenu})} >
                {__SCROOGE_CHAIN__ ?
                <>
                <div className="scrooge-menu">
                    <a onClick={()=>this.onBodyClick(true)}>
                        <div className="user-menu-scrooge">
                            <Translate content={"account.menu"}/>
                            <svg width="20" fill="#ffffff" version="1.1" x="0px" y="0px"viewBox="0 0 250.579 250.579" style={{marginLeft: "8px", enableBackground: "new 0 0 250.579 250.579"}}>
                                <g id="Menu">
                                    <path style={{fill:"#ffffff", fillRule: "evenodd", clipRule: "evenodd"}} d="M22.373,76.068h205.832c12.356,0,22.374-10.017,22.374-22.373
                                        c0-12.356-10.017-22.373-22.374-22.373H22.373C10.017,31.323,0,41.339,0,53.696C0,66.052,10.017,76.068,22.373,76.068z
                                        M228.205,102.916H22.373C10.017,102.916,0,112.933,0,125.289c0,12.357,10.017,22.373,22.373,22.373h205.832
                                        c12.356,0,22.374-10.016,22.374-22.373C250.579,112.933,240.561,102.916,228.205,102.916z M228.205,174.51H22.373
                                        C10.017,174.51,0,184.526,0,196.883c0,12.356,10.017,22.373,22.373,22.373h205.832c12.356,0,22.374-10.017,22.374-22.373
                                        C250.579,184.526,240.561,174.51,228.205,174.51z"/>
                                </g>
                            </svg>
                        </div>
                    </a>
                    <div>
                        <Icon className="lock-unlock" name={locked ? "account-inactive" : "account-active"}/>
                        <span className={"user-name"}>{currentAccount}</span>
                    </div>
                </div>
                <div className="scrooge-mobile">
                    <a className={cnames("user-menu-account", {"isLocked": locked })} onClick={()=>this.onBodyClick(true)}>
                        <Icon className="lock-unlock" name={locked ? "account-inactive" : "account-active"}/>
                        <span className={"user-name"}>{currentAccount}</span>
                    </a>
                </div>
                </> :
                <a className={cnames("user-menu-account", {"isLocked": locked })} onClick={()=>this.onBodyClick(true)}>
                    <Icon className="lock-unlock" name={locked ? "account-inactive" : "account-active"}/>
                    <span className={"user-name"}>{currentAccount}</span>
                </a>}
                {isVisibleMenu && this.createUserMenu()}
            </ClickOutside>
        );
    }
}

import React from "react";
import SettingsActions from "actions/SettingsActions";
import SettingsStore from "stores/SettingsStore";
import ClickOutside from "react-click-outside";
import cnames from "classnames";

export default class ChangeDropDownCash extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            isActive: false
        };
        this._toggleDropdown = this._toggleDropdown.bind(this);
        this._closeDropDown = this._closeDropDown.bind(this);
    }

    _onClickCurrency = (item, event) => {
        event.preventDefault();
        SettingsActions.changeSetting({
            setting: "unit",
            value: item
        });
    };
    _toggleDropdown = () => {
        this.setState({
            isActive: !this.state.isActive
        });
    };
    _closeDropDown = () => {
        if (this.state.isActive) {
            this.setState({isActive: false});
        }
    };

    render() {
        let { prefixClassName } = this.props;
        const units = SettingsStore.getState().defaults["unit"];
        if( !prefixClassName ) prefixClassName = "app-menu";

        return (
            <div className={prefixClassName}>
                <ClickOutside
                    className={`${prefixClassName}-outside-currency`}
                    onClickOutside={this._closeDropDown}
                >
                    <div
                        onClick={this._toggleDropdown}
                        className={cnames(
                            "menu-dropdown-wrapper dropdown-wrapper",
                            {active: this.state.isActive}
                        )}
                        style={{
                            height: "100%",
                            width: "fit-content",
                            paddingLeft: "5px",
                            paddingRight: "5px"
                        }}
                    >
                        <div className="menu-wrapper">
                            <div
                                className={`${prefixClassName}-arrow`}
                                style={{marginLeft: "auto"}}
                            />
                        </div>
                        <ul className={`dropdown ${prefixClassName}-header-menu`}>
                            {units.map(item => (
                                <li key={item}>
                                    <a
                                        /*style={{color: "#fff"}}*/
                                        href=""
                                        onClick={(event)=>this._onClickCurrency(item, event)}
                                    >
                                        {item}
                                    </a>
                                </li>
                            ))}
                        </ul>
                    </div>
                </ClickOutside>
            </div>
        );
    }
}
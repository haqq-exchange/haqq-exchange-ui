import React from "react";
import SettingsActions from "../../../actions/SettingsActions";
import Icon from "../../Icon/Icon";

export default class ChangeThemes extends React.PureComponent {
    render() {
        const {settings} = this.props;
        const newThemes = settings.get("themes") === "lightTheme" ? "darkTheme" : "lightTheme";
        const iconName = settings.get("themes") === "lightTheme" ? "sun" : "moon";
        return (
            <div className={"public-header-themes " + settings.get("themes")}>
                <a
                    href={"#"}
                    onClick={e => {
                        e.preventDefault();
                        SettingsActions.changeSetting({
                            setting: "themes",
                            value: newThemes
                        });
                    }}
                >
                    <Icon name={iconName} title={newThemes} />
                </a>
            </div>
        );
    }
}

import React from "react";
import SettingsStore from "stores/SettingsStore";
import ActionSheet from "react-foundation-apps/src/action-sheet";
import IntlActions from "actions/IntlActions";
import ImageLoad from "Utility/ImageLoad";
import Translate from "react-translate-component";
import counterpart from "counterpart";

export default class FlagDropdown extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            currentLocale: SettingsStore.getState().settings.get("locale")
        };
    }

    flagImage = (flag, width = 50, height = null) => {

        return (
            <ImageLoad
                staticPath={"images/flags"}
                customStyle={{
                    width,
                    height
                }}
                imageName={`${flag.toUpperCase()}.png`} />

        );
    };
    render() {
        const locales = SettingsStore.getState().defaults.locale;
        return (
            <div className="public-header-flags">
                <ActionSheet>
                    <ActionSheet.Button title="" style={{width: "64px"}}>
                        <a
                            style={{border: "none"}}
                            className="arrow-down"
                        >
                            <span style={{ backgroundImage: "url("+`https://static.haqq.exchange/images/flags/${this.state.currentLocale.toUpperCase()}.png`+")" }}></span>
                        </a>
                    </ActionSheet.Button>

                    <ActionSheet.Content>
                        <ul className="no-first-element-top-border">
                            {locales.map(locale => {
                                return (
                                    <li key={locale}>
                                        <a
                                            href={"#"}
                                            onClick={e => {
                                                e.preventDefault();
                                                IntlActions.switchLocale(
                                                    locale
                                                );
                                                this.setState({
                                                    currentLocale: locale
                                                });
                                            }}
                                        >
                                            <div className="table-cell">
                                                {this.flagImage(locale, 30)}
                                            </div>
                                            <div
                                                className="table-cell"
                                                style={{paddingLeft: 10}}
                                            >
                                                <Translate
                                                    content={
                                                        "languages." + locale
                                                    }
                                                />
                                            </div>
                                        </a>
                                    </li>
                                );
                            })}
                        </ul>
                    </ActionSheet.Content>
                </ActionSheet>
            </div>
        );
    }
}
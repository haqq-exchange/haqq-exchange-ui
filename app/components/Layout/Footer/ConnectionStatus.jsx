import React from "react";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";
import SettingsStore from "stores/SettingsStore";
import cname from "classnames";
import SettingsActions from "../../../actions/SettingsActions";
import willTransitionTo from "../../../routerTransition";

export default class ConnectionStatus extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showNodesPopup: false,
            minify: true
        };
    }

    /*shouldComponentUpdate = (nextProps, nextState) => {
        let apiLatencies1 = Object.values(nextProps.apiLatencies);
        let apiLatencies2 = Object.values(this.props.apiLatencies);
        let difference = apiLatencies1.filter(x => apiLatencies2.indexOf(x) == -1);
        console.log("shouldComponentUpdate nextProps", nextProps, this.props, difference, apiLatencies1, apiLatencies2);
        return (
            nextProps.activeNode !== this.props.activeNode ||
            !utils.are_equal_shallow(nextProps.apiLatencies, this.props.apiLatencies)
        );

    };*/


    activate(url) {
        SettingsActions.changeSetting({
            setting: "apiServer",
            value: url
        });
        setTimeout(
            function() {
                willTransitionTo(false);
            }.bind(this),
            50
        );
    }



    render(){

        const { apiLatencies, activeNode , currentNode} = this.props;
        // const {synced} = this.props;
        const connected = !(this.props.rpc_connection_status === "closed");

        // Current Node Details
        let currentNodePing = apiLatencies[currentNode || activeNode];
        // console.log("apiLatencies", apiLatencies);
        // console.log("currentNode", activeNode);
        // console.log("currentNodePing", currentNodePing);
        // console.log("shouldComponentUpdate nextProps", this.props);
        let sortable = [];
        for (let key in apiLatencies) {
            sortable.push([key, apiLatencies[key]]);
        }

        sortable.sort((a, b) => {
            return a[1] - b[1];
        });

        let firstSortable = sortable[0];

        // console.log("sortable", firstSortable, currentNodePing, currentNodePing < firstSortable[1]);


        // let block_height = this.props.dynGlobalObject.get("head_block_number");
        // let head_block_id = this.props.dynGlobalObject.get("head_block_id");
        // let version_match = APP_VERSION.match(/2\.0\.(\d\w+)/);
        // let version = version_match ? `.${version_match[1]}` : ` ${APP_VERSION}`;
        // let updateStyles = {display: "inline-block", verticalAlign: "top"};
        // let logoProps = {};

        const getLatncyClassName = (name) => {
            return cname("latency-indicator", name,  {
                "latency-bad": currentNodePing > 500,
                "latency-normal": currentNodePing > 300 && currentNodePing < 500,
                "latency-good": currentNodePing < 300,
            });
        };


        const latencyIdicator = (
            <span className="footer-latency-indicator">
                <span className={getLatncyClassName("indicator-small")}/>
                <span className={getLatncyClassName("indicator-medium")}/>
                <span className={getLatncyClassName("indicator-big")}/>
            </span>
        );

        return(
            <div className="footer-connection-status" >
                <Link to={"/settings/access"} title={currentNode || activeNode}>

                    {latencyIdicator}

                    <span className="footer-latency">
                        <Translate content="footer.latency" />
                        {!connected
                            ? "-"
                            : !currentNodePing
                                ? "-"
                                : " " + currentNodePing + " ms"}
                    </span>

                </Link>
                {currentNodePing > firstSortable[1] ? <span className={"footer-latency-reconnect"} onClick={()=>this.activate(firstSortable[0])}>reconnect</span> : null}
            </div>
        );
    }
}
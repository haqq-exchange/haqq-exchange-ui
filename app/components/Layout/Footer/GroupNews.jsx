import React  from "react";
import WPAPI  from "wpapi";
import Immutable from "immutable";
import ClickOutside from "react-click-outside";
import moment from "moment";
import Translate from "react-translate-component";
import SettingsStore from "stores/SettingsStore";

export default class GroupNews extends React.PureComponent {

    static defaultProps = {
        timeDuration: 30,
        tags: {
            en: 14,
            ru: 15,
            tr: 16,
        }
    };

    constructor(){
        super();

        this.state = {
            showPosts: false,
            lastUpdate: null,
            postList: Immutable.Map()
        };

        /*this.WP = new WPAPI({
            endpoint: "https://deex.blog/wp-json"
        });*/
    }


    componentDidMount() {
        this.createWpUrl();
    }


    createWpUrl = () => {
        const locale = SettingsStore.getState().settings.get("locale");
        return new Promise(resolve=>{
            let endPoint = ["https://deex.blog", "wp-json"];
            if(locale!== "ru") {
                endPoint.splice(1, 0, locale);
            }

            console.log("endPoint", endPoint);

            this.WP = new WPAPI({
                endpoint: endPoint.join("/")
            });
            resolve();
        });
    };

    _showPosts = () => {
        const { timeDuration } = this.props;
        const { postList , lastUpdate } = this.state;
        this.setState({ showPosts: true });
        let isUpdate = !postList.size;
        if( lastUpdate ) {
            const nextUpdate = moment(lastUpdate).add(timeDuration, "minute");
            isUpdate = nextUpdate.isBefore(new Date(), "minute");
        }
        if( isUpdate ) {
            this.loadedLastPosts();
        }
    };

    loadedLastPosts = () => {
        const _this = this;
        let { postList } = this.state;
        this.WP
            .posts()
            .perPage(3).then(function( data ) {
                postList.clear();
                data.map(item => {
                    postList = postList.set(item.id, Immutable.fromJS(item));
                });

                _this.setState({
                    postList,
                    lastUpdate: new Date()
                });
            }).catch(function( err ) {
                console.log('err', err);
            });
    };




    render(){
        const { showPosts, postList } = this.state;

        return(
            <div className="footer-group-news" >
                {__SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__  ? null :
                <>
                    <span className={"footer-group-news-link"} onClick={this._showPosts}>
                        <Translate component="span" content="news.news"/>
                    </span>
                    {showPosts ? <NewsWindow postList={postList} onCLoseWindow={()=>this.setState({ showPosts: false })} /> : null}
                </>}
            </div>
        );
    }
}

const NewsWindow = ({onCLoseWindow, postList}) => {

    return (
        <div className={"news-window"}>
            <div className={"news-window-title"}>
                <Translate content="news.news" />
                <span className={"news-window-close"} onClick={()=>onCLoseWindow()} />
            </div>
            <div className={"news-window-list"}>
                {postList.size ? postList.toArray().map(post=> {
                    return (
                        <div key={post.get("slug")} className={"news-item"}>
                            <time  className={"news-item-time"}>
                                {moment(post.get("date")).format("DD.MM.YYYY")}
                            </time>
                            <div className="news-item-content">
                                <div  className={"news-item-title"}>
                                    {post.getIn(["title","rendered"])}
                                </div>
                                <div  className={"news-item-desc"} dangerouslySetInnerHTML={{
                                    __html: post.getIn(["excerpt","rendered"])
                                }} />
                            </div>
                            <a href={post.get("link")} target={"_blank"}  className={"news-item-link"}>
                               <i />
                            </a>
                        </div>
                    );
                }) : null }
            </div>
        </div>
    );
}

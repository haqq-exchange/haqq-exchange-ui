import React from "react";
import counterpart from "counterpart";
import Translate from "react-translate-component";
import ConnectionStatus from "Components/Layout/Footer/ConnectionStatus";
import GroupOne from "Components/Layout/Footer/GroupOne";
import GroupSocialLink from "Components/Layout/Footer/GroupSocialLink";
import GroupNewsBtn from "Components/Layout/Footer/GroupNews";
import SettingsStore from "stores/SettingsStore";

class GroupTwo extends React.Component {
    static defaultProps = {
        chatBroId: {
            "en": "12qHg",
            "tr": "72qHn",
            "ru": "12pCE"
        }
    };
    render() {
        const {chatBroId} = this.props;
        const locale = SettingsStore.getState().settings.get("locale");
        return (
            <div className={"footer-group-two"}>
                 {__SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__ ? null : <Translate className={"footer-group-two-link"} component={"a"} content={"news.news"} href={["https://deex.blog/tag", locale].join("/")} target={"_blank"} />}
                {__SCROOGE_CHAIN__ ? null : <Translate className={"footer-group-two-link"} component={"a"} content={"chat.chat"} href={["https://www.chatbro.com", chatBroId[locale]].join("/")} target={"_blank"} />}
            </div>
        );
    }
}
export default class FooterDesktop extends React.Component {


    render() {

        return (
            <div className="footer-wrap">
                {/* <div className="footer-top">
                    <GroupOne />
                    <GroupTwo />
                </div> */}
                <div className="footer-center">
                    {__SCROOGE_CHAIN__ ? null : <GroupSocialLink />}
                </div>
                {__SCROOGE_CHAIN__ ? null :
                <div className="footer-bottom">
                    <ConnectionStatus {...this.props} />
                </div>}
            </div>
        );
    }
}
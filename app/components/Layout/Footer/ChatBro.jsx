import React from "react";
import Immutable from "immutable";
//import ClickOutside from "react-click-outside";
import cname from "classnames";
import Translate from "react-translate-component";
import SettingsStore from "stores/SettingsStore";

export default class ChatBro extends React.PureComponent {

    static defaultProps = {
        timeDuration: 30,
        chatBroId: {
            "en": "12qHg",
            "tr": "72qHn",
            "ru": "12pCE"
        }
    };

    constructor() {
        super();

        this.state = {
            isLoaded: false,
            showChat: false,
            unreadMessage: 0,
            lastUpdate: null,
            postList: Immutable.Map()
        };

    }

    componentDidMount() {
        const _this = this;
        document.addEventListener("chatLoaded", () => {
            document.addEventListener("newMessage", _this.newMessage);
        });
    }

    newMessage = (event) => {
        let { isLoaded, unreadMessage } = this.state;
        if( isLoaded ) {
            this.setState({
                unreadMessage: ++unreadMessage
            });
        }
    };

    loadChatBro = () => {
        const {chatBroId, theme} = this.props;
        const locale = SettingsStore.getState().settings.get("locale");
        const chatTheme = {
            "lightTheme": {
                chatHeaderBackgroundColor: "#ffffff",
                chatHeaderTextColor: "#2a2a2a",
                chatBodyBackgroundColor: "#ffffff",
                coloredUserNames: "#383838",
                chatBodyTextColor: "#383838",
                chatInputBackgroundColor: "#fafafa",
                chatInputTextColor: "#acb5b8"
            },
            "darkTheme": {
                chatHeaderBackgroundColor: "#515b5e",
                chatHeaderTextColor: "#ffffff",
                chatBodyBackgroundColor: "#383838",
                coloredUserNames: "#383838",
                chatBodyTextColor: "#ffffff",
                chatInputBackgroundColor: "#2a2a2a",
                chatInputTextColor: "#acb5b8"
            }
        };
        console.log("this.props", this.props);
        this.chatbroLoader(Object.assign({}, {
            containerDivId: "chat-bro-id",
            useStandardHistoryWidgetSettings: false,
            isStatic: true,
            chatLanguage: locale,
            encodedChatId: __DEV__ ? "1001148832367" : chatBroId[locale],
            chatHeight: "400",
            chatState: "maximized",
            allowMinimizeChat: false,
            chatPaginatorUrlPrefix: "http://deex.exchange/" + chatBroId[locale] + "/history/",

        }, chatTheme[theme]));
    };

    static removeElement(element) {
        element && element.parentNode && element.parentNode.removeChild(element);
    }

    chatbroLoader(chats, async) {
        async = !1 !== async;

        let params = {
                embedChatsParameters: chats instanceof Array ? chats : [chats],
                lang: "en",
                needLoadCode: "undefined" === typeof Chatbro,
                embedParamsVersion: localStorage.embedParamsVersion,
                chatbroScriptVersion: localStorage.chatbroScriptVersion
            },
            xhr = new XMLHttpRequest;
        xhr.withCredentials = !0;
        xhr.onload = function() {
            //eval(xhr.responseText);
            var s = document.createElement("script");
            s.type = "text/javascript";
            s.id = "chat-bro-script";
            //var code = 'alert("hello world!");';
            try {
                s.appendChild(document.createTextNode(xhr.responseText));
                document.body.appendChild(s);
            } catch (e) {
                s.text = xhr.responseText;
                document.body.appendChild(s);
            }

        };
        xhr.onerror = function() {
            console.error("Chatbro loading error");
        };
        xhr.open("GET", "//www.chatbro.com/embed.js?" + btoa(unescape(encodeURIComponent(JSON.stringify(params)))), async);
        xhr.send();
    }


    showChatBro = () => {
        const {isLoaded} = this.state;
        this.setState({showChat: true, isLoaded: true, unreadMessage: 0}, () => {
            if (!isLoaded) this.loadChatBro();
        });
    };
    closeChatBro = () => {
        this.setState({showChat: false}, () => {
            // document.getElementById("chat-bro-id").innerHTML="";
            // ChatBro.removeElement(document.getElementById("chat-bro-script"));
        });
    };


    render() {
        const {showChat, unreadMessage} = this.state;
        
        return (
            <>
                {__SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__ ? null :
                <div className="footer-group-chat">
                    <span className={cname("footer-group-chat-link")} onClick={() => this.showChatBro()}>
                        <Translate component="span" content="chat.chat"/>
                        {unreadMessage ? <i>{unreadMessage > 9 ? "9+" : unreadMessage}</i> : "" }
                    </span>
                    <ChatWindow showChat={showChat} onCLoseWindow={() => this.closeChatBro()}/>

                </div>}
            </>
        );
    }
}


const ChatWindow = ({showChat, onCLoseWindow}) => {
    return (
        <div className={cname("chat-window", {"visible-hidden": !showChat})}>
            <div className={"chat-window-title"}>
                <Translate content="chat.chat"/>
                <span className={"chat-window-close"} onClick={onCLoseWindow}/>
            </div>
            <div className={"chat-window-list"} id={"chat-bro-id"}>
            </div>
        </div>
    );
};
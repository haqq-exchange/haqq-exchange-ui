import React from "react";
import ChatBro from "Components/Layout/Footer/ChatBro";
import ConnectionStatus from "Components/Layout/Footer/ConnectionStatus";
import GroupOne from "Components/Layout/Footer/GroupOne";
import GroupSocialLink from "Components/Layout/Footer/GroupSocialLink";
import GroupNewsBtn from "Components/Layout/Footer/GroupNews";

export default class FooterDesktop extends React.Component {
    render() {
        return (
            <div className="footer-wrap">
                {__SCROOGE_CHAIN__ ? null :
                <div className="footer-left">
                    {/* <ChatBro {...this.props} /> */}
                    <ConnectionStatus {...this.props} />

                </div>}
                {__SCROOGE_CHAIN__ ? null :
                <div className="footer-right">
                    {/* <GroupOne /> */}
                    <GroupSocialLink />
                    {/* {__GBL_CHAIN__ || __GBLTN_CHAIN__ ? null :
                    <GroupNewsBtn />} */}
                </div>}
            </div>
        );
    }
}
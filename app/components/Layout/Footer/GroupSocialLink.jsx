import React  from "react";
import Icon from "Components/Icon/Icon";
import {Link} from "react-router-dom";
import SettingsStore from "../../../stores/SettingsStore";
import {ymSend} from "lib/common/metrika";

export default class GroupSocialLink extends React.Component {

    static defaultProps = {
        socialLinks: __GBL_CHAIN__ || __GBLTN_CHAIN__ ? 
        [
            [
                {
                    icon: "youtube",
                    links: "#",
                },{
                    icon: "facebook",
                    links: "#",
                },{
                    icon: "twitter",
                    links: "#",
                },{
                    icon: "vk",
                    links: "#",
                },{
                    icon: "instagram",
                    links: "#",
                },{
                    icon: "telegram",
                    links: {
                        tr: "#",
                        en: "#",
                        ru: "#"
                    },
                }
            ]
        ]: [
            // [
            //     // {
            //     //     icon: "kran",
            //     //     link: "/cran",
            //     //     link1: "https://deex4free.com/",
            //     // },
            //     {
            //         icon: "loto",
            //         link: "loto",
            //         links1: "https://loto.deex.exchange/",
            //     }
            // ],
            [
                // {
                //     icon: "bitcoin",
                //     links: {
                //         en : "https://bitcointalk.org/index.php?topic=5049227.0",
                //         tr: "https://bitcointalk.org/index.php?topic=5049227.0",
                //         ru: "https://bitcointalk.org/index.php?topic=5049232.0"
                //     },
                // },
                {
                    icon: "medium",
                    links: "https://medium.com/@halalcryptochat/",
                },
                // {
                //     icon: "reddit",
                //     links: "https://www.reddit.com/user/the_deex/",
                // },
                {
                    icon: "youtube",
                    links: "https://www.youtube.com/@halalcryptochatcommunity3497",
                },
                // {
                //     icon: "facebook",
                //     links: "https://www.facebook.com/groups/deex.exchange/",
                // },
                {
                    icon: "twitter",
                    links: " https://twitter.com/halalcryptochat",
                },
                // {
                //     icon: "steemit",
                //     links: "https://steemit.com/@deex",
                // },
                // {
                //     icon: "vk",
                //     links: "https://vk.com/deex.exchange",
                // },
                {
                    icon: "instagram",
                    links: "https://www.instagram.com/halalcryptochat/",
                },{
                    icon: "telegram",
                    links: "https://t.me/ISLMcoin/"
                    // links: {
                    //     tr: "https://t.me/deex_turkiye",
                    //     en: "https://t.me/deexexchange",
                    //     ru: "https://t.me/deex_exchange"
                    // },
                },{
                    icon: "telegram",
                    links: "https://t.me/halalcryptochat"
                }
            ]
        ],

        socialLinksGbl: [
            [
                {
                    icon: "youtube",
                    links: "https://www.youtube.com/channel/UCoI3BI305UA-1O1rzvNy60A",
                },{
                    icon: "facebook",
                    links: "https://www.facebook.com/genesisblocksystemsit/",
                },{
                    icon: "vk",
                    links: "https://vk.com/genesisblockit",
                },{
                    icon: "instagram",
                    links: "https://www.instagram.com/genesisblock.it/?hl=el",
                }
            ]
        ]
    };

    sendYm = props => {
        let linkSend = ["kran"];
        if( linkSend.includes(props.icon) ) {
            ymSend(props.icon.toUpperCase(), window.location.pathname);
        }
    };

    render(){
        const {socialLinks, socialLinksGbl}=this.props;
        const locale = SettingsStore.getState().settings.get("locale");
        let socials = [];
        __GBL_CHAIN__ ? socials = socialLinksGbl : socials = socialLinks;
        
        return(
            <div className="footer-group-social-wrap">
                {
                    socialLinks.map((social,index) => {
                        return (<div key={"footer-group-social-"+ index} className="footer-group-social" style={{flexDirection: "row", width: "100%"}}>
                            {social.map(data => {
                                let hrefLink = data.links;
                                if (typeof data.links === "object") {
                                    hrefLink = data.links[locale];
                                }
                                if (data.link) {
                                    return (
                                        <Link key={data.icon + hrefLink} to={data.link} className={"footer-group-social-link"}><Icon name={data.icon}/></Link>
                                    );
                                }
                                return (
                                    <a key={data.icon + hrefLink} href={hrefLink} onClick={()=>this.sendYm(data)} target={"_blank"} rel={"noopener noreferrer"} className={"footer-group-social-link"}>
                                        <Icon name={data.icon}/>
                                    </a>
                                );
                            })}
                        </div>);
                    })
                }
            </div>
        );
    }
}

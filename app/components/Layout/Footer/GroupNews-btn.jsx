import React  from "react";
import Translate from "react-translate-component";

export default class GroupNewsBtn extends React.Component {
    render(){
        return(
            <div className="footer-group-news">
                {__SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__ ? null :
                <a href={"https://deex.blog/"} target={"_blank"} className={"footer-group-news-link"}>
                    <Translate component="span" content="news.news"/>
                </a>}
            </div>
        );
    }
}

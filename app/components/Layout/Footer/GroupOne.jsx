import React  from "react";
import SettingsStore from "stores/SettingsStore";
import Translate from "react-translate-component";
import Icon from "../../Icon/Icon";
import {ymSend} from "../../../lib/common/metrika";

export default class GroupOne extends React.Component {
    render(){
        return(
            <div className="footer-group-one">
                {__SCROOGE_CHAIN__ || __GBL_CHAIN__ || __GBLTN_CHAIN__? null : <Listing />}
                <TechHelp />
            </div>
        );
    }
}

const Listing = () => {
    const locale = SettingsStore.getState().settings.get("locale");
    const linksListing = {
        en: "https://deex.blog/en/listing/",
        ru: "https://deex.blog/listing-ru/",
        tr: "https://deex.blog/tr/listing-tr/",
        zh: "https://deex.blog/zh/listing-zh/",

    };




    return (
        <a href={linksListing[locale]} onClick={()=>ymSend("LISTING", window.location.pathname)} target={"_blank"} className={"footer-group-one-link"}>
            <Icon name={"listing"}/>
            <Translate
                component="span"
                content= "footer.listing"
            />
        </a>
    );
};

const TechHelp = () => {
    return (
        <a href={__SCROOGE_CHAIN__ || __GBL_CHAIN__ ? "#" : "https://deexhelp.zendesk.com/hc/ru"} target={"_blank"} className={"footer-group-one-link"}>
            <Icon name={"support"}/>
            <Translate
                component="span"
                content="footer.tech_help"
            />
        </a>
    );
};

import React, { Component } from "react";
import AdBlockDetect from "react-ad-block-detect";

export default class AdBlockDetectComponent extends Component {
    render() {
        return (
            <AdBlockDetect>
                <div className={"ad-block-detect"}>
                    <div className={"ad-block-detect-title"}>У вас включен блокировщик рекламы</div>
                    <div className={"ad-block-detect-sub-title"}>При включенном плагине мы не можем гарантировать корректность работы сервисов</div>
                </div>
            </AdBlockDetect>
        );
    }
}
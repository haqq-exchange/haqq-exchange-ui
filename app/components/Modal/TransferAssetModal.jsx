import React from "react";
import DefaultModal from "components/Modal/DefaultModal";
import PropTypes from "prop-types";
// import Translate from "react-translate-component";


//import ls from "common/localStorage";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
//import WalletUnlockActions from "actions/WalletUnlockActions";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";
import loadable from "loadable-components";
//import ChainTypes from "Utility/ChainTypes";

const SendTransfer = loadable(() => import("Components/Account/AccountOverview/AccountPortfolio/SendTransfer"));
const DepositAddress = loadable(() => import("Components/Account/AccountOverview/AccountPortfolio/DepositAddress"));
const SendWithdraw = loadable(() => import("Components/Account/AccountOverview/AccountPortfolio/SendWithdraw"));
const SendDeexWithdraw = loadable(() => import("Components/Account/AccountOverview/AccountPortfolio/SendDeexWithdraw"));


class TransferAssetModal extends DefaultBaseModal {

    static propTypes = {
        account: PropTypes.string,
        asset: PropTypes.number,
        asset_id: PropTypes.number,
        onShowForm: PropTypes.func
    };

    constructor(props) {
        super(props);
    }


    afterOpenModal = () => {};

    afterCloseModal = () => {};

    closeModal = () => {
        const {resolve, modalId} = this.props;
        ModalActions.hide(modalId)
            .then(()=>resolve());
    };


    render() {
        const {isOpenModal} = this.state;
        const {modalId, modalIndex, data , typeTransfer } = this.props;

        let ComponentVisible, dataModal;
        if( data ) {
            dataModal = data.dataModal;
            ComponentVisible = typeTransfer[data.typeWindow || "empty"];
        }


        return (
            <DefaultModal
                id={modalId}
                modalId={modalId}
                isOpen={isOpenModal}
                className="UnlockModal"
                onAfterOpen={this.afterOpenModal}
                shouldCloseOnOverlayClick={false}
                shouldCloseOnEsc={false}
                customStyle={{zIndex: modalIndex[modalId]}} >
                <div className="modal fadeIn ng-enter is-active ng-enter-active">
                    <div className="modal-header">
                        <span onClick={this.closeModal} className="close-button">×</span>
                        <div className="modal__logo">
                            {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?  <div  style={{backgroundImage: "url("+ `https://static.haqq.exchange/gbl/images/logo-gbl.png` +")", height: "60px", width: "205px", margin: "0 auto", backgroundRepeat: "no-repeat"}} />
                            : (__SCROOGE_CHAIN__ ? <div  style={{backgroundImage: "url("+ `https://static.scrooge.club/images/general-logo.png` +")", height: "40px", width: "205px", margin: "0 auto"}} /> 
                            : <span className="logo-logotype" style={{height: 45}} />
                            // <img src={require("assets/logo-ico-blue.png")} />
                            )}
                        </div>
                    </div>
                    <div className="modal-content">
                        {data && <ComponentVisible {...dataModal} />}
                    </div>
                </div>
            </DefaultModal>
        );
    }
}


TransferAssetModal.defaultProps = {
    modalId: "transfer_asset_modal"
};

export default class PrivateContactModalContainer extends React.Component {
    render() {
        const defaultProps = {
            "empty": "",
            "sendTransfer": SendTransfer,
            "depositAddress": DepositAddress,
            "sendWithdraw": SendWithdraw,
            "deexWithdraw": SendDeexWithdraw,
        };

        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    data: () => ModalStore.getState().data["transfer_asset_modal"],
                    modalIndex: () => ModalStore.getState().modalIndex,
                    resolve: () => ModalStore.getState().resolve
                }}
            >
                <TransferAssetModal {...this.props} typeTransfer={defaultProps} />
            </AltContainer>
        );
    }
}

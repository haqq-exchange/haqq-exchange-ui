import React from "react";
import ZfApi from "react-foundation-apps/src/utils/foundation-api";
import Trigger from "react-foundation-apps/src/trigger";
import Translate from "react-translate-component";
import BaseModal from "./BaseModal";
import ls from "App/lib/common/localStorage";
import configConst from "App/config/const";
let ss = new ls(configConst.STORAGE_KEY);

export default class ChangeDomentModal extends React.Component {
    show() {
        ZfApi.publish("change_domain_modal", "open");
    }

    close() {
        ss.set("change_domain_modal", {show: false})
        ZfApi.publish("change_domain_modal", "close");
    }



    render() {
        return (
            <BaseModal id="change_domain_modal" overlay={true} ref="change_domain_modal">
                <div className="grid-block vertical no-overflow">
                    <Translate component="h3" content="modal.change_domen.title" />
                    {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?
                     <Translate
                        unsafe
                        component="p"
                        content={"modal.change_domen.text_gbl"}
                    />:
                    <Translate
                        unsafe
                        component="p"
                        content={__SCROOGE_CHAIN__ ? "modal.change_domen.text_scrooge" : "modal.change_domen.text"}
                    />}

                    <div
                        className="button-group no-overflow"
                        style={{paddingTop: 0}}
                    >
                        <div className="button" onClick={()=>this.close()}>
                            <Translate content="init_error.understand" />
                        </div>
                    </div>

                </div>
            </BaseModal>
        );
    }
}

import React from "react";
import ZfApi from "react-foundation-apps/src/utils/foundation-api";
import Trigger from "react-foundation-apps/src/trigger";
import Translate from "react-translate-component";
import BaseModal from "./BaseModal";

const BrowsersLinks ={
    chrome: "https://www.google.com/chrome/browser/desktop/",
    chromium: "https://www.chromium.org/Home",
    iron: "https://www.srware.net/iron/",
    coolnovo: "https://coolnovo.ru.downloadastro.com/",
    comodo: "https://www.comodo.com/home/browsers-toolbars/browser.php",
    rockmelt: "https://rockmelt.ru.uptodown.com/windows",
    maxthon: "https://ru.maxthon.com/",
    lunascape: "https://www.lunascape.tv/",
    torch: "https://torchbrowser.com/",
    sleipnir: "https://www.fenrir-inc.com/jp/sleipnir/",
    yandex: "https://browser.yandex.ru/"
}

export default class BrowserSupportModal extends React.Component {
    show() {
        ZfApi.publish("browser_modal", "open");
    }

    _openLink(link) {
        let newWnd = window.open(
            link,
            "_blank"
        );
        newWnd.opener = null;
    }

    render() {
        return (
            <BaseModal id="browser_modal" overlay={true} ref="browser_modal">
                <div className="grid-block vertical no-overflow">
                    <Translate component="h3" content="init_error.browser" />
                    <Translate
                        component="p"
                        content={__SCROOGE_CHAIN__ ? "init_error.browser_text_scrooge" : "init_error.browser_text"}
                    />
                    <br />

                    <p className="browser">
                        <a onClick={() => this._openLink(BrowsersLinks.chrome)}>Google Chrome</a>
                    </p>
                    {__SCROOGE_CHAIN__ ?
                    <>
                        <p className="browser"><a onClick={() => this._openLink(BrowsersLinks.chromium)}>Chromium</a></p>
                        <p className="browser"><a onClick={() => this._openLink(BrowsersLinks.iron)}>SRWare Iron</a></p>
                        <p className="browser"><a onClick={() => this._openLink(BrowsersLinks.coolnovo)}>CoolNovo</a></p>
                        <p className="browser"><a onClick={() => this._openLink(BrowsersLinks.comodo)}>Comodo Dragon</a></p>
                        <p className="browser"><a onClick={() => this._openLink(BrowsersLinks.rockmelt)}>RockMelt</a></p>
                        <p className="browser"><a onClick={() => this._openLink(BrowsersLinks.maxthon)}>Maxthon</a></p>
                        <p className="browser"><a onClick={() => this._openLink(BrowsersLinks.lunascape)}>Lunascape</a></p>
                        <p className="browser"><a onClick={() => this._openLink(BrowsersLinks.torch)}>Torch</a></p>
                        <p className="browser"><a onClick={() => this._openLink(BrowsersLinks.sleipnir)}>Sleipnir</a></p>
                        <p className="browser"><a onClick={() => this._openLink(BrowsersLinks.yandex)}>Yandex Browser</a></p>
                    </> : null}

                    <div
                        className="button-group no-overflow"
                        style={{paddingTop: 0}}
                    >
                        <Trigger close="browser_modal">
                            <div className="button">
                                <Translate content="init_error.understand" />
                            </div>
                        </Trigger>
                    </div>
                </div>
            </BaseModal>
        );
    }
}

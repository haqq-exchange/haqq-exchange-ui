import React from "react";
import PropTypes from "prop-types";
import AltContainer from "alt-container";

import ModalStore from "stores/ModalStore";
import GatewayStore from "stores/GatewayStore";
import SettingsStore from "stores/SettingsStore";

import ModalActions from "actions/ModalActions";
import DefaultModal from "Components/Modal/DefaultModal";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";
import {Form, Input} from "antd";
import LoadingIndicator from "Components/LoadingIndicator";
import Translate from "react-translate-component";

import AccountEmail from "Components/Account/CreateAccount/AccountEmail";
import counterpart from "counterpart";
import RefUtils from "../../lib/common/ref_utils";
import {getRequestAddress} from "../../api/apiConfig";

class SendSaveEmailModal extends DefaultBaseModal {


    static propTypes = {
        typeWindow: PropTypes.string,
    };

    constructor(props) {
        super(props);
    }


    componentDidMount() {

    }


    afterCloseModal = () => {};

    closeModal = () => {
        const {resolve, modalId} = this.props;
        ModalActions.hide(modalId)
            .then(()=>resolve());
    };


    render() {
        const {isOpenModal} = this.state;
        const {modalId, modalIndex, data , typeTransfer} = this.props;

        let ComponentVisible = null;
        if( data &&  typeTransfer[data.typeWindow] ) {
            ComponentVisible = typeTransfer[data.typeWindow];
        }
        
        // console.log("accountBalances", this.props );


        return (
            <DefaultModal
                id={modalId}
                modalId={modalId}
                isOpen={isOpenModal}
                className="UnlockModal SendModal"
                onAfterOpen={this.afterOpenModal}
                customStyle={{zIndex: modalIndex[modalId]}} >
                <div className="modal fadeIn ng-enter is-active ng-enter-active">
                    <div className="modal-header">
                        <span onClick={this.closeModal} className="close-button">×</span>
                    </div>
                    <div className="modal-content">
                        {data && <ComponentVisible {...this.props} />}
                    </div>
                </div>
            </DefaultModal>
        );
    }
}

SendSaveEmailModal.defaultProps = {
    modalId: "send_save_email"
};


export default class PrivateContactModalContainer extends React.Component {
    render() {
        const defaultProps = {
            "sendEmail": SendEmailForm,
        };


        return (
            <AltContainer
                stores={[ModalStore, GatewayStore, SettingsStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    data: () => ModalStore.getState().data["send_save_email"],
                    modalIndex: () => ModalStore.getState().modalIndex,
                    locale: SettingsStore.getState().settings.get("locale"),
                    resolve: () => ModalStore.getState().resolve
                }}
            >
                <SendSaveEmailModal  typeTransfer={defaultProps} {...this.props} />
            </AltContainer>
        );
    }
}

class SendEmail extends React.Component{
    constructor(props) {
        super(props);
        this.state= {
            loading: false,
            hasSend: false,
        };
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const {data, locale} = this.props;
        const {email, name} = this.state;

        this.props.form.validateFields((err) => {
            if (err) {
                return false;
            }

            const refutils = new RefUtils({
                url: getRequestAddress("api")
            });

            refutils.postTemplate("/verify/request_service_info", {
                email,
                name,
                lang: locale,
                service: data.service,
                system_id: refutils.getSystemId()
            }, "").then(res => {
                this.setState({
                    hasSend: true
                });
            });
            console.log("this.state", this.state, this.props);
        });
    };
    setAccountData = (name, value) => {
        this.setState({
            [name]: value
        });
    };

    getShowError = (name) => {
        const {isFieldTouched, getFieldError}= this.props.form;
        const hasValueError = isFieldTouched(name) && getFieldError(name);
        return {
            validateStatus: hasValueError ? "error" : "",
            help: hasValueError || ""
        };
    };


    render() {
        const {loading, hasSend} = this.state;
        const {getFieldDecorator} = this.props.form;

        return(
            <div className={"send-email"}>
                {!hasSend ? <>
                    <Translate content={"modal.title.sendEmail"} className={"send-email-title"} component={"h4"}  />
                    <Form onSubmit={this.handleSubmit}>
                        <div className={"send-email-item"}>
                            <Form.Item label={counterpart.translate("form.label.name")} >
                                {getFieldDecorator("name", {

                                })(<Input onKeyUp={(event) => this.setAccountData("name", event.target.value)} />)}
                            </Form.Item>
                        </div>
                        <AccountEmail
                            className={"send-email-item"}
                            notConfirm
                            getShowError={this.getShowError}
                            onChange={(data)=>this.setAccountData("email", data)} {...this.props} />
                        <Form.Item className={"send-email-btn"}>

                            {loading ?
                                <LoadingIndicator type="three-bounce"/> :
                                <Translate className={"btn btn-green"} component={"button"} type={"submit"} content="account.send"/>
                            }
                        </Form.Item>
                    </Form>
                </>: <Translate unsafe content={"modal.thx.sendEmail"} className={"send-email-title"} component={"h4"}  />}

            </div>
        );
    }
}

const SendEmailForm = Form.create({ name: "sendEmail" })(SendEmail);
import React from "react";
import DefaultModal from "Components/Modal/DefaultModal";
import PropTypes from "prop-types";
import AltContainer from "alt-container";

import ModalStore from "stores/ModalStore";
import GatewayStore from "stores/GatewayStore";
import SettingsStore from "stores/SettingsStore";

import ModalActions from "actions/ModalActions";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";
import loadable from "loadable-components";
import "./DepositWithdraw.scss";
import {FetchChain} from "deexjs";
import ChainTypes from "Utility/ChainTypes";
import Immutable from "immutable";
import BindToChainState from "Utility/BindToChainState";
const SelectedWithdraw = loadable(() => import("./SelectedWithdraw"));
const SelectedDeposit = loadable(() => import("./SelectedDeposit"));


class SelectedDepositWithdraw extends DefaultBaseModal {


    static propTypes = {
        account: ChainTypes.ChainAccount.isRequired,
        typeWindow: PropTypes.string,
    };

    constructor(props) {
        super(props);
    }


    componentDidMount() {

    }

    _getBalances = () => {
        let { account, backedCoins }         = this.props;
        let { backedCoinsAssets }         = this.state;
        let account_balances = account.get("balances");
        if( !backedCoinsAssets ) {
            backedCoinsAssets = Immutable.Map();
        }

        let depositAllowed = backedCoins.filter(a=> !a.deleted && a.depositAllowed).map(coins=>coins.symbol);
        let withdrawalAllowed = backedCoins.filter(a=> !a.deleted && a.withdrawalAllowed).map(coins=>coins.symbol);

        // console.log("depositAllowed", depositAllowed);
        // console.log("withdrawalAllowed", withdrawalAllowed);

        return new Promise(function(resolve) {
            let coinsName = backedCoins.filter(a=> !a.deleted).map(coins=>coins.symbol);
            FetchChain("getAsset", coinsName).then(assets => {
                resolve(assets);
            });
        }).then(results=>{
            results.map(item=>{
                if( item ) {
                    //console.log("item", item.toJS(), account_balances.toJS())
                    if(account_balances.has(item.get("id")) ) {
                        backedCoinsAssets = backedCoinsAssets.set(item.get("id"), account_balances.get(item.get("id")));
                    } else {
                        backedCoinsAssets = backedCoinsAssets.set(item.get("id"), null);
                    }
                    //backedCoinsAssets = backedCoinsAssets.set(item.get("dynamic_asset_data_id"), item);
                }
            });
            return backedCoinsAssets;
        }).then(balances => {
            let balancesData = balances.toJS();
            Promise.all([
                FetchChain("getAsset", Object.keys(balancesData)),
                FetchChain("getObject", balances.toArray().filter(a=>a) )
            ]).then(result => {
                let [assets, balanceObject] = result;
                let assetBalance = {};
                let assetDepositBalance = {};
                let assetWithdrawBalance = {};
                assets.map(asset => {
                    assetBalance[asset.get("id")] = asset;
                    if( withdrawalAllowed.indexOf(asset.get("symbol")) !== -1 ) {
                        assetWithdrawBalance[asset.get("id")] = asset;
                    }
                    if( depositAllowed.indexOf(asset.get("symbol")) !== -1 ) {
                        assetDepositBalance[asset.get("id")] = asset;
                    }
                });
                balanceObject.map(data => {
                    let dataMerge = assetBalance[data.get("asset_type")];
                    // console.log("dataMerge", dataMerge.get("symbol"));
                    // console.log("withdrawalAllowed", withdrawalAllowed);
                    // console.log("depositAllowed", depositAllowed);
                    if( data.has("balance") /*&& data.get("balance") > 0*/ ) {
                        if( withdrawalAllowed.indexOf(dataMerge.get("symbol")) !== -1 ) {
                            assetWithdrawBalance[data.get("asset_type")] = data.mergeDeep(dataMerge);
                        }
                        if( depositAllowed.indexOf(dataMerge.get("symbol")) !== -1 ) {
                            assetDepositBalance[data.get("asset_type")] = data.mergeDeep(dataMerge);
                        }
                        assetBalance[data.get("asset_type")] = data.mergeDeep(assetBalance[data.get("asset_type")]);
                    }
                });
                this.setState({
                    assetBalance,
                    assetDepositBalance,
                    assetWithdrawBalance
                });
            });
        });
    };


    afterOpenModal = () => {
        this._getBalances();
    };

    afterCloseModal = () => {};

    closeModal = () => {
        const {resolve, modalId} = this.props;
        ModalActions.hide(modalId)
            .then(()=>resolve());
    };

    _showModalDW = (asset, typeWindow) => {
        const { account } = this.props;
        let dataModal = {
            account: account.get("name"),
            asset: asset.get("id"),
            asset_id: asset.get("id"),
            onShowForm: () => {
                ModalActions.hide("transfer_asset_modal");
            }
        };

        ModalActions.show("transfer_asset_modal", {
            typeWindow,
            dataModal
        });
    };


    render() {
        const {isOpenModal, assetBalance, assetDepositBalance, assetWithdrawBalance} = this.state;
        const {modalId, modalIndex, data , typeTransfer, backedCoins,
            depositFavorite , withdrawFavorite} = this.props;

        let ComponentVisible,
            dataModal = {
                depositFavorite,
                withdrawFavorite,
                assetBalance,
                assetDepositBalance,
                assetWithdrawBalance,
                backedCoins,
                showModalDW: this._showModalDW
            };
        if( data && typeTransfer[data.typeWindow]) {
            ComponentVisible = typeTransfer[data.typeWindow];
        }

        // console.log("accountBalances", this.props );


        return (
            <DefaultModal
                id={modalId}
                modalId={modalId}
                isOpen={isOpenModal}
                className="UnlockModal DepositWithdraw"
                onAfterOpen={this.afterOpenModal}
                customStyle={{zIndex: modalIndex[modalId]}} >
                <div className="modal fadeIn ng-enter is-active ng-enter-active">
                    <div className="modal-header">
                        <span onClick={this.closeModal} className="close-button">×</span>
                    </div>
                    <div className="modal-content">
                        {data && <ComponentVisible {...dataModal} />}
                    </div>
                </div>
            </DefaultModal>
        );
    }
}


SelectedDepositWithdraw.defaultProps = {
    modalId: "selected_deposit_withdraw"
};

const SelectedDepositWithdrawBind = BindToChainState(SelectedDepositWithdraw, {
    show_loader: true
});

export default class PrivateContactModalContainer extends React.Component {
    render() {
        const defaultProps = {
            "transfer": SelectedWithdraw,
            "withdraw": SelectedWithdraw,
            "deposit": SelectedDeposit,
        };

        let backedCoins = GatewayStore.getState().backedCoins.has("TDEX")
            && GatewayStore.getState().backedCoins.get("TDEX");

        return (
            <AltContainer
                stores={[ModalStore, GatewayStore, SettingsStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    data: () => ModalStore.getState().data["selected_deposit_withdraw"],
                    modalIndex: () => ModalStore.getState().modalIndex,
                    resolve: () => ModalStore.getState().resolve,
                    backedCoins: backedCoins,
                    unit: SettingsStore.getState().defaults.unit,
                    depositFavorite: SettingsStore.getState().depositFavorite,
                    withdrawFavorite: SettingsStore.getState().withdrawFavorite,
                }}
            >
                <SelectedDepositWithdrawBind {...this.props} typeTransfer={defaultProps} />
            </AltContainer>
        );
    }
}

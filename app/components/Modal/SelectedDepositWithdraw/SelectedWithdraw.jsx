import React from "react";
import cn from "classnames";
import Translate from "react-translate-component";
import SettingsActions from "actions/SettingsActions";
import {Asset} from "lib/common/MarketClasses";
import ImageLoad from "Utility/ImageLoad";
import Icon from "Components/Icon/Icon";
import {getAlias} from "config/alias";

export default class SelectedWithdraw extends React.Component {

    static propTypes = {};

    constructor(props) {
        super(props);
    }


    componentDidUpdate(prevProps) {
        const {assetWithdrawBalance, withdrawFavorite, backedCoins} = this.props;
        if( assetWithdrawBalance && prevProps.assetWithdrawBalance !== assetWithdrawBalance && !withdrawFavorite.size) {
            let assets = Object.values(assetWithdrawBalance)
                .map(data => {
                    let coin = backedCoins.find(coin => coin.symbol === data.get("symbol"));
                    if( data.has("balance") && coin && coin.depositAllowed) {
                        return data;
                    }
                })
                .filter(a=>a)
                .sort((a,b) => a.get("balance") > b.get("balance"));
            if( assets ) {
                assets = assets.map(asset => asset.get("id"));
            } else {
                assets = Object.values(assetWithdrawBalance)
                    .map(asset => asset.get("id"));
            }

            this.changeFavorite(assets, true);
        }
    }


    changeFavorite = (unit, didMount ) => {
        let unitArray = unit.toJS ? unit.toJS() : unit;
        if( didMount ) {
            unitArray = unitArray.splice(0,6);
        }
        SettingsActions.changeWithdrawFavorite(unitArray);
    };

    selectedDeposit = (asset) => {
        this.props.showModalDW(asset, "sendWithdraw");
    };

    removeDeposit = (asset) => {
        let {withdrawFavorite} = this.props;
        withdrawFavorite = withdrawFavorite.delete(withdrawFavorite.indexOf(asset));
        this.changeFavorite(withdrawFavorite);
    };
    addFavorite = (asset) => {
        let {withdrawFavorite} = this.props;
        withdrawFavorite = withdrawFavorite.push(asset);
        this.changeFavorite(withdrawFavorite);
    };

    render() {
        const {withdrawFavorite, assetWithdrawBalance} = this.props;

        return (
            <div className="selected-dw">
                <Translate content={"transfer.withdraw.modal_title"} className={"selected-dw-modal-title"} component={"div"}  />

                <FindCurrency
                    assetWithdrawBalance={assetWithdrawBalance}
                    withdrawFavorite={withdrawFavorite}
                    selectedDeposit={this.selectedDeposit}
                    addFavorite={this.addFavorite}
                    removeDeposit={this.removeDeposit}
                />

                <div className="selected-dw-favorite">
                    {withdrawFavorite.size ? <><Translate content={"transfer.withdraw.currency_favorit"} className={"selected-dw-favorite-title"} component={"div"}  />
                    <div className="selected-dw-favorite-wrap">
                        {withdrawFavorite.map(asset_id => {
                            try {
                                let asset = assetWithdrawBalance[asset_id];
                                if( asset ) {
                                    let balance = asset.has("balance") ? asset.get("balance") : 0;
                                    let assetData = new Asset({
                                        "asset_id": asset.get("id"),
                                        "precision": asset.get("precision"),
                                        "amount": balance
                                    });
                                    return (
                                        <div className="selected-dw-favorite-btn" key={"selected-dw-favorite" + asset.get("id")} >
                                            <div className="selected-dw-favorite-btn-content"  onClick={()=>this.selectedDeposit(asset)}>
                                                <ImageLoad
                                                    customStyle={{maxWidth: 20, marginRight: 10, verticalAlign: "sub"}}
                                                    imageName={`${asset.get("symbol").toLowerCase()}.png`}/>
                                                <div className={"selected-dw-favorite-btn-wrap"}>
                                                    <div>{getAlias(asset.get("symbol"))}</div>
                                                    <small>{assetData.getAmount({real: true})}</small>
                                                    {/*<del onClick={()=>this.removeDeposit(asset.get("id"))} />*/}
                                                </div>
                                            </div>
                                            <Icon name={"sactive"} onClick={()=>this.removeDeposit(asset.get("id"))} />
                                        </div>
                                    );
                                }
                            } catch (e) {}
                        })}
                    </div></>: null}

                </div>
            </div>
        );
    }
}

class FindCurrency extends React.Component {
    state = {
        isShowDropDown: false,
        findValue: ""
    };
    setShowDropDown = (isShowDropDown) => {
        this.setState({isShowDropDown});
    };
    setFindValue = (findValue) => {
        this.setState({findValue});
    };
    selectedAsset = (asset) => {
        this.setShowDropDown(false);
        this.props.selectedDeposit(asset);
    };
    render(){
        const {isShowDropDown, findValue} = this.state;
        return (
            <div className="selected-dw-currency">
                <Translate content={"transfer.withdraw.currency_label"} className={"selected-dw-currency-label"} component={"label"}  />
                <div className="selected-dw-currency-drop-down">
                    <Translate
                        component="input" type="text" className="selected-dw-currency-input"
                        onFocus={()=>this.setShowDropDown(true)}
                        onChange={(event)=>this.setFindValue(event.target.value)}
                        attributes={{ placeholder: "transfer.deposit.currency_placeholder" }}
                    />

                    {isShowDropDown ? <Icon name={"menu-close"} className="selected-dw-currency-close" onClick={()=>this.setShowDropDown(false)}  /> : null}
                    {isShowDropDown ? <ListCurrency {...this.props} findValue={findValue} selectedAsset={this.selectedAsset}/> : null}
                </div>
            </div>
        );
    }
}

const ListCurrency = props => {
    let {assetWithdrawBalance, findValue, withdrawFavorite} = props;
    let assetBalancSort = Object.values(assetWithdrawBalance)
        .filter(asset => {

            if( findValue ) {
                let assetSymbol = asset.get("symbol");
                let aliasSymbol =  getAlias(assetSymbol);
                if( assetSymbol.toLowerCase().indexOf(findValue.toLowerCase()) !== -1 ||
                    aliasSymbol.toLowerCase().indexOf(findValue.toLowerCase()) !== -1 ) {
                    return asset;
                }
            } else {
                return asset;
            }
        })
        .sort((a,b) => {

            let aAsset = new Asset({
                asset_id: a.get("id"),
                precision: a.get("precision"),
                amount: a.has("balance") ? a.get("balance") : 0
            });
            let bAsset = new Asset({
                asset_id: b.get("id"),
                precision: b.get("precision"),
                amount: b.has("balance") ? b.get("balance") : 0
            });

            if( aAsset.getAmount({real: true}) < bAsset.getAmount({real: true}) ) {
                return 1;
            }
            if ( aAsset.getAmount({real: true}) > bAsset.getAmount({real: true})) {
                return -1;
            }
            return 0;
        });

    const toggleFavorite = (asset_id, hasFavorite) => {
        if( hasFavorite ) {
            props.removeDeposit(asset_id);
        } else {
            props.addFavorite(asset_id);

        }
    };

    return (
        <ul className="selected-dw-currency-list">
            {assetBalancSort.map(asset => {
                let balance = asset.has("balance") ? asset.get("balance") : "0.00";
                let hasFavorite = withdrawFavorite.find(a => a === asset.get("id"));
                let assetData = new Asset({
                    "asset_id": asset.get("id"),
                    "precision": asset.get("precision"),
                    "amount": balance
                });
                const hasNull = assetData.getAmount({real: true}) === 0;
                return (
                    <li key={"list-currency-" + asset.get("id")} className="selected-dw-currency-item">
                        <Icon name={hasFavorite ? "sactive" :"fi-star"} onClick={()=>toggleFavorite(asset.get("id"), hasFavorite)} />
                        <span className={cn("selected-dw-currency-item-name", {active: !hasNull})} onClick={ hasNull ? ()=>{} : ()=>props.selectedAsset(asset)}>
                            <ImageLoad
                                customStyle={{maxWidth: 25, marginRight: 10, marginLeft: 10, verticalAlign: "sub"}}
                                imageName={`${asset.get("symbol").toLowerCase()}.png`}/>
                            {getAlias(asset.get("symbol"))}
                        </span>
                        <span>{assetData.getAmount({real: true})}</span>
                    </li>
                );
            })}
        </ul>
    );
};
import React, {useState} from "react";
import Translate from "react-translate-component";
import SettingsActions from "actions/SettingsActions";
import {Asset} from "lib/common/MarketClasses";
import ImageLoad from "Utility/ImageLoad";
import Icon from "Components/Icon/Icon";
import {getAlias} from "config/alias";

// import DefaultModal from "Components/Modal/DefaultModal";
// import PropTypes from "prop-types";
// import AltContainer from "alt-container";
// import ModalStore from "app/stores/ModalStore";
// import ModalActions from "app/actions/ModalActions";
// import DefaultBaseModal from "Components/Modal/DefaultBaseModal";
//import loadable from "loadable-components";


export default class SelectedDeposit extends React.Component {

    static propTypes = {};

    constructor(props) {
        super(props);
    }


    componentDidUpdate(prevProps) {
        const {assetDepositBalance, depositFavorite, backedCoins} = this.props;
        if( assetDepositBalance && prevProps.assetDepositBalance !== assetDepositBalance && !depositFavorite.size) {
            let assets = Object.values(assetDepositBalance)
                .map(data => {
                    let coin = backedCoins.find(coin => coin.symbol === data.get("symbol"));
                    if( data.has("balance") && coin && coin.depositAllowed) {
                        return data;
                    }
                })
                .filter(a=>a)
                .sort((a,b) => a.get("balance") > b.get("balance"));
            if( assets ) {
                assets = assets.map(asset => asset.get("id"));
            } else {
                assets = Object.values(assetDepositBalance)
                    .map(asset => asset.get("id"));
            }

            this.changeFavorite(assets, true);
        }
    }


    changeFavorite = (unit, didMount ) => {
        let unitArray = unit.toJS ? unit.toJS() : unit;
        if( didMount ) {
            unitArray = unitArray.splice(0,6);
        }
        SettingsActions.changeDepositFavorite(unitArray);
    };

    selectedDeposit = (asset) => {
        this.props.showModalDW(asset, "depositAddress");
    };

    removeDeposit = (asset) => {
        let {depositFavorite} = this.props;
        depositFavorite = depositFavorite.delete(depositFavorite.indexOf(asset));
        this.changeFavorite(depositFavorite);
    };
    addFavorite = (asset) => {
        let {depositFavorite} = this.props;
        depositFavorite = depositFavorite.push(asset);
        this.changeFavorite(depositFavorite);
    };

    render() {
        const {depositFavorite, assetDepositBalance} = this.props;

        return (
            <div className="selected-dw">
                <Translate content={"transfer.deposit.modal_title"} className={"selected-dw-modal-title"} component={"div"}  />

                <FindCurrency
                    assetDepositBalance={assetDepositBalance}
                    depositFavorite={depositFavorite}
                    selectedDeposit={this.selectedDeposit}
                    addFavorite={this.addFavorite}
                    removeDeposit={this.removeDeposit}
                />

                <div className="selected-dw-favorite">
                    {depositFavorite.size ?
                        <Translate content={"transfer.deposit.currency_favorit"} className={"selected-dw-favorite-title"} component={"div"}  /> : null}
                    <div className="selected-dw-favorite-wrap">
                        {depositFavorite.size ? depositFavorite.map(asset_id => {
                            try {
                                let asset = assetDepositBalance[asset_id];
                                if( asset ) {
                                    let balance = asset.has("balance") ? asset.get("balance") : 0;
                                    let assetData = new Asset({
                                        "asset_id": asset.get("id"),
                                        "precision": asset.get("precision"),
                                        "amount": balance
                                    });
                                    return (
                                        <div className="selected-dw-favorite-btn" key={"selected-dw-favorite" + asset.get("id")} >
                                            <div className="selected-dw-favorite-btn-content" onClick={()=>this.selectedDeposit(asset)}>
                                                <ImageLoad
                                                    customStyle={{maxWidth: 20, marginRight: 10, verticalAlign: "sub"}}
                                                    imageName={`${asset.get("symbol").toLowerCase()}.png`}/>
                                                <div className={"selected-dw-favorite-btn-wrap"}>
                                                    <div>{getAlias(asset.get("symbol"))}</div>
                                                    <small>{assetData.getAmount({real: true})}</small>
                                                </div>
                                            </div>
                                            <Icon name={"sactive"} onClick={()=>this.removeDeposit(asset.get("id"))} />

                                        </div>
                                    );
                                }
                            } catch (e) {}
                        }) : null }
                    </div>

                </div>
            </div>
        );
    }
}

class FindCurrency extends React.Component {
    state = {
        isShowDropDown: false,
        findValue: ""
    };
    setShowDropDown = (isShowDropDown) => {
        this.setState({isShowDropDown});
    };
    setFindValue = (findValue) => {
        this.setState({findValue});
    };
    selectedAsset = (asset) => {
        this.setShowDropDown(false);
        this.props.selectedDeposit(asset);
    };
    render(){
        const {isShowDropDown, findValue} = this.state;
        return (
            <div className="selected-dw-currency">
                <Translate content={"transfer.deposit.currency_label"} className={"selected-dw-currency-label"} component={"label"}  />
                <div className="selected-dw-currency-drop-down">
                    <Translate
                        component="input" type="text" className="selected-dw-currency-input"
                        onFocus={()=>this.setShowDropDown(true)}
                        onChange={(event)=>this.setFindValue(event.target.value)}
                        attributes={{ placeholder: "transfer.deposit.currency_placeholder" }}
                    />

                    {isShowDropDown ? <Icon name={"menu-close"} className="selected-dw-currency-close" onClick={()=>this.setShowDropDown(false)}  /> : null}
                    {isShowDropDown ? <ListCurrency {...this.props} findValue={findValue} selectedAsset={this.selectedAsset}/> : null}
                </div>
            </div>
        );
    }
}


const ListCurrency = props => {
    console.log("ListCurrency props", props);
    let {assetDepositBalance, findValue, depositFavorite} = props;
    let assetBalancSort = Object.values(assetDepositBalance)
        .filter(asset => {
            if( findValue ) {
                let assetSymbol = asset.get("symbol");
                let aliasSymbol =  getAlias(assetSymbol);
                if( assetSymbol.toLowerCase().indexOf(findValue.toLowerCase()) !== -1 ||
                    aliasSymbol.toLowerCase().indexOf(findValue.toLowerCase()) !== -1 ) {
                    return asset;
                }
            } else {
                return asset;
            }
        })
        .sort((a,b) => {
            let aBalance = getAlias(a.get("symbol"));
            let bBalance = getAlias(b.get("symbol"));
            if( aBalance > bBalance ) {
                return 1;
            }
            if ( aBalance < bBalance) {
                return -1;
            }
            return 0;
        });

    const toggleFavorite = (asset_id, hasFavorite) => {
        if( hasFavorite ) {
            props.removeDeposit(asset_id);
        } else {
            props.addFavorite(asset_id);

        }
    };

    return (
        <ul className="selected-dw-currency-list">
            {assetBalancSort.map(asset => {
                let balance = asset.has("balance") ? asset.get("balance") : "0.00";
                let hasFavorite = depositFavorite.find(a => a === asset.get("id"));
                let assetData = new Asset({
                    "asset_id": asset.get("id"),
                    "precision": asset.get("precision"),
                    "amount": balance
                });
                return (
                    <li key={"list-currency-" + asset.get("id")} className="selected-dw-currency-item ">
                        <Icon name={hasFavorite ? "sactive" :"fi-star"} onClick={()=>toggleFavorite(asset.get("id"), hasFavorite)} />
                        <span className="selected-dw-currency-item-name active" onClick={()=>props.selectedAsset(asset)}>
                            <ImageLoad
                                customStyle={{maxWidth: 25, marginRight: 10, marginLeft: 10, verticalAlign: "sub"}}
                                imageName={`${asset.get("symbol").toLowerCase()}.png`}/>
                            {getAlias(asset.get("symbol"))}
                        </span>
                        <span>{assetData.getAmount({real: true})}</span>
                    </li>
                );
            })}
        </ul>
    );
};

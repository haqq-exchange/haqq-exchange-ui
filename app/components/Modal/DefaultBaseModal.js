import React from "react";
import PropTypes from "prop-types";


import "./defaultModal.scss";
class DefaultBaseModal extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            isOpenModal: false
        };
    }


    shouldComponentUpdate(prevProps, prevState){
        const {modalId, modals} = this.props;
        return modals[modalId] !== prevProps[modalId] ||
            prevState.isOpenModal !== this.state.isOpenModal;
    }


    //componentDidMount(){}

    componentDidUpdate() {
        const {resolve, modalId, modals} = this.props;
        const {isOpenModal} = this.state;
        
        //console.log("setting_wallet_modal", modalId, modals);

        if (resolve) {
            if (modals[modalId] && !isOpenModal) {
                this.setState({
                    isOpenModal: true
                }, () => {
                    this.setComponentUpdate(modalId);
                });
            } else  if( !modals[modalId] && isOpenModal ) {
                this.setState({
                    isOpenModal: false
                }, resolve);
            }
        } else if(!modals[modalId] && isOpenModal ) {
            this.setState({
                isOpenModal: false
            }, () => {
                this.setComponentUpdate(modalId);
            });
        }
    }

    setComponentUpdate(){}


}

DefaultBaseModal.defaultProps = {
    modalId: "default_base_modal"
};

export default DefaultBaseModal;

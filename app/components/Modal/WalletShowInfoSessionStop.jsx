import React from "react";
import Translate from "react-translate-component";
import DefaultModal from "components/Modal/DefaultModal";


import ls from "common/localStorage";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import ModalActions from "actions/ModalActions";
import WalletUnlockActions from "actions/WalletUnlockActions";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";



class WalletShowInfoSessionStop extends DefaultBaseModal {
    constructor(props) {
        super(props);
        this.state = {
            lastSecond: 30
        };
        this.deadline = new Date().setSeconds(30);
    }

    afterCloseModal = () => {

    };

    closeModal = () => {
        const {resolve, modalId} = this.props;
        console.log("closeModal this.props ", this.props);
        ModalActions.hide(modalId)
            .then(resolve);
    };

    showAuth = () => {
        this.closeModal();
        ModalActions.show("unlock_wallet_modal_public")
    };

    render() {
        const {isOpenModal, lastSecond} = this.state;
        const {modalId, modalIndex } = this.props;

        return (
            <DefaultModal
                id={modalId}
                modalId={modalId}
                ref={(ref) => this.RefUnlockModal = ref}
                isOpen={isOpenModal}
                className="UnlockModal"
                onAfterOpen={this.afterOpenModal}
                shouldCloseOnOverlayClick={false}
                shouldCloseOnEsc={false}
                customStyle={{zIndex: modalIndex[modalId]}}
                onRequestClose={this.closeModal}>
                <div className="modal fadeIn ng-enter is-active ng-enter-active">
                    <div className="public-login-modal">
                        <div className="modal-header">
                            <a onClick={this.closeModal} className="close-button">×</a>
                            <div className="modal__logo">
                                    {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?  <div  style={{backgroundImage: "url("+ `https://static.haqq.exchange/gbl/images/logo-gbl.png` +")", height: "60px", width: "205px", margin: "0 auto", backgroundRepeat: "no-repeat"}} />
                                    : (__SCROOGE_CHAIN__ ? <div  style={{backgroundImage: "url("+ `https://static.scrooge.club/images/general-logo.png` +")", height: "40px", width: "205px", margin: "0 auto"}} /> 
                                    : <span className="logo-logotype" style={{height: 45}} />
                                    // <img src={require("assets/logo-ico-blue.png")} />
                                    )}
                            </div>
                        </div>
                        <div className="modal-content">
                            <h3>Ваша сессия закончилась</h3>

                            <div style={{
                                "display": "flex",
                                "justifyContent": "space-between",
                                "maxWidth": "300px",
                                "margin": "0 auto"
                            }}>
                                <button type={"button"} className={"btn btn-green"} onClick={this.closeModal}>
                                    Закрыть окно
                                </button>
                                <button type={"button"} className={"btn btn-red"} onClick={this.showAuth}>
                                    Авторизоваться
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </DefaultModal>
        );
    }
}


WalletShowInfoSessionStop.defaultProps = {
    modalId: "show_info_session_stop"
};

export default class PrivateContactModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    modalIndex: () => ModalStore.getState().modalIndex,
                    resolve: () => ModalStore.getState().resolve
                }}
            >
                <WalletShowInfoSessionStop {...this.props} />
            </AltContainer>
        );
    }
}

import React from "react";
import PropTypes from "prop-types";
import Modal from "react-modal";
import SettingsStore from "stores/SettingsStore";


import "./defaultModal.scss";
// import ModalStore from "../../stores/ModalStore";

class DefaultModal extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    static state = {
        name: {}
    };

    componentDidMount(){}

    render() {
        const { modalId , customStyle, className } = this.props;
        var theme = SettingsStore.getState().settings.get("themes");

        const customStyles = {
            overlay : Object.assign({},  customStyle || {})
        };
        
        // console.log("customStyles", modalId , customStyles)

        return (
            <Modal
                id={modalId}
                parentSelector={() => document.getElementById("content-wrapper")}
                ariaHideApp={false}
                ref={(ref)=>this.InfoModal=ref}
                /*className="Modal"*/
                overlayClassName={["Overlay", className, theme].join(" ")}
                style={customStyles}
                {...this.props} >

                {this.props.children}
            </Modal>
        );
    }
}

DefaultModal.defaultProps = {
    modalId: "default_modal"
};

export default DefaultModal;



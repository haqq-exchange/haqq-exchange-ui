import React from "react";
import PropTypes from "prop-types";
import Translate from "react-translate-component";
import DefaultModal from "components/Modal/DefaultModal";


import ls from "common/localStorage";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import WalletUnlockStore from "stores/WalletUnlockStore";
import AccountStore from "stores/AccountStore";
import WalletManagerStore from "stores/WalletManagerStore";
import WalletDb from "stores/WalletDb";
import BackupStore from "stores/BackupStore";
import ModalActions from "actions/ModalActions";
import WalletUnlockActions from "actions/WalletUnlockActions";
import {Login2fa, LoginLW, LoginOldContainer} from "../../RoutesLink";
import SettingsStore from "../../stores/SettingsStore";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";

//let accountStorage = new ls("__deexgraphene__");


class WalletShowInfoSessionTimout extends DefaultBaseModal {


    constructor(props) {
        super(props);
        this.state = {
            lastSecond: 30
        };
        this.deadline = new Date().setSeconds(30);
    }


    timeRemaining = ( currentTime ) => {
        if( currentTime ) {
            this.lastSecond = setTimeout(()=>{
                let nextTime = currentTime -1;
                this.setState({
                    lastSecond: nextTime
                });
                this.timeRemaining(nextTime);
            }, 1000);
        } else {
            ModalActions.show("show_info_session_stop");
            this.closeModal();
            clearTimeout(this.lastSecond);
        }
    };

    afterOpenModal = () => {
        this.timeRemaining(30);
    };

    afterCloseModal = () => {

    };

    closeModal = () => {
        const {resolve, modalId} = this.props;
        console.log("closeModal this.props ", this.props);
        ModalActions.hide(modalId)
            .then(()=>{
                resolve();
            });
    };


    nextProcess = () => {
        WalletUnlockActions.setTimeOut();
        this.afterCloseModal();
        this.closeModal();
        clearTimeout(this.lastSecond);
    };

    render() {
        const {isOpenModal, lastSecond} = this.state;
        const {modalId, modalIndex } = this.props;


        return (
            <DefaultModal
                id={modalId}
                modalId={modalId}
                ref={(ref) => this.RefUnlockModal = ref}
                isOpen={isOpenModal}
                className="UnlockModal"
                onAfterOpen={this.afterOpenModal}
                shouldCloseOnOverlayClick={false}
                shouldCloseOnEsc={false}
                customStyle={{zIndex: 1100}}
                onRequestClose={this.closeModal}>
                <div className="modal fadeIn ng-enter is-active ng-enter-active">
                    <div className="public-login-modal">
                        <div className="modal-header">
                            <a onClick={this.closeModal} className="close-button">×</a>
                            <div className="modal__logo">
                                {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?   <div  style={{backgroundImage: "url("+ `https://static.haqq.exchange/gbl/images/logo-gbl.png` +")", height: "60px", width: "205px", margin: "0 auto", backgroundRepeat: "no-repeat"}} />
                                : (__SCROOGE_CHAIN__ ? <div  style={{backgroundImage: "url("+ `https://static.scrooge.club/images/general-logo.png` +")", height: "40px", width: "205px", margin: "0 auto"}} /> 
                                : <span className="logo-logotype" style={{height: 45}} />
                                // <img src={require("assets/logo-ico-blue.png")} />
                                )}
                            </div>
                        </div>
                        <div className="modal-content">
                            <h3>Осталось мало времени, ~{lastSecond} сек</h3>

                            <button type={"button"} className={"btn btn-green"} onClick={()=>this.nextProcess()}>
                                Продолжить работу сайта
                            </button>
                        </div>
                    </div>
                </div>
            </DefaultModal>
        );
    }
}


WalletShowInfoSessionTimout.defaultProps = {
    modalId: "show_info_session_timout"
};

export default class PrivateContactModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    modalIndex: () => ModalStore.getState().modalIndex,
                    resolve: () => ModalStore.getState().resolve
                }}
            >
                <WalletShowInfoSessionTimout {...this.props} />
            </AltContainer>
        );
    }
}

import React from "react";
import PropTypes from "prop-types";
import Translate from "react-translate-component";
import DefaultModal from "components/Modal/DefaultModal";


import ls from "common/localStorage";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import WalletUnlockStore from "stores/WalletUnlockStore";
import AccountStore from "stores/AccountStore";
import WalletManagerStore from "stores/WalletManagerStore";
import WalletDb from "stores/WalletDb";
import BackupStore from "stores/BackupStore";
import ModalActions from "actions/ModalActions";
import {Login2fa, LoginLW, LoginOldContainer} from "../../RoutesLink";
import SettingsStore from "../../stores/SettingsStore";
import configConst from "config/const";
let accountStorage = new ls(configConst.STORAGE_KEY);


class WalletUnlockModal extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor(props) {
        super(props);
        this.state = {
            isOpenModal: false,
            isLogin2fa: accountStorage.has("qrCode"),
            iSee: false
        };

    }

    componentDidUpdate() {
        const {resolve, modalId, modals} = this.props;
        const {isOpenModal} = this.state;

        // console.log("componentDidUpdate", this.props, this.state );
        //debugger;
        if (resolve) {
            if (modals[modalId] && !isOpenModal) {
                this.setState({
                    isOpenModal: true
                });
            } else  if( !modals[modalId] && isOpenModal ) {
                this.setState({
                    isOpenModal: false
                });
                resolve();
            }
        } else if(!modals[modalId] && isOpenModal ) {
            this.setState({
                isOpenModal: false
            });
        }

    }

    afterOpenModal = () => {};

    afterCloseModal = () => {
        const {resolve, modalId} = this.props;
        console.log("afterCloseModal this.props ", this.props);
    };

    closeModal = () => {
        const {resolve, reject, modalId, isLocked} = this.props;
        console.log("closeModal this.props ", this.props, isLocked);
        ModalActions.hide(modalId)
            .then(()=>{
                console.log("closeModal this.props >>>> ", this.props, isLocked);
                if( isLocked ) {
                    reject();
                } else {
                    resolve();
                }

            })
            .catch(reject);
    };


    render() {
        const {isOpenModal} = this.state;
        const { isLogin2fa } = this.state;
        const {modalId, walletNames, passwordLogin} = this.props;

        // console.log("passwordLogin", passwordLogin);
        // console.log("isOpenModal", isOpenModal);

        let showAuthComponent = null;
        switch (passwordLogin) {
            case "login2fa":
                showAuthComponent = <Login2fa accountQrCode={accountStorage.get("qrCode")} />;
                break;
            case "loginWallet":
                showAuthComponent = <LoginLW />;
                break;
            case "login":
                showAuthComponent = <LoginOldContainer />;
                break;
            default:
                showAuthComponent = <LoginOldContainer />;
                break;

        }

        return (
            <DefaultModal
                id={modalId}
                ref={(ref) => this.RefUnlockModal = ref}
                isOpen={isOpenModal}
                className="UnlockModal"
                onAfterOpen={this.afterOpenModal}
                onAfterClose={this.afterCloseModal}
                shouldCloseOnOverlayClick={false}
                shouldCloseOnEsc={false}
                /*customStyle={{zIndex: 1010}}*/
                /*onRequestClose={this.closeModal}*/ >
                <div className="modal fadeIn ng-enter is-active ng-enter-active">
                    <div className="public-login-modal">
                        <div className="modal-header">
                            <a onClick={this.closeModal} className="close-button">×</a>
                            <div className="modal__logo">
                            {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?  <div  style={{backgroundImage: "url("+ `https://static.haqq.exchange/gbl/images/logo-gbl.png` +")", height: "60px", width: "205px", margin: "0 auto", backgroundRepeat: "no-repeat"}} />
                            : (__SCROOGE_CHAIN__ ? <div  style={{backgroundImage: "url("+ `https://static.scrooge.club/images/general-logo.png` +")", height: "40px", width: "205px", margin: "0 auto"}} /> 
                            : <span className="logo-logotype" style={{height: 45}} />
                            // <img src={require("assets/logo-ico-blue.png")} />
                            )}
                            </div>
                        </div>
                        <div className="modal-content">
                            {showAuthComponent}
                        </div>
                    </div>
                </div>
            </DefaultModal>
        );
    }
}


WalletUnlockModal.defaultProps = {
    modalId: "unlock_wallet_modal_public"
};

class WalletUnlockModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[
                    WalletUnlockStore,
                    ModalStore,
                    AccountStore,
                    WalletManagerStore,
                    WalletDb,
                    BackupStore
                ]}
                inject={{
                    currentWallet: () =>
                        WalletManagerStore.getState().current_wallet,
                    walletNames: () =>
                        WalletManagerStore.getState().wallet_names,
                    dbWallet: () => WalletDb.getWallet(),
                    isLocked: () => WalletDb.isLocked(),
                    backup: () => BackupStore.getState(),
                    modals: () => ModalStore.getState().modals,
                    resolve: () => ModalStore.getState().resolve,
                    reject: () => ModalStore.getState().reject,
                    //resolve: () => WalletUnlockStore.getState().resolve,
                    //reject: () => WalletUnlockStore.getState().reject,
                    locked: () => WalletUnlockStore.getState().locked,
                    passwordLogin: () =>
                        SettingsStore.getState().settings.get(
                            "passwordLogin"
                        ),
                    passwordAccount: () =>
                        AccountStore.getState().passwordAccount || ""
                }}
            >
                <WalletUnlockModal {...this.props} />
            </AltContainer>
        );
    }
}
export default WalletUnlockModalContainer;

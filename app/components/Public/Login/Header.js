import React from "react";
import PublicHeader from "../../Layout/PublicHeader";
import Translate from "react-translate-component";
import {Link} from "react-router-dom";
import * as RoutesLink from '../../../RoutesLink';
const translator = require("counterpart");

export default class LoginHeader extends React.Component {
    static defaultProps = {
        contentStatic: {
            "login": "password",
            "login-2fa": "password-2fa",
            "login-lw": "wallet",
        }
    };

    render(){
        const {contentStatic, params} = this.props;
        let createLink = ["","create-account", contentStatic[params.type] || "password" ];
        return(
            <div className={"login-header"}>
                {/*<PublicHeader />*/}

                <div className="public-container top text-center">
                    {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?
                    <Translate
                        content={"public.welcome_to_deex_gbl"}
                        component="h2"
                    />:
                    <Translate
                        content={__SCROOGE_CHAIN__ ? "public.welcome_to_deex_scrooge" : "public.welcome_to_haqq"}
                        component="h2"
                    />}

                    <p className="public-gray-p">
                        {translator.translate("public.please_login")}
                        &nbsp;
                        <Link className="public-link" to={createLink.join("/")}>
                            <Translate content="public.create_a_new_account" />
                        </Link>
                    </p>
                </div>
            </div>
        );
    }
}
import React from "react";
import PropTypes from "prop-types";
import LoginHeader from "./Header";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";
import TranslateWithLinks from "../../Utility/TranslateWithLinks";
import * as RoutesLink from "../../../RoutesLink";
import AccountStore from "../../../stores/AccountStore";

class FooterLogin extends React.PureComponent{
    render(){
        return(
            <div className="public-gray-p-small">
                <div>
                    {__SCROOGE_CHAIN__ ? null :
                    <>
                        <Link to="/authorization/login-2fa">
                            <Translate content="public.log_in_using_2fa" />
                        </Link>
                        &nbsp;
                        <Translate content="public.or" />
                    </>}
                    &nbsp;
                    <Link to="/authorization/login-lw">
                        <Translate content="public.login_using_local_wallet" />
                    </Link>
                </div>
                <br/>
                <div>

                    <TranslateWithLinks
                        string="public.formatter"
                        keys={[
                            {
                                type: "link",
                                value: "/wallet/backup/restore",
                                translation:
                                    "public.restore_link",
                                arg: "restore_link"
                            },
                            {
                                type: "link",
                                value: "/create-account/wallet",
                                translation:
                                    "public.restore_form",
                                arg: "restore_form"
                            }
                        ]}
                    />
                </div>
            </div>
        );
    }
}
class FooterLogin2fa extends React.PureComponent {
    render(){

        return (
            <div className="public-gray-p-small">
                <div>
                    <Link to="/authorization/login">
                        <Translate content="public.log_in_using_account" />
                    </Link>
                    &nbsp;
                    <Translate content="public.or" />
                    &nbsp;
                    <Link to="/authorization/login-lw">
                        <Translate content="public.login_using_local_wallet" />
                    </Link>
                </div>
                <br/>
                <div>

                    <TranslateWithLinks
                        string="public.formatter"
                        keys={[
                            {
                                type: "link",
                                value: "/wallet/backup/restore",
                                translation:
                                    "public.restore_link",
                                arg: "restore_link"
                            },
                            {
                                type: "link",
                                value: "/create-account/wallet",
                                translation:
                                    "public.restore_form",
                                arg: "restore_form"
                            }
                        ]}
                    />
                </div>
            </div>
        );
    }
}
class FooterLoginLw extends React.PureComponent {
    render(){
        return(
            <div className="public-gray-p-small">
                <Link to="/authorization/login">
                    <Translate content="public.log_in_using_account" />
                </Link>
                &nbsp;
                <Translate content="public.or" />
                &nbsp;
                <Link to="/create-account/password">
                    <Translate content="public.create_an_account" />
                </Link>
            </div>
        );
    }
}
export default class LoginBody extends React.PureComponent {

    static propTypes = {
        route: PropTypes.object
    };

    static defaultProps = {
        contentStatic: {
            "login": {
                children: RoutesLink.LoginOldContainer,
                footer: FooterLogin
            },
            "login-2fa": {
                children: RoutesLink.Login2fa,
                footer: FooterLogin2fa
            },
            "login-lw": {
                children: RoutesLink.LoginLW,
                footer: FooterLoginLw
            }
        }
    };

    render(){
        const {contentStatic, match} = this.props;

        const ComponentFooter = contentStatic[match.params.type].footer;
        let { passwordAccount } = AccountStore.getState();
        
        console.log("passwordAccount", passwordAccount)

        if( passwordAccount ) {
            this.props.history.push("/");
        }

        return(
            <div className={"login-body"} style={{width: "100%"}}>
                <LoginHeader {...match} />
                {/*<ComponentChildren {...this.props} />*/}
                {this.props.children}
                <ComponentFooter />
            </div>
        );
    }
}


import React from "react";

class Form extends React.Component {
    render() {
        return (
            <div className={"public-form " + this.props.className}>
                <div className="public-fields">{this.props.children}</div>

                <div className="public-shadow" />
            </div>
        );
    }
}

export default Form;

import React from "react";
import PropTypes from "prop-types";
import ZfApi from "react-foundation-apps/src/utils/foundation-api";
import AltContainer from "alt-container";
import WalletDb from "stores/WalletDb";
import WalletUnlockStore from "stores/WalletUnlockStore";
import WalletManagerStore from "stores/WalletManagerStore";
import SettingsActions from "actions/SettingsActions";
import BackupStore from "stores/BackupStore";
import AccountStore from "stores/AccountStore";
import Crypto from "lib/common/Crypto";
import classnames from "classnames";

import WalletUnlockActions from "actions/WalletUnlockActions";
import WalletActions from "actions/WalletActions";
import BackupActions, {restore, backup} from "actions/BackupActions";
import AccountActions from "actions/AccountActions";
import utils from "common/utils";
import AccountName from "./AccountName";
import {PrivateKey} from "deexjs";
import {saveAs} from "file-saver";
import counterpart from "counterpart";
import {
    WalletSelector,
    CreateLocalWalletLink,
    WalletDisplay,
    CustomPasswordInput,
    LoginButtons,
    BackupWarning,
    BackupFileSelector,
    DisableChromeAutocomplete,
    CustomError,
    KeyFileLabel
} from "../Wallet/WalletUnlockModalLib";
import {backupName} from "common/backupUtils";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";
import TranslateWithLinks from "../Utility/TranslateWithLinks";
import Form from "./Form";
import Field from "./Field";
import LoginBody from "./Login/Body";
import QrReader from "react-qr-reader";
import ls from "common/localStorage";
let accountStorage = new ls("__deexgraphene__");

class LoginOld extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor(props) {
        super(props);
        this.state = this.initialState(props);

        this.handleScan = this.handleScan.bind(this);
    }

    initialState = (props = this.props) => {
        const {passwordAccount, currentWallet} = props;
        return {
            passwordError: null,
            accountName: passwordAccount,
            walletSelected: !!currentWallet,
            customError: null,
            isOpen: false,
            restoringBackup: false,
            stopAskingForBackup: false,
            delay: 300,
            qrCode: false,
            showScanner: false,
        };
    };

    UNSAFE_componentWillReceiveProps(np) {
        const {walletSelected, restoringBackup, accountName} = this.state;
        const {
            currentWallet: newCurrentWallet,
            passwordAccount: newPasswordAccount
        } = np;

        const newState = {};
        if (newPasswordAccount && !accountName)
            newState.accountName = newPasswordAccount;
        if (walletSelected && !restoringBackup && !newCurrentWallet)
            newState.walletSelected = false;
        if (this.props.passwordLogin != np.passwordLogin) {
            newState.passwordError = false;
            newState.customError = null;
        }

        this.setState(newState);
    }

    shouldComponentUpdate(np, ns) {
        return (
            !utils.are_equal_shallow(np, this.props) ||
            !utils.are_equal_shallow(ns, this.state)
        );
    }

    componentDidMount() {
        const {modalId} = this.props;
        SettingsActions.changeSetting({
            setting: "passwordLogin",
            value: "login"
        });

        ZfApi.subscribe(modalId, (name) => {
            if (name !== modalId) return;
        });
    }

    componentDidUpdate() {
        const {resolve, modalId, isLocked} = this.props;

        if (resolve)
            if (isLocked) {
                ZfApi.publish(modalId, "open");
            } else {
                resolve();
            }
        else ZfApi.publish(this.props.modalId, "close");
    }

    handleScan(data){
        if(data){
            this.setState({
                qrCode: data,
                showScanner: false
            });
            accountStorage.set("qrCode", data);
        }
    }
    handleError(err){
        console.error(err);
    }

    handleLogin = (event) => {
        event.preventDefault();
        const {accountQrCode} = this.props;
        const {qrCode, password_input, account_name} = this.state;
        if( (accountQrCode || qrCode) && account_name && password_input) {
            const decrypted = Crypto.decrypt(accountQrCode || qrCode, password_input);
            if( decrypted ) {
                this.validate(decrypted, account_name);
            }
        }
    };

    validate = (password, account) => {
        const {resolve} = this.props;
        const {cloudMode} = WalletDb.validatePassword(
            password || "",
            true, //unlock
            account
        );

        if (WalletDb.isLocked()) {
            this.setState({passwordError: true});
        } else {
            if (cloudMode) {
                AccountActions.setPasswordAccount(account);
            }
            WalletUnlockActions.change();
            if (resolve) {
                resolve();
            }
            WalletUnlockActions.cancel();
            if(this.props.router) {
                this.props.router.push("/");
            }
        }
    };

    render() {

        const {
            showScanner,
            qrCode,
            passwordError,
            customError,
            accountName
        } = this.state;

        const {accountQrCode} = this.props;

        const errorMessage = passwordError
            ? counterpart.translate("wallet.pass_incorrect")
            : customError;

        return (
            <div className="public-container short">
                <form onSubmit={this.handleLogin} className="full-width">
                    <Form>
                        <DisableChromeAutocomplete />
                        <AccountName
                            label="account.name"
                            ref="account_input"
                            accountName={accountName}
                            account={accountName}
                            onChange={(name)=>{
                                this.setState({account_name: name});
                            }}
                            onAccountChanged={() => {}}
                            size={60}
                            hideImage
                            placeholder=" "
                            useHR
                            labelClass="login-label"
                            reserveErrorSpace
                            onEnter={()=>{}}
                        />
                        <Field
                            type="password"
                            ref="password_input"
                            inputRef="password_input"
                            name="password"
                            id="password"
                            error={errorMessage}
                            placeholder={counterpart.translate(
                                "settings.password"
                            )}
                            onChange={(event)=>{
                                this.setState({password_input: event.target.value});
                            }}
                        />
                        {!showScanner && !accountQrCode &&
                            <button
                                type="button"
                                onClick={()=>{
                                    this.setState({showScanner: true});
                                }}
                                className={
                                    classnames("button public-button", {
                                        "button-active": !!qrCode
                                    })
                                }>
                                <Translate content={"public.show_scanner_qrcode"}/>
                            </button>
                        }
                        {showScanner &&
                            <QrReader
                                delay={this.state.delay}
                                onError={this.handleError}
                                onScan={this.handleScan}
                                style={{ width: "100%" }}
                            />}

                        <div className="public-buttons">
                            <button
                                type="button"
                                onClick={this.handleLogin}
                                className="button public-button">
                                <Translate content={"header.unlock_short"}/>
                            </button>
                        </div>
                    </Form>
                </form>
            </div>
        );
    }
}

class LoginOldContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[
                    WalletUnlockStore,
                    AccountStore,
                    WalletManagerStore,
                    WalletDb,
                    BackupStore
                ]}
                inject={{
                    currentWallet: () =>
                        WalletManagerStore.getState().current_wallet,
                    walletNames: () =>
                        WalletManagerStore.getState().wallet_names,
                    dbWallet: () => WalletDb.getWallet(),
                    isLocked: () => WalletDb.isLocked(),
                    backup: () => BackupStore.getState(),
                    resolve: () => WalletUnlockStore.getState().resolve,
                    reject: () => WalletUnlockStore.getState().reject,
                    locked: () => WalletUnlockStore.getState().locked,
                    passwordLogin: () =>
                        WalletUnlockStore.getState().passwordLogin,
                    passwordAccount: () =>
                        AccountStore.getState().passwordAccount || ""
                }}
            >
                <LoginOld {...this.props} />
            </AltContainer>
        );
    }
}
export default LoginOldContainer;

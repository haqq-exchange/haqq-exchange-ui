import React from "react";
import PropTypes from "prop-types";
import Translate from "react-translate-component";
import DefaultModal from "components/Modal/DefaultModal";


import ls from "common/localStorage";
import AltContainer from "alt-container";
import ModalStore from "stores/ModalStore";
import WalletUnlockStore from "stores/WalletUnlockStore";
import AccountStore from "stores/AccountStore";
import WalletManagerStore from "stores/WalletManagerStore";
import WalletDb from "stores/WalletDb";
import BackupStore from "stores/BackupStore";
import ModalActions from "actions/ModalActions";
import WalletUnlockActions from "actions/WalletUnlockActions";
import {Login2fa, LoginLW, LoginOldContainer} from "../../RoutesLink";
import SettingsStore from "../../stores/SettingsStore";
import DefaultBaseModal from "Components/Modal/DefaultBaseModal";

let accountStorage = new ls("__deexgraphene__");


class WalletShowInfoSessionTimout extends DefaultBaseModal {


    constructor(props) {
        super(props);

        this.deadline = new Date().setSeconds(30);
        this.startInterval = null;
    }

    componentDidMount() {


    }

    timeRemaining = () => {
        const _this = this;
        console.log("deadline", this.deadline);
        this.startInterval = setInterval(()=>{
            var t = Date.parse(new Date(this.deadline)) - Date.parse(new Date());
            console.log("t", t);
            var seconds = Math.floor( (t/1000) % 60 );
            console.log("seconds", seconds);
            _this.setState({
                lastSecond: seconds
            });
        }, 1000);
    };

    afterOpenModal = () => {
        // this.timeRemaining();
    };

    afterCloseModal = () => {
        clearInterval(this.startInterval);
        this.startInterval = null;
    };

    closeModal = () => {
        const {resolve, modalId} = this.props;
        console.log("closeModal this.props ", this.props);
        ModalActions.hide(modalId)
            .then(()=>{
                console.log("closeModal ModalActions.hide ");
                resolve();
            });
    };

    nextProcess = () => {
        WalletUnlockActions.setTimeOut();
        this.afterCloseModal();
        this.closeModal();
    };

    render() {
        const {isOpenModal, lastSecond} = this.state;
        const {modalId } = this.props;

        console.log("lastSecond", lastSecond);

        return (
            <DefaultModal
                id={modalId}
                ref={(ref) => this.RefUnlockModal = ref}
                isOpen={isOpenModal}
                className="UnlockModal"
                onAfterOpen={this.afterOpenModal}
                onAfterClose={this.afterCloseModal}
                shouldCloseOnOverlayClick={false}
                shouldCloseOnEsc={false}
                onRequestClose={this.closeModal}>
                <div className="modal fadeIn ng-enter is-active ng-enter-active">
                    <div className="public-login-modal">
                        <div className="modal-header">
                            <a onClick={this.closeModal} className="close-button">×</a>
                            <div className="modal__logo">
                            {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?  <div  style={{backgroundImage: "url("+ `https://static.deex.exchange/gbl/images/logo-gbl.png` +")", height: "60px", width: "205px", margin: "0 auto", backgroundRepeat: "no-repeat"}} />
                            : (__SCROOGE_CHAIN__ ? <div  style={{backgroundImage: "url("+ `https://static.scrooge.club/images/general-logo.png` +")", height: "40px", width: "205px", margin: "0 auto"}} /> 
                            : <img src={require("assets/logo-ico-blue.png")} />)}
                            </div>
                        </div>
                        <div className="modal-content">
                            <h3>Осталось мало времени, ~30сек</h3>

                            <button type={"button"} className={"btn btn-green"} onClick={()=>this.nextProcess()}>
                                Продолжить работу сайта
                            </button>
                        </div>
                    </div>
                </div>
            </DefaultModal>
        );
    }
}


WalletShowInfoSessionTimout.defaultProps = {
    modalId: "show_info_session_timout"
};

export default class PrivateContactModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[ModalStore]}
                inject={{
                    modals: () => ModalStore.getState().modals,
                    resolve: () => ModalStore.getState().resolve
                }}
            >
                <WalletShowInfoSessionTimout {...this.props} />
            </AltContainer>
        );
    }
}

import React from "react";
import {connect} from "alt-react";
import AccountStore from "stores/AccountStore";
import WalletDb from "stores/WalletDb";
import AccountActions from "actions/AccountActions";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";
import TranslateWithLinks from "../Utility/TranslateWithLinks";
import PublicHeader from "../Layout/PublicHeader";
import Form from "../Public/Form";
import Field from "../Public/Field";
import {ymSend} from "../../lib/common/metrika";

class Login extends React.Component {
    constructor(props) {
        super(props);

        this.data = {
            accountName: "",
            password: ""
        };
    }

    componentDidUpdate() {
        const myAccounts = AccountStore.getMyAccounts();

        // use ChildCount to make sure user is on /create-account page except /create-account/*
        // to prevent redirect when user just registered and need to make backup of wallet or password
        const childCount = React.Children.count(this.props.children);

        // do redirect to portfolio if user already logged in
        if (
            this.props.router &&
            Array.isArray(myAccounts) &&
            myAccounts.length !== 0 &&
            childCount === 0
        )
            this.props.router.push("/account/" + this.props.currentAccount);
    }

    handleSubmit = e => {
        e.preventDefault();

        const accountName = this.accountName();
        const password = this.password();

        this.validate(password, accountName);
    };

    accountName = () => {
        return this.data.accountName.value;
    };

    password = () => {
        return this.data.password.value;
    };

    validate = (password, account) => {
        //const { resolve } = this.props
        console.log(WalletDb);
        const {cloudMode} = WalletDb.validatePassword(
            password || "",
            true, //unlock
            account
        );

        if (WalletDb.isLocked()) {
            //this.setState({ passwordError: true })
        } else {
            this.data.accountName.value = "";
            this.data.password.value = "";

            AccountActions.setPasswordAccount(account);
            //"ENTER";

            ymSend("reachGoal", "ENTER");

        }
    };

    render() {
        const translator = require("counterpart");

        return (
            <div className="public-block">
                <PublicHeader />

                <div className="public-container top text-center">
                    {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?
                    <Translate
                        content={"public.welcome_to_deex_gbl"}
                        component="h2"
                    />:
                    <Translate
                        content={__SCROOGE_CHAIN__ ? "public.welcome_to_deex_scrooge" : "public.welcome_to_haqq"}
                        component="h2"
                    />}

                    <p className="public-gray-p">
                        {translator.translate("public.please_login")}
                        &nbsp;
                        <Link
                            className="public-link"
                            to="/create-account/password"
                        >
                            <Translate content="public.create_a_new_account" />
                        </Link>
                    </p>
                </div>

                <div className="public-container short">
                    <form onSubmit={this.handleSubmit} noValidate>
                        <Form>
                            <Field
                                inputRef={this.data.accountData}
                                placeholder="Account name"
                            />

                            <Field
                                inputRef={this.data.password}
                                type="password"
                                placeholder="Password"
                            />
                        </Form>

                        <div className="public-buttons">
                            <button
                                type="submit"
                                className="button public-button"
                            >
                                <Translate content="public.login" />
                            </button>

                            <Link
                                to="/create-account/wallet"
                                className="button margin-top public-button public-default"
                            >
                                <Translate content="public.login_using_local_wallet" />
                            </Link>
                        </div>

                        <p className="public-gray-p-small">
                            <TranslateWithLinks
                                string="public.formatter"
                                keys={[
                                    {
                                        type: "link",
                                        value: "/wallet/backup/restore",
                                        translation: "public.restore_link",
                                        arg: "restore_link"
                                    },
                                    {
                                        type: "link",
                                        value: "/create-account/wallet",
                                        translation: "public.restore_form",
                                        arg: "restore_form"
                                    }
                                ]}
                            />
                        </p>
                    </form>
                </div>
            </div>
        );
    }
}

export default connect(Login, {
    listenTo() {
        return [AccountStore];
    },
    getProps() {
        return {
            currentAccount:
                AccountStore.getState().currentAccount ||
                AccountStore.getState().passwordAccount
        };
    }
});

import React from "react";
import PropTypes from "prop-types";
import {connect} from "alt-react";
import classNames from "classnames";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import WalletDb from "stores/WalletDb";
import notify from "actions/NotificationActions";
import {Link} from "react-router-dom";
import AccountSelect from "../Forms/AccountSelect";
import TransactionConfirmStore from "stores/TransactionConfirmStore";
import LoadingIndicator from "../LoadingIndicator";
import Translate from "react-translate-component";
import counterpart from "counterpart";
import {ChainStore, FetchChain, key} from "deexjs";
import ReactTooltip from "react-tooltip";
import utils from "common/utils";
import SettingsActions from "actions/SettingsActions";
import WalletUnlockActions from "actions/WalletUnlockActions";
import Icon from "../Icon/Icon";
import PublicHeader from "../Layout/PublicHeader";
import TranslateWithLinks from "../Utility/TranslateWithLinks";
import Form from "./Form";
import Field from "./Field";
import AccountNameRegister from "./AccountNameRegister";

class RegisterPassword extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor() {
        super();
        this.state = {
            validAccountName: false,
            accountName: "",
            validPassword: false,
            registrar_account: null,
            loading: false,
            hide_refcode: true,
            show_identicon: false,
            step: 1,
            secondStep: false,
            finalStep: false,
            showPass: false,
            generatedPassword: ("P" + key.get_random_key().toWif()).substr(
                0,
                45
            ),
            confirm_password: "",
            understand_1: false,
            understand_2: false,
            understand_3: false,
            tempAccountName: null
        };
        this.onFinishConfirm = this.onFinishConfirm.bind(this);

        this.accountNameInput = null;
    }

    UNSAFE_componentWillMount() {
        if (!WalletDb.getWallet()) {
            SettingsActions.changeSetting({
                setting: "passwordLogin",
                value: "login"
            });
        }
    }

    componentDidMount() {
        ReactTooltip.rebuild();
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !utils.are_equal_shallow(nextState, this.state);
    }

    isValid() {
        let firstAccount = AccountStore.getMyAccounts().length === 0;
        let valid = this.state.validAccountName;
        if (!WalletDb.getWallet()) {
            valid = valid && this.state.validPassword;
        }
        if (!firstAccount) {
            valid = valid && this.state.registrar_account;
        }
        return valid && this.state.understand_1 && this.state.understand_2;
    }

    onAccountNameChange(e) {
        const state = {};
        if (e.valid !== undefined) state.validAccountName = e.valid;
        if (e.value !== undefined) state.accountName = e.value;
        if (!this.state.show_identicon) state.show_identicon = true;
        this.setState(state);
    }

    onFinishConfirm(confirm_store_state) {
        if (
            confirm_store_state.included &&
            confirm_store_state.broadcasted_transaction
        ) {
            TransactionConfirmStore.unlisten(this.onFinishConfirm);
            TransactionConfirmStore.reset();

            FetchChain("getAccount", this.state.accountName, undefined, {
                [this.state.accountName]: true
            }).then(() => {
                this.props.router.push("/wallet/backup/create?newAccount=true");
            });
        }
    }

    _unlockAccount(name, password) {
        SettingsActions.changeSetting({
            setting: "passwordLogin",
            value: "login"
        });

        WalletDb.validatePassword(password, true, name);
        WalletUnlockActions.checkLock.defer();
    }

    createAccount(name, password) {
        this.setState({
            step: 2
        });

        /* let refcode = this.refs.refcode ? this.refs.refcode.value() : null;
        let referralAccount = AccountStore.getState().referralAccount;
        this.setState({loading: true});

        AccountActions.createAccountWithPassword(
            name,
            password,
            this.state.registrar_account,
            referralAccount || this.state.registrar_account,
            0,
            refcode
        )
            .then(() => {
                AccountActions.setPasswordAccount(name);
                // User registering his own account
                if (this.state.registrar_account) {
                    FetchChain("getAccount", name, undefined, {
                        [name]: true
                    }).then(() => {
                        this.setState({
                            step: 2,
                            loading: false
                        });
                        this._unlockAccount(name, password);
                    });
                    TransactionConfirmStore.listen(this.onFinishConfirm);
                } else {
                    // Account registered by the faucet
                    FetchChain("getAccount", name, undefined, {
                        [name]: true
                    }).then(() => {
                        this.setState({
                            step: 2
                        });
                        this._unlockAccount(name, password);
                    });
                }
            })
            .catch(error => {
                console.log("ERROR AccountActions.createAccount", error);
                let error_msg =
                    error.base && error.base.length && error.base.length > 0
                        ? error.base[0]
                        : "unknown error";
                if (error.remote_ip) error_msg = error.remote_ip[0];
                notify.addNotification({
                    message: `Failed to create account: ${name} - ${error_msg}`,
                    level: "error",
                    autoDismiss: 10
                });
                this.setState({loading: false});
            }); */
    }

    onSubmit(e) {
        e.preventDefault();
        if (!this.isValid()) return;
        let account_name = this.accountNameInput.getValue();
        // if (WalletDb.getWallet()) {
        //     this.createAccount(account_name);
        // } else {
        this.setState({tempAccountName: account_name});
        let password = this.state.generatedPassword;
        this.createAccount(account_name, password);
    }

    handleCreateAccount = () => {
        let refcode = this.refs.refcode ? this.refs.refcode.value() : null;
        let referralAccount = AccountStore.getState().referralAccount;
        this.setState({loading: true});

        const name = this.state.tempAccountName;
        const password = this.state.generatedPassword;

        AccountActions.createAccountWithPassword(
            name,
            password,
            this.state.registrar_account,
            referralAccount || this.state.registrar_account,
            0,
            refcode
        )
            .then(() => {
                AccountActions.setPasswordAccount(name);
                // User registering his own account
                if (this.state.registrar_account) {
                    FetchChain("getAccount", name, undefined, {
                        [name]: true
                    }).then(() => {
                        this.setState({
                            step: 2,
                            loading: false
                        });
                        //this._unlockAccount(name, password);
                        this.props.router.push("/");
                    });
                    TransactionConfirmStore.listen(this.onFinishConfirm);
                } else {
                    // Account registered by the faucet
                    FetchChain("getAccount", name, undefined, {
                        [name]: true
                    }).then(() => {
                        this.setState({
                            step: 2
                        });
                        //this._unlockAccount(name, password);
                        this.props.router.push("/");
                    });
                }
            })
            .catch(error => {
                console.log("ERROR AccountActions.createAccount", error);
                let error_msg =
                    error.base && error.base.length && error.base.length > 0
                        ? error.base[0]
                        : "unknown error";
                if (error.remote_ip) error_msg = error.remote_ip[0];
                notify.addNotification({
                    message: `Failed to create account: ${name} - ${error_msg}`,
                    level: "error",
                    autoDismiss: 10
                });
                this.setState({loading: false});
            });
    };

    onRegistrarAccountChange(registrar_account) {
        this.setState({registrar_account});
    }

    isFirstStepValid() {
        let firstAccount = AccountStore.getMyAccounts().length === 0;
        let valid = this.state.validAccountName;
        if (!WalletDb.getWallet()) {
            valid = valid && this.state.validPassword;
        }
        if (!firstAccount) {
            valid = valid && this.state.registrar_account;
        }
        return valid;
    }

    handleNextStep = e => {
        if (this.isFirstStepValid()) {
            this.setState({
                secondStep: true
            });
        }
    };

    isFirstDisabled = () => {
        return !this.isFirstStepValid();
    };

    isSecondDisabled = () => {
        return !this.isValid();
    };

    // showRefcodeInput(e) {
    //     e.preventDefault();
    //     this.setState({hide_refcode: false});
    // }

    _onInput(value, e) {
        this.setState({
            [value]:
                value === "confirm_password"
                    ? e.target.value
                    : !this.state[value],
            validPassword:
                value === "confirm_password"
                    ? e.target.value === this.state.generatedPassword
                    : this.state.validPassword
        });
    }

    _renderAccountCreateForm() {
        let {registrar_account} = this.state;

        let my_accounts = AccountStore.getMyAccounts();
        let firstAccount = my_accounts.length === 0;
        let valid = this.isValid();
        let isLTM = false;
        let registrar = registrar_account
            ? ChainStore.getAccount(registrar_account)
            : null;
        if (registrar) {
            if (registrar.get("lifetime_referrer") == registrar.get("id")) {
                isLTM = true;
            }
        }

        let buttonClass = classNames("submit-button button no-margin", {
            disabled: !valid || (registrar_account && !isLTM)
        });

        const translator = require("counterpart");

        return (
            <div className="public-block">
                <PublicHeader />

                {!this.state.secondStep ? (
                    <div className="public-container top text-center">
                        <Translate
                            content="public.create_new_account"
                            component="h2"
                        />

                        <p className="public-gray-p">
                            <TranslateWithLinks
                                string="public.sign_up_for_free"
                                keys={[
                                    {
                                        type: "link",
                                        value: "/login",
                                        translation: "public.log_in",
                                        arg: "login"
                                    }
                                ]}
                            />
                        </p>
                    </div>
                ) : (
                    <div className="public-container top text-center">
                        <Translate
                            content="public.policy_confirmation"
                            component="h2"
                        />

                        <Translate
                            content="public.please_read_carefully"
                            component="p"
                            className="public-gray-p"
                        />
                    </div>
                )}

                <div className="public-container short">
                    <form onSubmit={this.onSubmit.bind(this)} noValidate>
                        {!this.state.secondStep ? (
                            <Form>
                                <AccountNameRegister
                                    label="account.name"
                                    ref={ref => {
                                        if (ref) {
                                            this.accountNameInput =
                                                ref.refs.nameInput;
                                        }
                                    }}
                                    cheapNameOnly={!!firstAccount}
                                    onChange={this.onAccountNameChange.bind(
                                        this
                                    )}
                                    accountShouldNotExist={true}
                                    placeholder={counterpart.translate(
                                        "wallet.account_public"
                                    )}
                                    onAccountChanged={() => {}}
                                    size={60}
                                    hideImage
                                    useHR
                                    labelClass="login-label"
                                    reserveErrorSpace
                                    tip={counterpart.translate(
                                        "public.account_name_tip"
                                    )}
                                />

                                <Field
                                    placeholder={counterpart.translate(
                                        "wallet.generated"
                                    )}
                                    readonly={true}
                                    value={this.state.generatedPassword}
                                    copy={true}
                                    copyTip="tooltip.copy_password"
                                    tip={counterpart.translate(
                                        "public.password_min"
                                    )}
                                    tipLine={true}
                                />

                                <Field
                                    placeholder={counterpart.translate(
                                        "public.repeat_password"
                                    )}
                                    type="password"
                                    name="password"
                                    id="password"
                                    value={this.state.confirm_password}
                                    onChange={this._onInput.bind(
                                        this,
                                        "confirm_password"
                                    )}
                                    error={
                                        this.state.confirm_password &&
                                        this.state.confirm_password !==
                                            this.state.generatedPassword
                                            ? counterpart.translate(
                                                  "wallet.confirm_error"
                                              )
                                            : null
                                    }
                                />
                            </Form>
                        ) : (
                            <Form className="only-content">
                                <label
                                    className={"public-checkbox"}
                                    htmlFor="checkbox-1"
                                >
                                    <input
                                        type="checkbox"
                                        id="checkbox-1"
                                        onChange={this._onInput.bind(
                                            this,
                                            "understand_3"
                                        )}
                                        checked={this.state.understand_3}
                                    />

                                    <Icon name="checkmark" />

                                    <Translate
                                        content="wallet.understand_3"
                                        className="first-checkbox"
                                    />
                                </label>

                                <label
                                    className={"public-checkbox"}
                                    htmlFor="checkbox-2"
                                >
                                    <input
                                        type="checkbox"
                                        id="checkbox-2"
                                        onChange={this._onInput.bind(
                                            this,
                                            "understand_1"
                                        )}
                                        checked={this.state.understand_1}
                                    />

                                    <Icon name="checkmark" />

                                    <Translate
                                        content="wallet.understand_1"
                                        className="second-checkbox"
                                    />
                                </label>

                                <label
                                    className={"public-checkbox"}
                                    htmlFor="checkbox-3"
                                >
                                    <input
                                        type="checkbox"
                                        id="checkbox-3"
                                        onChange={this._onInput.bind(
                                            this,
                                            "understand_2"
                                        )}
                                        checked={this.state.understand_2}
                                    />

                                    <Icon name="checkmark" />

                                    <Translate
                                        content="wallet.understand_2"
                                        className="third-checkbox"
                                    />
                                </label>
                            </Form>
                        )}

                        {/* If this is not the first account, show dropdown for fee payment account */}
                        {firstAccount
                            ? null
                            : {
                                  /* <div
                                className="full-width-content form-group no-overflow"
                                style={{paddingTop: 30}}
                            >
                                <label>
                                    <Translate content="account.pay_from" />
                                </label>
                                <AccountSelect
                                    account_names={my_accounts}
                                    onChange={this.onRegistrarAccountChange.bind(
                                        this
                                    )}
                                />
                                {registrar_account && !isLTM ? (
                                    <div
                                        style={{textAlign: "left"}}
                                        className="facolor-error"
                                    >
                                        <Translate content="wallet.must_be_ltm" />
                                    </div>
                                ) : null}
                            </div> */
                              }}

                        {/* Submit button */}

                        {this.state.loading ? (
                            <div className="public-buttons text-center">
                                <LoadingIndicator type="three-bounce" />
                            </div>
                        ) : (
                            <div className="public-buttons">
                                {!this.state.secondStep ? (
                                    <button
                                        type="button"
                                        className="button public-button"
                                        onClick={this.handleNextStep}
                                        disabled={this.isFirstDisabled()}
                                    >
                                        <Translate content="public.final_step" />
                                    </button>
                                ) : (
                                    <button
                                        type="submit"
                                        className="button public-button"
                                        disabled={this.isSecondDisabled()}
                                    >
                                        <Translate content="account.create_account" />
                                    </button>
                                )}
                            </div>
                        )}

                        <p className="public-gray-p-small">
                            <TranslateWithLinks
                                string="public.restore_or_create"
                                keys={[
                                    {
                                        type: "link",
                                        value: "/wallet/backup/restore",
                                        translation: "public.restore",
                                        arg: "restore"
                                    },
                                    {
                                        type: "link",
                                        value: "/create-account/wallet",
                                        translation:
                                            "public.create_local_wallet",
                                        arg: "create_local_wallet"
                                    }
                                ]}
                            />
                        </p>

                        {/* Backup restore option */}
                        {/* <div style={{paddingTop: 40}}>
                    <label>
                        <Link to="/existing-account">
                            <Translate content="wallet.restore" />
                        </Link>
                    </label>

                    <label>
                        <Link to="/create-wallet-brainkey">
                            <Translate content="settings.backup_brainkey" />
                        </Link>
                    </label>
                </div> */}

                        {/* Skip to step 3 */}
                        {/* {(!hasWallet || firstAccount ) ? null :<div style={{paddingTop: 20}}>
                    <label>
                        <a onClick={() => {this.setState({step: 3});}}><Translate content="wallet.go_get_started" /></a>
                    </label>
                </div>} */}
                    </form>
                    {/* <br />
                <p>
                    <Translate content="wallet.bts_rules" unsafe />
                </p> */}
                </div>
            </div>
        );
    }

    _renderAccountCreateText() {
        let my_accounts = AccountStore.getMyAccounts();
        let firstAccount = my_accounts.length === 0;

        return (
            <div>
                <h4
                    style={{
                        fontWeight: "normal",
                        fontFamily: "Roboto-Medium, arial, sans-serif",
                        fontStyle: "normal",
                        paddingBottom: 15
                    }}
                >
                    <Translate content="wallet.wallet_password" />
                </h4>

                <Translate
                    style={{textAlign: "left"}}
                    unsafe
                    component="p"
                    content="wallet.create_account_password_text"
                />

                <Translate
                    style={{textAlign: "left"}}
                    component="p"
                    content="wallet.create_account_text"
                />

                {firstAccount ? null : (
                    <Translate
                        style={{textAlign: "left"}}
                        component="p"
                        content="wallet.not_first_account"
                    />
                )}
            </div>
        );
    }

    handleFinalStep = e => {
        this.setState({
            finalStep: true
        });
    };

    _renderBackup() {
        return (
            <div className="public-block">
                <PublicHeader />

                <div className="public-container top text-center">
                    {__GBL_CHAIN__ || __GBLTN_CHAIN__ ?
                    <Translate
                        content={"public.welcome_to_deex_gbl"}
                        component="h2"
                    />:
                    <Translate
                        content={__SCROOGE_CHAIN__ ? "public.welcome_to_deex_scrooge" : "public.welcome_to_haqq"}
                        component="h2"
                    />}

                    <Translate
                        content="public.understand_important"
                        component="p"
                        className="public-gray-p"
                    />

                    <Translate
                        content="public.continuing_to_use"
                        component="p"
                        className="public-gray-p"
                    />
                </div>

                <div className="public-container short">
                    <Form>
                        {!this.state.showPass ? (
                            <div
                                onClick={() => {
                                    this.setState({showPass: true});
                                }}
                                className="public-show-password"
                            >
                                <Translate content="public.show_password" />
                            </div>
                        ) : (
                            <div className="public-show-password">
                                {this.state.generatedPassword}
                            </div>
                        )}
                    </Form>

                    <div className="public-buttons">
                        <button
                            type="button"
                            onClick={this.handleFinalStep}
                            className="button public-button public-small"
                        >
                            <Translate content="public.backedup_password" />
                        </button>
                    </div>
                </div>

                {/* <p className="txtlabel warning">
                    <Translate unsafe content="wallet.password_lose_warning" />
                </p>

                <div
                    style={{width: "100%"}}
                    onClick={() => {
                        this.props.history.push("/");
                    }}
                    className="button"
                >
                    <Translate content="wallet.ok_done" />
                </div> */}
            </div>
        );
    }

    _renderFinalStep() {
        return (
            <div className="public-block">
                <PublicHeader />

                <div className="public-container top text-center">
                    <Translate content="public.phishers" component="h2" />

                    <Translate
                        content="public.phishers_desc"
                        component="p"
                        className="public-gray-p"
                    />
                </div>

                <div className="public-articles">
                    <div className="large-6 small-12 text-center">
                        <Icon name="link-new" />

                        <Translate
                            content="public.phishers_1"
                            component="p"
                            className="public-gray-p no-margin"
                        />
                        <a href="https://alpha.deex.exchange" target="_blank">
                            https://alpha.deex.exchange
                        </a>
                    </div>

                    <div className="large-6 small-12 text-center">
                        <Icon name="extension" />

                        <Translate
                            content="public.phishers_2"
                            component="p"
                            className="public-gray-p no-margin"
                        />
                    </div>

                    <div className="large-6 small-12 text-center">
                        <Icon name="mail" />

                        <Translate
                            content="public.phishers_3"
                            component="p"
                            className="public-gray-p no-margin"
                        />
                    </div>

                    <div className="large-6 small-12 text-center">
                        <Icon name="browser" />

                        <Translate
                            content="public.phishers_4"
                            component="p"
                            className="public-gray-p no-margin"
                        />
                    </div>

                    <div className="large-6 small-12 text-center">
                        <Icon name="soft" />

                        <Translate
                            content="public.phishers_5"
                            component="p"
                            className="public-gray-p no-margin"
                        />
                    </div>

                    <div className="large-6 small-12 text-center">
                        <Icon name="wifi" />

                        <Translate
                            content="public.phishers_6"
                            component="p"
                            className="public-gray-p no-margin"
                        />
                    </div>
                </div>

                <div className="public-container short final-step">
                    <button
                        type="button"
                        onClick={this.handleCreateAccount}
                        className="button public-button"
                    >
                        <Translate content="public.continue_to_my_account" />
                    </button>
                </div>

                {/* <p className="txtlabel warning">
                    <Translate unsafe content="wallet.password_lose_warning" />
                </p>

                <div
                    style={{width: "100%"}}
                    onClick={() => {
                        this.props.history.push("/");
                    }}
                    className="button"
                >
                    <Translate content="wallet.ok_done" />
                </div> */}
            </div>
        );
    }

    _renderGetStarted() {
        return (
            <div>
                <table className="table">
                    <tbody>
                        <tr>
                            <td>
                                <Translate content="wallet.tips_dashboard" />:
                            </td>
                            <td>
                                <Link to="/">
                                    <Translate content="header.dashboard" />
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_account" />:
                            </td>
                            <td>
                                <Link
                                    to={`/account/${
                                        this.state.accountName
                                    }/overview`}
                                >
                                    <Translate content="wallet.link_account" />
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_deposit" />:
                            </td>
                            <td>
                                <Link to="/deposit-withdraw">
                                    <Translate content="wallet.link_deposit" />
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_transfer" />:
                            </td>
                            <td>
                                <Link to="/transfer">
                                    <Translate content="wallet.link_transfer" />
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_settings" />:
                            </td>
                            <td>
                                <Link to="/settings">
                                    <Translate content="header.settings" />
                                </Link>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }

    _renderGetStartedText() {
        return (
            <div>
                <p
                    style={{
                        fontWeight: "normal",
                        fontFamily: "Roboto-Medium, arial, sans-serif",
                        fontStyle: "normal"
                    }}
                >
                    <Translate content="wallet.congrat" />
                </p>

                <p>
                    <Translate content="wallet.tips_explore_pass" />
                </p>

                <p>
                    <Translate content="wallet.tips_header" />
                </p>

                <p className="txtlabel warning">
                    <Translate content="wallet.tips_login" />
                </p>
            </div>
        );
    }

    render() {
        let {step} = this.state;
        // let my_accounts = AccountStore.getMyAccounts();
        // let firstAccount = my_accounts.length === 0;
        return (
            <div className="public-block">
                {/* step === 2 ? (
                    <p
                        style={{
                            fontWeight: "normal",
                            fontFamily: "Roboto-Medium, arial, sans-serif",
                            fontStyle: "normal"
                        }}
                    >
                        <Translate content={"wallet.step_" + step} />
                    </p>
                ) : null */}

                {/* step === 3 ? this._renderGetStartedText() : null */}

                {step === 1
                    ? this._renderAccountCreateForm()
                    : step === 2
                        ? this.state.finalStep
                            ? this._renderFinalStep()
                            : this._renderBackup()
                        : null}
            </div>
        );
    }
}

export default connect(
    RegisterPassword,
    {
        listenTo() {
            return [AccountStore];
        },
        getProps() {
            return {};
        }
    }
);

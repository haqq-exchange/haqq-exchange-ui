import React, {PropTypes, Component} from "react";
import {Link} from "react-router-dom";
import {FormattedDate} from "react-intl";
import {connect} from "alt-react";
import WalletActions from "actions/WalletActions";
import WalletManagerStore from "stores/WalletManagerStore";
import BackupStore from "stores/BackupStore";
import WalletDb from "stores/WalletDb";
import BackupActions, {
    backup,
    decryptWalletBackup
} from "actions/BackupActions";
import notify from "actions/NotificationActions";
import {saveAs} from "file-saver";
import cname from "classnames";
import Translate from "react-translate-component";
import {PrivateKey} from "deexjs";
import SettingsActions from "actions/SettingsActions";
import {backupName} from "common/backupUtils";
import ActionSheet from "react-foundation-apps/src/action-sheet";
import IntlActions from "actions/IntlActions";
import SettingsStore from "stores/SettingsStore";
import FlagDropdown from "components/Layout/Header/FlagDropdown";

const FlagImage = ({flag, width = 30, height = "auto"}) => {
    return (
        <img
            height={height}
            width={width}
            src={require(`${__BASE_URL__}assets/language-dropdown/img/${flag.toUpperCase()}.png`)}
        />
    );
};

const connectObject = {
    listenTo() {
        return [WalletManagerStore, BackupStore];
    },
    getProps() {
        let wallet = WalletManagerStore.getState();
        let backup = BackupStore.getState();
        return {wallet, backup};
    }
};

//The default component is WalletManager.jsx
class BackupCreate extends Component {
    render() {
        return (
            <div style={{maxWidth: "40rem"}}>
                <Create
                    noText={this.props.noText}
                    newAccount={
                        this.props.location
                            ? this.props.location.query.newAccount
                            : null
                    }
                >
                    <NameSizeModified />
                    {this.props.noText ? null : <Sha1 />}
                    <Download downloadCb={this.props.downloadCb} />
                </Create>
            </div>
        );
    }
}
BackupCreate = connect(
    BackupCreate,
    connectObject
);

// layout is a small project
// class WalletObjectInspector extends Component {
//     static propTypes={ walletObject: PropTypes.object }
//     render() {
//         return <div style={{overflowY:'auto'}}>
//             <Inspector
//                 data={ this.props.walletObject || {} }
//                 search={false}/>
//         </div>
//     }
// }

class RestoreBackup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newWalletName: null,
            locales: SettingsStore.getState().defaults.locale,
            currentLocale: SettingsStore.getState().settings.get("locale")
        };
    }

    isRestored() {
        let new_wallet = this.props.wallet.new_wallet;
        let has_new_wallet = this.props.wallet.wallet_names.has(new_wallet);
        return has_new_wallet;
    }

    UNSAFE_componentWillMount() {
        BackupActions.reset();
    }

    reset() {
        BackupActions.reset();
        this.forceUpdate();
    }

    render() {
        // // // let new_wallet = this.props.wallet.new_wallet;
        // // let has_new_wallet = this.props.wallet.wallet_names.has(new_wallet);
        // let restored = has_new_wallet;

        const flagDropdown = (
            <ActionSheet>
                <ActionSheet.Button title="" style={{width: "30px"}}>
                    {/* <a
                        style={{padding: "1rem", border: "none"}}
                        className="button arrow-down"
                    > */}
                    <FlagImage flag={this.state.currentLocale} />
                    {/* </a> */}
                </ActionSheet.Button>
                <ActionSheet.Content>
                    <ul className="no-first-element-top-border">
                        {this.state.locales.map(locale => {
                            return (
                                <li key={locale}>
                                    <a
                                        href={"#"}
                                        onClick={e => {
                                            e.preventDefault();
                                            IntlActions.switchLocale(locale);
                                            this.setState({
                                                currentLocale: locale
                                            });
                                        }}
                                    >
                                        <div className="table-cell">
                                            <FlagImage
                                                width="20"
                                                height="20"
                                                flag={locale}
                                            />
                                        </div>
                                        <div
                                            className="table-cell"
                                            style={{paddingLeft: 10}}
                                        >
                                            <Translate
                                                content={"languages." + locale}
                                            />
                                        </div>
                                    </a>
                                </li>
                            );
                        })}
                    </ul>
                </ActionSheet.Content>
            </ActionSheet>
        );

        return (
            <div className="restore-backup">
                <header className="restore-backup__header">
                    <div
                        className="restore-backup__logo"
                        dangerouslySetInnerHTML={{
                            __html: require("assets/logo-ico.svg")
                        }}
                    />

                    <div className="restore-backup__lang">
                        <FlagDropdown />
                    </div>
                </header>
                <Translate
                    component="h1"
                    className="restore-backup__title"
                    content="wallet.restore_backup"
                />
                <Translate
                    style={{textAlign: "left", maxWidth: "30rem"}}
                    className="restore-backup__text"
                    component="p"
                    content="wallet.import_backup_choose"
                />
                <div className="restore-backup__container">
                    {new FileReader().readAsBinaryString ? null : (
                        <p className="error">
                            Warning! You browser doesn't support some some file
                            operations required to restore backup, we recommend
                            you to use Chrome or Firefox browsers to restore
                            your backup.
                        </p>
                    )}

                    {!this.isRestored() && (
                        <Upload>
                            <NameSizeModified reset={this.reset.bind(this)} />
                        </Upload>
                    )}

                    <DecryptBackup saveWalletObject={true}>
                        <NewWalletName>
                            <Restore />
                        </NewWalletName>
                    </DecryptBackup>

                    {!this.isRestored() && (
                        <div className="restore-backup__link">
                            <Link to="/authorization/login">
                                <Translate content="wallet.restore-login" />
                            </Link>
                            <span> or </span>
                            <Link to="/create-account/password">
                                <Translate content="wallet.create-account" />
                            </Link>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

RestoreBackup = connect(
    RestoreBackup,
    connectObject
);

class Restore extends Component {
    constructor() {
        super();
        this.state = {};
    }

    isRestored() {
        let new_wallet = this.props.wallet.new_wallet;
        let has_new_wallet = this.props.wallet.wallet_names.has(new_wallet);
        return has_new_wallet;
    }

    render() {
        let new_wallet = this.props.wallet.new_wallet;
        let has_new_wallet = this.isRestored();

        if (has_new_wallet) {
            return (
                <span className="restore-backup__form">
                    <h5>
                        <Translate
                            content="wallet.restore_success"
                            name={new_wallet.toUpperCase()}
                        />
                    </h5>
                    <Link to="/">
                        <div className="button outline restore-backup__form-button">
                            <Translate
                                component="span"
                                content="header.dashboard"
                            />
                        </div>
                    </Link>
                    <div>{this.props.children}</div>
                </span>
            );
        }

        return (
            <span className="restore-backup__form">
                <div
                    className="button outline restore-backup__form-button"
                    onClick={this.onRestore.bind(this)}
                >
                    <Translate
                        content="wallet.restore_wallet_of"
                        name={new_wallet}
                    />
                </div>
            </span>
        );
    }

    onRestore() {
        WalletActions.restore(
            this.props.wallet.new_wallet,
            this.props.backup.wallet_object
        );
        SettingsActions.changeSetting({
            setting: "passwordLogin",
            value: "LoginWallet"
        });
    }
}
Restore = connect(
    Restore,
    connectObject
);

class NewWalletName extends Component {
    constructor(props) {
        super(props);
        this.state = {
            new_wallet: null,
            accept: false || this.isRestored()
        };
    }

    isRestored() {
        let new_wallet = this.props.wallet.new_wallet;
        let has_new_wallet = this.props.wallet.wallet_names.has(new_wallet);
        return has_new_wallet;
    }

    UNSAFE_componentWillMount() {
        let has_current_wallet = !!this.props.wallet.current_wallet;
        if (!has_current_wallet) {
            let walletName = "default";
            if (this.props.backup.name) {
                walletName = this.props.backup.name.match(/[a-z0-9_-]*/)[0];
            }
            WalletManagerStore.setNewWallet(walletName);
            this.setState({accept: true});
        }
        if (
            has_current_wallet &&
            this.props.backup.name &&
            !this.state.new_wallet
        ) {
            // begning of the file name might make a good wallet name
            let new_wallet = this.props.backup.name
                .toLowerCase()
                .match(/[a-z0-9_-]*/)[0];
            if (new_wallet) this.setState({new_wallet});
        }
    }

    render() {
        if (this.state.accept) return <span>{this.props.children}</span>;

        let has_wallet_name = !!this.state.new_wallet;
        let has_wallet_name_conflict = has_wallet_name
            ? this.props.wallet.wallet_names.has(this.state.new_wallet)
            : false;
        let name_ready = !has_wallet_name_conflict && has_wallet_name;

        return (
            <form
                className="restore-backup__form"
                onSubmit={this.onAccept.bind(this)}
            >
                <h5>
                    <Translate content="wallet.new_wallet_name" />
                </h5>
                <input
                    className="restore-backup__form-input"
                    type="text"
                    id="new_wallet"
                    onChange={this.formChange.bind(this)}
                    value={this.state.new_wallet}
                />
                <p className="restore-backup__description">
                    {has_wallet_name_conflict ? (
                        <Translate content="wallet.wallet_exist" />
                    ) : null}
                </p>
                <div
                    onClick={this.onAccept.bind(this)}
                    type="submit"
                    className={cname(
                        "button outline restore-backup__form-button",
                        {disabled: !name_ready}
                    )}
                >
                    <Translate content="wallet.accept" />
                </div>
            </form>
        );
    }

    onAccept(e) {
        if (e) e.preventDefault();
        this.setState({accept: true});
        WalletManagerStore.setNewWallet(this.state.new_wallet);
    }

    formChange(event) {
        let key_id = event.target.id;
        let value = event.target.value;
        if (key_id === "new_wallet") {
            //case in-sensitive
            value = value.toLowerCase();
            // Allow only valid file name characters
            if (/[^a-z0-9_-]/.test(value)) return;
        }
        let state = {};
        state[key_id] = value;
        this.setState(state);
    }
}
NewWalletName = connect(
    NewWalletName,
    connectObject
);

class Download extends Component {
    UNSAFE_componentWillMount() {
        try {
            this.isFileSaverSupported = !!new Blob();
        } catch (e) {}
    }

    componentDidMount() {
        if (!this.isFileSaverSupported)
            notify.error("File saving is not supported");
    }

    render() {
        return (
            <div className="button" onClick={this.onDownload.bind(this)}>
                <Translate content="wallet.download" />
            </div>
        );
    }

    onDownload() {
        let blob = new Blob([this.props.backup.contents], {
            type: "application/octet-stream; charset=us-ascii"
        });

        if (blob.size !== this.props.backup.size)
            throw new Error("Invalid backup to download conversion");
        saveAs(blob, this.props.backup.name);
        WalletActions.setBackupDate();

        if (this.props.downloadCb) {
            this.props.downloadCb();
        }
    }
}
Download = connect(
    Download,
    connectObject
);

class Create extends Component {
    getBackupName() {
        return backupName(this.props.wallet.current_wallet);
    }

    render() {
        let has_backup = !!this.props.backup.contents;
        if (has_backup) return <div>{this.props.children}</div>;

        let ready = WalletDb.getWallet() != null;

        return (
            <div>
                {this.props.noText ? null : (
                    <div style={{textAlign: "left"}}>
                        {this.props.newAccount ? (
                            <Translate
                                component="p"
                                content="wallet.backup_new_account"
                            />
                        ) : null}
                        <Translate
                            component="p"
                            content="wallet.backup_explain"
                        />
                    </div>
                )}
                <div
                    onClick={this.onCreateBackup.bind(this)}
                    className={cname("button", {disabled: !ready})}
                    style={{marginBottom: 10}}
                >
                    <Translate
                        content="wallet.create_backup_of"
                        name={this.props.wallet.current_wallet}
                    />
                </div>
                <LastBackupDate />
            </div>
        );
    }

    onCreateBackup() {
        let backup_pubkey = WalletDb.getWallet().password_pubkey;
        backup(backup_pubkey).then(contents => {
            let name = this.getBackupName();
            BackupActions.incommingBuffer({name, contents});
        });
    }
}
Create = connect(
    Create,
    connectObject
);

class LastBackupDate extends Component {
    render() {
        if (!WalletDb.getWallet()) {
            return null;
        }
        let backup_date = WalletDb.getWallet().backup_date;
        let last_modified = WalletDb.getWallet().last_modified;
        let backup_time = backup_date ? (
            <h4>
                <Translate content="wallet.last_backup" />{" "}
                <FormattedDate value={backup_date} />
            </h4>
        ) : (
            <Translate
                style={{paddingTop: 20}}
                className="facolor-error"
                component="p"
                content="wallet.never_backed_up"
            />
        );
        let needs_backup = null;
        if (backup_date) {
            needs_backup =
                last_modified.getTime() > backup_date.getTime() ? (
                    <h4 className="facolor-error">
                        <Translate content="wallet.need_backup" />
                    </h4>
                ) : (
                    <h4 className="success">
                        <Translate content="wallet.noneed_backup" />
                    </h4>
                );
        }
        return (
            <span>
                {backup_time}
                {needs_backup}
            </span>
        );
    }
}

class Upload extends Component {
    reset() {
        // debugger;
        // this.refs.file_input.value = "";
        BackupActions.reset();
    }

    render() {
        let resetButton = (
            <div style={{paddingTop: 20}}>
                <div
                    onClick={this.reset.bind(this)}
                    className={cname("button outline", {
                        disabled: !this.props.backup.contents
                    })}
                >
                    <Translate content="wallet.reset" />
                </div>
            </div>
        );

        if (this.props.backup.contents && this.props.backup.public_key)
            return this.props.children;

        let is_invalid =
            this.props.backup.contents && !this.props.backup.public_key;

        return (
            <div className="backup-file-input">
                <div className="backup-file-label">Backup file</div>
                <div className="uplodad-file-input">
                    <input
                        ref="file_input"
                        accept=".bin"
                        type="file"
                        id="backup_input_file"
                        style={{border: "solid"}}
                        onChange={this.onFileUpload.bind(this)}
                    />
                    <span className="backup-input-text">No file selected</span>
                    <label
                        className="backup-input-label"
                        htmlFor="backup_input_file"
                    >
                        Browse
                    </label>

                    {is_invalid ? (
                        <h5>
                            <Translate content="wallet.invalid_format" />
                        </h5>
                    ) : null}
                </div>
            </div>
        );
    }

    onFileUpload(evt) {
        let file = evt.target.files[0];
        BackupActions.incommingWebFile(file);
        this.forceUpdate();
    }
}
Upload = connect(
    Upload,
    connectObject
);

class NameSizeModified extends Component {
    render() {
        return (
            <div className="backup-file-input">
                <div className="backup-file-label">Backup file</div>
                <div className="backup-uploaded-file-info">
                    <div
                        className="file-icon"
                        dangerouslySetInnerHTML={{
                            __html: require("assets/file.svg")
                        }}
                    />
                    <div className="backup-uploaded-file">
                        <div className="backup-uploaded-file-name">
                            {this.props.backup.name}
                        </div>
                        <div className="backup-uploaded-file-size">{`(${
                            this.props.backup.size
                        } bytes)`}</div>
                    </div>
                    <div
                        onClick={this.props.reset}
                        className="backup-reset-button"
                        dangerouslySetInnerHTML={{
                            __html: require("assets/cancel.svg")
                        }}
                    />
                </div>
            </div>
        );
    }
}
NameSizeModified = connect(
    NameSizeModified,
    connectObject
);

class DecryptBackup extends Component {
    static propTypes = {
        saveWalletObject: PropTypes.bool
    };

    constructor(props) {
        super(props);
        this.state = this._getInitialState(this.isRestored());
    }

    reset() {
        this.setState(this._getInitialState());
    }

    _getInitialState(restored) {
        return {
            backup_password: "",
            verified: false || restored
        };
    }

    isRestored() {
        let new_wallet = this.props.wallet.new_wallet;
        let has_new_wallet = this.props.wallet.wallet_names.has(new_wallet);
        return has_new_wallet;
    }

    render() {
        if (
            this.state.verified &&
            (this.props.backup.wallet_object || this.isRestored())
        )
            return <span>{this.props.children}</span>;
        return (
            <form
                className="restore-backup__form"
                onSubmit={this.onPassword.bind(this)}
            >
                <label
                    className="restore-backup__form-label"
                    htmlFor="backup_password"
                >
                    <Translate content="wallet.enter_password" />
                </label>
                <input
                    type="password"
                    className="restore-backup__form-input"
                    id="backup_password"
                    onChange={this.formChange.bind(this)}
                    value={this.state.backup_password}
                />

                <div
                    type="submit"
                    className="button outline restore-backup__form-button"
                    onClick={this.onPassword.bind(this)}
                >
                    <Translate content="wallet.restore-submit" />
                </div>
            </form>
        );
    }

    onPassword(e) {
        if (e) e.preventDefault();
        let private_key = PrivateKey.fromSeed(this.state.backup_password || "");
        let contents = this.props.backup.contents;
        decryptWalletBackup(private_key.toWif(), contents)
            .then(wallet_object => {
                this.setState({verified: true});
                if (this.props.saveWalletObject)
                    BackupStore.setWalletObjct(wallet_object);
            })
            .catch(error => {
                console.error(
                    "Error verifying wallet " + this.props.backup.name,
                    error,
                    error.stack
                );
                if (error === "invalid_decryption_key")
                    notify.error("Invalid Password");
                else notify.error("" + error);
            });
    }

    formChange(event) {
        let state = {};
        state[event.target.id] = event.target.value;
        this.setState(state);
    }
}
DecryptBackup = connect(
    DecryptBackup,
    connectObject
);

class Sha1 extends Component {
    render() {
        return (
            <div>
                <pre className="no-overflow">
                    {this.props.backup.sha1} * SHA1
                </pre>
                <br />
            </div>
        );
    }
}
Sha1 = connect(
    Sha1,
    connectObject
);

export {
    BackupCreate,
    RestoreBackup,
    Restore,
    NewWalletName,
    Download,
    Create,
    Upload,
    NameSizeModified,
    DecryptBackup,
    Sha1
};

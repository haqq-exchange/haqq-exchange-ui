import React from "react";
import CopyButton from "../Utility/CopyButton";

class Field extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: props.value || "",
            focus: false
        };
    }

    handleChange = e => {
        this.setState({
            value: e.target.value
        });

        if (this.props.onChange) {
            this.props.onChange(e);
        }
    };

    handleKeyDown = e => {
        if (this.props.onKeyDown) {
            this.props.onKeyDown(e);
        }

        if (this.props.onEnter && e.keyCode === 13) {
            this.props.onEnter(e);
        }
    };

    handleFocus = e => {
        this.setState({
            focus: true
        });
    };

    handleBlur = e => {
        this.setState({
            focus: false
        });
    };

    render() {
        const {
            placeholder = "",
            name = null,
            id = null,
            type = "text",
            error = null,
            inputRef = null,
            tabIndex = "",
            copy = false,
            copyTip = null,
            readonly = null,
            tip = null,
            tipLine = null,
            progress = null,
            score = 0
        } = this.props;

        return (
            <div
                className={
                    "public-field" +
                    (this.state.focus ? " focus" : "") +
                    (copy ? " copy" : "") +
                    (progress ? " withProggress" : "")
                }
            >
                <div className="public-field-placeholder">{placeholder}</div>
                <input
                    ref={inputRef}
                    name={name}
                    id={id}
                    className="public-input"
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    onChange={this.handleChange}
                    onKeyDown={this.handleKeyDown}
                    type={type}
                    tabIndex={tabIndex}
                    value={this.state.value}
                    readOnly={readonly}
                    placeholder={placeholder}
                    autoComplete={name === "password" ? "current-password" : name }
                />

                {progress ? (
                    <progress
                        style={{height: 10}}
                        className={
                            score === 5
                                ? "high"
                                : score === 4 ? "medium" : "low"
                        }
                        value={score}
                        max="5"
                        min="0"
                    />
                ) : null}

                {error ? (
                    <div>
                        <div className="public-error-icon">!</div>

                        <div className="public-error-text">{error}</div>
                    </div>
                ) : null}

                {copy ? (
                    <CopyButton
                        className="public-copy-icon"
                        text={this.state.value}
                        tip={copyTip}
                        dataPlace="top"
                    />
                ) : null}

                {tip ? (
                    <div
                        className={
                            "public-input-tip" + (tipLine ? " one-line" : "")
                        }
                    >
                        {tip}
                    </div>
                ) : null}
            </div>
        );
    }
}

export default Field;

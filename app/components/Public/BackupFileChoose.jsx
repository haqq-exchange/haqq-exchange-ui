import React from "react";
import cn from "classnames";
import Translate from "react-translate-component";
import Icon from "../Icon/Icon";

export const BackupFileChoose = ({name, size, onUseOtherWallet}) => (
    <div className="public-file-box">
        <Translate
            content="public.backup_file"
            component="p"
            className="public-file-label"
        />

        <div className={cn("public-file-choose", { hasNotBytes: !size })}>
            <Icon name="folder" className="public-file-icon" />
            <b>{name}</b>
            <br />
            {size ? <span>
                ({size} <Translate content="public.bytes" />)
            </span>: null}

            <a role="button" onClick={onUseOtherWallet}>
                &times;
            </a>
        </div>
    </div>
);

export default BackupFileChoose;

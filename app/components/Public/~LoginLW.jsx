import React from "react";
import PropTypes from "prop-types";
import ZfApi from "react-foundation-apps/src/utils/foundation-api";
import PasswordInput from "../Forms/PasswordInput";
import notify from "actions/NotificationActions";
import AltContainer from "alt-container";
import WalletDb from "stores/WalletDb";
import WalletUnlockStore from "stores/WalletUnlockStore";
import WalletManagerStore from "stores/WalletManagerStore";
import SettingsActions from "actions/SettingsActions";
import BackupStore from "stores/BackupStore";
import AccountStore from "stores/AccountStore";
import WalletUnlockActions from "actions/WalletUnlockActions";
import WalletActions from "actions/WalletActions";
import BackupActions, {restore, backup} from "actions/BackupActions";
import AccountActions from "actions/AccountActions";
import {Apis} from "deexjs-ws";
import utils from "common/utils";
import AccountName from "./AccountName";
import {PrivateKey} from "deexjs";
import {saveAs} from "file-saver";
import LoginTypeSelector from "../Wallet/LoginTypeSelector";
import counterpart from "counterpart";
import {
    WalletSelector,
    CreateLocalWalletLink,
    WalletDisplay,
    CustomPasswordInput,
    LoginButtons,
    BackupWarning,
    BackupFileSelector,
    DisableChromeAutocomplete,
    CustomError,
    KeyFileLabel
} from "../Wallet/WalletUnlockModalLib";
import {backupName} from "common/backupUtils";
import PublicHeader from "../Layout/PublicHeader";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";
import TranslateWithLinks from "../Utility/TranslateWithLinks";
import Form from "./Form";
import Field from "./Field";
import BackupFile from "./BackupFile";
import BackupFileChoose from "./BackupFileChoose";
import LoginHeader from "./Login/Header";

class LoginOld extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor(props) {
        super(props);
        this.state = this.initialState(props);
    }

    initialState = (props = this.props) => {
        const {passwordAccount, currentWallet} = props;
        return {
            passwordError: null,
            accountName: passwordAccount,
            walletSelected: !!currentWallet,
            customError: null,
            isOpen: false,
            restoringBackup: false,
            stopAskingForBackup: false
        };
    };

    UNSAFE_componentWillReceiveProps(np) {
        const {walletSelected, restoringBackup, accountName} = this.state;
        const {
            currentWallet: newCurrentWallet,
            passwordAccount: newPasswordAccount
        } = np;

        const newState = {};
        if (newPasswordAccount && !accountName)
            newState.accountName = newPasswordAccount;
        if (walletSelected && !restoringBackup && !newCurrentWallet)
            newState.walletSelected = false;
        if (this.props.passwordLogin !== np.passwordLogin) {
            newState.passwordError = false;
            newState.customError = null;
        }

        this.setState(newState);
    }

    shouldComponentUpdate(np, ns) {
        return (
            !utils.are_equal_shallow(np, this.props) ||
            !utils.are_equal_shallow(ns, this.state)
        );
    }

    componentDidMount() {
        const {modalId, passwordLogin} = this.props;

        SettingsActions.changeSetting({
            setting: "passwordLogin",
            value: "loginWallet"
        });

        ZfApi.subscribe(modalId, (name, msg) => {
            const {isOpen} = this.state;

            if (name !== modalId) return;
        });

        if (passwordLogin) {
            const {account_input} = this.refs;
            const {accountName} = this.state;

            console.log(
                "accountName && password_input", accountName);

            if (accountName && this.ref_password_input) {
                this.ref_password_input.focus();
            } else if (
                account_input &&
                typeof account_input.focus === "function"
            ) {
                account_input.focus();
            }
        }


    }

    componentDidUpdate() {
        const {resolve, modalId, isLocked} = this.props;

        if (resolve)
            if (isLocked) {
                ZfApi.publish(modalId, "open");
            } else {
                resolve();
            }
        else ZfApi.publish(this.props.modalId, "close");
    }

    validate = (password, account) => {
        const {passwordLogin, resolve} = this.props;
        const {stopAskingForBackup} = this.state;

        const {cloudMode} = WalletDb.validatePassword(
            password || "",
            true, //unlock
            account
        );

        if (WalletDb.isLocked()) {
            this.setState({passwordError: true});
        } else {
            const password_input = this.passwordInput();
            if (!passwordLogin) {
                //password_input.clear();
                password_input.value = "";
            } else {
                password_input.value = "";
                if (cloudMode) AccountActions.setPasswordAccount(account);
            }
            WalletUnlockActions.change();
            if (stopAskingForBackup) WalletActions.setBackupDate();
            else if (this.shouldUseBackupLogin()) this.backup();
            if (resolve) {
                resolve();
            }
            WalletUnlockActions.cancel();
        }
    };

    passwordInput = () =>
        this.ref_password_input.refs.ref_password_input ||
        this.ref_password_input;

    restoreBackup = (password, callback) => {
        const {backup} = this.props;
        const privateKey = PrivateKey.fromSeed(password || "");
        const walletName = backup.name.split(".")[0];
        restore(privateKey.toWif(), backup.contents, walletName)
            .then(() => {
                return WalletActions.setWallet(walletName)
                    .then(() => {
                        BackupActions.reset();
                        callback();
                    })
                    .catch(e => this.setState({customError: e.message}));
            })
            .catch(e => {
                const message = typeof e === "string" ? e : e.message;
                const invalidBackupPassword =
                    message === "invalid_decryption_key";
                this.setState({
                    customError: invalidBackupPassword ? null : message,
                    passwordError: invalidBackupPassword
                });
            });
    };

    handleLogin = e => {
        if (e) e.preventDefault();


        const {passwordLogin, backup} = this.props;
        const {walletSelected, accountName} = this.state;

        console.log('passwordLogin, backup', passwordLogin, backup)
        console.log('{walletSelected, accountName}', walletSelected, accountName);

        if (!passwordLogin && !walletSelected) {
            this.setState({
                customError: counterpart.translate(
                    "wallet.ask_to_select_wallet"
                )
            });
        } else {
            this.setState({passwordError: null}, () => {
                const password_input = this.passwordInput();
                const password = password_input.value;

                if (!passwordLogin && backup.name) {
                    this.restoreBackup(password, () => this.validate(password));
                } else {
                    const account = passwordLogin ? accountName : null;
                    this.validate(password, account);
                }
            });
        }
    };

    closeRedirect = path => {
        WalletUnlockActions.cancel();
        this.props.history.push(path);
    };

    handleCreateWallet = () => this.closeRedirect("/create-account/wallet");

    handleRestoreOther = () => this.closeRedirect("/settings/restore");

    loadBackup = e => {
        const fullPath = e.target.value;
        const file = e.target.files[0];

        this.setState({restoringBackup: true}, () => {
            const startIndex =
                fullPath.indexOf("\\") >= 0
                    ? fullPath.lastIndexOf("\\")
                    : fullPath.lastIndexOf("/");
            let filename = fullPath.substring(startIndex);
            if (filename.indexOf("\\") === 0 || filename.indexOf("/") === 0) {
                filename = filename.substring(1);
            }
            BackupActions.incommingWebFile(file);
            this.setState({
                walletSelected: true
            });
        });
    };

    handleSelectedWalletChange = e => {
        const {value} = e.target;
        const selectionType = value.split(".")[0];
        const walletName = value.substring(value.indexOf(".") + 1);

        BackupActions.reset();
        if (selectionType === "upload")
            this.setState({
                restoringBackup: true,
                customError: null
            });
        else
            WalletActions.setWallet(walletName).then(() =>
                this.setState({
                    walletSelected: true,
                    customError: null,
                    restoringBackup: false
                })
            );
    };

    backup = () =>
        backup(this.props.dbWallet.password_pubkey).then(contents => {
            const {currentWallet} = this.props;
            const name = backupName(currentWallet);
            BackupActions.incommingBuffer({name, contents});

            const {backup} = this.props;
            let blob = new Blob([backup.contents], {
                type: "application/octet-stream; charset=us-ascii"
            });
            if (blob.size !== backup.size)
                throw new Error("Invalid backup to download conversion");
            saveAs(blob, name);
            WalletActions.setBackupDate();
            BackupActions.reset();
        });

    handleAskForBackupChange = e =>
        this.setState({stopAskingForBackup: e.target.checked});

    handleUseOtherWallet = () => {
        this.setState({
            walletSelected: false,
            restoringBackup: false,
            passwordError: null,
            customError: null
        });
    };

    handleAccountNameChange = accountName =>
        this.setState({accountName, error: null});

    shouldShowBackupWarning = () =>
        !this.props.passwordLogin &&
        this.state.walletSelected &&
        !this.state.restoringBackup &&
        !(!!this.props.dbWallet && !!this.props.dbWallet.backup_date);

    shouldUseBackupLogin = () =>
        this.shouldShowBackupWarning() && !this.state.stopAskingForBackup;

    render() {
        let {
            myActiveAccounts,
            myHiddenAccounts,
            passwordAccount,
            currentAccount
        } = AccountStore.getState();


        if (currentAccount || passwordAccount) {
            //this.props.history.push("/");
        }

        const {
            backup,
            currentWallet,
            walletNames
        } = this.props;
        const {
            walletSelected,
            restoringBackup,
            passwordError,
            customError,
            stopAskingForBackup
        } = this.state;

        const noWalletNames = !(walletNames.size > 0);
        const walletDisplayName = backup.name || currentWallet;
        const walletDisplaySize = backup.size || 0;

        const errorMessage = passwordError
            ? counterpart.translate("wallet.pass_incorrect")
            : customError;

        return (
            // U N L O C K
            <div className="public-container short">
                <form onSubmit={this.handleLogin} className="full-width">
                    <div
                        className={
                            "key-file-selector " +
                                (restoringBackup && !walletSelected
                                    ? "restoring"
                                    : "")
                        } >
                        {walletSelected ? (
                            <BackupFileChoose
                                name={walletDisplayName}
                                size={walletDisplaySize}
                                onUseOtherWallet={
                                    this.handleUseOtherWallet
                                }
                            />
                        ) : (
                            restoringBackup || noWalletNames ?
                                (
                                    <BackupFile
                                        onFileChosen={
                                            this.loadBackup
                                        }
                                        onRestoreOther={
                                            this
                                                .handleRestoreOther
                                        }
                                    />
                                ) : (
                                    <WalletSelector
                                        onFileChosen={
                                            this.loadBackup
                                        }
                                        restoringBackup={
                                            restoringBackup
                                        }
                                        walletNames={
                                            walletNames
                                        }
                                        onWalletChange={
                                            this
                                                .handleSelectedWalletChange
                                        }
                                    />
                                )
                        )}
                    </div>

                    <Form>
                        <Field
                            type="password"
                            ref={(ref)=>this.ref_password_input=ref}
                            inputRef="ref_password_input"
                            name="password"
                            id="password"
                            error={errorMessage}
                            placeholder={counterpart.translate(
                                "settings.password"
                            )}
                        />
                    </Form>
                    {this.shouldShowBackupWarning() && (
                        <BackupWarning
                            onChange={this.handleAskForBackupChange}
                            checked={stopAskingForBackup}
                        />
                    )}
                    <div className="public-buttons">
                        <button
                            type="button"
                            onClick={this.handleLogin}
                            className="btn btn-green"
                        >
                            <Translate
                                content={
                                    this.shouldUseBackupLogin()
                                        ? "wallet.backup_login"
                                        : "header.unlock_short"
                                }
                            />
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}

class LoginOldContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[
                    WalletUnlockStore,
                    AccountStore,
                    WalletManagerStore,
                    WalletDb,
                    BackupStore
                ]}
                inject={{
                    currentWallet: () =>
                        WalletManagerStore.getState().current_wallet,
                    walletNames: () =>
                        WalletManagerStore.getState().wallet_names,
                    dbWallet: () => WalletDb.getWallet(),
                    isLocked: () => WalletDb.isLocked(),
                    backup: () => BackupStore.getState(),
                    resolve: () => WalletUnlockStore.getState().resolve,
                    reject: () => WalletUnlockStore.getState().reject,
                    locked: () => WalletUnlockStore.getState().locked,
                    passwordLogin: () =>
                        WalletUnlockStore.getState().passwordLogin,
                    passwordAccount: () =>
                        AccountStore.getState().passwordAccount || ""
                }}
            >
                <LoginOld {...this.props} />
            </AltContainer>
        );
    }
}
export default LoginOldContainer;

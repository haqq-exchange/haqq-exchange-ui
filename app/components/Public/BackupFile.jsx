import React from "react";
import Translate from "react-translate-component";

export class StyledUpload extends React.Component {
    render() {
        return (
            <div className="public-file-box">
                <Translate
                    content="public.backup_file"
                    component="p"
                    className="public-file-label"
                />

                <a role="button" className="public-file">
                    <Translate
                        content="public.no_file_selected"
                        component="span"
                        className="public-file-empty"
                    />

                    <Translate
                        content="public.browse"
                        component="span"
                        className="public-file-button"
                    />

                    <input
                        type="file"
                        onClick={e => e.stopPropagation()}
                        onChange={this.props.onFileChosen}
                        ref="input"
                        accept=".bin"
                    />
                </a>
            </div>
        );
    }
}

const BackupFile = ({onFileChosen, onRestoreOther}) => (
    <div>
        <StyledUpload onFileChosen={onFileChosen} />
        {/* <div className="login-hint">
      <Translate content="wallet.different_file_type" />{" "}
      <a onClick={onRestoreOther}>
        <Translate content="wallet.restore_it_here" />
      </a>
    </div> */}
    </div>
);

export default BackupFile;

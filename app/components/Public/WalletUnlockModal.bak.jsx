import React from "react";
import PropTypes from "prop-types";
import BaseModal from "../Modal/BaseModal";
import ZfApi from "react-foundation-apps/src/utils/foundation-api";
import AltContainer from "alt-container";
import WalletDb from "stores/WalletDb";
import WalletUnlockStore from "stores/WalletUnlockStore";
import WalletManagerStore from "stores/WalletManagerStore";
import CachedPropertyStore from "stores/CachedPropertyStore";
import BackupStore from "stores/BackupStore";
import AccountStore from "stores/AccountStore";
import {diff} from "deep-object-diff";


import ls from "common/localStorage";
let accountStorage = new ls("__deexgraphene__");
import {Login2fa, LoginLW, LoginOldContainer} from "../../RoutesLink";

class WalletUnlockModal extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor(props) {
        super(props);
        this.state = {
            isLogin2fa: accountStorage.has("qrCode")
        };

    }

    componentDidMount() {

    }

    componentDidUpdate() {
        const {resolve, modalId, isLocked} = this.props;
        
        console.log("resolve, modalId, isLocked", resolve, modalId, isLocked)

        if (resolve)
            if (isLocked) {
                ZfApi.publish(modalId, "open");
            } else {
                resolve();
            }
        else ZfApi.publish(this.props.modalId, "close");
    }


    render() {
        const { modalId, walletNames } = this.props;
        const { isLogin2fa } = this.state;

        let showAuthComponent = <LoginOldContainer />;
        if( isLogin2fa ) {
            showAuthComponent = <Login2fa accountQrCode={accountStorage.get("qrCode")} />;
        }
        if( walletNames.size ) {
            showAuthComponent = <LoginLW />;
        }

        return (
            // U N L O C K
            <BaseModal
                id={modalId}
                ref={(ref)=>this.modal=ref}
                overlay={true}
                overlayClose={false}
                modalHeader="header.unlock_short"
                className="public-login-modal"
                noLoggo={true}
                noHeader={true}
            >
                {showAuthComponent}
            </BaseModal>
        );
    }
}

WalletUnlockModal.defaultProps = {
    modalId: "unlock_wallet_modal_public.bak"
};

class WalletUnlockModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[
                    WalletUnlockStore,
                    AccountStore,
                    WalletManagerStore,
                    WalletDb,
                    BackupStore
                ]}
                inject={{
                    currentWallet: () =>
                        WalletManagerStore.getState().current_wallet,
                    walletNames: () =>
                        WalletManagerStore.getState().wallet_names,
                    dbWallet: () => WalletDb.getWallet(),
                    isLocked: () => WalletDb.isLocked(),
                    backup: () => BackupStore.getState(),
                    resolve: () => WalletUnlockStore.getState().resolve,
                    reject: () => WalletUnlockStore.getState().reject,
                    locked: () => WalletUnlockStore.getState().locked,
                    passwordLogin: () =>
                        WalletUnlockStore.getState().passwordLogin,
                    passwordAccount: () =>
                        AccountStore.getState().passwordAccount || ""
                }}
            >
                <WalletUnlockModal {...this.props} />
            </AltContainer>
        );
    }
}
export default WalletUnlockModalContainer;

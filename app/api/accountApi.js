import {Apis} from "deexjs-ws";
import {FetchChain} from "deexjs";

class Api {
    lookupAccounts(startChar, limit) {
        return Apis.instance()
            .db_api()
            .exec("lookup_accounts", [startChar, limit]);
    }

    async getAccountsById(accountId) {
        return await FetchChain("getAccount", accountId);
    }

    getAccountCounts(){
        return Apis.instance()
            .db_api()
            .exec("get_account_count", []);
    }
}

export default new Api();

/*
*
0: ["Aliemre05", "1.2.232"]
1: ["Armagyl", "1.2.189"]
2: ["Aylinkahraman", "1.2.244"]
3: ["Bitcoin", "1.2.149"]
4: ["Bjorn", "1.2.195"]
*
*
* 0: ["Aliemre05", "1.2.232"]
1: ["Armagyl", "1.2.189"]
2: ["Aylinkahraman", "1.2.244"]
3: ["Bitcoin", "1.2.149"]
4: ["Bjorn", "1.2.195"]
5: ["Boss", "1.2.158"]
6: ["DEEXY", "1.2.188"]
7: ["DeFi", "1.2.154"]
8: ["DeFideex", "1.2.141"]
9: ["Deex78", "1.2.211"]
* */

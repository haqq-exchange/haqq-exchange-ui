import SettingsStore from "stores/SettingsStore";
import cookies from "cookies-js";
import counterpart from "counterpart";
import ls from "common/localStorage";
//import ls from "../lib/common/localStorageImpl";

let LS = new ls();

let siteAddresDeex = {
    site_c6930e40: { /* prod deex */
        host: "https://haqq.exchange",
        ieo: "https://ieo-api.deex.exchange/api/ieo",
        referral: __GBL_CHAIN__ ? "https://pp.gbledger.net" : (__GBLTN_CHAIN__ ? "https://pp-testnet.gbledger.net" : "https://pp.haqq.exchange"),
        nodes: "https://nodes.deex.exchange/nodes",
        api: __GBL_CHAIN__ ? "https://pp.gbledger.net" : (__GBLTN_CHAIN__ ? "https://gw-testnet.gbledger.net" : "https://pp.haqq.exchange"),
        coins: __GBL_CHAIN__ ? "https://gw.gbledger.net" : (__GBLTN_CHAIN__ ? "https://gw-testnet.gbledger.net" : (__SCROOGE_CHAIN__ ? "https://gw.scrooge.club" : "https://gw.haqq.exchange")),
        adminExchange: "https://dgw.presale-deex.exchange/exchange",
    },
    site_4018d784: { /* prod bts */
        host: "https://gw.haqq.exchange",
        ieo: "https://ieo-api.deex.exchange/api/ieo",
        referral: __GBL_CHAIN__ ? "https://pp.gbledger.net" : (__GBLTN_CHAIN__ ? "https://pp-testnet.gbledger.net" : "https://pp-pb.deex.exchange"),
        nodes: "https://nodes.deex.exchange/nodes",
        api: __GBL_CHAIN__ ? "https://pp.gbledger.net" : (__GBLTN_CHAIN__ ? "https://gw-testnet.gbledger.net" : __DEEX_CHAIN__ ? "https://pp-pb.deex.exchange" : "https://pp.haqq.exchange"),
        coins:__GBL_CHAIN__ ? "https://gw.gbledger.net" : (__GBLTN_CHAIN__ ? "https://gw-testnet.gbledger.net" : (__SCROOGE_CHAIN__ ? "https://gw.scrooge.club" : "https://deex-gw.deex.exchange")),
        adminExchange: __GBL_CHAIN__ ? "https://dgw.presale-deex.exchange/exchange" : "https://dgw.presale-deex.exchange/exchange",
    },
    dev_c6930e40: { /* dev haqq */
        host: "https://haqq.exchange",
        ieo: "https://ieo-api.deex.exchange/api/ieo",
        referral: __GBL_CHAIN__ ? "https://pp.gbledger.net" : (__GBLTN_CHAIN__ ? "https://pp-testnet.gbledger.net" : "https://pp.haqq.exchange"),
        nodes: "https://nodes.deex.exchange/nodes",
        api: __GBL_CHAIN__ ? "https://pp.gbledger.net" : (__GBLTN_CHAIN__ ? "https://gw-testnet.gbledger.net" : "https://pp.haqq.exchange"),
        coins: __GBL_CHAIN__ ? "https://gw.gbledger.net" : (__GBLTN_CHAIN__ ? "https://gw-testnet.gbledger.net" : (__SCROOGE_CHAIN__ ? "https://gw.scrooge.club" : "https://gw.haqq.exchange")),
        adminExchange: "https://dgw.presale-deex.exchange/exchange",
    },
    dev_4018d784: { /* dev bts */
        host: "https://sys-test.presale-haqq.exchange",
        // host: "https://gw.deex.exchange",
        // host: "https://dt.presale-deex.exchange",
        ieo: "https://ieo-api.presale-deex.exchange/api/ieo",
        referral: __GBL_CHAIN__ ? "https://pp.gbledger.net" : (__GBLTN_CHAIN__ ? "https://pp-testnet.gbledger.net" : "https://api-test.presale-deex.exchange"),
        nodes: "https://nodes-api-test.presale-deex.exchange/nodes",
        api: __GBL_CHAIN__ ? "https://pp.gbledger.net" : (__GBLTN_CHAIN__ ? "https://gw-testnet.gbledger.net" : __DEEX_CHAIN__ ? "https://pp-pb.deex.exchange" : "https://pp.haqq.exchange"),
        adminExchange:  __GBL_CHAIN__ ? "https://dgw.presale-deex.exchange/exchange" : "https://sftb.presale-deex.exchange",
    },
    dev_39f5e2ed: { /* test */
        host: "https://deex.exchange",
        ieo: "https://ieo-api.deex.exchange/api/ieo",
        referral: __GBL_CHAIN__ ? "https://pp.gbledger.net" : (__GBLTN_CHAIN__ ? "https://pp-testnet.gbledger.net" : "https://pp-pb.deex.exchange"),
        nodes: "https://api-test.presale-deex.exchange/nodes",
        api: __GBL_CHAIN__ ? "https://pp.gbledger.net" : __DEEX_CHAIN__ ? "https://deex.exchange" : "https://pp.haqq.exchange",
    },
    bitshares_39f5e2ed: { /* bitshares */
        host: "http://192.168.40.64",
        ieo: "https://ieo-api.presale-deex.exchange/api/ieo",
        referral: __GBL_CHAIN__ ? "https://pp.gbledger.net" : (__GBLTN_CHAIN__ ? "https://pp-testnet.gbledger.net" : "https://pp-pb.deex.exchange"),
        nodes: "https://nodes-api-test.presale-deex.exchange/nodes",
        api: __GBL_CHAIN__ ? "https://pp.gbledger.net" : (__GBLTN_CHAIN__ ? "https://pp-testnet.gbledger.net" : __DEEX_CHAIN__ ? "https://api-test.presale-deex.exchange" : "https://pp.haqq.exchange"),
    },
};

let siteAddresGbl = {
    site_c6930e40: { /* prod deex */
        host: "https://deex.exchange",
        ieo: "https://ieo-api.deex.exchange/api/ieo",
        referral: "https://pp.gbledger.net",
        nodes: "https://nodes.deex.exchange/nodes",
        api: "https://pp.gbledger.net",
        coins: "https://gw.gbledger.net",
        adminExchange: "https://dgw.presale-deex.exchange/exchange",
    },
    site_4018d784: { /* prod bts */
        host: "https://gw.haqq.exchange",
        ieo: "https://ieo-api.deex.exchange/api/ieo",
        referral: "https://pp.gbledger.net",
        nodes: "https://nodes.deex.exchange/nodes",
        api: "https://pp.gbledger.net",
        coins: "https://gw.gbledger.net",
        adminExchange: "https://dgw.presale-deex.exchange/exchange",
    },
    dev_c6930e40: { /* dev deex */
        host: "https://deex.exchange",
        ieo: "https://ieo-api.deex.exchange/api/ieo",
        referral: "https://pp.gbledger.net",
        nodes: "https://nodes.deex.exchange/nodes",
        api: "https://pp.gbledger.net",
        coins: "https://gw.gbledger.net",
        // coins: "https://sys-deex-test.presale-deex.exchange",
        adminExchange: "https://dgw.presale-deex.exchange/exchange",
    },
    dev_4018d784: { /* dev bts */
        host: "https://sys-test.presale-haqq.exchange",
        // host: "https://gw.deex.exchange",
        // host: "https://dt.presale-deex.exchange",
        ieo: "https://ieo-api.presale-deex.exchange/api/ieo",
        referral: "https://pp.gbledger.net",
        nodes: "https://nodes-api-test.presale-deex.exchange/nodes",
        api: "https://pp.gbledger.net",
        adminExchange: "https://sftb.presale-deex.exchange",
    },
    dev_39f5e2ed: { /* test */
        host: "https://deex.exchange",
        ieo: "https://ieo-api.deex.exchange/api/ieo",
        referral: "https://pp.gbledger.net",
        nodes: "https://api-test.presale-deex.exchange/nodes",
        api: "https://pp.gbledger.net",
    },
    bitshares_39f5e2ed: { /* bitshares */
        host: "http://192.168.40.64",
        ieo: "https://ieo-api.presale-deex.exchange/api/ieo",
        referral: "https://pp.gbledger.net",
        nodes: "https://nodes-api-test.presale-deex.exchange/nodes",
        api: "https://pp.gbledger.net",
    },
};

let siteAddres = __GBL_CHAIN__ ? siteAddresGbl : siteAddresDeex;
console.log("siteAddres", siteAddres)
console.log("siteAddresGbl", siteAddresGbl)
console.log("siteAddresDeex", siteAddresDeex)

/*
можно добавить так любой конфиг
и добавить куку
?config_4018d784=dev_local
localStorage.setItem("api_config", JSON.stringify({dev_local: {
        host: "https://deex.exchange",
        ieo: "https://ieo-api.deex.exchange/api/ieo",
        referral: "https://api-test.presale-deex.exchange",
        nodes: "https://api-test.presale-deex.exchange/nodes",
        api: "https://deex.exchange",
        adminExchange: "https://sftb.presale-deex.exchange",
    }}))
* */

export function getRequestAddress(path_address, pathname) {
    let localConfig = Object.assign(siteAddres, LS.get("api_config"));
    let cookiePrefix = cookies.get("config_c6930e40"); /* dev_4018d784, site_4018d784 , dev_39f5e2ed */
    let prefix = !__DEV__ ? "dev" : __TESTNET__ ? "test" : "site";
    let keyPrefix = SettingsStore._getChainKey(prefix);
    console.log(keyPrefix);
    let currentObject = localConfig?.[cookiePrefix] ||
                        localConfig?.[keyPrefix] ||
                        localConfig["site_c6930e40"];
    const {origin} = window.location; // testpoint.deex.exchange
    let location = currentObject?.[path_address] || origin;
    if( pathname ) {
        location = [location, pathname].join("");
    }
    return location;
}

function getAddress(name="coins", url, path_addres) {
    let localConfig = Object.assign(siteAddres, LS.get("api_config"));
    let cookiePrefix = cookies.get("config_c6930e40"); /* dev_4018d784, site_4018d784 , dev_39f5e2ed */
    cookiePrefix = cookiePrefix ? cookiePrefix : __DEV__ ? "dev_c6930e40" : "site_c6930e40";
    let currentObject = localConfig[cookiePrefix] || localConfig["site_c6930e40"];

    let urlAddress = [currentObject[name], url];
    if( path_addres ){
        urlAddress.push(path_addres);
    }

    return urlAddress.join("");
}

/*export const blockTradesAPIs = {
    BASE: "https://api.blocktrades.us/v2",
    COINS_LIST: "/coins",
    ACTIVE_WALLETS: "/active-wallets",
    TRADING_PAIRS: "/trading-pairs",
    DEPOSIT_LIMIT: "/deposit-limits",
    ESTIMATE_OUTPUT: "/estimate-output-amount",
    ESTIMATE_INPUT: "/estimate-input-amount"
};*/

export const openledgerAPIs = {
    BASE: "https://ol-api1.openledger.info/api/v0/ol/support",
    COINS_LIST: "/coins",
    ACTIVE_WALLETS: "/active-wallets",
    TRADING_PAIRS: "/trading-pairs",
    DEPOSIT_LIMIT: "/deposit-limits",
    ESTIMATE_OUTPUT: "/estimate-output-amount",
    ESTIMATE_INPUT: "/estimate-input-amount"
};

export const rudexAPIs = {
    BASE: "https://gateway.rudex.org/api/v0_1",
    COINS_LIST: "/coins",
    NEW_DEPOSIT_ADDRESS: "/new-deposit-address"
};

export const tdexAPIs = {
    // BASE: "http://127.0.0.1:3000",
    BASE: getAddress("coins",""),
    COINS_LIST: "/gateway/pay/get_coins",
    NEW_DEPOSIT_ADDRESS: "/gateway/pay/get_user_address"
};

export const cryptoBridgeAPIs = {
    BASE: "https://api.crypto-bridge.org/api/v1",
    COINS_LIST: "/coins",
    ACTIVE_WALLETS: "/wallets",
    MARKETS: "/markets",
    TRADING_PAIRS: "/trading-pairs"
};

export const widechainAPIs = {
    BASE: "https://gateway.winex.pro/api/v0/ol/support",
    COINS_LIST: "/coins",
    ACTIVE_WALLETS: "/active-wallets",
    NEW_DEPOSIT_ADDRESS: "/new-deposit-address",
    WITHDRAW_HISTORY: "/latelyWithdraw",
    TRADING_PAIRS: "/trading-pairs",
    DEPOSIT_HISTORY: "/latelyRecharge"
};

/*export const gdex2APIs = {
    BASE: "https://api.gdex.io/adjust",
    COINS_LIST: "/coins",
    ACTIVE_WALLETS: "/active-wallets",
    TRADING_PAIRS: "/trading-pairs"
};*/

// Legacy Deposit/Withdraw
/*export const gdexAPIs = {
    BASE: "https://api.gdex.io",
    ASSET_LIST: "/gateway/asset/assetList",
    ASSET_DETAIL: "/gateway/asset/assetDetail",
    GET_DEPOSIT_ADDRESS: "/gateway/address/getAddress",
    CHECK_WITHDRAY_ADDRESS: "/gateway/address/checkAddress",
    DEPOSIT_RECORD_LIST: "/gateway/deposit/recordList",
    DEPOSIT_RECORD_DETAIL: "/gateway/deposit/recordDetail",
    WITHDRAW_RECORD_LIST: "/gateway/withdraw/recordList",
    WITHDRAW_RECORD_DETAIL: "/gateway/withdraw/recordDetail",
    GET_USER_INFO: "/gateway/user/getUserInfo",
    USER_AGREEMENT: "/gateway/user/isAgree",
    WITHDRAW_RULE: "/gateway/withdraw/rule"
};*/

let NODE_LIST_DEEX,
    NODE_LIST_BTS,
    // WS_NODE = "wss://node1.deex.exchange/ws";
    WS_NODE = "wss://node2.private.deexnet.com/ws";
    // WS_NODE = "wss://node5p.deexnet.com/ws";

let NODE_LIST_SCROOGE = [
    {
        url: "wss://node1-scrooge.cryptoplat.io/ws",
        location: "node.testnet"
    },
    {
        url: "wss://node2-scrooge.cryptoplat.io/ws",
        location: "node.testnet"
    },{
        url: "wss://node3-scrooge.cryptoplat.io/ws",
        location: "node.testnet"
    }
];

let SCROOGE_URL = NODE_LIST_SCROOGE[0].url;

let NODE_LIST_GBL = [{
    url: "https://node1.gbledger.net/ws",
    location: "node.testnet"
}];

let GBL_URL = NODE_LIST_GBL[0].url;

let NODE_LIST_GBLTN = [{
        url: "wss://node1-testnet.gbledger.net/ws",
        location: "node.testnet"
    },
    {
        url: "wss://node2-testnet.gbledger.net/ws",
        location: "node.testnet"
    },{
        url: "wss://node3-testnet.gbledger.net/ws",
        location: "node.testnet"
}];

let GBLTN_URL = NODE_LIST_GBLTN[0].url;
    
if( __TESTNET__ ) {
    WS_NODE = __GBL_CHAIN__ ? GBL_URL : (__GBLTN_CHAIN__ ? GBLTN_URL :(__SCROOGE_CHAIN__ ? SCROOGE_URL : "wss://node.testnet.bitshares.eu"));
    NODE_LIST_DEEX = NODE_LIST_BTS = [
        {
            url: "wss://node.testnet.bitshares.eu",
            location: "node.testnet"
        }
    ];
} else {
    NODE_LIST_DEEX = [
        {url: "wss://node.haqq.exchange/ws", location: "node1_private"},
        {url: "wss://node1.deex.exchange/ws", location: "node2_private"}
        // {url: "wss://node1.private.deexnet.com/ws", location: "node1_private"},
        // {url: "wss://node2.private.deexnet.com/ws", location: "node2_private"}
    ];
    NODE_LIST_BTS = [
        {
            "url": "wss://node1.deex.exchange/ws",
            "location": "node1_exchange"
        },
        {
            "url": "wss://node2.deex.exchange/ws",
            "location": "node2_exchange"
        },
        {
            "url": "wss://node3.deex.exchange/ws",
            "location": "node3_exchange"
        }
    ];
}

export const settingsAPIs = {
    DEFAULT_WS_NODE: __GBL_CHAIN__ ? GBL_URL : (__GBLTN_CHAIN__ ? GBLTN_URL : (__SCROOGE_CHAIN__ ? SCROOGE_URL : WS_NODE)),
    WS_NODE_LIST: __GBL_CHAIN__ ? NODE_LIST_GBL : (__GBLTN_CHAIN__ ? NODE_LIST_GBLTN :(__SCROOGE_CHAIN__ ? NODE_LIST_SCROOGE : (__DEEX_CHAIN__ ? NODE_LIST_DEEX : NODE_LIST_BTS))),
    DEFAULT_FAUCET: __GBLTN_CHAIN__ ? "https://faucet-testnet.gbledger.net" : (__GBL_CHAIN__ ? "https://faucet.gbledger.net" : (__SCROOGE_CHAIN__ ? "https://faucet.scrooge.club" : "https://faucet.haqq.exchange/faucet")),
    TESTNET_FAUCET: __GBLTN_CHAIN__ ? "https://faucet-testnet.gbledger.net" : (__GBL_CHAIN__ ? "https://faucet.gbledger.net" : (__SCROOGE_CHAIN__ ? "https://faucet.scrooge.club" : "https://faucet.haqq.exchange/faucet"))
};

export async function async_fetch(url) {
    let response = await fetch(url);

    if( url.indexOf(".md") !== -1 ) {
        if (response.ok) return await response.text();
    } else {
        if (response.ok) return await response.json();
    }

    throw new Error(response.status);
}

export const getSettingApi = async () => {
    try {
        const url = __GBLTN_CHAIN__ ? "https://static.haqq.exchange/gbl/config/settings_api_gbltn.json" : (__GBL_CHAIN__ ? "https://static.haqq.exchange/gbl/config/settings_api.json" :
            (__SCROOGE_CHAIN__ ? "https://static.scrooge.club/config/settings_api.json" : 
            (__DEEX_CHAIN__ ? "https://static.haqq.exchange/config/settings_api_self_bch.json?2" :
             "https://static.haqq.exchange/config/settings_api.json?2")));
             
        // (__DEEX_CHAIN__ ? "https://static.haqq.exchange/config/settings_api_self_bch.json?9" :
        let response = async_fetch(url);
        // let response = async_fetch("/static/settings_api_self_bch.json");
        // let response = async_fetch("/config/settings_api.json?2");
        response.then(result => settingsAPIs.WS_NODE_LIST = result.WS_NODE_LIST);
        return response;
    } catch (err) {
        console.log(err);
    }
};

export const getLoadedLocales = async (localesName, path = "locales", staticPath = "https://static.haqq.exchange") => {
    try {
        return new Promise(resolve => {
            const settings = SettingsStore.getState().settings;
            let response = async_fetch(`${staticPath}/${path}/${localesName}`);
            response.then(pageData => {
                Object.keys(pageData).map(local=>{
                    counterpart.registerTranslations(local, pageData[local]);
                });
                return pageData;
            }).then(pageData=>{
                counterpart.setLocale(settings.get("locale"));
                resolve(pageData);
            });
        });
    } catch (err) {
        console.log(err);
    }
};

/*export const settingsAPIs = {
    DEFAULT_WS_NODE: "wss://node3.deex.exchange/ws",
    WS_NODE_LIST: [
        {
            url: "wss://node3.deex.exchange/ws",
            location: {translate: "settings.api_closest"}
        },
        {url: "wss://node3.deex.exchange/ws", location: "Three"},
        {url: "wss://node2.deex.exchange/ws", location: "Two"},
        {url: "wss://node1.deex.exchange/ws", location: "One"}
    ],
    DEFAULT_FAUCET: "https://faucet.deex.exchange",
    TESTNET_FAUCET: "https://faucet.deex.exchange",
    RPC_URL: "https://openledger.info/api/"
};*/

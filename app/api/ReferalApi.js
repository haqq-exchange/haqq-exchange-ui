import WalletUnlockActions from "actions/WalletUnlockActions";
import notify from "actions/NotificationActions";
import WalletDb from "stores/WalletDb";
import CryptoJS from "crypto-js";
import {
    Aes,
    ChainValidation,
    TransactionBuilder,
    TransactionHelper,
    FetchChain,
    ChainStore
} from "deexjs";
import {Apis} from "deexjs-ws";
import counterpart from "counterpart";
import {Signature} from "deexjs";

const ReferalApi = {
    /**
        @param propose_account (or null) pays the fee to create the proposal, also used as memo from
    */
    transfer({
        account_id,
        referal_code
    }) {
        let unlock_promise = WalletUnlockActions.unlock();

        console.log("getAccount", account_id);

        return Promise.all([
            FetchChain("getAccount", account_id),
            unlock_promise
        ])
            .then(res => {
                console.log("res", account_id);
                let [chain_account] = res;

                console.log("ReferalApi res", res);
                console.log("ReferalApi chain_account", chain_account);
                console.log("ReferalApi chain_account id", chain_account.get("id"));
                console.log("ReferalApi chain_account name", chain_account.get("name"));

                let memo_from_public;
                if (referal_code) {
                    memo_from_public = chain_account.getIn([
                        "options",
                        "memo_key"
                    ]);



                    // The 1s are base58 for all zeros (null)
                    if (/111111111111111111111/.test(memo_from_public)) {
                        memo_from_public = null;
                    }
                }

                console.log("ReferalApi memo_from_public", memo_from_public);

                let memo_from_privkey;
                if (referal_code) {
                    memo_from_privkey = WalletDb.getPrivateKey(
                        memo_from_public
                    );

                    if (!memo_from_privkey) {
                        notify.addNotification({
                            message: counterpart.translate(
                                "account.errors.memo_missing"
                            ),
                            level: "error",
                            autoDismiss: 10
                        });
                        throw new Error(
                            "Missing private memo key for sender: " +
                                chain_account
                        );
                    }
                }

                console.log("ReferalApi memo_from_privkey", memo_from_privkey);

                let memo_object;
                console.log("ReferalApi memo_object");
                if (referal_code && memo_from_public) {
                    console.log("ReferalApi memo_object1");
                    // debugger;
                    let nonce = TransactionHelper.unique_nonce_uint64();
                    let checksum = Aes.encrypt_with_checksum(
                        memo_from_privkey,
                        memo_from_public,
                        nonce,
                        referal_code
                    );

                    var sig = Signature.signBuffer([
                        referal_code,
                        chain_account.get("name"),
                        "get_ref_link"
                    ].join("="), memo_from_privkey );
                    //encript
                    var wordArray = CryptoJS.enc.Utf8.parse(referal_code);
                    var base64 = CryptoJS.enc.Base64.stringify(wordArray);
                    console.log('encrypted:', sig ,
                        sig.toBuffer() ,
                        sig.toBuffer().toString("base64"),
                        sig.toHex());

                    //decrypt
                    var parsedWordArray = CryptoJS.enc.Base64.parse(base64);
                    var parsedStr = parsedWordArray.toString(CryptoJS.enc.Utf8);
                    console.log("parsed:", parsedStr);

                    // TransactionHelper.unique_nonce_uint64()


                    console.log("ReferalApi memo_object2");
                    memo_object = {
                        account: chain_account.get("name"),
                        referal_code,
                        sign: sig.toHex()
                    };
                    console.log("ReferalApi memo_object3");
                }
                console.log("ReferalApi memo_object", memo_object);
                console.log("ReferalApi memo_object", JSON.stringify(memo_object));
                console.log(
                    "ReferalApi chain_account",
                    chain_account,
                    chain_account.get("id")
                );

            })
            .catch(() => {});
    }
};

export default ReferalApi;

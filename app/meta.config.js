module.exports = {
    // "title": "SCROOGE - dpos blockchain ecosystem. Blockchain powered, community governed",
    // "title": "DEEX - dpos blockchain ecosystem. Blockchain powered, community governed",
    "title": "HAQQ",
    // "title": "GBLedger - dpos blockchain ecosystem. Blockchain powered, community governed",
    "meta": {
        "description": "Decentralized blockchain cryptocurrency exchange, trade with Bitcoin and Ethereum, ice and ieo marketplace, BTC and ETH cryptocurrency trading, masternodes for passive income, quick withdrawal.",
        "viewport": "width=device-width",
        "format-detection": "telephone=no",
        "yandex-verification": "0660cc85e206fcb4",
        "google-site-verification": "LpRplhDnE4P9tetNXWA2or5hIfb3OFtvLtKkV2cmKeo",
        "Content-Security-Policy": {
            "http-equiv": "Content-Security-Policy",
            "content": [

                /*"style-src 'self' http://!* https://!* https://!*.deex.exchange 'unsafe-inline';",
                "script-src 'self' http://!* https://!* https://!*.deex.exchange 'unsafe-inline' 'unsafe-eval';",
                "connect-src 'self' *;",
                "media-src 'self' *;",
                "worker-src 'self' https://www.google-analytics.com blob:;",
                "img-src 'self' * data:;",
                "default-src 'self' *;",
                "worker-src 'self' https://!*.yandex.ru/ https://www.google-analytics.com blob:;",
                "img-src 'self' * data:;",*/
            ].join("")
        }
    }
};

/*
 default-src gap://ready file://* *;
 style-src 'self' http://* https://* 'unsafe-inline';
 script-src 'self' http://* https://* 'unsafe-inline' 'unsafe-eval'
 */

// default-src *;
// style-src 'self' http://* 'unsafe-inline';
// script-src 'self' http://* 'unsafe-inline' 'unsafe-eval'"

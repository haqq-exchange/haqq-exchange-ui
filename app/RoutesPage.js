import React from "react";
import Loadable from "react-loadable";
import {Apis} from "deexjs-ws";
//import loadable from "loadable-components";
import LoadingIndicator from "Components/LoadingIndicator";
import {Route, Switch} from "react-router-dom";
import LoginSelector from "Components/LoginSelector";
import Authorization from "Components/Account/Autorithation";
import {CreateWalletFromBrainkey} from "Components/Wallet/WalletCreate";
import MainMarkets from "./components/MarketPlace/MainMarkets";
import NFT from "./components/MarketPlace/NFT";
import Products from "./components/MarketPlace/Products";
import Product from "./components/MarketPlace/Product";
import cookies from "cookies-js";
import PropTypes from "prop-types";
import * as RoutesLink from "RoutesLink";

const CreateAccountIndex = Loadable({
    loader: () => import(/* webpackChunkName: "CreateAccountIndex" */ "Components/Account/CreateAccount/CreateAccountIndex"),
    loading: LoadingIndicator
});
const AutorithationAccountIndex = Loadable({
    loader: () => import(/* webpackChunkName: "CreateAccountIndex" */ "Components/Account/Autorithation/AutorithationAccountIndex"),
    loading: LoadingIndicator
});
const Page404 = Loadable({
    loader: () => import(/* webpackChunkName: "ExchangeContainer" */ "Components/Page404/Page404"),
    loading: LoadingIndicator
});
const StealthPage = Loadable({
    loader: () =>
        import(/* webpackChunkName: "ExchangeContainer" */ "./components/Stealth/StealthPage"),
    loading: LoadingIndicator
});

const Exchange = Loadable({
    loader: () => import(/* webpackChunkName: "ExchangeContainer" */ "./components/Exchange/ExchangeContainer"),
    loading: LoadingIndicator
});

const Explorer = Loadable({
    loader: () => import(/* webpackChunkName: "explorer" */ "./components/Explorer/Explorer"),
    loading: LoadingIndicator
});

const AccountPage = Loadable({
    loader: () => import(/* webpackChunkName: "account" */ "./components/Account/AccountPage"),
    loading: LoadingIndicator
});
const AccountAsset = Loadable({
    loader: () => import(/* webpackChunkName: "AccountAsset" */ "./components/AccountAsset/AccountAsset"),
    loading: LoadingIndicator
});

const Transfer = Loadable({
    loader: () => import(/* webpackChunkName: "transfer" */ "./components/Transfer/Transfer"),
    loading: LoadingIndicator
});

const AccountDepositWithdraw = Loadable({
    loader: () => import(/* webpackChunkName: "deposit-withdraw" */ "./components/Account/AccountDepositWithdraw"),
    loading: LoadingIndicator
});

const Settings = Loadable({
    loader: () => import(/* webpackChunkName: "settings" */ "./components/Settings/SettingsContainer"),
    loading: LoadingIndicator
});

const Help = Loadable({
    loader: () => import(/* webpackChunkName: "help" */ "./components/Help"),
    loading: LoadingIndicator
});

const Asset = Loadable({
    loader: () => import(/* webpackChunkName: "asset" */ "./components/Blockchain/Asset"),
    loading: LoadingIndicator
});

const Block = Loadable({
    loader: () => import(/* webpackChunkName: "block" */ "./components/Blockchain/BlockContainer"),
    loading: LoadingIndicator
});

const DashboardAccountsOnly = Loadable({
    loader: () => import(/* webpackChunkName: "dashboard-accounts" */ "./components/Dashboard/DashboardAccountsOnly"),
    loading: LoadingIndicator
});

const MainPage = Loadable({
    loader: () => import(/* webpackChunkName: "mainpage" */ "./components/MainPage/MainPage"),
    loading: LoadingIndicator
});

const DashboardPage = Loadable({
    loader: () => import(/* webpackChunkName: "dashboard" */ "./components/Dashboard/DashboardPage"),
    loading: LoadingIndicator
});


const WalletManager = Loadable({
    loader: () => import(/* webpackChunkName: "wallet" */ "./components/Wallet/WalletManager"),
    loading: LoadingIndicator
});

const ExistingAccount = Loadable({
    loader: () => import(/* webpackChunkName: "existing-account" */ "./components/Wallet/ExistingAccount"),
    loading: LoadingIndicator
});

const CreateWorker = Loadable({
    loader: () => import(/* webpackChunkName: "create-worker" */ "./components/Account/CreateWorker"),
    loading: LoadingIndicator
});
const Invoice = Loadable({
    loader: () => import(/* webpackChunkName: "Invoice" */ "./components/Transfer/Invoice"),
    loading: LoadingIndicator
});
const InvoiceCreate = Loadable({
    loader: () => import(/* webpackChunkName: "Invoice" */ "./components/Transfer/InvoiceCreate"),
    loading: LoadingIndicator
});

const LocalesEditor = Loadable({
    loader: () => import(/* webpackChunkName: "LocalesEditor" */ "./components/LocalesEditor/LocalesEditor"),
    loading: LoadingIndicator
});
const LocalesEditorPage = Loadable({
    loader: () => import(/* webpackChunkName: "LocalesEditorPage" */ "./components/LocalesEditor/LocalesEditorPage"),
    loading: LoadingIndicator
});

const NodeAccount = Loadable({
    loader: () => import(/* webpackChunkName: "NodeMain" */ "./components/NodeAccount/NodeMain"),
    loading: LoadingIndicator
});

const AccountNetworkFees = Loadable({
    loader: () => import(/* webpackChunkName: "AccountNetworkFees" */ "./components/Account/AccountOverview/AccountNetworkFees"),
    loading: LoadingIndicator
});

const AccountVoting = Loadable({
    loader: () => import(/* webpackChunkName: "AccountVoting" */ "./components/Account/AccountVotingPage"),
    loading: LoadingIndicator
});

const AccountRefs = Loadable({
    loader: () => import(/* webpackChunkName: "AccountRefs/AccountRefsIndex" */ "./components/Account/AccountRefs/AccountRefsIndex"),
    loading: LoadingIndicator
});

const IEO = Loadable({
    loader: () => import(/* webpackChunkName: "IEOPage" */ "./components/IEO/IEOPage"),
    loading: LoadingIndicator
});

const IframePage = Loadable({
    loader: () => import(/* webpackChunkName: "IframePage" */ "./components/Layout/IframePage"),
    loading: LoadingIndicator
});

const AdminExchange = Loadable({
    loader: () => import(/* webpackChunkName: "AdminExchange" */ "./components/AdminExchange/IndexPage"),
    loading: LoadingIndicator
});

/* Private */
const CheckCrypto = Loadable({
    loader: () => import(/* webpackChunkName: "CheckCrypto" */ "./components/PrivatePage/CheckCrypto"),
    loading: LoadingIndicator
});
const IconPage = Loadable({
    loader: () => import(/* webpackChunkName: "CheckCrypto" */ "./components/Icon/IconPage"),
    loading: LoadingIndicator
});



export default  class RoutesPage extends React.Component{
    constructor() {
        super();
        this.state = {
            nftAssets: []
        };
    }

    static contextTypes = {
        router: PropTypes.object
    };

    componentDidMount() {
        this._getNFTAssets();
    }

    getParseQueryVariable = (search) => {
        let query = search.substring(1);
        let vars = query.split("&");
        let variable = {};
        for (let i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            variable[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
        }
        return variable;
    };
    getQueryVariable = (search, variable) => {
        // debugger;
        var query = search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("&");
            return decodeURIComponent(pair[0]).indexOf(variable) === 0;
        }
    };

    // получаю ассеты с типом nft
    _getNFTAssets = () => {
        let assetsList = [];

        Apis.instance()
            .db_api()
            .exec("list_assets", [0, 100])
            .then(resultAssets => {
                resultAssets.forEach(asset => {
                    const description = asset.options.description;
                    if (description) {
                        let type = JSON.parse(description).type;
                        if (
                            type == "nft" &&
                            asset.symbol != "BOXING" &&
                            asset.symbol != "RABMUTAN"
                        ) {
                            assetsList.push(asset);
                        }
                    }
                });
            })
            .then(() => this.setState({nftAssets: assetsList}));
    };

    render(){
        // console.log("replace", this);
        /* Private */
        const isPrivatePage = cookies.get("private_page_PpNJCKV");
        let {router} = this.context;
        let {location} = router.history;
        let isSearchPrivate = false;
        let setConfigPage;
        if( location && location.search ) {
            isSearchPrivate = this.getQueryVariable(location.search, "private_page_PpNJCKV") /*=== "private_page_PpNJCKV"*/;
            setConfigPage = this.getParseQueryVariable(location.search) /*=== "private_page_PpNJCKV"*/;
            // console.log("isSearchPrivate", this.getQueryVariable(location.search, "private"));
            if( setConfigPage && setConfigPage.config_c6930e40 ) {
                cookies.set("config_c6930e40", setConfigPage.config_c6930e40);
            }
        }

        return (
            <Switch>
                <Route
                    path="/"
                    exact
                    component={MainPage}
                />
               
                {/* Ноды */}
                <Route
                    exact
                    path="/nodes"
                    component={NodeAccount}
                />
                <Route
                    exact
                    path="/ieo"
                    component={IEO}
                />
                <Route
                    exact
                    path="/ieo/:name"
                    component={IEO}
                />

                {/* Marketplace */}
                <Route
                    path="/marketplace/products"
                    exact
                    render={() => (
                        <Products
                            nftAssets={this.state.nftAssets}
                        />
                    )}
                />
                <Route
                    path="/marketplace/product/:data"
                    exact
                    render={() => (
                        <Product
                            nftAssets={this.state.nftAssets}
                            accountName={this.props.currentAccount}
                            {...this.props}
                        />
                    )}
                />
                <Route
                    path="/marketplace/nft"
                    exact
                    component={NFT}
                />
                <Route
                    path="/marketplace"
                    exact
                    render={() => (
                        <MainMarkets
                            nftAssets={this.state.nftAssets}
                            {...this.props}
                        />
                    )}
                />

                {__SCROOGE_CHAIN__ ? null :
                <Route
                    path="/market"
                    exact
                    component={DashboardPage}
                />}

                {/* Рефералка */}
                <Route
                    path="/account/referrals"
                    exact
                    component={AccountRefs}
                />
                <Route
                    path="/voting"
                    exact
                    component={AccountVoting}
                />
                <Route
                    path={`/account/referrals/:type`}
                    exact
                    render={() => (
                        <AccountRefs />
                    )}
                />

                <Route
                    path="/networkfees"
                    exact
                    component={AccountNetworkFees}
                />
                <Route
                    path="/networkfees/:account"
                    exact
                    component={AccountNetworkFees}
                />

                <Route
                    path="/account/:account_name/:account_type/:asset"
                    component={AccountPage}
                />
                <Route
                    path="/account/:account_name/:account_type"
                    component={AccountPage}
                />
                <Route
                    path="/account/:account_name"
                    component={AccountPage}
                />
                <Route
                    path="/accounts"
                    component={DashboardAccountsOnly}
                />
                <Route
                    path="/account-asset/:symbol"
                    component={AccountAsset}
                />

                {/* Stelth */}
                <Route
                    path="/stealth/account/:name"
                    component={StealthPage}
                />
                <Route
                    path="/stealth/contact/:name"
                    component={StealthPage}
                />
                <Route
                    path="/stealth"
                    component={StealthPage}
                />
                <Route
                    path="/market/:marketID"
                    component={Exchange}
                />
                <Route
                    path="/settings/:tab"
                    component={Settings}
                />
                <Route path="/settings" component={Settings}/>

                <Route
                    path="/transfer"
                    exact
                    component={Transfer}
                />
                <Route
                    path="/deposit-withdraw"
                    exact
                    component={AccountDepositWithdraw}
                />
                <Route path="/create-account" exact component={CreateAccountIndex}/>
                <Route path="/authorization" exact component={AutorithationAccountIndex}/>
                <Route path="/create-account/:type" exact component={LoginSelector}/>
                <Route
                    path="/authorization/:type"
                    component={Authorization}
                />

                {/*Explorer routes*/}
                <Route path="/explorer/:tab" component={Explorer}/>
                <Route path="/explorer" component={Explorer}/>

                <Route
                    path="/asset/:symbol"
                    component={Asset}
                />
                <Route
                    exact
                    path="/block/:height"
                    component={Block}
                />
                <Route
                    exact
                    path="/block/:height/:txIndex"
                    component={Block}
                />

                {/*Wallet backup/restore routes*/}
                <Route
                    path="/wallet"
                    component={WalletManager}
                />
                <Route
                    path="/create-wallet-brainkey"
                    component={CreateWalletFromBrainkey}
                />
                <Route
                    path="/existing-account"
                    component={ExistingAccount}
                />

                <Route
                    exact
                    path="/cran"
                    component={IframePage}
                />
                <Route
                    exact
                    path="/loto"
                    component={IframePage}
                />

                <Route path="/create-worker" component={CreateWorker}/>
                <Route path="/invoice/:data" component={Invoice} />
                <Route path="/invoice-create" component={InvoiceCreate} />

                <Route path="/admin" component={RoutesLink.AdminRouters} />
                <Route path="/admin/:name" component={RoutesLink.AdminRouters} />
                {/* Кредит */}
                <Route path="/credit" component={RoutesLink.CreditPage} />
                <Route path="/credit/*" component={RoutesLink.CreditPage} />
                {/*Help routes go zendesk */}
                <Route path='/help' component={() => {
                    window.open("https://deexhelp.zendesk.com", "_blank");
                    router.history.goBack();
                    return null;
                } }/>


                {isPrivatePage || isSearchPrivate ? (
                    <>
                        <Route
                            exact
                            path="/check-crypto"
                            component={CheckCrypto}
                        />
                        <Route
                            exact
                            path="/icon-page"
                            component={IconPage}
                        />
                    </>
                ) : null }



                {__DEV__ ? <Route
                    exact
                    path="/locales-editor"
                    component={LocalesEditor}
                /> : null }
                {__DEV__ ? <Route
                    exact
                    path="/locales-editor/:data"
                    component={LocalesEditorPage}
                /> : null }
                <Route path="*" component={Page404} status={404} />
            </Switch>
        );
    }
}

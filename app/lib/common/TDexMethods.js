import ls from "./localStorage";
import {tdexAPIs} from "api/apiConfig";

const tdexStorage = new ls("");

export function fetchCoinList(url = tdexAPIs.BASE + tdexAPIs.COINS_LIST) {
    return fetch(url, {method: "post"})
        .then(reply =>
            reply.json().then(result => {
                return result;
            })
        )
        .catch(err => {
            console.log("error fetching tdex list of coins", err, url);
        });
}

export function requestDepositAddress({url = tdexAPIs.BASE, stateCallback, signal, ...body}) {
    if(!body.inputCoinType) return;

    let body_string = JSON.stringify(body);
    return fetch(url + tdexAPIs.NEW_DEPOSIT_ADDRESS, {
        method: "post",
        signal,
        headers: new Headers({
            "Content-Type": "application/json"
        }),
        body: body_string
    })
        .then(reply => {
            return reply
                .json()
                .then(json => {
                    //console.log(31,json);
                    delete json.result;
                    let address = {
                        address: json.inputAddress || "unknown",
                        memo: json.inputMemo,
                        key: json.key || null,
                        error: json.error || null,
                        ...json
                    };
                    if (stateCallback)
                        stateCallback(address);
                    return address;
                })
                .catch(error => {
                    if (stateCallback)
                        stateCallback({address: "unknown", memo: null});
                    return Promise.reject({address: "unknown", memo: null});
                });
        })
        .catch(err => {
            console.log("fetch error:", err);
            return Promise.reject({address: "unknown", memo: null});
        });
}

export function validateAddress({url = tdexAPIs.BASE, symbol, walletType, newAddress}) {
    if (!newAddress) return new Promise(res => res());

    let header = new Headers({
        "Accept": "application/json",
        "Content-Type": "application/json"
    });

    console.log(url + "/gateway/pay/check-address/");

    return fetch(
        // url + "/wallets/" + walletType + "/check-address",
        url + "/gateway/pay/check-address/",
        {
            method: "post",
            headers: header,
            body: JSON.stringify({
                address: newAddress,
                outputCoinType: symbol,
                walletType,
            })
        }
    )
        .then(reply => reply.json().then(json => json.isValid))
        .catch(err => {
            console.log("validate error:", err);
        });
}

function hasWithdrawalAddress(wallet) {
    return tdexStorage.has(`history_address_${wallet}`);
}

function setWithdrawalAddresses({wallet, addresses}) {
    tdexStorage.set(`history_address_${wallet}`, addresses);
}

function getWithdrawalAddresses(wallet) {
    return tdexStorage.get(`history_address_${wallet}`, []);
}

function setLastWithdrawalAddress({wallet, address}) {
    tdexStorage.set(`history_address_last_${wallet}`, address);
}

function getLastWithdrawalAddress(wallet) {
    return tdexStorage.get(`history_address_last_${wallet}`, "");
}

export const WithdrawAddresses = {
    has: hasWithdrawalAddress,
    set: setWithdrawalAddresses,
    get: getWithdrawalAddresses,
    setLast: setLastWithdrawalAddress,
    getLast: getLastWithdrawalAddress
};

const serialize = require("serialize-javascript");

module.exports = {
    SerializeToQuery: function(obj) {
        var str = [];
        for (var p in obj)
            if (obj.hasOwnProperty(p)) {
                str.push(
                    encodeURIComponent(p) + "=" + encodeURIComponent(obj[p])
                );
            }
        return str.join("&");
    },
    send: function(endPoint, jsonObject) {
        return fetch(endPoint, {
            method: "post",
            headers: {
                // "Access-Control-Allow-Origin": "*",
                // "Content-Type": "application/x-www-form-urlencoded",
                // "Accept": "application/json",
            },
            body: serialize(jsonObject)
        });
    }
};

// Localstorage
import ls, {ls_key_exists} from "./localStorageImpl";

if (null === ls)
    throw "localStorage is required but isn't available on this platform";

const localStorage = key => {
    const STORAGE_KEY = key;

    const getStorageKey = localKey => STORAGE_KEY ? STORAGE_KEY + localKey : localKey;

    return {
        get(key, dv = {}) {
            let rv;
            try {
                if (ls_key_exists(getStorageKey(key), ls)) {
                    rv = JSON.parse(ls.getItem(getStorageKey(key)));
                }
                return rv ? rv : dv;
            } catch (err) {
                return dv;
            }
        },

        set(key, object) {
            if (object && object.toJS) {
                object = object.toJS();
            }
            ls.setItem(getStorageKey(key), JSON.stringify(object));
        },

        remove(key) {
            ls.removeItem(getStorageKey(key));
        },

        has(key) {
            return ls_key_exists(getStorageKey(key), ls);
        }
    };
};

export default localStorage;

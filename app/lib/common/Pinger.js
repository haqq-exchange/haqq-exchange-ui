import {Apis, Manager} from "deexjs-ws";
import {inRange} from "lodash";

export default class Pinger {

    constructor(dataManager) {
        this.connectionManager = new Manager(dataManager);
        this._beSatisfiedWith = {
            instant: 200,
            low: 400,
            medium: 800
        };
    }

    addNodes = ( nodes ) => {
        return new Promise(resolve => {
            this._nodeURLs = nodes;
            this._current = 0;
            resolve();
        });
    };

    start = (callback) => {
        this._range = 1;
        this.pingNodes(callback);
    };

    pingNodes = (callback) => {
        console.log("pingNodes", this , this._nodeURLs[this._current]);
        let continueToPing = this._continueToPing();
        console.log("continueToPing", continueToPing );
        if( continueToPing ) {
            this.connectionManager.url = this._nodeURLs[this._current];
            this.connectionManager.urls = this._nodeURLs.slice(
                this._current + 1,
                this._current + this._range
            );

            this.connectionManager
                .checkConnections()
                .then((result)=>{
                    console.log("pingNodes result", result);
                    if( result && Object.keys(result).length > 0 ) {
                        let keyPing = Object.keys(result)[0];
                        return callback({
                            url: keyPing,
                            value: result[keyPing],
                            status: {
                                instant: inRange(result[keyPing], 0 , 200),
                                low: inRange(result[keyPing], 201, 400),
                                medium: inRange(result[keyPing], 401, 800),
                                long: inRange(result[keyPing], 801, 8000),
                            }
                        });
                    }
                })
                .catch(err => {
                    console.log("doLatencyUpdate error", err);
                })
                .finally(() => {
                    this._current = this._current + this._range;
                    setTimeout(()=>this.pingNodes(callback), 50);
                });
        }
    };

    stop = () => {};

    _continueToPing() {
        return (
            this._current < this._nodeURLs.length
        );
    }





}


import CryptoJS from "crypto-js";
const Blowfish = require("javascript-blowfish");

export default class Crypto {
    static keySize = 256;
    static inSize = 128;
    static iterations = 100;

    static encrypt(message, password) {
        let salt = this.getWordRandom();
        console.log("Crypto.encrypt salt", salt);
        console.log("Crypto.encrypt salt.toString()", salt.toString());
        let iv = this.getWordRandom();
        console.log("Crypto.encrypt iv", iv);
        console.log("Crypto.encrypt iv.toString()", iv.toString());
        let hashPassword = this.hashPassword(password, salt.toString());
        console.log("Crypto.encrypt hashPassword", hashPassword);
        let base64Coded = window.btoa(message);
        console.log("Crypto.encrypt base64Coded", base64Coded);
        let keyCrypto = this.getKeyCrypto(hashPassword, salt);
        console.log("Crypto.encrypt keyCrypto", keyCrypto);
        console.log("Crypto.encrypt keyCrypto.toString()", keyCrypto.toString());
        let encrypted = CryptoJS.DES.encrypt(base64Coded, keyCrypto, {
                iv: iv
            }),
            finalEncrypted = CryptoJS.enc.Base64.stringify(
                encrypted.ciphertext
            );
        console.log("Crypto.encrypt encrypted", encrypted);
        console.log("Crypto.encrypt finalEncrypted", finalEncrypted);
        console.log("Crypto.encrypt return", salt.toString() + iv.toString() + finalEncrypted.toString());
        return salt.toString() + iv.toString() + finalEncrypted.toString();
    }

    static decrypt(transitmessage, password) {
        try {
            /*
            * 3895eba35e4ad0bb6d59f451573b7736c6eb948d6c3b62d6a75b238ca3b044a0RiCzj18mRuVmK5MrYffUJA==
            * */
            let salt = CryptoJS.enc.Hex.parse(transitmessage.substr(0, 32));
            console.log("Crypto.decrypt salt", salt);
            console.log("Crypto.decrypt salt.toString()", salt.toString());
            let iv = CryptoJS.enc.Hex.parse(transitmessage.substr(32, 32));
            console.log("Crypto.decrypt iv", iv);
            console.log("Crypto.decrypt iv.toString()", iv.toString());
            let hashPassword = this.hashPassword(password, salt.toString());
            console.log("Crypto.decrypt hashPassword", hashPassword);
            let message = transitmessage.substring(64);
            console.log("Crypto.decrypt message", message);
            let base64Decoded = CryptoJS.enc.Base64.parse(message);
            console.log("Crypto.decrypt base64Decoded", base64Decoded);
            console.log("Crypto.decrypt base64Decoded.toString()", base64Decoded.toString());
            let keyCrypto = this.getKeyCrypto(hashPassword, salt);
            console.log("Crypto.decrypt keyCrypto", keyCrypto);
            /*console.log('salt', salt);
            console.log('iv', iv);
            console.log('hashPassword', hashPassword);
            console.log('message', message);
            console.log('base64Decoded', base64Decoded);
            console.log('keyCrypto', keyCrypto);*/
            let decrypted = CryptoJS.DES.decrypt(
                {
                    ciphertext: base64Decoded
                },
                keyCrypto,
                {
                    iv: iv
                }
            );



            return CryptoJS.enc.Base64.parse(
                decrypted.toString(CryptoJS.enc.Utf8)
            ).toString(CryptoJS.enc.Utf8);
        } catch (e) {
            //console.error(e);
        }
    }

    static encryptBF(message, password) {
        const bf = new Blowfish(password);
        const encrypted = bf.encrypt(message);
        return  bf.base64Encode(encrypted);
    }
    static decryptBF(message, password) {
        const bf = new Blowfish(password);
        let DecodeKeys = bf.base64Decode(message);
        let decrypt =  bf.decrypt(DecodeKeys);
        return  decrypt.replace(/\0/g, '');
    }

    static hashPassword(password, salt) {
        return CryptoJS.MD5(password + salt).toString();
    }

    static getWordRandom() {
        return CryptoJS.lib.WordArray.random(this.inSize / 8);
    }

    static getKeyCrypto(password, salt) {
        return CryptoJS.PBKDF2(password, salt, {
            keySize: this.keySize / 32,
            iterations: this.iterations
        });
    }
}

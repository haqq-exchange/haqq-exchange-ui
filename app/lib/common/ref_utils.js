import {ChainStore, Signature, FetchChain} from "deexjs";
import WalletDb from "../../stores/WalletDb";

export default class RefUtils {

    constructor(props){
        // console.log('RefUtils props', props, this);
        this.defaultUrl = __GBLTN_CHAIN__? "https://pp-testnet.gbledger.net" : (__SCROOGE_CHAIN__ ? "https://pp-scrooge.cryptoplat.io" : (props && props.url || "https://pp.haqq.exchange"));
    }

    serialize = (obj) => {
        return Object.keys(obj).reduce(function(a, k) {
            a.push(k + "=" + encodeURIComponent(obj[k]));
            return a;
        }, []).join("&");
    };

    postTemplate = (url, body, sign) => {
        // console.log('RefUtils url, body, sign', url, body, sign)
        // console.log('RefUtils this.defaultUrl', this.defaultUrl)
        return new Promise((resolve, reject) => {

            return fetch( this.defaultUrl + url, {
                method: "POST",
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                body: this.serialize({
                    data: JSON.stringify(body),
                    sign: sign
                })

            }).then(res => {
                let status = res.status;

                res.json().then(res => resolve({
                    status,
                    content: res
                }));
            }).catch(error => {
                reject(error);
            });
        });
    };

    getSystemId() {
        return __GBL_CHAIN__ || __GBLTN_CHAIN__ ? "gbl" : (__SCROOGE_CHAIN__ ? "scrooge" : "haqq");
    }

    getUserPrivateKey = name => {
        return new Promise((resolve, reject) => {
            FetchChain("getAccount", name ).then(chain_account => {
                //const chain_account = ChainStore.getAccount(name, false);
                const memo_from_public = chain_account.getIn(["options", "memo_key"]);
                // window.console.log("memo_from_public", memo_from_public);
                const memo_from_privkey = WalletDb.getPrivateKey(memo_from_public);
                // window.console.log("memo_from_privkey", memo_from_privkey);
                return resolve(memo_from_privkey);
            }).catch((error)=>{
                return reject(error);
            });
        });
    };

    getBodySign = (private_key, body) => {
        console.log('RefUtils getBodySign', private_key, body);
        return new Promise((resolve, reject)=> {
            if (private_key) {
                const bodyForSign = JSON.stringify(body);
                let sign = Signature.signBuffer(bodyForSign, private_key);
                sign = sign.toBuffer().toString("base64");
                return resolve({sign, body});
            }
            return reject("");
        });
    };

    getUserReferral = (account, referrer ) => {
        if( account && referrer ) {
            this.getUserPrivateKey(account)
                .then(res => this.getBodySign(res, {account, referrer,
                    system_id: this.getSystemId()}))
                .then(res => this.postTemplate("/register", res.body, res.sign ))
                .catch(error=>{
                    window.console.error(error);
                });
        }
    }
}

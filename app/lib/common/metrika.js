import configConst from "config/const";
import ym from "react-yandex-metrika";
import cookies from "cookies-js";

export const hasShowMetrik = () => {
    const isTestPage = cookies.get("enable_ya_PpNJCKV");
    return  !__DEV__ && window.location.host === configConst.hostProd || isTestPage;
};

export const ymSend = (name, type, params) => {
    //if( hasShowMetrik() ) {}
    let data = {
        title: document.title,
        referer: document.referrer,
    };
    console.log("ymSend", name, type, Object.assign(data, params || {}));
    ym(name, type, Object.assign(data, params || {}));
};

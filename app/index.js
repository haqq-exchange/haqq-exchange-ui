import React from "react";
import ReactDOM from "react-dom";
import AppInit from "./AppInit";

if (__DEV__) {
    const {registerObserver} = require("react-perf-devtool");
    registerObserver();
}

if(__DEEX_CHAIN__) {
    console.log(`App for DEEX is running now.`)
} else if(__SCROOGE_CHAIN__) {
    console.log(`App for SCROOGE is running now.`)
} else if(__GBL_CHAIN__) {
    console.log(`App for GBL is running now.`)
} else if(__GBLTN_CHAIN__) {
    console.log(`App for GBLTESTNET is running now.`)
} else {
    console.log(`App for BITSHARES is running now.`)
}

const rootEl = document.getElementById("content");
const render = () => {
    ReactDOM.render(
       <AppInit />, rootEl);
};
render();
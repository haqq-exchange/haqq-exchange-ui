import {hot} from "react-hot-loader";
import React from "react";
import PropTypes from "prop-types";
import App from "./App";
import {Provider} from "react-redux";
/* actions */
import IntlActions from "actions/IntlActions";
/* actions */
/* store */
import WalletManagerStore from "stores/WalletManagerStore";
import WalletUnlockStore from "stores/WalletUnlockStore";
import SettingsStore from "stores/SettingsStore";
import AccountStore from "stores/AccountStore";
import IntlStore from "stores/IntlStore";
/* store */
import intlData from "./components/Utility/intlData";
import alt from "alt-instance";
import {connect, supplyFluxContext} from "alt-react";
import {IntlProvider} from "react-intl";

import ReactGA from "react-ga";

import {YMInitializer} from "react-yandex-metrika";
import {hasShowMetrik, ymSend} from "lib/common/metrika";

import willTransitionTo, {routerTransitioner} from "./routerTransition";
import LoadingIndicator from "./components/LoadingIndicator";
import InitError from "./components/InitError";
import SyncError from "./components/SyncError";
import counterpart from "counterpart";
import FormPost from "common/formPost";
import Bowser from "bowser";
import configBrowser from "config/browser";
import configConst from "config/const";
import ErrorReporter from "react-error-reporter";
//import ErrorBoundary from "react-error-boundary";

/*
* Electron does not support browserHistory, so we need to use hashHistory.
* The same is true for servers without configuration options, such as Github Pages
*/
import { BrowserRouter } from "react-router-dom";
import titleUtils from "./lib/common/titleUtils";
import ls from "./lib/common/localStorage";

//const STORAGE_KEY = "__deexgraphene__";
let ss = new ls(configConst.STORAGE_KEY);

/*
const hasShowMetrik = () => {
    const isTestPage = cookies.get("enable_ya_PpNJCKV");
    return  !__DEV__ && window.location.host === configConst.hostProd || isTestPage;
};
*/

let routeHistory = [];

class LocationListener extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    componentDidMount() {
        this.handleLocationChange(this.context.router.history.location);
        this.unlisten = this.context.router.history.listen(this.handleLocationChange);
    }

    componentWillUnmount() {
        this.unlisten();
    }

    static onRouteChanged(pathname) {
        document.title = titleUtils.GetTitleByPath(pathname);
    }

    handleLocationChange(location) {
        LocationListener.onRouteChanged(location.pathname);
        //if( hasShowMetrik() ) {}
        let url = [window.location.origin, location.pathname].join("");
        window.console.log(`- - - location: '${url}'`);
        ymSend("hit", location.pathname, {url: window.location.href});

        if( configConst.gaMetrika ) {
            ReactGA.pageview(location.pathname);
        }
        routeHistory.push(location.pathname);
        window.scrollTo(0, 0);
    }


    render() {
        return this.props.children;
    }
}

class RootIntl extends React.Component {
    /*UNSAFE_componentWillMount() {
        IntlActions.switchLocale(this.props.locale);
    }*/

    componentDidMount(){
        // IntlActions.switchLocale(this.props.locale);
        let locale = SettingsStore.getState().settings.get("locale");
        if( locale !== this.props.locale ) {
            IntlActions.switchLocale(this.props.locale);
        }
    }

    render() {

        const ymOptions = {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        };
        return (
            <IntlProvider
                locale={this.props.locale}
                formats={intlData.formats}
                initialNow={Date.now()}
            >
                <BrowserRouter>
                    <LocationListener>
                        <App {...this.props} />
                        <YMInitializer accounts={[hasShowMetrik() ? configConst.metrika : 53104036]} options={ymOptions} version="2" />
                    </LocationListener>
                </BrowserRouter>
            </IntlProvider>
        );
    }
}

class AppInit extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };
    constructor() {
        super();

        this.state = {
            connectionAttempt: 0,
            apiConnected: false,
            apiError: false,
            syncError: null,
            status: ""
        };
        this.startErrorLog();
    }

    starCheckFrame(){
        return new Promise((resolve, reject) => {
            // if (window.parent.length > 0) {
            //     window.stop();
            //     reject();
            // } else {
                resolve();
            // }
        });
    }

    checkLatencyUpdate = () => {
        routerTransitioner.doLatencyUpdate(true);
        window.setInterval(()=>{
            routerTransitioner.doLatencyUpdate(true);
        }, 3 * 60 * 1000);
    };

    startWillTransitionTo() {
        let {connectionAttempt} = this.state;
        willTransitionTo(true, this._statusCallback.bind(this))
            .then(() => {
                this.setState({
                    apiConnected: true,
                    apiError: false,
                    syncError: null
                });
                this.checkLatencyUpdate();
            })
            .catch(data => {
                this.setState({
                    connectionAttempt: connectionAttempt + 1,
                    apiConnected: false,
                    apiError: connectionAttempt > 5,
                    syncError: !data.error ? null : data.error && data.error.message.indexOf("ChainStore sync error") !== -1
                }, () => {
                    if( connectionAttempt <= 5 ) {
                        setTimeout(()=>{
                            this.startWillTransitionTo();
                        }, 1000);
                    } else {
                        let dataError = data && data.error;
                        this.formPostSend(dataError && data.error.message, dataError, "routerTransition.js", dataError && data.error.line);
                    }
                });

            });
    }

    componentDidMount() {
        //Detect OS for platform specific fixes
        if (navigator.platform.indexOf("Win") > -1) {
            var main = document.getElementById("content");
            var windowsClass = "windows";
            if (main.className.indexOf("windows") === -1) {
                main.className =
                    main.className +
                    (main.className.length ? " " : "") +
                    windowsClass;
            }
        }
        if( configConst.gaMetrika ) {
            ReactGA.initialize(hasShowMetrik() ? configConst.gaMetrika : "UA-138360399-1");
        }

        this.starCheckFrame()
            .then(()=>this.startWillTransitionTo())
            .catch(()=>{
                window.console.error("Hi man, WTF?");
            });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps.locked !== this.props.locked && !this.props.locked) {
            // CreditActions.clearCredit()
        }
    }

    _statusCallback(status) {
        this.setState({status});
    }

    startErrorLog = () => {
        const browser = Bowser.getParser(window.navigator.userAgent);
        const isValidBrowser = browser.satisfies(configBrowser);

        // console.log("startErrorLog this.props", this.props);
        // console.log("startErrorLog this.props", document.title );
        if (!__DEV__ && isValidBrowser) {
            // console.log("startErrorLog __DEV__");
            window.onerror = (message, file, line, column, errorObject) =>{
                window.console.log("message, file, line, column, errorObject", message, file, line, column, errorObject);
                this.formPostSend(message, file, line, column, errorObject);
            };

            /*window.addEventListener("error", function (e) {
                console.log(e.error.message, "from", e.error.stack);
            });*/
        }
    };

    formPostSend = (message, file, line, column, errorObject) => {
        const {currentAccount, locale} = this.props;
        const settings = ss.get("settings_v3");
        const viewSettings = ss.get("viewSettings_v1");
        console.log("settings_v3", ss.get("settings_v3"));
        console.log("viewSettings_v1", ss.get("viewSettings_v1"));

        console.log("this.context.router", routeHistory );

        FormPost.send("https://ui.presale-deex.exchange/report/ui", {
            login: currentAccount,
            errorMessage:{
                message,
                stack: errorObject && errorObject.stack,
                file,
                line,
                column
            },
            utlPage: window.location.href,
            title: document.title,
            routeHistory: routeHistory.splice(-5),
            version: navigator.appVersion,
            locale,
            settings,
            viewSettings,
            dateTime: counterpart.localize(new Date(), {
                format: "short"
            })
        });
    };

    render() {
        const {theme, apiServer} = this.props;
        const {apiConnected, apiError, syncError, status, connectionAttempt} = this.state;

        // console.log("syncError", syncError);
        // console.log("apiError", apiError);
        // console.log("apiConnected", !apiConnected);

        if (!apiConnected) {
            let server = apiServer;
            if (!server) {
                server = "";
            }

            // let connecting = counterpart.translate("app_init.connecting", {server: server});
            // let connection_attempt = counterpart.translate("app_init.connection_attempt", {con_number: connectionAttempt});
            return (
                <div className={theme} style={{height: "100%"}}>
                    <div id="content-wrapper" style={{height: "100%"}}>
                        <div className="grid-frame-app" style={{height: "100%", alignItems: "center", flexDirection: "inherit"}}>
                            {!apiError ? (
                                <LoadingIndicator type={"loaded-node"} customStyle={{maxWidth: 800}}/>
                            ) : syncError ? (
                                <SyncError />
                            ) : (
                                <InitError connected={()=>this.setState({
                                    apiError: false,
                                    apiConnected: true,

                                })} />
                            )}
                        </div>
                    </div>
                </div>
            );
        }
        return <ErrorReporter captureException={(error, ...args) => window.console.error(error, ...args)}>
            <RootIntl {...this.props} {...this.state} />
        </ErrorReporter>;
    }
}

const AppInitConnect = connect(AppInit, {
    listenTo() {
        return [IntlStore, WalletManagerStore, WalletUnlockStore, SettingsStore, AccountStore];
    },
    getProps() {
        let {
            currentAccount,
            passwordLogin,
            myActiveAccounts,
            myHiddenAccounts,
            passwordAccount } = AccountStore.getState();
        return {
            currentAccount,
            myActiveAccounts,
            myHiddenAccounts,
            passwordAccount,
            passwordLogin,
            locale: IntlStore.getState().currentLocale,
            locked: WalletUnlockStore.getState().locked,
            walletMode: !SettingsStore.getState().settings.get("passwordLogin") || !!WalletManagerStore.getState().current_wallet,
            theme: SettingsStore.getState().settings.get("themes"),
            apiServer: SettingsStore.getState().settings.get("activeNode", "")
        };
    }
});

const AppInitSupp = supplyFluxContext(alt)(AppInitConnect);
export default hot(module)(AppInitSupp);

import idb_helper from "idb-helper";

const DB_VERSION_MAIN = 2;
const DB_PREFIX = "graphene_db_v2";

/** Usage: openIndexDB.then( db => ... */
export default class iDBRoot {
    constructor(impl) {
        // console.log("idb impl", impl)
        this.impl = impl;
    }

    setDbSuffix(db_suffix) {
        // "graphene_db_06f667"
        // console.log("idb db_suffix", db_suffix);
        this.database_name = DB_PREFIX + db_suffix;
    }

    /** @return promise */
    openIndexedDB() {
        // console.log("openIndexedDB", this.db);
        if (this.db) return Promise.resolve(this.db);
        return new Promise((resolve, reject) => {
            // debugger;
            var openRequest = this.impl.open(
                this.database_name,
                DB_VERSION_MAIN
            );
            // console.log("openRequest", openRequest);
            openRequest.onupgradeneeded = event => {
                //console.log("event", event);
                // var is_new_db = isNaN(event.oldVersion) || event.oldVersion === 0;
                // console.log("is_new_db", is_new_db)
                try {
                    this.db = event.target.result;
                    this.db.createObjectStore("properties", {keyPath: "name"});
                } catch (e) {}
                
            };
            openRequest.onsuccess = e => {
                // console.log("event", e);
                this.db = e.target.result;
                resolve(this.db);
            };
            openRequest.onerror = e => {
                // console.log("event", e);
                reject(e.target.error);
            };
        });
    }

    /** @return promise */
    getProperty(name, default_value) {
        return this.openIndexedDB()
            .then(db => {
                // debugger;
                try {
                    var transaction = db.transaction(["properties"], "readonly");
                    var store = transaction.objectStore("properties");
                    return idb_helper
                        .on_request_end(store.get(name))
                        .then(event => {
                            var result = event.target.result;
                            return result ? result.value : default_value;
                        }).catch(error => {
                            console.error(error);
                            throw error;
                        });
                } catch (e) {
                    console.log("error", e);
                }

            })
            .catch(error => {
                console.error(error);
                throw error;
            });
    }

    /** @return promise */
    setProperty(name, value) {
        return this.openIndexedDB()
            .then(db => {
                var transaction = db.transaction(["properties"], "readwrite");
                var store = transaction.objectStore("properties");
                if (value && value["toJS"]) value = value.toJS(); //Immutable-js
                return idb_helper.on_request_end(store.put({name, value}));
            })
            .catch(error => {
                console.error(error);
                throw error;
            });
    }

    deleteDatabase(are_you_sure = false) {
        if (!are_you_sure) return "Are you sure?";
        // console.log("deleting", this.database_name);
        var req = this.impl.deleteDatabase(this.database_name);
        return req.result;
    }

    close() {
        this.db.close();
        this.db = null;
    }
}

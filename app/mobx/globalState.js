import { observable } from "mobx";

class State {
    @observable title = "Title";

    changeTitle = e => {
        this.title = e.target.value;
    };
}

const state = new State();

export default state;

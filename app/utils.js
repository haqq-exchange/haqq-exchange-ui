// функция удаляет одинаковые ключь/значение из массива обьектов
export const getUniqueArray = (arr) => {
    const uniqueArray = arr.filter((thing, index) => {
        const _thing = JSON.stringify(thing);
            return index === arr.findIndex(obj => {
                return JSON.stringify(obj) === _thing;
    });
  })
  return uniqueArray;
};

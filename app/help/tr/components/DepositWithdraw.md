[# receive]
### Kullanıcı adınız
Deex hesabınıza bir başkasından ya da bir borsadan DEEX gönderimi için, göndericiye kullanıcı adınızı vermeniz yeterlidir. DEEX'ler doğrudan kullanıcı adı belirtilerek gönderilir:

[# deposit-short]
### Kripto para yatır/çek
Başka blokzincirlerinden Deex hesabınıza (ör. BTC gibi) "coin" aktarmak için ya da TL, dolar gibi fiat para birimleriyle Deex'de işlem yapabilmek için lütfen aşağıdan bir transfer hizmet sağlayıcısı seçin.

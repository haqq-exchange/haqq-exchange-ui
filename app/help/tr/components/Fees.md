# Ücretler

DEEX ekosistemi içerisinde her işleme tahsis edilmiş *bireysel* bir ücret vardır. 
Bu ücretler değişime tabidirler. Bununla birlikte, bunlar sadece hissedarların oylarıyla 
belirlenirler, böylece Scrooge'in esas varlığı olan DEEX hissedarlarının her biri 
ücretlerin ne olacağı konusunda söz sahibi olur. Eğer hissedarlar belli bir ücretin 
düşürülmesi konusunda fikir birliği sağlarlarsa o ücret blok zinciri tarafından otomatik 
olarak düşürülür. Blok zinciri parametrelerinin düzenlemesiyle ilgili teklifler kurul 
üyeleri tarafından yapılırlar. Bu üyeler hissedarlar tarafından oy kullanılarak seçilirler 
ve herhangi bir durumda, esneklik ve  reaksiyon kabiliyetini arttırırlar.

<!-- Scrooge ekosistemi içerisinde her işleme tahsis edilmiş *bireysel* bir ücret vardır. 
Bu ücretler değişime tabidirler. Bununla birlikte, bunlar sadece hissedarların oylarıyla 
belirlenirler, böylece Scrooge'in esas varlığı olan SCROOGE hissedarlarının her biri 
ücretlerin ne olacağı konusunda söz sahibi olur. Eğer hissedarlar belli bir ücretin 
düşürülmesi konusunda fikir birliği sağlarlarsa o ücret blok zinciri tarafından otomatik 
olarak düşürülür. Blok zinciri parametrelerinin düzenlemesiyle ilgili teklifler kurul 
üyeleri tarafından yapılırlar. Bu üyeler hissedarlar tarafından oy kullanılarak seçilirler 
ve herhangi bir durumda, esneklik ve  reaksiyon kabiliyetini arttırırlar. -->

<!-- GBLedger ekosistemi içerisinde her işleme tahsis edilmiş *bireysel* bir ücret vardır. 
Bu ücretler değişime tabidirler. Bununla birlikte, bunlar sadece hissedarların oylarıyla 
belirlenirler, böylece Scrooge'in esas varlığı olan GBL hissedarlarının her biri 
ücretlerin ne olacağı konusunda söz sahibi olur. Eğer hissedarlar belli bir ücretin 
düşürülmesi konusunda fikir birliği sağlarlarsa o ücret blok zinciri tarafından otomatik 
olarak düşürülür. Blok zinciri parametrelerinin düzenlemesiyle ilgili teklifler kurul 
üyeleri tarafından yapılırlar. Bu üyeler hissedarlar tarafından oy kullanılarak seçilirler 
ve herhangi bir durumda, esneklik ve  reaksiyon kabiliyetini arttırırlar. -->
# Fees

In the DEEX ecosystem every operation is assigned an *individual* fee.
These fees are subject to change. However, they are defined solely by
shareholder approval, thus each and every shareholder of the Deex core
asset (DEEX) has a say as to what the fees should be. If shareholders can be
convinced to reduce a certain fee and consensus is reached, the fee will be
reduced automatically by the blockchain. Changes of blockchain parameters are
proposed by members of the committee. These members are voted by shareholders
and improve the flexibility and reaction rate.

\*: These operations require Lifetime membership!

<!-- In the Scrooge ecosystem every operation is assigned an *individual* fee.
These fees are subject to change. However, they are defined solely by
shareholder approval, thus each and every shareholder of the Scrooge core
asset (SCROOGE) has a say as to what the fees should be. If shareholders can be
convinced to reduce a certain fee and consensus is reached, the fee will be
reduced automatically by the blockchain. Changes of blockchain parameters are
proposed by members of the committee. These members are voted by shareholders
and improve the flexibility and reaction rate.

\*: These operations require Lifetime membership! -->


<!-- In the GBLedger ecosystem every operation is assigned an *individual* fee.
These fees are subject to change. However, they are defined solely by
shareholder approval, thus each and every shareholder of the GBLedger core
asset (GBL) has a say as to what the fees should be. If shareholders can be
convinced to reduce a certain fee and consensus is reached, the fee will be
reduced automatically by the blockchain. Changes of blockchain parameters are
proposed by members of the committee. These members are voted by shareholders
and improve the flexibility and reaction rate.

\*: These operations require Lifetime membership! -->

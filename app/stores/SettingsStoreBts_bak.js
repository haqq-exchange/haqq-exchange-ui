import alt from "alt-instance";
import SettingsActions from "actions/SettingsActions";
import IntlActions from "actions/IntlActions";
import Immutable, {fromJS} from "immutable";
import {merge, set, get} from "lodash";
import ls from "common/localStorage";
import {Apis} from "bitsharesjs-ws";
import {settingsAPIs, getSettingApi} from "api/apiConfig";
import topMarkets from "config/markets";
import GatewayStore from "./GatewayStore";
import GatewayActions from "../actions/GatewayActions";
import marketUtils from "common/market_utils";
import {availableGateways} from "../lib/common/gateways";

const CORE_ASSET = __GBLTN_CHAIN__ ? "GBLTEST" : (__GBL_CHAIN__ ? "GBL" :(__SCROOGE_CHAIN__ ? "SCROOGE" :  "DEEX")); // Setting this to BTS to prevent loading issues when used with BTS chain which is the most usual case currently

const STORAGE_KEY = "__deexgraphene__";
let ss = new ls(STORAGE_KEY);

class SettingsStore {
    constructor() {
        let settingsApi = {};



        // If you want a default value to be translated, add the translation to settings in locale-xx.js
        // and use an object {translate: key} in the defaults array
        //let apiServer = settingsApi.WS_NODE_LIST;

        //const testnetAPI = apiServer.find(a => a.url.indexOf("node.testnet.bitshares.eu") !== -1);

        this.exportPublicMethods({
            init: this.init.bind(this),
            _getChainKey: this._getChainKey,
            addChartsMarkets: this.addChartsMarkets.bind(this),
            //setTopHour24Volume: this.setTopHour24Volume.bind(this),
            getSetting: this.getSetting.bind(this),
            getBackedCoins: this.getBackedCoins.bind(this),
            getLastBudgetObject: this.getLastBudgetObject.bind(this),
            setLastBudgetObject: this.setLastBudgetObject.bind(this)
        });

        this.bindListeners({
            onSetExchangeLastExpiration: SettingsActions.setExchangeLastExpiration,
            onChangeSetting: SettingsActions.changeSetting,
            onChangeViewSetting: SettingsActions.changeViewSetting,
            onSetTopHour24Volume: SettingsActions.setTopHour24Volume,
            onChangeChartsObjectList: SettingsActions.changeChartsObjectList,
            onChangeMarketDirection: SettingsActions.changeMarketDirection,
            onAddStarMarket: SettingsActions.addStarMarket,
            onRemoveStarMarket: SettingsActions.removeStarMarket,
            onClearStarredMarkets: SettingsActions.clearStarredMarkets,
            onAddWS: SettingsActions.addWS,
            onRemoveWS: SettingsActions.removeWS,
            onShowWS: SettingsActions.showWS,
            onHideWS: SettingsActions.hideWS,
            onHideAsset: SettingsActions.hideAsset,
            onFavoriteAsset: SettingsActions.favoriteAsset,
            onHideMarket: SettingsActions.hideMarket,
            onClearSettings: SettingsActions.clearSettings,
            onSwitchLocale: IntlActions.switchLocale,
            onSetUserMarket: SettingsActions.setUserMarket,
            onUpdateLatencies: SettingsActions.updateLatencies,
            onDeleteMarketTab: SettingsActions.deleteMarketTab,
            onAddMarketTab: SettingsActions.addMarketTab,
            onChangeAssetMarketTab: SettingsActions.changeAssetMarketTab,
            onChangeDepositFavoriteTab: SettingsActions.changeDepositFavorite,
            onChangeWithdrawFavoriteTab: SettingsActions.changeWithdrawFavorite,
            onFetchCoinsSimple: GatewayActions.fetchCoinsSimple
        });

        this.initDone = false;
        this.defaultSettings = Immutable.Map({
            locale: "en",
            apiServer: settingsAPIs.DEFAULT_WS_NODE,
            faucet_address: settingsAPIs.DEFAULT_FAUCET,
            // eslint-disable-next-line no-undef
            unit: __TESTNET__ ? "TEST" : CORE_ASSET,
            balance: "BTS",
            fee_asset: __GBLTN_CHAIN__ ? "GBLTEST" : (__GBL_CHAIN__ ? "GBL" : (__SCROOGE_CHAIN__ ? "SCROOGE" : "DEEX")),
            showSettles: false,
            showAssetPercent: false,
            walletLockTimeout: 60 * 10,
            themes: "darkTheme",
            passwordLogin: "login",
            browser_notifications: {
                allow: true,
                additional: {
                    transferToMe: true
                }
            }
        });


        let defaults = {
            locale: [
                "en",
                "ru",
                "tr",
                "zh"
            ],
            apiServer: settingsAPIs.WS_NODE_LIST,
            // eslint-disable-next-line no-undef
            isTestNet: __TESTNET__,
            // eslint-disable-next-line no-undef
            unit: __GBL_CHAIN__ ? [CORE_ASSET, "GBL"] : (__TESTNET__ ? ["TEST"] : [CORE_ASSET, "DEEX", "USD", "CNY", "DEEX.BTC", "EUR", "QIWIRUBLE"]),
            showSettles: [{translate: "yes"}, {translate: "no"}],
            showAssetPercent: [{translate: "yes"}, {translate: "no"}],
            themes: ["lightTheme", "darkTheme"], //["darkTheme", "lightTheme", "midnightTheme"],
            passwordLogin: [
                {translate: "cloud_login"},
                {translate: "local_wallet"}
            ]
            // confirmMarketOrder: [
            //     {translate: "confirm_yes"},
            //     {translate: "confirm_no"}
            // ]
        };

        this.settings = Immutable.Map(
            merge(this.defaultSettings.toJS(), ss.get("settings_v3"))
        );

        let savedDefaults = ss.get("defaults_v1", {});
        /* Fix for old clients after changing cn to zh */
        if (savedDefaults && savedDefaults.locale) {
            if( defaults.locale !== savedDefaults.locale ) {
                savedDefaults.locale = defaults.locale;
            }
        }
        if (savedDefaults && savedDefaults.themes) {
            let olIdx = savedDefaults.themes.findIndex(
                a => a === "olDarkTheme"
            );
            if (olIdx !== -1) savedDefaults.themes[olIdx] = "midnightTheme";
        }

        this.settings.set("themes", "lightTheme"); //принудительно ставим светлую...

        this.defaults = merge({}, defaults, savedDefaults);
        this.viewSettings = Immutable.Map(ss.get("viewSettings_v1"));

        this.marketDirections = Immutable.Map(ss.get("marketDirections"));

        this.hiddenAssets = Immutable.Map(Immutable.fromJS(ss.get("hiddenAssets", {"defaultHidden": {"1.3.0": true}})));
        this.favoriteAssets = Immutable.Map(Immutable.fromJS(ss.get("favoriteAssets", [])));
        this.hiddenMarkets = Immutable.Map(ss.get("hiddenMarkets", []));


        this.apiLatencies = Immutable.fromJS(ss.get("apiLatencies", {}));
        this.topHour24Volume = Immutable.fromJS(ss.get("topHour24Volume", {}));
        this.mainnet_faucet = ss.get(
            "mainnet_faucet",
            settingsAPIs.DEFAULT_FAUCET
        );
        this.testnet_faucet = ss.get(
            "testnet_faucet",
            settingsAPIs.TESTNET_FAUCET
        );

        this.exchange = fromJS(ss.get("exchange", {}));
        this.chartsObjectList = {};
        this.chartsPostion = Immutable.fromJS({
            top: {
                left: {
                    deleteChart: true,
                },
                center: {
                    deleteChart: true,
                },
                right: {
                    deleteChart: true
                }
            },
            bottom: {
                left: {
                    deleteChart: true,
                },
                center: {
                    deleteChart: true
                },
                right: {
                    deleteChart: true
                }
            }
        });
        this.viewMarketsMix = {
            top: {
                /*center: {
                    component: loadable(() => import("components/Dashboard/MarketCard/MixChart/ieoCryptomat/ieoCryptomat"))
                }*/
            },
            bottom: {

            }
        };


        getSettingApi().then(this.setApiLatencies);
    }

    init() {
        return new Promise(resolve => {
            if (this.initDone) resolve();
            this.starredKey = this._getChainKey("markets");
            this.marketsKey = this._getChainKey("userMarkets");
            this.marketsTabKey = this._getChainKey("marketsTab");

            let bases = {
                markets_4018d784: 
                __GBLTN_CHAIN__ ?
                [
                    "GBLTEST",
                    "CRYPTOMAT",
                    "GBL.BTC",
                    "GBL.ETH",
                    "USDTETHER"
                ] :
                (__GBL_CHAIN__  ?
                [
                    "GBL",
                    "CRYPTOMAT",
                    "GBL.BTC",
                    "GBL.ETH",
                    "USDTETHER"
                ] :
                (__SCROOGE_CHAIN__ ?
                [
                    "SCROOGE",
                    "CRYPTOMAT",
                    "SCROOGE.BTC",
                    "SCROOGE.ETH",
                    "USDTETHER"
                ]: [
                    "DEEX",
                    "CRYPTOMAT",
                    "DEEX.BTC",
                    "DEEX.ETH",
                    "USDTETHER"
                ])),
                markets_39f5e2ed: [
                    // TESTNET
                    "TEST"
                ]
            };


            let coreAssets = {
                markets_4018d784: "BTS",
                markets_39f5e2ed: "TEST"
            };
            let coreAsset = coreAssets[this.starredKey] || "DEEX" || "GBL";

            if( this.defaults.unit.indexOf(coreAsset) === -1 ) {
                this.defaults.unit.shift();
                this.defaults.unit.push(coreAsset);
            }

            this.basesMarkets = bases[this.starredKey] || bases.markets_4018d784;
            this._setDefaultMarket();
            this._setAllMarket();
            this._setDefaultMarketTab();

            this.starredMarkets = Immutable.Map(ss.get(this.starredKey, []));
            this.userMarkets = Immutable.Map(ss.get(this.marketsKey, {}));

            this.assetMarketTab = fromJS(ss.get(this._getChainKey("assetMarketTab"), {}));
            this.depositFavorite = fromJS(ss.get(this._getChainKey("depositFavorite"), {}));
            this.withdrawFavorite = fromJS(ss.get(this._getChainKey("withdrawFavorite"), {}));


            this.initDone = true;
            //this.addChartsMarkets(chartsMarkets);
            resolve();
        });
    }

    setApiLatencies = (settingsApi) => {
        let nodeListValues = settingsApi.WS_NODE_LIST.map(node_list => node_list.url);
        let apiLatencies = ss.get("apiLatencies", {});
        let apiLatenciesKeys = Object.keys(apiLatencies);
        let diffAddNew = nodeListValues.filter(x => !apiLatenciesKeys.includes(x));
        let diffRemoveNew = apiLatenciesKeys.filter(x => !nodeListValues.includes(x));
        if(diffAddNew.length  ) {
            diffAddNew.map(url => {apiLatencies[url] = 0;});
        }

        if(  diffRemoveNew.length  ) {
            diffRemoveNew.map(url => { delete apiLatencies[url];});
        }

        //
        //

        if(  diffAddNew.length || diffRemoveNew.length  ) {
            SettingsActions.updateLatencies(apiLatencies);
            this.defaults.apiServer = Object.values(merge({}, this.defaults.apiServer, settingsApi.WS_NODE_LIST));
            ss.set("defaults_v1", this.defaults);
        }

        // this.addChartsMarkets(settingsApi.CHART_MARKETS);
        this.addChartsMarkets([
            {
                autoChart: true,
                "positionChart": ["top","left"],
            },{
                autoChart: true,
                "positionChart": ["top","center"]
            },{
                autoChart: true,
                "positionChart": ["top","right"]
            },{
                autoChart: true,
                "positionChart": ["bottom","left"]
            },{
                autoChart: true,
                "positionChart": ["bottom","center"]
            },{
                autoChart: true,
                "positionChart": ["bottom","right"]
            }
        ]);

        SettingsActions.changeViewSetting({
            "email_code": settingsApi.EMAIL_CODE || 8
        });

        SettingsActions.changeViewSetting({
            "pair_markets": settingsApi.PAIR_MARKETS || []
        });

    };

    addChartsMarkets(chartsMarkets = this.chartsMarkets.toJS()) {
        //debugger;
        let chartsObject = {};

        //

        this.chartsMarkets = Immutable.List(chartsMarkets);
        let viewMarketsCharts = this.viewSettings.get("viewMarketsCharts") || this.viewMarketsMix;

        //

        if( this.viewSettings.get("viewMarketsCharts")  ) {
            let imViewMarketsMix = Immutable.fromJS(this.viewMarketsMix);
            let imViewMarketsCharts = Immutable.fromJS(viewMarketsCharts);
            viewMarketsCharts = imViewMarketsCharts.mergeDeep(imViewMarketsMix).toJS();
        }


        chartsMarkets.map(charts=>{
            if( viewMarketsCharts && get(viewMarketsCharts, charts.positionChart) ) {
                set(chartsObject, charts.positionChart, get(viewMarketsCharts, charts.positionChart));
            } else {
                set(chartsObject, charts.positionChart, charts);
            }
        });

        //
        this.chartsObjectList = chartsObject;
        this.chartsMarketsList = Immutable.fromJS(chartsObject).mergeDeep(this.chartsPostion);
    }

    getSetting(setting) {
        return this.settings.get(setting);
    }

    onChangeSetting(payload) {
        if( payload.setting ) {
            this._changeSetting(payload);
        } else {
            payload.map((data)=>this._changeSetting(data));
        }
    }
    _changeSetting(payload) {
        this.settings = this.settings.set(payload.setting, payload.value);

        switch (payload.setting) {
            case "faucet_address":
                if (payload.value.indexOf("testnet") === -1) {
                    this.mainnet_faucet = payload.value;
                    ss.set("mainnet_faucet", payload.value);
                } else {
                    this.testnet_faucet = payload.value;
                    ss.set("testnet_faucet", payload.value);
                }
                break;

            case "apiServer":
                // eslint-disable-next-line no-case-declarations
                let faucetUrl = payload.value.indexOf("testnet") !== -1 ? this.testnet_faucet : this.mainnet_faucet;
                this.settings = this.settings.set("faucet_address", faucetUrl);
                break;

            case "walletLockTimeout":
                ss.set("lockTimeout", payload.value);
                break;

            default:
                break;
        }

        ss.set("settings_v3", this.settings.toJS());
    }

    onChangeViewSetting(payload) {
        for (let key in payload) {
            this.viewSettings = this.viewSettings.set(key, payload[key]);

        }



        ss.set("viewSettings_v1", this.viewSettings.toJS());
    }

    onChangeMarketDirection(payload) {
        for (let key in payload) {
            this.marketDirections = this.marketDirections.set(
                key,
                payload[key]
            );
        }
        ss.set("marketDirections", this.marketDirections.toJS());
    }

    onHideAsset(payload) {
        this._onHiddenSelected(payload, "hiddenAssets");
    }

    onFavoriteAsset(payload) {
        this._onHiddenSelected(payload, "favoriteAssets");
    }
    onHideMarket(payload) {
        this._onHiddenSelected(payload, "hiddenMarkets");
    }

    _onHiddenSelected( payload, type ) {
        if( type && this[type] ) {
            if (payload.id) {
                if (payload.status) {
                    this[type] = this[type].setIn([payload.accountName, payload.id], payload.status);
                } else {
                    this[type] = this[type].deleteIn([payload.accountName, payload.id]);
                }
            }



            ss.set(type, this[type].toJS());
        }
    }

    onAddStarMarket(market) {
        let marketID = market.quote + "_" + market.base;
        if (!this.starredMarkets.has(marketID)) {
            this.starredMarkets = this.starredMarkets.set(marketID, {
                quote: market.quote,
                base: market.base
            });

            ss.set(this.starredKey, this.starredMarkets.toJS());
        } else {
            return false;
        }
    }

    onSetUserMarket(payload) {
        let marketID = payload.quote + "_" + payload.base;
        if (payload.value) {
            this.userMarkets = this.userMarkets.set(marketID, {
                quote: payload.quote,
                base: payload.base
            });
        } else {
            this.userMarkets = this.userMarkets.delete(marketID);
        }
        ss.set(this.marketsKey, this.userMarkets.toJS());
    }

    onRemoveStarMarket(market) {
        let marketID = market.quote + "_" + market.base;

        this.starredMarkets = this.starredMarkets.delete(marketID);

        ss.set(this.starredKey, this.starredMarkets.toJS());
    }

    onClearStarredMarkets() {
        this.starredMarkets = Immutable.Map({});
        ss.set(this.starredKey, this.starredMarkets.toJS());
    }

    onAddWS(ws) {
        if (typeof ws === "string") {
            ws = {url: ws, location: null};
        }
        this.defaults.apiServer.push(ws);
        ss.set("defaults_v1", this.defaults);
    }

    onRemoveWS(index) {
        this.defaults.apiServer.splice(index, 1);
        ss.set("defaults_v1", this.defaults);
    }

    onHideWS(url) {
        let node = this.defaults.apiServer.find(node => node.url === url);
        node.hidden = true;
        ss.set("defaults_v1", this.defaults);
    }

    onShowWS(url) {
        let node = this.defaults.apiServer.find(node => node.url === url);
        node.hidden = false;
        ss.set("defaults_v1", this.defaults);
    }

    onClearSettings(resolve) {
        ss.remove("settings_v3");
        this.settings = this.defaultSettings;

        ss.set("settings_v3", this.settings.toJS());

        if (resolve) {
            resolve();
        }
    }

    onSwitchLocale({locale}) {
        this.onChangeSetting({setting: "locale", value: locale});
    }

    _getChainKey(key) {
        const chainId = Apis.instance().chain_id;
        return key + (chainId ? `_${chainId.substr(0, 8)}` : "");
    }

    onUpdateLatencies(latencies) {
        //
        //
        ss.set("apiLatencies", latencies);
        //this.defaults.apiServer = Object.keys(latencies);
        this.apiLatencies = Immutable.fromJS(latencies);
    }

    getLastBudgetObject() {
        return ss.get(this._getChainKey("lastBudgetObject"), "2.13.1");
    }

    setLastBudgetObject(value) {
        ss.set(this._getChainKey("lastBudgetObject"), value);
    }

    setExchangeSettings(key, value) {
        this.exchange = this.exchange.set(key, value);

        ss.set("exchange", this.exchange.toJS());
    }

    getExchangeSettings(key) {
        return this.exchange.get(key);
    }

    onSetExchangeLastExpiration(value) {
        this.setExchangeSettings("lastExpiration", fromJS(value));
    }

    getExhchangeLastExpiration() {
        return this.getExchangeSettings("lastExpiration");
    }

    onAddMarketTab(tab) {
        /*let MarketsTab = this.marketsTab.toJS();
        if( !MarketsTab.length ) {
            MarketsTab = this.preferredBases.toJS();
        }*/
        this.marketsTab = this.marketsTab.add(tab);
        ss.set(this.marketsTabKey, this.marketsTab.toJS());
    }
    onDeleteMarketTab(tab) {
        let MarketsTab = this.marketsTab.toJS();
        if( !MarketsTab.length ) {
            MarketsTab = this.preferredBases.toJS();
            this.marketsTab = Immutable.Set(MarketsTab);
        }
        this.marketsTab = this.marketsTab.delete(tab);
        ss.set(this.marketsTabKey, this.marketsTab.toJS());
    }

    onChangeAssetMarketTab = ({base, data}) => {
        this.assetMarketTab = this.assetMarketTab.set(base, fromJS(data));
        ss.set(this._getChainKey("assetMarketTab"), this.assetMarketTab.toJS());
    };

    onChangeDepositFavoriteTab = (data) => {
        this.depositFavorite = fromJS(data);
        ss.set(this._getChainKey("depositFavorite"), this.depositFavorite.toJS());
    };

    onChangeWithdrawFavoriteTab = ( data ) => {
        this.withdrawFavorite = fromJS(data);
        ss.set(this._getChainKey("withdrawFavorite"), this.withdrawFavorite.toJS());
    };

    getBackedCoins = (gateways) => {
        return new  Promise(resolve=>{
            gateways.map(gateway=>{
                try {
                    const backedCoins = GatewayStore.getState().backedCoins.get(gateway, []);
                    if( backedCoins.hasOwnProperty("length")) {
                        let markets = backedCoins
                            .map(coins => coins.symbol)
                            .filter(a=>a)
                            .sort();
                        var uniqueArray = function(arrArg) {
                            return arrArg.filter(function(elem, pos,arr) {
                                return arr.indexOf(elem) === pos;
                            });
                        };
                        topMarkets[this.starredKey] = uniqueArray(markets.concat(topMarkets[this.starredKey]));
                    }
                } catch (err) {
                    console.log(err);
                }
            });
            resolve(topMarkets);
        });

    };

    _setAllMarket = () => {
        let availableGateway = Object.values(availableGateways).map(gateway => {
            if( gateway.isEnabled ) {
                return gateway.id;
            }
        }).filter(a=>a);
        this.getBackedCoins(availableGateway).then(allMarkets=>{
            let chainTopMarkets = allMarkets[this.starredKey] || allMarkets.markets_4018d784;
            this.preferredTopMarkets = Immutable.List(chainTopMarkets);

            let defaultMarkets = [];
            //let chainMarkets = topMarkets[this.starredKey] || [];
            this.preferredTopMarkets.forEach(base => {
                marketUtils.addMarkets(defaultMarkets, base, chainTopMarkets);
            });

            this.defaultAllMarkets = Immutable.Map(defaultMarkets);

        });
    };

    _setDefaultMarket = () => {
        let chainBases = this.basesMarkets;
        let availableGateway = Object.values(availableGateways).map(gateway => {
            if( gateway.isEnabled ) {
                return gateway.id;
            }
        }).filter(a=>a);
        this.getBackedCoins(["TDEX"]).then(allMarkets=>{
            let chainTopMarkets = allMarkets[this.starredKey] || allMarkets.markets_4018d784;
            //chainTopMarkets = chainTopMarkets.concat(backedCoins);
            this.preferredBases = Immutable.List(chainBases);
            this.preferredTopMarkets = Immutable.List(chainTopMarkets);

            let defaultMarkets = [];
            //let chainMarkets = topMarkets[this.starredKey] || [];
            this.preferredTopMarkets.forEach(base => {
                marketUtils.addMarkets(defaultMarkets, base, chainTopMarkets);
            });

            this.defaultMarkets = Immutable.Map(defaultMarkets);

        });


    };

    _setDefaultMarketTab = () => {
        let ssKey = ss.get(this.marketsTabKey, this.basesMarkets);
        /* Меняем родной CVN на CRYPVISER */
        if( ssKey.includes("CVN") ) {
            ssKey.splice(ssKey.indexOf("CVN"), 1, "CRYPVISER");
            ss.set(this.marketsTabKey, ssKey);
        }
        this.marketsTab = Immutable.Set(ssKey);
    };

    onFetchCoinsSimple = ({coins} = {}) => {
        // let fetchCoins =  this._getBackedCoins(coins);
        //
        this._setDefaultMarket();
    };

    onChangeChartsObjectList = (viewMarketsCharts) => {


        this.chartsObjectList = viewMarketsCharts;
    };

    onSetTopHour24Volume = ({data, resolve}) => {
        let chartsMarketsList = this.chartsMarketsList;
        let viewSettings = this.viewSettings;
        let topHour24Volume = this.topHour24Volume.toJS();
        let activePairs = [];
        let autoChartPosition = [];


        //

        data.sort((a,b)=> {
            return a.volume < b.volume ? 1 : b.volume < a.volume ? -1 : 0 ;
        }).map(item=>{
            topHour24Volume[[item.base, item.quote].join("_")] = item;
        });
        ss.set("topHour24Volume", topHour24Volume);

        //

        Object.keys(chartsMarketsList.toJS()).map(chartsKey => {
            //
            let charts = chartsMarketsList.get(chartsKey).toJS();
            Object.keys(charts).map(chartKey => {
                let chartPosition = [chartsKey, chartKey];
                let chart = chartsMarketsList.getIn(chartPosition).toJS();
                //
                if ( chart.autoChart ) {
                    autoChartPosition.push(chart.positionChart);
                }
                if ( chart.base && chart.quote ) {
                    activePairs.push([chart.base, chart.quote].join("_"));
                }
            });
        });

        //

        if( autoChartPosition.length ) {
            let viewMarketsCharts = viewSettings.get("viewMarketsCharts") || {};
            Object.keys(topHour24Volume).map(item => {
                if (!activePairs.includes(item)) {
                    let data = topHour24Volume[item];
                    let chartPosition = autoChartPosition.pop();
                    if( chartPosition ) {
                        set(viewMarketsCharts, chartPosition, {
                            "quote": data.quote,
                            "base": data.base,
                            "autoChart": false,
                            "positionChart": chartPosition
                        });
                    }

                }
            });

            resolve({
                viewMarketsCharts: viewMarketsCharts
            });
        }

        resolve(false);
    }


}

export default alt.createStore(SettingsStore, "SettingsStore");

import alt from "alt-instance";
import WalletUnlockActions from "actions/WalletUnlockActions";
import SettingsActions from "actions/SettingsActions";
import WalletDb from "stores/WalletDb";
import App from "../App";
import ls from "common/localStorage";
import BaseStore from "./BaseStore";
import ModalActions from "../actions/ModalActions";
import configConst from "config/const";
// import { time } from "console";
const STORAGE_KEY = "__deexgraphene__";
let ss = new ls(configConst.STORAGE_KEY);

class WalletUnlockStore extends BaseStore {
    constructor() {
        super();

        this.bindActions(WalletUnlockActions);

        // can't use settings store due to possible initialization race conditions
        const storedSettings = ss.get("settings_v4", {});
        if (storedSettings.passwordLogin === undefined) {
            storedSettings.passwordLogin = "login";
        }
        let passwordLogin = storedSettings.passwordLogin;
        this.state = {
            isCanceled: false,
            locked: true,
            passwordLogin,
        };

        // this.walletLockTimeout = this._getTimeout(); // seconds (10 minutes)
        this.walletLockTimeout = null;
        this.timeout = null;
        this.timeoutInfo = null;

        this.bindListeners({
            onChangeSetting: SettingsActions.changeSetting
        });

        // let timeoutSetting = this._getTimeout();

        // if (timeoutSetting) {
        //     this.walletLockTimeout = timeoutSetting;
        // }
    }

    onUnlock({resolve, reject}) {
        //DEBUG
        // debugger;
        // console.log("WalletUnlockStore.js onUnlock");
        // console.log('... onUnlock setState', WalletDb.isLocked());
        //
        this._setLockTimeout();
        if (!WalletDb.isLocked()) {
            this.setState({locked: false});
            resolve();
            return;
        }

        this.setState({
            resolve,
            reject,
            isCanceled: !WalletDb.isLocked(),
            locked: WalletDb.isLocked()
        });

        resolve();
    }

    onLock({resolve}) {
        //DEBUG
        // console.log('... WalletUnlockStore\tprogramatic lock', WalletDb.isLocked())
        if (WalletDb.isLocked()) {
            resolve();
            return;
        }
        WalletDb.onLock();
        this.setState({
            resolve: null,
            reject: null,
            isCanceled: false,
            locked: WalletDb.isLocked()
        }, resolve);
    }

    onCancel() {
        // console.log("WalletUnlockStore.js onCancel");
        if (typeof this.state.reject === "function")
            this.state.reject({
                isCanceled: true
            });
        this.setState({
            resolve: null,
            reject: null,
            isCanceled: true
        });
        this._setLockTimeout();
    }

    onChange() {
        console.log("Интервал таймаута", this.walletLockTimeout)
        // console.log("WalletUnlockStore.js onChange");
        this.setState({
            locked: WalletDb.isLocked()
        });

    }

    onChangeSetting(payload) {
        // console.log("WalletUnlockStore.js onChangeSetting", payload);
        if (payload.setting === "walletLockTimeout") {
            this.walletLockTimeout = payload.value;
            console.log("ИНТЕРВАЛ таймера", this.walletLockTimeout )
            this._clearLockTimeout();
            this._setLockTimeout();
        } else if (payload.setting === "passwordLogin") {
            this.setState({
                passwordLogin: payload.value
            });
        }
    }

    _setLockTimeout() {
        const _this = this;
        // console.log("WalletUnlockStore.js _setLockTimeout");
        this.walletLockTimeout = this._getTimeout();
        console.log("Интервал таймера", this.walletLockTimeout )

         let timer = setInterval(() => {
            if(WalletUnlockActions.getUserAction()) {
                // console.log("пользователь активен");
                this.walletLockTimeout = null;
            } else {
                // console.log("пользователь НЕактивен");
                this.walletLockTimeout =  parseInt(ss.get("lockTimeout", 600), 10);
                clearInterval(timer);
        

                this._clearLockTimeout();
                /* If the timeout is different from zero, auto unlock the wallet using a timeout */
                if (this.walletLockTimeout) {
                        // if( this.walletLockTimeout >= 600) {
                            if( this.walletLockTimeout >= 600) {
                        this.timeoutInfo = setTimeout(() => {
                            ModalActions.show("show_info_session_timout");
                        }, ( this.walletLockTimeout - 30 ) * 1000);
                    }
                    this.timeout = setTimeout(() => {
                        console.log("!WalletDb.isLocked()", !WalletDb.isLocked())
                        console.log("!WalletUnlockActions.getUserAction()", !WalletUnlockActions.getUserAction())
                        console.log("вместе", !WalletUnlockActions.getUserAction())
                        if (!WalletDb.isLocked() && !WalletUnlockActions.getUserAction()) { // если кошелек НЕ заблокирован (пользователь зеленый)
                            console.log(
                                "auto locking after",
                                this.walletLockTimeout,
                                "s",
                                _this
                            );
                            WalletDb.onLock();
                            WalletUnlockActions.unlock();
                            ModalActions.hide("show_info_session_timout");
                        }
                        this._clearLockTimeout();
                    }, this.walletLockTimeout * 1000);
                }
            }
        }, 20000);
    }

    _clearLockTimeout() {
        console.log("ИНТЕРВАЛ", this.walletLockTimeout)
        // console.log("_clearLockTimeout", this );
        if (this.timeout) {
            clearTimeout(this.timeout);
            clearTimeout(this.timeoutInfo);
            this.timeout = null;
            this.timeoutInfo = null;
        }
    }

    _getTimeout() {
        return parseInt(ss.get("lockTimeout", 600), 10);
    }

    onCheckLock() {
        // console.log("WalletUnlockStore.js onCheckLock");
        this.setState({locked: WalletDb.isLocked()});
    }

    onSetTimeOut() {
        // this.walletLockTimeout = this._getTimeout();
        this._setLockTimeout();

    }
}

export default alt.createStore(WalletUnlockStore, "WalletUnlockStore");


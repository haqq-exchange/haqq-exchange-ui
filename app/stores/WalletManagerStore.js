import alt from "alt-instance";
import WalletDb from "stores/WalletDb";
import AccountRefsStore from "stores/AccountRefsStore";
import AccountStore from "stores/AccountStore";
import SettingsStore from "stores/SettingsStore";
import BalanceClaimActiveStore from "stores/BalanceClaimActiveStore";
import CachedPropertyStore from "stores/CachedPropertyStore";
import PrivateKeyActions from "actions/PrivateKeyActions";
import WalletActions from "actions/WalletActions";
import {ChainStore, ChainValidation} from "deexjs";
import BaseStore from "stores/BaseStore";
import iDB from "idb-instance";
import Immutable from "immutable";
import {PrivateKey} from "deexjs";
import Backup from "../lib/common/Backup";
import * as hash from "deexjs/es/ecc/src/hash";
import {cloneDeep} from "lodash";

/**  High-level container for managing multiple wallets.
 */
class WalletManagerStore extends BaseStore {
    constructor() {
        super();
        this.state = this._getInitialState();
        this.bindListeners({
            onRestore: WalletActions.restore,
            onSetWallet: WalletActions.setWallet,
            onSetVirtualWallet: WalletActions.setVirtualWallet,
            onSetBackupDate: WalletActions.setBackupDate,
            onSetBrainkeyBackupDate: WalletActions.setBrainkeyBackupDate,
            onDeleteWallet: WalletActions.deleteWallet,
            onDeleteCurrentWallet: WalletActions.deleteCurrentWallet
        });
        super._export(
            "init",
            "setNewWallet",
            "onDeleteWallet",
            "onDeleteAllWallets"
        );
    }

    _getInitialState() {
        return {
            new_wallet: undefined, // pending restore
            bin_wallet: undefined,
            current_wallet: undefined,
            wallet_names: Immutable.Set()
        };
    }

    /** This will change the current wallet the newly restored wallet. */
    onRestore({wallet_name, wallet_object, password}) {

        let wallet_clone = cloneDeep(wallet_object);
        let password_private = PrivateKey.fromSeed(password);
        let password_pubkey = password_private
            .toPublicKey()
            .toPublicKeyString();
        //console.log("wallet_name, wallet_object, password", wallet_name, wallet_object, password);

        return Backup.encrypt(wallet_object.wallet, password_pubkey).then(result=>{
            // debugger;
            let encrypted_result = {
                encrypted_wallet : result.toString("base64"),
                local_hash : hash.sha256(result).toString("base64")
            };



            // console.log("restore result", encrypted_result);
            // console.log("restore wallet_object wallet_clone", wallet_clone, wallet_object);
            wallet_object.wallet[0] = Object.assign(wallet_object.wallet[0], encrypted_result);

            iDB
                .restore(wallet_name, wallet_object)
                .then(() => {
                    AccountStore.setWallet(wallet_name);
                    return this.onSetWallet({wallet_name});
                })
                .catch(error => {
                    console.error(error);
                    return Promise.reject(error);
                });
        });
    }

    /** This may result in a new wallet name being added, only in this case
        should a <b>create_wallet_password</b> be provided.
    */
    onSetWallet({
        wallet_name = "default",
        create_wallet_password,
        brnkey,
        resolve
    }) {
        // console.log('WalletManagerStore.onSetWallet wallet_name', wallet_name, create_wallet_password)
        const loginWallet = SettingsStore.getSetting("passwordLogin") === "loginWallet";

        var p = new Promise(res => {
            //debugger;
            console.log("!passwordLogin", loginWallet, this.state); 
            console.log("!wallet_name", wallet_name, ChainValidation.is_account_name(wallet_name) ); 
            let hasTestWalletName =  loginWallet
                ? /[^a-zа-я0-9_-]/.test(wallet_name)
                : !ChainValidation.is_account_name(wallet_name);    
            // console.log("!hasTestWalletName", hasTestWalletName) 
            console.log("!wallet_name", wallet_name)
            if (hasTestWalletName || wallet_name === "")
                throw new Error("Invalid wallet name");


            if (this.state.current_wallet === wallet_name ) {
                res();
                return;
            }
            let add = new Promise(res => { res(); });
            let virtual = new Promise(res => { res(); });

            if (!this.state.wallet_names.has(wallet_name)) {
                var wallet_names = this.state.wallet_names.add(wallet_name);
                add = iDB.root.setProperty("wallet_names", wallet_names);
                this.setState({wallet_names});
            }
            // debugger;
            let current = iDB.root.setProperty("current_wallet", wallet_name);

            if( !loginWallet ) {
                virtual = iDB.root.setProperty("virtual_wallet", wallet_name);
            } else {
                virtual = iDB.root.setProperty("bin_wallet", wallet_name);
            }

            res(
                Promise.all([add, current, virtual]).then((result) => {
                    // debugger;
                    // console.log("Promise.all", result);
                    // The database must be closed and re-opened first before the current
                    // application code can initialize its new state.
                    iDB.close();
                    ChainStore.clearCache();
                    BalanceClaimActiveStore.reset();
                    // Stores may reset when loadDbData is called
                    return iDB.init_instance().init_promise.then(() => {

                        // Make sure the database is ready when calling CachedPropertyStore.reset()
                        CachedPropertyStore.reset();
                        return Promise.all([
                            WalletDb.loadDbData(2).then(() =>{
                                // console.log("return AccountStore.loadDbData()")
                                return AccountStore.loadDbData();
                            }),
                            PrivateKeyActions.loadDbData().then(() => {
                                // console.log("PrivateKeyActions.loadDbData")
                                return AccountRefsStore.loadDbData();
                            })
                        ]).then(() => {
                            // Update state here again to make sure listeners re-render
                            //debugger;
                            // console.log("WalletDb.loadDbData()")
                            // console.log("PrivateKeyActions.loadDbData()", create_wallet_password, wallet_name)

                            if (!create_wallet_password) {
                                this.setState({
                                    current_wallet: wallet_name,
                                    bin_wallet: wallet_name,
                                });
                                return;
                            }

                            return WalletDb.onCreateWallet(
                                create_wallet_password,
                                brnkey, //brainkey,
                                true, //unlock
                                wallet_name
                            ).then(() => {
                                this.setState({
                                    current_wallet: wallet_name,
                                    bin_wallet: wallet_name,
                                });
                                Promise.resolve();
                            });
                        });
                    });
                })
            );
        }).catch(error => {
            console.error(error);
            return Promise.reject(error);
        });
        // console.log('create_wallet_password', p);
        if (resolve) resolve(p);
    }

    onSetVirtualWallet({wallet_name = "default", create_wallet_password, brnkey, resolve}) {
        const virtualWallet = () => {
            return WalletDb.createVirtualWallet(
                create_wallet_password,
                brnkey, //brainkey,
                wallet_name
            );
        };
        const p = new Promise(res => {
            if (/[^a-z0-9_-]/.test(wallet_name) || wallet_name === "")
                throw new Error("Invalid wallet name");

            // console.log('onSetVirtualWallet wallet_name', this.state.current_wallet , wallet_name)
            // console.log('onSetVirtualWallet wallet_names', this.state.wallet_names );
            if (this.state.current_wallet === wallet_name) {
                // console.log(">>>>>>>")
                return virtualWallet()
                    .then(({wallet})=>WalletDb.setWallet(wallet))
                    .then(res);
            }

            var add = new Promise(res => res);
            var current = new Promise(res => res);
            if (!this.state.wallet_names.has(wallet_name)) {
                var wallet_names = this.state.wallet_names.add(wallet_name);
                // add = iDB.root.setProperty("wallet_names", wallet_names);
                // console.log("onSetVirtualWallet wallet_names", wallet_names);
                this.setState({wallet_names});
            }
            //var current = iDB.root.setProperty("current_wallet", wallet_name);
            //console.log("onSetVirtualWallet current", current, res );

            res(
                Promise.all([add, current]).then(() => {
                    // console.log("Promise.all")
                    // The database must be closed and re-opened first before the current
                    // application code can initialize its new state.
                    iDB.close();
                    ChainStore.clearCache();
                    BalanceClaimActiveStore.reset();
                    // Stores may reset when loadDbData is called
                    return iDB.init_instance().init_promise.then(() => {

                        // Make sure the database is ready when calling CachedPropertyStore.reset()
                        CachedPropertyStore.reset();
                        return Promise.all([
                            WalletDb.loadDbData(3).then(() =>{
                                // console.log("onSetVirtualWallet return AccountStore.loadDbData()")
                                return AccountStore.loadDbData();
                            }),
                            PrivateKeyActions.loadDbData().then(() => {
                                // console.log("onSetVirtualWallet PrivateKeyActions.loadDbData")
                                return AccountRefsStore.loadDbData();
                            })
                        ]).then(() => {
                            // Update state here again to make sure listeners re-render

                            // console.log("onSetVirtualWallet WalletDb.loadDbData()")
                            //console.log("onSetVirtualWallet PrivateKeyActions.loadDbData()", create_wallet_password)

                            if (!create_wallet_password) {
                                this.setState({current_wallet: wallet_name});
                                return;
                            }

                            return virtualWallet()
                                .then(({wallet})=>WalletDb.setWallet(wallet))
                                .then(() =>
                                    this.setState({current_wallet: wallet_name})
                                );
                        });
                    });
                })
            );
        }).catch(error => {
            console.error(error);
            return Promise.reject(error);
        });
        // console.log('create_wallet_password onSetVirtualWallet', p , resolve);
        if (resolve) resolve(p);
    }



    /** Used by the components during a pending wallet create. */
    setNewWallet(new_wallet) {
        this.setState({new_wallet});
    }

    init() {
        let current_wallet, bin_wallet,wallet_names;
        return iDB.root.getProperty("current_wallet")
            .then(wallet => current_wallet = wallet)
            .then(()=>iDB.root.getProperty("bin_wallet"))
            .then(wallet => bin_wallet = wallet)
            .then(()=>iDB.root.getProperty("wallet_names", []))
            .then(wallet => wallet_names = wallet)
            .then(()=>{
                // console.log("current_wallet, bin_wallet,wallet_names",
                //     current_wallet, bin_wallet,wallet_names);
                this.setState({
                    wallet_names: Immutable.Set(wallet_names),
                    bin_wallet: bin_wallet,
                    wallet_name: current_wallet
                });

                WalletDb.loadDbData(4).then(()=>{
                    if( WalletDb.getWallet() ) {
                        AccountStore.setWallet(current_wallet);
                    }
                });
            });

        /*return iDB.root.getProperty("bin_wallet", []).then(bin_wallet=>{
                return iDB.root.getProperty("wallet_names", [])
                    .then(wallet_names => {
                        console.log("current_wallet", current_wallet)
                        this.setState({
                            wallet_names: Immutable.Set(wallet_names),
                            bin_wallet: bin_wallet,
                            wallet_name: current_wallet
                        });

                        WalletDb.loadDbData(4).then(()=>{
                            if( WalletDb.getWallet() ) {
                                AccountStore.setWallet(current_wallet);
                            }
                        });
                    });
            });*/

    }

    init1() {
        let current_wallet, bin_wallet,wallet_names;
        return iDB.root.getProperty("current_wallet").then(current_wallet => {
            return iDB.root.getProperty("bin_wallet" ).then(bin_wallet=>{
                return iDB.root.getProperty("wallet_names", [])
                    .then(wallet_names => {
                        console.log("current_wallet", current_wallet, bin_wallet, wallet_names)
                        this.setState({
                            wallet_names: Immutable.Set(wallet_names),
                            bin_wallet: bin_wallet,
                            wallet_name: current_wallet
                        });

                        WalletDb.loadDbData(4).then(()=>{
                            if( WalletDb.getWallet() ) {
                                AccountStore.setWallet(current_wallet);
                            }
                        });
                    });
            });
        });
    }

    onDeleteAllWallets() {
        var deletes = [];
        this.state.wallet_names.forEach(wallet_name =>
            deletes.push(this.onDeleteWallet(wallet_name))
        );
        return Promise.all(deletes);
    }

    onDeleteCurrentWallet() {
        return new Promise(resolve => {
            //debugger;
            let database_name;
            let {current_wallet, wallet_names} = this.state;
            if (wallet_names.has(current_wallet)) {
                //throw new Error("Can't delete wallet, does not exist in index");
                iDB.root.setProperty("current_wallet", "");
                iDB.root.setProperty("bin_wallet", "");
                iDB.root.setProperty("wallet_names", []);
                //iDB.root.remove_from_store(iDB.root.getDatabaseName(), "wallet_names");

                database_name = iDB.getDatabaseName(current_wallet);
                iDB.impl.deleteDatabase(database_name);

                this.setState({
                    current_wallet: undefined
                });
            }
            resolve(database_name);
        });
    }

    onDeleteWallet(delete_wallet_name) {
        return new Promise(resolve => {
            var {current_wallet, wallet_names} = this.state;
            if (!wallet_names.has(delete_wallet_name)) {
                throw new Error("Can't delete wallet, does not exist in index");
            }
            wallet_names = wallet_names.delete(delete_wallet_name);
            iDB.root.setProperty("wallet_names", wallet_names);
            if (current_wallet === delete_wallet_name) {
                current_wallet = wallet_names.size
                    ? wallet_names.first()
                    : undefined;
                iDB.root.setProperty("current_wallet", current_wallet);
                if (current_wallet) WalletActions.setWallet(current_wallet);
            }
            this.setState({current_wallet, wallet_names});
            var database_name = iDB.getDatabaseName(delete_wallet_name);
            var req = iDB.impl.deleteDatabase(database_name);
            resolve(database_name);
        });
    }

    onSetBackupDate() {
        WalletDb.setBackupDate();
    }

    onSetBrainkeyBackupDate() {
        WalletDb.setBrainkeyBackupDate();
    }
}

export var WalletManagerStoreWrapped = alt.createStore(
    WalletManagerStore,
    "WalletManagerStore"
);
export default WalletManagerStoreWrapped;

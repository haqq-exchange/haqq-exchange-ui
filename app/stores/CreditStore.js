import alt from "alt-instance";
import CreditActions from "actions/CreditActions";
import Immutable, {fromJS} from "immutable";
import ls from "common/localStorage";
import {settingsAPIs, getSettingApi} from "api/apiConfig";
import BaseStore from "./BaseStore";

import configConst from "config/const";
const STORAGE_KEY = "__deexgraphene__";
let ss = new ls(configConst.STORAGE_KEY);

class CreditStore extends BaseStore{
    constructor() {
        super();

        this.creditStore = Immutable.Map({});

        this.state = {
        };

        this.bindListeners({
            onClear: CreditActions.clearCredit,
            onSetDataCredit: CreditActions.setDataCredit,
            removeParamsState: CreditActions.removeParamsCredit,
        });

        this._export("getCreditStore");


    }

    onClear = () => {
        this.creditStore = this.creditStore.clear();
    };

    onSetDataCredit = (data) => {
        for (let item in data) {
            if( data.hasOwnProperty(item) ) {
                this.creditStore = this.creditStore.set(item, data[item]);
            }
        }
    };

    removeParamsState = params => {
        params.map(param=>{
            this.creditStore = this.creditStore.remove(param);
        })
    };

    getCreditStore = () => {
        return this.creditStore.toJS();
    }
}

export default alt.createStore(CreditStore, "CreditStore");

import Immutable from "immutable";
import assert from "assert";
import ByteBuffer from "bytebuffer";
import bs58 from "bs58";

import AddressIndex from "stores/AddressIndex";
import BaseStore from "stores/BaseStore";

import {Apis} from "deexjs-ws";
import {PrivateKey, PublicKey, brainKey, FetchChain, NumberUtils, key, hash , Aes , TransactionBuilder } from "deexjs";
import { Serializer, ops } from "deexjs/es/serializer";
import iDB from "../idb-instance";
import AccountStore from "./AccountStore";
import WalletDb from "./WalletDb";
import WalletActions from "../actions/WalletActions";
import alt from "../alt-instance";
import idb_helper from "../idb-helper";
import utils from "../lib/common/utils";
import { getAccountId } from "../lib/common/accountHelper";
import {Asset} from "../lib/common/MarketClasses";

let Long = ByteBuffer.Long;
let { toImpliedDecimal } = NumberUtils;
let { stealth_memo_data, stealth_confirmation, blind_transfer, transfer_from_blind , transfer_to_blind } = ops;

class ConfidentialWallet extends BaseStore{
    constructor(){
        super();

        this.bindListeners({
            createBlindAccount: WalletActions.createBlindAccount,
        });
        super._export("init", "setKeyLabel", "getKeys",
            "getLabels", "getPublicKey", "setWalletObject", "blindToTransfer",
            "blindHistory", "getBlindBalances" , "receiveBlindTransfer" ,"transferFromBlind", "blindTransfer",
            "getFeeFromBlind", "getFeeFromAccount", "getConfirmationReceipts" , "deleteKeyLabel"
        );

        this.wallet = {
            wallet_object: Immutable.Map()
        };

        this.state = {
            wallet_names: Immutable.Set(),
            wallet_object: Immutable.Map(),
        };

        this.process_transaction = (id, tr, broadcast, broadcast_confirmed_callback = () => Promise.resolve()) =>
            broadcast_confirmed_callback()
                .then(()=> {
                    console.warn(id , "transferFromBlind process_transaction");
                    console.warn(id , this );


                    return tr.process_transaction(this, null/*signer keys*/, broadcast);
                });

        this.update = update.bind(this);
        this.getSigner = getSigner.bind(this);
        this.fetch_blinded_balances = fetch_blinded_balances.bind(this);
        this.getPubkeys_having_PrivateKey = getPubkeys_having_PrivateKey.bind(this);
        this.blind_transfer_help = blind_transfer_help.bind(this);
        this.send_blind_tr = send_blind_tr.bind(this);

        this.blind_receipts = () => this.getWalletObject().get("blind_receipts", Immutable.List());
        this.commitments = (receipts = this.blind_receipts()) => receipts
            .reduce( (r, receipt) => r.push(receipt.getIn(["data", "commitment"])), Immutable.List());

    }

    init() {
        return iDB.root.getProperty("wallet_names", [])
            .then(wallet_names => {
                this.setState({
                    wallet_names: Immutable.Set(wallet_names)
                });
            });
    }

    getKeys = () => {
        //console.log("this.wallet.wallet_object", this.getWalletObject().toJS());
        return this.getWalletObject().getIn(["keys"], Immutable.Map());
    };

    getLabels = (filter) => {
        //console.log("this.getKeys()", this.getKeys())
        return this.getKeys()
            .reduce( (s, key) =>
                key.has("label") && filter(key)
                    ? s.add(key.get("label"))
                    : s, Immutable.Set()
            );
    };

    getKeyLabel = public_key => {

        //this.assertLogin()
        let key = this.getKeys().get(public_key);
        return key ? key.get("label") : null;
    };

    setWalletObject( wallet_object ){
        // console.log("wallet_object", wallet_object);
        this.wallet.wallet_object = Immutable.fromJS(wallet_object);
    }
    getWalletObject(){
        // console.log("this.wallet.wallet_object", this.wallet.wallet_object);
        return this.wallet.wallet_object;
    }

    /**
     @arg {string} pubkey_or_label
     @return {PublicKey} or null
     */
    getPublicKey = ( pubkey_or_label ) => {
        if( pubkey_or_label.Q )
            return pubkey_or_label;

        try {
            return PublicKey.fromStringOrThrow(pubkey_or_label);
        } catch(e) {}

        let pubkey = this.getKeys().findKey( key => {
            return key.get("label") === pubkey_or_label;
        } );

        if( !pubkey) {
            return null;
        }
        return PublicKey.fromStringOrThrow( pubkey );
    };

    /**
     @arg {PublicKey|string} pubkey_or_label - public key string or object or label
     @return {PrivateKey} or null
     */
    getPrivateKey( pubkey_or_label ) {

        //this.assertLogin()

        if(!pubkey_or_label) {
            return null;
        }

        let keys = this.getKeys();
        let priv = pubkey => {
            let key = keys.get( pubkey );
            if( !key ) {
                return null;
            }

            let wif = key.get("private_wif");
            if( !wif ) {
                return null;
            }
            return PrivateKey.fromWif( wif );
        };

        if( pubkey_or_label.Q ) {
            return priv( pubkey_or_label.toString() );
        }
        try {
            //PublicKey.fromStringOrThrow( pubkey_or_label );
            return priv(pubkey_or_label);
        } catch(error) {
            // slowest operation last
            let pubkey = this.getKeys().findKey( key => key.get("label") === pubkey_or_label );
            return pubkey ? priv( pubkey ) : null;
        }
    }

    deleteKeyLabel(label) {
        let public_key = this.getPublicKey(label);
        return this.update(wallet => {
            wallet = wallet.deleteIn(["keys", public_key.toString()]);
            return wallet;
        });
    }

    setKeyLabel( key, label = null, public_key = null ) {
        if(typeof key === "string") {
            public_key = PublicKey.fromStringOrThrow(key);
        }

        let private_key;
        if(key.d) {
            private_key = key;
        } else {
            if( !public_key ) {
                public_key = key;
            }

        }
        if( !public_key ) {
            public_key = private_key.toPublicKey();
        }

        if(private_key) {
            private_key = private_key.toWif();
        }
        public_key =  public_key.toString();

        console.warn("key, label = null, public_key", label , key, public_key);


        return this.update(wallet => {
            wallet = wallet.updateIn(["keys", public_key], Immutable.Map(),
                key => key.withMutations( key =>{
                    if( label )
                        key.set("label", label);

                    if( private_key )
                        key.set("private_wif", private_key);

                    return key;
                })
            );
            return wallet;
        });

        //return true;
    }

    createBlindAccount({label, brain_key }) {
        return new Promise((resolve, reject) => {
            brain_key = brainKey( brain_key );
            let private_key = PrivateKey.fromSeed( brain_key );
            let public_key = private_key.toPublicKey();

            if( ! this.setKeyLabel(private_key, label, public_key )) {
                reject("label_exists");
            }
            resolve(public_key);
        });
    }

    // transferToBlind
    blindToTransfer( from_account_id , asset_id , to_amounts , broadcast  ){

        let promises = [];

        promises.push(FetchChain("getAccount", from_account_id));
        promises.push(FetchChain("getAsset", asset_id));
        return Promise.all(promises).then(result => {
            let idx = 0;
            let [account, asset] = result;
            if (!account) return Promise.reject("unknown_from_account_id");
            if (!asset) return Promise.reject("unknown_asset_id");

            for( let to_amount of to_amounts) {
                assert(Array.isArray( to_amount ), 'to_amounts parameter should look like: [["alice",1],["bob",1]]');
                assert.equal(typeof to_amount[0], "string", 'to_amounts parameter should look like: [["alice",1],["bob",1]]');
                assert.equal(typeof to_amount[1], "number", 'to_amounts parameter should look like: [["alice",1],["bob",1]]');

                to_amount[1] = toImpliedDecimal(to_amount[1], asset.get("precision"));

                let public_key;

                try {
                    public_key = PublicKey.fromStringOrThrow(to_amount[0]);
                }
                catch(error) {
                    public_key = this.getPublicKey(to_amount[0]);
                }

                assert(public_key, "Unknown to_amounts[" + (idx++) + "][0] (from_key_or_label): " + to_amount[0])
                to_amount[3] = public_key;

            }

            let promises = [];
            let total_amount = 0;
            let blinding_factors = [];
            let bop = {
                fee: {},
                from: account.get("id"),
                outputs: []
            };
            let confirm = {
                outputs: []
            };

            for( let to_amount of to_amounts) {

                let label = to_amount[0];
                let amount = to_amount[1];
                let to_public = to_amount[3];

                let one_time_private = key.get_random_key();
                let secret = one_time_private.get_shared_secret( to_public );
                let child = hash.sha256( secret );
                let nonce = hash.sha256( one_time_private.toBuffer() );
                let blind_factor = hash.sha256( child );

                // allows the wallet to store and decrypt the receipt
                // Store ones own receipts (fails in "blind to blind (external)")
                // this.setKeyLabel(one_time_private)

                blinding_factors.push( blind_factor.toString("hex") );
                total_amount = longAdd(total_amount, amount);

                let out = {}, conf_output;

                let derived_child = to_public.child( child );
                out.owner = {
                    weight_threshold: 1,
                    key_auths: [[ derived_child.toString(), 1 ]],
                    account_auths: [],
                    address_auths: []
                };

                promises.push( Promise.resolve()
                    .then( ()=> {
                        return Apis.instance()
                            .crypto_api()
                            .exec("blind", [blind_factor.toString("hex"),  amount.toString()]);

                    })
                    .then( ret =>{
                        out.commitment = ret;
                    })
                    .then( ()=>
                        to_amounts.length > 1 ?
                            Apis.instance()
                                .crypto_api( "range_proof_sign", [
                                    0, out.commitment, blind_factor.toString("hex"), nonce, 0, 0, amount.toString()
                                ] )
                                .then( res => {
                                    out.range_proof = res;
                                })
                            :
                            out.range_proof = ""
                    )
                    .then(()=> {
                        //debugger;
                        conf_output = {
                            label,
                            pub_key: to_public.toString(),
                            decrypted_memo: {
                                //fee: feeAmount,
                                amount: {
                                    amount: amount.toString(),
                                    asset_id: asset.get("id")
                                },
                                blinding_factor: blind_factor.toString("hex"),
                                commitment: out.commitment.toString("hex"),
                                check: bufferToNumber(secret.slice(0, 4)),
                                from: one_time_private.toPublicKey().toString()
                            },
                            confirmation: {
                                one_time_key: one_time_private.toPublicKey().toString(),
                                to: to_public.toString(),
                                owner: out.owner, // allows wallet save before broadcasting (durable)
                                external_receipt: true,
                            }
                        };
                        let memo = stealth_memo_data.toBuffer( conf_output.decrypted_memo );
                        conf_output.confirmation.encrypted_memo = Aes.fromBuffer(secret).encrypt( memo ).toString("hex");

                        bop.outputs.push( out );
                        confirm.outputs.push( conf_output );
                    })

                );
            }

            return Promise.all(promises).then(()=>{
                let name = account.get("name");
                let p1 = () => this.receiveBlindTransfer( confirm.outputs, "@"+name, "from @"+name );
                return Promise.resolve()

                    .then( ()=> {
                        return Apis
                            .instance()
                            .crypto_api()
                            .exec("blind_sum", [
                                blinding_factors,
                                blinding_factors.length
                            ]);
                    })
                    .then( res => {
                        bop.blinding_factor = res;
                    })
                    .then(()=> {
                        /*
                         * get fee public to private transfer_to_blind
                         * */
                        return this.getFeeFromAccount(asset.get("id"));
                    })
                    .then(_fee => {
                        bop.fee.asset_id = _fee.asset_id;
                        bop.fee.amount = _fee.amount.toString();
                    })
                    .then( ()=>{
                        bop.amount = {
                            amount: total_amount.toString(),
                            asset_id: asset.get("id")
                        };
                        //let last_confrim = confirm.outputs[confirm.outputs.length - 1];
                        /*

                        console.log("last_confrim", last_confrim );
                        let pub_key = this.getPublicKey(account.getIn(["options", "memo_key"]));
                        let private_key = this.getPrivateKey(account.getIn(["options", "memo_key"]));
                        console.log("pub_key", pub_key);
                        console.log("private_key", private_key);*/
                        //let signer = this.getSigner(account.getIn(["options", "memo_key"]), last_confrim.confirmation.one_time_key );
                        // console.log("signer", signer);


                        let tr = new TransactionBuilder();
                        tr.add_type_operation("transfer_to_blind", bop);
                        //tr.add_signer();

                        return this.process_transaction(464, tr, broadcast, p1).then(()=> {
                            confirm.trx = tr.serialize();

                            confirm.confirmation_receipts = confirmation_receipts(confirm.outputs)
                                .reduce( (r, receipt)=>r.push(bs58.encode(stealth_confirmation.toBuffer(receipt))) , Immutable.List()).toJS();
                            console.warn("confirm.confirmation_receipts", confirm.confirmation_receipts);
                            return confirm;
                        });
                    });
            });

        });
    }

    /**
     Transfers funds from a set of blinded balances to a public account balance.

     @arg {string} from_blind_account_key_or_label
     @arg {string} to_account_id_or_name
     @arg {string} amount
     @arg {string} asset_symbol
     @arg {boolean} [broadcast = false]
     @return blind_confirmation
     */
    transferFromBlind( from_blind_account_key_or_label, to_account_id_or_name, amount, asset_symbol, broadcast = false ){
        // this.assertLogin()

        let promises = [];
        promises.push(FetchChain("getAccount", to_account_id_or_name));
        promises.push(FetchChain("getAsset", asset_symbol));

        return Promise.all(promises).then( res =>{

            let [ to_account, asset ] = res
            if( ! to_account ) return Promise.reject("unknown_to_account")
            if( ! asset ) return Promise.reject("unknown_asset")

            amount = toImpliedDecimal(amount, asset.get("precision"))

            let from_blind = {
                inputs: [],
                trx: {
                    operations: []
                }
            };

            let blind_in;

            return Promise.resolve()
                /*
                * transfer private to public
                * */
                .then( ()=> get_blind_transfer_fee(asset.get("id"), 1) )
                .then( fee =>{
                    from_blind.fee = fee;
                    blind_in = longAdd(fee.amount, amount);
                })

                .then( ()=>
                    this.blind_transfer_help(
                        from_blind_account_key_or_label, /* "BTS5h82psxgHzXNvEeDmJqAz2wrUCZ51fvJwARMQLxRcubi9WKvJY ",*/
                        to_account.getIn(["options", "memo_key"]),
                        blind_in, asset.get("symbol"), false, true
                    )
                )
                .then( conf => {

                    assert( conf.outputs.length > 0 );

                    from_blind.to = to_account.get("id");
                    from_blind.amount = { amount: amount.toString(), asset_id: asset.get("id") };
                    let decrypted_memo = conf.outputs[conf.outputs.length - 1].decrypted_memo;

                    from_blind.blinding_factor = decrypted_memo.blinding_factor;
                    from_blind.inputs.push({
                        commitment: decrypted_memo.commitment,
                        owner: authority(),
                        //one_time_key: confirmation.one_time_key
                    });

                    // from_blind.fee  = fees->calculate_fee( from_blind, asset_obj->options.core_exchange_rate );
                    conf.trx.operations.push(from_blind);
                    return conf;
                })

                .then(conf => {

                    let operations = conf.trx.operations;

                    let p1;
                    return this.send_blind_tr(
                        operations,
                        from_blind_account_key_or_label,
                        broadcast,
                        conf.one_time_keys,
                        p1
                    ).then( tr => {
                        return conf;
                    });
                });
        });
    }

    /**

     Used to transfer from one set of blinded balances to another.

     private -> private

     @arg {string} from_key_or_label
     @arg {string} to_key_or_label
     @arg {string} amount
     @arg {string} asset_symbol
     @arg {boolean} [broadcast = false]
     @return blind_confirmation
     */
    blindTransfer( from_key_or_label, to_key_or_label, amount, asset_symbol, broadcast = false) {

        // this.assertLogin()
        let p1 = Promise.resolve(),
            available_amount= Long.ZERO,
            feeAmount,
            has_change = false;
        return Promise.resolve()
            .then( ()=> FetchChain("getAsset", asset_symbol) )
            .then( asset => {
                if( ! asset ) return Promise.reject("unknown_asset");
                amount = toImpliedDecimal(amount, asset.get("precision"));
                return Promise.resolve()
                    .then(()=>this.fetch_blinded_balances(from_key_or_label, (bal, receipt, commitment) =>{
                        available_amount = longAdd(available_amount, receipt.get("amount").get("amount"));
                        return longCmp(available_amount, amount) < 0;
                    }))
                    .then(()=> get_blind_transfer_fee(asset.get("id")) )
                    .then(_fee =>feeAmount=_fee)
                    .then(()=>this.blind_transfer_help(from_key_or_label, to_key_or_label, amount, asset_symbol, broadcast))

                    .then( conf =>{

                        let change = longSub(longSub(available_amount, amount), feeAmount.amount);
                        has_change = longCmp(change, 0) > 0;
                        if( has_change ) {
                            // make sure the receipts are stored first before broadcasting
                            p1 = () => this.receiveBlindTransfer(conf.outputs, from_key_or_label);
                        }

                        return conf;
                    })
                    .then( conf =>{
                        // Their may or may not be a change receipt.  Always return the last receipt
                        let cs = conf.confirmation_receipts;
                        if(cs.length === 2) {
                            conf.change_receipt = cs[0];
                        }

                        conf.confirmation_receipt = cs[ cs.length - 1 ]; // the last one
                        delete conf.confirmation_receipts;
                        return conf;
                    })

                    .then(conf => {

                        let operations = conf.trx.operations;


                        return this.send_blind_tr(
                            operations,
                            from_key_or_label,
                            broadcast,
                            conf.one_time_keys,
                            p1
                        ).then( () => {
                            return conf;
                        });
                    });
            });

    }

    receiveBlindTransfer(  confirmation_receipts, opt_from, opt_memo ){
        let receipt = conf => {
            // This conf.from_key -> opt_from in an array allows the receipts to be batched (this triggers a wallet backup). This should only needed if the wallet wants to store its own receipts sent out to external addresses.
            // Store ones own receipts fails, the "from_secret" can't be calculated
            if( conf.from_key && ! opt_from ) {
                //Caller may store the one_time_key in the wallet and provide the public version here
                opt_from = conf.from_key;
            }

            if(conf.confirmation) {
                conf = conf.confirmation;
            }

            if( ! conf.to) assert( conf.to, "to is required" );

            let result = { conf : Immutable.fromJS(conf).toJS() };

            delete result.conf.owner; // not stored, it is provided via get_blinded_balances(commitment)

            let to_private = this.getPrivateKey( conf.to );

            assert( typeof conf.encrypted_memo === "string", "Expecting HEX string for confirmation_receipt.encrypted_memo")
            assert( typeof conf.one_time_key === "string", "Expecting Public Key string for confirmation_receipt.one_time_key")

            let memo, secret;
            function decr(secr) {
                // console.log('d bufferToNumber', bufferToNumber(secr.slice(0, 4)))
                try {
                    let plain_memo = Aes.fromBuffer(secr).decryptHexToText( conf.encrypted_memo );
                    let m = stealth_memo_data.fromBuffer( plain_memo );

                    if(m.check !== bufferToNumber(secr.slice(0, 4))) {
                        throw new Error("mismatch");
                    }
                    memo = stealth_memo_data.toObject(m);

                    secret = secr;
                    return true;
                } catch(error) {
                    // console.log("wrong key" + (error === " (check mismatch)" ? error : " (parsing error)"))
                    return false;
                }
            }


            let from_private;
            let to_key = this.getPublicKey( conf.to );

            // Turn off parsing error debug, the "check" is inside of the memo so most often we will get debug parsing errors
            // console.log('opt_from,to_key', opt_from, to_key.toString(), !!this.getPrivateKey( opt_from ))
            Serializer.printDebug = false;
            if(
                ! (to_private && decr(to_private.get_shared_secret(conf.one_time_key)) )
                &&
                ! ((from_private = this.getPrivateKey( opt_from )) && to_key && decr(from_private.get_shared_secret(to_key)) )
            ) {
                Serializer.printDebug = true;
                // Both wallets (the cli_wallet and js wallet) do not save receipts coming from the wallet.  They should save receipts that were sent to a spendable key in the wallet.  The code may play it safe and try to save all receipts.  This error may be commented out after beta.
                // console.log("ConfidentialWallet\tINFO Unable to decrypt memo", JSON.stringify(memo_error))
                throw new Error("Unable to decrypt memo, " + (to_private == null && from_private == null ? "missing key" : "wrong key"))
            }

            let child = hash.sha256( secret );
            let child_private = PrivateKey.fromBuffer( child );

            if( to_key ) {
                result.to_key = to_key.toString();
            }

            result.to_label = this.getKeyLabel( result.to_key );
            if( memo.from ) {
                result.from_key = memo.from;
                result.from_label = this.getKeyLabel( result.from_key );
                if( ! result.from_label ) {
                    result.from_label = opt_from;
                    this.wallet.queue_mills = 20*1000;
                    this.setKeyLabel( result.from_key, result.from_label );
                }
            } else {
                result.from_label = opt_from;
            }
            result.amount = memo.amount;
            result.memo = opt_memo;

            let owner = authority({ key_auths: [[ to_key.child(child).toString(), 1 ]] });

            // confirm the amount matches the commitment (verify the blinding factor)
            return Apis.instance()
                .crypto_api()
                .exec("blind", [memo.blinding_factor,  memo.amount.amount])
                .then( commtiment => {
                    return Apis.instance()
                        .crypto_api()
                        .exec("verify_sum", [[commtiment], [memo.commitment], 0])
                        .then( res => {
                            return assert(res, "verify_sum " + res);
                        })
                        .then( () => {

                            result.control_authority = owner;
                            result.data = memo;

                            this.wallet.queue_mills = 20*1000;
                            this.setKeyLabel( child_private );
                            result.date = new Date().toISOString();
                            return result;
                        });
                });
        };

        if( ! Array.isArray( confirmation_receipts ) && ! Immutable.List.isList(confirmation_receipts)) {
            confirmation_receipts = [ confirmation_receipts ];
        }

        let rp = [];
        Immutable.List(confirmation_receipts).forEach( r =>{
            try {
                if( typeof r === "string" ) {
                    r = stealth_confirmation.fromBuffer(new Buffer(bs58.decode(r), "binary"));
                    r = stealth_confirmation.toObject(r);
                }
            } catch (err) {

            }
            rp.push(
                Promise.resolve()
                    .then(()=> receipt(r))
                    .catch( error => {
                        // Do not re-throw unless fatal
                        // All promises must resolve for the wallet to update

                        // If the receipt does not have a private key, the memo will the only and deciding error
                        if( ! /Unable to decrypt memo/.test(error.toString()) ) {
                            throw error;
                        }

                        // external_receipt means: save if the account happens to be in this wallet, don't error if not.
                        if( ! r.confirmation || ! r.confirmation.external_receipt ) throw error;

                        let from_label = this.getKeyLabel( r.decrypted_memo.from ) || opt_from;

                        // The wallet does not want to decrypt external receipts even when the one-time-key is recorded.  This will record them anyways.  This is probably better, decrypting the memo is not required and this saves the over-head of keeping one-time-keys.
                        let external_receipt = {
                            conf: r.confirmation,
                            to_key: r.confirmation.to,
                            to_label: r.label,
                            from_key: r.decrypted_memo.from,
                            from_label,
                            amount: r.decrypted_memo.amount,
                            memo: opt_memo,
                            control_authority: r.confirmation.owner,
                            data: r.decrypted_memo,
                            date: new Date().toISOString(),
                        };
                        return external_receipt;
                    })
            );
        });

        let receipts;
        return Promise.all(rp)
        // Convert to Immutable.
        // Remove nulls (this wallet did not accept the receipt).
            .then( res => {
                receipts = Immutable.List(res)
                    .reduce( (r, el) => el ? r.push(Immutable.fromJS(el)) : r , Immutable.List());
            })
            // Update the wallet
            .then(() => console.log( "receiveBlindTransfer ConfidentialWallet\tINFO saving " + receipts.size + " receipts" ))
            .then(() => this.update(wallet => {
                /* UPDATE  */
                wallet = wallet.update("blind_receipts", Immutable.List(),
                    // append receipts to blind_receipts
                    blind_receipts => receipts.reduce((r, rr) => r ? r.push(rr) : r, blind_receipts)
                );
                return wallet;
            }))
            .then( () => {
                return receipts;
            }).catch((result)=>{
                console.warn("receiveBlindTransfer result", result);
            });
    }

    blindHistory( pubkey_or_label ) {

        // this.assertLogin()

        let blind_receipts = null

        if (pubkey_or_label) {
            let public_key = this.getPublicKey(pubkey_or_label);
            // console.log("public_key", public_key);
            assert(public_key, "missing pubkey_or_label ");
            let pubkey = public_key.toString();
            // console.log("pubkey", pubkey);
            blind_receipts = this.blind_receipts().filter( receipt => receipt.get("from_key") === pubkey || receipt.get("to_key") === pubkey );
            // console.log("blind_receipts", blind_receipts);
        } else {
            blind_receipts = this.blind_receipts();
        }

        return blind_receipts
            .reduce( (r, receipt) => {
                receipt = receipt.update("from_label", label => flipPrefix(label))
                receipt = receipt.update("to_label", label => flipPrefix(label))
                //console.log("-- receipt -->", receipt.toJS());
                return r.push( receipt );
            }, Immutable.List())
            .sort( (a, b) => a.get("date") > b.get("date") );
    }

    getBlindBalances(pubkey_or_label) {
        // this.assertLogin()
        let balances = Immutable.Map().asMutable();
        let p1 = this.fetch_blinded_balances( pubkey_or_label, (bal, receipt)=> {
            let amount = receipt.getIn(["data", "amount"]);
            balances.update(amount.get("asset_id"), 0, amt => longAdd(amt, amount.get("amount")).toString() );
        });
        return p1.then(()=> balances.asImmutable());
    }

    /**
     @arg {number|string} asset_symbol_or_id
     @return {object} like { asset_id: "1.3.0", amount: 2000000 }
     */
    getFeeFromAccount(asset_symbol_or_id = "1.3.0") {
        let f1, f2, asset;
        return Promise.resolve()

            .then(()=> FetchChain("getAsset", asset_symbol_or_id))
            .then(_asset=> asset = _asset)
            .then(()=> console.log("_updateFeeBlind asset", asset))

            .then(()=> get_blind_transfer_fee(asset.get("id"), 2)).then(_fee=> f1 = _fee)
            .then(()=> get_blind_transfer_fee(asset.get("id"), 1)).then(_fee=> f2 = _fee)

            .then(()=> {
                return {
                    asset_id: f1.asset_id,
                    precision: asset.get("precision"),
                    amount: parseInt(f1.amount) + parseInt(f2.amount)
                };
            } );
    }

    /**
     @arg {number|string} asset_symbol_or_id
     @return {object} like { asset_id: "1.3.0", amount: 2000000 }
     */
    getFeeFromBlind(asset_symbol_or_id = "1.3.0") {
        let f1, f2 , f3, asset ;
        return Promise.resolve()

            .then(()=> FetchChain("getAsset", asset_symbol_or_id))
            .then(_asset=> asset = _asset)
            .then(()=> console.log("_updateFeeBlind asset", asset))
            .then(()=> assert(asset, "Asset not found: " + asset_symbol_or_id))

            .then(()=> get_blind_transfer_fee(asset.get("id"), 0 )).then(_fee=> f2 = _fee)
            .then(()=> get_blind_transfer_fee(asset.get("id"), 1 )).then(_fee=> f2 = _fee)
            .then(()=> get_blind_transfer_fee(asset.get("id"), 2 )).then(_fee=> f3 = _fee)

            .then(()=> {
                return {
                    asset_id: f3.asset_id,
                    precision: asset.get("precision"),
                    amount: parseInt(f3.amount) + parseInt(f2.amount)
                };
            });
    }

    /*
    * Array({
    *   confirmation: {
    *       encrypted_memo: "",
    *       external_receipt: "",
    *       one_time_key: ""
    *       owner: {}
    *       to: ""
    *   }
    *   decrypted_memo: {
    *       amount: {
    *          amount,
    *          asset_id,
    *       }
    *       blinding_factor: ""
    *       check: ,
    *       commitment: ""
    *       from: ""
    *   },
    *   label: "",
    *   pub_key: ""
    * })
    * */
    getConfirmationReceipts( receipts ){
        return confirmation_receipts(receipts)
            .reduce( (r, receipt) => r.push(bs58.encode(stealth_confirmation.toBuffer(receipt))) , Immutable.List()).toJS();
    }
}

export var ConfidentialWalletWrapped = alt.createStore(
    ConfidentialWallet,
    "ConfidentialWallet"
);
export default ConfidentialWalletWrapped;

let long = (operation, arg1, arg2) => new Long(arg1)[operation]( new Long(arg2) );
let longAdd = (a, b) => {
    return long("add", a, b);
};
let longMul = (a, b) => long("multiply", a, b);
let longCmp = (a, b) => long("compare", a, b);
let longSub = (a, b) => long("subtract", a, b);
var flipPrefix = label => ! label ? label : /^@/.test(label) ? label.substring(1) : "~" + label;
let isDigits = value => value === "numeric" || /^[0-9]+$/.test(value);

let bufferToNumber = (buf, type = "Uint32") =>
    ByteBuffer.fromBinary(buf.toString("binary"), ByteBuffer.LITTLE_ENDIAN)["read" + type]();

let confirmation_receipts = outputs => Immutable.List(outputs)
    .reduce( (r, out)=>{
        assert(out.confirmation, "confirmation required");
        return r.push(out.confirmation);
    }, Immutable.List());

let authority = (data)  => {
    let weight_threshold = data ? 1 : 0;
    return Immutable.fromJS({
        weight_threshold,
        key_auths: [],
        account_auths: [],
        address_auths: [] }).merge(data).toJS();
};

/** @return {Promise} */
function update(callback) {
    let wallet_object = callback(this.getWalletObject());
    //this.wallet.wallet_object = wallet_object;
    this.setWalletObject(wallet_object.toJS());
    // console.log("updateStateWallet wallet", wallet_object.toJS());
    return WalletDb.updateStateWallet(wallet_object.toJS())
        .catch( error =>{
            console.error("ERROR\tConfidentialWallet\tupdate", error, "stack", error.stack)
            throw error;
        });
}

function getPubkeys_having_PrivateKey( pubkeys, addys = null ) {
    let return_pubkeys = [];
    let keys = this.getKeys();
    if(pubkeys) {
        for(let pubkey of pubkeys) {
            let key = keys.get(pubkey)
            if(key && key.has("private_wif")) {
                return_pubkeys.push(pubkey);
            }
        }
    }
    /*if(addys) {
        for (let addy of addys) {
            let pubkey = AddressIndex.getPubkey(addy);
            return_pubkeys.push(pubkey);
        }
    }*/
    return return_pubkeys;
}

function fetch_blinded_balances(pubkey_or_label, callback) {
    let public_key = this.getPublicKey( pubkey_or_label );
    assert( public_key, "missing pubkey_or_label " + pubkey_or_label);

    let pubkey = public_key.toString();
    let receipts = this.blind_receipts()
        .filter( r => r.get("to_key") === pubkey);
    // .filterNot( r => r.get("used") === true )// just for testing

    let used = Immutable.Set();
    let commitments = this.commitments(receipts);

    let p1 = Apis.instance().db_api().exec("get_blinded_balances", [commitments.toJS()])
        .then( bbal => {

            let bbalMap = Immutable.List(bbal)
                .reduce( (r, bal)=> r.set(bal.commitment, bal), Immutable.Map());

            receipts.forEach( receipt => {
                let commitment = receipt.getIn(["data", "commitment"]);
                // console.log("get_blinded_balances commitment", commitment);
                let bal = bbalMap.get(commitment);
                // console.log("get_blinded_balances bal", bal);

                if( !bal ) {
                    used = used.add(commitment);
                    return;
                }
                // console.log("get_blinded_balances commitment", commitment);
                // console.log("get_blinded_balances bal", bal);
                let ret = callback(bal, receipt, commitment);
                if( ret === false ) {
                    return false;
                }

                // return ret;
            });
        });

    // update "used" in the wallet after the API call finishes
    return p1.then(()=> {

        if( used.size )
            return this.update(wallet => {
                wallet = wallet.update("blind_receipts", receipts =>
                    receipts.reduce( (r, receipt) =>
                        used.has( receipt.getIn(["data", "commitment"]) ) ?
                            r.push(receipt.set("used", true)) : r.push(receipt), Immutable.List()
                    ));
                return wallet;
            });
    });
}

/** Feeds need to be obtained in advance before calculating inputs and outputs. */
function get_blind_transfer_fee(asset_id, output_count = 2) {
    const coreAsset =  "1.3.0";
    let send_asset_id = asset_id;

    /*
    * fee 16.21572
    * 0.428 * 16.21572
    * 0.001608 * 16.21572 = 0,02607487776
    * */

    let pr1 = (fee) => new Promise(resolve => resolve(fee));

    if( asset_id !== coreAsset ) {
        send_asset_id = coreAsset;
        pr1 = (fee) => new Promise(resolve => {
            utils.calculateAsset(coreAsset, asset_id, fee.amount).then(res => {
                resolve({
                    asset_id,
                    amount: parseInt(res.amount)
                });
            });
        });
    }

    const typeTransfer = [
        transfer_from_blind, /* 60785 = 6.0785 deex  */
        transfer_to_blind, /* 81048 = 8.1048 deex */
        blind_transfer /* 750000 = 75.0000 deex */
    ];
    /*
    *
    * blind_transfer:  - private -> private
    * transfer_from_blind:  - public -> private
    * transfer_to_blind:  - private -> public
    * */

    // Create an empty but serilizable operation to send to the server
    // let op = output_count === 0 ? transfer_from_blind : blind_transfer;
    let op = typeTransfer[output_count];

    let object = op.toObject({}, {use_default: true});

    for(let i = 1; i < output_count; i++) {
        object.outputs.push( object.outputs[0] );
    }


    let tr = new TransactionBuilder();
    tr.add_type_operation(op.operation_name, object);
    return tr.set_required_fees(send_asset_id)
        .then(()=>{
            let fee = tr.operations[0][1].fee;
            assert.equal( send_asset_id, fee.asset_id, "expecting fee asset_id to match" );
            return fee;
        }).then( fee => pr1(fee));
}

/**
 Short method to help form and send a blind transfer..

 @arg {string} from_key_or_label
 @arg {string} to_key_or_label
 @arg {string} amount - The implied decimal places amount
 @arg {string} asset_symbol
 @arg {boolean} broadcast
 @arg {boolean} to_temp

 @typedef {object} ret
 @property {object} outputs
 @property {object} outputs.decrypted_memo: {from, amount, blinding_factor, commitment, check}
 @property {object} outputs.confirmation: {one_time_key, to, owner, encrypted_memo}
 @property {string} outputs.label - "alice"
 @property {string} outputs.pub_key: "GPHAbc9Def..."
 @property {object} outputs.auth: {weight_threshold, ...}

 @property {object} fee: {amount, asset_id}
 @property {object} trx: {ref_block_num, ...}
 @property {array<string>} one_time_keys - for signing

 @return {Promise<ret>}
 */
function blind_transfer_help(
    from_key_or_label,
    to_key_or_label,
    amount,
    asset_symbol,
    broadcast = false,
    to_temp = false
) {

    // assert digits (for implied decimal format)
    assert(isDigits(amount), "expecting only digits in amount " + amount); //not prefect, but would catch some invalid calls

    let confirm = {
        outputs: []
    };

    let from_key = this.getPublicKey(from_key_or_label);
    let to_key = this.getPublicKey(to_key_or_label);

    assert(from_key, "missing from_key_or_label: " + from_key_or_label);
    assert(to_key, "missing to_key_or_label: " + to_key_or_label);

    let promises = [];
    promises.push(FetchChain("getAsset", asset_symbol));

    return Promise.all(promises).then( res =>{
        // debugger;
        let [ asset ] = res;
        if( ! asset ) return Promise.reject("unknown_asset");

        let blind_tr = {
            outputs: [],
            inputs: []
        };
        let one_time_keys = Immutable.Set();
        let available_amount = Long.ZERO;
        let blinding_factors = [];

        let amount_with_fee;

        return Promise.resolve()
            .then( ()=> get_blind_transfer_fee(asset.get("id")) )
            .then( fee =>{
                blind_tr.fee = fee;
                confirm.fee = fee;
            })

            .then( ()=>{
                amount_with_fee = longAdd(amount, blind_tr.fee.amount);
            })
            .then( ()=> this.fetch_blinded_balances(from_key_or_label, (bal, receipt, commitment) =>{
                let control_authority = receipt.get("control_authority");
                let one_time_key = receipt.get("conf").get("one_time_key");
                //for signing
                one_time_keys = one_time_keys.add( one_time_key );

                blind_tr.inputs.push({
                    commitment: commitment,
                    owner: control_authority.toJS(),
                    one_time_key: one_time_key
                });
                blinding_factors.push( receipt.get("data").get("blinding_factor") );

                available_amount = longAdd(available_amount, receipt.get("amount").get("amount"));

                // return false to "break"
                // Break if the available_amount total is >= amount_with_fee
                // Being over is fine, change will be provided...
                return longCmp(available_amount, amount) < 0;

            }) )
            .then(()=> {
                //let amount_without_fee = longAdd(amount_with_fee, -blind_tr.fee.amount);
                assert( longCmp(available_amount, amount_with_fee) >= 0,
                    `Insufficent Balance, available ${available_amount.toString()}, transfer amount plus fees ${amount_with_fee.toString()}`);

                // debugger;
                let one_time_private = key.get_random_key();
                let secret = one_time_private.get_shared_secret( to_key );
                let child = hash.sha256( secret );
                let nonce = hash.sha256( one_time_private.toBuffer() );
                let blind_factor;

                // Saving this one_time_key may allow the wallet to decrypt sent receipt memos (this is probably not requied).  Also extra backup overhead here...
                // (did not store in unit test: "blind to blind (external)")
                // this.setKeyLabel( one_time_private )

                let from_secret = one_time_private.get_shared_secret( from_key );
                let from_child = hash.sha256( from_secret );
                let from_nonce = hash.sha256( nonce );
                let change = longSub(longSub(available_amount, amount), blind_tr.fee.amount);
                let change_blind_factor;

                let has_change = longCmp(change, 0) > 0;

                let bf_promise;
                //console.log("has_change", has_change);
                if( has_change ) {
                    blind_factor = hash.sha256( child );
                    blinding_factors.push( blind_factor.toString("hex") );
                    bf_promise = Apis.instance()
                        .crypto_api()
                        .exec("blind_sum", [blinding_factors, blinding_factors.length - 1])
                        .then( bf => {
                            change_blind_factor = bf;
                        } );
                } else {
                    bf_promise = Apis.instance()
                        .crypto_api()
                        .exec("blind_sum", [blinding_factors, blinding_factors.length] )
                        .then( bf => blind_factor = bf )
                        .then( () => blinding_factors.push( blind_factor.toString("hex") ));
                }


                return bf_promise.then(()=> {
                    // debugger;
                    let to_out = { };

                    let derived_child = to_key.child( child );
                    to_out.owner = to_temp ? authority() :
                        authority({ key_auths: [[ derived_child.toString(), 1 ]] });

                    return Promise.resolve()
                        .then( ()=> Apis.instance()
                            .crypto_api()
                            .exec("blind", [blind_factor.toString("hex"), amount.toString()]))
                        .then( ret => {
                            to_out.commitment = ret;
                        })

                        .then( ()=>{
                            // debugger;

                            let rp_promise;

                            if( ! has_change ) {

                                rp_promise = Promise.resolve();
                                to_out.range_proof = "";

                            } else {
                                // debugger;
                                let change_out = {};
                                let derived_child = from_key.child( from_child );
                                change_out.owner = authority({ key_auths: [[ derived_child.toString(), 1 ]] });

                                rp_promise = Apis.instance()
                                    .crypto_api()
                                    .exec("range_proof_sign",
                                        [0, to_out.commitment, blind_factor.toString("hex"), nonce.toString("hex"), 0, 0, amount.toString()]
                                    )
                                    .then( res => to_out.range_proof = res)

                                    .then( ()=> Apis.instance()
                                        .crypto_api()
                                        .exec("blind", [change_blind_factor, change.toString()]) )
                                    .then( res => {
                                        change_out.commitment = res;
                                    })

                                    .then( ()=> Apis.instance()
                                        .crypto_api()
                                        .exec("range_proof_sign",
                                            [0, change_out.commitment, change_blind_factor, from_nonce.toString("hex"), 0, 0, change.toString()]
                                        ))
                                    .then( res => {
                                        change_out.range_proof = res;
                                    })

                                    .then( ()=>{
                                        blind_tr.outputs[1] = change_out;

                                        let conf_output = {
                                            decrypted_memo: {},
                                            confirmation: {}
                                        };

                                        conf_output.label = from_key_or_label;
                                        conf_output.pub_key = from_key.toString();
                                        // conf_output.from_key = one_time_private.toPublicKey().toString()//trying to save "from blind" receipt
                                        conf_output.decrypted_memo.from = from_key.toString();
                                        conf_output.decrypted_memo.amount = { amount: change.toString(), asset_id: asset.get("id") };
                                        conf_output.decrypted_memo.blinding_factor = change_blind_factor.toString("hex");
                                        conf_output.decrypted_memo.commitment = change_out.commitment.toString("hex");
                                        conf_output.decrypted_memo.check = bufferToNumber(from_secret.slice(0, 4));
                                        conf_output.confirmation.one_time_key = one_time_private.toPublicKey().toString();
                                        conf_output.confirmation.to = from_key.toString();
                                        conf_output.confirmation.owner = change_out.owner;
                                        // console.log('b bufferToNumber', bufferToNumber(from_secret.slice(0, 4)))

                                        let from_aes = Aes.fromBuffer(from_secret);
                                        let memo = stealth_memo_data.toBuffer( conf_output.decrypted_memo );
                                        conf_output.confirmation.encrypted_memo = from_aes.encrypt( memo ).toString("hex");

                                        conf_output.auth = change_out.owner;
                                        conf_output.confirmation_receipt = conf_output.confirmation;

                                        confirm.outputs.push( conf_output );
                                    });
                            }

                            return rp_promise.then(()=>{
                                // debugger;
                                blind_tr.outputs[0] = to_out;

                                let conf_output = {
                                    decrypted_memo: {},
                                    confirmation: {}
                                };

                                conf_output.label = to_key_or_label;
                                conf_output.pub_key = to_key.toString();
                                conf_output.decrypted_memo.from = from_key.toString();
                                conf_output.decrypted_memo.amount = { amount: amount.toString(), asset_id: asset.get("id") };
                                conf_output.decrypted_memo.blinding_factor = blind_factor.toString("hex");
                                conf_output.decrypted_memo.commitment = to_out.commitment.toString("hex");
                                conf_output.decrypted_memo.check = bufferToNumber(secret.slice(0, 4));
                                // console.log('2 bufferToNumber', bufferToNumber(from_secret.slice(0, 4)))
                                conf_output.confirmation.one_time_key = one_time_private.toPublicKey().toString();
                                conf_output.confirmation.to = to_key.toString();
                                conf_output.confirmation.owner = to_out.owner;
                                conf_output.confirmation.external_receipt = true;

                                let aes = Aes.fromBuffer(secret);
                                let memo = stealth_memo_data.toBuffer( conf_output.decrypted_memo );
                                conf_output.confirmation.encrypted_memo = aes.encrypt( memo ).toString("hex");

                                conf_output.auth = to_out.owner;
                                // conf_output.confirmation_receipt = conf_output.confirmation

                                // transferFromBlind needs to_out as last
                                confirm.outputs.push( conf_output );

                                let p1;
                                if( ! to_temp || has_change ) {
                                    // make sure the receipts are stored first before broadcasting
                                    p1 = ()=> this.receiveBlindTransfer(confirm.outputs, from_key_or_label);
                                }

                                return this.send_blind_tr([blind_tr], from_key_or_label, broadcast, null, p1, !to_temp || !has_change)
                                    .then( tr => {
                                        confirm.trx = tr;
                                        confirm.one_time_keys = one_time_keys.toJS();
                                        console.warn("confirm.outputs", confirm.outputs);
                                        confirm.confirmation_receipts = confirmation_receipts(confirm.outputs)
                                            .reduce( (r, receipt)=> r.push(bs58.encode(stealth_confirmation.toBuffer(receipt))) , Immutable.List()).toJS();
                                        console.warn("confirm.confirmation_receipts", confirm.confirmation_receipts);
                                        return confirm;
                                    });
                            });
                        });
                });
            });
    });
}

function  getSigner(from_key_or_label, one_time_key) {
    let signer;
    if( !signer ) {
        if( typeof from_key_or_label === "string") {
            from_key_or_label =  this.getPublicKey(from_key_or_label);
        }
        signer = this.getPrivateKey(from_key_or_label.toPublicKeyString());
    }

    if( !signer )
        throw new Error("Missing private key for: " + from_key_or_label);
    // debugger;
    let secret = signer.get_shared_secret( one_time_key );
    let child = hash.sha256( secret );
    return signer.child(child);
}

function send_blind_tr(ops, from_key_or_label, broadcast, one_time_keys, broadcast_confirmed_callback, to_temp = false) {

    let tr = new TransactionBuilder();
    // debugger
    for(let op of ops) {
        if( Array.isArray(op) ) {
            op = op[1];
        }

        assert(op.inputs, "inputs are required");
        let op_name = op.outputs ? "blind_transfer" : "transfer_from_blind";
        tr.add_type_operation(op_name, op)
        {
            let signer;
            let sign = one_time_key => {
                if( !signer ) {
                    if( typeof from_key_or_label === "string") {
                        from_key_or_label =  this.getPublicKey(from_key_or_label);
                    }
                    signer = this.getPrivateKey(from_key_or_label.toPublicKeyString());
                }

                if( !signer )
                    throw new Error("Missing private key for: " + from_key_or_label);
                // debugger;
                let secret = signer.get_shared_secret( one_time_key );
                let child = hash.sha256( secret );
                tr.add_signer(signer.child(child));
            };

            for(let input of op.inputs) {
                if( input.one_time_key ) {
                    sign( input.one_time_key );
                }
            }

            if( one_time_keys ) {
                for(let one_time_key of one_time_keys) {
                    sign( one_time_key );
                }
            }
        }
    }
    if(to_temp) {
        // don't trigger the confirmation dialog
        return tr.process_transaction(this, null/*signer keys*/, false/*broadcast*/).then(()=> tr);
    } else {
        // confirmation dialog
        return this.process_transaction(1619, tr, broadcast, broadcast_confirmed_callback)
            .then(()=> tr);
    }

}

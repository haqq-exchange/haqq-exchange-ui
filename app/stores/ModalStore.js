import alt from "alt-instance";
import ModalActions from "actions/ModalActions";
import WalletUnlockActions from "actions/WalletUnlockActions";
import BaseStore from "stores/BaseStore";

// import ls from "common/localStorage";
// const STORAGE_KEY = "__deexgraphene__";
// let ss = new ls(STORAGE_KEY);

class ModalStore extends BaseStore {
    constructor() {
        super();

        this.bindListeners({
            onShow: ModalActions.show,
            onHide: ModalActions.hide,
            onToggle: ModalActions.toggle,
            onCancel: WalletUnlockActions.cancel,
        });

        // can't use settings store due to possible initialization race conditions
        //const storedSettings = ss.get("settings_modal", {});
        this.state = {
            modals: {},
            data: {},
            modalIndex: {}
        };

    }

    onShow({modalId, resolve, reject, data }) {
        
        return this.changeState(modalId, true, data)
            .then(resolve)
            .catch(reject);
    }

    onHide({modalId, resolve, reject}) {
        //DEBUG
        
        return this.changeState(modalId, false)
            .then(resolve)
            .catch(reject);

    }

    onCancel() {
        const {resolve, modals, modalIndex, data} = this.state;
        if( resolve ) {
            Object.keys(modals).map(modalId => {
                if( modals[modalId] ) {
                    resolve();
                    delete modalIndex[modalId];
                    this.setState({
                        resolve: null,
                        reject: null,
                        modalIndex,
                        modals: Object.assign(modals, {[modalId]: false}),
                        data: Object.assign(data, {[modalId]: null})
                    });
                }
            });
        }
        


    }

    onToggle() {
        

    }

    changeState(modalId, isActive, dataState){
        const _this = this;
        if( modalId ) {
            let {modals, modalIndex, data} = this.state;
            return new Promise((resolve,reject)=>{
                //DEBUG
                let zindex = 0;
                let promise = new Promise(resolve=>resolve());
                if( !modalIndex[modalId] ) {
                    promise = new Promise(resolve=>{
                        zindex = 990 + Object.values(modals).reduce((a,b) => {
                            if( b ) {
                                return a + b;
                            }
                            return 0;
                        }, 1 );
                        modalIndex[modalId] = zindex.toString();
                        resolve();
                    });
                }

                promise.then(()=>{
                    _this.setState({
                        resolve,
                        reject,
                        modalIndex,
                        modals: Object.assign(modals, {[modalId]: isActive}),
                        data: Object.assign(data, {[modalId]: dataState})
                    });
                });
            });
        } else {
            return new Promise((resolve, reject)=>{
                reject("Not found modalId");
            });
        }
    }

}

export default alt.createStore(ModalStore, "ModalStore");

import alt from "alt-instance";
import BaseStore from "stores/BaseStore";
import Backup from "common/Backup";

import iDB from "idb-instance";
import idb_helper from "idb-helper";
import {cloneDeep} from "lodash";

import PrivateKeyStore from "stores/PrivateKeyStore";
import SettingsStore from "stores/SettingsStore";
import {WalletTcomb} from "./tcomb_structs";
import TransactionConfirmActions from "actions/TransactionConfirmActions";
import WalletUnlockActions from "actions/WalletUnlockActions";
import PrivateKeyActions from "actions/PrivateKeyActions";
import AccountActions from "actions/AccountActions";
import {ChainStore, PrivateKey, key, Aes, FetchChain, hash} from "deexjs";
import {Apis, ChainConfig} from "deexjs-ws";
import AddressIndex from "stores/AddressIndex";
import SettingsActions from "actions/SettingsActions";
import notify from "actions/NotificationActions";
import counterpart from "counterpart";
import Immutable from "immutable";
import ConfidentialWallet from "./ConfidentialWallet";
import IndexedDbPersistence from "./IndexedDbPersistence";
import IntlStore from "./IntlStore";


let aes_private = null;
let _passwordKey = null;
// let transaction;

let TRACE = false;

let dictJson, AesWorker;


/** Represents a single wallet and related indexedDb database operations. */
class WalletDb extends BaseStore {
    constructor() {
        super();
        this.state = {
            wallet: null,
            saving_keys: false
        };
        // Confirm only works when there is a UI (this is for mocha unit tests)
        this.confirm_transactions = true;
        ChainStore.subscribe(this.checkNextGeneratedKey.bind(this));
        this.generateNextKey_pubcache = [];
        this.subscribers = Immutable.Set();
        // WalletDb use to be a plan old javascript class (not an Alt store) so
        // for now many methods need to be exported...
        this._export(
            "checkNextGeneratedKey",
            "getWallet",
            "setWallet",
            "login",
            "onLock",
            "isLocked",
            "decryptTcomb_PrivateKey",
            "getPrivateKey",
            "process_transaction",
            "transaction_update",
            "transaction_update_keys",
            "getBrainKey",
            "getBrainKeyPrivate",
            "onCreateWallet",
            "createVirtualWallet",
            "validatePassword",
            "changePassword",
            "generateNextKey",
            "incrementBrainKeySequence",
            "saveKeys",
            "saveKey",

            "addPasswordKey",
            "setWalletModified",
            "setBackupDate",
            "setBrainkeyBackupDate",
            "updateStateWallet",
            "_updateWallet",
            "loadDbData",
            "importKeysWorker",
            "resetBrainKeySequence",
            "decrementBrainKeySequence",
            "generateKeyFromPassword",
            "subscribe",
            "unsubscribe",

            "setConfirmTransaction"
        );
        this.generatingKey = false;
        this.notifyWallet = notifyWallet.bind(this);
        this.setWallet = this.setWallet.bind(this);
    }

    subscribe( callback ) {
        if(this.subscribers.has(callback)) {
            return;
        }
        this.subscribers = this.subscribers.add(callback);
    }

    unsubscribe( callback ) {
        if( ! this.subscribers.has(callback)) {
            return;
        }
        this.subscribers = this.subscribers.remove( callback );
    }

    /** Discover derived keys that are not in this wallet */
    checkNextGeneratedKey() {
        if (!this.state.wallet) return;
        if (!aes_private) return; // locked
        if (!this.state.wallet.encrypted_brainkey) return; // no brainkey
        if (
            this.chainstore_account_ids_by_key === ChainStore.account_ids_by_key
        )
            return; // no change
        this.chainstore_account_ids_by_key = ChainStore.account_ids_by_key;
        // Helps to ensure we are looking at an un-used key
        try {
            this.generateNextKey(false /*save*/);
        } catch (e) {
            
        }
    }

    getWallet() {
        //
        return this.state.wallet;
    }

    updateStateWallet(nextState) {

        return new Promise(resolve => {
            this.setState({
                wallet: !nextState ? {} : Object.assign(this.getWallet() || {}, nextState)
            });

            this._updateWallet();
            resolve();
        });
    }

    onLock() {
        //
        _passwordKey = null;
        aes_private = null;
    }

    isLocked() {
        return !(!!aes_private || !!_passwordKey);
    }

    addPasswordKey = (keys) => {
        if(!_passwordKey) _passwordKey = {};
        _passwordKey[keys.pubKey] = keys.privKey;
        console.log('_passwordKey: ', _passwordKey);
        
    }

    decryptTcomb_PrivateKey(private_key_tcomb) {
        if (!private_key_tcomb) return null;
        if (this.isLocked()) throw new Error("wallet locked");
        if (_passwordKey && _passwordKey[private_key_tcomb.pubkey]) {
            return _passwordKey[private_key_tcomb.pubkey];
        }
        let private_key_hex = aes_private.decryptHex(
            private_key_tcomb.encrypted_key
        );
        return PrivateKey.fromBuffer(new Buffer(private_key_hex, "hex"));
    }

    /** @return ecc/PrivateKey or null */
    getPrivateKey(public_key) {
        console.log('public_key: ', public_key);

        if (_passwordKey && _passwordKey[public_key]) return _passwordKey[public_key];
        if (!public_key) return null;
        if (public_key.Q) public_key = public_key.toPublicKeyString();
        let private_key_tcomb = PrivateKeyStore.getTcomb_byPubkey(public_key);
        console.log('private_key_tcomb: ', private_key_tcomb);
        if (!private_key_tcomb) return null;
        return this.decryptTcomb_PrivateKey(private_key_tcomb);
    }

    process_transaction(tr, signer_pubkeys, broadcast, extra_keys = []) {
        
        
        const passwordLogin = SettingsStore.getState().settings.get(
            "passwordLogin"
        );
        

        if (
            !passwordLogin && this.state.wallet &&
            Apis.instance().chain_id !== this.state.wallet.chain_id
        ) {
            
            return Promise.reject(
                "Mismatched chain_id; expecting " +
                    this.state.wallet.chain_id +
                    ", but got " +
                    Apis.instance().chain_id
            );
        }

        return WalletUnlockActions.unlock()
            .then(() => {
                
                AccountActions.tryToSetCurrentAccount();
                return Promise.all([
                    tr.set_required_fees(),
                    tr.update_head_block()
                ]).then(() => {

                    let signer_pubkeys_added = {};
                    
                    if (signer_pubkeys) {
                        // Balance claims are by address, only the private
                        // key holder can know about these additional
                        // potential keys.
                        let pubkeys = PrivateKeyStore.getPubkeys_having_PrivateKey(
                            signer_pubkeys
                        );
                        if (!pubkeys.length)
                            throw new Error("Missing signing key");

                        for (let pubkey_string of pubkeys) {
                            let private_key = this.getPrivateKey(pubkey_string);
                            tr.add_signer(private_key, pubkey_string);
                            signer_pubkeys_added[pubkey_string] = true;
                        }
                    }

                    return tr
                        .get_potential_signatures()
                        .then(({pubkeys, addys}) => {
                            
                            let my_pubkeys = PrivateKeyStore.getPubkeys_having_PrivateKey(
                                pubkeys.concat(extra_keys),
                                addys
                            );

                            //{//Testing only, don't send All public keys!
                            //    let pubkeys_all = PrivateKeyStore.getPubkeys() // All public keys
                            //    tr.get_required_signatures(pubkeys_all).then( required_pubkey_strings =>
                            //        
                            //    tr.get_required_signatures(my_pubkeys).then( required_pubkey_strings =>
                            //        
                            //}
                            return tr
                                .get_required_signatures(my_pubkeys)
                                .then(required_pubkeys => {
                                    for (let pubkey_string of required_pubkeys) {
                                        if (signer_pubkeys_added[pubkey_string])
                                            continue;
                                        let private_key = this.getPrivateKey(
                                            pubkey_string
                                        );
                                        if (!private_key)
                                            // This should not happen, get_required_signatures will only
                                            // returned keys from my_pubkeys
                                            throw new Error(
                                                "Missing signing key for " +
                                                    pubkey_string
                                            );
                                        tr.add_signer(
                                            private_key,
                                            pubkey_string
                                        );
                                    }
                                });
                        })
                        .then(() => {
                            if (broadcast) {
                                if (this.confirm_transactions) {
                                    let p = new Promise((resolve, reject) => {
                                        TransactionConfirmActions.confirm(
                                            tr,
                                            resolve,
                                            reject
                                        );
                                    });
                                    return p;
                                } else return tr.broadcast();
                            } else return tr.serialize();
                        });
                });
            })
            .catch(() => {});
    }

    transaction_update() {

        let transaction = iDB.instance()
            .db()
            .transaction(["wallet"], "readwrite");
        return transaction;
    }

    transaction_update_keys() {
        let transaction = iDB
            .instance()
            .db()
            .transaction(["wallet", "private_keys"], "readwrite");
        return transaction;
    }

    getBrainKey() {
        let wallet = this.getWallet();
        //
        if (!wallet.encrypted_brainkey) throw new Error("missing brainkey");
        if (!aes_private) throw new Error("wallet locked");
        let brainkey_plaintext = aes_private.decryptHexToText(
            wallet.encrypted_brainkey
        );
        return brainkey_plaintext;
    }

    getBrainKeyPrivate(brainkey_plaintext = this.getBrainKey()) {
        if (!brainkey_plaintext) throw new Error("missing brainkey");
        return PrivateKey.fromSeed(key.normalize_brainKey(brainkey_plaintext));
    }

    getDictionary() {
        return fetch(`${__BASE_URL__}/dictionary.json`)
            .then(res => res.json())
            .then(res => {
                return res;
            })
            .catch(err => {
                // 
            });
    }

    createVirtualWallet(password_plaintext, brainkey_plaintext, public_name = "default"){
        //
        return new Promise((resolve) => {
            this.getDictionary().then(dictionary=>{
                //

                // let dictionary = IntlStore.getState().localesObject;
                let brainkey_backup_date;
                if (brainkey_plaintext) {
                    if (typeof brainkey_plaintext !== "string")
                        throw new Error("Brainkey must be a string");

                    if (brainkey_plaintext.trim() === "")
                        throw new Error("Brainkey can not be an empty string");

                    if (brainkey_plaintext.length < 50)
                        throw new Error(
                            "Brainkey must be at least 50 characters long"
                        );

                    // The user just provided the Brainkey so this avoids
                    // bugging them to back it up again.
                    brainkey_backup_date = new Date();
                }
                let password_aes = Aes.fromSeed(password_plaintext);

                let encryption_buffer = key.get_random_key().toBuffer();
                // encryption_key is the global encryption key (does not change even if the passsword changes)
                let encryption_key = password_aes.encryptToHex(
                    encryption_buffer
                );
                // If unlocking, local_aes_private will become the global aes_private object
                let local_aes_private = Aes.fromSeed(encryption_buffer);

                if (!brainkey_plaintext)
                    brainkey_plaintext = key.suggest_brain_key(dictionary.en);
                else
                    brainkey_plaintext = key.normalize_brainKey(
                        brainkey_plaintext
                    );
                let brainkey_private = this.getBrainKeyPrivate(
                    brainkey_plaintext
                );
                let brainkey_pubkey = brainkey_private
                    .toPublicKey()
                    .toPublicKeyString();
                let encrypted_brainkey = local_aes_private.encryptToHex(
                    brainkey_plaintext
                );

                let password_private = PrivateKey.fromSeed(password_plaintext);
                let password_pubkey = password_private
                    .toPublicKey()
                    .toPublicKeyString();

                let wallet = {
                    public_name,
                    password_pubkey,
                    encryption_key,
                    encrypted_brainkey,
                    brainkey_pubkey,
                    brainkey_sequence: 0,
                    brainkey_backup_date,
                    created: new Date(),
                    last_modified: new Date(),
                    chain_id: Apis.instance().chain_id
                };
                Backup.encrypt(wallet, password_pubkey).then(result=> {
                    let encrypted_result = {
                        encrypted_wallet: result.toString("base64"),
                        local_hash: hash.sha256(result).toString("base64")
                    };
                    wallet = Object.assign(wallet, encrypted_result);
                    aes_private = local_aes_private;
                    resolve({
                        encrypted_result,
                        local_aes_private,
                        wallet
                    });
                });
            });
        });
    }

    onCreateWallet(
        password_plaintext,
        brainkey_plaintext,
        unlock = false,
        public_name = "default"
    ) {
        let walletCreateFct = (dect) => {
            return new Promise((resolve, reject) => {
                if (typeof password_plaintext !== "string")
                    throw new Error("password string is required");

                this.createVirtualWallet(password_plaintext, brainkey_plaintext, public_name)
                    .then(({wallet, encrypted_result, local_aes_private})=>{

                        /*console.log("onCreateWallet wallet, encrypted_result, local_aes_private",
                            wallet,
                            encrypted_result,
                            local_aes_private
                        );*/
                        // 

                        WalletTcomb(wallet); // validation
                        //


                        let transaction = this.transaction_update();
                        let add = idb_helper.add(
                            transaction.objectStore("wallet"),
                            wallet
                        );
                        //
                        let end = idb_helper
                            .on_transaction_end(transaction)
                            .then(() => {
                                //
                                this.state.wallet = wallet;
                                this.setState({wallet});
                                if (unlock) {
                                    aes_private = local_aes_private;
                                    WalletUnlockActions.unlock().catch(() => {});
                                }
                            });
                        //
                        Promise.all([add, end])
                            .then(() => {
                                resolve();
                            })
                            .catch(err => {
                                reject(err);
                            });

                    });

            });
        };

        if (__ELECTRON__) {
            return walletCreateFct(dictJson);
        } else {
            let dictionaryPromise = brainkey_plaintext
                ? null
                : fetch(`${__BASE_URL__}/dictionary.json`);
            return Promise.all([dictionaryPromise])
                .then(res => {
                    return brainkey_plaintext
                        ? walletCreateFct(null)
                        : res[0].json().then(walletCreateFct);
                })
                .catch(err => {
                    
                });
        }
    }

    onCreateWallet111(
        password_plaintext,
        brainkey_plaintext,
        unlock = false,
        public_name = "default"
    ) {
        //
        let walletCreateFct = dictionary => {
            return new Promise((resolve, reject) => {
                if (typeof password_plaintext !== "string")
                    throw new Error("password string is required");

                let brainkey_backup_date;
                if (brainkey_plaintext) {
                    if (typeof brainkey_plaintext !== "string")
                        throw new Error("Brainkey must be a string");

                    if (brainkey_plaintext.trim() === "")
                        throw new Error("Brainkey can not be an empty string");

                    if (brainkey_plaintext.length < 50)
                        throw new Error(
                            "Brainkey must be at least 50 characters long"
                        );

                    // The user just provided the Brainkey so this avoids
                    // bugging them to back it up again.
                    brainkey_backup_date = new Date();
                }
                let password_aes = Aes.fromSeed(password_plaintext);

                let encryption_buffer = key.get_random_key().toBuffer();
                // encryption_key is the global encryption key (does not change even if the passsword changes)
                let encryption_key = password_aes.encryptToHex(
                    encryption_buffer
                );
                // If unlocking, local_aes_private will become the global aes_private object
                let local_aes_private = Aes.fromSeed(encryption_buffer);

                if (!brainkey_plaintext)
                    brainkey_plaintext = key.suggest_brain_key(dictionary.en);
                else
                    brainkey_plaintext = key.normalize_brainKey(
                        brainkey_plaintext
                    );
                let brainkey_private = this.getBrainKeyPrivate(
                    brainkey_plaintext
                );
                let brainkey_pubkey = brainkey_private
                    .toPublicKey()
                    .toPublicKeyString();
                let encrypted_brainkey = local_aes_private.encryptToHex(
                    brainkey_plaintext
                );

                let password_private = PrivateKey.fromSeed(password_plaintext);
                let password_pubkey = password_private
                    .toPublicKey()
                    .toPublicKeyString();

                let wallet = {
                    public_name,
                    password_pubkey,
                    encryption_key,
                    encrypted_brainkey,
                    brainkey_pubkey,
                    brainkey_sequence: 0,
                    brainkey_backup_date,
                    created: new Date(),
                    last_modified: new Date(),
                    chain_id: Apis.instance().chain_id
                };

                Backup.encrypt(wallet, password_pubkey).then(result=>{
                    let encrypted_result = {
                        encrypted_wallet : result.toString("base64"),
                        local_hash : hash.sha256(result).toString("base64")
                    };
                    wallet = Object.assign(wallet, encrypted_result);

                    //
                    // 
                    WalletTcomb(wallet); // validation
                    let transaction = this.transaction_update();
                    let add = idb_helper.add(
                        transaction.objectStore("wallet"),
                        wallet //encrypted_result
                    );
                    let end = idb_helper
                        .on_transaction_end(transaction)
                        .then(() => {
                            this.state.wallet = wallet;
                            this.setState({wallet});
                            if (unlock) {
                                aes_private = local_aes_private;
                                WalletUnlockActions.unlock().catch(() => {});
                            }
                        });
                    Promise.all([add, end])
                        .then(() => {
                            resolve();
                        })
                        .catch(err => {
                            reject(err);
                        });

                });

            });
        };

        if (__ELECTRON__) {
            return walletCreateFct(dictJson);
        } else {
            let dictionaryPromise = brainkey_plaintext
                ? null
                : fetch(`${__BASE_URL__}/dictionary.json`);
            return Promise.all([dictionaryPromise])
                .then(res => {
                    return brainkey_plaintext
                        ? walletCreateFct(null)
                        : res[0].json().then(walletCreateFct);
                })
                .catch(err => {
                    
                });
        }
    }

    generateKeyFromPassword(accountName, role, password) {
        let seed = accountName + role + password;
        let privKey = PrivateKey.fromSeed(seed);
        let pubKey = privKey.toPublicKey().toString();

        return {privKey, pubKey};
    }
    

    /** This also serves as 'unlock' */
    validatePassword(
        password,
        unlock = false,
        account = null,
        roles = ["memo", "owner", "active"]
    ) {

        return new Promise((resolve, _reject)=>{

            // debugger;

            let password_private = PrivateKey.fromSeed(password);
            if (account) {
                // 
                let id = 0;
                const setKey = (role, priv, pub) => {
                    if (!_passwordKey) _passwordKey = {};
                    _passwordKey[pub] = priv;
                    ConfidentialWallet.setKeyLabel(priv, null, pub);

                    id++;
                    PrivateKeyStore.setPasswordLoginKey({
                        pubkey: pub,
                        import_account_names: [account],
                        encrypted_key: null,
                        id,
                        brainkey_sequence: null
                    });
                };

                /* Check if the user tried to login with a private key */
                let fromWif;
                try {
                    fromWif = PrivateKey.fromWif(password);
                } catch (err) { }

                // let acc = ChainStore.getAccount(account, false);

                return FetchChain("getAccount", account).then(acc => {
                    // 
                    let keyData;
                    if (fromWif) {
                        keyData = {
                            privKey: fromWif,
                            pubKey: fromWif.toPublicKey().toString()
                        };
                    }

                    //
                    // 
                    // 
                    // 
                    let foundRole;

                    return new Promise((_resolve, _reject) => {
                        /* Test the pubkey for each role against either the wif key, or the password generated keys */
                        roles.forEach(role => {
                            if (!fromWif) {
                                keyData = this.generateKeyFromPassword(account, role, password);
                            }

                            if (acc) {
                                // 
                                if (role === "memo") {
                                    if (acc.getIn(["options", "memo_key"]) === keyData.pubKey) {
                                        setKey(role, keyData.privKey, keyData.pubKey);
                                        foundRole = keyData.pubKey;
                                        // 
                                        //_resolve(keyData.pubKey);
                                    }
                                } else {
                                    acc.getIn([role, "key_auths"]).forEach(auth => {
                                        // 
                                        // 
                                        if (auth.get(0) === keyData.pubKey) {
                                            foundRole = keyData.pubKey;
                                            setKey(role, keyData.privKey, keyData.pubKey);
                                            if (unlock) {
                                                this.decryptWallet(password_private)
                                                    .then(()=>{
                                                        return this.login(password);
                                                    }).then(()=>{
                                                        // 
                                                        _resolve(foundRole);
                                                    }).catch(()=>{ });
                                            }
                                            return false;
                                        }
                                    });

                                    // 
                                    // debugger;
                                    if (!foundRole) {
                                        let alsoCheckRole = role === "active" ? "owner" : "active";
                                        acc.getIn([alsoCheckRole, "key_auths"]).forEach(
                                            auth => {
                                                // 
                                                // 
                                                if (auth.get(0) === keyData.pubKey) {
                                                    setKey(
                                                        alsoCheckRole,
                                                        keyData.privKey,
                                                        keyData.pubKey
                                                    );
                                                    foundRole = keyData.pubKey;
                                                    _resolve(foundRole);
                                                    return false;

                                                }
                                            }
                                        );
                                    }
                                    _resolve(keyData.pubKey);
                                }
                            } else {
                                _reject({
                                    success: false,
                                    cloudMode: false,
                                    wallet: this.getWallet()
                                });
                            }
                        });
                    }).then(()=> {
                        // debugger;
                        let wallet = this.getWallet();
                        // 
                        // 
                        // 
                        // 
                        /* If the unlock fails and the user has a wallet, check the password against the wallet as well */
                        if (!foundRole && wallet) {
                            this.validatePassword(password, true).then(()=>{
                                // 
                                resolve( {
                                    wallet,
                                    success: true,
                                    cloudMode: false
                                });
                            });
                        }

                        // 
                        // 

                        // 

                        resolve({
                            success: Object.keys(_passwordKey).indexOf(foundRole) !== -1,
                            cloudMode: true,
                            wallet: this.getWallet()
                        });
                    }).catch(()=>{
                        _reject();
                    });
                });



            } else {
                //debugger;
                //let password_aes = Aes.fromSeed(password);
                let wallet = this.getWallet();
                // 
                try {
                    //let password_private = PrivateKey.fromSeed(password);
                    let password_pubkey = password_private
                        .toPublicKey()
                        .toPublicKeyString();

                    if (wallet && wallet.password_pubkey !== password_pubkey) {
                        _reject( {
                            success: false,
                            wallet: this.getWallet(),
                            cloudMode: false
                        });
                    } else {
                        if( !wallet ) {
                            this.onCreateWallet(password, false, true)
                                .then(()=>{
                                    // 
                                    this.validatePassword(password, true);
                                });
                        } else {

                            // 
                            if (unlock) {
                                this.decryptWallet(password_private)
                                    .then(()=>this.login(password))
                                    .then(()=>{
                                        let wallet = this.getWallet();
                                        // let private_key = this.decryptTcomb_PrivateKey(password_private);

                                        // 
                                        // 
                                        // if (!_passwordKey) _passwordKey = {};
                                        // _passwordKey[wallet.password_pubkey] = password_private;
                                        //this.onSetWallet(password_private);
                                        ConfidentialWallet.setKeyLabel(password_private);
                                        // 
                                        resolve( {
                                            success: true,
                                            wallet: this.getWallet(),
                                            cloudMode: false
                                        });
                                    });
                            } else {
                                _reject( {
                                    success: false,
                                    wallet: this.getWallet(),
                                    cloudMode: false
                                });
                            }
                        }
                    }

                } catch (e) {
                    
                    resolve( {
                        success: false,
                        cloudMode: false
                    });
                }
            }
        });
    }


    decryptWallet(private_key) {
        const _this = this;
        return new Promise((resolve, reject)=>{
            //debugger;
            // 
            // 
            const wallet = _this.getWallet();
            let public_key = private_key.toPublicKey().toPublicKeyString();
            // 

            if (!_passwordKey) _passwordKey = {};

            _passwordKey[public_key] = private_key;

            // 

            if ( !wallet ) {
                reject();
            } else if ( wallet && wallet.password_pubkey === public_key) {
                // debugger;

                const resolveWallet = wallet_object => {
                    // debugger;
                    // 

                    /*if( chain_id && chain_id !== wallet_object.chain_id)
                        throw new Error( "Missmatched chain id, wallet has " + wallet_object.chain_id + " but login is expecting " + chain_id )

                    // Merge the wallet_object..  The application code can provided the wallet then login to backup to the server (the wallet restore does this)
                    this.wallet_object = this.wallet_object.mergeDeep(wallet_object)
                    this.private_api_key = this.getPrivateApiKey(private_key)// unlock
                    this.private_key = private_key // unlock
                    this.notify = true
                    return this.notifyResolve(this.sync())*/
                    let wallets = wallet_object.length ? wallet_object[0] : wallet_object;
                    wallets = this.walletNormalize(wallets);
                    let walletKeys = Object.assign(wallet.keys || {}, wallets.keys || {});
                    let walletAssign = Object.assign(wallet, wallets, {keys: walletKeys});
                    //_this.state.wallet = wallet;
                    _this.setState({
                        wallet: walletAssign
                    });
                    ConfidentialWallet.setWalletObject(walletAssign);

                    resolve();
                };

                const {encrypted_wallet} = wallet;
                // 
                // 
                if( encrypted_wallet ) {

                    // Setup wallet_object so sync will have something too look at
                    let backup_buffer = new Buffer(encrypted_wallet, "base64");
                    // 
                    return Backup.decrypt(backup_buffer, private_key).then( wallet_object => {
                        resolveWallet(wallet_object);
                    });
                } else {
                    resolveWallet(wallet);
                }
            }
        });
    }

    login(password) {
        return new Promise(resolve=>{
            // debugger;
            let wallet = this.getWallet();
            // 
            if( wallet && wallet.encryption_key ) {
                let password_aes = Aes.fromSeed(password);
                let encryption_plainbuffer = password_aes.decryptHexToBuffer(
                    wallet.encryption_key
                );
                aes_private = Aes.fromSeed(encryption_plainbuffer);
            }
            resolve();
        });
    }

    walletNormalize = wallet => {
        wallet.created = new Date(wallet.created);
        wallet.last_modified = new Date(wallet.created);

        wallet.backup_date = wallet.backup_date ? new Date(wallet.backup_date) : null;
        wallet.brainkey_backup_date = wallet.brainkey_backup_date ? new Date(wallet.brainkey_backup_date) : null;

        return wallet;

    };

    /** This may lock the wallet unless <b>unlock</b> is used. */
    changePassword(old_password, new_password, unlock = false, account) {
        return new Promise(resolve => {
            let wallet = this.state.wallet;
            // let {success} = this.validatePassword(old_password);
            this.validatePassword(old_password, unlock, account).then(({success})=>{

                if (!success) throw new Error("wrong password");

                let old_password_aes = Aes.fromSeed(old_password);
                let new_password_aes = Aes.fromSeed(new_password);

                if (!wallet.encryption_key)
                    // This change pre-dates the live chain..
                    throw new Error(
                        "This wallet does not support the change password feature."
                    );
                let encryption_plainbuffer = old_password_aes.decryptHexToBuffer(
                    wallet.encryption_key
                );
                wallet.encryption_key = new_password_aes.encryptToHex(
                    encryption_plainbuffer
                );

                let new_password_private = PrivateKey.fromSeed(new_password);
                wallet.password_pubkey = new_password_private
                    .toPublicKey()
                    .toPublicKeyString();

                if (unlock) {
                    aes_private = Aes.fromSeed(encryption_plainbuffer);
                } else {
                    // new password, make sure the wallet gets locked
                    aes_private = null;
                }
                resolve(this.setWalletModified());
            })
        });
    }

    /** @throws "missing brainkey", "wallet locked"
        @return { private_key, sequence }
    */
    generateNextKey(save = true) {
        if (this.generatingKey) return;
        this.generatingKey = true;
        let brainkey = this.getBrainKey();
        let wallet = this.state.wallet;
        let sequence = Math.max(wallet.brainkey_sequence, 0);
        let used_sequence = null;
        // Skip ahead in the sequence if any keys are found in use
        // Slowly look ahead (1 new key per block) to keep the wallet fast after unlocking
        this.brainkey_look_ahead = Math.min(
            10,
            (this.brainkey_look_ahead || 0) + 1
        );
        /* If sequence is 0 this is the first lookup, so check at least the first 10 positions */
        const loopMax = !sequence
            ? Math.max(sequence + this.brainkey_look_ahead, 10)
            : sequence + this.brainkey_look_ahead;
        // 

        for (let i = sequence; i < loopMax; i++) {
            let private_key = key.get_brainPrivateKey(brainkey, i);
            let pubkey = this.generateNextKey_pubcache[i]
                ? this.generateNextKey_pubcache[i]
                : (this.generateNextKey_pubcache[i] = private_key.toPublicKey().toPublicKeyString());

            let next_key = ChainStore.getAccountRefsOfKey(pubkey);
            // TODO if ( next_key === undefined ) return undefined

            /* If next_key exists, it means the generated private key controls an account, so we need to save it */
            if (next_key && next_key.size) {
                used_sequence = i;
                console.log(
                    "WARN: Private key sequence " +
                        used_sequence +
                        " in-use. " +
                        "I am saving the private key and will go onto the next one."
                );
                this.saveKey(private_key, used_sequence);
                // this.brainkey_look_ahead++;
            }
        }
        if (used_sequence !== null) {
            wallet.brainkey_sequence = used_sequence + 1;
            this._updateWallet();
        }
        sequence = Math.max(wallet.brainkey_sequence, 0);
        let private_key = key.get_brainPrivateKey(brainkey, sequence);
        if (save && private_key) {
            // save deterministic private keys ( the user can delete the brainkey )
            // 
            this.saveKey(private_key, sequence);
            //TODO  .error( error => ErrorStore.onAdd( "wallet", "saveKey", error ))
            this.incrementBrainKeySequence();
        }
        this.generatingKey = false;
        return {private_key, sequence};
    }

    incrementBrainKeySequence(transaction) {
        let wallet = this.state.wallet;
        // increment in RAM so this can't be out-of-sync
        wallet.brainkey_sequence++;
        // update last modified
        return this._updateWallet(transaction);
        //TODO .error( error => ErrorStore.onAdd( "wallet", "incrementBrainKeySequence", error ))
    }

    decrementBrainKeySequence() {
        let wallet = this.state.wallet;
        // increment in RAM so this can't be out-of-sync
        wallet.brainkey_sequence = Math.max(0, wallet.brainkey_sequence - 1);
        return this._updateWallet();
    }

    resetBrainKeySequence() {
        let wallet = this.state.wallet;
        // increment in RAM so this can't be out-of-sync
        wallet.brainkey_sequence = 0;
        //
        // update last modified
        return this._updateWallet();
    }

    importKeysWorker(private_key_objs) {
        return new Promise((resolve, reject) => {
            let pubkeys = [];
            for (let private_key_obj of private_key_objs)
                pubkeys.push(private_key_obj.public_key_string);
            let addyIndexPromise = AddressIndex.addAll(pubkeys);

            let private_plainhex_array = [];
            for (let private_key_obj of private_key_objs) {
                private_plainhex_array.push(private_key_obj.private_plainhex);
            }
            if (!__ELECTRON__) {
                AesWorker = require("worker-loader!workers/AesWorker");
            }
            let worker = new AesWorker();
            worker.postMessage({
                private_plainhex_array,
                key: aes_private.key,
                iv: aes_private.iv
            });
            let _this = this;
            this.setState({saving_keys: true});
            worker.onmessage = event => {
                try {
                    //
                    let private_cipherhex_array = event.data;
                    let enc_private_key_objs = [];
                    for (let i = 0; i < private_key_objs.length; i++) {
                        let private_key_obj = private_key_objs[i];
                        let {
                            import_account_names,
                            public_key_string,
                            private_plainhex
                        } = private_key_obj;
                        let private_cipherhex = private_cipherhex_array[i];
                        if (!public_key_string) {
                            // 
                            let private_key = PrivateKey.fromHex(
                                private_plainhex
                            );
                            let public_key = private_key.toPublicKey(); // S L O W
                            public_key_string = public_key.toPublicKeyString();
                        } else if (
                            public_key_string.indexOf(
                                ChainConfig.address_prefix
                            ) != 0
                        )
                            throw new Error(
                                "Public Key should start with " +
                                    ChainConfig.address_prefix
                            );

                        let private_key_object = {
                            import_account_names,
                            encrypted_key: private_cipherhex,
                            pubkey: public_key_string
                            // null brainkey_sequence
                        };
                        enc_private_key_objs.push(private_key_object);
                    }
                    // 
                    let transaction = _this.transaction_update_keys();
                    let insertKeysPromise = idb_helper.on_transaction_end(
                        transaction
                    );
                    try {
                        let duplicate_count = PrivateKeyStore.addPrivateKeys_noindex(
                            enc_private_key_objs,
                            transaction
                        );
                        if (private_key_objs.length != duplicate_count)
                            _this.setWalletModified(transaction);
                        _this.setState({saving_keys: false});
                        resolve(
                            Promise.all([
                                insertKeysPromise,
                                addyIndexPromise
                            ]).then(() => {
                                console.log(
                                    "Done saving keys",
                                    new Date().toString()
                                );
                                // return { duplicate_count }
                            })
                        );
                    } catch (e) {
                        transaction.abort();
                        
                        reject(e);
                    }
                } catch (e) {
                    
                }
            };
        });
    }

    saveKeys(private_keys, transaction, public_key_string) {
        let promises = [];
        for (let private_key_record of private_keys) {
            promises.push(
                this.saveKey(
                    private_key_record.private_key,
                    private_key_record.sequence,
                    null, //import_account_names
                    public_key_string,
                    transaction
                )
            );
        }
        return Promise.all(promises);
    }

    saveKey(
        private_key,
        brainkey_sequence,
        import_account_names,
        public_key_string,
        transaction = this.transaction_update_keys()
    ) {
        let private_cipherhex = aes_private.encryptToHex(
            private_key.toBuffer()
        );
        let wallet = this.state.wallet;
        if (!public_key_string) {
            //S L O W
            // 
            let public_key = private_key.toPublicKey();
            public_key_string = public_key.toPublicKeyString();
        } else if (public_key_string.indexOf(ChainConfig.address_prefix) != 0)
            throw new Error(
                "Public Key should start with " + ChainConfig.address_prefix
            );

        let private_key_object = {
            import_account_names,
            encrypted_key: private_cipherhex,
            pubkey: public_key_string,
            brainkey_sequence
        };
        let p1 = PrivateKeyActions.addKey(private_key_object, transaction).then(
            ret => {
                if (TRACE)
                    
                return ret;
            }
        );
        return p1;
    }

    setWalletModified(transaction) {
        return this._updateWallet(transaction);
    }
    setWallet(wallet) {
        // 
        this.setState({wallet});
    }

    setBackupDate() {
        let wallet = this.state.wallet;
        wallet.backup_date = new Date();
        return this._updateWallet();
    }

    setBrainkeyBackupDate() {
        let wallet = this.state.wallet;
        wallet.brainkey_backup_date = new Date();
        return this._updateWallet();
    }

    /** Saves wallet object to disk.  Always updates the last_modified date. */
    _updateWallet(transaction) {
        //debugger;
        const _this = this;
        let wallet = this.state.wallet;

        if (!wallet) {
            reject("missing wallet");
            return;
        }

        if( !wallet.public_name ) {
            return Promise.resolve();
        }
        //DEBUG 
        wallet.encrypted_wallet = "";
        let wallet_clone = cloneDeep(wallet);
        wallet_clone.last_modified = new Date();
        //wallet_clone.key = new Date().getTime();

        //wallet_clone.encrypted_wallet = "";

        // 

        WalletTcomb(wallet_clone); // validate


        let public_key = wallet.password_pubkey;


        Backup.encrypt(wallet, public_key).then(result => {
            wallet_clone = Object.assign(wallet_clone, {
                encrypted_wallet : result.toString("base64"),
                local_hash : hash.sha256(result).toString("base64")
            });

            if(!transaction) {
                transaction = this.transaction_update();
            }
            let wallet_store = transaction.objectStore("wallet");
            let p = idb_helper.on_request_end(wallet_store.put(wallet_clone));
            let p2 = idb_helper.on_transaction_end(transaction).then(() => {
                //debugger;
                // 
                //_this.state.wallet = wallet_clone;
                _this.setState({wallet: wallet_clone});
                _this.notifyWallet();
            }).catch(res=>{
                // 
            });
            return Promise.all([p, p2]);

        });
    }

    /** This method may be called again should the main database change */
    loadDbData_new(num){
        let wallet_names = Immutable.Set();
        this.legacy_wallet_names = Immutable.Set();

        let current_wallet;

        // 
        // 

        return Promise.resolve()

        // get all wallet names
            .then(()=> {
                return new IndexedDbPersistence("wallet::" + ChainConfig.address_prefix).open();
            })
            .then( db => db.getAllKeys())
            .then( keys => {
                //debugger;
                // 

                for(let name of keys) {
                    wallet_names = wallet_names.add(name);
                }
            })
            // legacy wallet_names
            .then(()=>
                iDB.root.getProperty("wallet_names", []).then( legacy_wallet_names => {
                    for(let name of legacy_wallet_names) {
                        wallet_names = wallet_names.add(name);
                        this.legacy_wallet_names = this.legacy_wallet_names.add(name);
                    }
                })
            )

            .then(()=> iDB.root.getProperty("current_wallet").then(c => current_wallet = c))
            .then(()=>{
                if( !wallet_names.has(current_wallet))
                    current_wallet = wallet_names.size ? wallet_names.first() : undefined;
            })
            .then( ()=> this.setState({ current_wallet, wallet_names }) )
            .then( ()=> this.openWallet(current_wallet) );
    }

    openWallet(wallet_name) {
        // 
    }

    loadDbData() {
        //
        //debugger;
        return idb_helper.cursor("wallet", (cursor) => {
            //debugger;
            if (!cursor) return false;
            let wallet = cursor.value;
            // Convert anything other than a string or number back into its proper type
            wallet.created = new Date(wallet.created);
            wallet.last_modified = new Date(wallet.last_modified);
            wallet.backup_date = wallet.backup_date
                ? new Date(wallet.backup_date)
                : null;
            wallet.brainkey_backup_date = wallet.brainkey_backup_date
                ? new Date(wallet.brainkey_backup_date)
                : null;
            try {
                if( wallet.brainkey_pubkey )
                    WalletTcomb(wallet);
            } catch (e) {
                
            }
            this.state.wallet = wallet;
            // 
            this.setState({wallet});
            ConfidentialWallet.setWalletObject(wallet);
            return false; //stop iterating
        });
    }

    setConfirmTransaction = is_transaction => {
        this.confirm_transactions = is_transaction;
    }
}

const WalletDbWrapped = alt.createStore(WalletDb, "WalletDb");
export default WalletDbWrapped;

function reject(error) {
    
    throw new Error(error);
}


function notifyWallet() {
    this.subscribers.forEach( callback => {
        try { callback("subscribers"); }
        catch(error) {
            
        }
    });
}

import Immutable from "immutable";
import {BigNumber} from "bignumber.js";
import {LimitOrder} from "../lib/common/MarketClasses";
import MarketsCardActions from "actions/MarketsCardActions";

import alt from "../alt-instance";
import ChainStore from "deexjs/es/chain/src/ChainStore";
import ls from "../lib/common/localStorage";
import market_utils from "../lib/common/market_utils";
import {diff} from "deep-object-diff";

//let marketStorage = new ls("__deexgraphene__");
const nullPrice = {
    getPrice: () => {
        return 0;
    },
    sellPrice: () => {
        return 0;
    }
};
class MarketsCardStore {
    constructor() {
        this.marketLimitOrders = Immutable.Map();
        this.marketSettleOrders = Immutable.OrderedSet();
        this.activeMarketHistory = Immutable.OrderedSet();
        this.marketCallOrders = Immutable.Map();
        this.marketData = Immutable.Map();
        /*
            bids: [],
            asks: [],
            calls: [],
            combinedBids: [],
            highestBid: nullPrice,
            combinedAsks: [],
            lowestAsk: nullPrice,
            flatBids: [],
            flatAsks: [],
            flatCalls: [],
            flatSettles: []
        * */
        this.totals = {};
        this.allCallOrders = [];
        this.priceData = [];
        this.volumeData = [];
        this.pendingCreateLimitOrders = [];
        this.activeMarket = [];
        this.quoteAsset = null;
        this.pendingCounter = 0;
        this.priceHistory = [];
        this.subscribers = new Map();


        // console.log('MarketsCardStore this', this);
        // console.log('MarketsCardStore MarketsCardActions', MarketsCardActions);

        /*
        * MarketsCardActions.subscribeMarket
        * MarketsCardActions.subscribeMarket
        * */

        // console.log('MarketsCardActions', MarketsCardActions);


        this.bindListeners({
            onSubscribeCardMarket: MarketsCardActions.subscribeCardMarket,
            onCardUnSubscribeMarket: MarketsCardActions.unSubscribeCardMarket,
            onCardClearMarket: MarketsCardActions.clearMarket,
            onCardSwitchMarket: MarketsCardActions.switchMarket
        });

        this.exportPublicMethods({
            subscribe: this.subscribe.bind(this),
            unsubscribe: this.unsubscribe.bind(this),
            clearSubs: this.clearSubs.bind(this)
        });
    }

    /**
     *  Add a callback that will be called anytime any object in the cache is updated
     */
    subscribe(id, callback) {
        if (this.subscribers.has(id) && this.subscribers.get(id) === callback)
            return console.error("Subscribe callback already exists", callback);
        this.subscribers.set(id, callback);
    }

    /**
     *  Remove a callback that was previously added via subscribe
     */
    unsubscribe(id) {
        if (this.subscribers.has(id)) {
            this.subscribers.delete(id);
        }
    }

    _notifySubscriber(id, data) {
        if (this.subscribers.has(id)) this.subscribers.get(id)(data);
    }

    clearSubs() {
        this.subscribers.clear();
    }

    onSubscribeCardMarket(result) {

        // console.log('result onSubscribeMarket', result, this);
        // console.log('MarketsCardActions.switchMarket', MarketsCardActions);
        if (result.switchMarket) {
            this.marketReady = false;
            return this.emitChange();
        }


        let newMarket = false;
        let limitsChanged = false,
            callsChanged = false;
        this.invertedCalls = result.inverted;

        // Get updated assets every time for updated feed data
        //console.log('ALT.DISPATCHER RESULT ChainStore',ChainStore.getAsset(result.quote.get("id")));
        this.quoteAsset = ChainStore.getAsset(result.quote.get("id"));
        this.baseAsset = ChainStore.getAsset(result.base.get("id"));
        if( !this.quoteAsset ) this.quoteAsset = result.quote;
        if( !this.baseAsset ) this.baseAsset = result.base;

        const assets = {
            [this.quoteAsset.get("id")]: {
                precision: this.quoteAsset.get("precision")
            },
            [this.baseAsset.get("id")]: {
                precision: this.baseAsset.get("precision")
            }
        };

        if (result.market && this.activeMarket.indexOf( result.market ) === -1 ) {
            // console.log("switch active market from", this.activeMarket, "to", result.market);
            this.onCardClearMarket();
            this.activeMarket.push(result.market);
            this.marketLimitOrders = this.marketLimitOrders.set(result.market, Immutable.Map());
            newMarket = true;

            this.unsubscribe("subscribeCard");
        }

        if (result.buckets) {
            this.buckets = result.buckets;
            if (result.buckets.indexOf(this.bucketSize) === -1) {
                this.bucketSize = result.buckets[result.buckets.length - 1];
            }
        }


        if (result.limits) {
            // Keep an eye on this as the number of orders increases, it might not scale well
            const oldmarketLimitOrders = this.marketLimitOrders;
            // this.marketLimitOrders = this.marketLimitOrders.clear();
            // console.time("Create limit orders " + this.activeMarket);
            result.limits.forEach(order => {
                // ChainStore._updateObject(order, false, false);
                if (typeof order.for_sale !== "number") {
                    order.for_sale = parseInt(order.for_sale, 10);
                }
                let getMarketLimitOrders = this.marketLimitOrders.get(result.market);
                // console.log('order.id', order.id, order, this);
                order.expiration = new Date(order.expiration);
                this.marketLimitOrders = this.marketLimitOrders.set(
                    result.market,
                    getMarketLimitOrders.set(order.id, new LimitOrder(order, assets, this.quoteAsset.get("id")))
                );
               // console.log("getMarketLimitOrders", order.id,order, new LimitOrder(order, assets, this.quoteAsset.get("id")));
            });

            for (let i = 0; i <= this.activeMarket.length; i++ ) {
                let currentMarket  = this.activeMarket[i];
                let getOldmarketLimitOrders = oldmarketLimitOrders.get(currentMarket);
                let getMarketLimitOrders = this.marketLimitOrders.get(currentMarket);

                limitsChanged = diff(
                    getMarketLimitOrders,
                    getOldmarketLimitOrders
                );

                if( Object.keys(limitsChanged).length > 0) {
                    this._orderBook(currentMarket);
                    this._depthChart(currentMarket);
                }
            }

            // Loop over pending orders to remove temp order from orders map and remove from pending
            for (
                let i = this.pendingCreateLimitOrders.length - 1;
                i >= 0;
                i--
            ) {
                let myOrder = this.pendingCreateLimitOrders[i];
                let order = this.marketLimitOrders.find(order => {
                    return (
                        myOrder.seller === order.seller &&
                        myOrder.expiration === order.expiration
                    );
                });

                // If the order was found it has been confirmed, delete it from pending
                if (order) {
                    this.pendingCreateLimitOrders.splice(i, 1);
                }
            }

            // console.timeEnd("Create limit orders " + this.activeMarket);

            if (this.pendingCreateLimitOrders.length === 0) {
                this.pendingCounter = 0;
            }

            // console.log("time to process limit orders:", new Date() - limitStart, "ms");
        }


        /*if (result.history) {
            this.activeMarketHistory = this.activeMarketHistory.clear();
            result.history.forEach(order => {
                if (!/Z$/.test(order.time)) {
                    order.time += "Z";
                }
                order.op.time = order.time;
                /!* Only include history objects that aren't 'something for nothing' to avoid confusion *!/
                if (
                    !(
                        order.op.receives.amount == 0 ||
                        order.op.pays.amount == 0
                    )
                ) {
                    this.activeMarketHistory = this.activeMarketHistory.add(
                        order.op
                    );
                }
            });
        }

        if (result.fillOrders) {
            result.fillOrders.forEach(fill => {
                // console.log("fill:", fill);
                this.activeMarketHistory = this.activeMarketHistory.add(
                    fill[0][1]
                );
            });
        }*/


        //marketStorage.set("lowVolumeMarkets", this.lowVolumeMarkets.toJS());

        this.marketReady = true;
        //this.emitChange();

        if (newMarket) {
            // console.log("_notifySubscriber market_change");
            this._notifySubscriber(
                "market_change",
                this.quoteAsset.get("symbol") +
                "_" +
                this.baseAsset.get("symbol")
            );
        }
        if (result.resolve) result.resolve();
    }

    onCardUnSubscribeMarket(payload) {
        // Optimistic removal of activeMarket
        if (payload && payload.unSub) {
            this.activeMarket = [];
        } else {
            // Unsub failed, restore activeMarket
            this.activeMarket.push(payload.market);
        }
    }

    onCardClearMarket() {
        // console.log("onClearMarket");
    }

    onCardSwitchMarket() {
        this.marketReady = false;
    }

    _getMarketData() {
        return {
            bids: [],
            asks: [],
            calls: [],
            combinedBids: [],
            highestBid: nullPrice,
            combinedAsks: [],
            lowestAsk: nullPrice,
            flatBids: [],
            flatAsks: [],
            flatCalls: [],
            flatSettles: []
        };
    }


    _orderBook(nameData) {
        // Loop over limit orders and return array containing bids
        let constructBids = orderArray => {
            let bids = orderArray
                .filter(a => {
                    return a.isBid();
                })
                .sort((a, b) => {
                    return a.getPrice() - b.getPrice();
                })
                .map(order => {
                    return order;
                })
                .toArray();

            // Sum bids at same price
            if (bids.length > 1) {
                for (let i = bids.length - 2; i >= 0; i--) {
                    if (bids[i].getPrice() === bids[i + 1].getPrice()) {
                        bids[i] = bids[i].sum(bids[i + 1]);
                        bids.splice(i + 1, 1);
                    }
                }
            }
            return bids;
        };
        // Loop over limit orders and return array containing asks
        let constructAsks = orderArray => {
            let asks = orderArray
                .filter(a => {
                    return !a.isBid();
                })
                .sort((a, b) => {
                    return a.getPrice() - b.getPrice();
                })
                .map(order => {
                    return order;
                })
                .toArray();

            // Sum asks at same price
            if (asks.length > 1) {
                for (let i = asks.length - 2; i >= 0; i--) {
                    if (asks[i].getPrice() === asks[i + 1].getPrice()) {
                        asks[i] = asks[i].sum(asks[i + 1]);
                        asks.splice(i + 1, 1);
                    }
                }
            }
            return asks;
        };

        if(!this.marketData.has(nameData)) {
            this.marketData = this.marketData
                .set(nameData, Immutable.Map(this._getMarketData()));
        }
        // Assign to store variables
        let bids = constructBids(this.marketLimitOrders.get(nameData));
        let asks = constructAsks(this.marketLimitOrders.get(nameData));
        let marketData = this.marketData.get(nameData);
        marketData = marketData
            .set("bids", bids)
            .set("asks", asks);
        this.marketData = this.marketData.set(nameData, marketData);
        // console.log("time to construct orderbook:", new Date() - orderBookStart, "ms");
    }

    _depthChart(nameData) {
        let bids = [],
            asks = [],
            calls = [],
            totalBids = 0,
            totalAsks = 0,
            totalCalls = 0;
        let flat_bids = [],
            flat_asks = [],
            flat_calls = [],
            flat_settles = [];


        if (this.marketLimitOrders.has(nameData)) {
            let marketData = this.marketData.get(nameData).toJS();
            let totalBigNumberAsk = new BigNumber(0);
            let totalBigNumberBid = new BigNumber(0);

            marketData.bids.forEach(order => {
                let orderBnBid = new BigNumber(order.amountForSale().getAmount({real: true}));
                totalBigNumberBid = totalBigNumberBid.plus(orderBnBid);
                bids.push([
                    order.getPrice(),
                    orderBnBid.toNumber()
                ]);
                totalBids = totalBigNumberBid.toNumber();
            });

            // console.log('depthChart bids', bids);
            // console.log('depthChart totalBids', totalBids);



            marketData.asks.forEach(order => {
                let orderBnAsk = new BigNumber(order.amountForSale().getAmount({real: true}));
                totalBigNumberAsk = totalBigNumberAsk.plus(orderBnAsk);
                // if( typeof order.amountToReceive().getAmount({real: true}) === "number") {
                /*asks.push([
                    order.getPrice(),
                    order.amountForSale().getAmount({real: true})
                ]);*/
                asks.push([
                    order.getPrice(),
                    orderBnAsk.toNumber()
                ]);
                // console.log("totalAsks order", order, order.getPrice(), order.amountForSale(), order.amountForSale().getAmount({real: true}));
                // console.log("totalAsks orderBnAsk", orderBnAsk, orderBnAsk.toFixed(), orderBnAsk.toNumber());
                // console.log("totalAsks orderSumBnAsk", totalBigNumberAsk, totalBigNumberAsk.toFixed(), totalBigNumberAsk.toNumber());
                totalAsks = totalBigNumberAsk.toNumber();
                // totalAsks += order.amountToReceive().getAmount({real: true});
                // }
            });

            // Make sure the arrays are sorted properly
            asks.sort((a, b) => {
                return a[0] - b[0];
            });

            bids.sort((a, b) => {
                return a[0] - b[0];
            });

            // Flatten the arrays to get the step plot look
            flat_bids = market_utils.flatten_orderbookchart_highcharts(
                bids,
                true,
                true,
                1000
            );


            if (flat_bids.length) {
                flat_bids.unshift([0, flat_bids[0][1]]);
            }

            // console.log("flat_asks asks", asks);

            flat_asks = market_utils.flatten_orderbookchart_highcharts(
                asks,
                true,
                false,
                1000
            );
            if (flat_asks.length) {
                flat_asks.push([
                    flat_asks[flat_asks.length - 1][0] * 1.5,
                    flat_asks[flat_asks.length - 1][1]
                ]);
                //totalAsks = flat_asks[flat_asks.length - 1][1];
            }
            /* Flatten call orders if there any */
            if (marketData.calls.length) {
                let callsAsBids = marketData.calls[0].isBid();
                marketData.calls.forEach(order => {
                    calls.push([
                        order.getSqueezePrice(),
                        order[order.isBid() ? "amountToReceive" : "amountForSale"]().getAmount({real: true})
                    ]);
                });

                // Calculate total value of call orders
                calls.forEach(call => {
                    if (this.invertedCalls) {
                        totalCalls += call[1];
                    } else {
                        totalCalls += call[1] * call[0];
                    }
                });

                // console.log("totalCalls", totalCalls);

                if (callsAsBids) {
                    totalBids += totalCalls;
                } else {
                    totalAsks += totalCalls;
                }

                // Make sure the array is sorted properly
                calls.sort((a, b) => {
                    return a[0] - b[0];
                });

                // Flatten the array to get the step plot look
                if (this.invertedCalls) {
                    flat_calls = market_utils.flatten_orderbookchart_highcharts(
                        calls,
                        true,
                        false,
                        1000
                    );
                    if (
                        flat_asks.length &&
                        flat_calls[flat_calls.length - 1][0] <
                        flat_asks[flat_asks.length - 1][0]
                    ) {
                        flat_calls.push([
                            flat_asks[flat_asks.length - 1][0],
                            flat_calls[flat_calls.length - 1][1]
                        ]);
                    }
                } else {
                    flat_calls = market_utils.flatten_orderbookchart_highcharts(
                        calls,
                        true,
                        true,
                        1000
                    );
                    if (flat_calls.length > 0) {
                        flat_calls.unshift([0, flat_calls[0][1]]);
                    }
                }
            }

            /* Flatten settle orders if there are any */
            if (this.marketSettleOrders.size) {
                flat_settles = this.marketSettleOrders.reduce((final, a) => {
                    if (!final) {
                        return [
                            [
                                a.getPrice(),
                                a[!a.isBid() ? "amountForSale" : "amountToReceive"]().getAmount({real: true})
                            ]
                        ];
                    } else {
                        final[0][1] =
                            final[0][1] +
                            a[!a.isBid() ? "amountForSale" : "amountToReceive"]().getAmount({real: true});
                        return final;
                    }
                }, null);

                flat_settles.push([
                    flat_asks[flat_asks.length - 1][0],
                    flat_settles[0][1]
                ]);
            }

            // console.log("totalAsks", totalAsks);

            // Assign to store variables
            marketData.flatAsks = flat_asks;
            marketData.flatBids = flat_bids;
            marketData.flatCalls = flat_calls;
            marketData.flatSettles = flat_settles;
            marketData.totalBids = totalBids || 0;
            marketData.totalAsk = totalAsks || 0;
            marketData.totalCalls = totalCalls || 0;
            this.totals[nameData] = {
                bid: totalBids || 0,
                ask: totalAsks || 0,
                call: totalCalls || 0
            };
            this.marketData = this.marketData.set(nameData, Immutable.fromJS(marketData));
            // console.log('this.marketData', this.marketData);

        }

    }

}

export default alt.createStore(MarketsCardStore, "MarketsCardStore");

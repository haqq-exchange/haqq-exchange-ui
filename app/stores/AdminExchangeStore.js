import alt from "alt-instance";
import AdminExchangeActions from "actions/AdminExchangeActions";
import Immutable, {fromJS} from "immutable";
import ls from "common/localStorage";
import {settingsAPIs, getSettingApi} from "api/apiConfig";
import BaseStore from "./BaseStore";

import configConst from "config/const";
let ss = new ls(configConst.STORAGE_KEY);

class AdminExchangeStore extends BaseStore{
    constructor() {
        super();

        this.state = {
            sortTableCurrency : ss.get("sort_table", null),
            sort : ss.get("sort", {
                sort_by: "name"
            }),
            referralInfo : {},
            exchangeAssets: [],
            referralUsers : []
        };

        this.bindListeners({
            onSetReferralInfo: AdminExchangeActions.setReferralInfo,
            onSetReferralUsers: AdminExchangeActions.setReferralUsers,
            onSetExchangeAssets: AdminExchangeActions.setExchangeAssets,
            onSetSortTable: AdminExchangeActions.setSortTable,
            onSetSortTableCurrency: AdminExchangeActions.setSortTableCurrency,
            onSetReferralUsersStats: AdminExchangeActions.setReferralUsersStats,
        });


    }


    onSetReferralInfo = (referralInfo) => {
        // console.log("info", referralInfo);
        this.setState({referralInfo});

    };

    onSetReferralUsers = (referralUsers) => {
        // console.log("users", referralUsers);
        this.setState({referralUsers});
    };
    onSetReferralUsersStats = (stats) => {
        let {referralUsers} = this.state;
        new Promise(resolve=>{
            resolve(referralUsers
                .map(user=> Object.assign(user, stats[user.name])));
        }).then(nextUsersState=>{
            this.setState({referralUsers: nextUsersState});
        });
    };

    onSetExchangeAssets = (exchangeAssets) => {
        this.setState({exchangeAssets});
    };

    onSetSortTableCurrency = (sortTableCurrency) => {
        ss.set("sort_table", sortTableCurrency);
        this.setState({sortTableCurrency});
    };

    onSetSortTable  = (sort) => {
        ss.set("sort", sort);
        this.setState({sort});
    };
}

export default alt.createStore(AdminExchangeStore, "AdminExchangeStore");

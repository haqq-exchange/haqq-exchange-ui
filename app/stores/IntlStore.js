import alt from "alt-instance";
import IntlActions from "actions/IntlActions";
import SettingsActions from "actions/SettingsActions";
import counterpart from "counterpart";
import ls from "common/localStorage";
import configConst from "config/const";
let ss = new ls(configConst.STORAGE_KEY);
var locale_en = __GBL_CHAIN__ ? require("assets/locales-gbl/locale-en.json") : require("assets/locales/locale-en.json");
//let locale_en = require("json-loader!/app/assets/locales/locale-en.json");
// let locale_en = require("/app/assets/locales/locale-en.json");
// let locale_en1 = require("./assets/locales/locale-en.json");
// let locale_en2 = require("./locales/locale-en.json");
// let locale_en3 = require("./locale-en.json");
// let locale_en4 = require("json-loader!locale-en.json");
// let locale_en3 = require("json-loader!./locale-en.json");
// let locale_en2 = require("json-loader!./locales/locale-en.json");
// let locale_en1 = require("json-loader!./assets/locales/locale-en.json");
// let locale_en = require("json-loader!/app/assets/localeslocale-en.json");

//window.console.log("locale_en", locale_en);

let localesData = {};

import {addLocaleData} from "react-intl"; 

import localeCodes from "assets/locales"; 

for (let localeCode of localeCodes) { 
    let siteLocale = localeCode === "es" ? "en" : localeCode;
    localesData[siteLocale] = __GBL_CHAIN__ ? require(`assets/locales-gbl/locale-${siteLocale}.json`) : require(`assets/locales/locale-${siteLocale}.json`);
    addLocaleData(require(`react-intl/locale-data/${localeCode}`));
}

class IntlStore {
    constructor() {
        if (!ss.has("resetSettings_1")) {
            try {
                localStorage.clear();
            } catch (e) {
                console.error(e);
            }
            ss.set("resetSettings_1", {date: new Date().getTime()});
        }
        var langData = {
            "en-US": "en",
            "ru-RU": "ru",
            "tr-TR": "tr",
            "zh-Hans-CN": "zh",
        };
        var language = navigator.language || navigator.userLanguage;
        this.currentLocale = ss.has("settings_v3")
            ? ss.get("settings_v3").locale
            : langData[language] || "en";

        this.locales = Object.keys(localesData);
        this.localesObject = localesData;

        this.bindListeners({
            onSwitchLocale: IntlActions.switchLocale,
            onGetLocale: IntlActions.getLocale,
            onClearSettings: SettingsActions.clearSettings
        });

        counterpart.registerTranslations(this.currentLocale, localesData[this.currentLocale]);
        counterpart.setFallbackLocale(this.currentLocale);
        counterpart.setLocale(this.currentLocale);
    }

    hasLocale(locale) {
        return this.locales.indexOf(locale) !== -1;
    }

    getCurrentLocale() {
        return this.currentLocale;
    }

    onSwitchLocale({locale, localeData}) {
        counterpart.registerTranslations(locale, localeData);
        counterpart.setLocale(locale);
        this.currentLocale = locale;
    }

    onGetLocale(locale) {
        if (this.locales.indexOf(locale) === -1) {
            this.locales.push(locale);
        }
    }

    onClearSettings() {
        this.onSwitchLocale({locale: "en"});
    }
}

export default alt.createStore(IntlStore, "IntlStore");

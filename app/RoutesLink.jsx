import loadable from "loadable-components";
import Loadable from "react-loadable";
import LoadingIndicator from "Components/LoadingIndicator";


export const EmptyPage = loadable(() => import("components/EmptyPage"));
export const DashboardPage = loadable(() => import("components/Dashboard/DashboardPage"));
export const DashboardAccountsOnly = loadable(() => import("components/Dashboard/DashboardAccountsOnly"));

export const Explorer = loadable(() => import("components/Explorer/Explorer"));
//export const FeesContainer = loadable(() => import("components/Explorer/FeesContainer"));
export const BlocksContainer = loadable(() => import("components/Explorer/BlocksContainer"));
export const AssetsContainer = loadable(() => import("components/Explorer/AssetsContainer"));
export const AccountsContainer = loadable(() => import("components/Explorer/AccountsContainer"));
export const Witnesses = loadable(() => import("components/Explorer/Witnesses"));
export const CommitteeMembers = loadable(() => import("components/Explorer/CommitteeMembers"));
// export const MarketsContainer = loadable(() => import("components/Explorer/MarketsContainer"));

export const WalletManager = loadable(() => import("components/Wallet/WalletManager"));
export const WalletOptions = loadable(() => import("components/Wallet/WalletOptions"));
export const ChangeActiveWallet = loadable(() => import("components/Wallet/ChangeActiveWallet"));
export const WalletDelete = loadable(() => import("components/Wallet/WalletDelete"));

export const WalletChangePassword = loadable(() => import("components/Wallet/WalletChangePassword"));
export const ImportKeys = loadable(() => import("components/Wallet/ImportKeys"));
export const Brainkey = loadable(() => import("components/Wallet/Brainkey"));
export const WalletCreate = loadable(() => import("components/Wallet/WalletCreate"));
export const BackupCreate = loadable(() => import("components/Wallet/BackupCreate"));
export const BackupRestore = loadable(() => import("components/Wallet/BackupRestore"));
export const BackupBrainkey = loadable(() => import("components/Wallet/BackupBrainkey"));
export const BalanceClaimActive = loadable(() => import("components/Wallet/BalanceClaimActive"));
export const ExistingAccount = loadable(() => import("components/Wallet/ExistingAccount"));

export const Transfer = loadable(() => import("components/Transfer/Transfer"));
export const Invoice = loadable(() => import("components/Transfer/Invoice"));

export const ExchangeContainer = loadable(() => import("components/Exchange/ExchangeContainer"));

export const SettingsContainer = loadable(() => import("components/Settings/SettingsContainer"));

export const BlockContainer = loadable(() => import("components/Blockchain/BlockContainer"));
export const Asset = loadable(() => import("components/Blockchain/Asset"));

export const AccountPage = loadable(() => import("components/Account/AccountPage"));
export const AccountOverview = loadable(() => import("components/Account/AccountOverview"));
export const AccountAssets = loadable(() => import("components/Account/AccountAssets"));
export const AccountAssetCreate = loadable(() => import("components/Account/AccountAssetCreate"));
export const AccountAssetUpdate = loadable(() => import("components/Account/AccountAssetUpdate"));
export const AccountMembership = loadable(() => import("components/Account/AccountMembership"));
export const AccountVesting = loadable(() => import("components/Account/AccountVesting"));
export const AccountPermissions = loadable(() => import("components/Account/AccountPermissions"));
export const AccountVoting = loadable(() => import("components/Account/AccountVoting"));
export const AccountWhitelist = loadable(() => import("components/Account/AccountWhitelist"));
export const AccountSignedMessages = loadable(() => import("components/Account/AccountSignedMessages"));
export const AccountDepositWithdraw = loadable(() => import("components/Account/AccountDepositWithdraw"));
export const AccountCreateWorker = loadable(() => import("components/Account/CreateWorker"));



export const InitError = loadable(() => import("components/InitError"));
export const News = loadable(() => import("components/News"));
export const Help = loadable(() => import("components/Help"));
export const Page404 = loadable(() => import("components/Page404/Page404"));

export const Create2faAccountPassword = loadable(() => import("./components/Account/CreateAccount/Create2faAccountPassword"));
export const CreateAccountPassword = loadable(() => import("./components/Account/CreateAccount/CreateAccountPassword"));
export const CreateAccount = loadable(() => import("./components/Account/CreateAccount/CreateAccount"));
export const CreateAccountIndex = loadable(() => import("./components/Account/CreateAccount/CreateAccountIndex"));
export const AutorithationAccountIndex = loadable(() => import("./components/Account/Autorithation/AutorithationAccountIndex"));

export const LoginBody = loadable(() => import("./components/Public/Login/Body"));
export const LoginSelector = loadable(() => import("components/LoginSelector"));
export const LoginOldContainer = loadable(() => import("./components/Account/Autorithation/LoginOld"));
export const Login2fa = loadable(() => import("./components/Account/Autorithation/Login2fa"));
export const LoginLW = loadable(() => import("./components/Account/Autorithation/LoginLW"));

/* Admin */
export const AdminRouters = Loadable({
    loader: () => import(/* webpackChunkName: "AdminExchange" */ "Components/AdminPages/AdminRouters"),
    loading: LoadingIndicator
});
export const AdminExchange = Loadable({
    loader: () => import(/* webpackChunkName: "AdminExchange" */ "Components/AdminExchange/IndexPage"),
    loading: LoadingIndicator
});
export const MainAdmin = Loadable({
    loader: () => import(/* webpackChunkName: "MainAdmin" */ "Components/AdminPages/MainAdmin"),
    loading: LoadingIndicator
});
/* Admin */
/* Credit */

export const CreditPage = Loadable({
    loader: () => import(/* webpackChunkName: "CreditPage" */ "./components/Credit/CreditPage"),
    loading: LoadingIndicator
});
export const CreditCalculator = Loadable({
    loader: () => import(/* webpackChunkName: "CreditPage" */ "./components/Credit/CreditCalculator"),
    loading: LoadingIndicator
});
export const MyCredit = Loadable({
    loader: () => import(/* webpackChunkName: "MyCredit" */ "./components/Credit/MyCredit/MyCredit"),
    loading: LoadingIndicator
});
export const ViewCredit = Loadable({
    loader: () => import(/* webpackChunkName: "ViewCredit" */ "./components/Credit/ViewCredit/ViewCredit"),
    loading: LoadingIndicator
});
/* Credit */

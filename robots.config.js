module.exports = {
    policy: [
        {
            userAgent: "Yandex",
            allow: ["/$", "/market/DEEX_BTC", "/create-account", "/authorization"],
            disallow: ["/", "/create-account/*", "/authorization/*"]
        },{
            userAgent: "Googlebot",
            allow: ["/$", "/market/DEEX_BTC", "/create-account", "/authorization"],
            disallow: ["/", "/create-account/*", "/authorization/*"]
        },{
            userAgent: "*",
            allow: ["/$", "/market/DEEX_BTC", "/create-account", "/authorization"],
            disallow: ["/", "/create-account/*", "/authorization/*"]
        }

    ],
    sitemap: "https://deex.exchange/sitemap.xml",
    host: "https://deex.exchange"
};
